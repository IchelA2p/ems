﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace VendorIDRequestForm
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }


        private void numbersOnly(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || e.KeyChar == (char)13)
            {
                e.Handled = false;

                if (e.KeyChar == (char)13)
                {
                    FindUSP();
                }
            }
            else
            {               
                e.Handled = true; 
            }
        }

        private void FindUSP()
        {
            string query = string.Format(@"select distinct rtrim(Vendor_ID) Vendor_ID, rtrim(Vendor_Name) Vendor_Name, rtrim([Address]) [Address], rtrim(City) City, rtrim([State]) [State], rtrim(Zipcode) Zipcode, case when vdr_status = 0 then 'Inactive' else 'Active' end [Status] from vw_VIDList where vdr_status < 2 and ");

            if (txtVID.Text != string.Empty)
            {
                query += string.Format("Vendor_ID = '{0}' and ", txtVID.Text);
            }

            if (txtUSP.Text != string.Empty)
            {
                query += string.Format("Vendor_Name like '%{0}%' and ", txtUSP.Text);
            }


            if (txtStreet.Text != string.Empty)
            {
                query += string.Format("[Address] like '%{0}%' and ", txtStreet.Text);
            }


            if (txtCity.Text != string.Empty)
            {
                query += string.Format("City like '%{0}%' and ", txtCity.Text);
            }

            if (txtZipcode.Text != string.Empty)
            {
                query += string.Format("Zipcode = '{0}' and ", txtZipcode.Text);
            }

            query = query.ToString().Substring(0, query.Length - 4) + "order by Vendor_Name, [Address]";

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            dgList.DataSource = dt;
        }


        private void btnRequest_Click(object sender, EventArgs e)
        {
            Form frmReq = new Form1();
            frmReq.Show();
        }


        private void triggerSearch(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnSearch_Click(null, null);
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            FindUSP();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            FindUSP();
        }

    }
}
