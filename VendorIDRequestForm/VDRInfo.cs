﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VendorIDRequestForm
{
    public class VDRInfo
    {
        public string Vendor_ID { get; set; }
        public string Vendor_Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zipcode { get; set; }
        public string Phonenumber { get; set; }
        public string vdr_status { get; set; }
        public string vms_status { get; set; }
    }
}
