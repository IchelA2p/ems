﻿namespace VendorIDRequestForm
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.tb_USPName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tb_Street = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tb_City = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tb_ZipCode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btn_Submit = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_Email = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.cb_NoEmail = new System.Windows.Forms.CheckBox();
            this.tb_Telephone = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.dgv_VIDRequest = new System.Windows.Forms.DataGridView();
            this.imgPrio = new System.Windows.Forms.DataGridViewImageColumn();
            this.btnApprove = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnReject = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cmbDeclineReason = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.label12 = new System.Windows.Forms.Label();
            this.cmb_State = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbPrio = new System.Windows.Forms.ComboBox();
            this.cmbSource = new System.Windows.Forms.ComboBox();
            this.label26 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.chkBox_Payments = new System.Windows.Forms.CheckBox();
            this.chkBox_Water = new System.Windows.Forms.CheckBox();
            this.chkBox_Electricity = new System.Windows.Forms.CheckBox();
            this.chkBox_Gas = new System.Windows.Forms.CheckBox();
            this.chkBox_Others = new System.Windows.Forms.CheckBox();
            this.tb_SourceOfInfo = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.tb_POBox = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.tb_Dept = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.tb_OfficeName = new System.Windows.Forms.TextBox();
            this.tb_BldgName = new System.Windows.Forms.TextBox();
            this.btnShowAll = new System.Windows.Forms.Button();
            this.tb_CoOrAtt = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.dgDashboard = new System.Windows.Forms.DataGridView();
            this.dgv_VDR = new System.Windows.Forms.DataGridView();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tagToInvoiceToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label27 = new System.Windows.Forms.Label();
            this.timer_CountDown = new System.Windows.Forms.Timer(this.components);
            this.tb_VDR = new System.Windows.Forms.TextBox();
            this.tb_VIDR = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.grpSummary = new System.Windows.Forms.GroupBox();
            this.lbluTotal = new System.Windows.Forms.Label();
            this.lblRejected = new System.Windows.Forms.Label();
            this.lblApproved = new System.Windows.Forms.Label();
            this.lblPending = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.grpSummary2 = new System.Windows.Forms.GroupBox();
            this.lbloTotal = new System.Windows.Forms.Label();
            this.lbloRej = new System.Windows.Forms.Label();
            this.lbloApp = new System.Windows.Forms.Label();
            this.label41 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.tbMain = new System.Windows.Forms.TabControl();
            this.tbNewPending = new System.Windows.Forms.TabPage();
            this.tbRejects = new System.Windows.Forms.TabPage();
            this.dgRejects = new System.Windows.Forms.DataGridView();
            this.imgPrio_rej = new System.Windows.Forms.DataGridViewImageColumn();
            this.btnApprove_rej = new System.Windows.Forms.DataGridViewButtonColumn();
            this.btnReject_rej = new System.Windows.Forms.DataGridViewButtonColumn();
            this.cmbDeclineReason_rej = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.tbAll = new System.Windows.Forms.TabPage();
            this.dgAll = new System.Windows.Forms.DataGridView();
            this.btnMain = new System.Windows.Forms.Button();
            this.btnReports = new System.Windows.Forms.Button();
            this.grpReports = new System.Windows.Forms.GroupBox();
            this.label47 = new System.Windows.Forms.Label();
            this.lnkFile = new System.Windows.Forms.LinkLabel();
            this.btnExport = new System.Windows.Forms.Button();
            this.btnShow = new System.Windows.Forms.Button();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.cmbApprover = new System.Windows.Forms.ComboBox();
            this.label46 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkAll = new System.Windows.Forms.CheckBox();
            this.dtpTo = new System.Windows.Forms.DateTimePicker();
            this.label35 = new System.Windows.Forms.Label();
            this.dtpFrom = new System.Windows.Forms.DateTimePicker();
            this.label36 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_VIDRequest)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDashboard)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_VDR)).BeginInit();
            this.contextMenuStrip1.SuspendLayout();
            this.grpSummary.SuspendLayout();
            this.grpSummary2.SuspendLayout();
            this.tbMain.SuspendLayout();
            this.tbNewPending.SuspendLayout();
            this.tbRejects.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgRejects)).BeginInit();
            this.tbAll.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgAll)).BeginInit();
            this.grpReports.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(670, 168);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(96, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type of Request:";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "ACTIVATION",
            "CREATE NEW"});
            this.comboBox1.Location = new System.Drawing.Point(788, 164);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(187, 22);
            this.comboBox1.TabIndex = 1;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            this.comboBox1.TextChanged += new System.EventHandler(this.comboBox1_TextChanged);
            // 
            // tb_USPName
            // 
            this.tb_USPName.Location = new System.Drawing.Point(126, 52);
            this.tb_USPName.MaxLength = 100;
            this.tb_USPName.Name = "tb_USPName";
            this.tb_USPName.Size = new System.Drawing.Size(181, 22);
            this.tb_USPName.TabIndex = 3;
            this.tb_USPName.TextChanged += new System.EventHandler(this.tb_USPName_TextChanged);
            this.tb_USPName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_USPName_KeyPress);
            this.tb_USPName.Leave += new System.EventHandler(this.tb_USPName_Leave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 57);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 14);
            this.label3.TabIndex = 4;
            this.label3.Text = "USP Name:";
            // 
            // tb_Street
            // 
            this.tb_Street.Location = new System.Drawing.Point(427, 24);
            this.tb_Street.MaxLength = 100;
            this.tb_Street.Name = "tb_Street";
            this.tb_Street.Size = new System.Drawing.Size(185, 22);
            this.tb_Street.TabIndex = 4;
            this.tb_Street.TextChanged += new System.EventHandler(this.tb_Street_TextChanged);
            this.tb_Street.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_Street_KeyPress);
            this.tb_Street.Leave += new System.EventHandler(this.tb_Street_Leave);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(342, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(42, 14);
            this.label4.TabIndex = 6;
            this.label4.Text = "Street:";
            // 
            // tb_City
            // 
            this.tb_City.Location = new System.Drawing.Point(427, 51);
            this.tb_City.MaxLength = 50;
            this.tb_City.Name = "tb_City";
            this.tb_City.Size = new System.Drawing.Size(185, 22);
            this.tb_City.TabIndex = 5;
            this.tb_City.TextChanged += new System.EventHandler(this.tb_City_TextChanged);
            this.tb_City.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_City_KeyPress);
            this.tb_City.Leave += new System.EventHandler(this.tb_City_Leave);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(342, 55);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(29, 14);
            this.label5.TabIndex = 8;
            this.label5.Text = "City:";
            // 
            // tb_ZipCode
            // 
            this.tb_ZipCode.Location = new System.Drawing.Point(559, 79);
            this.tb_ZipCode.MaxLength = 5;
            this.tb_ZipCode.Name = "tb_ZipCode";
            this.tb_ZipCode.Size = new System.Drawing.Size(53, 22);
            this.tb_ZipCode.TabIndex = 7;
            this.tb_ZipCode.TextChanged += new System.EventHandler(this.tb_ZipCode_TextChanged);
            this.tb_ZipCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_ZipCode_KeyPress);
            this.tb_ZipCode.KeyUp += new System.Windows.Forms.KeyEventHandler(this.textBox4_KeyUp);
            this.tb_ZipCode.Leave += new System.EventHandler(this.tb_ZipCode_Leave);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(503, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 14);
            this.label6.TabIndex = 10;
            this.label6.Text = "Zipcode:";
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(126, 24);
            this.textBox5.MaxLength = 10;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(105, 22);
            this.textBox5.TabIndex = 2;
            this.textBox5.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            this.textBox5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox5_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(13, 27);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(63, 14);
            this.label7.TabIndex = 12;
            this.label7.Text = "Vendor ID:";
            // 
            // btn_Submit
            // 
            this.btn_Submit.Location = new System.Drawing.Point(788, 189);
            this.btn_Submit.Name = "btn_Submit";
            this.btn_Submit.Size = new System.Drawing.Size(60, 28);
            this.btn_Submit.TabIndex = 11;
            this.btn_Submit.Text = "Submit";
            this.btn_Submit.UseVisualStyleBackColor = true;
            this.btn_Submit.Click += new System.EventHandler(this.button1_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(342, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 14);
            this.label8.TabIndex = 15;
            this.label8.Text = "State:";
            // 
            // tb_Email
            // 
            this.tb_Email.Location = new System.Drawing.Point(788, 23);
            this.tb_Email.MaxLength = 255;
            this.tb_Email.Multiline = true;
            this.tb_Email.Name = "tb_Email";
            this.tb_Email.Size = new System.Drawing.Size(187, 49);
            this.tb_Email.TabIndex = 9;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(670, 26);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(104, 14);
            this.label9.TabIndex = 17;
            this.label9.Text = "Email or Website:";
            // 
            // cb_NoEmail
            // 
            this.cb_NoEmail.AutoSize = true;
            this.cb_NoEmail.Location = new System.Drawing.Point(981, 23);
            this.cb_NoEmail.Name = "cb_NoEmail";
            this.cb_NoEmail.Size = new System.Drawing.Size(55, 18);
            this.cb_NoEmail.TabIndex = 10;
            this.cb_NoEmail.Text = "None";
            this.cb_NoEmail.UseVisualStyleBackColor = true;
            this.cb_NoEmail.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // tb_Telephone
            // 
            this.tb_Telephone.Location = new System.Drawing.Point(427, 107);
            this.tb_Telephone.MaxLength = 10;
            this.tb_Telephone.Name = "tb_Telephone";
            this.tb_Telephone.Size = new System.Drawing.Size(185, 22);
            this.tb_Telephone.TabIndex = 8;
            this.tb_Telephone.TextChanged += new System.EventHandler(this.tb_Telephone_TextChanged);
            this.tb_Telephone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_Telephone_KeyPress);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(342, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(68, 14);
            this.label10.TabIndex = 20;
            this.label10.Text = "Telephone:";
            // 
            // dgv_VIDRequest
            // 
            this.dgv_VIDRequest.AllowUserToAddRows = false;
            this.dgv_VIDRequest.AllowUserToDeleteRows = false;
            this.dgv_VIDRequest.AllowUserToResizeRows = false;
            this.dgv_VIDRequest.BackgroundColor = System.Drawing.Color.White;
            this.dgv_VIDRequest.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_VIDRequest.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.imgPrio,
            this.btnApprove,
            this.btnReject,
            this.cmbDeclineReason});
            this.dgv_VIDRequest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv_VIDRequest.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgv_VIDRequest.Location = new System.Drawing.Point(3, 3);
            this.dgv_VIDRequest.Name = "dgv_VIDRequest";
            this.dgv_VIDRequest.RowHeadersVisible = false;
            this.dgv_VIDRequest.Size = new System.Drawing.Size(649, 375);
            this.dgv_VIDRequest.TabIndex = 23;
            this.dgv_VIDRequest.DataSourceChanged += new System.EventHandler(this.dgv_VIDRequest_DataSourceChanged);
            this.dgv_VIDRequest.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_VIDRequest_CellClick);
            this.dgv_VIDRequest.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_VIDRequest_CellDoubleClick);
            this.dgv_VIDRequest.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_VIDRequest_CellMouseDown);
            this.dgv_VIDRequest.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgv_VIDRequest_DataBindingComplete);
            this.dgv_VIDRequest.EditingControlShowing += new System.Windows.Forms.DataGridViewEditingControlShowingEventHandler(this.dgv_VIDRequest_EditingControlShowing);
            this.dgv_VIDRequest.Sorted += new System.EventHandler(this.dgv_VIDRequest_Sorted);
            // 
            // imgPrio
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle10.NullValue")));
            dataGridViewCellStyle10.SelectionBackColor = System.Drawing.Color.White;
            this.imgPrio.DefaultCellStyle = dataGridViewCellStyle10;
            this.imgPrio.Frozen = true;
            this.imgPrio.HeaderText = "";
            this.imgPrio.Image = global::VendorIDRequestForm.Properties.Resources.dflag;
            this.imgPrio.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.imgPrio.MinimumWidth = 25;
            this.imgPrio.Name = "imgPrio";
            this.imgPrio.Visible = false;
            this.imgPrio.Width = 25;
            // 
            // btnApprove
            // 
            this.btnApprove.HeaderText = "Approve";
            this.btnApprove.MinimumWidth = 60;
            this.btnApprove.Name = "btnApprove";
            this.btnApprove.Text = "Approve";
            this.btnApprove.UseColumnTextForButtonValue = true;
            this.btnApprove.Visible = false;
            this.btnApprove.Width = 60;
            // 
            // btnReject
            // 
            this.btnReject.HeaderText = "Reject";
            this.btnReject.MinimumWidth = 60;
            this.btnReject.Name = "btnReject";
            this.btnReject.Text = "Reject";
            this.btnReject.UseColumnTextForButtonValue = true;
            this.btnReject.Visible = false;
            this.btnReject.Width = 60;
            // 
            // cmbDeclineReason
            // 
            this.cmbDeclineReason.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.cmbDeclineReason.DropDownWidth = 350;
            this.cmbDeclineReason.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbDeclineReason.HeaderText = "Reject Reason";
            this.cmbDeclineReason.Items.AddRange(new object[] {
            "Vendor ID already exists",
            "Vendor is blacklisted",
            "Incorrect Address",
            "USP Name Incorrect",
            "Incorrect Phone Number",
            "Incorrect Vendor Details",
            "Require Documentation"});
            this.cmbDeclineReason.Name = "cmbDeclineReason";
            this.cmbDeclineReason.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(680, 282);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(90, 18);
            this.label12.TabIndex = 24;
            this.label12.Text = "VID Requests";
            // 
            // cmb_State
            // 
            this.cmb_State.FormattingEnabled = true;
            this.cmb_State.Location = new System.Drawing.Point(427, 79);
            this.cmb_State.MaxLength = 2;
            this.cmb_State.Name = "cmb_State";
            this.cmb_State.Size = new System.Drawing.Size(55, 22);
            this.cmb_State.TabIndex = 25;
            this.cmb_State.TextChanged += new System.EventHandler(this.cmb_State_TextChanged);
            this.cmb_State.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmb_State_KeyPress);
            this.cmb_State.Leave += new System.EventHandler(this.cmbState_Leave);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.cmbPrio);
            this.groupBox1.Controls.Add(this.cmbSource);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.label26);
            this.groupBox1.Controls.Add(this.tb_Email);
            this.groupBox1.Controls.Add(this.cb_NoEmail);
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Controls.Add(this.tb_SourceOfInfo);
            this.groupBox1.Controls.Add(this.btn_Submit);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.tb_POBox);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.comboBox1);
            this.groupBox1.Controls.Add(this.tb_Dept);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.tb_OfficeName);
            this.groupBox1.Controls.Add(this.tb_BldgName);
            this.groupBox1.Controls.Add(this.btnShowAll);
            this.groupBox1.Controls.Add(this.tb_CoOrAtt);
            this.groupBox1.Controls.Add(this.textBox5);
            this.groupBox1.Controls.Add(this.tb_USPName);
            this.groupBox1.Controls.Add(this.cmb_State);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.tb_Street);
            this.groupBox1.Controls.Add(this.label15);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label11);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.tb_City);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label10);
            this.groupBox1.Controls.Add(this.tb_ZipCode);
            this.groupBox1.Controls.Add(this.tb_Telephone);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label25);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Location = new System.Drawing.Point(12, 41);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1095, 234);
            this.groupBox1.TabIndex = 27;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "USP Information";
            // 
            // cmbPrio
            // 
            this.cmbPrio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbPrio.FormattingEnabled = true;
            this.cmbPrio.Items.AddRange(new object[] {
            "Escalations",
            "Liens",
            "Credit Card Payment",
            "Others"});
            this.cmbPrio.Location = new System.Drawing.Point(788, 136);
            this.cmbPrio.Name = "cmbPrio";
            this.cmbPrio.Size = new System.Drawing.Size(187, 22);
            this.cmbPrio.TabIndex = 37;
            // 
            // cmbSource
            // 
            this.cmbSource.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSource.FormattingEnabled = true;
            this.cmbSource.Items.AddRange(new object[] {
            "Callout",
            "Invoice",
            "Research/Internet"});
            this.cmbSource.Location = new System.Drawing.Point(427, 135);
            this.cmbSource.Name = "cmbSource";
            this.cmbSource.Size = new System.Drawing.Size(185, 22);
            this.cmbSource.TabIndex = 36;
            this.cmbSource.SelectedIndexChanged += new System.EventHandler(this.cmbSource_SelectedIndexChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.Red;
            this.label26.Location = new System.Drawing.Point(615, 135);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(13, 14);
            this.label26.TabIndex = 12;
            this.label26.Text = "*";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.chkBox_Payments);
            this.panel1.Controls.Add(this.chkBox_Water);
            this.panel1.Controls.Add(this.chkBox_Electricity);
            this.panel1.Controls.Add(this.chkBox_Gas);
            this.panel1.Controls.Add(this.chkBox_Others);
            this.panel1.Location = new System.Drawing.Point(423, 164);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(213, 50);
            this.panel1.TabIndex = 34;
            // 
            // chkBox_Payments
            // 
            this.chkBox_Payments.AutoSize = true;
            this.chkBox_Payments.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBox_Payments.Location = new System.Drawing.Point(79, 26);
            this.chkBox_Payments.Name = "chkBox_Payments";
            this.chkBox_Payments.Size = new System.Drawing.Size(73, 17);
            this.chkBox_Payments.TabIndex = 32;
            this.chkBox_Payments.Text = "Payments";
            this.chkBox_Payments.UseVisualStyleBackColor = true;
            this.chkBox_Payments.CheckedChanged += new System.EventHandler(this.chkBox_Payments_CheckedChanged);
            // 
            // chkBox_Water
            // 
            this.chkBox_Water.AutoSize = true;
            this.chkBox_Water.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBox_Water.Location = new System.Drawing.Point(80, 4);
            this.chkBox_Water.Name = "chkBox_Water";
            this.chkBox_Water.Size = new System.Drawing.Size(56, 17);
            this.chkBox_Water.TabIndex = 30;
            this.chkBox_Water.Text = "Water";
            this.chkBox_Water.UseVisualStyleBackColor = true;
            this.chkBox_Water.CheckedChanged += new System.EventHandler(this.chkBox_Water_CheckedChanged);
            // 
            // chkBox_Electricity
            // 
            this.chkBox_Electricity.AutoSize = true;
            this.chkBox_Electricity.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBox_Electricity.Location = new System.Drawing.Point(7, 5);
            this.chkBox_Electricity.Name = "chkBox_Electricity";
            this.chkBox_Electricity.Size = new System.Drawing.Size(73, 17);
            this.chkBox_Electricity.TabIndex = 28;
            this.chkBox_Electricity.Text = "Electricity";
            this.chkBox_Electricity.UseVisualStyleBackColor = true;
            this.chkBox_Electricity.CheckedChanged += new System.EventHandler(this.chkBox_Electricity_CheckedChanged);
            // 
            // chkBox_Gas
            // 
            this.chkBox_Gas.AutoSize = true;
            this.chkBox_Gas.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBox_Gas.Location = new System.Drawing.Point(7, 26);
            this.chkBox_Gas.Name = "chkBox_Gas";
            this.chkBox_Gas.Size = new System.Drawing.Size(44, 17);
            this.chkBox_Gas.TabIndex = 29;
            this.chkBox_Gas.Text = "Gas";
            this.chkBox_Gas.UseVisualStyleBackColor = true;
            this.chkBox_Gas.CheckedChanged += new System.EventHandler(this.chkBox_Gas_CheckedChanged);
            // 
            // chkBox_Others
            // 
            this.chkBox_Others.AutoSize = true;
            this.chkBox_Others.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkBox_Others.Location = new System.Drawing.Point(146, 5);
            this.chkBox_Others.Name = "chkBox_Others";
            this.chkBox_Others.Size = new System.Drawing.Size(58, 17);
            this.chkBox_Others.TabIndex = 31;
            this.chkBox_Others.Text = "Others";
            this.chkBox_Others.UseVisualStyleBackColor = true;
            this.chkBox_Others.CheckedChanged += new System.EventHandler(this.chkBox_Others_CheckedChanged);
            // 
            // tb_SourceOfInfo
            // 
            this.tb_SourceOfInfo.BackColor = System.Drawing.Color.White;
            this.tb_SourceOfInfo.Location = new System.Drawing.Point(788, 78);
            this.tb_SourceOfInfo.MaxLength = 255;
            this.tb_SourceOfInfo.Multiline = true;
            this.tb_SourceOfInfo.Name = "tb_SourceOfInfo";
            this.tb_SourceOfInfo.ReadOnly = true;
            this.tb_SourceOfInfo.Size = new System.Drawing.Size(187, 51);
            this.tb_SourceOfInfo.TabIndex = 32;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.ForeColor = System.Drawing.Color.Red;
            this.label38.Location = new System.Drawing.Point(638, 167);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(13, 14);
            this.label38.TabIndex = 12;
            this.label38.Text = "*";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(670, 140);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(111, 14);
            this.label39.TabIndex = 0;
            this.label39.Text = "Priority request for:";
            // 
            // tb_POBox
            // 
            this.tb_POBox.Location = new System.Drawing.Point(126, 192);
            this.tb_POBox.Name = "tb_POBox";
            this.tb_POBox.Size = new System.Drawing.Size(181, 22);
            this.tb_POBox.TabIndex = 27;
            this.tb_POBox.TextChanged += new System.EventHandler(this.tb_POBox_TextChanged);
            this.tb_POBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.tb_POBox_KeyPress);
            this.tb_POBox.Leave += new System.EventHandler(this.tb_POBox_Leave);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(670, 80);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(32, 14);
            this.label37.TabIndex = 0;
            this.label37.Text = "Link:";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(338, 180);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(42, 14);
            this.label34.TabIndex = 33;
            this.label34.Text = "Utility:";
            // 
            // tb_Dept
            // 
            this.tb_Dept.Location = new System.Drawing.Point(126, 164);
            this.tb_Dept.Name = "tb_Dept";
            this.tb_Dept.Size = new System.Drawing.Size(181, 22);
            this.tb_Dept.TabIndex = 27;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(338, 133);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(74, 28);
            this.label33.TabIndex = 33;
            this.label33.Text = "Source of \r\ninformation:";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(851, 189);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(60, 28);
            this.btnClear.TabIndex = 11;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // tb_OfficeName
            // 
            this.tb_OfficeName.Location = new System.Drawing.Point(126, 136);
            this.tb_OfficeName.Name = "tb_OfficeName";
            this.tb_OfficeName.Size = new System.Drawing.Size(181, 22);
            this.tb_OfficeName.TabIndex = 27;
            // 
            // tb_BldgName
            // 
            this.tb_BldgName.Location = new System.Drawing.Point(126, 109);
            this.tb_BldgName.Name = "tb_BldgName";
            this.tb_BldgName.Size = new System.Drawing.Size(181, 22);
            this.tb_BldgName.TabIndex = 27;
            // 
            // btnShowAll
            // 
            this.btnShowAll.Location = new System.Drawing.Point(914, 189);
            this.btnShowAll.Name = "btnShowAll";
            this.btnShowAll.Size = new System.Drawing.Size(62, 28);
            this.btnShowAll.TabIndex = 11;
            this.btnShowAll.Text = "Show All";
            this.btnShowAll.UseVisualStyleBackColor = true;
            this.btnShowAll.Click += new System.EventHandler(this.btnShowAll_Click);
            // 
            // tb_CoOrAtt
            // 
            this.tb_CoOrAtt.Location = new System.Drawing.Point(126, 81);
            this.tb_CoOrAtt.Name = "tb_CoOrAtt";
            this.tb_CoOrAtt.Size = new System.Drawing.Size(181, 22);
            this.tb_CoOrAtt.TabIndex = 27;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(13, 195);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 14);
            this.label15.TabIndex = 6;
            this.label15.Text = "PO Box:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(13, 167);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 14);
            this.label14.TabIndex = 6;
            this.label14.Text = "Department:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(13, 139);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 14);
            this.label13.TabIndex = 6;
            this.label13.Text = "Office Name:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(13, 111);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 14);
            this.label11.TabIndex = 6;
            this.label11.Text = "Bldg. Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "C/O or Attention To:";
            // 
            // label23
            // 
            this.label23.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label23.AutoSize = true;
            this.label23.ForeColor = System.Drawing.Color.Red;
            this.label23.Location = new System.Drawing.Point(536, 82);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(13, 14);
            this.label23.TabIndex = 12;
            this.label23.Text = "*";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.ForeColor = System.Drawing.Color.Red;
            this.label24.Location = new System.Drawing.Point(485, 79);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(13, 14);
            this.label24.TabIndex = 12;
            this.label24.Text = "*";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.ForeColor = System.Drawing.Color.Red;
            this.label22.Location = new System.Drawing.Point(615, 79);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(13, 14);
            this.label22.TabIndex = 12;
            this.label22.Text = "*";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.Red;
            this.label21.Location = new System.Drawing.Point(615, 51);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(13, 14);
            this.label21.TabIndex = 12;
            this.label21.Text = "*";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.ForeColor = System.Drawing.Color.Red;
            this.label25.Location = new System.Drawing.Point(615, 108);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(13, 14);
            this.label25.TabIndex = 12;
            this.label25.Text = "*";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.ForeColor = System.Drawing.Color.Red;
            this.label20.Location = new System.Drawing.Point(615, 24);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(13, 14);
            this.label20.TabIndex = 12;
            this.label20.Text = "*";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.ForeColor = System.Drawing.Color.Red;
            this.label19.Location = new System.Drawing.Point(309, 52);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 14);
            this.label19.TabIndex = 12;
            this.label19.Text = "*";
            // 
            // label18
            // 
            this.label18.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label18.AutoSize = true;
            this.label18.ForeColor = System.Drawing.Color.Red;
            this.label18.Location = new System.Drawing.Point(233, 24);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(13, 14);
            this.label18.TabIndex = 12;
            this.label18.Text = "*";
            // 
            // dgDashboard
            // 
            this.dgDashboard.AllowUserToAddRows = false;
            this.dgDashboard.AllowUserToDeleteRows = false;
            this.dgDashboard.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgDashboard.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgDashboard.Location = new System.Drawing.Point(12, 168);
            this.dgDashboard.Name = "dgDashboard";
            this.dgDashboard.ReadOnly = true;
            this.dgDashboard.Size = new System.Drawing.Size(1331, 564);
            this.dgDashboard.TabIndex = 41;
            // 
            // dgv_VDR
            // 
            this.dgv_VDR.AllowUserToAddRows = false;
            this.dgv_VDR.AllowUserToDeleteRows = false;
            this.dgv_VDR.AllowUserToResizeRows = false;
            this.dgv_VDR.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_VDR.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv_VDR.BackgroundColor = System.Drawing.Color.White;
            this.dgv_VDR.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Sunken;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_VDR.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgv_VDR.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_VDR.Location = new System.Drawing.Point(12, 325);
            this.dgv_VDR.Name = "dgv_VDR";
            this.dgv_VDR.ReadOnly = true;
            this.dgv_VDR.RowHeadersVisible = false;
            this.dgv_VDR.Size = new System.Drawing.Size(663, 375);
            this.dgv_VDR.TabIndex = 28;
            this.dgv_VDR.DataSourceChanged += new System.EventHandler(this.dgv_VDR_DataSourceChanged);
            this.dgv_VDR.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_VDR_CellDoubleClick);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Calibri", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(9, 282);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(34, 18);
            this.label16.TabIndex = 29;
            this.label16.Text = "VDR";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(77, 286);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(414, 14);
            this.label17.TabIndex = 31;
            this.label17.Text = "Double click the inactive vendor ID to request for activation or search the vendo" +
    "r ID.";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tagToInvoiceToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(150, 26);
            // 
            // tagToInvoiceToolStripMenuItem
            // 
            this.tagToInvoiceToolStripMenuItem.Name = "tagToInvoiceToolStripMenuItem";
            this.tagToInvoiceToolStripMenuItem.Size = new System.Drawing.Size(149, 22);
            this.tagToInvoiceToolStripMenuItem.Text = "Tag to invoice";
            this.tagToInvoiceToolStripMenuItem.Click += new System.EventHandler(this.tagToInvoiceToolStripMenuItem_Click);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(797, 285);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(282, 14);
            this.label27.TabIndex = 31;
            this.label27.Text = "Right click to tag the pending VID request to the invoice.";
            // 
            // timer_CountDown
            // 
            this.timer_CountDown.Interval = 500;
            this.timer_CountDown.Tick += new System.EventHandler(this.timer_CountDown_tick);
            // 
            // tb_VDR
            // 
            this.tb_VDR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.tb_VDR.BackColor = System.Drawing.Color.White;
            this.tb_VDR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_VDR.Enabled = false;
            this.tb_VDR.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_VDR.ForeColor = System.Drawing.Color.Black;
            this.tb_VDR.Location = new System.Drawing.Point(631, 714);
            this.tb_VDR.Name = "tb_VDR";
            this.tb_VDR.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tb_VDR.Size = new System.Drawing.Size(35, 15);
            this.tb_VDR.TabIndex = 34;
            this.tb_VDR.TabStop = false;
            // 
            // tb_VIDR
            // 
            this.tb_VIDR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.tb_VIDR.BackColor = System.Drawing.Color.White;
            this.tb_VIDR.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.tb_VIDR.Enabled = false;
            this.tb_VIDR.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tb_VIDR.ForeColor = System.Drawing.Color.Black;
            this.tb_VIDR.Location = new System.Drawing.Point(1302, 713);
            this.tb_VIDR.Name = "tb_VIDR";
            this.tb_VIDR.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.tb_VIDR.Size = new System.Drawing.Size(35, 15);
            this.tb_VIDR.TabIndex = 34;
            this.tb_VIDR.TabStop = false;
            // 
            // label31
            // 
            this.label31.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(579, 715);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(41, 14);
            this.label31.TabIndex = 35;
            this.label31.Text = "Count:";
            // 
            // label32
            // 
            this.label32.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(1256, 715);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 14);
            this.label32.TabIndex = 35;
            this.label32.Text = "Count:";
            // 
            // grpSummary
            // 
            this.grpSummary.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSummary.Controls.Add(this.lbluTotal);
            this.grpSummary.Controls.Add(this.lblRejected);
            this.grpSummary.Controls.Add(this.lblApproved);
            this.grpSummary.Controls.Add(this.lblPending);
            this.grpSummary.Controls.Add(this.label40);
            this.grpSummary.Controls.Add(this.label30);
            this.grpSummary.Controls.Add(this.label29);
            this.grpSummary.Controls.Add(this.label28);
            this.grpSummary.Location = new System.Drawing.Point(1113, 41);
            this.grpSummary.Name = "grpSummary";
            this.grpSummary.Size = new System.Drawing.Size(231, 135);
            this.grpSummary.TabIndex = 37;
            this.grpSummary.TabStop = false;
            this.grpSummary.Text = "Summary";
            this.grpSummary.Visible = false;
            // 
            // lbluTotal
            // 
            this.lbluTotal.AutoSize = true;
            this.lbluTotal.Location = new System.Drawing.Point(122, 98);
            this.lbluTotal.Name = "lbluTotal";
            this.lbluTotal.Size = new System.Drawing.Size(11, 14);
            this.lbluTotal.TabIndex = 38;
            this.lbluTotal.Text = "-";
            // 
            // lblRejected
            // 
            this.lblRejected.AutoSize = true;
            this.lblRejected.Location = new System.Drawing.Point(99, 74);
            this.lblRejected.Name = "lblRejected";
            this.lblRejected.Size = new System.Drawing.Size(11, 14);
            this.lblRejected.TabIndex = 21;
            this.lblRejected.Text = "-";
            // 
            // lblApproved
            // 
            this.lblApproved.AutoSize = true;
            this.lblApproved.Location = new System.Drawing.Point(99, 51);
            this.lblApproved.Name = "lblApproved";
            this.lblApproved.Size = new System.Drawing.Size(11, 14);
            this.lblApproved.TabIndex = 21;
            this.lblApproved.Text = "-";
            // 
            // lblPending
            // 
            this.lblPending.AutoSize = true;
            this.lblPending.Location = new System.Drawing.Point(99, 30);
            this.lblPending.Name = "lblPending";
            this.lblPending.Size = new System.Drawing.Size(11, 14);
            this.lblPending.TabIndex = 21;
            this.lblPending.Text = "-";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(20, 98);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(96, 14);
            this.label40.TabIndex = 20;
            this.label40.Text = "Total processed:";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(20, 74);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(57, 14);
            this.label30.TabIndex = 20;
            this.label30.Text = "Rejected:";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(20, 51);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(61, 14);
            this.label29.TabIndex = 20;
            this.label29.Text = "Approved:";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(20, 30);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(54, 14);
            this.label28.TabIndex = 20;
            this.label28.Text = "Pending:";
            // 
            // grpSummary2
            // 
            this.grpSummary2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.grpSummary2.Controls.Add(this.lbloTotal);
            this.grpSummary2.Controls.Add(this.lbloRej);
            this.grpSummary2.Controls.Add(this.lbloApp);
            this.grpSummary2.Controls.Add(this.label41);
            this.grpSummary2.Controls.Add(this.label42);
            this.grpSummary2.Controls.Add(this.label43);
            this.grpSummary2.Location = new System.Drawing.Point(1113, 180);
            this.grpSummary2.Name = "grpSummary2";
            this.grpSummary2.Size = new System.Drawing.Size(231, 95);
            this.grpSummary2.TabIndex = 36;
            this.grpSummary2.TabStop = false;
            this.grpSummary2.Text = "Overall";
            this.grpSummary2.Visible = false;
            // 
            // lbloTotal
            // 
            this.lbloTotal.AutoSize = true;
            this.lbloTotal.Location = new System.Drawing.Point(125, 72);
            this.lbloTotal.Name = "lbloTotal";
            this.lbloTotal.Size = new System.Drawing.Size(11, 14);
            this.lbloTotal.TabIndex = 22;
            this.lbloTotal.Text = "-";
            // 
            // lbloRej
            // 
            this.lbloRej.AutoSize = true;
            this.lbloRej.Location = new System.Drawing.Point(99, 49);
            this.lbloRej.Name = "lbloRej";
            this.lbloRej.Size = new System.Drawing.Size(11, 14);
            this.lbloRej.TabIndex = 21;
            this.lbloRej.Text = "-";
            // 
            // lbloApp
            // 
            this.lbloApp.AutoSize = true;
            this.lbloApp.Location = new System.Drawing.Point(99, 26);
            this.lbloApp.Name = "lbloApp";
            this.lbloApp.Size = new System.Drawing.Size(11, 14);
            this.lbloApp.TabIndex = 21;
            this.lbloApp.Text = "-";
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(20, 49);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(57, 14);
            this.label41.TabIndex = 20;
            this.label41.Text = "Rejected:";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(20, 26);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(61, 14);
            this.label42.TabIndex = 20;
            this.label42.Text = "Approved:";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(20, 72);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(96, 14);
            this.label43.TabIndex = 20;
            this.label43.Text = "Total processed:";
            // 
            // tbMain
            // 
            this.tbMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tbMain.Controls.Add(this.tbNewPending);
            this.tbMain.Controls.Add(this.tbRejects);
            this.tbMain.Controls.Add(this.tbAll);
            this.tbMain.Location = new System.Drawing.Point(681, 299);
            this.tbMain.Name = "tbMain";
            this.tbMain.SelectedIndex = 0;
            this.tbMain.Size = new System.Drawing.Size(663, 408);
            this.tbMain.TabIndex = 38;
            // 
            // tbNewPending
            // 
            this.tbNewPending.Controls.Add(this.dgv_VIDRequest);
            this.tbNewPending.Location = new System.Drawing.Point(4, 23);
            this.tbNewPending.Name = "tbNewPending";
            this.tbNewPending.Padding = new System.Windows.Forms.Padding(3);
            this.tbNewPending.Size = new System.Drawing.Size(655, 381);
            this.tbNewPending.TabIndex = 0;
            this.tbNewPending.Text = "New Requests";
            this.tbNewPending.UseVisualStyleBackColor = true;
            // 
            // tbRejects
            // 
            this.tbRejects.Controls.Add(this.dgRejects);
            this.tbRejects.Location = new System.Drawing.Point(4, 23);
            this.tbRejects.Name = "tbRejects";
            this.tbRejects.Padding = new System.Windows.Forms.Padding(3);
            this.tbRejects.Size = new System.Drawing.Size(655, 381);
            this.tbRejects.TabIndex = 1;
            this.tbRejects.Text = "Rejected Requests";
            this.tbRejects.UseVisualStyleBackColor = true;
            // 
            // dgRejects
            // 
            this.dgRejects.AllowUserToAddRows = false;
            this.dgRejects.AllowUserToDeleteRows = false;
            this.dgRejects.AllowUserToResizeRows = false;
            this.dgRejects.BackgroundColor = System.Drawing.Color.White;
            this.dgRejects.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgRejects.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.imgPrio_rej,
            this.btnApprove_rej,
            this.btnReject_rej,
            this.cmbDeclineReason_rej});
            this.dgRejects.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgRejects.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgRejects.Location = new System.Drawing.Point(3, 3);
            this.dgRejects.Name = "dgRejects";
            this.dgRejects.RowHeadersVisible = false;
            this.dgRejects.Size = new System.Drawing.Size(649, 375);
            this.dgRejects.TabIndex = 24;
            this.dgRejects.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgRejects_CellClick);
            this.dgRejects.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgRejects_CellDoubleClick);
            this.dgRejects.DataBindingComplete += new System.Windows.Forms.DataGridViewBindingCompleteEventHandler(this.dgRejects_DataBindingComplete);
            // 
            // imgPrio_rej
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.NullValue = ((object)(resources.GetObject("dataGridViewCellStyle12.NullValue")));
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.Color.White;
            this.imgPrio_rej.DefaultCellStyle = dataGridViewCellStyle12;
            this.imgPrio_rej.Frozen = true;
            this.imgPrio_rej.HeaderText = "";
            this.imgPrio_rej.Image = global::VendorIDRequestForm.Properties.Resources.dflag;
            this.imgPrio_rej.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Zoom;
            this.imgPrio_rej.MinimumWidth = 25;
            this.imgPrio_rej.Name = "imgPrio_rej";
            this.imgPrio_rej.Width = 25;
            // 
            // btnApprove_rej
            // 
            this.btnApprove_rej.HeaderText = "Approve";
            this.btnApprove_rej.MinimumWidth = 60;
            this.btnApprove_rej.Name = "btnApprove_rej";
            this.btnApprove_rej.Text = "Approve";
            this.btnApprove_rej.UseColumnTextForButtonValue = true;
            this.btnApprove_rej.Width = 60;
            // 
            // btnReject_rej
            // 
            this.btnReject_rej.HeaderText = "Reject";
            this.btnReject_rej.MinimumWidth = 60;
            this.btnReject_rej.Name = "btnReject_rej";
            this.btnReject_rej.Text = "Reject";
            this.btnReject_rej.UseColumnTextForButtonValue = true;
            this.btnReject_rej.Width = 60;
            // 
            // cmbDeclineReason_rej
            // 
            this.cmbDeclineReason_rej.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.Nothing;
            this.cmbDeclineReason_rej.DropDownWidth = 350;
            this.cmbDeclineReason_rej.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbDeclineReason_rej.HeaderText = "Reject Reason";
            this.cmbDeclineReason_rej.Items.AddRange(new object[] {
            "Vendor ID already exists",
            "Vendor is blacklisted",
            "Incorrect Address",
            "USP Name Incorrect",
            "Incorrect Phone Number",
            "Incorrect Vendor Details",
            "Require Documentation"});
            this.cmbDeclineReason_rej.Name = "cmbDeclineReason_rej";
            // 
            // tbAll
            // 
            this.tbAll.Controls.Add(this.dgAll);
            this.tbAll.Location = new System.Drawing.Point(4, 23);
            this.tbAll.Name = "tbAll";
            this.tbAll.Padding = new System.Windows.Forms.Padding(3);
            this.tbAll.Size = new System.Drawing.Size(655, 381);
            this.tbAll.TabIndex = 2;
            this.tbAll.Text = "All";
            this.tbAll.UseVisualStyleBackColor = true;
            // 
            // dgAll
            // 
            this.dgAll.AllowUserToAddRows = false;
            this.dgAll.AllowUserToDeleteRows = false;
            this.dgAll.AllowUserToResizeRows = false;
            this.dgAll.BackgroundColor = System.Drawing.Color.White;
            this.dgAll.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgAll.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgAll.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgAll.Location = new System.Drawing.Point(3, 3);
            this.dgAll.Name = "dgAll";
            this.dgAll.ReadOnly = true;
            this.dgAll.RowHeadersVisible = false;
            this.dgAll.Size = new System.Drawing.Size(649, 375);
            this.dgAll.TabIndex = 25;
            // 
            // btnMain
            // 
            this.btnMain.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnMain.Location = new System.Drawing.Point(13, 11);
            this.btnMain.Name = "btnMain";
            this.btnMain.Size = new System.Drawing.Size(87, 23);
            this.btnMain.TabIndex = 39;
            this.btnMain.Text = "VID Request";
            this.btnMain.UseVisualStyleBackColor = true;
            this.btnMain.Click += new System.EventHandler(this.btnMain_Click);
            // 
            // btnReports
            // 
            this.btnReports.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnReports.Location = new System.Drawing.Point(99, 11);
            this.btnReports.Name = "btnReports";
            this.btnReports.Size = new System.Drawing.Size(87, 23);
            this.btnReports.TabIndex = 39;
            this.btnReports.Text = "Reports";
            this.btnReports.UseVisualStyleBackColor = true;
            this.btnReports.Click += new System.EventHandler(this.btnReports_Click);
            // 
            // grpReports
            // 
            this.grpReports.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpReports.Controls.Add(this.label47);
            this.grpReports.Controls.Add(this.lnkFile);
            this.grpReports.Controls.Add(this.btnExport);
            this.grpReports.Controls.Add(this.btnShow);
            this.grpReports.Controls.Add(this.cmbType);
            this.grpReports.Controls.Add(this.cmbStatus);
            this.grpReports.Controls.Add(this.cmbApprover);
            this.grpReports.Controls.Add(this.label46);
            this.grpReports.Controls.Add(this.label45);
            this.grpReports.Controls.Add(this.label44);
            this.grpReports.Controls.Add(this.groupBox2);
            this.grpReports.Location = new System.Drawing.Point(12, 44);
            this.grpReports.Name = "grpReports";
            this.grpReports.Size = new System.Drawing.Size(1332, 121);
            this.grpReports.TabIndex = 40;
            this.grpReports.TabStop = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(27, 97);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(59, 14);
            this.label47.TabIndex = 7;
            this.label47.Text = "Open file:";
            this.label47.Visible = false;
            // 
            // lnkFile
            // 
            this.lnkFile.AutoSize = true;
            this.lnkFile.Location = new System.Drawing.Point(96, 97);
            this.lnkFile.Name = "lnkFile";
            this.lnkFile.Size = new System.Drawing.Size(0, 14);
            this.lnkFile.TabIndex = 6;
            this.lnkFile.Visible = false;
            this.lnkFile.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkFile_LinkClicked);
            // 
            // btnExport
            // 
            this.btnExport.Location = new System.Drawing.Point(1240, 38);
            this.btnExport.Name = "btnExport";
            this.btnExport.Size = new System.Drawing.Size(75, 30);
            this.btnExport.TabIndex = 5;
            this.btnExport.Text = "Export";
            this.btnExport.UseVisualStyleBackColor = true;
            this.btnExport.Click += new System.EventHandler(this.btnExport_Click);
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(1159, 38);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 30);
            this.btnShow.TabIndex = 5;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "All",
            "Create New",
            "VDR Activation",
            "VMS Activation"});
            this.cmbType.Location = new System.Drawing.Point(904, 48);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(121, 22);
            this.cmbType.TabIndex = 4;
            // 
            // cmbStatus
            // 
            this.cmbStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Items.AddRange(new object[] {
            "All",
            "Pending",
            "Approved",
            "Rejected"});
            this.cmbStatus.Location = new System.Drawing.Point(711, 48);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(121, 22);
            this.cmbStatus.TabIndex = 4;
            // 
            // cmbApprover
            // 
            this.cmbApprover.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbApprover.FormattingEnabled = true;
            this.cmbApprover.Items.AddRange(new object[] {
            "All",
            "TL",
            "Compliance",
            "AVS"});
            this.cmbApprover.Location = new System.Drawing.Point(519, 47);
            this.cmbApprover.Name = "cmbApprover";
            this.cmbApprover.Size = new System.Drawing.Size(121, 22);
            this.cmbApprover.TabIndex = 4;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(861, 51);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(34, 14);
            this.label46.TabIndex = 3;
            this.label46.Text = "Type:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(661, 53);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(44, 14);
            this.label45.TabIndex = 3;
            this.label45.Text = "Status:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(456, 52);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(58, 14);
            this.label44.TabIndex = 3;
            this.label44.Text = "Approver:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.chkAll);
            this.groupBox2.Controls.Add(this.dtpTo);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.dtpFrom);
            this.groupBox2.Controls.Add(this.label36);
            this.groupBox2.Location = new System.Drawing.Point(27, 30);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(394, 53);
            this.groupBox2.TabIndex = 2;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Date processed";
            // 
            // chkAll
            // 
            this.chkAll.AutoSize = true;
            this.chkAll.Location = new System.Drawing.Point(316, 21);
            this.chkAll.Name = "chkAll";
            this.chkAll.Size = new System.Drawing.Size(41, 18);
            this.chkAll.TabIndex = 2;
            this.chkAll.Text = "All";
            this.chkAll.UseVisualStyleBackColor = true;
            this.chkAll.CheckStateChanged += new System.EventHandler(this.chkAll_CheckStateChanged);
            // 
            // dtpTo
            // 
            this.dtpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpTo.Location = new System.Drawing.Point(204, 20);
            this.dtpTo.Name = "dtpTo";
            this.dtpTo.Size = new System.Drawing.Size(89, 22);
            this.dtpTo.TabIndex = 1;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(31, 24);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(37, 14);
            this.label35.TabIndex = 0;
            this.label35.Text = "From:";
            // 
            // dtpFrom
            // 
            this.dtpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFrom.Location = new System.Drawing.Point(72, 21);
            this.dtpFrom.Name = "dtpFrom";
            this.dtpFrom.Size = new System.Drawing.Size(92, 22);
            this.dtpFrom.TabIndex = 1;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(179, 23);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(22, 14);
            this.label36.TabIndex = 0;
            this.label36.Text = "To:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1356, 744);
            this.Controls.Add(this.grpReports);
            this.Controls.Add(this.dgDashboard);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.tb_VIDR);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.tb_VDR);
            this.Controls.Add(this.btnMain);
            this.Controls.Add(this.btnReports);
            this.Controls.Add(this.tbMain);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.dgv_VDR);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.grpSummary);
            this.Controls.Add(this.grpSummary2);
            this.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Vendor ID Request Software v3.0";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            ((System.ComponentModel.ISupportInitialize)(this.dgv_VIDRequest)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgDashboard)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_VDR)).EndInit();
            this.contextMenuStrip1.ResumeLayout(false);
            this.grpSummary.ResumeLayout(false);
            this.grpSummary.PerformLayout();
            this.grpSummary2.ResumeLayout(false);
            this.grpSummary2.PerformLayout();
            this.tbMain.ResumeLayout(false);
            this.tbNewPending.ResumeLayout(false);
            this.tbRejects.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgRejects)).EndInit();
            this.tbAll.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgAll)).EndInit();
            this.grpReports.ResumeLayout(false);
            this.grpReports.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.TextBox tb_USPName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox tb_Street;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tb_City;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox tb_ZipCode;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button btn_Submit;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_Email;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox cb_NoEmail;
        private System.Windows.Forms.TextBox tb_Telephone;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.DataGridView dgv_VIDRequest;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.ComboBox cmb_State;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox tb_POBox;
        private System.Windows.Forms.TextBox tb_Dept;
        private System.Windows.Forms.TextBox tb_OfficeName;
        private System.Windows.Forms.TextBox tb_BldgName;
        private System.Windows.Forms.TextBox tb_CoOrAtt;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView dgv_VDR;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem tagToInvoiceToolStripMenuItem;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Timer timer_CountDown;
        private System.Windows.Forms.TextBox tb_VDR;
        private System.Windows.Forms.TextBox tb_VIDR;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox chkBox_Water;
        private System.Windows.Forms.CheckBox chkBox_Gas;
        private System.Windows.Forms.CheckBox chkBox_Electricity;
        private System.Windows.Forms.CheckBox chkBox_Others;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.TextBox tb_SourceOfInfo;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ComboBox cmbSource;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.CheckBox chkBox_Payments;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox cmbPrio;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.GroupBox grpSummary;
        private System.Windows.Forms.Label lbluTotal;
        private System.Windows.Forms.Label lblRejected;
        private System.Windows.Forms.Label lblApproved;
        private System.Windows.Forms.Label lblPending;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.GroupBox grpSummary2;
        private System.Windows.Forms.Label lbloTotal;
        private System.Windows.Forms.Label lbloRej;
        private System.Windows.Forms.Label lbloApp;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Button btnShowAll;
        private System.Windows.Forms.DataGridViewImageColumn imgPrio;
        private System.Windows.Forms.DataGridViewButtonColumn btnApprove;
        private System.Windows.Forms.DataGridViewButtonColumn btnReject;
        private System.Windows.Forms.DataGridViewComboBoxColumn cmbDeclineReason;
        private System.Windows.Forms.TabControl tbMain;
        private System.Windows.Forms.TabPage tbNewPending;
        private System.Windows.Forms.TabPage tbRejects;
        private System.Windows.Forms.DataGridView dgRejects;
        private System.Windows.Forms.DataGridViewImageColumn imgPrio_rej;
        private System.Windows.Forms.DataGridViewButtonColumn btnApprove_rej;
        private System.Windows.Forms.DataGridViewButtonColumn btnReject_rej;
        private System.Windows.Forms.DataGridViewComboBoxColumn cmbDeclineReason_rej;
        private System.Windows.Forms.Button btnMain;
        private System.Windows.Forms.TabPage tbAll;
        private System.Windows.Forms.DataGridView dgAll;
        private System.Windows.Forms.Button btnReports;
        private System.Windows.Forms.GroupBox grpReports;
        private System.Windows.Forms.DateTimePicker dtpFrom;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DateTimePicker dtpTo;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.CheckBox chkAll;
        private System.Windows.Forms.ComboBox cmbApprover;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Button btnShow;
        private System.Windows.Forms.DataGridView dgDashboard;
        private System.Windows.Forms.Button btnExport;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.LinkLabel lnkFile;
    }
}

