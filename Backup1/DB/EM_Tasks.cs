﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class EM_Tasks
    {
        public int EmployeePrimaryId;
        public string property_code;
        public string client_code;
        public string usp_name;
        public DateTime DueDate;
        public DateTime ServiceFrom;
        public DateTime ServiceTo;
        public string Amount;
        public decimal AmountAfterDue;
        public string AccountNumber;
        public float Duration;
        public int Complete;
        public DateTime LastDateModified;
        public string PendingReasons;
        public string Invoice;
        public string vendor_group;
        public string VendorId;
        public string vendor_address;
        public string final_bill;
        public string InvoiceFolderName;
        public string work_item;
        public DateTime date_bulkuploaded;
        public string userid;
        public DateTime invoice_date;
        public bool isActive;
        public decimal CurrentCharge;
        public decimal PreviousBalance;
        public decimal PaymentReceived;
        public decimal LateFee;
        public decimal DisconnectionFee;
        public string SpecialInstruction;
        public decimal AmountBeforeDue;
        public string remarks;
        public int id;
        public string HouseNoEntered;
        public string StreetEntered;
        public string StateEntered;
        public string CityEntered;
        public string ZipcodeEntered;
        public string FileAttachment;
        public bool isOutOfREO;
        public DateTime DisconnectionDate;
        public bool isVendorAssigned;
        public DateTime dateVendorAssigned;
        public string VendorAssignedStatus;
        public string AuditResult;
        public string AuditedBy;
        public DateTime AuditDate;
        public string HasTransferAmount;
        public bool isRPM;
        public bool isASFI;
        public bool isLatestData;
        public string lockedto;
        public int InvoiceCount;
        public bool isAddressedToASFI;
        public string BillTo;
        public string ParentAccount;
        public bool isLienValid;
        public string AttachmentFile;
        public string full_address;
        public string property_status;
        public decimal PreviousAmtPaid;
        public string DupWIReview;
        public decimal Deposit;
        public string Age;
        public decimal ActAmount;
    }
}
