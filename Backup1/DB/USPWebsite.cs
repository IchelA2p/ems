﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class USPWebsite
    {
        public int uspWebsiteID;
        public string uspName;
        public string website;
        public string username;
        public string password;
        public DateTime dateUpdated;
        public string updatedBy;
        public string oldUsername;
        public string oldPassword;
        public bool isWorking;
    }
}
