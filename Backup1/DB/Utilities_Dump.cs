﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class Utilities_Dump
    {
        public string property_code;
        public string elec_vendor_code;
        public string elec_vendor_name;
        public string elec_accntno;
        public string gas_vendor_code;
        public string gas_vendor_name;
        public string gas_accntno;
        public string water_vendor_code;
        public string water_vendor_name;
        public string water_accntno;
    }
}
