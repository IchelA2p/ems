﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class RESI_WI
    {
        public string property_code;
        public string Gas_WI;
        public string Elec_WI;
        public string Water_WI;
        public string Gas_Price;
        public string Water_Price;
        public string Elec_Price;
        public DateTime Elec_WI_Date;
        public DateTime Water_WI_Date;
        public DateTime Gas_WI_Date;
    }
}
