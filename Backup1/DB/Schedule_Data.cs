﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class Schedule_Data
    {
        public uint ScheduleOfEmployeeId;
        public string userId;
        public DateTime SchedIn;
        public DateTime SchedOut;
        public DateTime Date;
        public DateTime Break1Start;
        public DateTime Break1End;
        public DateTime Break2Start;
        public DateTime Break2End;
        public DateTime LunchStart;
        public DateTime LunchEnd;
        public bool RestDay;
        public bool PTO;
        public bool Absent;
    }
}
