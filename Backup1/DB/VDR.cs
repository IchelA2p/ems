﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class VDR
    {
        public string Vendor_ID;
        public string Vendor_Name;
        public string VDR_Vendor_Name;
        public string VMS_Vendor_Name;
        public string Address;        
        public string City;
        public string State;
        public string Zipcode;
        public string Type;
        public int vdr_status;
        public int vms_status;
    }
}
