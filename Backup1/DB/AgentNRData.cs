﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class AgentNRData
    {
        public int AgentNRId;
        public string userId;
        public DateTime NotReadyStartdt;
        public DateTime NotReadyEnddt;
        public string ReasonId;
        public DateTime workSched;
        public int ValidationId;
    }
}
