﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class USP_Contact
    {
        public string Name;
        public string Address;
        public string PhoneNumber;
        public string Type;
        public string VendorId;
        public string City;
        public string State;
        public string Zipcode;
        public string Status;
    }
}
