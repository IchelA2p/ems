﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class RSCApproval
    {
        public int rscapproval_id;
        public string invoice;
        public DateTime date_created;
        public string created_by;
        public DateTime date_modified;
        public string modified_by;
        public string status;
        public string notes;
        public string property_code;
        public string full_address;
        public string vendor_code;
        public string vendor_name;
        public double amount;
        public string vendor_type;
        public string property_status;
        public string invoice_link;
        public string asset_manager;
    }
}
