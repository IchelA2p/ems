﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class RRProperties
    {
        public string property_id;
        public string street_number;
        public string street_name;
        public string city;
        public string zip;
        public string status;
        public DateTime status_date;
    }
}
