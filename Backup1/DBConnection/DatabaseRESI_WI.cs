﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseRESI_WI : DatabaseConnector, IDatabase<DB.RESI_WI>
    {
        string Elec_WI_Date = "NULL";
        string Gas_WI_Date = "NULL";
        string Water_WI_Date = "NULL";

        public List<DB.RESI_WI> GetAllData()
        {
            DatabaseSelector selector = new SelectorRESI_WI();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_RESI_WI (nolock)");

            base.ExecuteSelect(selector);

            return ((SelectorRESI_WI)selector).GetResults();
        }

        public uint WriteData(DB.RESI_WI data)
        {
            if (data.Elec_WI_Date != DateTime.MinValue)
            {
                Elec_WI_Date = string.Format("'{0}'", data.Elec_WI_Date.ToString("yyyy-MM-dd HH:mm:ss"));   
            }

            if (data.Gas_WI_Date != DateTime.MinValue)
            {
                Gas_WI_Date = string.Format("'{0}'", data.Gas_WI_Date.ToString("yyyy-MM-dd HH:mm:ss"));
            }

            if (data.Water_WI_Date != DateTime.MinValue)
            {
                Water_WI_Date = string.Format("'{0}'", data.Water_WI_Date.ToString("yyyy-MM-dd HH:mm:ss"));
            }

            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_RESI_WI(
                                                property_code,
                                                Elec_WI_Date,
                                                Gas_WI_Date,
                                                Water_WI_Date,
                                                Elec_WI,
                                                Elec_Price,
                                                Gas_WI,
                                                Gas_Price,
                                                Water_WI,
                                                Water_Price)
                                            VALUES('{0}',{1},{2},{3},'{4}','{5}','{6}','{7}','{8}','{9}')",
                                                  data.property_code, //0
                                                  Elec_WI_Date, //1
                                                  Gas_WI_Date, //2
                                                  Water_WI_Date, //3
                                                  data.Elec_WI, //4
                                                  data.Elec_Price,  //5
                                                  data.Gas_WI,  //6
                                                  data.Gas_Price,   //7
                                                  data.Water_WI,    //8
                                                  data.Water_Price); //9

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.RESI_WI data)
        {
            if (data.Elec_WI_Date != DateTime.MinValue)
            {
                Elec_WI_Date = string.Format("'{0}'", data.Elec_WI_Date.ToString("yyyy-MM-dd HH:mm:ss"));
            }

            if (data.Gas_WI_Date != DateTime.MinValue)
            {
                Gas_WI_Date = string.Format("'{0}'", data.Gas_WI_Date.ToString("yyyy-MM-dd HH:mm:ss"));
            }

            if (data.Water_WI_Date != DateTime.MinValue)
            {
                Water_WI_Date = string.Format("'{0}'", data.Water_WI_Date.ToString("yyyy-MM-dd HH:mm:ss"));
            }

            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_RESI_WI
                                           SET
                                                Elec_WI = '{0}',
                                                Elec_Price = '{1}',
                                                Elec_WI_Date = {2},
                                                Gas_WI = '{3}',
                                                Gas_Price = '{4}',
                                                Gas_WI_Date = {5},
                                                Water_WI = '{6}',
                                                Water_Price = '{7}',
                                                Water_WI_Date = {8}
                                           WHERE
                                                property_code = '{9}'",

                                            data.Elec_WI, //0
                                            data.Elec_Price, //1
                                            Elec_WI_Date, //2
                                            data.Gas_WI, //3
                                            data.Gas_Price, //4
                                            Gas_WI_Date, //5
                                            data.Water_WI,  //6
                                            data.Water_Price,   //7
                                            Water_WI_Date, //8
                                            
                                            data.property_code); //9

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.RESI_WI GetData(uint property_code)
        {
            DatabaseSelector selector = new SelectorRESI_WI();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_RESI_WI (nolock) WHERE property_code={0}", property_code));
            
            base.ExecuteSelect(selector);

            return ((SelectorRESI_WI)selector).GetResult();
        }

        public DB.RESI_WI GetDataProcessor(string property_code)
        {
            DatabaseSelector selector = new SelectorRESI_WI();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_RESI_WI (nolock) WHERE property_code = '{0}'", property_code));

            base.ExecuteSelect(selector);

            return ((SelectorRESI_WI)selector).GetResult();
        }

        public void TruncateTable()
        {
            string query = @"TRUNCATE TABLE [MIS_ALTI].[dbo].[tbl_RESI_WI]";

            base.ExecuteNonQuery(query);
        }

    }
}
