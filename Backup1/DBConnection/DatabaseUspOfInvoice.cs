﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseUspOfInvoice : DatabaseConnector, IDatabase<DB.UspOfInvoice>
    {
        public List<DB.UspOfInvoice> GetAllData()
        {
            DatabaseSelector selector = new SelectorUspOfInvoice();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_UspOfInvoice]");

            base.ExecuteSelect(selector);

            return ((SelectorUspOfInvoice)selector).GetResults();
        }

        public uint WriteData(DB.UspOfInvoice data)
        {
            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_UspOfInvoice](
                                                invoice_name,
                                                usp_name,
                                                processed)
                                            VALUES('{0}','{1}','{2}')",
                                                  data.invoice_name, //0
                                                  data.usp_name,
                                                  data.processed); //1

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.UspOfInvoice data)
        {
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_EM_UspOfInvoice]
                                           SET
                                                invoice_name = '{0}',
                                                processed = '{1}'
                                           WHERE
                                                usp_name = '{2}'",

                                            data.invoice_name, //0
                                            data.usp_name,
                                            data.processed); //1

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.UspOfInvoice GetData(uint usp_name)
        {
            DatabaseSelector selector = new SelectorUspOfInvoice();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_UspOfInvoice] WHERE usp_name={0}", usp_name));
            
            base.ExecuteSelect(selector);

            return ((SelectorUspOfInvoice)selector).GetResult();
        }

        public List<DB.UspOfInvoice> GetDataProcessor(string usp_name)
        {
            DatabaseSelector selector = new SelectorUspOfInvoice();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_UspOfInvoice] WHERE usp_name='{0}'", usp_name));

            base.ExecuteSelect(selector);

            return ((SelectorUspOfInvoice)selector).GetResults();
        }
    }
}
