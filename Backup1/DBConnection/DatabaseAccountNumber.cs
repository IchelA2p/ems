﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseAccountNumber : DatabaseConnector, IDatabase<DB.AccountNumber>
    {
        public List<DB.AccountNumber> GetAllData()
        {
            DatabaseSelector selector = new SelectorAccountNumber();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_AccountNumber");

            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResults();
        }

        public uint WriteData(DB.AccountNumber data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_EM_AccountNumber(
                                                property_code,
                                                account_number)
                                            VALUES('{0}','{1}')",
                                                  data.property_code, //0
                                                  data.account_number); //1

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.AccountNumber data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_EM_AccountNumber
                                           SET
                                                account_number = '{0}'
                                           WHERE
                                                property_code = '{1}'",

                                            data.account_number, //0
                                            data.property_code); //1

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.AccountNumber GetData(uint account_number)
        {
            DatabaseSelector selector = new SelectorAccountNumber();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.view_EM_ProcessedAccountNumbers with(index(account_number)) WHERE account_number={0}", account_number));
            
            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResult();
        }

        public List<DB.AccountNumber> GetDataProcessor(string account_number, string utility)
        {
            DatabaseSelector selector = new SelectorAccountNumber();
            //selector.SetQuery(String.Format(@"SELECT DISTINCT TOP 1 property_code, account_number, [Source] FROM MIS_Alti.dbo.view_EM_ProcessedAccountNumbers with(index(account_number)) WHERE account_number = SUBSTRING('{0}', PATINDEX('%[^0]%', '{0}'), LEN('{0}')) and vendor_group = '{1}' order by [Source]", account_number, utility));

            selector.SetQuery(String.Format(@"SELECT DISTINCT TOP 1 tbl_prop.property_code, tbl_src.account_number, tbl_src.vendor_group, tbl_src.[Source] FROM
                                            (
                                            SELECT RTRIM(property_code) AS property_code, 
                                            AccountNumber AS account_number, 
                                            vendor_group, 
                                            'EMS' AS Source 
                                            FROM  dbo.tbl_ADHOC_EM_Tasks
                                            WHERE SUBSTRING(AccountNumber, PATINDEX('%[^0]%', AccountNumber), LEN(AccountNumber)) = SUBSTRING('{0}', PATINDEX('%[^0]%', '{0}'), LEN('{0}')) 
                                            and (property_code <> '') AND (AccountNumber <> '') 
                                            and (property_code <> 'NULL')
                                            and vendor_group = '{1}'
                                            UNION ALL
                                            SELECT PROPERTY_CODE, AccountNumber, Utility, [Source]  FROM vw_UtilitiesDataDump
                                            WHERE SUBSTRING(AccountNumber, PATINDEX('%[^0]%', AccountNumber), LEN(AccountNumber)) = SUBSTRING('{0}', PATINDEX('%[^0]%', '{0}'), LEN('{0}'))
                                            and Utility = case when '{1}' in ('T', 'D', 'S') then 'W' else '{1}' end
                                            ) tbl_src
                                            INNER JOIN tbl_REO_Properties tbl_m
                                            on tbl_src.property_code = tbl_m.property_code
                                            OUTER APPLY
                                              (SELECT     TOP 1 x.property_code
                                                FROM          tbl_REO_Properties x
                                                WHERE      x.full_address = tbl_m.full_address
                                                ORDER BY x.client_hierarchy) tbl_prop", account_number, utility));

            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResults();
        }

        public DB.AccountNumber GetDataProcessor1(string account_number, string property_code)
        {
            DatabaseSelector selector = new SelectorAccountNumber();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.view_EM_ProcessedAccountNumbers with(index(property_code)) 
                                                WHERE account_number = '{0}' 
                                                AND property_code = '{1}'",
                                                account_number,
                                                property_code));

            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResult();
        }

        public DB.AccountNumber GetAccountNumber(string property_code)
        {
            DatabaseSelector selector = new SelectorAccountNumber();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.view_EM_ProcessedAccountNumbers with(index(property_code)) 
                                                WHERE property_code = '{0}'",
                                                property_code));

            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResult();
        }

        public List<DB.AccountNumber> GetAccountNumbers(string property_code, string vendor_group)
        {
            DatabaseSelector selector = new SelectorAccountNumber();
//            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.view_EM_ProcessedAccountNumbers with(index(property_code)) 
//                                                WHERE property_code = '{0}' and vendor_group = '{1}'",
//                                                property_code,
//                                                vendor_group));


            selector.SetQuery(String.Format(@"SELECT     TOP (100) PERCENT tbl_prop.property_code, account_number, vendor_group, [Source]
                                                FROM         (SELECT DISTINCT 
                                                                                              RTRIM(property_code) AS property_code, RTRIM(AccountNumber) AS account_number, vendor_group, 
                                                                                              'EMS' AS Source
                                                                       FROM          dbo.tbl_ADHOC_EM_Tasks
                                                                       WHERE      (property_code <> '') AND (AccountNumber <> '') AND (property_code <> 'NULL')
                                                                       UNION ALL
                                                                       SELECT     PROPERTY_CODE, RTRIM(ELEC_ACCNTNO) AS Expr1, 'E' AS Expr2, 'DUMP' AS Expr3
                                                                       FROM         dbo.tbl_EM_Utilities_Dump
                                                                       WHERE     (ELEC_ACCNTNO IS NOT NULL) 
                                                                       UNION ALL
                                                                       SELECT     PROPERTY_CODE, RTRIM(GAS_ACCNTNO) AS Expr1, 'G' AS Expr2, 'DUMP' AS Expr3
                                                                       FROM         dbo.tbl_EM_Utilities_Dump AS tbl_EM_Utilities_Dump_2
                                                                       WHERE     (GAS_ACCNTNO IS NOT NULL)
                                                                       UNION ALL
                                                                       SELECT     PROPERTY_CODE, RTRIM(WATER_ACCNTNO) AS Expr1, 'W' AS Expr2, 'DUMP' AS Expr3
                                                                       FROM         dbo.tbl_EM_Utilities_Dump AS tbl_EM_Utilities_Dump_1
                                                                       WHERE     (WATER_ACCNTNO IS NOT NULL) ) AS tbl_src INNER JOIN
                                                                      tbl_REO_Properties tbl_m ON tbl_src.property_code = tbl_m.property_code OUTER APPLY
                                                                          (SELECT     TOP 1 x.property_code
                                                                            FROM          tbl_REO_Properties x
                                                                            WHERE      x.full_address = tbl_m.full_address
                                                                            ORDER BY x.client_hierarchy) tbl_prop
                                                WHERE     (ISNULL(account_number, '') <> 'NULL' AND ISNULL(account_number, '') <> '') and tbl_src.property_code = '{0}' and tbl_src.vendor_group = '{1}'
",
                                               property_code,
                                               vendor_group));

            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResults();
        }
    }
}
