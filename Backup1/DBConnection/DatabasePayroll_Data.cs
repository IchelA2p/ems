﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabasePayroll_Data : DatabaseConnector, IDatabase<DB.Payroll_Data>
    {
        public List<DB.Payroll_Data> GetAllData()
        {
            DatabaseSelector selector = new SelectorPayrollData();
            selector.SetQuery(@"SELECT * FROM altisource.Payroll_Data");

            base.ExecuteSelect(selector);

            return ((SelectorPayrollData)selector).GetResults();
        }

        public uint WriteData(DB.Payroll_Data data)
        {
            string query = string.Format(@"INSERT INTO altisource.Payroll_Data(
                                                userId,
                                                workSched,
                                                HoursND,
                                                HoursReg,
                                                HoursOT,
                                                HoursNDAdjustment,
                                                HoursRegAdjustment,
                                                HoursOTAdjustment,
                                                HoursNDHoliday,
                                                HoursRegHoliday,
                                                HoursOTHoliday)
                                            VALUES('{0}','{1}',{2},{3},{4},{5},{6},{7},{8},{9}{10})",
                                                  data.userId,
                                                  data.workSched,
                                                  data.HoursND,
                                                  data.HoursReg,
                                                  data.HoursOT,
                                                  data.HoursNDAdjustment,
                                                  data.HoursRegAdjustment,
                                                  data.HoursOTAdjustment,
                                                  data.HoursNDHoliday,
                                                  data.HoursRegHoliday,
                                                  data.HoursOTHoliday);

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.Payroll_Data data)
        {
            string query = string.Format(@"UPDATE altisource.Payroll_Data
                                           SET
                                               userId = '{0}',
                                               workSched = '{1}',
                                               HoursND = {2},
                                               HoursReg = {3},
                                               HoursOT = {4},
                                               HoursNDAdjustment = {5} ,
                                               HoursRegAdjustment = {6} ,
                                               HoursOTAdjustment = {7} ,
                                               HoursNDHoliday = {8} ,
                                               HoursRegHoliday = {9} ,
                                               HoursOTHoliday = {10} ,
                                           WHERE
                                               Payroll_Data_Id = {11}",

                                                  data.userId,
                                                  data.workSched,
                                                  data.HoursND,
                                                  data.HoursReg,
                                                  data.HoursOT,
                                                  data.HoursNDAdjustment,
                                                  data.HoursRegAdjustment,
                                                  data.HoursOTAdjustment,
                                                  data.HoursNDHoliday,
                                                  data.HoursRegHoliday,
                                                  data.HoursOTHoliday,

                                                  data.Payroll_Data_Id);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.Payroll_Data GetData(uint Payroll_Data_Id)
        {
            DatabaseSelector selector = new SelectorPayrollData();
            selector.SetQuery(String.Format(@"SELECT * FROM altisource.Payroll_Data WHERE AgentNRId={0}", Payroll_Data_Id));
            
            base.ExecuteSelect(selector);

            return ((SelectorPayrollData)selector).GetResult();
        }

        public List<DB.Payroll_Data> GetDataProcessor(string userId)
        {
            DatabaseSelector selector = new SelectorPayrollData();
            selector.SetQuery(String.Format(@"SELECT * FROM altisource.Payroll_Data WHERE userId={0}", userId));

            base.ExecuteSelect(selector);

            return ((SelectorPayrollData)selector).GetResults();
        }
    }
}
