﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseREO_Properties : DatabaseConnector, IDatabase<DB.REO_Properties>
    {
        public List<DB.REO_Properties> GetAllData()
        {
            DatabaseSelector selector = new SelectorREO_Properties();
            selector.SetQuery(@"SELECT * FROM MIS_ALTI.dbo.tbl_REO_Properties (nolock)");

            base.ExecuteSelect(selector);

            return ((SelectorREO_Properties)selector).GetResults();
        }

        public uint WriteData(DB.REO_Properties data)
        {
            string query = string.Format(@"INSERT INTO MIS_ALTI.dbo.tbl_REO_Properties(
                                                city_name,
                                                full_address,
                                                property_code,
                                                property_number,
                                                state,
                                                zip_code,
                                                customer_id,
                                                customer_name,
                                                investor_name,
                                                property_status,
                                                reosrc_date,
                                                reoslc_date,
                                                reosfc_date,
                                                active,
                                                pod,
                                                property_status_change_date,
                                                inactive_date)
                                            VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},{11},{12},'{13}','{14}',{15},{16})",
                                                  data.city_name, //0
                                                  data.full_address, //1
                                                  data.property_code, //2
                                                  data.property_number, //3
                                                  data.state, //4
                                                  data.zip_code, //5
                                                  data.customer_id, //6
                                                  data.customer_name, //7
                                                  data.investor_name, //8
                                                  data.property_status, //9
                                                  GetString(data.reosrc_date), //10
                                                  GetString(data.reoslc_date), //11
                                                  GetString(data.reosfc_date), //12
                                                  data.active,  //13
                                                  data.pod, //14
                                                  GetString(data.property_status_change_date),  //15
                                                  GetString(data.inactive_date));    //16

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.REO_Properties data)
        {
            string query = string.Format(@"UPDATE MIS_ALTI.dbo.tbl_REO_Properties
                                           SET
                                                property_code = '{0}',
                                                city_name = '{1}',
                                                full_address = '{2}',
                                                state = '{3}',
                                                zip_code = '{4}',
                                                customer_id = '{5}',
                                                customer_name = '{6}',
                                                investor_name = '{7}',
                                                property_status = '{8}',
                                                reosrc_date = {9},
                                                reoslc_date = {10},
                                                reosfc_date = {11},
                                                active = '{12}',
                                                pod = '{13}',
                                                property_status_change_date = {14},
                                                inactive_date = {15}
                                           WHERE
                                               property_number = '{16}'",

                                            data.property_code, //0
                                            data.city_name, //1
                                            data.full_address, //2
                                            data.state, //3
                                            data.zip_code, //4
                                            data.customer_id, //5
                                            data.customer_name, //6
                                            data.investor_name, //7
                                            data.property_status, //8
                                            GetString(data.reosrc_date), //9
                                            GetString(data.reoslc_date), //10
                                            GetString(data.reosfc_date), //11
                                            data.active, //12
                                            data.pod,   //13
                                            GetString(data.property_status_change_date),    //14
                                            GetString(data.inactive_date),  //15

                                            data.property_number); //16

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.REO_Properties GetData(uint property_code)
        {
            DatabaseSelector selector = new SelectorREO_Properties();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_ALTI.dbo.tbl_REO_Properties (nolock) WHERE property_code={0}", property_code));
            
            base.ExecuteSelect(selector);

            return ((SelectorREO_Properties)selector).GetResult();
        }

        public List<DB.REO_Properties> GetZipCode(string property_code, string zip_code, string street_number, string street_name, string state, string city_name)
        {
            DatabaseSelector selector = new SelectorREO_Properties();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_ALTI.dbo.tbl_REO_Properties (nolock) WHERE zip_code LIKE '%{0}%' 
                                                AND full_address LIKE '%{1}%' AND state LIKE '%{2}%' AND city_name LIKE '%{3}%' and property_code LIKE '%{4}%'
                                                AND full_address LIKe '%{5}%'",
                                                zip_code, street_number, state, city_name.ToUpper(), property_code, street_name));

            base.ExecuteSelect(selector);

            return ((SelectorREO_Properties)selector).GetResults();
        }

        public DB.REO_Properties GetData(string property_code)
        {
            DatabaseSelector selector = new SelectorREO_Properties();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_ALTI.dbo.tbl_REO_Properties (nolock) WHERE (property_code = '{0}' or property_code = '{0}' + '1') order by client_hierarchy asc", property_code));

            base.ExecuteSelect(selector);

            return ((SelectorREO_Properties)selector).GetResult();
        }


        public DB.REO_Properties GetData2(string property_code)
        {
            DatabaseSelector selector = new SelectorREO_Properties();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_ALTI.dbo.tbl_REO_Properties (nolock) where (property_code = '{0}' or property_code = '{0}' + '1') order by client_hierarchy asc", property_code));

            base.ExecuteSelect(selector);

            return ((SelectorREO_Properties)selector).GetResult();
        }


        public List<DB.REO_Properties> GetDatas(string property_code)
        {
            DatabaseSelector selector = new SelectorREO_Properties();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_ALTI.dbo.tbl_REO_Properties (nolock) WHERE property_code = '{0}'", property_code));

            base.ExecuteSelect(selector);

            return ((SelectorREO_Properties)selector).GetResults();
        }

        public DB.REO_Properties GetDataUsingPropertyNumber(string property_number)
        {
            DatabaseSelector selector = new SelectorREO_Properties();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_ALTI.dbo.tbl_REO_Properties (nolock) WHERE property_number = '{0}'", property_number));

            base.ExecuteSelect(selector);

            return ((SelectorREO_Properties)selector).GetResult();
        }

        public DB.REO_Properties GetActiveClientCode(string property_code)
        {
            DatabaseSelector selector = new SelectorREO_Properties();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_ALTI.dbo.tbl_REO_Properties (nolock) WHERE property_code = '{0}' AND active = '{1}'", property_code, "Active"));

            base.ExecuteSelect(selector);

            return ((SelectorREO_Properties)selector).GetResult();
        }

        public void DeleteData()
        {
            string query = @"Truncate Table MIS_ALTI.dbo.tbl_REO_Properties";

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public List<DB.REO_Properties> SearchActiveProperty(string house_no, string street_name, 
            string city_name, string state, string zipcode)
        {            
            DatabaseSelector selector = new SelectorREO_Properties();

            // Modified by RLA: 2015-07-19
//            string query = string.Format(@"SELECT top 50 * FROM MIS_ALTI.dbo.tbl_REO_Properties (nolock) WHERE 
//                                            full_address LIKE '{0} %' AND
//                                            city_name LIKE '%{1}%' AND
//                                            city_name NOT LIKE '%{2}%' AND
//                                            state LIKE '%{3}%' AND
//                                            state NOT LIKE '%{4}%' AND
//                                            RTRIM(zip_code) Like '%{5}%' AND
//                                            RTRIM(zip_code) NOT Like '%{6}%'",
//                                            house_no,
//                                            city_name,
//                                            house_no,
//                                            state,
//                                            house_no,
//                                            zipcode,
//                                            house_no);

            string query = string.Empty;

          
                if (zipcode != "")
                {
                    query = string.Format(@"SELECT top 50 * FROM MIS_ALTI.dbo.tbl_REO_Properties with(index(full_address)) 
                                            WHERE (full_address LIKE '{0}%' or substring(full_address,1,9) like '%{0}') AND
                                            city_name LIKE '%{1}%' AND
                                            city_name NOT LIKE '%{2}%' AND
                                            [state] LIKE '%{3}%' AND
                                            [state] NOT LIKE '%{4}%' AND
                                            RTRIM(zip_code) = '{5}'",
                                           house_no,
                                           city_name,
                                           house_no,
                                           state,
                                           house_no,
                                           zipcode,
                                           house_no);
                }
                else
                {
                    query = string.Format(@"SELECT top 50 * FROM MIS_ALTI.dbo.tbl_REO_Properties with(index(full_address)) 
                                            WHERE (full_address LIKE '{0}%' or substring(full_address,1,9) like '%{0}') AND
                                            city_name LIKE '%{1}%' AND
                                            city_name NOT LIKE '%{2}%' AND
                                           [state] LIKE '%{3}%' AND
                                            [state] NOT LIKE '%{4}%'",
                                           house_no,
                                           city_name,
                                           house_no,
                                           state,
                                           house_no,
                                           zipcode,
                                           house_no);
                }

                      
            string newStreetName = string.Empty;

            string[] streets;
            
            if (street_name != string.Empty)
            {
                streets = street_name.Split(' ');

                if (streets.Count() > 1)
                {
                    bool hasBigWord = false;

                    foreach (string st in streets)
                    {
                        if (st.Length > 2)
                        {
                            hasBigWord = true;
                        }
                    }

                    if (hasBigWord)
                    {
                        foreach (string st in streets)
                        {
                            if (st.Length > 2)
                            {
                                newStreetName += st + " ";
                            }
                        }

                        street_name = newStreetName.Trim();
                        streets = street_name.Split(' ');
                    }
                }

                if (streets.Count() > 1)
                {
                    query = query + string.Format("AND ( full_address LIKE '%{0}%' OR ", streets[0]);

                    for (int i = 1; i < streets.Count() - 1; i++)
                    {
                        query = query = query + string.Format(" full_address LIKE '%{0}%' OR ", streets[i]);
                    }

                    query = query + string.Format(" full_address LIKE '%{0}%' ) ", streets[streets.Count() - 1]);
                }
                else
                {
                    query = query + string.Format(" AND full_address LIKE '%{0}%'", street_name);
                }
            }
            else
            {
                query = query + string.Format(" AND full_address LIKE '%{0}%'", street_name);
            }

            query = query + string.Format(" ORDER BY full_address asc, Active asc, client_hierarchy asc");
            

            selector.SetQuery(query);

            selector.SetQuery(DBHelper.toSingleSpace(selector.GetQuery()));
            base.ExecuteSelect(selector);
            
            return ((SelectorREO_Properties)selector).GetResults();
        }

        public List<DB.REO_Properties> SearchInactiveProperty(string house_no, string street_name,
            string city_name, string state, string zipcode)
        {
            DatabaseSelector selector = new SelectorREO_Properties();
            selector.SetQuery(string.Format(@"SELECT top 50 * FROM MIS_ALTI.dbo.tbl_REO_Properties (nolock) WHERE 
                                            full_address LIKE '{0} %' AND
                                            full_address LIKE '%{1}%' AND
                                            city_name LIKE '%{2}%' AND
                                            state LIKE '%{3}%' AND
                                            RTRIM(zip_code) Like '%{4}%' AND
                                            active = 'Inactive'",
                                            house_no,
                                            street_name,
                                            city_name,
                                            state,
                                            zipcode));

            selector.SetQuery(DBHelper.toSingleSpace(selector.GetQuery()));
            base.ExecuteSelect(selector);

            return ((SelectorREO_Properties)selector).GetResults();
        }

        public List<DB.REO_Properties> SearchFromRecentPropertiesSearches(string house_no, string street_name,
            string city_name, string state, string zipcode)
        {
            DatabaseSelector selector = new SelectorREO_Properties();
            selector.SetQuery(string.Format(@"SELECT * FROM MIS_ALTI.dbo.view_EM_RecentPropertySearch (nolock) WHERE 
                                            full_address LIKE '{0}%' AND
                                            full_address LIKE '%{1}%' AND
                                            city_name LIKE '%{2}%' AND
                                            state LIKE '%{3}%' AND
                                            RTRIM(zip_code) Like '%{4}%'",
                                            house_no,
                                            street_name,
                                            city_name,
                                            state,
                                            zipcode));

            selector.SetQuery(DBHelper.toSingleSpace(selector.GetQuery()));
            base.ExecuteSelect(selector);

            return ((SelectorREO_Properties)selector).GetResults();
        }
    }
}
