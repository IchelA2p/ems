﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;


namespace Database.DBConnection
{
    public class DatabaseConnector
    {
        private SqlConnection Connection;
        public string connectionString = @"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a";
        //public string connectionString = @"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;Trusted_Connection=Yes";

        public DatabaseConnector()
        {
            //string connectionString = @"provider=sqloledb;server=PHP-5F-005\SQLEXPRESS;database=MIS_Alti;uid=bfa021;pwd=@lti_08";
            //string connectionString = @"Data Source=PHP-5F-005\SQLEXPRESS;Database=MIS_Alti;uid=bfa021;pwd=@lti_08;Connect Timeout=30";
            //string connectionString = @"Data Source=PHP-5F-TR2-206\SQLEXPRESS;Database=MIS_Alti;uid=bfa021;pwd=@lti_08";
            //string connectionString = @"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a";
            
            Connection = new SqlConnection(connectionString);
        }

        public bool OpenConn(out SqlConnection connection)
        {
            string cString = @"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a";
            connection = new SqlConnection(cString);

            try
            {
                connection.Open();
                return true;
            }
            catch
            {
                return false;
            }
        }

        private bool OpenConnection()
        {
            try
            {
                Connection.Open();
                return true;
            }
            catch //(Exception e)
            {
                return false;
            }
        }

        private bool CloseConnection()
        {
            try
            {
                Connection.Close();
                return true;
            }
            catch //(Exception e)
            {
                return false;
            }
        }

        protected long ExecuteNonQuery(string query)
        {
            long affectedRow = 0;
            if (this.OpenConnection() == true)
            {
                SqlCommand cmd = new SqlCommand(query, Connection);
                affectedRow = cmd.ExecuteNonQuery();
                this.CloseConnection();
            }
            return affectedRow;
        }

        protected void ExecuteSelect(Database.DBSelector.DatabaseSelector selector)
        {
            if (this.OpenConnection() == true)
            {
                SqlCommand cmd = new SqlCommand(selector.GetQuery(), Connection);
                SqlDataReader dataReader = cmd.ExecuteReader();

                selector.GetData(dataReader);
                dataReader.Close();

                this.CloseConnection();
            }
        }

        public string GetString(DateTime dt)
        {
            if (dt == DateTime.MinValue)
            { return "NULL"; }
            else
            { return string.Format(@"'{0}'", dt.ToString("yyyy-MM-dd HH:mm:ss")); }
        }
    }
}
