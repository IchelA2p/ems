﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseAssignQueue : DatabaseConnector, IDatabase<DB.AssignQueue>
    {
        public List<DB.AssignQueue> GetAllData()
        {
            DatabaseSelector selector = new SelectorAssignQueue();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_Queue NOLOCK order by userid");

            base.ExecuteSelect(selector);

            return ((SelectorAssignQueue)selector).GetResults();
        }

        public uint WriteData(DB.AssignQueue data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_EM_Queue(
                                                assignee,
                                                current_queue,
                                                unchangeable,
                                                userid)
                                            VALUES('{0}','{1}',{2},'{3}')",
                                                  data.assignee, //0
                                                  data.current_queue,   //1
                                                  data.unchangeable,    //2
                                                  data.userid); //3

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.AssignQueue data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_EM_Queue
                                           SET
                                                assignee = '{1}',
                                                current_queue = '{2}',
                                                unchangeable = {3},
                                                userid = '{4}'
                                           WHERE
                                                queue_id = {0}",

                                            data.queue_id, //0
                                            data.assignee,  //1
                                            data.current_queue, //2
                                            data.unchangeable,  //3
                                            data.userid);   //4

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.AssignQueue GetData(uint queue_id)
        {
            DatabaseSelector selector = new SelectorAssignQueue();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_Queue NOLOCK WHERE queue_id={0}", queue_id));
            
            base.ExecuteSelect(selector);

            return ((SelectorAssignQueue)selector).GetResult();
        }

        public DB.AssignQueue GetDataByUserid(string userid)
        {
            DatabaseSelector selector = new SelectorAssignQueue();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_Queue NOLOCK WHERE userid='{0}'", userid));

            base.ExecuteSelect(selector);

            return ((SelectorAssignQueue)selector).GetResult();
        }
    }
}
