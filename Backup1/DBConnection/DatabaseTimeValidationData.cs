﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseTimeValidationData : DatabaseConnector, IDatabase<DB.TimeValidation_Data>
    {
        public List<DB.TimeValidation_Data> GetAllData()
        {
            DatabaseSelector selector = new SelectorTimeValidationData();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Timevalidation");

            base.ExecuteSelect(selector);

            return ((SelectorTimeValidationData)selector).GetResults();
        }

        public uint WriteData(DB.TimeValidation_Data data)
        {
            DB.TimeValidation_Data data1 = new TimeValidation_Data();
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_PAY_Timevalidation(
                                                StartDate,
                                                EndDate,
                                                Validator_EmpId,
                                                TimeValidationDate,
                                                Userid,
                                                isCreatedColumn,
                                                NewNotReadyReasonId)
                                            VALUES('{0}','{1}',{2},'{3}','{4}',{5},'{6}')",
                                                  GetString(data.StartDate),
                                                  GetString(data.EndDate),
                                                  data.Validator_EmpId,
                                                  GetString(data.TimeValidationDate),
                                                  data.Userid,
                                                  data.isCreatedColumn == true ? 1 : 0,
                                                  data.NewNotReadyReasonId);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
            data1 = GetIdTimeValidation(data.StartDate, data.EndDate, data.Validator_EmpId, data.TimeValidationDate, data.Userid);
            return ((uint)data1.TimeValidationId);
        }

        public void UpdateData(DB.TimeValidation_Data data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_PAY_Timevalidation
                                           SET
                                               StartDate = '{0}',
                                               EndDate = '{1}',
                                               Validator_EmpId = {2},
                                               TimeValidationDate = '{3}',
                                               Userid = '{4}',
                                               isCreatedColumn = {5} ,
                                               NewNotReadyReasonId = '{6}'
                                           WHERE
                                               Time_validationid = {7}",

                                            GetString(data.StartDate),
                                            GetString(data.EndDate),
                                            data.Validator_EmpId,
                                            GetString(data.TimeValidationDate),
                                            data.Userid,
                                            data.isCreatedColumn,
                                            data.NewNotReadyReasonId,
                                            data.TimeValidationId);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.TimeValidation_Data GetData(uint ValidationId)
        {
            DatabaseSelector selector = new SelectorTimeValidationData();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Timevalidation WHERE Time_validationid = {0}", ValidationId));
            
            base.ExecuteSelect(selector);

            return ((SelectorTimeValidationData)selector).GetResult();
        }

        public List<DB.TimeValidation_Data> GetDataProcessor(uint Validator_EmpId)
        {
            DatabaseSelector selector = new SelectorTimeValidationData();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Timevalidation WHERE Validator_EmpId = {0}", Validator_EmpId));

            base.ExecuteSelect(selector);

            return ((SelectorTimeValidationData)selector).GetResults();
        }

        public DB.TimeValidation_Data GetIdTimeValidation(DateTime startdate, DateTime enddate, int validatorid, DateTime validationdate, string Userid)
        {
            DatabaseSelector selector = new SelectorTimeValidationData();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Timevalidation WHERE StartDate = '{0}' AND EndDate = '{1}' AND Validator_EmpId = {2} AND TimeValidationDate = '{3}' AND Userid = '{4}'",
                                            startdate,
                                            enddate,
                                            validatorid,
                                            validationdate,
                                            Userid));

            base.ExecuteSelect(selector);

            return ((SelectorTimeValidationData)selector).GetResult();
        }

        public void DeleteValidation(uint Time_validationid)
        {
            DatabaseSelector selector = new SelectorTimeValidationData();
            selector.SetQuery(String.Format(@"DELETE FROM MIS_Alti.dbo.tbl_PAY_Timevalidation WHERE Time_validationid = {0}", Time_validationid));

            base.ExecuteSelect(selector);
        }
    }
}
