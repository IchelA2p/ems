﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseAssignVendor : DatabaseConnector, IDatabase<DB.AssignVendor>
    {
        public List<DB.AssignVendor> GetAllData()
        {
            DatabaseSelector selector = new SelectorAssignVendor();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_AssignVendor]");

            base.ExecuteSelect(selector);

            return ((SelectorAssignVendor)selector).GetResults();
        }

        public uint WriteData(DB.AssignVendor data)
        {
            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_AssignVendor](
                                                dateAssigned,
                                                dateCreated,
                                                isAssigned,
                                                property_code,
                                                remarks,
                                                vendor_code,
                                                vendor_group,
                                                isActive,
                                                createdBy)
                                            VALUES({0},{1},{2},'{3}','{4}','{5}','{6}',{7},'{8}')",
                                                  GetString(data.dateAssigned), //0
                                                  GetString(data.dateCreated),  //1
                                                  data.isAssigned == true? 1:0, //2
                                                  data.property_code,           //3
                                                  data.remarks,                 //4
                                                  data.vendor_code,             //5
                                                  data.vendor_group,            //6
                                                  data.isActive == true ? 1 : 0,//7
                                                  data.createdBy);              //8

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.AssignVendor data)
        {
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_EM_AssignVendor]
                                           SET
                                                dateAssigned = {1},
                                                dateCreated = {2},
                                                property_code = {3},
                                                remarks = {4},
                                                vendor_code = {5},
                                                vendor_group = {6},
                                                isAssigned = {7},
                                                isActive = {8},
                                                createdBy = '{9}'
                                           WHERE
                                                id = {0}",

                                            data.id,                        //0
                                            GetString(data.dateAssigned),   //1
                                            GetString(data.dateCreated),    //2
                                            data.property_code,             //3
                                            data.remarks,                   //4
                                            data.vendor_code,               //5
                                            data.vendor_group,              //6
                                            data.isAssigned == true ? 1 : 0,//7
                                            data.isActive == true ? 1 : 0,  //8
                                            data.createdBy);                //9

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.AssignVendor GetData(uint id)
        {
            DatabaseSelector selector = new SelectorAssignVendor();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_AssignVendor] WHERE id={0}", id));
            
            base.ExecuteSelect(selector);

            return ((SelectorAssignVendor)selector).GetResult();
        }

        public List<DB.AssignVendor> GetDataProcessor(string id)
        {
            DatabaseSelector selector = new SelectorAssignVendor();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_AssignVendor] WHERE id={0}", id));

            base.ExecuteSelect(selector);

            return ((SelectorAssignVendor)selector).GetResults();
        }
    }
}
