﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseEbillWebsites : DatabaseConnector, IDatabase<DB.EbillWebsites>
    {
        public List<DB.EbillWebsites> GetAllData()
        {
            DatabaseSelector selector = new SelectorEbillWebsites();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_EbillWebsites]");

            base.ExecuteSelect(selector);

            return ((SelectorEbillWebsites)selector).GetResults();
        }

        public uint WriteData(DB.EbillWebsites data)
        {
            string last_date_modified = string.Empty;
            string date_created = string.Empty;

            if (data.last_date_modified == DateTime.MinValue)
            { last_date_modified = "NULL"; }
            else
            { last_date_modified = string.Format(@"'{0}'", data.last_date_modified.ToString("yyyy-MM-dd HH:mm:ss")); }

            if (data.date_created == DateTime.MinValue)
            { date_created = "NULL"; }
            else
            { date_created = string.Format(@"'{0}'", data.date_created.ToString("yyyy-MM-dd HH:mm:ss")); }


            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_EbillWebsites](
                                                username,
                                                password,
                                                website_name,
                                                createdby,
                                                updatedby,
                                                last_date_modified,
                                                date_created,
                                                usp_name,
                                                email)
                                            VALUES('{0}','{1}','{2}','{3}','{4}',{5},{6},'{7}','{8}')",
                                                  data.username, //0
                                                  data.password,    //1
                                                  data.website_name,    //2
                                                  data.createdby,   //3
                                                  data.updatedby,   //4
                                                  last_date_modified,   //5
                                                  date_created, //6
                                                  data.usp_name,    //7
                                                  data.email);    //8

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.EbillWebsites data)
        {
            string last_date_modified = string.Empty;
            string date_created = string.Empty;

            if (data.last_date_modified == DateTime.MinValue)
            { last_date_modified = "NULL"; }
            else
            { last_date_modified = string.Format(@"'{0}'", data.last_date_modified.ToString("yyyy-MM-dd HH:mm:ss")); }

            if (data.date_created == DateTime.MinValue)
            { date_created = "NULL"; }
            else
            { date_created = string.Format(@"'{0}'", data.date_created.ToString("yyyy-MM-dd HH:mm:ss")); }

            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_EM_EbillWebsites]
                                           SET
                                                website_name = '{1}',
                                                username = '{2}',
                                                password = '{3}',
                                                createdby = '{4}',
                                                date_created = {5},
                                                updatedby = '{6}',
                                                last_date_modified = {7},
                                                email = '{8}',
                                                usp_name = '{9}'
                                           WHERE
                                                id = {0}",

                                            data.id,    //0
                                            data.website_name,  //1
                                            data.username,  //2
                                            data.password,  //3
                                            data.createdby, //4
                                            date_created,  //5
                                            data.updatedby, //6
                                            last_date_modified, //7
                                            data.email, //8
                                            data.usp_name);   //9

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.EbillWebsites GetData(uint id)
        {
            DatabaseSelector selector = new SelectorEbillWebsites();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_EbillWebsites] WHERE id={0}", id));
            
            base.ExecuteSelect(selector);

            return ((SelectorEbillWebsites)selector).GetResult();
        }

        public List<DB.EbillWebsites> GetDataProcessor(string website_name)
        {
            DatabaseSelector selector = new SelectorEbillWebsites();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_EbillWebsites] WHERE website_name='{0}'", website_name));

            base.ExecuteSelect(selector);

            return ((SelectorEbillWebsites)selector).GetResults();
        }
    }
}
