﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseAssetManager : DatabaseConnector, IDatabase<DB.AssetManager>
    {
        public List<DB.AssetManager> GetAllData()
        {
            DatabaseSelector selector = new SelectorAssetManager();
            //selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_AssetManagers ORDER BY asset_manager");

            selector.SetQuery(@"SELECT distinct asset_manager FROM MIS_Alti.dbo.tbl_REO_Properties where asset_manager is not null");

            base.ExecuteSelect(selector);

            return ((SelectorAssetManager)selector).GetResults();
        }

        public uint WriteData(DB.AssetManager data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_EM_AssetManagers(
                                                property_code,
                                                asset_manager)
                                            VALUES('{0}','{1}')",
                                                  data.property_code, //0
                                                  data.asset_manager); //1

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.AssetManager data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_EM_AssetManagers
                                           SET
                                                asset_manager = '{0}'
                                           WHERE
                                                property_code = '{1}'",

                                            data.asset_manager, //0
                                            data.property_code); //1

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.AssetManager GetData(uint property_code)
        {
            DatabaseSelector selector = new SelectorAssetManager();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_AssetManagers WHERE property_code={0}", property_code));
            
            base.ExecuteSelect(selector);

            return ((SelectorAssetManager)selector).GetResult();
        }

        public DB.AssetManager GetDataProcessor(string property_code)
        {
            DatabaseSelector selector = new SelectorAssetManager();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_AssetManagers WHERE property_code='{0}'", property_code));

            base.ExecuteSelect(selector);

            return ((SelectorAssetManager)selector).GetResult();
        }
    }
}
