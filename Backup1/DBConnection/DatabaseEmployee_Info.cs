﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseEmployee_Info : DatabaseConnector, IDatabase<DB.Employee_Info>
    {
        public List<DB.Employee_Info> GetAllData()
        {
            DatabaseSelector selector = new SelectorEmployee();
            selector.SetQuery("SELECT * FROM MIS_Alti.dbo.tbl_PAY_Employeeinfo");

            base.ExecuteSelect(selector);

            return ((SelectorEmployee)selector).GetResults();
        }

        public uint WriteData(DB.Employee_Info data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_PAY_Employeeinfo (
                                                Employeeid,
                                                Userid,
                                                Firstname,
                                                Middlename,
                                                Lastname,
                                                Nickname,
                                                ntid,
                                                Position,
                                                isActive,
                                                Username,
                                                Password,
                                                EmployeePrimaryId)
                                            VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}',{8},'{9}','{10}',{11})",
                                                  data.EmployeeId,
                                                  data.userId,
                                                  data.FirstName,
                                                  data.MiddleName,
                                                  data.LastName,
                                                  data.Nickname,
                                                  data.NTID,
                                                  data.Position,
                                                  data.isActive == true ? 1 : 0,
                                                  data.Username,
                                                  data.Password,
                                                  data.EmployeePrimaryId);

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.Employee_Info data)
        {
            string query = string.Format(@"UPDATE altisource.employee_info
                                           SET
                                               Userid = '{0}',
                                               Firstname = '{1}',
                                               Middlename = '{2}',
                                               Lastname = '{3}',
                                               Nickname = '{4}',
                                               ntid = '{5}',
                                               Position = '{6}',
                                               Employeeid = {7},
                                               isActive = {8},
                                               Username = '{9}',
                                               Password = '{10}'  
                                           WHERE
                                               Employeeprimaryid = {11}",

                                            data.userId,
                                            data.FirstName,
                                            data.MiddleName,
                                            data.LastName,
                                            data.Nickname,
                                            data.NTID,
                                            data.Position,
                                            data.EmployeeId,
                                            data.isActive == true ? 1 : 0,
                                            data.Username,
                                            data.Password,
                                            data.EmployeePrimaryId);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.Employee_Info GetData(uint EmployeePrimaryId)
        {
            DatabaseSelector selector = new SelectorEmployee();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Employeeinfo WHERE Employeeprimaryid = {0}", EmployeePrimaryId));
            
            base.ExecuteSelect(selector);

            return ((SelectorEmployee)selector).GetResult();
        }

        public DB.Employee_Info GetDataUsingEmployeePrimaryId(int EmployeePrimaryId)
        {
            DatabaseSelector selector = new SelectorEmployee();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Employeeinfo WHERE Employeeprimaryid = {0}", EmployeePrimaryId));

            base.ExecuteSelect(selector);

            return ((SelectorEmployee)selector).GetResult();
        }

        public DB.Employee_Info GetDataProcessor(string userId)
        {
            DatabaseSelector selector = new SelectorEmployee();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Employeeinfo WHERE Userid = '{0}'", userId));
            base.ExecuteSelect(selector);

            return ((SelectorEmployee)selector).GetResult();
        }


    }
}
