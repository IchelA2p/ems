﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseUSP_Contact : DatabaseConnector, IDatabase<DB.USP_Contact>
    {
        public List<DB.USP_Contact> GetAllData()
        {
            DatabaseSelector selector = new SelectorUSP_Contact();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_UtilityServiceProvider");

            base.ExecuteSelect(selector);

            return ((SelectorUSP_Contact)selector).GetResults();
        }

        public uint WriteData(DB.USP_Contact data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_UtilityServiceProvider(
                                                Address,
                                                Name,
                                                PhoneNumber,
                                                Type,
                                                VendorId,
                                                City,
                                                State,
                                                Zipcode,
                                                Status)
                                            VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')",
                                                  data.Address, //0
                                                  data.Name, //1
                                                  data.PhoneNumber, //2
                                                  data.Type, //3
                                                  data.VendorId,    //4
                                                  data.City,    //5
                                                  data.State,   //6
                                                  data.Zipcode, //7
                                                  data.Status);    //8

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.USP_Contact data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_UtilityServiceProvider
                                           SET
                                                Address = '{1}',
                                                PhoneNumber = '{2}',
                                                Type = '{3}',
                                                Name = '{4}',
                                                City = '{5}',
                                                State = '{6}',
                                                Zipcode = '{7}',
                                                Status = '{8}'
                                           WHERE
                                                VendorId = '{0}'",

                                            data.VendorId,  //0
                                            data.Address,   //1
                                            data.PhoneNumber,   //2
                                            data.Type,  //3
                                            data.Name,  //4
                                            data.City,  //5
                                            data.State, //6
                                            data.Zipcode,   //7
                                            data.Status);  //8

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.USP_Contact GetData(uint Name)
        {
            DatabaseSelector selector = new SelectorUSP_Contact();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_UtilityServiceProvider WHERE Name={0}", Name));
            
            base.ExecuteSelect(selector);

            return ((SelectorUSP_Contact)selector).GetResult();
        }

        public List<DB.USP_Contact> GetDataProcessor(string VendorId, string VendorName, string StreetNumber, string StreetName, string CityName, string State, string PhoneNumber, string Type)
        {
            DatabaseSelector selector = new SelectorUSP_Contact();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_UtilityServiceProvider WHERE VendorId LIKE '%{0}%' AND
                                                Name LIKE '%{1}%' AND Address LIKE '%{2}%' AND Address LIKE '%{3}%' AND Address LIKE '%{4}%'
                                                AND Address LIKE '%{5}%' AND PhoneNumber LIKE '%{6}%' AND Type LIKE '%{7}%'",
                                                VendorId, VendorName, StreetNumber, StreetName, CityName, State, PhoneNumber, Type));

            base.ExecuteSelect(selector);

            return ((SelectorUSP_Contact)selector).GetResults();
        }

        public DB.USP_Contact GetUspData(string VendorId)
        {
            DatabaseSelector selector = new SelectorUSP_Contact();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_UtilityServiceProvider WHERE VendorId = '{0}'",VendorId));

            base.ExecuteSelect(selector);

            return ((SelectorUSP_Contact)selector).GetResult();
        }

        public void DeleteData()
        {
            string query = @"DELETE FROM MIS_Alti.dbo.tbl_UtilityServiceProvider";

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }
    }
}
