﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorEM_Tasks : DatabaseSelector
    {
        private List<Database.DB.EM_Tasks> list;

        public SelectorEM_Tasks()
         {
             list = new List<DB.EM_Tasks>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.EM_Tasks info = new DB.EM_Tasks();

                info.AccountNumber = GetStrData(dataReader, "AccountNumber");
                info.AmountBeforeDue = GetDecimal(dataReader, "AmountBeforeDue");
                info.Amount = GetStrData(dataReader, "Amount");
                info.property_code = GetStrData(dataReader, "property_code");
                info.vendor_group = GetStrData(dataReader, "vendor_group");

                try
                {
                    info.Complete = (int)dataReader["Complete"];
                }
                catch
                {
                    info.Complete = 0;
                }
                

                info.InvoiceFolderName = GetStrData(dataReader, "InvoiceFolderName");
                info.work_item = GetStrData(dataReader, "work_item");
                info.ParentAccount = GetStrData(dataReader, "ParentAccount");
                info.PendingReasons = GetStrData(dataReader, "PendingReasons");

                try
                {
                    info.InvoiceCount = Convert.ToInt32(GetStrData(dataReader, "InvoiceCount"));
                }
                catch
                {
                    info.InvoiceCount = 0;
                }
                

                info.isAddressedToASFI = GetBool(dataReader, "isAddressedToASFI");
                info.BillTo = GetStrData(dataReader, "BillTo");
                try
                {
                    info.Duration = float.Parse(dataReader["Duration"].ToString());
                }
                catch
                {
                    info.Duration = 0;
                }

                try
                {


                    info.Invoice = GetStrData(dataReader, "Invoice");
                    info.id = (int)(dataReader["id"]);

                    info.DueDate = GetDateTimeData(dataReader, "DueDate");
                                      

                    info.EmployeePrimaryId = (int)dataReader["EmployeePrimaryId"];
                   

                    info.client_code = GetStrData(dataReader, "client_code");
                    info.ServiceFrom = GetDateTimeData(dataReader, "ServiceFrom"); //(DateTime)dataReader["ServiceFrom"];

                    info.ServiceTo = GetDateTimeData(dataReader, "ServiceTo");
                    info.usp_name = GetStrData(dataReader, "usp_name");

                    info.LastDateModified = GetDateTimeData(dataReader, "LastDateModified");
                    info.PendingReasons = GetStrData(dataReader, "PendingReasons");

                    
                    info.final_bill = GetStrData(dataReader, "final_bill");

                    info.date_bulkuploaded = GetDateTimeData(dataReader, "date_bulkuploaded");
                    info.userid = GetStrData(dataReader, "userid");

                    info.VendorId = GetStrData(dataReader, "VendorId");
                    info.invoice_date = GetDateTimeData(dataReader, "invoice_date");

                    info.isActive = GetBool(dataReader, "isActive");
                    info.CurrentCharge = GetDecimal(dataReader, "CurrentCharge");

                    info.DisconnectionFee = GetDecimal(dataReader, "DisconnectionFee");
                    info.LateFee = GetDecimal(dataReader, "LateFee");

                    info.PreviousBalance = GetDecimal(dataReader, "PreviousBalance");
                    info.PaymentReceived = GetDecimal(dataReader, "PaymentReceived");

                    info.vendor_address = GetStrData(dataReader, "vendor_address");
                    info.SpecialInstruction = GetStrData(dataReader, "SpecialInstruction");

                    info.remarks = GetStrData(dataReader, "remarks");

                   
                    info.HouseNoEntered = GetStrData(dataReader, "HouseNoEntered");

                    info.StateEntered = GetStrData(dataReader, "StateEntered");
                    info.StreetEntered = GetStrData(dataReader, "StreetEntered");

                    info.CityEntered = GetStrData(dataReader, "CityEntered");
                    info.ZipcodeEntered = GetStrData(dataReader, "ZipcodeEntered");

                    info.FileAttachment = GetStrData(dataReader, "FileAttachment");
                    info.isOutOfREO = GetBool(dataReader, "isOutOfREO");

                    info.DisconnectionDate = GetDateTimeData(dataReader, "DisconnectionDate");
                    info.AmountAfterDue = GetDecimal(dataReader, "AmountAfterDue");

                    info.VendorAssignedStatus = GetStrData(dataReader, "VendorAssignedStatus");
                    info.HasTransferAmount = GetStrData(dataReader, "HasTransferAmount");

                    info.isRPM = GetBool(dataReader, "isRPM");
                    info.isASFI = GetBool(dataReader, "isASFI");

                    info.isLatestData = GetBool(dataReader, "isLatestData");
                    info.lockedto = GetStrData(dataReader, "lockedto");

                    info.isLienValid = GetBool(dataReader, "isLienValid");
                    info.AttachmentFile = GetStrData(dataReader, "AttachmentFile");


                    try
                    {
                        info.full_address = GetStrData(dataReader, "full_address");
                    }
                    catch
                    {
                        info.full_address = "";
                    }

                    try
                    {
                        info.property_status = GetStrData(dataReader, "property_status");
                    }
                    catch
                    {
                        info.property_status = "";
                    }

                    try
                    {
                        info.Age = GetStrData(dataReader, "Age");
                    }
                    catch
                    {
                        info.Age = "0";
                    }

                    info.PreviousAmtPaid = GetDecimal(dataReader, "PreviousAmtPaid");
                    info.Deposit = GetDecimal(dataReader, "Deposit");
                    info.ActAmount = GetDecimal(dataReader, "ActAmount");
                }
                catch
                {
 
                }                

                list.Add(info);
            }
        }

        public List<DB.EM_Tasks> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.EM_Tasks GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
