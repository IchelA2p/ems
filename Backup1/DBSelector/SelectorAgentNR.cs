﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorAgentNR : DatabaseSelector
    {
        private List<Database.DB.AgentNRData> list;

        public SelectorAgentNR()
         {
             list = new List<DB.AgentNRData>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.AgentNRData info = new DB.AgentNRData();
                info.AgentNRId = (int)dataReader["agentnrid"];
                info.userId = GetStrData(dataReader, "userid");
                info.workSched = GetDateTimeData(dataReader, "worksched");
                info.NotReadyStartdt = GetDateTimeData(dataReader, "NotReadyStartdt");
                info.NotReadyEnddt = GetDateTimeData(dataReader, "NotReadyEnddt");
                info.ReasonId = GetStrData(dataReader, "reasonid");
                try
                {
                    info.ValidationId = (int)dataReader["validationid"];
                }
                catch
                {
                    info.ValidationId = 0;
                }

                list.Add(info);
            }
        }

        public List<DB.AgentNRData> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.AgentNRData GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
