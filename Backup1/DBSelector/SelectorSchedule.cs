﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorSchedule : DatabaseSelector
    {
        private List<Database.DB.Schedule_Data> schedlist;

        public SelectorSchedule()
         {
             schedlist = new List<DB.Schedule_Data>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            schedlist.Clear();
            int absent, restday, pto;
            while (dataReader.Read())
            {
                DB.Schedule_Data info = new DB.Schedule_Data();
                info.userId = (string)dataReader["userId"];
                try
                {
                    info.SchedIn = (DateTime)dataReader["SchedIn"];
                }
                catch
                {
                    info.SchedIn = DateTime.MinValue;
                }
                try
                {
                    info.SchedOut = (DateTime)dataReader["SchedOut"];
                }
                catch
                {
                    info.SchedOut = DateTime.MinValue;
                }
                info.Date = (DateTime)dataReader["Date"];
                
                
                
                info.Absent = false;
                info.RestDay = false;
                info.PTO = false;
                try
                {
                    absent = (int)dataReader["Absent"];
                    if (absent == 1)
                    {
                        info.Absent = true;
                    }
                }
                catch
                { 
                    // do nothing
                }
                try
                {
                    restday = (int)dataReader["RestDay"];
                    if (restday == 1)
                    {
                        info.RestDay = true;
                    }
                }
                catch
                { 
                    //do nothing
                }
                try
                {
                    pto = (int)dataReader["PTO"];
                    if (pto == 1)
                    {
                        info.PTO = true;
                    }
                }
                catch
                { 
                    //do nothing
                }
                schedlist.Add(info);
            }
        }

        public List<DB.Schedule_Data> GetResults()
        {
            return schedlist.Count == 0 ? null : schedlist;
        }

        public DB.Schedule_Data GetResult()
        {
            return schedlist.Count == 0 ? null : schedlist[0];
        }
    }
}
