﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorProperty_Investors : DatabaseSelector
    {
        private List<Database.DB.Property_Investors> list;

        public SelectorProperty_Investors()
         {
             list = new List<DB.Property_Investors>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.Property_Investors info = new DB.Property_Investors();

                info.investor_name = GetStrData(dataReader, "investor_name");
                info.investor_code1 = GetStrData(dataReader, "investor_code1");
                info.investor_code2 = GetStrData(dataReader, "investor_code2");
                info.number_of_days = float.Parse(dataReader["number_of_days"].ToString());
                
                list.Add(info);
            }
        }

        public List<DB.Property_Investors> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.Property_Investors GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
