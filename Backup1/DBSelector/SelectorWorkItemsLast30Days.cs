﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorWorkItemsLast30Days : DatabaseSelector
    {
        private List<Database.DB.WorkItemsLast30Days> list;

        public SelectorWorkItemsLast30Days()
         {
             list = new List<DB.WorkItemsLast30Days>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.WorkItemsLast30Days info = new DB.WorkItemsLast30Days();

                //info.completed_date = GetDateTimeData(dataReader, "completed_date");
                //info.customer_name = GetStrData(dataReader, "customer_name");
                //info.issued_date = GetDateTimeData(dataReader, "issued_date");
                info.property_code = GetStrData(dataReader, "property_code");
                info.VendorId = GetStrData(dataReader, "VendorId");
                info.Amount = GetDecimal(dataReader, "Amount");
                info.wo_task = GetStrData(dataReader, "wo_task");
                info.work_order_number = GetStrData(dataReader, "work_order_number");
                info.WorkItemStatus = GetStrData(dataReader, "WorkItemStatus");

                list.Add(info);
            }
        }

        public List<DB.WorkItemsLast30Days> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.WorkItemsLast30Days GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
