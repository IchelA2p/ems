﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorEbillWebsites : DatabaseSelector
    {
        private List<Database.DB.EbillWebsites> list;

        public SelectorEbillWebsites()
         {
             list = new List<DB.EbillWebsites>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.EbillWebsites info = new DB.EbillWebsites();
                info.id = int.Parse(dataReader["id"].ToString());
                info.website_name = GetString(dataReader, "website_name");
                info.usp_name = GetString(dataReader, "usp_name");
                info.username = GetString(dataReader, "username");
                info.password = GetString(dataReader, "password");
                info.email = GetString(dataReader, "email");
                info.createdby = GetString(dataReader, "createdby");
                info.updatedby = GetString(dataReader, "updatedby");
                info.date_created = GetDateTime(dataReader, "date_created");
                info.last_date_modified = GetDateTime(dataReader, "last_date_modified");

                list.Add(info);
            }
        }

        public List<DB.EbillWebsites> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.EbillWebsites GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }

        public string GetString(SqlDataReader rd, string columnName)
        {
            try
            {
                return rd[columnName].ToString().Trim();
            }
            catch
            {
                return string.Empty;
            }
        }

        public DateTime GetDateTime(SqlDataReader rd, string columnName)
        {
            try
            {
                return DateTime.Parse(rd[columnName].ToString());        
            }
            catch
            {
                return DateTime.MinValue;
            }
        }
    }
}
