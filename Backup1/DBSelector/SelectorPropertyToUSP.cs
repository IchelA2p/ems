﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorPropertyToUSP : DatabaseSelector
    {
        private List<Database.DB.PropertyToUSP> list;

        public SelectorPropertyToUSP()
        {
             list = new List<DB.PropertyToUSP>();
        }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.PropertyToUSP info = new DB.PropertyToUSP();
                try
                {
                    info.property_code = ((string)dataReader["property_code"]).Trim();
                }
                catch
                { 
                    info.property_code = string.Empty;
                }
                try
                {
                    info.gas_vendor_code = ((string)dataReader["gas_vendor_code"]).Trim();
                }
                catch
                {
                    info.gas_vendor_code = string.Empty;
                }
                try
                {
                    info.electricity_vendor_code = ((string)dataReader["electricity_vendor_code"]).Trim();
                }
                catch
                {
                    info.electricity_vendor_code = string.Empty;
                }
                try
                {
                    info.water_vendor_code = ((string)dataReader["water_vendor_code"]).Trim();
                }
                catch
                {
                    info.water_vendor_code = string.Empty;
                }
                try
                {
                    info.electricity_turnoff_date = (DateTime)dataReader["electricity_turnoff_date"];
                }
                catch
                {
                    info.electricity_turnoff_date = DateTime.MinValue;
                }
                try
                {
                    info.gas_turnoff_date = (DateTime)dataReader["gas_turnoff_date"];
                }
                catch
                {
                    info.gas_turnoff_date = DateTime.MinValue;
                }
                try
                {
                    info.water_turnoff_date = (DateTime)dataReader["water_turnoff_date"];
                }
                catch
                {
                    info.water_turnoff_date = DateTime.MinValue;
                }
                list.Add(info);
            }
        }

        public List<DB.PropertyToUSP> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.PropertyToUSP GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
