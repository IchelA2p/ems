﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorPreviousWI : DatabaseSelector
    {
        private List<Database.DB.PreviousWI> list;

        public SelectorPreviousWI()
         {
             list = new List<DB.PreviousWI>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.PreviousWI info = new DB.PreviousWI();
                info.completed_date = GetDateTimeData(dataReader, "completed_date");
                info.customer_name = GetStrData(dataReader, "customer_name");
                info.issued_date = GetDateTimeData(dataReader, "issued_date");
                info.property_code = GetStrData(dataReader, "property_code");
                info.vendor_code = GetStrData(dataReader, "vendor_code");
                info.vendor_price = GetDecimal(dataReader, "vendor_price");
                info.wo_task = GetStrData(dataReader, "wo_task");
                info.work_order_number = GetStrData(dataReader, "work_order_number");
                info.work_order_status = GetStrData(dataReader, "work_order_status");

                list.Add(info);
            }
        }

        public List<DB.PreviousWI> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.PreviousWI GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
