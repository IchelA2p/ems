﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorRRProperties : DatabaseSelector
    {
        private List<Database.DB.RRProperties> list;

        public SelectorRRProperties()
         {
             list = new List<DB.RRProperties>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.RRProperties info = new DB.RRProperties();

                info.city = GetString(dataReader, "city");
                info.property_id = GetString(dataReader, "property_id");
                info.status = GetString(dataReader, "status");
                info.street_name = GetString(dataReader, "street_name");
                info.street_number = GetString(dataReader, "street_number");
                info.zip = GetString(dataReader, "zip");

                try
                {
                    info.status_date = DateTime.Parse(dataReader["status_date"].ToString());
                }
                catch
                {
                    info.status_date = DateTime.MinValue;
                }

                list.Add(info);
            }
        }

        public List<DB.RRProperties> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.RRProperties GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }

        public string GetString(SqlDataReader rd, string cn)
        {
            try
            {
                return rd[cn].ToString().Trim();
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
