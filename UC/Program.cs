﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Threading;

namespace UC
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            Clipboard.Clear();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Forms.WelcomeScreen());
            Application.Run(new Form1());
        }

        public static Thread CreateLoadingScreen(string message)
        {
            Thread t = new Thread(() => ShowLoadingScreen(message));
            t.Start();
            return t;
        }

        static void ShowLoadingScreen(string message)
        {
            Application.Run(new LoadingScreen(message));
        }

    }
}
