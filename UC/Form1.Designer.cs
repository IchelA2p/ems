﻿namespace UC
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.cPropertyCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFullAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cInvestorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cReosrcDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cReoslcDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cReosfcDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAccountNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPodId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cproperty_status_change_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cinactivedate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.button2 = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.panel_specialinstructions2 = new System.Windows.Forms.Panel();
            this.button13 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.panel_specialInstructions = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label56 = new System.Windows.Forms.Label();
            this.checkBox25 = new System.Windows.Forms.CheckBox();
            this.button14 = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.label51 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label48 = new System.Windows.Forms.Label();
            this.checkBox21 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.textBox_amount = new System.Windows.Forms.TextBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.textBox_accountNumber = new System.Windows.Forms.TextBox();
            this.btn_specialInstructions = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.panel_callout = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.lbl_phoneNumber = new System.Windows.Forms.Label();
            this.lbl_serviceProviderName = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label57 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.cmboBx_UspOnInvoice = new System.Windows.Forms.ComboBox();
            this.lbl_UspOnInvoice = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label13 = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.BW_Load_REO_Properties = new System.ComponentModel.BackgroundWorker();
            this.BW_Load_USP = new System.ComponentModel.BackgroundWorker();
            this.BW_Load_VDR = new System.ComponentModel.BackgroundWorker();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.functionsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eMSDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testModeToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.normalToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.testToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auditFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.forApprovalAmount1000ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.duplicateApprovalFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ndLevelDuplicateInvoiceReviewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.unreadableInvoiceApprovalFormToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.counterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incompleteVIDRequestsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dashboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.funnelReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dailyReportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.qADashboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.auditDashboardToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.invoiceTimelineToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.partialDuplicatesReportRESIToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eMSFunnelBetaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.eMSSLAToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.assignTaskFormToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.skipToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.invoiceInquiryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uSPsForMPDToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.uSPWebsiteInformationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.queueToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.realResolutionToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.button3 = new System.Windows.Forms.Button();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.chkBx_NoPropertyAddress = new System.Windows.Forms.CheckBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.button12 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.BW_LoadTasksCompleted = new System.ComponentModel.BackgroundWorker();
            this.pdfviewer = new System.Windows.Forms.WebBrowser();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.checkBox22 = new System.Windows.Forms.CheckBox();
            this.label53 = new System.Windows.Forms.Label();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.label34 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.button4 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.label38 = new System.Windows.Forms.Label();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.label30 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.button11 = new System.Windows.Forms.Button();
            this.label59 = new System.Windows.Forms.Label();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.btn_AddMorePaymentBreakdown = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.chkbx_noZipCode = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.txtTotal = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.checkBox24 = new System.Windows.Forms.CheckBox();
            this.checkBox23 = new System.Windows.Forms.CheckBox();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.chkTransAmtNo = new System.Windows.Forms.CheckBox();
            this.chkTransAmtYes = new System.Windows.Forms.CheckBox();
            this.label58 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.pnlAddress = new System.Windows.Forms.Panel();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.dgv_usp = new System.Windows.Forms.DataGridView();
            this.vdr_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vdr_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vdr_address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vdr_status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vms_status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button_noMatchUSP = new System.Windows.Forms.Button();
            this.label47 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.btnCancelUpload = new System.Windows.Forms.Button();
            this.label60 = new System.Windows.Forms.Label();
            this.chkASFIYes = new System.Windows.Forms.CheckBox();
            this.chkASFINo = new System.Windows.Forms.CheckBox();
            this.cmbBillName = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.lblAdrsNotif = new System.Windows.Forms.Label();
            this.numBalTransfer = new System.Windows.Forms.NumericUpDown();
            this.label62 = new System.Windows.Forms.Label();
            this.chkBalInfoYes = new System.Windows.Forms.CheckBox();
            this.chkBalInfoNo = new System.Windows.Forms.CheckBox();
            this.label63 = new System.Windows.Forms.Label();
            this.chkBalAmtYes = new System.Windows.Forms.CheckBox();
            this.chkBalAmtNo = new System.Windows.Forms.CheckBox();
            this.lblCount = new System.Windows.Forms.Label();
            this.lblOf = new System.Windows.Forms.Label();
            this.lblTransferCount = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.chknYes = new System.Windows.Forms.CheckBox();
            this.chknNo = new System.Windows.Forms.CheckBox();
            this.label66 = new System.Windows.Forms.Label();
            this.label67 = new System.Windows.Forms.Label();
            this.txtUSPZipcode = new System.Windows.Forms.TextBox();
            this.label68 = new System.Windows.Forms.Label();
            this.btnSearchUSP = new System.Windows.Forms.Button();
            this.txtUSPCity = new System.Windows.Forms.TextBox();
            this.txtUSPState = new System.Windows.Forms.TextBox();
            this.label69 = new System.Windows.Forms.Label();
            this.txtUSPStrt = new System.Windows.Forms.TextBox();
            this.panel_acctNumber = new System.Windows.Forms.Panel();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.panel_specialinstructions2.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.panel_specialInstructions.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.panel_callout.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.pnlAddress.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_usp)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBalTransfer)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(126, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(49, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Zipcode:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(175, 20);
            this.textBox1.MaxLength = 5;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(55, 20);
            this.textBox1.TabIndex = 26;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.zipcodeSearch);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cPropertyCode,
            this.cFullAddress,
            this.cCustomerName,
            this.cActive,
            this.cInvestorName,
            this.cReosrcDate,
            this.cReoslcDate,
            this.cReosfcDate,
            this.cAccountNumber,
            this.cPodId,
            this.cproperty_status_change_date,
            this.cinactivedate});
            this.dataGridView1.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(12, 356);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(652, 88);
            this.dataGridView1.TabIndex = 16;
            this.toolTip1.SetToolTip(this.dataGridView1, "Please select property");
            this.dataGridView1.Visible = false;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // cPropertyCode
            // 
            this.cPropertyCode.HeaderText = "Property ID";
            this.cPropertyCode.Name = "cPropertyCode";
            this.cPropertyCode.ReadOnly = true;
            this.cPropertyCode.Visible = false;
            // 
            // cFullAddress
            // 
            this.cFullAddress.HeaderText = "Full Address";
            this.cFullAddress.Name = "cFullAddress";
            this.cFullAddress.ReadOnly = true;
            // 
            // cCustomerName
            // 
            this.cCustomerName.HeaderText = "Client";
            this.cCustomerName.Name = "cCustomerName";
            this.cCustomerName.ReadOnly = true;
            this.cCustomerName.Visible = false;
            // 
            // cActive
            // 
            this.cActive.HeaderText = "Active";
            this.cActive.Name = "cActive";
            this.cActive.ReadOnly = true;
            this.cActive.Visible = false;
            // 
            // cInvestorName
            // 
            this.cInvestorName.HeaderText = "Investor Name";
            this.cInvestorName.Name = "cInvestorName";
            this.cInvestorName.ReadOnly = true;
            this.cInvestorName.Visible = false;
            // 
            // cReosrcDate
            // 
            this.cReosrcDate.HeaderText = "REOSRC Date";
            this.cReosrcDate.Name = "cReosrcDate";
            this.cReosrcDate.ReadOnly = true;
            this.cReosrcDate.Visible = false;
            // 
            // cReoslcDate
            // 
            this.cReoslcDate.HeaderText = "REOSLC Date";
            this.cReoslcDate.Name = "cReoslcDate";
            this.cReoslcDate.ReadOnly = true;
            this.cReoslcDate.Visible = false;
            // 
            // cReosfcDate
            // 
            this.cReosfcDate.HeaderText = "REOSFC Date";
            this.cReosfcDate.Name = "cReosfcDate";
            this.cReosfcDate.ReadOnly = true;
            this.cReosfcDate.Visible = false;
            // 
            // cAccountNumber
            // 
            this.cAccountNumber.HeaderText = "Account Number";
            this.cAccountNumber.Name = "cAccountNumber";
            this.cAccountNumber.ReadOnly = true;
            // 
            // cPodId
            // 
            this.cPodId.HeaderText = "Pod Id";
            this.cPodId.Name = "cPodId";
            this.cPodId.ReadOnly = true;
            this.cPodId.Visible = false;
            // 
            // cproperty_status_change_date
            // 
            this.cproperty_status_change_date.HeaderText = "Property Status Change Date";
            this.cproperty_status_change_date.Name = "cproperty_status_change_date";
            this.cproperty_status_change_date.ReadOnly = true;
            this.cproperty_status_change_date.Visible = false;
            // 
            // cinactivedate
            // 
            this.cinactivedate.HeaderText = "Inactive Date";
            this.cinactivedate.Name = "cinactivedate";
            this.cinactivedate.ReadOnly = true;
            this.cinactivedate.Visible = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(5, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Due Date:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker1.Location = new System.Drawing.Point(88, 50);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(96, 20);
            this.dateTimePicker1.TabIndex = 40;
            this.dateTimePicker1.Visible = false;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // button2
            // 
            this.button2.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button2.BackColor = System.Drawing.Color.DodgerBlue;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(427, 798);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(58, 31);
            this.button2.TabIndex = 64;
            this.button2.Text = "Save";
            this.toolTip1.SetToolTip(this.button2, "Save the work item created");
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(374, 7);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(35, 13);
            this.label6.TabIndex = 14;
            this.label6.Text = "State:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(189, 7);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(27, 13);
            this.label7.TabIndex = 16;
            this.label7.Text = "City:";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.ForeColor = System.Drawing.Color.Red;
            this.label39.Location = new System.Drawing.Point(813, 108);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(335, 13);
            this.label39.TabIndex = 127;
            this.label39.Text = "Did you find the match? Click on the property or click on search again";
            this.label39.Visible = false;
            // 
            // panel_specialinstructions2
            // 
            this.panel_specialinstructions2.Controls.Add(this.button13);
            this.panel_specialinstructions2.Controls.Add(this.groupBox10);
            this.panel_specialinstructions2.Location = new System.Drawing.Point(69, 489);
            this.panel_specialinstructions2.Name = "panel_specialinstructions2";
            this.panel_specialinstructions2.Size = new System.Drawing.Size(658, 268);
            this.panel_specialinstructions2.TabIndex = 142;
            this.panel_specialinstructions2.Visible = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.DodgerBlue;
            this.button13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Location = new System.Drawing.Point(292, 233);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 36;
            this.button13.Text = "SAVE";
            this.button13.UseVisualStyleBackColor = false;
            this.button13.Click += new System.EventHandler(this.button13_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.textBox33);
            this.groupBox10.Location = new System.Drawing.Point(9, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(637, 221);
            this.groupBox10.TabIndex = 0;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Special Instructions";
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(6, 15);
            this.textBox33.Multiline = true;
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(625, 200);
            this.textBox33.TabIndex = 0;
            // 
            // panel_specialInstructions
            // 
            this.panel_specialInstructions.Controls.Add(this.groupBox2);
            this.panel_specialInstructions.Controls.Add(this.panel_callout);
            this.panel_specialInstructions.Location = new System.Drawing.Point(12, 353);
            this.panel_specialInstructions.Name = "panel_specialInstructions";
            this.panel_specialInstructions.Size = new System.Drawing.Size(775, 446);
            this.panel_specialInstructions.TabIndex = 132;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label56);
            this.groupBox2.Controls.Add(this.checkBox25);
            this.groupBox2.Controls.Add(this.button14);
            this.groupBox2.Controls.Add(this.groupBox3);
            this.groupBox2.Controls.Add(this.btn_specialInstructions);
            this.groupBox2.Controls.Add(this.label29);
            this.groupBox2.Controls.Add(this.label35);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.textBox5);
            this.groupBox2.Controls.Add(this.textBox7);
            this.groupBox2.Controls.Add(this.textBox9);
            this.groupBox2.Controls.Add(this.textBox13);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Controls.Add(this.textBox15);
            this.groupBox2.Location = new System.Drawing.Point(20, 60);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(769, 383);
            this.groupBox2.TabIndex = 24;
            this.groupBox2.TabStop = false;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(658, 24);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(100, 13);
            this.label56.TabIndex = 65;
            this.label56.Text = "1 out of 4 invoice(s)";
            // 
            // checkBox25
            // 
            this.checkBox25.AutoSize = true;
            this.checkBox25.Location = new System.Drawing.Point(14, 360);
            this.checkBox25.Name = "checkBox25";
            this.checkBox25.Size = new System.Drawing.Size(262, 17);
            this.checkBox25.TabIndex = 64;
            this.checkBox25.Text = "Invoice has complete information. Not for Call-Out.";
            this.checkBox25.UseVisualStyleBackColor = true;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.SlateGray;
            this.button14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.Location = new System.Drawing.Point(307, 356);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 63;
            this.button14.Text = "CLEAR";
            this.button14.UseVisualStyleBackColor = false;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox32);
            this.groupBox3.Controls.Add(this.textBox31);
            this.groupBox3.Controls.Add(this.textBox30);
            this.groupBox3.Controls.Add(this.label52);
            this.groupBox3.Controls.Add(this.label51);
            this.groupBox3.Controls.Add(this.label50);
            this.groupBox3.Controls.Add(this.textBox29);
            this.groupBox3.Controls.Add(this.textBox28);
            this.groupBox3.Controls.Add(this.textBox27);
            this.groupBox3.Controls.Add(this.textBox26);
            this.groupBox3.Controls.Add(this.textBox25);
            this.groupBox3.Controls.Add(this.textBox24);
            this.groupBox3.Controls.Add(this.textBox23);
            this.groupBox3.Controls.Add(this.textBox22);
            this.groupBox3.Controls.Add(this.textBox21);
            this.groupBox3.Controls.Add(this.textBox20);
            this.groupBox3.Controls.Add(this.textBox18);
            this.groupBox3.Controls.Add(this.textBox3);
            this.groupBox3.Controls.Add(this.label49);
            this.groupBox3.Controls.Add(this.label48);
            this.groupBox3.Controls.Add(this.checkBox21);
            this.groupBox3.Controls.Add(this.checkBox20);
            this.groupBox3.Controls.Add(this.checkBox19);
            this.groupBox3.Controls.Add(this.textBox_amount);
            this.groupBox3.Controls.Add(this.checkBox18);
            this.groupBox3.Controls.Add(this.checkBox17);
            this.groupBox3.Controls.Add(this.textBox_accountNumber);
            this.groupBox3.Location = new System.Drawing.Point(93, 184);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(582, 170);
            this.groupBox3.TabIndex = 62;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Confirmed Detail";
            // 
            // textBox32
            // 
            this.textBox32.BackColor = System.Drawing.Color.White;
            this.textBox32.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox32.Enabled = false;
            this.textBox32.Location = new System.Drawing.Point(202, 139);
            this.textBox32.MaxLength = 200;
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(45, 20);
            this.textBox32.TabIndex = 92;
            // 
            // textBox31
            // 
            this.textBox31.BackColor = System.Drawing.Color.White;
            this.textBox31.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox31.Enabled = false;
            this.textBox31.Location = new System.Drawing.Point(202, 113);
            this.textBox31.MaxLength = 200;
            this.textBox31.Name = "textBox31";
            this.textBox31.Size = new System.Drawing.Size(45, 20);
            this.textBox31.TabIndex = 91;
            // 
            // textBox30
            // 
            this.textBox30.BackColor = System.Drawing.Color.White;
            this.textBox30.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox30.Enabled = false;
            this.textBox30.Location = new System.Drawing.Point(202, 87);
            this.textBox30.MaxLength = 200;
            this.textBox30.Name = "textBox30";
            this.textBox30.Size = new System.Drawing.Size(45, 20);
            this.textBox30.TabIndex = 90;
            this.textBox30.TextChanged += new System.EventHandler(this.textBox30_TextChanged);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(495, 71);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(46, 13);
            this.label52.TabIndex = 89;
            this.label52.Text = "Zipcode";
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(452, 71);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(32, 13);
            this.label51.TabIndex = 88;
            this.label51.Text = "State";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(380, 71);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(24, 13);
            this.label50.TabIndex = 87;
            this.label50.Text = "City";
            // 
            // textBox29
            // 
            this.textBox29.BackColor = System.Drawing.Color.White;
            this.textBox29.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox29.Enabled = false;
            this.textBox29.Location = new System.Drawing.Point(491, 139);
            this.textBox29.MaxLength = 5;
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(55, 20);
            this.textBox29.TabIndex = 86;
            this.textBox29.TextChanged += new System.EventHandler(this.textBox29_TextChanged);
            // 
            // textBox28
            // 
            this.textBox28.BackColor = System.Drawing.Color.White;
            this.textBox28.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox28.Enabled = false;
            this.textBox28.Location = new System.Drawing.Point(491, 113);
            this.textBox28.MaxLength = 5;
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(55, 20);
            this.textBox28.TabIndex = 85;
            this.textBox28.TextChanged += new System.EventHandler(this.textBox28_TextChanged);
            // 
            // textBox27
            // 
            this.textBox27.BackColor = System.Drawing.Color.White;
            this.textBox27.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox27.Enabled = false;
            this.textBox27.Location = new System.Drawing.Point(491, 87);
            this.textBox27.MaxLength = 5;
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(55, 20);
            this.textBox27.TabIndex = 84;
            this.textBox27.TextChanged += new System.EventHandler(this.textBox27_TextChanged);
            // 
            // textBox26
            // 
            this.textBox26.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox26.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox26.Enabled = false;
            this.textBox26.Location = new System.Drawing.Point(451, 139);
            this.textBox26.MaxLength = 2;
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(34, 20);
            this.textBox26.TabIndex = 83;
            this.textBox26.Leave += new System.EventHandler(this.textBox26_Leave);
            // 
            // textBox25
            // 
            this.textBox25.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox25.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox25.Enabled = false;
            this.textBox25.Location = new System.Drawing.Point(451, 113);
            this.textBox25.MaxLength = 2;
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(34, 20);
            this.textBox25.TabIndex = 82;
            this.textBox25.Leave += new System.EventHandler(this.textBox25_Leave);
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox24.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox24.Enabled = false;
            this.textBox24.Location = new System.Drawing.Point(451, 87);
            this.textBox24.MaxLength = 2;
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(34, 20);
            this.textBox24.TabIndex = 81;
            this.textBox24.TextChanged += new System.EventHandler(this.textBox24_TextChanged);
            this.textBox24.Leave += new System.EventHandler(this.textBox24_Leave);
            // 
            // textBox23
            // 
            this.textBox23.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox23.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox23.Enabled = false;
            this.textBox23.Location = new System.Drawing.Point(346, 139);
            this.textBox23.MaxLength = 200;
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(99, 20);
            this.textBox23.TabIndex = 80;
            // 
            // textBox22
            // 
            this.textBox22.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox22.Enabled = false;
            this.textBox22.Location = new System.Drawing.Point(346, 113);
            this.textBox22.MaxLength = 200;
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(99, 20);
            this.textBox22.TabIndex = 79;
            // 
            // textBox21
            // 
            this.textBox21.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.textBox21.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBox21.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox21.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox21.Enabled = false;
            this.textBox21.Location = new System.Drawing.Point(346, 87);
            this.textBox21.MaxLength = 200;
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(99, 20);
            this.textBox21.TabIndex = 78;
            this.textBox21.TextChanged += new System.EventHandler(this.textBox21_TextChanged);
            // 
            // textBox20
            // 
            this.textBox20.BackColor = System.Drawing.Color.White;
            this.textBox20.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox20.Enabled = false;
            this.textBox20.Location = new System.Drawing.Point(253, 139);
            this.textBox20.MaxLength = 200;
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(87, 20);
            this.textBox20.TabIndex = 77;
            // 
            // textBox18
            // 
            this.textBox18.BackColor = System.Drawing.Color.White;
            this.textBox18.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox18.Enabled = false;
            this.textBox18.Location = new System.Drawing.Point(253, 113);
            this.textBox18.MaxLength = 200;
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(87, 20);
            this.textBox18.TabIndex = 76;
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.White;
            this.textBox3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(253, 87);
            this.textBox3.MaxLength = 200;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(87, 20);
            this.textBox3.TabIndex = 75;
            this.textBox3.TextChanged += new System.EventHandler(this.textBox3_TextChanged);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(273, 71);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(35, 13);
            this.label49.TabIndex = 74;
            this.label49.Text = "Street";
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(200, 71);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(48, 13);
            this.label48.TabIndex = 73;
            this.label48.Text = "House #";
            // 
            // checkBox21
            // 
            this.checkBox21.AutoSize = true;
            this.checkBox21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox21.Location = new System.Drawing.Point(43, 140);
            this.checkBox21.Name = "checkBox21";
            this.checkBox21.Size = new System.Drawing.Size(131, 17);
            this.checkBox21.TabIndex = 72;
            this.checkBox21.Text = "USP Physical Address";
            this.checkBox21.UseVisualStyleBackColor = true;
            this.checkBox21.CheckedChanged += new System.EventHandler(this.checkBox21_CheckedChanged);
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox20.Location = new System.Drawing.Point(43, 114);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(142, 17);
            this.checkBox20.TabIndex = 70;
            this.checkBox20.Text = "USP Address on Invoice";
            this.checkBox20.UseVisualStyleBackColor = true;
            this.checkBox20.CheckedChanged += new System.EventHandler(this.checkBox20_CheckedChanged);
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox19.Location = new System.Drawing.Point(43, 88);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(106, 17);
            this.checkBox19.TabIndex = 68;
            this.checkBox19.Text = "Property Address";
            this.checkBox19.UseVisualStyleBackColor = true;
            this.checkBox19.CheckedChanged += new System.EventHandler(this.checkBox19_CheckedChanged);
            // 
            // textBox_amount
            // 
            this.textBox_amount.BackColor = System.Drawing.Color.White;
            this.textBox_amount.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_amount.Enabled = false;
            this.textBox_amount.Location = new System.Drawing.Point(202, 44);
            this.textBox_amount.MaxLength = 25;
            this.textBox_amount.Name = "textBox_amount";
            this.textBox_amount.Size = new System.Drawing.Size(188, 20);
            this.textBox_amount.TabIndex = 66;
            this.textBox_amount.TextChanged += new System.EventHandler(this.textBox_amount_TextChanged);
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox18.Location = new System.Drawing.Point(43, 44);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(62, 17);
            this.checkBox18.TabIndex = 65;
            this.checkBox18.Text = "Amount";
            this.checkBox18.UseVisualStyleBackColor = true;
            this.checkBox18.CheckedChanged += new System.EventHandler(this.checkBox18_CheckedChanged);
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox17.Location = new System.Drawing.Point(43, 20);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(106, 17);
            this.checkBox17.TabIndex = 64;
            this.checkBox17.Text = "Account Number";
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.CheckedChanged += new System.EventHandler(this.checkBox17_CheckedChanged);
            // 
            // textBox_accountNumber
            // 
            this.textBox_accountNumber.BackColor = System.Drawing.Color.White;
            this.textBox_accountNumber.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_accountNumber.Enabled = false;
            this.textBox_accountNumber.Location = new System.Drawing.Point(202, 19);
            this.textBox_accountNumber.MaxLength = 25;
            this.textBox_accountNumber.Name = "textBox_accountNumber";
            this.textBox_accountNumber.Size = new System.Drawing.Size(188, 20);
            this.textBox_accountNumber.TabIndex = 62;
            this.textBox_accountNumber.TextChanged += new System.EventHandler(this.textBox_accountNumber_TextChanged);
            // 
            // btn_specialInstructions
            // 
            this.btn_specialInstructions.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_specialInstructions.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btn_specialInstructions.ForeColor = System.Drawing.Color.White;
            this.btn_specialInstructions.Location = new System.Drawing.Point(386, 356);
            this.btn_specialInstructions.Name = "btn_specialInstructions";
            this.btn_specialInstructions.Size = new System.Drawing.Size(75, 23);
            this.btn_specialInstructions.TabIndex = 35;
            this.btn_specialInstructions.Text = "SAVE";
            this.btn_specialInstructions.UseVisualStyleBackColor = false;
            this.btn_specialInstructions.TextChanged += new System.EventHandler(this.btn_specialInstructions_Click);
            this.btn_specialInstructions.Click += new System.EventHandler(this.btn_specialInstructions_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(218, 160);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(60, 13);
            this.label29.TabIndex = 33;
            this.label29.Text = "Spoke with";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(227, 134);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(51, 13);
            this.label35.TabIndex = 32;
            this.label35.Text = "Called for";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(200, 108);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(78, 13);
            this.label40.TabIndex = 31;
            this.label40.Text = "Phone Number";
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.White;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox5.Location = new System.Drawing.Point(295, 158);
            this.textBox5.MaxLength = 15;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(188, 20);
            this.textBox5.TabIndex = 29;
            this.textBox5.TextChanged += new System.EventHandler(this.textBox5_TextChanged);
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox7.Location = new System.Drawing.Point(295, 132);
            this.textBox7.Name = "textBox7";
            this.textBox7.ReadOnly = true;
            this.textBox7.Size = new System.Drawing.Size(188, 20);
            this.textBox7.TabIndex = 28;
            // 
            // textBox9
            // 
            this.textBox9.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox9.Location = new System.Drawing.Point(295, 106);
            this.textBox9.Name = "textBox9";
            this.textBox9.ReadOnly = true;
            this.textBox9.Size = new System.Drawing.Size(188, 20);
            this.textBox9.TabIndex = 27;
            // 
            // textBox13
            // 
            this.textBox13.BackColor = System.Drawing.Color.Gainsboro;
            this.textBox13.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox13.Location = new System.Drawing.Point(295, 80);
            this.textBox13.Name = "textBox13";
            this.textBox13.ReadOnly = true;
            this.textBox13.Size = new System.Drawing.Size(188, 20);
            this.textBox13.TabIndex = 26;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(155, 83);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(123, 13);
            this.label41.TabIndex = 25;
            this.label41.Text = "Service Provider\'s Name";
            // 
            // textBox15
            // 
            this.textBox15.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox15.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox15.Location = new System.Drawing.Point(183, 12);
            this.textBox15.Multiline = true;
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(402, 68);
            this.textBox15.TabIndex = 24;
            this.textBox15.Text = "Hi! This is (State your name), I\'m the property manager for Altisource Single Fam" +
    "ily. I need to find out some information on the invoice we received from your co" +
    "mpany.";
            this.textBox15.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // panel_callout
            // 
            this.panel_callout.Controls.Add(this.button1);
            this.panel_callout.Controls.Add(this.lbl_phoneNumber);
            this.panel_callout.Controls.Add(this.lbl_serviceProviderName);
            this.panel_callout.Controls.Add(this.label27);
            this.panel_callout.Controls.Add(this.label57);
            this.panel_callout.Controls.Add(this.label8);
            this.panel_callout.Controls.Add(this.label21);
            this.panel_callout.Controls.Add(this.label20);
            this.panel_callout.Location = new System.Drawing.Point(395, 3);
            this.panel_callout.Name = "panel_callout";
            this.panel_callout.Size = new System.Drawing.Size(377, 57);
            this.panel_callout.TabIndex = 6;
            this.panel_callout.Visible = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.DodgerBlue;
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Location = new System.Drawing.Point(281, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(89, 51);
            this.button1.TabIndex = 128;
            this.button1.Text = "Show the call out information and script";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbl_phoneNumber
            // 
            this.lbl_phoneNumber.AutoSize = true;
            this.lbl_phoneNumber.Location = new System.Drawing.Point(139, 41);
            this.lbl_phoneNumber.Name = "lbl_phoneNumber";
            this.lbl_phoneNumber.Size = new System.Drawing.Size(78, 13);
            this.lbl_phoneNumber.TabIndex = 127;
            this.lbl_phoneNumber.Text = "Phone Number";
            // 
            // lbl_serviceProviderName
            // 
            this.lbl_serviceProviderName.AutoSize = true;
            this.lbl_serviceProviderName.Location = new System.Drawing.Point(139, 24);
            this.lbl_serviceProviderName.Name = "lbl_serviceProviderName";
            this.lbl_serviceProviderName.Size = new System.Drawing.Size(123, 13);
            this.lbl_serviceProviderName.TabIndex = 126;
            this.lbl_serviceProviderName.Text = "Service Provider\'s Name";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(49, 41);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(81, 13);
            this.label27.TabIndex = 125;
            this.label27.Text = "Phone Number:";
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(359, 8);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(13, 13);
            this.label57.TabIndex = 143;
            this.label57.Text = "0";
            this.label57.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(4, 24);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 13);
            this.label8.TabIndex = 124;
            this.label8.Text = "Service Provider\'s Name:";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(139, 3);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(111, 17);
            this.label21.TabIndex = 1;
            this.label21.Text = "Call Out Reason";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(16, 3);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(115, 17);
            this.label20.TabIndex = 0;
            this.label20.Text = "Call Out Reason:";
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(112, 251);
            this.textBox10.MaxLength = 25;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(172, 20);
            this.textBox10.TabIndex = 23;
            this.textBox10.Visible = false;
            this.textBox10.TextChanged += new System.EventHandler(this.textBox10_TextChanged);
            this.textBox10.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.triggerSearchButton);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(15, 254);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(90, 13);
            this.label14.TabIndex = 33;
            this.label14.Text = "Account Number:";
            this.label14.Visible = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.DodgerBlue;
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(502, 326);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(84, 23);
            this.button9.TabIndex = 32;
            this.button9.Text = "Reset / Clear";
            this.toolTip1.SetToolTip(this.button9, "Resets the selected property");
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Visible = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.DodgerBlue;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(502, 301);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(84, 23);
            this.button7.TabIndex = 31;
            this.button7.Text = "Search";
            this.toolTip1.SetToolTip(this.button7, "Search for properties");
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Visible = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // textBox14
            // 
            this.textBox14.Location = new System.Drawing.Point(53, 5);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(101, 20);
            this.textBox14.TabIndex = 28;
            this.textBox14.TextChanged += new System.EventHandler(this.textBox14_TextChanged);
            this.textBox14.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.triggerSearch);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(2, 8);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(38, 13);
            this.label5.TabIndex = 21;
            this.label5.Text = "Street:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(56, 20);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(46, 20);
            this.textBox4.TabIndex = 25;
            this.textBox4.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ClearTextbox);
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            this.textBox4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.triggerSearch);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(5, 23);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(51, 13);
            this.label26.TabIndex = 19;
            this.label26.Text = "House #:";
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox8.Location = new System.Drawing.Point(676, 459);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(101, 30);
            this.checkBox8.TabIndex = 36;
            this.checkBox8.Text = "no usp address \r\non invoice";
            this.toolTip1.SetToolTip(this.checkBox8, "Select if there is no written USP on invoice");
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.Visible = false;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // cmboBx_UspOnInvoice
            // 
            this.cmboBx_UspOnInvoice.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.cmboBx_UspOnInvoice.FormattingEnabled = true;
            this.cmboBx_UspOnInvoice.Location = new System.Drawing.Point(502, 251);
            this.cmboBx_UspOnInvoice.Name = "cmboBx_UspOnInvoice";
            this.cmboBx_UspOnInvoice.Size = new System.Drawing.Size(286, 21);
            this.cmboBx_UspOnInvoice.TabIndex = 14;
            this.cmboBx_UspOnInvoice.Visible = false;
            this.cmboBx_UspOnInvoice.SelectedIndexChanged += new System.EventHandler(this.cmboBx_UspOnInvoice_SelectedIndexChanged);
            this.cmboBx_UspOnInvoice.TextChanged += new System.EventHandler(this.comboBox4_TextChanged);
            // 
            // lbl_UspOnInvoice
            // 
            this.lbl_UspOnInvoice.AutoSize = true;
            this.lbl_UspOnInvoice.Location = new System.Drawing.Point(499, 232);
            this.lbl_UspOnInvoice.Name = "lbl_UspOnInvoice";
            this.lbl_UspOnInvoice.Size = new System.Drawing.Size(85, 13);
            this.lbl_UspOnInvoice.TabIndex = 11;
            this.lbl_UspOnInvoice.Text = "USP on Invoice:";
            this.lbl_UspOnInvoice.Visible = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(4, 78);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(72, 13);
            this.label12.TabIndex = 27;
            this.label12.Text = "Service From:";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker2.Location = new System.Drawing.Point(87, 75);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(97, 20);
            this.dateTimePicker2.TabIndex = 42;
            this.dateTimePicker2.Visible = false;
            this.dateTimePicker2.ValueChanged += new System.EventHandler(this.dateTimePicker2_ValueChanged);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(4, 103);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(62, 13);
            this.label13.TabIndex = 29;
            this.label13.Text = "Service To:";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker3.Location = new System.Drawing.Point(87, 100);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(97, 20);
            this.dateTimePicker3.TabIndex = 44;
            this.dateTimePicker3.Visible = false;
            this.dateTimePicker3.ValueChanged += new System.EventHandler(this.dateTimePicker3_ValueChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.SteelBlue;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.functionsToolStripMenuItem,
            this.auditToolStripMenuItem,
            this.dashboardToolStripMenuItem,
            this.toolsToolStripMenuItem,
            this.queueToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1584, 24);
            this.menuStrip1.TabIndex = 62;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // functionsToolStripMenuItem
            // 
            this.functionsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.eMSDataToolStripMenuItem,
            this.testModeToolStripMenuItem});
            this.functionsToolStripMenuItem.Name = "functionsToolStripMenuItem";
            this.functionsToolStripMenuItem.Size = new System.Drawing.Size(92, 20);
            this.functionsToolStripMenuItem.Text = "Administrator";
            this.functionsToolStripMenuItem.Click += new System.EventHandler(this.functionsToolStripMenuItem_Click);
            // 
            // eMSDataToolStripMenuItem
            // 
            this.eMSDataToolStripMenuItem.Name = "eMSDataToolStripMenuItem";
            this.eMSDataToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.eMSDataToolStripMenuItem.Text = "EMS data";
            this.eMSDataToolStripMenuItem.Visible = false;
            this.eMSDataToolStripMenuItem.Click += new System.EventHandler(this.eMSDataToolStripMenuItem_Click);
            // 
            // testModeToolStripMenuItem
            // 
            this.testModeToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.normalToolStripMenuItem,
            this.testToolStripMenuItem});
            this.testModeToolStripMenuItem.Name = "testModeToolStripMenuItem";
            this.testModeToolStripMenuItem.Size = new System.Drawing.Size(129, 22);
            this.testModeToolStripMenuItem.Text = "Test Mode";
            this.testModeToolStripMenuItem.Visible = false;
            // 
            // normalToolStripMenuItem
            // 
            this.normalToolStripMenuItem.Name = "normalToolStripMenuItem";
            this.normalToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.normalToolStripMenuItem.Text = "Normal";
            this.normalToolStripMenuItem.Click += new System.EventHandler(this.normalToolStripMenuItem_Click);
            // 
            // testToolStripMenuItem
            // 
            this.testToolStripMenuItem.Name = "testToolStripMenuItem";
            this.testToolStripMenuItem.Size = new System.Drawing.Size(114, 22);
            this.testToolStripMenuItem.Text = "Test ";
            this.testToolStripMenuItem.Click += new System.EventHandler(this.testToolStripMenuItem_Click);
            // 
            // auditToolStripMenuItem
            // 
            this.auditToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.auditFormToolStripMenuItem,
            this.forApprovalAmount1000ToolStripMenuItem,
            this.duplicateApprovalFormToolStripMenuItem,
            this.ndLevelDuplicateInvoiceReviewToolStripMenuItem,
            this.unreadableInvoiceApprovalFormToolStripMenuItem,
            this.counterToolStripMenuItem,
            this.incompleteVIDRequestsToolStripMenuItem});
            this.auditToolStripMenuItem.Name = "auditToolStripMenuItem";
            this.auditToolStripMenuItem.Size = new System.Drawing.Size(48, 20);
            this.auditToolStripMenuItem.Text = "Audit";
            // 
            // auditFormToolStripMenuItem
            // 
            this.auditFormToolStripMenuItem.Name = "auditFormToolStripMenuItem";
            this.auditFormToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.auditFormToolStripMenuItem.Text = "Audit Invoice";
            this.auditFormToolStripMenuItem.Click += new System.EventHandler(this.auditFormToolStripMenuItem_Click);
            // 
            // forApprovalAmount1000ToolStripMenuItem
            // 
            this.forApprovalAmount1000ToolStripMenuItem.Name = "forApprovalAmount1000ToolStripMenuItem";
            this.forApprovalAmount1000ToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.forApprovalAmount1000ToolStripMenuItem.Text = "Invoices for Review";
            this.forApprovalAmount1000ToolStripMenuItem.Click += new System.EventHandler(this.forApprovalAmount1000ToolStripMenuItem_Click);
            // 
            // duplicateApprovalFormToolStripMenuItem
            // 
            this.duplicateApprovalFormToolStripMenuItem.Name = "duplicateApprovalFormToolStripMenuItem";
            this.duplicateApprovalFormToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.duplicateApprovalFormToolStripMenuItem.Text = "Duplicate Invoice 1st Review";
            this.duplicateApprovalFormToolStripMenuItem.Click += new System.EventHandler(this.duplicateApprovalFormToolStripMenuItem_Click);
            // 
            // ndLevelDuplicateInvoiceReviewToolStripMenuItem
            // 
            this.ndLevelDuplicateInvoiceReviewToolStripMenuItem.Name = "ndLevelDuplicateInvoiceReviewToolStripMenuItem";
            this.ndLevelDuplicateInvoiceReviewToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.ndLevelDuplicateInvoiceReviewToolStripMenuItem.Text = "Pre-work Item Creation Review";
            this.ndLevelDuplicateInvoiceReviewToolStripMenuItem.Click += new System.EventHandler(this.ndLevelDuplicateInvoiceReviewToolStripMenuItem_Click);
            // 
            // unreadableInvoiceApprovalFormToolStripMenuItem
            // 
            this.unreadableInvoiceApprovalFormToolStripMenuItem.Name = "unreadableInvoiceApprovalFormToolStripMenuItem";
            this.unreadableInvoiceApprovalFormToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.unreadableInvoiceApprovalFormToolStripMenuItem.Text = "Unreadable Invoice Approval Form";
            this.unreadableInvoiceApprovalFormToolStripMenuItem.Click += new System.EventHandler(this.unreadableInvoiceApprovalFormToolStripMenuItem_Click);
            // 
            // counterToolStripMenuItem
            // 
            this.counterToolStripMenuItem.Name = "counterToolStripMenuItem";
            this.counterToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.counterToolStripMenuItem.Text = "Counter";
            this.counterToolStripMenuItem.Visible = false;
            this.counterToolStripMenuItem.Click += new System.EventHandler(this.counterToolStripMenuItem_Click);
            // 
            // incompleteVIDRequestsToolStripMenuItem
            // 
            this.incompleteVIDRequestsToolStripMenuItem.Name = "incompleteVIDRequestsToolStripMenuItem";
            this.incompleteVIDRequestsToolStripMenuItem.Size = new System.Drawing.Size(257, 22);
            this.incompleteVIDRequestsToolStripMenuItem.Text = "Incomplete VID Requests";
            this.incompleteVIDRequestsToolStripMenuItem.Visible = false;
            this.incompleteVIDRequestsToolStripMenuItem.Click += new System.EventHandler(this.incompleteVIDRequestsToolStripMenuItem_Click);
            // 
            // dashboardToolStripMenuItem
            // 
            this.dashboardToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.funnelReportToolStripMenuItem,
            this.dailyReportToolStripMenuItem,
            this.qADashboardToolStripMenuItem,
            this.auditDashboardToolStripMenuItem,
            this.invoiceTimelineToolStripMenuItem,
            this.partialDuplicatesReportRESIToolStripMenuItem,
            this.eMSFunnelBetaToolStripMenuItem,
            this.eMSSLAToolStripMenuItem});
            this.dashboardToolStripMenuItem.Name = "dashboardToolStripMenuItem";
            this.dashboardToolStripMenuItem.Size = new System.Drawing.Size(76, 20);
            this.dashboardToolStripMenuItem.Text = "Dashboard";
            // 
            // funnelReportToolStripMenuItem
            // 
            this.funnelReportToolStripMenuItem.Name = "funnelReportToolStripMenuItem";
            this.funnelReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.funnelReportToolStripMenuItem.Text = "Funnel Report";
            this.funnelReportToolStripMenuItem.Visible = false;
            this.funnelReportToolStripMenuItem.Click += new System.EventHandler(this.funnelReportToolStripMenuItem_Click);
            // 
            // dailyReportToolStripMenuItem
            // 
            this.dailyReportToolStripMenuItem.Name = "dailyReportToolStripMenuItem";
            this.dailyReportToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.dailyReportToolStripMenuItem.Text = "Productivity Report";
            this.dailyReportToolStripMenuItem.Click += new System.EventHandler(this.dailyReportToolStripMenuItem_Click);
            // 
            // qADashboardToolStripMenuItem
            // 
            this.qADashboardToolStripMenuItem.Name = "qADashboardToolStripMenuItem";
            this.qADashboardToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.qADashboardToolStripMenuItem.Text = "QA Dashboard";
            this.qADashboardToolStripMenuItem.Click += new System.EventHandler(this.qADashboardToolStripMenuItem_Click);
            // 
            // auditDashboardToolStripMenuItem
            // 
            this.auditDashboardToolStripMenuItem.Name = "auditDashboardToolStripMenuItem";
            this.auditDashboardToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.auditDashboardToolStripMenuItem.Text = "Audit Dashboard";
            this.auditDashboardToolStripMenuItem.Visible = false;
            this.auditDashboardToolStripMenuItem.Click += new System.EventHandler(this.auditDashboardToolStripMenuItem_Click);
            // 
            // invoiceTimelineToolStripMenuItem
            // 
            this.invoiceTimelineToolStripMenuItem.Name = "invoiceTimelineToolStripMenuItem";
            this.invoiceTimelineToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.invoiceTimelineToolStripMenuItem.Text = "Invoice Timeline Report";
            this.invoiceTimelineToolStripMenuItem.Click += new System.EventHandler(this.invoiceTimelineToolStripMenuItem_Click);
            // 
            // partialDuplicatesReportRESIToolStripMenuItem
            // 
            this.partialDuplicatesReportRESIToolStripMenuItem.Name = "partialDuplicatesReportRESIToolStripMenuItem";
            this.partialDuplicatesReportRESIToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.partialDuplicatesReportRESIToolStripMenuItem.Text = "Partial Duplicates Report (RESI)";
            this.partialDuplicatesReportRESIToolStripMenuItem.Visible = false;
            this.partialDuplicatesReportRESIToolStripMenuItem.Click += new System.EventHandler(this.partialDuplicatesReportRESIToolStripMenuItem_Click);
            // 
            // eMSFunnelBetaToolStripMenuItem
            // 
            this.eMSFunnelBetaToolStripMenuItem.Name = "eMSFunnelBetaToolStripMenuItem";
            this.eMSFunnelBetaToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.eMSFunnelBetaToolStripMenuItem.Text = "EMS Funnel (Beta)";
            this.eMSFunnelBetaToolStripMenuItem.Click += new System.EventHandler(this.eMSFunnelBetaToolStripMenuItem_Click);
            // 
            // eMSSLAToolStripMenuItem
            // 
            this.eMSSLAToolStripMenuItem.Name = "eMSSLAToolStripMenuItem";
            this.eMSSLAToolStripMenuItem.Size = new System.Drawing.Size(236, 22);
            this.eMSSLAToolStripMenuItem.Text = "EMS SLA";
            this.eMSSLAToolStripMenuItem.Click += new System.EventHandler(this.eMSSLAToolStripMenuItem_Click);
            // 
            // toolsToolStripMenuItem
            // 
            this.toolsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.assignTaskFormToolStripMenuItem1,
            this.skipToolStripMenuItem1,
            this.invoiceInquiryToolStripMenuItem,
            this.uSPsForMPDToolStripMenuItem,
            this.uSPWebsiteInformationToolStripMenuItem});
            this.toolsToolStripMenuItem.Name = "toolsToolStripMenuItem";
            this.toolsToolStripMenuItem.Size = new System.Drawing.Size(47, 20);
            this.toolsToolStripMenuItem.Text = "Tools";
            // 
            // assignTaskFormToolStripMenuItem1
            // 
            this.assignTaskFormToolStripMenuItem1.Name = "assignTaskFormToolStripMenuItem1";
            this.assignTaskFormToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.assignTaskFormToolStripMenuItem1.Text = "Assign Task Form";
            this.assignTaskFormToolStripMenuItem1.Click += new System.EventHandler(this.assignTaskFormToolStripMenuItem1_Click);
            // 
            // skipToolStripMenuItem1
            // 
            this.skipToolStripMenuItem1.Name = "skipToolStripMenuItem1";
            this.skipToolStripMenuItem1.Size = new System.Drawing.Size(206, 22);
            this.skipToolStripMenuItem1.Text = "Skip Invoice";
            this.skipToolStripMenuItem1.Click += new System.EventHandler(this.skipToolStripMenuItem1_Click);
            // 
            // invoiceInquiryToolStripMenuItem
            // 
            this.invoiceInquiryToolStripMenuItem.Name = "invoiceInquiryToolStripMenuItem";
            this.invoiceInquiryToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.invoiceInquiryToolStripMenuItem.Text = "Invoice Inquiry";
            this.invoiceInquiryToolStripMenuItem.Click += new System.EventHandler(this.invoiceInquiryToolStripMenuItem_Click);
            // 
            // uSPsForMPDToolStripMenuItem
            // 
            this.uSPsForMPDToolStripMenuItem.Name = "uSPsForMPDToolStripMenuItem";
            this.uSPsForMPDToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.uSPsForMPDToolStripMenuItem.Text = "USPs for MPD";
            this.uSPsForMPDToolStripMenuItem.Click += new System.EventHandler(this.uSPsForMPDToolStripMenuItem_Click);
            // 
            // uSPWebsiteInformationToolStripMenuItem
            // 
            this.uSPWebsiteInformationToolStripMenuItem.Name = "uSPWebsiteInformationToolStripMenuItem";
            this.uSPWebsiteInformationToolStripMenuItem.Size = new System.Drawing.Size(206, 22);
            this.uSPWebsiteInformationToolStripMenuItem.Text = "USP Website Information";
            this.uSPWebsiteInformationToolStripMenuItem.Click += new System.EventHandler(this.uSPWebsiteInformationToolStripMenuItem_Click);
            // 
            // queueToolStripMenuItem
            // 
            this.queueToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.realResolutionToolStripMenuItem});
            this.queueToolStripMenuItem.Name = "queueToolStripMenuItem";
            this.queueToolStripMenuItem.Size = new System.Drawing.Size(54, 20);
            this.queueToolStripMenuItem.Text = "Queue";
            // 
            // realResolutionToolStripMenuItem
            // 
            this.realResolutionToolStripMenuItem.Name = "realResolutionToolStripMenuItem";
            this.realResolutionToolStripMenuItem.Size = new System.Drawing.Size(155, 22);
            this.realResolutionToolStripMenuItem.Text = "Real Resolution";
            this.realResolutionToolStripMenuItem.Visible = false;
            this.realResolutionToolStripMenuItem.Click += new System.EventHandler(this.realResolutionToolStripMenuItem_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 200;
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ReshowDelay = 100;
            // 
            // button3
            // 
            this.button3.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.button3.BackColor = System.Drawing.Color.DodgerBlue;
            this.button3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Location = new System.Drawing.Point(339, 798);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(81, 30);
            this.button3.TabIndex = 63;
            this.button3.Text = "Save and Exit";
            this.toolTip1.SetToolTip(this.button3, "Save the work item created");
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox6.Location = new System.Drawing.Point(323, 253);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(150, 17);
            this.checkBox6.TabIndex = 24;
            this.checkBox6.Text = "no account no. on invoice";
            this.toolTip1.SetToolTip(this.checkBox6, "Select if there is no account number written on invoice");
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.Visible = false;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // chkBx_NoPropertyAddress
            // 
            this.chkBx_NoPropertyAddress.AutoSize = true;
            this.chkBx_NoPropertyAddress.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkBx_NoPropertyAddress.Location = new System.Drawing.Point(502, 278);
            this.chkBx_NoPropertyAddress.Name = "chkBx_NoPropertyAddress";
            this.chkBx_NoPropertyAddress.Size = new System.Drawing.Size(171, 17);
            this.chkBx_NoPropertyAddress.TabIndex = 12;
            this.chkBx_NoPropertyAddress.Text = "no property address on invoice";
            this.toolTip1.SetToolTip(this.chkBx_NoPropertyAddress, "Select if there is no property address written on invoice");
            this.chkBx_NoPropertyAddress.UseVisualStyleBackColor = true;
            this.chkBx_NoPropertyAddress.Visible = false;
            this.chkBx_NoPropertyAddress.CheckedChanged += new System.EventHandler(this.chkBx_NoPropertyAddress_CheckedChanged);
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.LightSteelBlue;
            this.textBox11.Location = new System.Drawing.Point(193, 110);
            this.textBox11.Name = "textBox11";
            this.textBox11.ReadOnly = true;
            this.textBox11.Size = new System.Drawing.Size(106, 20);
            this.textBox11.TabIndex = 57;
            this.toolTip1.SetToolTip(this.textBox11, "Check if it matches the Total amount on invoice");
            this.textBox11.Visible = false;
            this.textBox11.TextChanged += new System.EventHandler(this.textBox11_TextChanged);
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(193, 133);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(106, 20);
            this.textBox12.TabIndex = 59;
            this.toolTip1.SetToolTip(this.textBox12, "Total amount if you pay after due date");
            this.textBox12.Visible = false;
            this.textBox12.TextChanged += new System.EventHandler(this.textBox12_TextChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 10);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 13);
            this.label3.TabIndex = 90;
            this.label3.Text = "Payment Breakdown";
            this.toolTip1.SetToolTip(this.label3, "Look at the account summary, Billing summary, Summary of charges, Overall Summary" +
        "");
            this.label3.Visible = false;
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox10.Location = new System.Drawing.Point(216, 77);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(99, 17);
            this.checkBox10.TabIndex = 43;
            this.checkBox10.Text = "No Billing Cycle";
            this.toolTip1.SetToolTip(this.checkBox10, "Can be the Billing Cycle date, Service Period date, Billing Period");
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.checkBox10_CheckedChanged);
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(193, 27);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(106, 20);
            this.textBox2.TabIndex = 50;
            this.textBox2.Text = "0.00";
            this.toolTip1.SetToolTip(this.textBox2, "Enter previous balance. Type \"0.00\" if there is no any.");
            this.textBox2.Visible = false;
            this.textBox2.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ClearTextbox);
            this.textBox2.TextChanged += new System.EventHandler(this.ShowAddMore);
            this.textBox2.Leave += new System.EventHandler(this.ComputeAmount);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox2.BackgroundImage")));
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Help;
            this.pictureBox2.Location = new System.Drawing.Point(418, 27);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(19, 19);
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox2, "Look for the ff keywords in the invoice:\r\nFor Electricity: kpwh, electric charge," +
        " consumption kwph\r\nFor Water: consumption per Gallons \r\nFor Gas: Natural Gas cos" +
        "t, check biller\'s name like Nicor Gas");
            // 
            // button12
            // 
            this.button12.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("button12.BackgroundImage")));
            this.button12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button12.Enabled = false;
            this.button12.Location = new System.Drawing.Point(193, 20);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(28, 23);
            this.button12.TabIndex = 140;
            this.toolTip1.SetToolTip(this.button12, "Open special instructions page");
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // button8
            // 
            this.button8.Enabled = false;
            this.button8.Image = ((System.Drawing.Image)(resources.GetObject("button8.Image")));
            this.button8.ImageAlign = System.Drawing.ContentAlignment.BottomLeft;
            this.button8.Location = new System.Drawing.Point(40, 20);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 136;
            this.button8.Text = "Upload";
            this.button8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.toolTip1.SetToolTip(this.button8, "Upload invoice");
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // button10
            // 
            this.button10.Enabled = false;
            this.button10.Image = ((System.Drawing.Image)(resources.GetObject("button10.Image")));
            this.button10.Location = new System.Drawing.Point(160, 20);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(28, 23);
            this.button10.TabIndex = 139;
            this.toolTip1.SetToolTip(this.button10, "Upload file ");
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click_1);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox7.BackgroundImage")));
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox7.Cursor = System.Windows.Forms.Cursors.Help;
            this.pictureBox7.Location = new System.Drawing.Point(190, 50);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(19, 19);
            this.pictureBox7.TabIndex = 125;
            this.pictureBox7.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox7, "Can be the \"Payment Due by, Payment received after (date), Must be paid by");
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox6.BackgroundImage")));
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox6.Cursor = System.Windows.Forms.Cursors.Help;
            this.pictureBox6.Location = new System.Drawing.Point(190, 24);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(19, 19);
            this.pictureBox6.TabIndex = 124;
            this.pictureBox6.TabStop = false;
            this.toolTip1.SetToolTip(this.pictureBox6, "Print date, Billing date, Bill date, Issue Date, Bill Issue Date, Statement Date " +
        "or the unlabeled date on the top of the invoice");
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(284, 227);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(44, 17);
            this.checkBox15.TabIndex = 21;
            this.checkBox15.Text = "Yes";
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.Visible = false;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.checkBox15_CheckedChanged);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(331, 227);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(40, 17);
            this.checkBox16.TabIndex = 22;
            this.checkBox16.Text = "No";
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.Visible = false;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.checkBox16_CheckedChanged);
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker4.Location = new System.Drawing.Point(87, 25);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(97, 20);
            this.dateTimePicker4.TabIndex = 38;
            this.dateTimePicker4.ValueChanged += new System.EventHandler(this.dateTimePicker4_ValueChanged);
            // 
            // BW_LoadTasksCompleted
            // 
            this.BW_LoadTasksCompleted.DoWork += new System.ComponentModel.DoWorkEventHandler(this.BW_LoadTasksCompleted_DoWork);
            // 
            // pdfviewer
            // 
            this.pdfviewer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pdfviewer.Location = new System.Drawing.Point(794, 59);
            this.pdfviewer.MinimumSize = new System.Drawing.Size(20, 20);
            this.pdfviewer.Name = "pdfviewer";
            this.pdfviewer.Size = new System.Drawing.Size(798, 772);
            this.pdfviewer.TabIndex = 69;
            this.pdfviewer.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.pdfviewer_DocumentCompleted_1);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(5, 27);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(71, 13);
            this.label22.TabIndex = 78;
            this.label22.Text = "Invoice Date:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.checkBox22);
            this.groupBox5.Controls.Add(this.label53);
            this.groupBox5.Controls.Add(this.dateTimePicker5);
            this.groupBox5.Controls.Add(this.pictureBox8);
            this.groupBox5.Controls.Add(this.pictureBox7);
            this.groupBox5.Controls.Add(this.pictureBox6);
            this.groupBox5.Controls.Add(this.checkBox12);
            this.groupBox5.Controls.Add(this.checkBox11);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.checkBox1);
            this.groupBox5.Controls.Add(this.checkBox10);
            this.groupBox5.Controls.Add(this.checkBox9);
            this.groupBox5.Controls.Add(this.dateTimePicker1);
            this.groupBox5.Controls.Add(this.label2);
            this.groupBox5.Controls.Add(this.dateTimePicker2);
            this.groupBox5.Controls.Add(this.dateTimePicker4);
            this.groupBox5.Controls.Add(this.label22);
            this.groupBox5.Controls.Add(this.label12);
            this.groupBox5.Controls.Add(this.dateTimePicker3);
            this.groupBox5.Controls.Add(this.label13);
            this.groupBox5.Location = new System.Drawing.Point(12, 612);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(335, 180);
            this.groupBox5.TabIndex = 80;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Billing Information";
            this.groupBox5.Visible = false;
            // 
            // checkBox22
            // 
            this.checkBox22.AutoSize = true;
            this.checkBox22.Location = new System.Drawing.Point(157, 130);
            this.checkBox22.Name = "checkBox22";
            this.checkBox22.Size = new System.Drawing.Size(46, 17);
            this.checkBox22.TabIndex = 45;
            this.checkBox22.Text = "Y/N";
            this.checkBox22.UseVisualStyleBackColor = true;
            this.checkBox22.CheckedChanged += new System.EventHandler(this.checkBox22_CheckedChanged);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(5, 131);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(147, 13);
            this.label53.TabIndex = 128;
            this.label53.Text = "Is there a disconnection date:";
            this.label53.Visible = false;
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.Enabled = false;
            this.dateTimePicker5.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker5.Location = new System.Drawing.Point(209, 128);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.Size = new System.Drawing.Size(97, 20);
            this.dateTimePicker5.TabIndex = 46;
            this.dateTimePicker5.Visible = false;
            this.dateTimePicker5.ValueChanged += new System.EventHandler(this.dateTimePicker5_ValueChanged);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox8.BackgroundImage")));
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox8.Cursor = System.Windows.Forms.Cursors.Help;
            this.pictureBox8.Location = new System.Drawing.Point(190, 75);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(19, 19);
            this.pictureBox8.TabIndex = 126;
            this.pictureBox8.TabStop = false;
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox12.Location = new System.Drawing.Point(291, 156);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(40, 17);
            this.checkBox12.TabIndex = 48;
            this.checkBox12.Text = "No";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.Visible = false;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.SetDoNotMail);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox11.Location = new System.Drawing.Point(243, 156);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(44, 17);
            this.checkBox11.TabIndex = 47;
            this.checkBox11.Text = "Yes";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.Visible = false;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.SetDoNotMail);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(5, 157);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(234, 13);
            this.label34.TabIndex = 82;
            this.label34.Text = "Is there a comment that says \"DO NOT MAIL\" ?";
            this.label34.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox1.Location = new System.Drawing.Point(215, 26);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(104, 17);
            this.checkBox1.TabIndex = 39;
            this.checkBox1.Text = "No Invoice Date";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.Visible = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox9.Location = new System.Drawing.Point(216, 52);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(89, 17);
            this.checkBox9.TabIndex = 41;
            this.checkBox9.Text = "No Due Date";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.Visible = false;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.checkBox9_CheckedChanged);
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.Color.White;
            this.textBox19.Location = new System.Drawing.Point(879, 28);
            this.textBox19.Name = "textBox19";
            this.textBox19.ReadOnly = true;
            this.textBox19.Size = new System.Drawing.Size(612, 20);
            this.textBox19.TabIndex = 82;
            this.textBox19.Visible = false;
            this.textBox19.WordWrap = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(794, 31);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(68, 13);
            this.label37.TabIndex = 83;
            this.label37.Text = "Invoice Link:";
            this.label37.Visible = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.DodgerBlue;
            this.button4.Enabled = false;
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Location = new System.Drawing.Point(1497, 26);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(83, 23);
            this.button4.TabIndex = 84;
            this.button4.Text = "Load Invoice";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Visible = false;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.pictureBox2);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this.comboBox2);
            this.groupBox6.Controls.Add(this.checkBox14);
            this.groupBox6.Controls.Add(this.checkBox13);
            this.groupBox6.Controls.Add(this.label30);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.checkBox5);
            this.groupBox6.Controls.Add(this.checkBox4);
            this.groupBox6.Controls.Add(this.checkBox3);
            this.groupBox6.Controls.Add(this.checkBox2);
            this.groupBox6.Controls.Add(this.button11);
            this.groupBox6.Location = new System.Drawing.Point(12, 26);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(530, 75);
            this.groupBox6.TabIndex = 86;
            this.groupBox6.TabStop = false;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(3, 50);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(144, 13);
            this.label38.TabIndex = 9;
            this.label38.Text = "Please specify the type of Bill";
            this.label38.Visible = false;
            // 
            // comboBox2
            // 
            this.comboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "Garbage/Trash/Disposal",
            "Stormwater/Drainage/WasteWater/Sewer",
            "TaxBill/TaxSale/property bill",
            "Cut Grass/weeds removal/Lawn maintenance",
            "Removal of debris",
            "Association/assessment fee",
            "Septic",
            "Others (Non-Billing Information)"});
            this.comboBox2.Location = new System.Drawing.Point(184, 48);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(259, 21);
            this.comboBox2.TabIndex = 7;
            this.comboBox2.Visible = false;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(361, 30);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(57, 17);
            this.checkBox14.TabIndex = 6;
            this.checkBox14.Text = "Others";
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.checkBox14_CheckedChanged);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Location = new System.Drawing.Point(306, 29);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(55, 17);
            this.checkBox13.TabIndex = 5;
            this.checkBox13.Text = "Water";
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.checkBox13_CheckedChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(3, 29);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(181, 13);
            this.label30.TabIndex = 5;
            this.label30.Text = "Is this an electricity, gas or water bill?";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(3, 10);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(99, 13);
            this.label9.TabIndex = 0;
            this.label9.Text = "Is this bill readable?";
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(255, 29);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(45, 17);
            this.checkBox5.TabIndex = 4;
            this.checkBox5.Text = "Gas";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.checkBox5_CheckedChanged);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(185, 28);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(71, 17);
            this.checkBox4.TabIndex = 3;
            this.checkBox4.Text = "Electricity";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.checkBox4_CheckedChanged);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(255, 10);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(40, 17);
            this.checkBox3.TabIndex = 2;
            this.checkBox3.Text = "No";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.checkBox3_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Location = new System.Drawing.Point(185, 9);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(44, 17);
            this.checkBox2.TabIndex = 1;
            this.checkBox2.Text = "Yes";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.DodgerBlue;
            this.button11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Location = new System.Drawing.Point(449, 30);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(71, 39);
            this.button11.TabIndex = 8;
            this.button11.Text = "Click here to continue";
            this.button11.UseVisualStyleBackColor = false;
            this.button11.Visible = false;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(288, 153);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(123, 13);
            this.label59.TabIndex = 124;
            this.label59.Text = "No. of balance transfers:";
            this.label59.Visible = false;
            // 
            // linkLabel2
            // 
            this.linkLabel2.AutoSize = true;
            this.linkLabel2.Location = new System.Drawing.Point(9, 97);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(57, 13);
            this.linkLabel2.TabIndex = 31;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "see details";
            this.linkLabel2.Visible = false;
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // comboBox8
            // 
            this.comboBox8.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Items.AddRange(new object[] {
            "Current Charge",
            "Payment Received",
            "Late Fee",
            "Disconnection Fee",
            "Deposit Payment",
            "Deposit Refund"});
            this.comboBox8.Location = new System.Drawing.Point(12, 75);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(158, 21);
            this.comboBox8.TabIndex = 54;
            this.comboBox8.Visible = false;
            this.comboBox8.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.breakdown_cb1_DrawItem);
            this.comboBox8.SelectedIndexChanged += new System.EventHandler(this.ShowAddMore);
            this.comboBox8.DropDownClosed += new System.EventHandler(this.HideToolTip);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(177, 78);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(13, 13);
            this.label25.TabIndex = 99;
            this.label25.Text = "$";
            this.label25.Visible = false;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(193, 75);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(106, 20);
            this.textBox8.TabIndex = 55;
            this.textBox8.Text = "0.00";
            this.textBox8.Visible = false;
            this.textBox8.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ClearTextbox);
            this.textBox8.TextChanged += new System.EventHandler(this.ShowAddMore);
            this.textBox8.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox8_KeyPress);
            this.textBox8.Leave += new System.EventHandler(this.ComputeAmount);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(177, 54);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(13, 13);
            this.label24.TabIndex = 97;
            this.label24.Text = "$";
            this.label24.Visible = false;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(193, 51);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(106, 20);
            this.textBox6.TabIndex = 53;
            this.textBox6.Text = "0.00";
            this.textBox6.Visible = false;
            this.textBox6.MouseClick += new System.Windows.Forms.MouseEventHandler(this.ClearTextbox);
            this.textBox6.TextChanged += new System.EventHandler(this.ShowAddMore);
            this.textBox6.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox6_KeyPress);
            this.textBox6.Leave += new System.EventHandler(this.ComputeAmount);
            // 
            // comboBox4
            // 
            this.comboBox4.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBox4.ForeColor = System.Drawing.SystemColors.WindowText;
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "Current Charge",
            "Payment Received",
            "Late Fee",
            "Disconnection Fee",
            "Deposit Payment",
            "Deposit Refund"});
            this.comboBox4.Location = new System.Drawing.Point(12, 51);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(158, 21);
            this.comboBox4.TabIndex = 52;
            this.comboBox4.Visible = false;
            this.comboBox4.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.breakdown_cb1_DrawItem);
            this.comboBox4.SelectedIndexChanged += new System.EventHandler(this.ShowAddMore);
            this.comboBox4.DropDownClosed += new System.EventHandler(this.HideToolTip);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(177, 30);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(13, 13);
            this.label23.TabIndex = 93;
            this.label23.Text = "$";
            this.label23.Visible = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(224, 10);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(43, 13);
            this.label10.TabIndex = 92;
            this.label10.Text = "Amount";
            this.label10.Visible = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(8, 113);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(160, 13);
            this.label31.TabIndex = 103;
            this.label31.Text = "TOTAL AMOUNT ON INVOICE:";
            this.label31.Visible = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(9, 136);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(167, 13);
            this.label33.TabIndex = 106;
            this.label33.Text = "TOTAL AMT AFTER DUE DATE:";
            this.label33.Visible = false;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(308, 29);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(76, 17);
            this.checkBox7.TabIndex = 51;
            this.checkBox7.Text = "no amount";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.Visible = false;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.checkBox7_CheckedChanged);
            // 
            // btn_AddMorePaymentBreakdown
            // 
            this.btn_AddMorePaymentBreakdown.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_AddMorePaymentBreakdown.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_AddMorePaymentBreakdown.ForeColor = System.Drawing.Color.White;
            this.btn_AddMorePaymentBreakdown.Location = new System.Drawing.Point(308, 74);
            this.btn_AddMorePaymentBreakdown.Name = "btn_AddMorePaymentBreakdown";
            this.btn_AddMorePaymentBreakdown.Size = new System.Drawing.Size(63, 23);
            this.btn_AddMorePaymentBreakdown.TabIndex = 56;
            this.btn_AddMorePaymentBreakdown.Text = "Add more";
            this.btn_AddMorePaymentBreakdown.UseVisualStyleBackColor = false;
            this.btn_AddMorePaymentBreakdown.Visible = false;
            this.btn_AddMorePaymentBreakdown.Click += new System.EventHandler(this.btn_AddMorePaymentBreakdown_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DodgerBlue;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(676, 396);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(108, 48);
            this.button5.TabIndex = 34;
            this.button5.Text = "No Match Found. Save and Next Invoice";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Visible = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DodgerBlue;
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(676, 352);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(109, 38);
            this.button6.TabIndex = 33;
            this.button6.Text = "No Match Found. Search Again.";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Visible = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label36);
            this.panel1.Location = new System.Drawing.Point(1168, 30);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(71, 23);
            this.panel1.TabIndex = 120;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(3, 1);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(66, 20);
            this.label36.TabIndex = 0;
            this.label36.Text = "Invoice";
            // 
            // chkbx_noZipCode
            // 
            this.chkbx_noZipCode.AutoSize = true;
            this.chkbx_noZipCode.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkbx_noZipCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkbx_noZipCode.Location = new System.Drawing.Point(234, 23);
            this.chkbx_noZipCode.Name = "chkbx_noZipCode";
            this.chkbx_noZipCode.Size = new System.Drawing.Size(71, 16);
            this.chkbx_noZipCode.TabIndex = 27;
            this.chkbx_noZipCode.Text = "No Zipcode";
            this.chkbx_noZipCode.UseVisualStyleBackColor = true;
            this.chkbx_noZipCode.CheckedChanged += new System.EventHandler(this.chkbx_noZipCode_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.txtTotal);
            this.groupBox7.Controls.Add(this.label55);
            this.groupBox7.Controls.Add(this.textBox35);
            this.groupBox7.Controls.Add(this.checkBox24);
            this.groupBox7.Controls.Add(this.checkBox23);
            this.groupBox7.Controls.Add(this.label54);
            this.groupBox7.Controls.Add(this.textBox34);
            this.groupBox7.Controls.Add(this.label3);
            this.groupBox7.Controls.Add(this.comboBox4);
            this.groupBox7.Controls.Add(this.comboBox8);
            this.groupBox7.Controls.Add(this.label31);
            this.groupBox7.Controls.Add(this.linkLabel2);
            this.groupBox7.Controls.Add(this.label33);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Controls.Add(this.label23);
            this.groupBox7.Controls.Add(this.btn_AddMorePaymentBreakdown);
            this.groupBox7.Controls.Add(this.textBox2);
            this.groupBox7.Controls.Add(this.textBox6);
            this.groupBox7.Controls.Add(this.checkBox7);
            this.groupBox7.Controls.Add(this.label24);
            this.groupBox7.Controls.Add(this.textBox12);
            this.groupBox7.Controls.Add(this.textBox8);
            this.groupBox7.Controls.Add(this.textBox11);
            this.groupBox7.Controls.Add(this.label25);
            this.groupBox7.Location = new System.Drawing.Point(348, 612);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(436, 180);
            this.groupBox7.TabIndex = 124;
            this.groupBox7.TabStop = false;
            this.groupBox7.Visible = false;
            // 
            // txtTotal
            // 
            this.txtTotal.BackColor = System.Drawing.Color.PeachPuff;
            this.txtTotal.Location = new System.Drawing.Point(309, 110);
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.Size = new System.Drawing.Size(65, 20);
            this.txtTotal.TabIndex = 58;
            this.txtTotal.Text = "0";
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(320, 158);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(52, 13);
            this.label55.TabIndex = 132;
            this.label55.Text = "Deposit $";
            this.label55.Visible = false;
            // 
            // textBox35
            // 
            this.textBox35.Location = new System.Drawing.Point(373, 154);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(41, 20);
            this.textBox35.TabIndex = 62;
            this.textBox35.Visible = false;
            this.textBox35.TextChanged += new System.EventHandler(this.textBox35_TextChanged);
            this.textBox35.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.numbersonly);
            // 
            // checkBox24
            // 
            this.checkBox24.AutoSize = true;
            this.checkBox24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox24.Location = new System.Drawing.Point(277, 159);
            this.checkBox24.Name = "checkBox24";
            this.checkBox24.Size = new System.Drawing.Size(40, 17);
            this.checkBox24.TabIndex = 61;
            this.checkBox24.Text = "No";
            this.checkBox24.UseVisualStyleBackColor = true;
            this.checkBox24.Visible = false;
            this.checkBox24.CheckedChanged += new System.EventHandler(this.checkBox24_CheckedChanged);
            // 
            // checkBox23
            // 
            this.checkBox23.AutoSize = true;
            this.checkBox23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox23.Location = new System.Drawing.Point(231, 159);
            this.checkBox23.Name = "checkBox23";
            this.checkBox23.Size = new System.Drawing.Size(44, 17);
            this.checkBox23.TabIndex = 60;
            this.checkBox23.Text = "Yes";
            this.checkBox23.UseVisualStyleBackColor = true;
            this.checkBox23.Visible = false;
            this.checkBox23.CheckedChanged += new System.EventHandler(this.checkBox23_CheckedChanged);
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(9, 160);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(216, 13);
            this.label54.TabIndex = 112;
            this.label54.Text = "Is there a comment that says \"FINAL BILL\"?";
            this.label54.Visible = false;
            // 
            // textBox34
            // 
            this.textBox34.BackColor = System.Drawing.Color.White;
            this.textBox34.Enabled = false;
            this.textBox34.Location = new System.Drawing.Point(12, 27);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(156, 20);
            this.textBox34.TabIndex = 49;
            this.textBox34.Text = "Previous Balance";
            // 
            // chkTransAmtNo
            // 
            this.chkTransAmtNo.AutoSize = true;
            this.chkTransAmtNo.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkTransAmtNo.Location = new System.Drawing.Point(243, 153);
            this.chkTransAmtNo.Name = "chkTransAmtNo";
            this.chkTransAmtNo.Size = new System.Drawing.Size(40, 17);
            this.chkTransAmtNo.TabIndex = 15;
            this.chkTransAmtNo.Text = "No";
            this.chkTransAmtNo.UseVisualStyleBackColor = true;
            this.chkTransAmtNo.Visible = false;
            this.chkTransAmtNo.CheckedChanged += new System.EventHandler(this.chkTransAmtNo_CheckedChanged);
            this.chkTransAmtNo.CheckStateChanged += new System.EventHandler(this.chkTransAmtNo_CheckStateChanged);
            // 
            // chkTransAmtYes
            // 
            this.chkTransAmtYes.AutoSize = true;
            this.chkTransAmtYes.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkTransAmtYes.Location = new System.Drawing.Point(196, 153);
            this.chkTransAmtYes.Name = "chkTransAmtYes";
            this.chkTransAmtYes.Size = new System.Drawing.Size(44, 17);
            this.chkTransAmtYes.TabIndex = 14;
            this.chkTransAmtYes.Text = "Yes";
            this.chkTransAmtYes.UseVisualStyleBackColor = true;
            this.chkTransAmtYes.Visible = false;
            this.chkTransAmtYes.CheckedChanged += new System.EventHandler(this.chkTransAmtYes_CheckedChanged);
            this.chkTransAmtYes.CheckStateChanged += new System.EventHandler(this.chkTransAmtYes_CheckStateChanged);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(15, 153);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(136, 13);
            this.label58.TabIndex = 112;
            this.label58.Text = "Is there a balance transfer?";
            this.label58.Visible = false;
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.pnlAddress);
            this.groupBox8.Controls.Add(this.pictureBox3);
            this.groupBox8.Controls.Add(this.label26);
            this.groupBox8.Controls.Add(this.textBox4);
            this.groupBox8.Controls.Add(this.chkbx_noZipCode);
            this.groupBox8.Controls.Add(this.textBox1);
            this.groupBox8.Controls.Add(this.label1);
            this.groupBox8.Location = new System.Drawing.Point(12, 275);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(481, 78);
            this.groupBox8.TabIndex = 126;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Property Address";
            this.groupBox8.Visible = false;
            // 
            // pnlAddress
            // 
            this.pnlAddress.Controls.Add(this.label7);
            this.pnlAddress.Controls.Add(this.pictureBox4);
            this.pnlAddress.Controls.Add(this.label6);
            this.pnlAddress.Controls.Add(this.textBox14);
            this.pnlAddress.Controls.Add(this.textBox16);
            this.pnlAddress.Controls.Add(this.label5);
            this.pnlAddress.Controls.Add(this.textBox17);
            this.pnlAddress.Location = new System.Drawing.Point(3, 42);
            this.pnlAddress.Name = "pnlAddress";
            this.pnlAddress.Size = new System.Drawing.Size(466, 31);
            this.pnlAddress.TabIndex = 124;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox4.BackgroundImage")));
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Help;
            this.pictureBox4.Location = new System.Drawing.Point(156, 5);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(19, 19);
            this.pictureBox4.TabIndex = 12;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.MouseLeave += new System.EventHandler(this.textBox14_MouseLeave);
            this.pictureBox4.MouseHover += new System.EventHandler(this.textBox14_MouseHover);
            // 
            // textBox16
            // 
            this.textBox16.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.textBox16.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBox16.Location = new System.Drawing.Point(240, 5);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(122, 20);
            this.textBox16.TabIndex = 30;
            this.textBox16.TextChanged += new System.EventHandler(this.textBox16_TextChanged);
            this.textBox16.PreviewKeyDown += new System.Windows.Forms.PreviewKeyDownEventHandler(this.textBox16_PreviewKeyDown);
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(415, 4);
            this.textBox17.MaxLength = 2;
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(39, 20);
            this.textBox17.TabIndex = 29;
            this.textBox17.TextChanged += new System.EventHandler(this.textBox17_TextChanged);
            this.textBox17.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.triggerStateSearch);
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox3.BackgroundImage")));
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Help;
            this.pictureBox3.Location = new System.Drawing.Point(105, 21);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(19, 19);
            this.pictureBox3.TabIndex = 12;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.MouseLeave += new System.EventHandler(this.textBox4_MouseLeave);
            this.pictureBox3.MouseHover += new System.EventHandler(this.textBox4_MouseHover);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(30, 453);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(225, 13);
            this.label4.TabIndex = 128;
            this.label4.Text = "What\'s the Service provider name on invoice?";
            this.label4.Visible = false;
            // 
            // comboBox5
            // 
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(261, 450);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(403, 21);
            this.comboBox5.TabIndex = 35;
            this.comboBox5.Visible = false;
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            this.comboBox5.TextChanged += new System.EventHandler(this.comboBox5_TextChanged);
            // 
            // dgv_usp
            // 
            this.dgv_usp.AllowUserToAddRows = false;
            this.dgv_usp.AllowUserToDeleteRows = false;
            this.dgv_usp.AllowUserToResizeRows = false;
            this.dgv_usp.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_usp.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dgv_usp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_usp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.vdr_id,
            this.vdr_name,
            this.vdr_address,
            this.vdr_status,
            this.vms_status});
            this.dgv_usp.Cursor = System.Windows.Forms.Cursors.Hand;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv_usp.DefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_usp.Location = new System.Drawing.Point(12, 504);
            this.dgv_usp.Name = "dgv_usp";
            this.dgv_usp.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_usp.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dgv_usp.RowHeadersVisible = false;
            this.dgv_usp.Size = new System.Drawing.Size(652, 105);
            this.dgv_usp.TabIndex = 130;
            this.dgv_usp.Visible = false;
            this.dgv_usp.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_usp_CellClick);
            // 
            // vdr_id
            // 
            this.vdr_id.HeaderText = "VDR ID";
            this.vdr_id.Name = "vdr_id";
            this.vdr_id.ReadOnly = true;
            this.vdr_id.Visible = false;
            this.vdr_id.Width = 5;
            // 
            // vdr_name
            // 
            this.vdr_name.HeaderText = "Biller Name";
            this.vdr_name.MinimumWidth = 165;
            this.vdr_name.Name = "vdr_name";
            this.vdr_name.ReadOnly = true;
            this.vdr_name.Width = 165;
            // 
            // vdr_address
            // 
            this.vdr_address.HeaderText = "Biller Address";
            this.vdr_address.MinimumWidth = 300;
            this.vdr_address.Name = "vdr_address";
            this.vdr_address.ReadOnly = true;
            this.vdr_address.Width = 300;
            // 
            // vdr_status
            // 
            this.vdr_status.HeaderText = "VDR Status";
            this.vdr_status.MinimumWidth = 90;
            this.vdr_status.Name = "vdr_status";
            this.vdr_status.ReadOnly = true;
            this.vdr_status.Width = 90;
            // 
            // vms_status
            // 
            this.vms_status.HeaderText = "VMS Status";
            this.vms_status.MinimumWidth = 90;
            this.vms_status.Name = "vms_status";
            this.vms_status.ReadOnly = true;
            this.vms_status.Width = 90;
            // 
            // button_noMatchUSP
            // 
            this.button_noMatchUSP.BackColor = System.Drawing.Color.DodgerBlue;
            this.button_noMatchUSP.ForeColor = System.Drawing.Color.White;
            this.button_noMatchUSP.Location = new System.Drawing.Point(674, 541);
            this.button_noMatchUSP.Name = "button_noMatchUSP";
            this.button_noMatchUSP.Size = new System.Drawing.Size(109, 30);
            this.button_noMatchUSP.TabIndex = 37;
            this.button_noMatchUSP.Text = "No Match Found";
            this.button_noMatchUSP.UseVisualStyleBackColor = false;
            this.button_noMatchUSP.Visible = false;
            this.button_noMatchUSP.Click += new System.EventHandler(this.button_noMatchUSP_Click);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.ForeColor = System.Drawing.Color.Black;
            this.label47.Location = new System.Drawing.Point(15, 228);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(244, 13);
            this.label47.TabIndex = 133;
            this.label47.Text = "Are there multiple property address in one invoice?";
            this.label47.Visible = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "UploadInvoice.pdf";
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.btnCancelUpload);
            this.groupBox9.Controls.Add(this.button12);
            this.groupBox9.Controls.Add(this.button8);
            this.groupBox9.Controls.Add(this.button10);
            this.groupBox9.Location = new System.Drawing.Point(541, 174);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(247, 53);
            this.groupBox9.TabIndex = 141;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Upload Invoice";
            this.groupBox9.Visible = false;
            // 
            // btnCancelUpload
            // 
            this.btnCancelUpload.BackgroundImage = global::UC.Properties.Resources.favicon__2_;
            this.btnCancelUpload.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnCancelUpload.Location = new System.Drawing.Point(124, 20);
            this.btnCancelUpload.Name = "btnCancelUpload";
            this.btnCancelUpload.Size = new System.Drawing.Size(28, 23);
            this.btnCancelUpload.TabIndex = 141;
            this.btnCancelUpload.UseVisualStyleBackColor = true;
            this.btnCancelUpload.Click += new System.EventHandler(this.btnCancelUpload_Click);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(15, 107);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(251, 13);
            this.label60.TabIndex = 112;
            this.label60.Text = "Is the bill addressed to Altisource or c/o Altisource? ";
            this.label60.Visible = false;
            // 
            // chkASFIYes
            // 
            this.chkASFIYes.AutoSize = true;
            this.chkASFIYes.Location = new System.Drawing.Point(284, 107);
            this.chkASFIYes.Name = "chkASFIYes";
            this.chkASFIYes.Size = new System.Drawing.Size(44, 17);
            this.chkASFIYes.TabIndex = 9;
            this.chkASFIYes.Text = "Yes";
            this.chkASFIYes.UseVisualStyleBackColor = true;
            this.chkASFIYes.Visible = false;
            this.chkASFIYes.CheckedChanged += new System.EventHandler(this.chkASFIYes_CheckedChanged);
            // 
            // chkASFINo
            // 
            this.chkASFINo.AutoSize = true;
            this.chkASFINo.Location = new System.Drawing.Point(331, 107);
            this.chkASFINo.Name = "chkASFINo";
            this.chkASFINo.Size = new System.Drawing.Size(40, 17);
            this.chkASFINo.TabIndex = 10;
            this.chkASFINo.Text = "No";
            this.chkASFINo.UseVisualStyleBackColor = true;
            this.chkASFINo.Visible = false;
            this.chkASFINo.CheckedChanged += new System.EventHandler(this.chkASFINo_CheckedChanged);
            // 
            // cmbBillName
            // 
            this.cmbBillName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbBillName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbBillName.FormattingEnabled = true;
            this.cmbBillName.Location = new System.Drawing.Point(541, 124);
            this.cmbBillName.Name = "cmbBillName";
            this.cmbBillName.Size = new System.Drawing.Size(247, 21);
            this.cmbBillName.TabIndex = 13;
            this.cmbBillName.Visible = false;
            this.cmbBillName.SelectedValueChanged += new System.EventHandler(this.cmbBillName_SelectedValueChanged);
            this.cmbBillName.TextChanged += new System.EventHandler(this.cmbBillName_TextChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(104, 9);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(72, 13);
            this.label11.TabIndex = 65;
            this.label11.Text = "Your Invoices";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(165, 33);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(13, 13);
            this.label16.TabIndex = 34;
            this.label16.Text = "0";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(4, 9);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 13);
            this.label15.TabIndex = 33;
            this.label15.Text = "Best in the team";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(70, 33);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 13);
            this.label19.TabIndex = 66;
            this.label19.Text = "0";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(187, 10);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(47, 13);
            this.label18.TabIndex = 36;
            this.label18.Text = "Duration";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(27, 33);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(34, 13);
            this.label32.TabIndex = 67;
            this.label32.Text = "Total:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(205, 33);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(13, 13);
            this.label17.TabIndex = 35;
            this.label17.Text = "0";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(122, 33);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(34, 13);
            this.label42.TabIndex = 68;
            this.label42.Text = "Total:";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(108, 50);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(51, 13);
            this.label44.TabIndex = 70;
            this.label44.Text = "Ave / Hr:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(70, 51);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(13, 13);
            this.label45.TabIndex = 71;
            this.label45.Text = "0";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(165, 51);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(13, 13);
            this.label46.TabIndex = 72;
            this.label46.Text = "0";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.label46);
            this.groupBox4.Controls.Add(this.label45);
            this.groupBox4.Controls.Add(this.label44);
            this.groupBox4.Controls.Add(this.label43);
            this.groupBox4.Controls.Add(this.label42);
            this.groupBox4.Controls.Add(this.label17);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.label15);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Controls.Add(this.label11);
            this.groupBox4.Location = new System.Drawing.Point(544, 26);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(244, 75);
            this.groupBox4.TabIndex = 76;
            this.groupBox4.TabStop = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(13, 50);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(51, 13);
            this.label43.TabIndex = 69;
            this.label43.Text = "Ave / Hr:";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(540, 108);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(155, 13);
            this.label61.TabIndex = 112;
            this.label61.Text = "To whom is the bill addressed? ";
            this.label61.Visible = false;
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(725, 149);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(60, 23);
            this.btnAdd.TabIndex = 146;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Visible = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnCancel.ForeColor = System.Drawing.Color.White;
            this.btnCancel.Location = new System.Drawing.Point(725, 152);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(60, 23);
            this.btnCancel.TabIndex = 146;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = false;
            this.btnCancel.Visible = false;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // lblAdrsNotif
            // 
            this.lblAdrsNotif.AutoSize = true;
            this.lblAdrsNotif.ForeColor = System.Drawing.Color.Red;
            this.lblAdrsNotif.Location = new System.Drawing.Point(589, 154);
            this.lblAdrsNotif.Name = "lblAdrsNotif";
            this.lblAdrsNotif.Size = new System.Drawing.Size(136, 13);
            this.lblAdrsNotif.TabIndex = 127;
            this.lblAdrsNotif.Text = "Please enter the addressee";
            this.lblAdrsNotif.Visible = false;
            // 
            // numBalTransfer
            // 
            this.numBalTransfer.Location = new System.Drawing.Point(444, 149);
            this.numBalTransfer.Name = "numBalTransfer";
            this.numBalTransfer.Size = new System.Drawing.Size(57, 20);
            this.numBalTransfer.TabIndex = 16;
            this.numBalTransfer.Visible = false;
            this.numBalTransfer.ValueChanged += new System.EventHandler(this.numBalTransfer_ValueChanged_1);
            this.numBalTransfer.KeyUp += new System.Windows.Forms.KeyEventHandler(this.numBalTransfer_KeyUp);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(15, 177);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(377, 13);
            this.label62.TabIndex = 112;
            this.label62.Text = "Is there an Account number / Service Address on the balance transfer details?";
            this.label62.Visible = false;
            // 
            // chkBalInfoYes
            // 
            this.chkBalInfoYes.AutoSize = true;
            this.chkBalInfoYes.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkBalInfoYes.Location = new System.Drawing.Point(406, 177);
            this.chkBalInfoYes.Name = "chkBalInfoYes";
            this.chkBalInfoYes.Size = new System.Drawing.Size(44, 17);
            this.chkBalInfoYes.TabIndex = 17;
            this.chkBalInfoYes.Text = "Yes";
            this.chkBalInfoYes.UseVisualStyleBackColor = true;
            this.chkBalInfoYes.Visible = false;
            this.chkBalInfoYes.CheckedChanged += new System.EventHandler(this.chkBalInfoYes_CheckedChanged);
            // 
            // chkBalInfoNo
            // 
            this.chkBalInfoNo.AutoSize = true;
            this.chkBalInfoNo.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkBalInfoNo.Location = new System.Drawing.Point(453, 177);
            this.chkBalInfoNo.Name = "chkBalInfoNo";
            this.chkBalInfoNo.Size = new System.Drawing.Size(40, 17);
            this.chkBalInfoNo.TabIndex = 18;
            this.chkBalInfoNo.Text = "No";
            this.chkBalInfoNo.UseVisualStyleBackColor = true;
            this.chkBalInfoNo.Visible = false;
            this.chkBalInfoNo.CheckedChanged += new System.EventHandler(this.chkBalInfoNo_CheckedChanged);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(15, 202);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(368, 13);
            this.label63.TabIndex = 112;
            this.label63.Text = "Does the invoice show any charges other than the balance transfer amount?\r\n";
            this.label63.Visible = false;
            // 
            // chkBalAmtYes
            // 
            this.chkBalAmtYes.AutoSize = true;
            this.chkBalAmtYes.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkBalAmtYes.Location = new System.Drawing.Point(406, 202);
            this.chkBalAmtYes.Name = "chkBalAmtYes";
            this.chkBalAmtYes.Size = new System.Drawing.Size(44, 17);
            this.chkBalAmtYes.TabIndex = 19;
            this.chkBalAmtYes.Text = "Yes";
            this.chkBalAmtYes.UseVisualStyleBackColor = true;
            this.chkBalAmtYes.Visible = false;
            this.chkBalAmtYes.CheckedChanged += new System.EventHandler(this.chkBalAmtYes_CheckedChanged);
            // 
            // chkBalAmtNo
            // 
            this.chkBalAmtNo.AutoSize = true;
            this.chkBalAmtNo.Cursor = System.Windows.Forms.Cursors.Default;
            this.chkBalAmtNo.Location = new System.Drawing.Point(453, 202);
            this.chkBalAmtNo.Name = "chkBalAmtNo";
            this.chkBalAmtNo.Size = new System.Drawing.Size(40, 17);
            this.chkBalAmtNo.TabIndex = 20;
            this.chkBalAmtNo.Text = "No";
            this.chkBalAmtNo.UseVisualStyleBackColor = true;
            this.chkBalAmtNo.Visible = false;
            this.chkBalAmtNo.CheckedChanged += new System.EventHandler(this.chkBalAmtNo_CheckedChanged);
            // 
            // lblCount
            // 
            this.lblCount.AutoSize = true;
            this.lblCount.Location = new System.Drawing.Point(411, 153);
            this.lblCount.Name = "lblCount";
            this.lblCount.Size = new System.Drawing.Size(13, 13);
            this.lblCount.TabIndex = 148;
            this.lblCount.Text = "0";
            this.lblCount.Visible = false;
            // 
            // lblOf
            // 
            this.lblOf.AutoSize = true;
            this.lblOf.Location = new System.Drawing.Point(427, 153);
            this.lblOf.Name = "lblOf";
            this.lblOf.Size = new System.Drawing.Size(16, 13);
            this.lblOf.TabIndex = 149;
            this.lblOf.Text = "of";
            this.lblOf.Visible = false;
            // 
            // lblTransferCount
            // 
            this.lblTransferCount.AutoSize = true;
            this.lblTransferCount.Location = new System.Drawing.Point(514, 151);
            this.lblTransferCount.Name = "lblTransferCount";
            this.lblTransferCount.Size = new System.Drawing.Size(10, 13);
            this.lblTransferCount.TabIndex = 150;
            this.lblTransferCount.Text = "-";
            this.lblTransferCount.Visible = false;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(15, 129);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(212, 13);
            this.label64.TabIndex = 112;
            this.label64.Text = "Is the invoice billed to Altisource’s address?";
            this.label64.Visible = false;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(226, 129);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(209, 14);
            this.label65.TabIndex = 112;
            this.label65.Text = "PO Box 105265, Atlanta, GA, 30348-5265";
            this.label65.Visible = false;
            // 
            // chknYes
            // 
            this.chknYes.AutoSize = true;
            this.chknYes.Location = new System.Drawing.Point(442, 128);
            this.chknYes.Name = "chknYes";
            this.chknYes.Size = new System.Drawing.Size(44, 17);
            this.chknYes.TabIndex = 11;
            this.chknYes.Text = "Yes";
            this.chknYes.UseVisualStyleBackColor = true;
            this.chknYes.Visible = false;
            this.chknYes.CheckedChanged += new System.EventHandler(this.chknYes_CheckedChanged);
            // 
            // chknNo
            // 
            this.chknNo.AutoSize = true;
            this.chknNo.Location = new System.Drawing.Point(491, 128);
            this.chknNo.Name = "chknNo";
            this.chknNo.Size = new System.Drawing.Size(40, 17);
            this.chknNo.TabIndex = 12;
            this.chknNo.Text = "No";
            this.chknNo.UseVisualStyleBackColor = true;
            this.chknNo.Visible = false;
            this.chknNo.CheckedChanged += new System.EventHandler(this.chknNo_CheckedChanged);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(261, 482);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(24, 13);
            this.label66.TabIndex = 128;
            this.label66.Text = "City";
            this.label66.Visible = false;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(470, 481);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(32, 13);
            this.label67.TabIndex = 128;
            this.label67.Text = "State";
            this.label67.Visible = false;
            // 
            // txtUSPZipcode
            // 
            this.txtUSPZipcode.Location = new System.Drawing.Point(617, 478);
            this.txtUSPZipcode.MaxLength = 5;
            this.txtUSPZipcode.Name = "txtUSPZipcode";
            this.txtUSPZipcode.Size = new System.Drawing.Size(46, 20);
            this.txtUSPZipcode.TabIndex = 153;
            this.txtUSPZipcode.Visible = false;
            this.txtUSPZipcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUSPZipcode_KeyPress);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(564, 480);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(50, 13);
            this.label68.TabIndex = 128;
            this.label68.Text = "Zip Code";
            this.label68.Visible = false;
            // 
            // btnSearchUSP
            // 
            this.btnSearchUSP.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnSearchUSP.ForeColor = System.Drawing.Color.White;
            this.btnSearchUSP.Location = new System.Drawing.Point(674, 505);
            this.btnSearchUSP.Name = "btnSearchUSP";
            this.btnSearchUSP.Size = new System.Drawing.Size(109, 30);
            this.btnSearchUSP.TabIndex = 37;
            this.btnSearchUSP.Text = "Search USP";
            this.btnSearchUSP.UseVisualStyleBackColor = false;
            this.btnSearchUSP.Visible = false;
            this.btnSearchUSP.Click += new System.EventHandler(this.btnSearchUSP_Click);
            // 
            // txtUSPCity
            // 
            this.txtUSPCity.Location = new System.Drawing.Point(291, 477);
            this.txtUSPCity.Name = "txtUSPCity";
            this.txtUSPCity.Size = new System.Drawing.Size(174, 20);
            this.txtUSPCity.TabIndex = 153;
            this.txtUSPCity.Visible = false;
            this.txtUSPCity.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUSPCity_KeyPress);
            // 
            // txtUSPState
            // 
            this.txtUSPState.Location = new System.Drawing.Point(508, 477);
            this.txtUSPState.MaxLength = 2;
            this.txtUSPState.Name = "txtUSPState";
            this.txtUSPState.Size = new System.Drawing.Size(51, 20);
            this.txtUSPState.TabIndex = 153;
            this.txtUSPState.Visible = false;
            this.txtUSPState.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUSPState_KeyPress);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(32, 482);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(35, 13);
            this.label69.TabIndex = 128;
            this.label69.Text = "Street";
            this.label69.Visible = false;
            // 
            // txtUSPStrt
            // 
            this.txtUSPStrt.Location = new System.Drawing.Point(68, 477);
            this.txtUSPStrt.Name = "txtUSPStrt";
            this.txtUSPStrt.Size = new System.Drawing.Size(189, 20);
            this.txtUSPStrt.TabIndex = 153;
            this.txtUSPStrt.Visible = false;
            this.txtUSPStrt.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtUSPCity_KeyPress);
            // 
            // panel_acctNumber
            // 
            this.panel_acctNumber.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("panel_acctNumber.BackgroundImage")));
            this.panel_acctNumber.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.panel_acctNumber.Location = new System.Drawing.Point(11, 455);
            this.panel_acctNumber.Name = "panel_acctNumber";
            this.panel_acctNumber.Size = new System.Drawing.Size(776, 310);
            this.panel_acctNumber.TabIndex = 123;
            this.panel_acctNumber.Visible = false;
            this.panel_acctNumber.Paint += new System.Windows.Forms.PaintEventHandler(this.panel_acctNumber_Paint);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox5.BackgroundImage")));
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox5.Cursor = System.Windows.Forms.Cursors.Help;
            this.pictureBox5.Location = new System.Drawing.Point(287, 251);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(19, 19);
            this.pictureBox5.TabIndex = 12;
            this.pictureBox5.TabStop = false;
            this.pictureBox5.Visible = false;
            this.pictureBox5.MouseLeave += new System.EventHandler(this.HidePic_AcctNo);
            this.pictureBox5.MouseHover += new System.EventHandler(this.ShowPic_AcctNo);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1584, 832);
            this.Controls.Add(this.panel_specialinstructions2);
            this.Controls.Add(this.lblTransferCount);
            this.Controls.Add(this.panel_acctNumber);
            this.Controls.Add(this.chknNo);
            this.Controls.Add(this.lbl_UspOnInvoice);
            this.Controls.Add(this.chknYes);
            this.Controls.Add(this.lblOf);
            this.Controls.Add(this.lblCount);
            this.Controls.Add(this.numBalTransfer);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.cmbBillName);
            this.Controls.Add(this.panel_specialInstructions);
            this.Controls.Add(this.chkASFINo);
            this.Controls.Add(this.chkASFIYes);
            this.Controls.Add(this.chkBalAmtNo);
            this.Controls.Add(this.chkBalInfoNo);
            this.Controls.Add(this.chkTransAmtNo);
            this.Controls.Add(this.label59);
            this.Controls.Add(this.chkBalAmtYes);
            this.Controls.Add(this.chkBalInfoYes);
            this.Controls.Add(this.chkTransAmtYes);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.label65);
            this.Controls.Add(this.label64);
            this.Controls.Add(this.label60);
            this.Controls.Add(this.label63);
            this.Controls.Add(this.label62);
            this.Controls.Add(this.label58);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.pictureBox5);
            this.Controls.Add(this.checkBox16);
            this.Controls.Add(this.checkBox15);
            this.Controls.Add(this.label47);
            this.Controls.Add(this.btnSearchUSP);
            this.Controls.Add(this.button_noMatchUSP);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.cmboBx_UspOnInvoice);
            this.Controls.Add(this.dgv_usp);
            this.Controls.Add(this.checkBox8);
            this.Controls.Add(this.comboBox5);
            this.Controls.Add(this.label68);
            this.Controls.Add(this.label69);
            this.Controls.Add(this.label67);
            this.Controls.Add(this.label66);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.lblAdrsNotif);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.label37);
            this.Controls.Add(this.textBox19);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.chkBx_NoPropertyAddress);
            this.Controls.Add(this.checkBox6);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.pdfviewer);
            this.Controls.Add(this.label39);
            this.Controls.Add(this.txtUSPState);
            this.Controls.Add(this.txtUSPStrt);
            this.Controls.Add(this.txtUSPCity);
            this.Controls.Add(this.txtUSPZipcode);
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Expense Management Software";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.panel_specialinstructions2.ResumeLayout(false);
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            this.panel_specialInstructions.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.panel_callout.ResumeLayout(false);
            this.panel_callout.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.pnlAddress.ResumeLayout(false);
            this.pnlAddress.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_usp)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numBalTransfer)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.ComponentModel.BackgroundWorker BW_Load_REO_Properties;
        private System.ComponentModel.BackgroundWorker BW_Load_USP;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Button button7;
        private System.ComponentModel.BackgroundWorker BW_Load_VDR;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.ComponentModel.BackgroundWorker BW_LoadTasksCompleted;
        private System.Windows.Forms.WebBrowser pdfviewer;
        private System.Windows.Forms.ToolStripMenuItem functionsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dashboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem dailyReportToolStripMenuItem;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.ToolStripMenuItem queueToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.ComboBox cmboBx_UspOnInvoice;
        private System.Windows.Forms.Label lbl_UspOnInvoice;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.ToolStripMenuItem toolsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem uSPWebsiteInformationToolStripMenuItem;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox chkBx_NoPropertyAddress;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.LinkLabel linkLabel2;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.Panel panel_callout;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button btn_AddMorePaymentBreakdown;
        private System.Windows.Forms.ToolStripMenuItem eMSDataToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.CheckBox chkbx_noZipCode;
        private System.Windows.Forms.Panel panel_acctNumber;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ToolStripMenuItem realResolutionToolStripMenuItem;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.DataGridView dgv_usp;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lbl_phoneNumber;
        private System.Windows.Forms.Label lbl_serviceProviderName;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_noMatchUSP;
        private System.Windows.Forms.Panel panel_specialInstructions;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btn_specialInstructions;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.ToolStripMenuItem testModeToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem normalToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem testToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.CheckBox checkBox21;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.TextBox textBox_amount;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.TextBox textBox_accountNumber;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.ToolStripMenuItem auditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem duplicateApprovalFormToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem unreadableInvoiceApprovalFormToolStripMenuItem;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.Panel panel_specialinstructions2;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.CheckBox checkBox22;
        private System.Windows.Forms.ToolStripMenuItem funnelReportToolStripMenuItem;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.ToolStripMenuItem forApprovalAmount1000ToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.CheckBox checkBox24;
        private System.Windows.Forms.CheckBox checkBox23;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPropertyCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFullAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn cInvestorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cReosrcDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cReoslcDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cReosfcDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAccountNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPodId;
        private System.Windows.Forms.DataGridViewTextBoxColumn cproperty_status_change_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn cinactivedate;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.ToolStripMenuItem auditFormToolStripMenuItem;
        private System.Windows.Forms.CheckBox checkBox25;
        private System.Windows.Forms.ToolStripMenuItem incompleteVIDRequestsToolStripMenuItem;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.ToolStripMenuItem invoiceTimelineToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem partialDuplicatesReportRESIToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem auditDashboardToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem qADashboardToolStripMenuItem;
        private System.Windows.Forms.Panel pnlAddress;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.CheckBox chkTransAmtNo;
        private System.Windows.Forms.CheckBox chkTransAmtYes;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.CheckBox chkASFIYes;
        private System.Windows.Forms.CheckBox chkASFINo;
        private System.Windows.Forms.ComboBox cmbBillName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Label lblAdrsNotif;
        private System.Windows.Forms.NumericUpDown numBalTransfer;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.CheckBox chkBalInfoYes;
        private System.Windows.Forms.CheckBox chkBalInfoNo;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.CheckBox chkBalAmtYes;
        private System.Windows.Forms.CheckBox chkBalAmtNo;
        private System.Windows.Forms.Label lblCount;
        private System.Windows.Forms.Label lblOf;
        private System.Windows.Forms.Label lblTransferCount;
        private System.Windows.Forms.TextBox txtTotal;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.CheckBox chknYes;
        private System.Windows.Forms.CheckBox chknNo;
        private System.Windows.Forms.ToolStripMenuItem uSPsForMPDToolStripMenuItem;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.TextBox txtUSPZipcode;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Button btnSearchUSP;
        private System.Windows.Forms.TextBox txtUSPCity;
        private System.Windows.Forms.TextBox txtUSPState;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.TextBox txtUSPStrt;
        private System.Windows.Forms.DataGridViewTextBoxColumn vdr_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn vdr_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn vdr_address;
        private System.Windows.Forms.DataGridViewTextBoxColumn vdr_status;
        private System.Windows.Forms.DataGridViewTextBoxColumn vms_status;
        private System.Windows.Forms.ToolStripMenuItem assignTaskFormToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem skipToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem invoiceInquiryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ndLevelDuplicateInvoiceReviewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem counterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem eMSFunnelBetaToolStripMenuItem;
        private System.Windows.Forms.Button btnCancelUpload;
        private System.Windows.Forms.ToolStripMenuItem eMSSLAToolStripMenuItem;
    }
}

