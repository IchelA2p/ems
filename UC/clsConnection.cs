﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Configuration;
using System.Diagnostics;
using System.Data.SqlClient;



public class clsConnection
{
    //Dim ConString As String = "Server=DAV8DBHSND01;Initial Catalog=MIS_MLA_UAT;Persist Security Info=True;User ID=mis414;Password=juchUt4a"
    //public clsFunctions FUNC = new clsFunctions();

    //string connStr = ConfigurationManager.ConnectionStrings["MISConnectionString"].ConnectionString.ToString();
    string connStr = "";
    SqlConnection con;
    SqlCommand cmd;
    SqlDataAdapter da;
    //Dim clsmail As New clsEmail


    private SqlCommand _command = new SqlCommand();

    public void setConn(string con)
    {
        if (con == "PIV")
        {
            connStr = "Data Source=PIV8DBMISNP01;Initial Catalog = MIS_ALTI;Integrated Security=True";
        }
        else
        {
            connStr = "Data Source=DAV8DBHSND01;Initial Catalog=MIS_MLA_UAT;Persist Security Info=True;User ID=mis414;Password=juchUt4a;Connection Timeout=0";
        }
    }

    public int testConnection()
    {
        int rtn;
        rtn = 0;
        try
        {
            string qry = "";
            con = new SqlConnection(connStr);
            con.Open();
            if (con.State == ConnectionState.Open)
            {
                rtn = 1;
                con.Close();
            }
            else
            {
                rtn = 0;
            }

        }
        catch (Exception e)
        {
            return 0;
        }

        return rtn;
    }

    public DataTable GetData(string Query)
    {

        DataTable dt = new DataTable();
        try
        {

            setConn("PIV");

            con = new SqlConnection(connStr);
            con.Open();
            da = new SqlDataAdapter(Query, con);
            da.Fill(dt);
            con.Close();
            return dt;
        }
        catch (Exception ex)
        {
            return dt;
        }
    }

    public DataSet GetDataSet(string strQuery)
    {

        setConn("PIV");

        con = new SqlConnection(connStr);

        try
        {
            SqlCommand mycmd = new SqlCommand(strQuery, con);

            mycmd.CommandTimeout = 3000;

            mycmd.CommandType = CommandType.Text;


            SqlDataAdapter myDataAdapter = new SqlDataAdapter(mycmd);

            DataSet myDataSet = new DataSet();

            myDataAdapter.Fill(myDataSet);

            myDataAdapter.Dispose();

            return myDataSet;
        }
        catch (SqlException ex)
        {
            //Interaction.MsgBox(ex.Message + Constants.vbCr + "Contact administrator.", MsgBoxStyle.Critical, "Error!");
            return null;
        }
        finally
        {
            con.Close();
        }
    }


    public int ExecuteQuery(string query)
    {

        int i = 0;

        try
        {
            setConn("PIV");

            con = new SqlConnection(connStr);
            con.Open();
            _command.Connection = con;
            _command.CommandType = CommandType.Text;
            _command.CommandText = query;
            //_command.CommandTimeout = 180

            i = _command.ExecuteNonQuery();
            _command.Dispose();
        }
        catch (Exception ex)
        {
            i = 0;
            //clsmail.CreateSendEmail("An error occurred while exceing the query..." & vbCrLf & vbCrLf & query & vbCrLf & vbCrLf & ex.ToString)
        }

        return i;
    }


    public void SQLBulkCopy(string table, DataTable dtb)
    {
        con = new SqlConnection(connStr);
        con.Open();

        using (SqlBulkCopy copy = new SqlBulkCopy(con))
        {
            copy.DestinationTableName = table;
            //copy.BulkCopyTimeout = 120
            copy.WriteToServer(dtb);
        }
    }

    public int ExecuteScalarQuery(string query)
    {

        int i = 0;

        try
        {
            con = new SqlConnection(connStr);
            con.Open();
            _command.Connection = con;
            _command.CommandType = CommandType.Text;
            _command.CommandText = query;
            //_command.CommandTimeout = 180

            i = Convert.ToInt32(_command.ExecuteScalar());
            _command.Dispose();
            con.Close();
        }
        catch (Exception ex)
        {
            i = 0;
            //clsmail.CreateSendEmail("An error occurred while exceing the query..." & vbCrLf & vbCrLf & query & vbCrLf & vbCrLf & ex.ToString)
        }

        return i;
    }

}
