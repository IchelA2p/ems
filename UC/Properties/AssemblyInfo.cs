﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Expense Management Software")]
[assembly: AssemblyDescription("Encodes bills processed by the expense management team")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Altisource Business Solutions")]
[assembly: AssemblyProduct("Expense Management Software")]
[assembly: AssemblyCopyright("Jonathan Elayron")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("a0a1ec44-0803-4595-88ec-75e61172b706")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("6.8.5.5")]
[assembly: AssemblyFileVersion("6.8.5.5")]
[assembly: NeutralResourcesLanguageAttribute("en")]
