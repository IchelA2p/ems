﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UC
{
    public class Constants
    {
        public string[] investor_status = new string[3]{ "REOSRC","REOSLC","REOSFC"};


        public const string resi_complete = "RESI Complete";
        public const string inactive_complete = "Inactive Complete";
        public const string callout_complete = "Call-Out Complete";
        public const string forwardtouc_complete = "Forward To UC Complete";
        public const string vendoridrequest_complete = "Vendor Id Request Complete";
        public const string pendingactionfromuc_complete = "Pending Action from UC Complete";
        public const string trailingasset_complete = "Trailing Asset Complete";
        public const string uspissue_complete = "USP Issue Complete";
        public const string noactionneeded_complete = "No Action Needed Complete";
        public const string done_complete = "Done Complete";

        public const string notutilitybill = "Not a Utility Bill";
        public const string blankinvoice = @"Blank/Unreadable/Cut Invoice";

        public const double callout = 0;
        public const double complete = 1;
        public const double trailingasset = 2;
        public const double propertynotfound = 3;
        public const double uspissue = 4;
        public const double done = 5;
        public const double noactionneeded = 6;
        public const double pendingactionfromuc = 7;
        public const double forwardtouc = 8;
        public const double vendoridrequest = 9;
        public const double resi = 10;
        public const double inactive = 11;
        public const double newinvoice = 20;

        public const string AtlantaInvoiceFolderRoot = @"\\corp.ocwen.com\data\bangalore\commonshare\ExpenseManagement\Expenses\";
        public const string NonHSBCFolder = @"\\corp.ocwen.com\data\bangalore\commonshare\ExpenseManagement\Expenses\Non HSBC Invoices\";

        public const string connectionString = @"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=600;";


        public const string CompleteFolder = @"\\ascorp.com\data\Bangalore\CommonShare\Strategic$\Utility Invoices\EM_TOOL_BulkUpload\";
        public const string NoActionNeededFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\NOACTIONNEEDED\";
        public const string TempFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\ONGOING\";
        public const string CallOutFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\CALLOUT\";
        public const string PropertyNotFoundFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\PROPERTYNOTFOUND\";
        public const string RR_NEW = @"\\PIV8FSASNP01\CommonShare\EMProject\RR\NEW\";
        public const string HSBC_NEW = @"\\PIV8FSASNP01\CommonShare\EMProject\HSBC\NEW\";
        public const string TrailingAssetFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\TRAILINGASSET\";
        public const string RSCFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\RSCAPPROVAL\";
        public const string InactiveFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\INACTIVE\";
        public const string SourceFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\";

        
    }

}
