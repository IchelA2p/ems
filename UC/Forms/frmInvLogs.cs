﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms
{
    public partial class frmInvLogs : Form
    {


        public frmInvLogs(string invoicename)
        {
            InitializeComponent();

            string qry = "";

            qry = string.Format(@"select id, property_code, case when PendingReasons = 'New Invoice' then 'Invoice Received' when PendingReasons = 'Pending for Audit' and isLatestData = 0 then 'Encoding' else PendingReasons end [Queue], 
                                case when PendingReasons = 'Pending for Audit' and isLatestData = 1 then GETDATE() else LastDateModified end [Date Processed], 
                                case when PendingReasons = 'Pending for Audit' and isLatestData = 1 then NULL else userid end [Processed By]
                                from tbl_ADHOC_EM_Tasks
                                where Invoice = '{0}' 

                                union all

                                select id, property_code, case when PendingReasons = 'New Invoice' then 'Invoice Received' when PendingReasons = 'Pending for Audit' then 'QA' else PendingReasons end [Queue], 
                                AuditDate [Date Processed], 
                                AuditedBy [Processed By]
                                from tbl_ADHOC_EM_Tasks
                                where Invoice = '{0}'
                                and AuditDate is not null
                                order by [Date Processed]", invoicename);

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            DataTable dtf = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(qry, conn);

            conn.Open();

            da.Fill(dt);

            conn.Close();

            dgList.DataSource = dt;
        }


        private void frmInvLogs_Load(object sender, EventArgs e)
        {


        }
    }
}
