﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms.Tools
{
    public partial class frmUSPMPD : Form
    {
        public frmUSPMPD()
        {
            InitializeComponent();
        }

        private void frmUSPMPD_Load(object sender, EventArgs e)
        {
            string query = string.Empty;

            query = string.Format(@"select distinct RTRIM(a.Vendor_ID) [Vendor ID], RTRIM(a.Vendor_Name) [USP Name], RTRIM(RTRIM(a.[Address]) + ' ' + RTRIM(a.City) + ' ' + RTRIM(a.[State] + ' ' + RTRIM(a.Zipcode))) [Address], ISNULL(b.isMPD, 0) isMPD 
                                    from tbl_EM_VDR a 
                                    left join tbl_EM_MPDUSP b 
                                    on a.Vendor_ID = b.VendorID 
                                    where (a.Vendor_Name like '%{0}%' or a.Vendor_ID = '{1}') 
                                    order by [USP Name]", 
                                    txtUSP.Text.ToString().Replace("'", "''"), 
                                    txtVID.Text.ToString().Replace("'", "''"));

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            dgList.DataSource = dt;

            dgList.Columns["Add"].DisplayIndex = 5;
            dgList.Columns["Remove"].DisplayIndex = 5;
        }

        private void dgList_CellLeave(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string query = string.Empty;

            if (txtUSP.Text.ToString().Replace("'", "''") != string.Empty && txtVID.Text.ToString().Replace("'", "''") == string.Empty)
            {
                query = string.Format(@"select distinct RTRIM(a.Vendor_ID) [Vendor ID], RTRIM(a.Vendor_Name) [USP Name], RTRIM(RTRIM(a.[Address]) + ' ' + RTRIM(a.City) + ' ' + RTRIM(a.[State] + ' ' + RTRIM(a.Zipcode))) [Address], ISNULL(b.isMPD, 0) isMPD
                                    from vw_vid a 
                                    left join tbl_EM_MPDUSP b 
                                    on a.Vendor_ID = b.VendorID 
                                    where (a.Vendor_Name like '%{0}%') 
                                    order by [USP Name]",
                                   txtUSP.Text.ToString().Replace("'", "''"),
                                   txtVID.Text.ToString().Replace("'", "''"));
            };

            if (txtUSP.Text.ToString().Replace("'", "''") == string.Empty && txtVID.Text.ToString().Replace("'", "''") != string.Empty)
            {
                query = string.Format(@"select distinct RTRIM(a.Vendor_ID) [Vendor ID], RTRIM(a.Vendor_Name) [USP Name], RTRIM(RTRIM(a.[Address]) + ' ' + RTRIM(a.City) + ' ' + RTRIM(a.[State] + ' ' + RTRIM(a.Zipcode))) [Address], ISNULL(b.isMPD, 0) isMPD
                                    from vw_VID a 
                                    left join tbl_EM_MPDUSP b 
                                    on a.Vendor_ID = b.VendorID 
                                    where (a.Vendor_ID = '{1}') 
                                    order by [USP Name]",
                                   txtUSP.Text.ToString().Replace("'", "''"),
                                   txtVID.Text.ToString().Replace("'", "''"));
            };

            if (txtUSP.Text.ToString().Replace("'", "''") != string.Empty && txtVID.Text.ToString().Replace("'", "''") != string.Empty)
            {
                query = string.Format(@"select distinct RTRIM(a.Vendor_ID) [Vendor ID], RTRIM(a.Vendor_Name) [USP Name], RTRIM(RTRIM(a.[Address]) + ' ' + RTRIM(a.City) + ' ' + RTRIM(a.[State] + ' ' + RTRIM(a.Zipcode))) [Address], ISNULL(b.isMPD, 0) isMPD
                                    from vw_VID a 
                                    left join tbl_EM_MPDUSP b 
                                    on a.Vendor_ID = b.VendorID 
                                    where (a.Vendor_Name like '%{0}%' or a.Vendor_ID = '{1}') 
                                    order by [USP Name]",
                                   txtUSP.Text.ToString().Replace("'", "''"),
                                   txtVID.Text.ToString().Replace("'", "''"));
            };

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            dgList.DataSource = dt;
        }

        private void SearchVendor(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnSearch_Click(null, null);
            }
            else if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar) || char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            dgList.Columns["Add"].DisplayIndex = 4;
            dgList.Columns["Remove"].DisplayIndex = 4;
        }

        private void dgList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgList.Rows.Count > 0)
            {

                string query = string.Empty;

                if (dgList.Columns[e.ColumnIndex].Name == "Add")
                {
                    query = string.Format(@"if (select COUNT(*) from tbl_em_mpdusp where vendorid = '{0}') > 0
                                            begin
	                                            update tbl_em_mpdusp set isMPD = 1, Lastdatemodified = getdate(), ModifiedBy = '{1}' where vendorid = '{0}'
                                            end
                                            else
                                            begin
	                                            insert into tbl_em_mpdusp(vendorid, ismpd, postedby, posteddate)
	                                            values ('{0}', 1, '{1}', GETDATE())
                                            end", dgList.Rows[e.RowIndex].Cells["Vendor ID"].Value, Environment.UserName);
                }

                if (dgList.Columns[e.ColumnIndex].Name == "Remove")
                {
                    query = string.Format(@"if (select COUNT(*) from tbl_em_mpdusp where vendorid = '{0}') > 0
                                            begin
	                                            update tbl_em_mpdusp set isMPD = 0, Lastdatemodified = getdate(), ModifiedBy = '{1}' where vendorid = '{0}'
                                            end
                                            else
                                            begin
	                                            insert into tbl_em_mpdusp(vendorid, ismpd, postedby, posteddate)
	                                            values ('{0}', 0, '{1}', GETDATE())
                                            end", dgList.Rows[e.RowIndex].Cells["Vendor ID"].Value, Environment.UserName);
                }


                if (query != string.Empty)
                {
                    SqlConnection sqlcon = new SqlConnection(Constants.connectionString);

                    try
                    {
                        sqlcon.Open();
                        SqlCommand sqlcom = new SqlCommand(query, sqlcon);
                        sqlcom.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Unable to save changes. Error: " + ex.Message);

                        sqlcon.Close();

                        return;
                    }

                    string query1 = string.Empty;

                    if (txtUSP.Text.ToString().Replace("'", "''") != string.Empty && txtVID.Text.ToString().Replace("'", "''") == string.Empty)
                    {
                        query1 = string.Format(@"select distinct RTRIM(a.Vendor_ID) [Vendor ID], RTRIM(a.Vendor_Name) [USP Name], RTRIM(RTRIM(a.[Address]) + ' ' + RTRIM(a.City) + ' ' + RTRIM(a.[State] + ' ' + RTRIM(a.Zipcode))) [Address], ISNULL(b.isMPD, 0) isMPD
                                    from vw_vid a 
                                    left join tbl_EM_MPDUSP b 
                                    on a.Vendor_ID = b.VendorID 
                                    where (a.Vendor_Name like '%{0}%') 
                                    order by [USP Name]",
                                           txtUSP.Text.ToString().Replace("'", "''"),
                                           txtVID.Text.ToString().Replace("'", "''"));
                    };

                    if (txtUSP.Text.ToString().Replace("'", "''") == string.Empty && txtVID.Text.ToString().Replace("'", "''") != string.Empty)
                    {
                        query1 = string.Format(@"select distinct RTRIM(a.Vendor_ID) [Vendor ID], RTRIM(a.Vendor_Name) [USP Name], RTRIM(RTRIM(a.[Address]) + ' ' + RTRIM(a.City) + ' ' + RTRIM(a.[State] + ' ' + RTRIM(a.Zipcode))) [Address], ISNULL(b.isMPD, 0) isMPD
                                    from vw_vid a 
                                    left join tbl_EM_MPDUSP b 
                                    on a.Vendor_ID = b.VendorID 
                                    where (a.Vendor_ID = '{1}') 
                                    order by [USP Name]",
                                           txtUSP.Text.ToString().Replace("'", "''"),
                                           txtVID.Text.ToString().Replace("'", "''"));
                    };

                    if (txtUSP.Text.ToString().Replace("'", "''") != string.Empty && txtVID.Text.ToString().Replace("'", "''") != string.Empty)
                    {
                        query1 = string.Format(@"select distinct RTRIM(a.Vendor_ID) [Vendor ID], RTRIM(a.Vendor_Name) [USP Name], RTRIM(RTRIM(a.[Address]) + ' ' + RTRIM(a.City) + ' ' + RTRIM(a.[State] + ' ' + RTRIM(a.Zipcode))) [Address], ISNULL(b.isMPD, 0) isMPD
                                    from vw_vid a 
                                    left join tbl_EM_MPDUSP b 
                                    on a.Vendor_ID = b.VendorID 
                                    where (a.Vendor_Name like '%{0}%' or a.Vendor_ID = '{1}') 
                                    order by [USP Name]",
                                           txtUSP.Text.ToString().Replace("'", "''"),
                                           txtVID.Text.ToString().Replace("'", "''"));
                    };

                    SqlConnection conn = new SqlConnection(Constants.connectionString);
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(query1, conn);

                    conn.Open();
                    da.SelectCommand.CommandTimeout = 1800;
                    da.Fill(dt);

                    conn.Close();

                    dgList.DataSource = dt;

                    dgList.Columns["Add"].DisplayIndex = 5;
                    dgList.Columns["Remove"].DisplayIndex = 5;
                }
            }
        }

        
    }
}
