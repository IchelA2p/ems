﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms.Tools
{
    public partial class USPWebInfo : Form
    {
        List<Database.DB.USPWebsite> listOfWebsite;

        public USPWebInfo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Form addUspWebsiteInfo = new UC.Forms.Tools.AddUSPWebInfo("ADD", null);
            addUspWebsiteInfo.ShowDialog();

            updatedgv();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int id = Convert.ToInt16(dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells["usp_id"].Value.ToString());
            
            Form addUspWebsiteInfo = new UC.Forms.Tools.AddUSPWebInfo("EDIT", listOfWebsite.First(p => p.uspWebsiteID == id));
            addUspWebsiteInfo.ShowDialog();

            updatedgv();
        }

        private void updatedgv()
        {
            Database.DBConnection.DatabaseUSPWebsite conn = new Database.DBConnection.DatabaseUSPWebsite();

            listOfWebsite = new List<Database.DB.USPWebsite>();

            listOfWebsite = conn.GetAllData();

            if (listOfWebsite != null)
            {
                dataGridView1.Rows.Clear();
                foreach (Database.DB.USPWebsite web in listOfWebsite)
                {
                    dataGridView1.Rows.Add(
                        web.uspWebsiteID,
                        web.uspName,
                        web.website,
                        web.username,
                        web.password,
                        web.isWorking == true ? "Yes" : "No",
                        web.updatedBy,
                        web.dateUpdated == DateTime.MinValue ? "" : web.dateUpdated.ToShortDateString(),
                        web.oldUsername,
                        web.oldPassword
                        );
                }
            }
        }

        private void USPWebInfo_Load(object sender, EventArgs e)
        {
            updatedgv();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            List<Database.DB.USPWebsite> web = new List<Database.DB.USPWebsite>();

            web.AddRange(listOfWebsite.FindAll(p => p.uspName.ToLower().Contains(textBox1.Text.ToLower().Trim())));

            if (web != null)
            {
                dataGridView1.Rows.Clear();

                foreach (Database.DB.USPWebsite wb in web)
                {
                    dataGridView1.Rows.Add(
                        wb.uspWebsiteID,
                        wb.uspName,
                        wb.website,
                        wb.username,
                        wb.password,
                        wb.isWorking == true ? "Yes" : "No",
                        wb.updatedBy,
                        wb.dateUpdated == DateTime.MinValue ? "" : wb.dateUpdated.ToShortDateString(),
                        wb.oldUsername,
                        wb.oldPassword
                        );
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) { return; }

            string username = dataGridView1.Rows[e.RowIndex].Cells["usp_username"].Value.ToString();
            string password = dataGridView1.Rows[e.RowIndex].Cells["usp_password"].Value.ToString();
            
            int id = Convert.ToInt32(dataGridView1.Rows[e.RowIndex].Cells[0].Value);

            string message = "Is the information correct? \nUsername: " + username + "\nPassword: " + password;
            var result = MessageBox.Show(message, "USP Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.No)
            {
                Database.DBConnection.DatabaseUSPWebsite webCon = new Database.DBConnection.DatabaseUSPWebsite();
                Database.DB.USPWebsite webInfo = webCon.GetData((uint)id);

                webInfo.isWorking = false;
                webCon.UpdateData(webInfo);

                var updateResult = MessageBox.Show("Do you want to update the username and password?",
                    "Update USP Information", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (updateResult == DialogResult.Yes)
                {
                    Form updateUspWebInfo = new Forms.Tools.AddUSPWebInfo("Update", listOfWebsite.First(p => p.uspWebsiteID == id));
                    updateUspWebInfo.Show();
                }

                updatedgv();
            }
        }
    }
}
