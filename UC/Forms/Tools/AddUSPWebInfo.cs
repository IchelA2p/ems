﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms.Tools
{
    public partial class AddUSPWebInfo : Form
    {
        string _action;
        Database.DB.USPWebsite _wb;

        public AddUSPWebInfo(string action, Database.DB.USPWebsite wb)
        {
            InitializeComponent();

            _action = action;

            button1.Text = action;

            _wb = new Database.DB.USPWebsite();
            _wb = wb;
        }

        private void AddUSPWebInfo_Load(object sender, EventArgs e)
        {
            if (_wb != null)
            {
                textBox1.Text = _wb.uspName;
                textBox2.Text = _wb.website;
                textBox3.Text = _wb.username;
                textBox4.Text = _wb.password;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (button1.Text == "ADD")
            {
                addUspWebsite();
            }
            else
            {
                updateUspWebsite();
            }
        }

        private void addUspWebsite()
        {
            Database.DB.USPWebsite info = new Database.DB.USPWebsite();
            Database.DBConnection.DatabaseUSPWebsite conn = new Database.DBConnection.DatabaseUSPWebsite();

            info.uspName = textBox1.Text.Trim();
            info.website = textBox2.Text.Trim();
            info.username = textBox3.Text.Trim();
            info.password = textBox4.Text.Trim();
            info.isWorking = true;
            info.dateUpdated = DateTime.Now;
            info.updatedBy = Environment.NewLine;
            info.oldUsername = string.Empty;
            info.oldPassword = string.Empty;

            try
            {
                conn.WriteData(info);
                MessageBox.Show("Successfully added USP Website Information");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void updateUspWebsite()
        {
            _wb.oldUsername = _wb.username;
            _wb.oldPassword = _wb.password;
            _wb.username = textBox3.Text.Trim();
            _wb.password = textBox4.Text.Trim();
            _wb.uspName = textBox1.Text.Trim();
            _wb.website = textBox2.Text.Trim();
            _wb.dateUpdated = DateTime.Now;
            _wb.updatedBy = Environment.UserName;
            _wb.isWorking = true;

            Database.DBConnection.DatabaseUSPWebsite conn = new Database.DBConnection.DatabaseUSPWebsite();

            try
            {
                conn.UpdateData(_wb);
                MessageBox.Show("Successfully updated USP Website Information");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
