﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms
{
    public partial class VendorIdRequest : Form
    {
        Database.DB.VendorIdRequest info;

        string VendorIdRequestFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\VENDORIDREQUEST\";

        string telephone = string.Empty;
        string email = string.Empty;

        Database.DB.EM_Tasks em_data;

        Form1 parentForm;
        Forms.Supervisor.Audit parentForm1;

        public VendorIdRequest(Form1 form, string type)
        {
            InitializeComponent();

            parentForm = form;

            em_data = parentForm.info;

            info = new Database.DB.VendorIdRequest();
            info.invoice_name = em_data.Invoice;

            info.type = type;

            cmbType.Text = type;

           

            if (type == "CREATE NEW")
            {
                textBox1.Text = "None";
                textBox1.ReadOnly = true;
            }
            else if (type == "ACTIVATION")
            {
                textBox1.Text = em_data.VendorId;
                textBox1.ReadOnly = false;

                cmbType.Text = type;
                textBox2.ReadOnly = true;
                txtCO.ReadOnly = true;
                txtBldg.ReadOnly = true;
                txtOffice.ReadOnly = true;
                txtDept.ReadOnly = true;
                txtPOBox.ReadOnly = true;
                textBox6.ReadOnly = true;
                comboBox3.Enabled = false;
                textBox5.ReadOnly = true;
                textBox7.ReadOnly = true;

                GetVIDDetails();
            }
        }

        public VendorIdRequest(Forms.Supervisor.Audit form, string type)
        {
            InitializeComponent();

            parentForm1 = form;

            em_data = parentForm1.data;

            info = new Database.DB.VendorIdRequest();
            info.invoice_name = em_data.Invoice;

            info.type = type;

            cmbType.Text = type;

            if (type == "CREATE NEW")
            {                
                textBox1.Text = "None";
                textBox1.ReadOnly = true;
            }
            else if (type == "ACTIVATION")
            {
                textBox1.Text = em_data.VendorId;
                textBox2.Text = em_data.usp_name;
                textBox1.ReadOnly = true;

                GetVIDRequests();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            if (textBox7.Text.Trim().Length < 5)
            {
                MessageBox.Show("Zipcode should be 5 digits.");
                return;
            }

            if (textBox3.Text.Trim().Length < 10)
            {
                MessageBox.Show("Telephone number should be 10 digits.");
                return;
            }

            if (textBox3.Text.ToString().Contains("99999") || textBox3.Text.ToString().Contains("88888") || textBox3.Text.ToString().Contains("77777") || textBox3.Text.ToString().Contains("66666") || textBox3.Text.ToString().Contains("55555") || textBox3.Text.ToString().Contains("44444") || textBox3.Text.ToString().Contains("33333") || textBox3.Text.ToString().Contains("22222") || textBox3.Text.ToString().Contains("11111") || textBox3.Text.ToString().Contains("00000") || textBox3.Text.ToString().Contains("12345") || textBox3.Text.ToString().Contains("98765"))
            {
                MessageBox.Show("Invalid telephone number.");

                return;
            }

            Database.DBConnection.DatabaseVendorIdRequest vdrConn = new Database.DBConnection.DatabaseVendorIdRequest();
            Database.DB.VendorIdRequest data = new Database.DB.VendorIdRequest();
            Database.DBConnection.DatabaseVDR_Data conn = new Database.DBConnection.DatabaseVDR_Data();
            Database.DB.VDR vdrData = new Database.DB.VDR();

            info.request_id = Convert.ToInt32(textBox1.Tag);
            info.vendor_account_requested = textBox1.Text;
            info.name = textBox2.Text;
            info.street_name = textBox6.Text;
            info.telephone = textBox3.Text;
            info.city = comboBox3.Text;
            info.email = textBox4.Text;
            info.postal_code = textBox7.Text;
            info.state = textBox5.Text;

            info.c_o = txtCO.Text;
            info.Bldg = txtBldg.Text;
            info.Office = txtOffice.Text;
            info.usp_dept = txtDept.Text;
            info.POBox = txtPOBox.Text;


            //info.type = "CREATE NEW";

            //info.type = cmbType.Text;

            if (info.type == "CREATE NEW")
            {
                save_request("NEW");
            }
            else if (info.type == "ACTIVATION")
            {
                save_request("New");
            }
            else
            {
                em_data.usp_name = info.name;
                em_data.VendorId = info.request_id.ToString();
                em_data.Complete = 9;

                em_data.InvoiceFolderName = strCreateInvoiceFolderName(VendorIdRequestFolder);
                em_data.PendingReasons = string.Format("Vendor ID Request: {0}", info.type);

                em_data.vendor_address = info.street_name + ", " + info.city + ", " + info.state + ", " + info.postal_code;

                MessageBox.Show("Data successfully saved!");
                this.Close();
            }

            //vdrData = null;
            //vdrData = conn.CheckIfForCreation1(info.vendor_account_requested);
            //if (vdrData == null) { info.type = "CREATE NEW"; }

            //vdrData = null;
            //vdrData = conn.CheckIfForUpdateInfo1(info.vendor_account_requested, info.name);
            //if (vdrData != null) { info.type = "UPDATE INFORMATION"; }

            //vdrData = null;
            //vdrData = conn.CheckIfForUpdateInfo2(info.vendor_account_requested, info.name);
            //if (vdrData != null) { info.type = "UPDATE INFORMATION"; }

            //vdrData = null;
            //vdrData = conn.CheckIfForCreation2(info.vendor_account_requested, info.name, info.street_name);
            //if (vdrData != null) { info.type = "CREATE NEW"; }

            //vdrData = null;
            //vdrData = conn.CheckIfForActivation1(info.vendor_account_requested, info.name, info.street_name);
            //if (vdrData != null) { info.type = "ACTIVATION"; }
                       
            ////------------------------------------------------
            ////          Check for duplicates
            ////------------------------------------------------
            //data = vdrConn.GetDuplicateRequest2(info.name, info.telephone, info.postal_code, info.state, info.email, info.city, info.street_name);

            //if (data == null)
            //{
            //    if (info.telephone == string.Empty ||
            //        info.email == string.Empty ||
            //        info.telephone == "none" ||
            //        info.email == "none")
            //    {
            //        save_request("AUDIT");
            //    }
            //    else
            //    {
            //        //save_request("OPEN");
            //        save_request("NEW");
            //    }
            //}
            //else
            //{

            //    //if (data.vmo_status == "REQ_COM")
            //    //{
            //    //    if (data.vendor_account_created != string.Empty)
            //    //    {
            //    //        em_data.VendorId = data.vendor_account_created;
            //    //    }
            //    //    else
            //    //    {
            //    //        em_data.VendorId = data.vendor_account_requested;
            //    //    }

            //    //    em_data.usp_name = textBox2.Text;
            //    //    em_data.VendorAssignedStatus = "NEW";

            //    //    MessageBox.Show("Saved!");

            //    //    this.Close();
            //    //}
            //    //else
            //    //{
            //    //    save_request("PENDING");
            //    //}

            //    if (data.vmo_status == "REQ_COM")
            //    {
            //        if (data.vendor_account_created != string.Empty)
            //        {
            //            em_data.VendorId = data.vendor_account_created;
            //        }
            //        else
            //        {
            //            em_data.VendorId = data.vendor_account_requested;
            //        }

            //        em_data.usp_name = textBox2.Text;
            //        em_data.VendorAssignedStatus = "NEW";
            //    }
            //    else if (data.vmo_status == "REQ_PEND")
            //    {
            //        save_request("PENDING");
            //    }
            //    else if (data.vmo_status == "REQ_INC")
            //    {
            //        save_request("DECLINED");

            //        em_data.VendorId = string.Empty;
            //        em_data.PendingReasons = "Vendor ID Request: DECLINED";
            //    }
            //    else
            //    {
            //        save_request("PENDING");
            //    }
                
            //    MessageBox.Show("Saved!");

            //    this.Close();
            //}
        }

        private void save_request(string status)
        {
            Database.DBConnection.DatabaseVendorIdRequest conn = new Database.DBConnection.DatabaseVendorIdRequest();

            info.vendor_account_requested = textBox1.Text;
            info.notes = "Put comments here...";
            info.username = Environment.UserName;
            info.last_date_modified = DateTime.Now;
            info.userid_requestor = Environment.UserName;
            info.date_requested = DateTime.Now;
            info.name = textBox2.Text.Trim();
            info.street_name = textBox6.Text.Trim();
            info.city = comboBox3.Text.Trim();
            info.state = textBox5.Text.Trim().ToUpper();
            info.postal_code = textBox7.Text;
            info.telephone = textBox3.Text;
            info.email = textBox4.Text;
            info.status = status;

            if (!string.IsNullOrEmpty(info.name) &&
                !string.IsNullOrEmpty(info.street_name) &&
                !string.IsNullOrEmpty(info.city) &&
                !string.IsNullOrEmpty(info.state) &&
                !string.IsNullOrEmpty(info.postal_code) &&
                !string.IsNullOrEmpty(info.telephone))
            {
                try
                {


                    string name = string.Empty;
                    string strt = string.Empty;

                    char[] delimiterChars = { ' ', '\t' };

                    name = info.name.ToLower();
                    strt = info.street_name.ToLower();

                    string[] words = name.Split(delimiterChars);
                    string x = "%";

                    foreach (string w in words)
                    {
                        x += w + "%";
                    }

                    string[] street = strt.Split(delimiterChars);
                    string y = "%";

                    foreach (string z in street)
                    {
                        y += z + "%";
                    }


                    string query = "";

                    if (cmbType.Text == "ACTIVATION")
                    {
                        query = string.Format(@"select COUNT(*) from tbl_EM_VendorIDRequest 
                                                where ([status] in ('NEW', 'OPEN') or [status] = 'COMP_APP' and ISNULL(vmo_status, '') IN ('', 'REQ_PEND')) 
                                                and [type] = '{7}' 
                                                and (vendor_account_requested = '{0}' or
                                                (name like '{1}' and
                                                (street_name like '%''' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('{2}', 'PO', ''), 'P O', ''), 'P.O', ''), 'box', ''), ' ', '') + '''%'
                                                or street_name like '%{2}%'
                                                or street_name like '{4}'
                                                or pobox = '{3}')
                                                and [state] = '{5}'
                                                and postal_code = '{6}'))",
                                                    textBox1.Text,
                                                    x.Replace("'", "''"),
                                                    strt,
                                                    txtPOBox.Text,
                                                    y.Replace("'", "''"),
                                                    textBox5.Text,
                                                    textBox7.Text,
                                                    cmbType.Text);

                    }
                    else
                    {
                        string.Format(@"select COUNT(*) from tbl_EM_VendorIDRequest 
                                                where ([status] in ('NEW', 'OPEN') or [status] = 'COMP_APP' and ISNULL(vmo_status, '') IN ('', 'REQ_PEND')) 
                                                and
                                                (name like '{1}' and
                                                (street_name like '%''' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('{2}', 'PO', ''), 'P O', ''), 'P.O', ''), 'box', ''), ' ', '') + '''%'
                                                or street_name like '%{2}%'
                                                or street_name like '{4}'
                                                or pobox = '{3}')
                                                and [state] = '{5}'
                                                and postal_code = '{6}'))",
                                                    textBox1.Text,
                                                    x.Replace("'", "''"),
                                                    strt,
                                                    txtPOBox.Text,
                                                    y.Replace("'", "''"),
                                                    textBox5.Text,
                                                    textBox7.Text,
                                                    cmbType.Text);

                    }

                    SqlConnection conn1 = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
                    DataTable dt = new DataTable();
                    SqlDataAdapter da = new SqlDataAdapter(query, conn1);

                    conn1.Open();
                    da.SelectCommand.CommandTimeout = 1800;
                    da.Fill(dt);

                    if (dt.Rows[0][0].ToString() != "0")
                    {
                        MessageBox.Show("Vendor ID request already exists.");

                        return;
                    }

                    conn.WriteData(info);

                    em_data.usp_name = info.name;
                    em_data.VendorId = info.request_id.ToString();
                    em_data.Complete = 9;
                    em_data.VendorId = null;
                    em_data.vendor_address = info.street_name + ", " + info.city + ", " + info.state + ", " + info.postal_code;


                    //em_data.VendorAssignedStatus = "For Vendor ID Request";
                    em_data.InvoiceFolderName = strCreateInvoiceFolderName(VendorIdRequestFolder);
                    em_data.PendingReasons = string.Format("Vendor ID Request: {0}", info.type);

                    //em_data.Complete = 21;
                    //em_data.PendingReasons = "Pending for Audit";

                    MessageBox.Show("Data successfully saved!");
                    this.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to save data. Error: " + ex.Message);
                }
            }
            else
            {
                MessageBox.Show("Please re-enter all information");

            }
        }

        public string strCreateInvoiceFolderName(string folder)
        {
            string strDate = DateTime.Now.ToString("MM-dd-yyyy");
            return string.Format(@"{0}{1}\{2}.pdf", folder, strDate, em_data.Invoice);
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                textBox4.Enabled = false;
                textBox4.Text = "none";
            }
            else
            {
                textBox4.Enabled = true;
            }
        }

        //private void textBox4_TextChanged(object sender, EventArgs e)
        //{
        //    info.email = textBox4.Text;
        //    this.email = textBox4.Text;
        //    EnableSaveButton(null, null);
        //}

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            info.telephone = textBox3.Text;
            this.telephone = textBox3.Text;

            EnableSaveButton(null, null);
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked == true)
            {
                textBox3.Text = "none";
                textBox3.Enabled = false;
                info.telephone = "none";
            }
            else
            {
                textBox3.Enabled = true;
                textBox3.Text = this.email;
                info.telephone = this.email;
            }
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            info.postal_code = textBox7.Text;

            if (textBox7.TextLength > 3)
            {
                Database.DBConnection.DatabaseZipcodeStateCity conn = new Database.DBConnection.DatabaseZipcodeStateCity();
                List<Database.DB.ZipcodeStateCity> dataList =  conn.GetCityStates(textBox7.Text);

                if (dataList != null)
                {
                    comboBox3.Items.Clear();

                    foreach (Database.DB.ZipcodeStateCity data in dataList)
                    {
                        comboBox3.Items.Add(data.city_name);
                        textBox5.Text = data.abbrev_state;
                    }
                }
            }

            EnableSaveButton(null, null);
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsControl(e.KeyChar) || char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void EnableSaveButton(object sender, EventArgs e)
        {
            bool hasBlank = false;

            foreach (Control c in groupBox1.Controls)
            {
                if (c.GetType() == typeof(TextBox) || c.GetType() == typeof(ComboBox))
                {
                    if (c.Text == string.Empty && c.Name != "textBox4" && c.Name != "txtCO" && c.Name != "txtBldg" && c.Name != "txtOffice" && c.Name != "txtDept" && c.Name != "txtPOBox")
                    {
                        hasBlank = true;
                    }
                }
            }

            button1.Enabled = !hasBlank;

            textBox2.CharacterCasing = CharacterCasing.Upper;

            textBox2.Text = textBox2.Text.ToString().Replace("  ", " ");

            GetVIDRequests();
        }

        private void textBox5_Leave(object sender, EventArgs e)
        {
            textBox5.Text = textBox5.Text.ToUpper();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '\'')
            {
                e.Handled = true;
            }
            else
            {
                e.Handled = false;
            }

        }

        private void VendorIdRequest_Load(object sender, EventArgs e)
        {

        }

        private void cmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbType.Text == "CREATE NEW")
            {
                textBox1.Enabled = false;

                textBox1.Text = "None";
                textBox2.Text = string.Empty;
                textBox6.Text = string.Empty;
                comboBox3.Text = string.Empty;
                textBox5.Text = string.Empty;
                textBox7.Text = string.Empty;
                textBox3.Text = string.Empty;
            }
        }

        private void GetVIDRequests()
        {

            if (textBox2.Text.Length > 2)
            {

                string name = string.Empty;
                string strt = string.Empty;

                char[] delimiterChars = { ' ', '\t' };

                name = textBox2.Text.ToLower();
                strt = textBox6.Text.ToLower();

                string[] words = name.Split(delimiterChars);
                string x = "%";

                foreach (string w in words)
                {
                    x += w + "%";
                }

                string[] street = strt.Split(delimiterChars);
                string y = "%";

                foreach (string z in street)
                {
                    y += z + "%";
                }

                string query = string.Format(@"SELECT request_id , [type], vendor_account_created, name, c_o, Bldg, Office, usp_dept, POBox, street_name, city, [state], postal_code, telephone, date_requested, userid_requestor, [status], com_decline_reason, vmo_status, decline_reason, notes 
                                    FROM tbl_EM_VendorIDRequest
                                    WHERE ([status] in ('NEW', 'OPEN') or [status] = 'COMP_APP' and ISNULL(vmo_status, '') IN ('', 'REQ_PEND')) and name like '{0}' and ",
                                        x.Replace("'", "''"));

                if (textBox6.Text != string.Empty)
                {
                    query += string.Format("street_name like '{0}' and ", y);
                }

                if (comboBox3.Text != string.Empty)
                {
                    query += string.Format("city like '%{0}%' and ", comboBox3.Text);
                }


                if (textBox5.Text != string.Empty)
                {
                    query += string.Format("[state] like '%{0}%' and ", textBox5.Text);
                }


                if (textBox7.Text != string.Empty)
                {
                    query += string.Format("postal_code like '%{0}%' and ", textBox7.Text);
                }


                query = query.ToString().Substring(0, query.Length - 4) + "order by date_requested";

                SqlConnection conn = new SqlConnection(Constants.connectionString);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);

                conn.Open();
                da.SelectCommand.CommandTimeout = 1800;
                da.Fill(dt);

                conn.Close();

                dgList.DataSource = dt;
            }
        }


        private void GetVIDDetails()
        {
            string query = string.Format(@"select * from tbl_EM_VDR where Vendor_ID = '{0}' ", textBox1.Text.Trim());

                SqlConnection conn = new SqlConnection(Constants.connectionString);
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);

                conn.Open();
                da.SelectCommand.CommandTimeout = 1800;
                da.Fill(dt);

                conn.Close();

                if (dt.Rows.Count > 0)
                {
                    textBox2.Text = dt.Rows[0]["vendor_name"].ToString().Trim();
                    textBox6.Text = dt.Rows[0]["address"].ToString().Trim();
                    comboBox3.Text = dt.Rows[0]["City"].ToString().Trim();
                    textBox5.Text = dt.Rows[0]["State"].ToString().Trim();
                    textBox7.Text = dt.Rows[0]["Zipcode"].ToString().Trim();
                    //txtCO.Text = dt.Rows[0]["c_o"].ToString().Trim();
                    //txtBldg.Text = dt.Rows[0]["Bldg"].ToString().Trim();
                    //txtOffice.Text = dt.Rows[0]["Office"].ToString().Trim();
                    //txtDept.Text = dt.Rows[0]["usp_dept"].ToString().Trim();
                    //txtPOBox.Text = dt.Rows[0]["POBox"].ToString().Trim();

                    textBox2.ReadOnly = true;
                    textBox6.ReadOnly = true;
                    comboBox3.Enabled = false;
                    textBox5.ReadOnly = true;
                }
                else
                {
                    textBox2.Text = string.Empty;
                    textBox6.Text = string.Empty;
                    comboBox3.Text = string.Empty;
                    textBox5.Text = string.Empty;
                    textBox7.Text = string.Empty;

                    txtCO.Text = string.Empty;
                    txtBldg.Text = string.Empty;
                    txtOffice.Text = string.Empty;
                    txtDept.Text = string.Empty;
                    txtPOBox.Text = string.Empty;

                    textBox2.ReadOnly = true;
                    textBox6.ReadOnly = true;
                    comboBox3.Enabled = true;
                    textBox5.ReadOnly = true;
                }
        }

        private void dgList_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {

            if (dgList.RowCount > 0)
            {

                info.type = "TAG PENDING VID REQUEST";
                cmbType.Text = "TAG PENDING VID REQUEST";

                textBox1.Text = "None";
                textBox1.Tag = dgList.CurrentRow.Cells["request_id"].Value.ToString().Trim();
                textBox2.Text = dgList.CurrentRow.Cells["name"].Value.ToString().Trim();
                textBox6.Text = dgList.CurrentRow.Cells["street_name"].Value.ToString().Trim();
                comboBox3.Text = dgList.CurrentRow.Cells["city"].Value.ToString().Trim();
                textBox5.Text = dgList.CurrentRow.Cells["state"].Value.ToString().Trim();
                textBox7.Text = dgList.CurrentRow.Cells["postal_code"].Value.ToString().Trim();
                textBox3.Text = dgList.CurrentRow.Cells["telephone"].Value.ToString().Trim();

                EnableSaveButton(sender, e);
            }
        }

       
    }
}
