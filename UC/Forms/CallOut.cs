﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;

namespace UC.Forms
{
    public partial class CallOut : Form
    {
        //Network Path
        public string CompleteFolder = @"\\ascorp.com\data\Bangalore\CommonShare\Strategic$\Utility Invoices\EM_TOOL_BulkUpload\";
        public string CallOutFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\CALLOUT\";
        public string SourceFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\";
        public string TempFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\ONGOING\";
        public string TrailingAssetFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\TRAILINGASSET\";
        public string PropertyNotFoundFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\PROPERTYNOTFOUND\";
        //Update 9/27/2014 - Enables VDR request to go directly to bulk upload
        public string VendorIdRequestFolder = @"\\ascorp.com\data\Bangalore\CommonShare\Strategic$\Utility Invoices\EM_TOOL_BulkUpload\"; 
        public string DoneFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\DONE\";
        public string NoActionNeededFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\NOACTIONNEEDED\";
        public string PendingActionFromUCFolder = @"\\ascorp.com\data\Bangalore\CommonShare\Strategic$\Utility Invoices\EM_TOOL_BulkUpload\";
        public string ForwardToUCFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\FORWARDTOUC\";
        public string ResiFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\RESI\";
        public string InactiveFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\INACTIVE\";
        public string ResiCompleteFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\RESICOMPLETE\";
        public string ManualWIFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\ManualWI\";
        public string OngoingFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\ONGOING\";

        string[] listOfInvoices;
        List<string> strListOfUSP;
        List<Database.DB.EM_Tasks> listOfInvoiceData;
        List<Database.DB.EM_Tasks> listOfInvoiceToLoad;
        
        public int employeeId;

        public CallOut(int employeeId)
        {
            InitializeComponent();

            this.employeeId = employeeId;

            InitializeDGV1();
        }

        private void InitializeDGV1()
        {
            dataGridView1.Columns.Clear();
            dataGridView1.Rows.Clear();

            dataGridView1.Columns.Add("invoice", "Invoice");
            dataGridView1.Columns.Add("reason", "Reason for CallOut");
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dataGridView1.CurrentCell.RowIndex > -1)
            {
                try
                {
                    string invoice = dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells["invoice"].Value.ToString().Trim();
                    foreach (Database.DB.EM_Tasks data in listOfInvoiceData)
                    {
                        if (data.Invoice == invoice)
                        {
                            if (!(System.IO.File.Exists(data.InvoiceFolderName)))
                            {
                                throw new Exception("File was already processed");
                            }

                            //data.InvoiceFolderName = listOfInvoices.First(p => p.Contains(invoice));
                            LoadEMData(data);
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to load data. Error: " + ex.Message + "\nPlease refresh your list");

                }
            }
        }

        public Database.DB.EM_Tasks GetMostRecentData(string invoice)
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            List<Database.DB.EM_Tasks> listOfEmTasks = new List<Database.DB.EM_Tasks>();
            Database.DB.EM_Tasks mostRecentdata = new Database.DB.EM_Tasks();

            listOfEmTasks = conn.GetInvoice(invoice);
            int countOfData;

            if (listOfEmTasks != null && listOfEmTasks.Count() > 0)
            {
                countOfData = listOfEmTasks.Count();

                mostRecentdata = listOfEmTasks[0];
                for (int i = 0; i < countOfData; i++)
                {
                    if (mostRecentdata.LastDateModified < listOfEmTasks[i].LastDateModified)
                    {
                        mostRecentdata = listOfEmTasks[i];
                    }
                }
            }
            else
            {
                // Throw an error if there is no data from database
                throw new Exception("No data from database");
            }

            // Capture unexpected errors
            try
            {
                return mostRecentdata;
            }
            catch
            {
                return null;
            }
        }

        public void LoadEMData(Database.DB.EM_Tasks mostRecentData)
        {
            try
            {
                linkLabel1.Links.Clear();
                linkLabel1.Links.Add(0,12,mostRecentData.InvoiceFolderName);
                textBox4.Text = mostRecentData.Amount;
                textBox5.Text = mostRecentData.AccountNumber;

                dateTimePicker1.Value = mostRecentData.DueDate;
                dateTimePicker2.Value = mostRecentData.ServiceFrom;
                dateTimePicker3.Value = mostRecentData.ServiceTo;

            }
            catch
            {

            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            string url;
            if (e.Link.LinkData != null)
                url = e.Link.LinkData.ToString();
            else
                url = linkLabel1.Text.Substring(e.Link.Start, e.Link.Length);

                var si = new ProcessStartInfo(url);
                Process.Start(si);
                linkLabel1.LinkVisited = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            Database.DB.EM_Tasks mostRecentData = new Database.DB.EM_Tasks();
            string invoice = dataGridView1.Rows[dataGridView1.CurrentCell.RowIndex].Cells["invoice"].Value.ToString();
            mostRecentData = GetMostRecentData(invoice);

            try
            {
                switch (comboBox2.Text)
                {
                    case "Complete":
                        mostRecentData.Complete = 1;
                        mostRecentData.InvoiceFolderName = CompleteFolder + DateTime.Now.ToString("MM-dd-yyyy") + @"\" + mostRecentData.Invoice + ".pdf";
                        if (mostRecentData.PendingReasons == "Trailing Asset")
                        {
                            mostRecentData.InvoiceFolderName = TrailingAssetFolder + DateTime.Now.ToString("MM-dd-yyyy") + @"\" + mostRecentData.Invoice + ".pdf";
                        }

                        break;

                    case "Call Out":
                        mostRecentData.Complete = 0;
                        mostRecentData.InvoiceFolderName = CallOutFolder + DateTime.Now.ToString("MM-dd-yyyy") + @"\" + mostRecentData.Invoice + ".pdf";
                        break;

                    case "Trailing Asset":
                        mostRecentData.Complete = 2;
                        mostRecentData.InvoiceFolderName = TrailingAssetFolder + DateTime.Now.ToString("MM-dd-yyyy") + @"\" + mostRecentData.Invoice + ".pdf";
                        break;

                    case "Property Not In Tool":
                        mostRecentData.Complete = 3;
                        mostRecentData.InvoiceFolderName = PropertyNotFoundFolder + DateTime.Now.ToString("MM-dd-yyyy") + @"\" + mostRecentData.Invoice + ".pdf";
                        break;

                    case "USP Issue":
                        mostRecentData.Complete = 4;
                        break;

                    case "Zero or Neg Value":
                        mostRecentData.InvoiceFolderName = DoneFolder + DateTime.Now.ToString("MM-dd-yyyy") + @"\" + mostRecentData.Invoice + ".pdf";
                        mostRecentData.Complete = 5;
                        break;

                    case "Not Utility Bill":
                        mostRecentData.Complete = 6;
                        mostRecentData.InvoiceFolderName = NoActionNeededFolder + DateTime.Now.ToString("MM-dd-yyyy") + @"\" + mostRecentData.Invoice + ".pdf";
                        break;

                    case "Vendor ID Request":
                        mostRecentData.Complete = 9;
                        mostRecentData.InvoiceFolderName = VendorIdRequestFolder + DateTime.Now.ToString("MM-dd-yyyy") + @"\" + mostRecentData.Invoice + ".pdf";
                        break;

                    case "Manual WI Created":
                        comboBox2.Text = "Manual WI Created";
                        mostRecentData.Complete = 12;
                        mostRecentData.PendingReasons = comboBox3.Text;
                        mostRecentData.InvoiceFolderName = ManualWIFolder + DateTime.Now.ToString("MM-dd-yyyy") + @"\" + mostRecentData.Invoice + ".pdf";
                        break;
                }

                try
                {
                    mostRecentData.work_item = textBox1.Text.Trim();
                }
                catch
                {
                    mostRecentData.work_item = string.Empty;
                }

                // Save the data to database
                mostRecentData.EmployeePrimaryId = this.employeeId;
                mostRecentData.LastDateModified = DateTime.Now;

                // Write to database
                conn.WriteData(mostRecentData);

                // Prompt for successful confirmation
                MessageBox.Show("Data is saved successfully!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            // Count of task today 
            CountTaskToday();
        }

        private void CountTaskToday()
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            List<Database.DB.EM_Tasks> listOfTask = new List<Database.DB.EM_Tasks>();

            if (DateTime.Now < DateTime.Today.Date.AddHours(12) && DateTime.Now > DateTime.Today.Date.AddDays(-1).AddHours(12))
            {
                listOfTask = conn.GetListOfEmTasks(DateTime.Today.Date.AddDays(-1).AddHours(20), DateTime.Today.Date.AddHours(6));
            }
            else
            {
                listOfTask = conn.GetListOfEmTasks(DateTime.Today.Date.AddHours(20), DateTime.Today.Date.AddDays(1).AddHours(6));
            }

            try
            {
                label8.Text = listOfTask.FindAll(p => p.EmployeePrimaryId == this.employeeId).Count().ToString();
            }
            catch
            {
                label8.Text = "0";
            }

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            textBox1.Visible = false;
            label1.Visible = false;

            linkLabel3.Visible = false;
            comboBox3.Text = string.Empty;

            //Complete
            if (comboBox2.Text == "Call Out")
            {
                comboBox3.Enabled = true;
                comboBox3.Items.Clear();

                comboBox3.Items.Add("No Property/Service Address");
                comboBox3.Items.Add("No USP Address on Invoice");
                comboBox3.Items.Add("No Account Number");
                comboBox3.Items.Add("No Amount");
                comboBox3.Items.Add("No Type of Service");
                comboBox3.Items.Add("Diff Property Address");
                comboBox3.Items.Add("Diff USP Address");
            }
            else if (comboBox2.Text == "Complete")
            {
                comboBox3.Enabled = true;
                comboBox3.Items.Clear();

                comboBox3.Items.Add("Trailing Asset");
                comboBox3.Items.Add("High Amount");
                comboBox3.Items.Add("Duplicate");
                comboBox3.Items.Add("Stop Preservation Flag");
                comboBox3.Items.Add("Zero or Negative Value");
            }
            else if (comboBox2.Text == "Not Utility Bill")
            {
                comboBox3.Enabled = true;
                comboBox3.Items.Clear();

                comboBox3.Items.Add("Not a Utility Bill");
                comboBox3.Items.Add("Blank/Unreadable/Cut Invoice");
                
            }
            else if (comboBox2.Text == "Vendor ID Request")
            {
                comboBox3.Enabled = true;
                comboBox3.Items.Clear();

                comboBox3.Items.Add("No Vendor ID");
                comboBox3.Items.Add("Different Address");
                comboBox3.Items.Add("Inactive Vendor ID");
                comboBox3.Items.Add("Vendor ID Requested");

                linkLabel3.Visible = true;

            }
            else if (comboBox2.Text == "Manual WI Created")
            {
                comboBox3.Enabled = true;
                comboBox3.Items.Clear();

                comboBox3.Items.Add("E-Bills");
                comboBox3.Items.Add("Inactive");

                textBox1.Visible = true;
                label1.Visible = true;
            }
            else
            {
                comboBox3.Enabled = false;
                comboBox3.Items.Clear();
                comboBox3.Text = string.Empty;
            }
        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox3.Text == "RESI Manual WI")
            {
                textBox1.Visible = true;
                label1.Visible = true;
            }
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            LoadAllResources();
        }

        private void LoadAllResources()
        {
            // Clear datagridview
            ClearDGV1();

            // Get all invoices from Call-Out Folder
            GetListOfInvoices();

            // Get data of the invoices from Database
            GetInvoicesData();

            // Load USP
            LoadUSP();

            // Count of Task Today
            CountTaskToday();
        }

        private void ClearDGV1()
        {
            dataGridView1.Rows.Clear();
        }

        private void GetListOfInvoices()
        {
            // Get all invoices from Call-Out Folder
            this.listOfInvoices = System.IO.Directory.GetFiles(this.CallOutFolder, "*.pdf", SearchOption.AllDirectories);

            // Update the total list of invoice in the Call-Out Folder
            label13.Text = listOfInvoices.Count().ToString();
        }

        private void GetInvoicesData()
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            Database.DB.EM_Tasks InvoiceData = new Database.DB.EM_Tasks();
            listOfInvoiceData = new List<Database.DB.EM_Tasks>();
            strListOfUSP = new List<string>();

            if (listOfInvoices != null && listOfInvoices.Count() > 0)
            {
                foreach (string invoice in listOfInvoices)
                {
                    List<Database.DB.EM_Tasks> DataOfInvoice = new List<Database.DB.EM_Tasks>();
                    Database.DB.EM_Tasks mostRecentData = new Database.DB.EM_Tasks();
                    string InvoiceName = System.IO.Path.GetFileNameWithoutExtension(invoice);
                    string[] cleanInvoiceName = InvoiceName.Split('_');

                    // Get data of invoice
                    DataOfInvoice = conn.GetInvoice(cleanInvoiceName[0]);

                    // Get most recent data of invoice
                    mostRecentData = DataOfInvoice[0];
                    for (int i = 0; i < DataOfInvoice.Count(); i++)
                    {
                        if (mostRecentData.LastDateModified < DataOfInvoice[0].LastDateModified)
                        {
                            mostRecentData = DataOfInvoice[0];
                        }
                    }

                    // Add the list of USP
                    this.listOfInvoiceData.Add(mostRecentData);
                    if (!strListOfUSP.Contains(mostRecentData.usp_name))
                    {
                        strListOfUSP.Add(mostRecentData.usp_name);
                    }
                }
            }
        }

        public void LoadUSP()
        {
            Random rdm = new Random();
            int index = rdm.Next(0,strListOfUSP.Count());
            listOfInvoiceToLoad = new List<Database.DB.EM_Tasks>();

            string USPToLoad = strListOfUSP[index];

            // Load USP information from database
            try
            {
                Database.DBConnection.DatabaseUSPGroup conn = new Database.DBConnection.DatabaseUSPGroup();
                Database.DB.USPGroup data = new Database.DB.USPGroup();
                data = conn.GetDataProcessor(USPToLoad);
                label16.Text = data.USPGroupName;
                label18.Text = data.PhoneNo;
            }
            catch
            { 
                
            }
            
            label4.Text = USPToLoad;
            listOfInvoiceToLoad.AddRange(listOfInvoiceData.FindAll(p => p.usp_name == USPToLoad));


            int maxloadnumber = 10;
            int countOfInvoice = listOfInvoiceToLoad.Count();
            int numberOfInvoicesLoaded =0;
            if (countOfInvoice < 10)
            {
                maxloadnumber = countOfInvoice;
            }

            foreach (Database.DB.EM_Tasks dataToLoad in listOfInvoiceToLoad)
            {
                if (numberOfInvoicesLoaded < maxloadnumber)
                {
                    if (System.IO.File.Exists(dataToLoad.InvoiceFolderName))
                    {
                        // Moves file to ongoing folder
                        string strPath = Path.Combine(OngoingFolder, dataToLoad.Invoice + "_" + Environment.UserName + ".pdf");
                        System.IO.File.Move(dataToLoad.InvoiceFolderName, strPath);
                        listOfInvoiceData.Remove(dataToLoad);

                        // Change the address to the ongoing folder address
                        dataToLoad.InvoiceFolderName = strPath;
                        listOfInvoiceData.Add(dataToLoad);

                        dataGridView1.Rows.Add(dataToLoad.Invoice, dataToLoad.PendingReasons);
                        numberOfInvoicesLoaded++;
                    }
                }
            }

            MessageBox.Show(string.Format("Loaded {0} out of {1} invoices of {2}",
                numberOfInvoicesLoaded,
                countOfInvoice,
                USPToLoad),
                "Load Invoice",
                MessageBoxButtons.OK,
                MessageBoxIcon.Information);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            CountTaskToday();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

    }
}
