﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms
{
    public partial class frmInvoiceInquiry : Form
    {
        public frmInvoiceInquiry()
        {
            InitializeComponent();
        }

        private void frmInvoiceInquiry_Load(object sender, EventArgs e)
        {
            cmbState.SelectedIndex = 0;
            cmbUtility.SelectedIndex = 0;
            getVendorList();
            getUserList();

            //dgPayments.Columns["Type"].DisplayIndex = 0;
        }

        private void getVendorList()
        {

            string query = string.Format(@"select distinct rtrim(ltrim(usp_name)) usp_name from tbl_ADHOC_EM_Tasks where isnull(usp_name, '') <> '' and ISNUMERIC(usp_name) = 0 and usp_name not in ('', '', '', '', '', '', '') order by rtrim(ltrim(usp_name))");

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();

            da.Fill(dt);

            conn.Close();

            cmbUSP.DataSource = dt;
            cmbUSP.DisplayMember = "usp_name";
            cmbUSP.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbUSP.AutoCompleteSource = AutoCompleteSource.ListItems;

            cmbUSP.SelectedIndex = -1;
        }

        private void getUserList()
        {

            string query = string.Format(@"select distinct userid from tbl_EM_Queue order by userid");

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();

            da.Fill(dt);

            conn.Close();

            cmbUserid.DataSource = dt;
            cmbUserid.DisplayMember = "userid";
            cmbUserid.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbUserid.AutoCompleteSource = AutoCompleteSource.ListItems;

            cmbUserid.SelectedIndex = -1;
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            dgPayments.DataSource = null;

            PopulateInvoices();
            PopulatePayments();

            dgPayments.CommitEdit(DataGridViewDataErrorContexts.Commit);
            dgPayments.Refresh();

            
            //for (int x = 0; x <= dgPayments.Rows.Count - 1; x++)
            //{
            //    if (dgPayments.Rows[x].Cells["isOvernight"].Value.ToString() == "1")
            //    {
            //        dgPayments.Rows[x].Cells["Type"].Value = true;
            //    }
            //    else
            //    {
            //        dgPayments.Rows[x].Cells["Type"].Value = false;
            //    }
            //}

            
            //    for (int y = 0; y <= dgPayments.Columns.Count - 1; y++)
            //    {
            //        if (dgPayments.Columns[y].Name != "Type")
            //        {
            //            dgPayments.Columns[y].ReadOnly = true;
            //        }
            //        else
            //        {
            //            try
            //            {
            //                dgPayments.Columns[y].ReadOnly = false;
            //            }
            //            catch { }
                       
            //        }
            //    }
        }

        private void PopulateInvoices()
        {
            int countofval = 0;
            string obj = string.Empty;

            foreach (var groupBox in Controls.OfType<GroupBox>())
            {
                foreach (var textBox in groupBox.Controls.OfType<TextBox>())
                {
                    if (textBox.Text != string.Empty)
                    { countofval += 1; };
                }

                foreach (var combobox in groupBox.Controls.OfType<ComboBox>())
                {
                    if (combobox.Text != string.Empty)
                    { countofval += 1; };
                }
            }

            bool param_address = false;
            string query = string.Empty;

            if (txtStrNum.Text.ToString().Trim() == string.Empty && txtStrName.Text.ToString().Trim() == string.Empty && txtCity.Text.ToString().Trim() == string.Empty && cmbState.Text.ToString().Trim() == string.Empty && cmbState.Text.ToString().Trim() == "--" && txtZipCode.Text == string.Empty)
            {
                param_address = false;
            }
            else
            {
                param_address = true;
            }

            query = @"select distinct top 200 a.property_code [Property Code], 
                    c.full_address [Service Address], c.customer_name [Client], 
                    b.vendor_group [Utility], case when e.REFDESC is null and b.PendingReasons like '%Manual WI%' then 'WI Creation' else e.REFDESC end [Queue], 
                    case when e.REFDESC in ('WI Creation', 'Work item creation') and b.VendorAssignedStatus = 'Unable to save the record. USP not found.' then 'VID is not available in VMS'
                    when e.REFDESC in ('WI Creation', 'Work item creation') and b.VendorAssignedStatus in ('New', 'USP is correct.', 'USP successfully assigned', 'USP successfully assigned; Utility is Exception') and b.date_bulkuploaded is null then b.PendingReasons
                    when e.REFDESC = 'WI Creation' and b.VendorAssignedStatus in ('USP is correct.', 'USP successfully assigned', 'USP successfully assigned; Utility is Exception') and b.date_bulkuploaded is not null and b.work_item = '' then 'VMS Bulk Upload Error'
                    else b.PendingReasons end [Pending Reason],
                    case when e.REFDESC in ('WI Creation', 'Work item creation') and b.VendorAssignedStatus = 'Unable to save the record. USP not found.' then 'VID tagging issue' 
                    when e.REFDESC in ('WI Creation', 'Work item creation') and b.VendorAssignedStatus in ('USP is correct.', 'USP successfully assigned', 'USP successfully assigned; Utility is Exception', '', 'Manual WI', 'New', 'Manual WI') and b.work_item = '' and b.date_bulkuploaded is not null then b.remarks
                    end [Bulk Upload Error],
                    b.work_item [Work Item], b.AccountNumber [Account No.], b.usp_name [Utility Service Provider], b.VendorId [Vendor ID], 
                    b.invoice_date [Bill Date], b.ServiceFrom [Service from], b.ServiceTo [Service to], b.DueDate [Due Date], 
                    b.Amount [Amount Due], b.PreviousBalance [Previous Balance], b.PaymentReceived [Payment Received], b.CurrentCharge [Current Charge], b.Deposit [Deposit Payment], b.DepositRefund [Deposit Refund], b.LateFee [Late Fee], b.DisconnectionFee [Disconnection Fee], 
                    b.final_bill [Final bill], b.SpecialInstruction [Notes], b.WorkItemStatus [Work Item Status], b.SentToExpeditedPaymentDate [Batch Date], 
                    b1.LastDateModified [Invoice Date Received], b.LastDateModified [Last Date Modified], b.userid [Last Processed by], b.InvoiceFolderName, a.Invoice
                    from tbl_ADHOC_EM_Tasks a
                    outer apply
                    (select top 1 * from tbl_ADHOC_EM_Tasks  x where x.Invoice = a.Invoice and x.Complete <> 20 order by x.LastDateModified desc) b
                    outer apply
                    (select top 1 * from tbl_ADHOC_EM_Tasks  x where x.Invoice = a.Invoice and x.Complete = 20) b1
                    left join tbl_REO_Properties c
                    on a.property_code = c.property_code
                    left join (select [state], abbrev_state from tbl_EM_ZipcodeStateCity where abbrev_state is not null) d
                    on c.[state] = d.[state]
                    left join tbl_EM_ParameterReference e
                    on b.PendingReasons = e.REFVALUE
                    and e.REFKEY = 'EMS_Status' 
                    where ";


            if (txtPropCode.Text.ToString().Trim() != string.Empty)
            {
                query += string.Format(@"a.property_code like '{0}%'", txtPropCode.Text.ToString().Trim());
                obj = "txtPropCode";
                goto MoreFilter;
            }

            if (txtPropCode.Text.ToString().Trim() == string.Empty && param_address == true && cmbUtility.Text == string.Empty && cmbUtility.Text == "--- All ---" && txtAccountNum.Text == string.Empty && cmbUSP.Text == string.Empty)
            {
                if (txtStrNum.Text != string.Empty || txtStrName.Text != string.Empty)
                {
                    query += string.Format(@"(c.full_address like '{0}%' and c.full_address like '%{1}%') ", txtStrNum.Text, txtStrName.Text);
                }

                if (txtCity.Text != string.Empty)
                {
                    query += string.Format(@"c.city_name = '{0}' ", txtCity.Text);
                }

                if (cmbState.Text != string.Empty && cmbState.Text != "--")
                {
                    query += string.Format(@"d.abbrev_state = '{0}' ", cmbState.Text);
                }

                if (txtZipCode.Text != string.Empty)
                {
                    query += string.Format(@"c.zip_code = '{0}' ", txtZipCode.Text);
                }

                goto MoreFilter;
            }


            if (cmbUtility.Text != string.Empty)
            {
                if (cmbUtility.Text == "--- All ---")
                {
                    query += string.Format(@"a.vendor_group in ('E', 'G', 'W', 'S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Gas")
                {
                    query += string.Format(@"a.vendor_group = 'G' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Water")
                {
                    query += string.Format(@"a.vendor_group = 'W' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Electricity")
                {
                    query += string.Format(@"a.vendor_group = 'E' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Others")
                {
                    query += string.Format(@"a.vendor_group in ('S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                obj = "cmbUtility";
                goto MoreFilter;
            }

            if (txtAccountNum.Text != string.Empty)
            {
                query += string.Format(@"a.AccountNumber = '{0}' ", txtAccountNum.Text);

                obj = "txtAccountNum";
                goto MoreFilter;
            }

            if (txtWorkItem.Text != string.Empty)
            {
                query += string.Format(@"a.work_item = '{0}' ", txtWorkItem.Text);

                obj = "txtWorkItem";
                goto MoreFilter;
            }

            if (cmbUSP.Text != string.Empty)
            {
                query += string.Format(@"a.usp_name like '%{0}%' ", cmbUSP.Text);

                obj = "cmbUSP";
                goto MoreFilter;
            }

            if (dtpFrom.Value.Date != System.DateTime.Now.Date || (dtpTo.Value.Date != System.DateTime.Now.Date && dtpTo.Value.Date > dtpFrom.Value.Date))
            {
                query += string.Format(@"a.LastDateModified between '{0}' and '{1}' ", dtpFrom.Value, dtpTo.Value);

                obj = "dtp";
                goto MoreFilter;
            }

            if (cmbUserid.Text != String.Empty)
            {
                query += string.Format(@"a.userid = '{0}' ", cmbUserid.Text);

                obj = "cmbUserid";
                goto MoreFilter;
            }


        MoreFilter:

            if (txtPropCode.Text.ToString().Trim() != string.Empty && obj != "txtPropCode")
            {
                query += string.Format(@"and a.property_code like '{0}%'", txtPropCode.Text.ToString().Trim());
            }

            if (txtPropCode.Text.ToString().Trim() == string.Empty && param_address == true)
            {
                if (txtStrNum.Text != string.Empty || txtStrName.Text != string.Empty)
                {
                    query += string.Format(@"and (c.full_address like '{0}%' and c.full_address like '%{1}%') ", txtStrNum.Text, txtStrName.Text);
                }

                if (txtCity.Text != string.Empty)
                {
                    query += string.Format(@"and  c.city_name = '{0}' ", txtCity.Text);
                }

                if (cmbState.Text != string.Empty && cmbState.Text != "--")
                {
                    query += string.Format(@"and d.abbrev_state = '{0}' ", cmbState.Text);
                }

                if (txtZipCode.Text != string.Empty)
                {
                    query += string.Format(@"and c.zip_code = '{0}' ", txtZipCode.Text);
                }
            }

            if (cmbUtility.Text != string.Empty && obj != "cmbUtility")
            {
                if (cmbUtility.Text == "--- All ---")
                {
                    query += string.Format(@"and a.vendor_group in ('E', 'G', 'W', 'S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Gas")
                {
                    query += string.Format(@"and a.vendor_group = 'G' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Water")
                {
                    query += string.Format(@"and a.vendor_group = 'W' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Electricity")
                {
                    query += string.Format(@"and a.vendor_group = 'E' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Others")
                {
                    query += string.Format(@"and a.vendor_group in ('S', 'T', 'D', 'None') ", txtZipCode.Text);
                }
            }

            if (txtAccountNum.Text != string.Empty && obj != "txtAccountNum")
            {
                query += string.Format(@"and a.AccountNumber = '{0}' ", txtAccountNum.Text);
            }

            if (txtWorkItem.Text != string.Empty && obj != "txtWorkItem")
            {
                query += string.Format(@"and a.work_item = '{0}' ", txtWorkItem.Text);
            }

            if (cmbUSP.Text != string.Empty && obj != "cmbUSP")
            {
                query += string.Format(@"and a.usp_name like '%{0}%' ", cmbUSP.Text);
            }

            if (dtpFrom.Value.Date != System.DateTime.Now.Date || (dtpTo.Value.Date != System.DateTime.Now.Date && dtpTo.Value.Date > dtpFrom.Value.Date) && obj != "dtp")
            {
                query += string.Format(@"and a.LastDateModified between '{0}' and '{1}' ", dtpFrom.Value, dtpTo.Value);
            }

            if (cmbUserid.Text != String.Empty && obj != "cmbUserid")
            {
                query += string.Format(@"and a.userid = '{0}' ", cmbUserid.Text);
            }

            query += string.Format(@"order by b.LastDateModified desc");

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            dgList.DataSource = dt;

            conn.Close();

            dgList.Columns["InvoiceFolderName"].Visible = false;
            dgList.Columns["Invoice"].Visible = false;
            
            foreach (DataGridViewRow row in dgList.Rows)
            {
                row.Cells["View"].Value = "View Invoice History";
            }

        }

        private void PopulatePayments()
        {
            int countofval = 0;
            string obj = string.Empty;

            foreach (var groupBox in Controls.OfType<GroupBox>())
            {
                foreach (var textBox in groupBox.Controls.OfType<TextBox>())
                {
                    if (textBox.Text != string.Empty)
                    { countofval += 1; };
                }

                foreach (var combobox in groupBox.Controls.OfType<ComboBox>())
                {
                    if (combobox.Text != string.Empty)
                    { countofval += 1; };
                }
            }

            bool param_address = false;
            string query = string.Empty;

            if (txtStrNum.Text.ToString().Trim() == string.Empty && txtStrName.Text.ToString().Trim() == string.Empty && txtCity.Text.ToString().Trim() == string.Empty && cmbState.Text.ToString().Trim() == string.Empty && cmbState.Text.ToString().Trim() == "--" && txtZipCode.Text == string.Empty)
            {
                param_address = false;
            }
            else
            {
                param_address = true;
            }

            query = @"select distinct a.isOvernight [overnight_payment], a.property_code, b.full_address, work_item, line_item, work_item_status, account_num, vendor_code, c.Vendor_Name, vendor_price, issued_date, pay_approved_datetime, ecp_date, issued_by, reviewed_by, review_datetime, ecp_remarks, isOvernight
                    from tbl_EM_PayAppWorkItems a
                    outer apply
                    (select top 1 x.* from tbl_REO_Properties x where x.property_code = a.property_code order by x.client_hierarchy) b
                    left join vw_VID c
                    on a.vendor_code = c.Vendor_ID
                    left join (select [state], abbrev_state from tbl_EM_ZipcodeStateCity where abbrev_state is not null) d
                    on c.[state] = d.[state]
                    where ";

            if (txtPropCode.Text.ToString().Trim() != string.Empty)
            {
                query += string.Format(@"a.property_code like '{0}%'", txtPropCode.Text.ToString().Trim());
                obj = "txtPropCode";
                goto MoreFilter;
            }

            if (txtPropCode.Text.ToString().Trim() == string.Empty && param_address == true && cmbUtility.Text == string.Empty && cmbUtility.Text == "--- All ---" && txtAccountNum.Text == string.Empty && cmbUSP.Text == string.Empty)
            {
                if (txtStrNum.Text != string.Empty || txtStrName.Text != string.Empty)
                {
                    query += string.Format(@"(b.full_address like '{0}%' and b.full_address like '%{1}%') ", txtStrNum.Text, txtStrName.Text);
                }

                if (txtCity.Text != string.Empty)
                {
                    query += string.Format(@"b.city_name = '{0}' ", txtCity.Text);
                }

                if (cmbState.Text != string.Empty && cmbState.Text != "--")
                {
                    query += string.Format(@"d.abbrev_state = '{0}' ", cmbState.Text);
                }

                if (txtZipCode.Text != string.Empty)
                {
                    query += string.Format(@"b.zip_code = '{0}' ", txtZipCode.Text);
                }

                goto MoreFilter;
            }


            if (cmbUtility.Text != string.Empty)
            {
                if (cmbUtility.Text == "--- All ---")
                {
                    query += string.Format(@"a.utility in ('E', 'G', 'W', 'S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Gas")
                {
                    query += string.Format(@"a.utility = 'G' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Water")
                {
                    query += string.Format(@"a.utility = 'W' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Electricity")
                {
                    query += string.Format(@"a.utility = 'E' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Others")
                {
                    query += string.Format(@"a.utility in ('S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                obj = "cmbUtility";
                goto MoreFilter;
            }

            if (txtAccountNum.Text != string.Empty)
            {
                query += string.Format(@"a.account_num = '{0}' ", txtAccountNum.Text);

                obj = "txtAccountNum";
                goto MoreFilter;
            }

            if (txtWorkItem.Text != string.Empty)
            {
                query += string.Format(@"a.work_item = '{0}' ", txtWorkItem.Text);

                obj = "txtWorkItem";
                goto MoreFilter;
            }

            if (cmbUSP.Text != string.Empty)
            {
                query += string.Format(@"c.Vendor_Name like '%{0}%' ", cmbUSP.Text);

                obj = "cmbUSP";
                goto MoreFilter;
            }

            if (dtpFrom.Value.Date != System.DateTime.Now.Date || (dtpTo.Value.Date != System.DateTime.Now.Date && dtpTo.Value.Date > dtpFrom.Value.Date))
            {
                query += string.Format(@"(cast(a.ecp_date as date) between '{0}' and '{1}' or a.issued_date between '{0}' and '{1}') ", dtpFrom.Value, dtpTo.Value);

                obj = "dtp";
                goto MoreFilter;
            }

            if (cmbUserid.Text != String.Empty)
            {
                query += string.Format(@"a.issued_by = '{0}' ", cmbUserid.Text);

                obj = "cmbUserid";
                goto MoreFilter;
            }


        MoreFilter:

            if (txtPropCode.Text.ToString().Trim() != string.Empty && obj != "txtPropCode")
            {
                query += string.Format(@"and a.property_code like '{0}%'", txtPropCode.Text.ToString().Trim());
            }

            if (txtPropCode.Text.ToString().Trim() == string.Empty && param_address == true)
            {
                if (txtStrNum.Text != string.Empty || txtStrName.Text != string.Empty)
                {
                    query += string.Format(@"and (b.full_address like '{0}%' and b.full_address like '%{1}%') ", txtStrNum.Text, txtStrName.Text);
                }

                if (txtCity.Text != string.Empty)
                {
                    query += string.Format(@"and  b.city_name = '{0}' ", txtCity.Text);
                }

                if (cmbState.Text != string.Empty && cmbState.Text != "--")
                {
                    query += string.Format(@"and d.abbrev_state = '{0}' ", cmbState.Text);
                }

                if (txtZipCode.Text != string.Empty)
                {
                    query += string.Format(@"and b.zip_code = '{0}' ", txtZipCode.Text);
                }
            }

            if (cmbUtility.Text != string.Empty && obj != "cmbUtility")
            {
                if (cmbUtility.Text == "--- All ---")
                {
                    query += string.Format(@"and a.utility in ('E', 'G', 'W', 'S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Gas")
                {
                    query += string.Format(@"and a.utility = 'G' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Water")
                {
                    query += string.Format(@"and a.utility = 'W' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Electricity")
                {
                    query += string.Format(@"and a.utility = 'E' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Others")
                {
                    query += string.Format(@"and a.utility in ('S', 'T', 'D', 'None') ", txtZipCode.Text);
                }
            }

            if (txtAccountNum.Text != string.Empty && obj != "txtAccountNum")
            {
                query += string.Format(@"and a.account_num = '{0}' ", txtAccountNum.Text);
            }

            if (txtWorkItem.Text != string.Empty && obj != "txtWorkItem")
            {
                query += string.Format(@"and a.work_item = '{0}' ", txtWorkItem.Text);
            }

            if (cmbUSP.Text != string.Empty && obj != "cmbUSP")
            {
                query += string.Format(@"and c.Vendor_Name like '%{0}%' ", cmbUSP.Text);
            }

            if (dtpFrom.Value.Date != System.DateTime.Now.Date || (dtpTo.Value.Date != System.DateTime.Now.Date && dtpTo.Value.Date > dtpFrom.Value.Date) && obj != "dtp")
            {
                query += string.Format(@"and (cast(a.ecp_date as date) between '{0}' and '{1}' or a.issued_date between '{0}' and '{1}') ", dtpFrom.Value, dtpTo.Value);
            }

            if (cmbUserid.Text != String.Empty && obj != "cmbUserid")
            {
                query += string.Format(@"and a.issued_by = '{0}' ", cmbUserid.Text);
            }

            query += string.Format(@"order by a.issued_date desc");

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            dgPayments.DataSource = dt;

            conn.Close();

            dgPayments.Columns["isOvernight"].Visible = false;
            
            for (int y = 0; y <= dgPayments.Columns.Count - 1; y++)
            {
                if (dgPayments.Columns[y].Name == "overnight_payment")
                {
                    for (int x = 0; x <= dgPayments.Rows.Count - 1; x++)
                    {
                        if (dgPayments.Rows[x].Cells["ecp_date"].Value.ToString() != "" && dgPayments.Rows[x].Cells["isOvernight"].Value.ToString() == "true") // already batched
                        {
                            dgPayments.Rows[x].Cells["overnight_payment"].Value = true;
                            dgPayments.Rows[x].Cells["overnight_payment"].ReadOnly = true;
                        }
                        else if (dgPayments.Rows[x].Cells["ecp_date"].Value.ToString() != "" && dgPayments.Rows[x].Cells["isOvernight"].Value.ToString() == "false") // already batched and regular batch
                        {
                            dgPayments.Rows[x].Cells["overnight_payment"].Value = false;
                            dgPayments.Rows[x].Cells["overnight_payment"].ReadOnly = true;
                        }
                        else if (dgPayments.Rows[x].Cells["ecp_date"].Value.ToString() == "" && dgPayments.Rows[x].Cells["isOvernight"].Value.ToString() == "false") // pending for batching and not yet tagged as overnight
                        {
                            dgPayments.Rows[x].Cells["overnight_payment"].Value = false;
                            dgPayments.Rows[x].Cells["overnight_payment"].ReadOnly = false;
                        }
                        else if (dgPayments.Rows[x].Cells["ecp_date"].Value.ToString() == "" && dgPayments.Rows[x].Cells["isOvernight"].Value.ToString() == "true") // pending for batching and tagged as overnight
                        {
                            dgPayments.Rows[x].Cells["overnight_payment"].Value = true;
                            dgPayments.Rows[x].Cells["overnight_payment"].ReadOnly = false;
                        }
                    }
                }
                else
                {
                    dgPayments.Columns[y].ReadOnly = true;
                }
            }
        }

        private void dgList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex > -1)
            {
                if (dgList.CurrentCell.ColumnIndex > 0)
                {
                    try
                    {
                        Uri myUri = new Uri(dgList.Rows[e.RowIndex].Cells["InvoiceFolderName"].Value.ToString());
                        pdfviewer.Navigate(myUri);
                    }
                    catch
                    {
                        pdfviewer.Navigate("about:blank");
                    }
                }
                else
                {
                    Form invLogs = new frmInvLogs(dgList.Rows[e.RowIndex].Cells["Invoice"].Value.ToString());
                    invLogs.ShowDialog();
                }
            }
        }

        private void triggerSearchButton(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnGo_Click(null, null);
            }
            else if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar) || char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {

            txtWI.Text = "";
            txtPCode.Text = "";
            cmbLineItem.Text = "";
            numAmount.Value = 0;
            cmbStat.Text = "";
            txtAcct.Text = "";
            txtVID.Text = "";
            cmbStat.SelectedIndex = -1;
            cmbLineItem.SelectedIndex = -1;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            if (txtWI.Text.Trim() != "" &&
                txtPCode.Text.Trim() != "" &&
                cmbLineItem.Text.Trim() != "" &&
                numAmount.Value > 0 &&
                cmbStat.Text.Trim() != "" &&
                txtAcct.Text.Trim() != "" &&
                txtVID.Text.Trim() != "")
            {

                SqlConnection conn = new SqlConnection(Constants.connectionString);
                DataTable dta = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter("select * from tbl_EM_PayAppWorkItems where work_item = '" + txtWI.Text.Trim() + "'", conn);
                SqlCommand cmd = new SqlCommand();

                conn.Open();
                da.SelectCommand.CommandTimeout = 1800;
                da.Fill(dta);


                if (dta.Rows.Count > 0)
                {

                    conn.Close();

                    MessageBox.Show("Work item already exist.");

                    txtWI.Text = "";
                    txtPCode.Text = "";
                    cmbLineItem.Text = "";
                    numAmount.Value = 0;
                    cmbStat.Text = "";
                    txtAcct.Text = "";
                    txtVID.Text = "";
                    cmbStat.SelectedIndex = -1;
                    cmbLineItem.SelectedIndex = -1;

                    return;
                }
                else
                {

                    string utility = "";
                    int i = 0;

                    if (cmbLineItem.Text.Contains("Elec"))
                    {
                        utility = "E";
                    }

                    if (cmbLineItem.Text.Contains("Gas"))
                    {
                        utility = "G";
                    }

                    if (cmbLineItem.Text.Contains("Water"))
                    {
                        utility = "W";
                    }

                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = string.Format(@"insert into tbl_EM_PayAppWorkItems (property_code, customer_name, utility, work_item, line_item, work_item_status, issued_date, vendor_price, vendor_code, account_num, created_by, dtcreated, work_item_action)
                                                      values ('{0}', (select top 1 customer_name from tbl_REO_Properties where property_code = '{0}' order by client_hierarchy),  '{1}', '{2}', '{3}', '{4}', dateadd(hour, -12, '{5}'), '{6}', '{7}', '{8}', '{9}', GETDATE(), {10})",
                                                      txtPCode.Text.Trim(),
                                                      utility,
                                                      txtWI.Text.Trim(),
                                                      cmbLineItem.Text.Trim(),
                                                      cmbStat.Text.Trim(),
                                                      DateTime.Now.ToString(),
                                                      numAmount.Value,
                                                      txtVID.Text.Trim(),
                                                      txtAcct.Text.Trim(),
                                                      Environment.UserName,
                                                      (cmbStat.Text == "Payment Approved" || cmbStat.Text == "Closed") ? 1 : 0);

                    i = cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    conn.Close();

                    if (i > 0)
                    {
                        MessageBox.Show("Work item has been added.");

                        txtWI.Text = "";
                        txtPCode.Text = "";
                        cmbLineItem.Text = "";
                        numAmount.Value = 0;
                        cmbStat.Text = "";
                        txtAcct.Text = "";
                        txtVID.Text = "";
                        cmbStat.SelectedIndex = -1;
                        cmbLineItem.SelectedIndex = -1;

                        if (txtPCode.Text == txtPropCode.Text)
                        {
                            PopulatePayments();
                        }
                        else
                        {
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("An error occurred while trying to save the data. Please contact your system administrator");

                        return;
                    }
                }
            }
            else
            {
                MessageBox.Show("All fields are mandatory.");

                return;
            }
        }

        private void num_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void dgPayments_CurrentCellDirtyStateChanged(object sender, EventArgs e)
        {
            if (dgPayments.IsCurrentCellDirty)
            {
                dgPayments.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }
        }

        private void dgPayments_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {

                if (dgPayments.IsCurrentCellDirty)
                {
                    dgPayments.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }

                DataGridViewCheckBoxCell chkchecking = dgPayments.Rows[e.RowIndex].Cells["overnight_payment"] as DataGridViewCheckBoxCell;

                if (e.ColumnIndex == 0)
                {
                    if (dgPayments.Rows[e.RowIndex].Cells["ecp_date"].Value != null && dgPayments.Rows[e.RowIndex].Cells["ecp_date"].Value.ToString() != string.Empty)
                    {
                        MessageBox.Show("Work item was already batched/cancelled.");

                        return;
                    }
                    else
                    {   

                        if (Convert.ToBoolean(chkchecking.Value) == true && 
                            dgPayments.Rows[e.RowIndex].Cells["isOvernight"].Value.ToString().ToLower() == "false" &&
                            dgPayments.Rows[e.RowIndex].Cells["work_item_status"].Value.ToString() != "Cancelled" &&
                            dgPayments.Rows[e.RowIndex].Cells["work_item_status"].Value.ToString() != "Cr-Cancel" &&
                            dgPayments.Rows[e.RowIndex].Cells["work_item_status"].Value.ToString() != "Payment Rejected" &&
                            dgPayments.Rows[e.RowIndex].Cells["work_item_status"].Value.ToString() != "Rejected" &&
                            dgPayments.Rows[e.RowIndex].Cells["work_item_status"].Value.ToString() != "Expired")
                        {
                            DialogResult res = MessageBox.Show("Do you want to process this work item as overnight payment?", "Confirmation", MessageBoxButtons.YesNo);

                            if (res == DialogResult.No)
                            {
                                return;
                            }
                            else
                            {
                                SqlConnection conn = new SqlConnection(Constants.connectionString);
                                SqlCommand cmd = new SqlCommand();
                                int i = 0;

                                conn.Open();

                                cmd.Connection = conn;
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = string.Format(@"update tbl_EM_PayAppWorkItems set isOvernight = 1 where work_item = '{0}'", dgPayments.Rows[e.RowIndex].Cells["work_item"].Value);

                                i = cmd.ExecuteNonQuery();
                                cmd.Dispose();
                                conn.Close();

                                if (i > 0)
                                {
                                    MessageBox.Show("Update successful.");

                                    PopulatePayments();

                                    return;
                                }
                                else
                                {
                                    MessageBox.Show("An error occurred while trying to save the data. Please contact your system administrator");

                                    return;
                                }

                            }
                        }

                        if (Convert.ToBoolean(chkchecking.Value) == false &&
                            dgPayments.Rows[e.RowIndex].Cells["isOvernight"].Value.ToString().ToLower() == "true" &&
                            dgPayments.Rows[e.RowIndex].Cells["work_item_status"].Value.ToString() != "Cancelled" &&
                            dgPayments.Rows[e.RowIndex].Cells["work_item_status"].Value.ToString() != "Cr-Cancel" &&
                            dgPayments.Rows[e.RowIndex].Cells["work_item_status"].Value.ToString() != "Payment Rejected" &&
                            dgPayments.Rows[e.RowIndex].Cells["work_item_status"].Value.ToString() != "Rejected" &&
                            dgPayments.Rows[e.RowIndex].Cells["work_item_status"].Value.ToString() != "Expired")
                        {
                            DialogResult res = MessageBox.Show("Do you want to process this work item as regular payment?", "Confirmation", MessageBoxButtons.YesNo);

                            if (res == DialogResult.No)
                            {
                                return;
                            }
                            else
                            {
                                SqlConnection conn = new SqlConnection(Constants.connectionString);
                                SqlCommand cmd = new SqlCommand();
                                int i = 0;

                                conn.Open();

                                cmd.Connection = conn;
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = string.Format(@"update tbl_EM_PayAppWorkItems set isOvernight = 0 where work_item = '{0}'", dgPayments.Rows[e.RowIndex].Cells["work_item"].Value);

                                i = cmd.ExecuteNonQuery();
                                cmd.Dispose();
                                conn.Close();

                                if (i > 0)
                                {
                                    MessageBox.Show("Update successful.");

                                    PopulatePayments();

                                    return;
                                }
                                else
                                {
                                    MessageBox.Show("An error occurred while trying to save the data. Please contact your system administrator");

                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }

        private void dgPayments_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgPayments.EditMode = DataGridViewEditMode.EditOnEnter;

            for (int y = 0; y <= dgPayments.Columns.Count - 1; y++)
            {
                if (dgPayments.Columns[y].Name == "overnight_payment")
                {
                    for (int x = 0; x <= dgPayments.Rows.Count - 1; x++)
                    {
                        if (dgPayments.Rows[x].Cells["ecp_date"].Value.ToString() != "") // already batched
                        {
                            dgPayments.Rows[x].Cells["overnight_payment"].ReadOnly = true;
                            dgPayments.Rows[x].Cells["btnSave"].ReadOnly = true;
                        }
                        else if (dgPayments.Rows[x].Cells["ecp_date"].Value.ToString() == "" && dgPayments.Rows[x].Cells["isOvernight"].Value.ToString().ToLower() == "false") // pending for batching and not yet tagged as overnight
                        {
                            dgPayments.Rows[x].Cells["overnight_payment"].ReadOnly = false;
                            dgPayments.Rows[x].Cells["btnSave"].ReadOnly = false;
                        }
                        else if (dgPayments.Rows[x].Cells["ecp_date"].Value.ToString() == "" && dgPayments.Rows[x].Cells["isOvernight"].Value.ToString().ToLower() == "true") // pending for batching and tagged as overnight
                        {
                            dgPayments.Rows[x].Cells["overnight_payment"].ReadOnly = false;
                            dgPayments.Rows[x].Cells["btnSave"].ReadOnly = false;
                        }
                    }
                }
                else
                {
                    dgPayments.Columns[y].ReadOnly = true;
                }
            }

            if (dgPayments.IsCurrentCellDirty)
            {
                dgPayments.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }

            

            //for (int x = 0; x <= dgPayments.Rows.Count - 1; x++)
            //{

            //    if (dgPayments.Rows[x].Cells["ecp_date"].Value.ToString() == "")
            //    {

            //        dgPayments.Rows[x].Cells["Type"].ReadOnly = false;

            //        if (dgPayments.Rows[x].Cells["isOvernight"].Value.ToString() == "1")
            //        {
            //            dgPayments.Rows[x].Cells["Type"].Value = true;
            //        }
            //        else
            //        {
            //            dgPayments.Rows[x].Cells["Type"].Value = false;
            //        }
            //    }
            //    else
            //    {
            //        dgPayments.Rows[x].Cells["Type"].ReadOnly = true;
            //    }                
            //}
        }
    }
}
