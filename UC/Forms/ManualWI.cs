﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms
{
    public partial class ManualWI : Form
    {
        Form1 _mainForm;

        public ManualWI(Form1 mainForm)
        {
            InitializeComponent();

            _mainForm = mainForm;
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text != string.Empty)
            {
                button1.Enabled = true;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                comboBox1.Visible = true;
                label4.Visible = true;
                label5.Visible = true;
                textBox1.Enabled = false;
                button1.Enabled = false;
            }
            else
            {
                comboBox1.Visible = false;
                label4.Visible = false;
                label5.Visible = false;
                textBox1.Enabled = true;
                button1.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                _mainForm.info.Complete = 12;
                _mainForm.info.PendingReasons = comboBox1.Text;
                _mainForm.info.work_item = textBox1.Text.Trim();
                this.DialogResult = DialogResult.OK;
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ManualWI_Load(object sender, EventArgs e)
        {
            textBox1.Text = _mainForm.info.work_item;
            comboBox1.Text = _mainForm.info.PendingReasons;
        }


    }
}
