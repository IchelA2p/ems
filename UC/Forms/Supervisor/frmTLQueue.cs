﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms.Supervisor
{
    public partial class frmTLQueue : Form
    {

        string dgSort = "asc";
        int dgMode = 0;

        public frmTLQueue()
        {
            InitializeComponent();
        }

        private void frmTLQueue_Load(object sender, EventArgs e)
        {
            GetData("", "", dgMode);
        }

        private void GetData(string sortby, string order, int mode)
        {
            string query = "";

            if (mode == 0)
            {
                if (sortby != "")
                {
                    query = string.Format(@"SELECT DISTINCT * FROM
                                            (
                                            /*Pending*/

                                            select 'New' [Status], cast(dateadd(hour, -12, b.LastDateModified) as date) [InflowDate], a.property_code [PropertyCode], a.usp_name [USP], a.vendor_group [Utility], a.AccountNumber, a.invoice_date [InvoiceDate], a.ServiceFrom, a.ServiceTo, a.DueDate,
                                            a.Amount, a.LastDateModified, a.userid [ProcessedBy], a.Invoice, a.id, a.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            inner join tbl_ADHOC_EM_Tasks b
                                            on a.Invoice = b.Invoice
                                            and b.Complete = 20
                                            where a.PendingReasons = 'Pre-work item Review' 
                                            and a.isLatestData = 1 
                                            and a.LastDateModified > '12/01/2017' 


                                            UNION ALL
                                            /* Approved (22 days) */

                                            select distinct 'Processed', cast(dateadd(hour, -12, b.LastDateModified) as date) [inflow_date], c.property_code, c.usp_name, c.vendor_group [utility], c.AccountNumber, c.invoice_date, c.ServiceFrom, c.ServiceTo, c.DueDate,
                                            c.Amount, c.LastDateModified, c.userid, c.Invoice, c.id, c.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            inner join tbl_ADHOC_EM_Tasks b
                                            on a.Invoice = b.Invoice
                                            and b.Complete = 20
                                            inner join tbl_ADHOC_EM_Tasks c
                                            on a.Invoice <> c.Invoice
                                            and a.property_code = c.property_code
                                            and (c.PendingReasons like '%manual wi%' or c.PendingReasons like '%bulk upload%') 
                                            and a.isLatestData = 1 
                                            and datediff(day, c.LastDateModified, GETDATE()) <= 22 
                                            where a.PendingReasons = 'Pre-work item Review' 
                                            and a.isLatestData = 1 
                                            and a.LastDateModified > '12/01/2017'
                                            ) as tbl_src
                                            order by PropertyCode asc, {0} {1}
                                            ", sortby, order);
                }
                else
                {
                    query = string.Format(@"SELECT DISTINCT * FROM
                                            (
                                            /*Pending*/

                                            select 'New' [Status], cast(dateadd(hour, -12, b.LastDateModified) as date) [InflowDate], a.property_code [PropertyCode], a.usp_name [USP], a.vendor_group [Utility], a.AccountNumber, a.invoice_date [InvoiceDate], a.ServiceFrom, a.ServiceTo, a.DueDate,
                                            a.Amount, a.LastDateModified, a.userid [ProcessedBy], a.Invoice, a.id, a.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            inner join tbl_ADHOC_EM_Tasks b
                                            on a.Invoice = b.Invoice
                                            and b.Complete = 20
                                            where a.PendingReasons = 'Pre-work item Review' 
                                            and a.isLatestData = 1 
                                            and a.LastDateModified > '12/01/2017' 


                                            UNION ALL
                                            /* Approved (22 days) */

                                            select distinct 'Processed', cast(dateadd(hour, -12, b.LastDateModified) as date) [inflow_date], c.property_code, c.usp_name, c.vendor_group [utility], c.AccountNumber, c.invoice_date, c.ServiceFrom, c.ServiceTo, c.DueDate,
                                            c.Amount, c.LastDateModified, c.userid, c.Invoice, c.id, c.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            inner join tbl_ADHOC_EM_Tasks b
                                            on a.Invoice = b.Invoice
                                            and b.Complete = 20
                                            inner join tbl_ADHOC_EM_Tasks c
                                            on a.Invoice <> c.Invoice
                                            and a.property_code = c.property_code
                                            and (c.PendingReasons like '%manual wi%' or c.PendingReasons like '%bulk upload%') 
                                            and a.isLatestData = 1 
                                            and datediff(day, c.LastDateModified, GETDATE()) <= 22 
                                            where a.PendingReasons = 'Pre-work item Review' 
                                            and a.isLatestData = 1 
                                            and a.LastDateModified > '12/01/2017'
                                            ) as tbl_src
                                            order by PropertyCode asc, [Status] desc, AccountNumber asc");
                }
            }
            else
            {
                if (mode == 1)
                {

                    if (sortby != "")
                    {
                        query = string.Format(@"SELECT DISTINCT tbl_src.*, x.active + ' | ' + x.property_status [PropertyStatus] FROM
                                                                (
                                                                /*Pending*/

                                                                select 'New' [Status], cast(dateadd(hour, -12, b.LastDateModified) as date) [InflowDate], a.property_code [PropertyCode], a.usp_name [USP], a.vendor_group [Utility], a.AccountNumber, a.invoice_date [InvoiceDate], a.ServiceFrom, a.ServiceTo, a.DueDate,
                                                                a.Amount, a.LastDateModified, a.userid [ProcessedBy], a.Invoice, a.id, a.InvoiceFolderName
                                                                from tbl_ADHOC_EM_Tasks a
                                                                inner join tbl_ADHOC_EM_Tasks b
                                                                on a.Invoice = b.Invoice
                                                                and b.Complete = 20
                                                                where a.PendingReasons = 'Pre-work item Review' 
                                                                and a.isLatestData = 1 
                                                                and a.LastDateModified > '12/01/2017' 


                                                                UNION ALL
                                                                /* Approved (22 days) */

                                                                select distinct 'Processed', cast(dateadd(hour, -12, b.LastDateModified) as date) [inflow_date], c.property_code, c.usp_name, c.vendor_group [utility], c.AccountNumber, c.invoice_date, c.ServiceFrom, c.ServiceTo, c.DueDate,
                                                                c.Amount, c.LastDateModified, c.userid, c.Invoice, c.id, c.InvoiceFolderName
                                                                from tbl_ADHOC_EM_Tasks a
                                                                inner join tbl_ADHOC_EM_Tasks b
                                                                on a.Invoice = b.Invoice
                                                                and b.Complete = 20
                                                                inner join tbl_ADHOC_EM_Tasks c
                                                                on a.Invoice <> c.Invoice
                                                                and a.property_code = c.property_code
                                                                and (c.PendingReasons like '%manual wi%' or c.PendingReasons like '%bulk upload%') 
                                                                and a.isLatestData = 1 
                                                                and datediff(day, c.LastDateModified, GETDATE()) <= 22 
                                                                where a.PendingReasons = 'Pre-work item Review' 
                                                                and a.isLatestData = 1 
                                                                and a.LastDateModified > '12/01/2017'
                                                                ) as tbl_src
                                                                inner join tbl_REO_Properties x
                                                                on tbl_src.PropertyCode = x.property_code
                                                                inner join (select distinct property_code from 
				                                                                (
				                                                                select a.property_code
				                                                                from tbl_ADHOC_EM_Tasks a
				                                                                inner join tbl_ADHOC_EM_Tasks b
				                                                                on a.Invoice = b.Invoice
				                                                                and b.Complete = 20
				                                                                where a.PendingReasons = 'Pre-work item Review' 
				                                                                and a.isLatestData = 1 
				                                                                and a.LastDateModified > '12/01/2017' 


				                                                                UNION ALL
				                                                                /* Approved (22 days) */

				                                                                select c.property_code
				                                                                from tbl_ADHOC_EM_Tasks a
				                                                                inner join tbl_ADHOC_EM_Tasks b
				                                                                on a.Invoice = b.Invoice
				                                                                and b.Complete = 20
				                                                                inner join tbl_ADHOC_EM_Tasks c
				                                                                on a.Invoice <> c.Invoice
				                                                                and a.property_code = c.property_code
				                                                                and (c.PendingReasons like '%manual wi%' or c.PendingReasons like '%bulk upload%') 
				                                                                and a.isLatestData = 1 
				                                                                and datediff(day, c.LastDateModified, GETDATE()) <= 22 
				                                                                where a.PendingReasons = 'Pre-work item Review' 
				                                                                and a.isLatestData = 1 
				                                                                and a.LastDateModified > '12/01/2017'
				                                                                ) as x
				                                                                group by property_code
				                                                                having COUNT(property_code) = 1) z
                                                                on tbl_src.PropertyCode = z.property_code
                                                                order by PropertyCode asc, {0} {1}
                                                                 ", sortby, order);
                    }
                    else
                    {
                        query = string.Format(@"SELECT DISTINCT tbl_src.*, x.active + ' | ' + x.property_status [PropertyStatus] FROM
                                                                (
                                                                /*Pending*/

                                                                select 'New' [Status], cast(dateadd(hour, -12, b.LastDateModified) as date) [InflowDate], a.property_code [PropertyCode], a.usp_name [USP], a.vendor_group [Utility], a.AccountNumber, a.invoice_date [InvoiceDate], a.ServiceFrom, a.ServiceTo, a.DueDate,
                                                                a.Amount, a.LastDateModified, a.userid [ProcessedBy], a.Invoice, a.id, a.InvoiceFolderName
                                                                from tbl_ADHOC_EM_Tasks a
                                                                inner join tbl_ADHOC_EM_Tasks b
                                                                on a.Invoice = b.Invoice
                                                                and b.Complete = 20
                                                                where a.PendingReasons = 'Pre-work item Review' 
                                                                and a.isLatestData = 1 
                                                                and a.LastDateModified > '12/01/2017' 


                                                                UNION ALL
                                                                /* Approved (22 days) */

                                                                select distinct 'Processed', cast(dateadd(hour, -12, b.LastDateModified) as date) [inflow_date], c.property_code, c.usp_name, c.vendor_group [utility], c.AccountNumber, c.invoice_date, c.ServiceFrom, c.ServiceTo, c.DueDate,
                                                                c.Amount, c.LastDateModified, c.userid, c.Invoice, c.id, c.InvoiceFolderName
                                                                from tbl_ADHOC_EM_Tasks a
                                                                inner join tbl_ADHOC_EM_Tasks b
                                                                on a.Invoice = b.Invoice
                                                                and b.Complete = 20
                                                                inner join tbl_ADHOC_EM_Tasks c
                                                                on a.Invoice <> c.Invoice
                                                                and a.property_code = c.property_code
                                                                and (c.PendingReasons like '%manual wi%' or c.PendingReasons like '%bulk upload%') 
                                                                and a.isLatestData = 1 
                                                                and datediff(day, c.LastDateModified, GETDATE()) <= 22 
                                                                where a.PendingReasons = 'Pre-work item Review' 
                                                                and a.isLatestData = 1 
                                                                and a.LastDateModified > '12/01/2017'
                                                                ) as tbl_src
                                                                inner join tbl_REO_Properties x
                                                                on tbl_src.PropertyCode = x.property_code
                                                                inner join (select distinct property_code from 
				                                                                (
				                                                                select a.property_code
				                                                                from tbl_ADHOC_EM_Tasks a
				                                                                inner join tbl_ADHOC_EM_Tasks b
				                                                                on a.Invoice = b.Invoice
				                                                                and b.Complete = 20
				                                                                where a.PendingReasons = 'Pre-work item Review' 
				                                                                and a.isLatestData = 1 
				                                                                and a.LastDateModified > '12/01/2017' 


				                                                                UNION ALL
				                                                                /* Approved (22 days) */

				                                                                select c.property_code
				                                                                from tbl_ADHOC_EM_Tasks a
				                                                                inner join tbl_ADHOC_EM_Tasks b
				                                                                on a.Invoice = b.Invoice
				                                                                and b.Complete = 20
				                                                                inner join tbl_ADHOC_EM_Tasks c
				                                                                on a.Invoice <> c.Invoice
				                                                                and a.property_code = c.property_code
				                                                                and (c.PendingReasons like '%manual wi%' or c.PendingReasons like '%bulk upload%') 
				                                                                and a.isLatestData = 1 
				                                                                and datediff(day, c.LastDateModified, GETDATE()) <= 22 
				                                                                where a.PendingReasons = 'Pre-work item Review' 
				                                                                and a.isLatestData = 1 
				                                                                and a.LastDateModified > '12/01/2017'
				                                                                ) as x
				                                                                group by property_code
				                                                                having COUNT(property_code) = 1) z
                                                                on tbl_src.PropertyCode = z.property_code
                                                                order by PropertyCode asc, [Status] desc, AccountNumber asc");
                    }
                   
                }
                else
                {

                    if (sortby != "")
                    {
                        query = string.Format(@"SELECT DISTINCT tbl_src.*, x.active + ' | ' + x.property_status [PropertyStatus] FROM
                                            (
                                            /*Pending*/

                                            select 'New' [Status], cast(dateadd(hour, -12, b.LastDateModified) as date) [InflowDate], a.property_code [PropertyCode], a.usp_name [USP], a.vendor_group [Utility], a.AccountNumber, a.invoice_date [InvoiceDate], a.ServiceFrom, a.ServiceTo, a.DueDate,
                                            a.Amount, a.LastDateModified, a.userid [ProcessedBy], a.Invoice, a.id, a.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            inner join tbl_ADHOC_EM_Tasks b
                                            on a.Invoice = b.Invoice
                                            and b.Complete = 20
                                            where a.PendingReasons = 'Pre-work item Review' 
                                            and a.isLatestData = 1 
                                            and a.LastDateModified > '12/01/2017' 


                                            UNION ALL
                                            /* Approved (22 days) */

                                            select distinct 'Processed', cast(dateadd(hour, -12, b.LastDateModified) as date) [inflow_date], c.property_code, c.usp_name, c.vendor_group [utility], c.AccountNumber, c.invoice_date, c.ServiceFrom, c.ServiceTo, c.DueDate,
                                            c.Amount, c.LastDateModified, c.userid, c.Invoice, c.id, c.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            inner join tbl_ADHOC_EM_Tasks b
                                            on a.Invoice = b.Invoice
                                            and b.Complete = 20
                                            inner join tbl_ADHOC_EM_Tasks c
                                            on a.Invoice <> c.Invoice
                                            and a.property_code = c.property_code
                                            and (c.PendingReasons like '%manual wi%' or c.PendingReasons like '%bulk upload%') 
                                            and a.isLatestData = 1 
                                            and datediff(day, c.LastDateModified, GETDATE()) <= 22 
                                            where a.PendingReasons = 'Pre-work item Review' 
                                            and a.isLatestData = 1 
                                            and a.LastDateModified > '12/01/2017'
                                            ) as tbl_src
                                            inner join tbl_REO_Properties x
                                            on tbl_src.PropertyCode = x.property_code
                                            inner join (select distinct property_code from 
				                                            (
				                                            select a.property_code
				                                            from tbl_ADHOC_EM_Tasks a
				                                            inner join tbl_ADHOC_EM_Tasks b
				                                            on a.Invoice = b.Invoice
				                                            and b.Complete = 20
				                                            where a.PendingReasons = 'Pre-work item Review' 
				                                            and a.isLatestData = 1 
				                                            and a.LastDateModified > '12/01/2017' 


				                                            UNION ALL
				                                            /* Approved (22 days) */

				                                            select c.property_code
				                                            from tbl_ADHOC_EM_Tasks a
				                                            inner join tbl_ADHOC_EM_Tasks b
				                                            on a.Invoice = b.Invoice
				                                            and b.Complete = 20
				                                            inner join tbl_ADHOC_EM_Tasks c
				                                            on a.Invoice <> c.Invoice
				                                            and a.property_code = c.property_code
				                                            and (c.PendingReasons like '%manual wi%' or c.PendingReasons like '%bulk upload%') 
				                                            and a.isLatestData = 1 
				                                            and datediff(day, c.LastDateModified, GETDATE()) <= 22 
				                                            where a.PendingReasons = 'Pre-work item Review' 
				                                            and a.isLatestData = 1 
				                                            and a.LastDateModified > '12/01/2017'
				                                            ) as x
				                                            group by property_code
				                                            having COUNT(property_code) > 1) z
                                            on tbl_src.PropertyCode = z.property_code
                                            order by PropertyCode asc, {0} {1}", sortby, order);
                    }
                    else
                    {
                        query = string.Format(@"SELECT DISTINCT tbl_src.*, x.active + ' | ' + x.property_status [PropertyStatus] FROM
                                            (
                                            /*Pending*/

                                            select 'New' [Status], cast(dateadd(hour, -12, b.LastDateModified) as date) [InflowDate], a.property_code [PropertyCode], a.usp_name [USP], a.vendor_group [Utility], a.AccountNumber, a.invoice_date [InvoiceDate], a.ServiceFrom, a.ServiceTo, a.DueDate,
                                            a.Amount, a.LastDateModified, a.userid [ProcessedBy], a.Invoice, a.id, a.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            inner join tbl_ADHOC_EM_Tasks b
                                            on a.Invoice = b.Invoice
                                            and b.Complete = 20
                                            where a.PendingReasons = 'Pre-work item Review' 
                                            and a.isLatestData = 1 
                                            and a.LastDateModified > '12/01/2017' 


                                            UNION ALL
                                            /* Approved (22 days) */

                                            select distinct 'Processed', cast(dateadd(hour, -12, b.LastDateModified) as date) [inflow_date], c.property_code, c.usp_name, c.vendor_group [utility], c.AccountNumber, c.invoice_date, c.ServiceFrom, c.ServiceTo, c.DueDate,
                                            c.Amount, c.LastDateModified, c.userid, c.Invoice, c.id, c.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            inner join tbl_ADHOC_EM_Tasks b
                                            on a.Invoice = b.Invoice
                                            and b.Complete = 20
                                            inner join tbl_ADHOC_EM_Tasks c
                                            on a.Invoice <> c.Invoice
                                            and a.property_code = c.property_code
                                            and (c.PendingReasons like '%manual wi%' or c.PendingReasons like '%bulk upload%') 
                                            and a.isLatestData = 1 
                                            and datediff(day, c.LastDateModified, GETDATE()) <= 22 
                                            where a.PendingReasons = 'Pre-work item Review' 
                                            and a.isLatestData = 1 
                                            and a.LastDateModified > '12/01/2017'
                                            ) as tbl_src
                                            inner join tbl_REO_Properties x
                                            on tbl_src.PropertyCode = x.property_code
                                            inner join (select distinct property_code from 
				                                            (
				                                            select a.property_code
				                                            from tbl_ADHOC_EM_Tasks a
				                                            inner join tbl_ADHOC_EM_Tasks b
				                                            on a.Invoice = b.Invoice
				                                            and b.Complete = 20
				                                            where a.PendingReasons = 'Pre-work item Review' 
				                                            and a.isLatestData = 1 
				                                            and a.LastDateModified > '12/01/2017' 


				                                            UNION ALL
				                                            /* Approved (22 days) */

				                                            select c.property_code
				                                            from tbl_ADHOC_EM_Tasks a
				                                            inner join tbl_ADHOC_EM_Tasks b
				                                            on a.Invoice = b.Invoice
				                                            and b.Complete = 20
				                                            inner join tbl_ADHOC_EM_Tasks c
				                                            on a.Invoice <> c.Invoice
				                                            and a.property_code = c.property_code
				                                            and (c.PendingReasons like '%manual wi%' or c.PendingReasons like '%bulk upload%') 
				                                            and a.isLatestData = 1 
				                                            and datediff(day, c.LastDateModified, GETDATE()) <= 22 
				                                            where a.PendingReasons = 'Pre-work item Review' 
				                                            and a.isLatestData = 1 
				                                            and a.LastDateModified > '12/01/2017'
				                                            ) as x
				                                            group by property_code
				                                            having COUNT(property_code) > 1) z
                                            on tbl_src.PropertyCode = z.property_code
                                            order by PropertyCode asc, [Status] desc, AccountNumber asc");
                    }                    
                }
            }            
            

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            DataTable dtf = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();

            da.Fill(dt);

            conn.Close();

            dgInvoice.DataSource = dt;

            lblCount.Text = dgInvoice.RowCount.ToString();

            HighlightDuplicate(dgInvoice);
        }

        public void HighlightDuplicate(DataGridView grv)
        {
            //use the currentRow to compare against
            for (int currentRow = 0; currentRow < grv.Rows.Count - 1; currentRow++)
            {
                DataGridViewRow rowToCompare = grv.Rows[currentRow];

                if (dgInvoice.Rows[currentRow].Cells["Status"].Value.ToString() == "Processed")
                {
                    dgInvoice.Rows[currentRow].Cells[0].Style.BackColor = Color.LightGray;
                }
                else
                {
                    dgInvoice.Rows[currentRow].Cells[0].Style.BackColor = Color.White;
                }
                
                //specify otherRow as currentRow + 1
                for (int otherRow = currentRow + 1; otherRow < grv.Rows.Count; otherRow++)
                {
                    DataGridViewRow row = grv.Rows[otherRow];

                    bool duplicateRow = true;
                    bool duplicateUtility = true;
                    bool duplicateAcct = true;
                    bool duplicateAmt = true;
                    bool stat = true;
                   
                    //property code
                    if (!rowToCompare.Cells["PropertyCode"].Value.Equals(row.Cells["PropertyCode"].Value))
                    {
                        duplicateRow = false;
                        break;
                    }
                    
                    if (duplicateRow)
                    {

                        dgInvoice.Rows[rowToCompare.Index].Cells["PropertyCode"].Style.BackColor = Color.LightCoral;
                        dgInvoice.Rows[row.Index].Cells["PropertyCode"].Style.BackColor = Color.LightCoral;
                    }


                    //Utility
                    if (!rowToCompare.Cells["Utility"].Value.Equals(row.Cells["Utility"].Value))
                    {
                        duplicateUtility = false;
                        break;
                    }

                    if (duplicateUtility)
                    {

                        dgInvoice.Rows[rowToCompare.Index].Cells["Utility"].Style.BackColor = Color.GreenYellow;
                        dgInvoice.Rows[row.Index].Cells["Utility"].Style.BackColor = Color.GreenYellow;
                    }


                    //Account number
                    if (!rowToCompare.Cells["AccountNumber"].Value.Equals(row.Cells["AccountNumber"].Value))
                    {
                        duplicateAcct = false;
                        break;
                    }

                    if (duplicateAcct)
                    {

                        dgInvoice.Rows[rowToCompare.Index].Cells["AccountNumber"].Style.BackColor = Color.LightSkyBlue;
                        dgInvoice.Rows[row.Index].Cells["AccountNumber"].Style.BackColor = Color.LightSkyBlue;
                    }


                    //Amount
                    if (!rowToCompare.Cells["Amount"].Value.Equals(row.Cells["Amount"].Value))
                    {
                        duplicateAmt = false;
                        break;
                    }

                    if (duplicateAmt)
                    {

                        dgInvoice.Rows[rowToCompare.Index].Cells["Amount"].Style.BackColor = Color.Plum;
                        dgInvoice.Rows[row.Index].Cells["Amount"].Style.BackColor = Color.Plum;
                    }

                                    
                }
            }
        }

        private void dgInvoice_Sorted(object sender, EventArgs e)
        {
            DataGridViewColumn dgCol = dgInvoice.SortedColumn;

            if (dgCol.Index == 5)
            {
                GetData("Utility", dgSort, dgMode);
            }
            else if (dgCol.Index == 6)
            {
                GetData("AccountNumber", dgSort, dgMode);
            }
            else if (dgCol.Index == 11)
            {
                GetData("Amount", dgSort, dgMode);
            }
            else
            {
                GetData("", "", dgMode);
            }

            if (dgSort == "asc")
            {
                dgSort = "desc";
            }
            else
            {
                dgSort = "asc";
            }
            
            HighlightDuplicate(dgInvoice);
        }


        private void cmbCriteria_DrawItem(object sender, DrawItemEventArgs e)
        {
            
            //Color HighlightColor = Color.White;

            //if (cmbField.Text == "PropertyCode")
            //{
            //    HighlightColor = Color.LightCoral;
            //}
            //else if (cmbField.Text == "Utility")
            //{
            //    HighlightColor = Color.GreenYellow;
            //}
            //else if (cmbField.Text == "AccountNumber")
            //{
            //    HighlightColor = Color.LightSkyBlue;
            //}
            //else if (cmbField.Text == "Amount")
            //{
            //    HighlightColor = Color.Plum;
            //}
            //else
            //{
            //    return;
            //}

            //if (e.Index >= 0)
            //{
            //    string deger = cmbCriteria.Items[e.Index].ToString();
                 
            //    if (deger != "No fill")
            //    {
            //        e.Graphics.FillRectangle(new SolidBrush(HighlightColor), e.Bounds);
            //    }

            //    e.Graphics.DrawString(cmbCriteria.Items[e.Index].ToString(), e.Font, new SolidBrush(cmbCriteria.ForeColor), new Point(e.Bounds.X, e.Bounds.Y));

            //    e.DrawFocusRectangle();
            //}
        }

        private void cmbFilter_SelectedIndexChanged(object sender, EventArgs e)
        {
            //if (cmbFilter.SelectedIndex == 0)
            //{
            //    cmbCriteria.DropDownStyle = ComboBoxStyle.DropDown;
            //    cmbCriteria.Items.Clear();
            //    cmbCriteria.Items.Add("No fill");
            //    cmbCriteria.Items.Add("");
            //}
            //else
            //{
            //    cmbCriteria.DropDownStyle = ComboBoxStyle.Simple;
            //}

            //dgMode = 0;
        }

        private void cmbCriteria_SelectedIndexChanged(object sender, EventArgs e)
        {

            if (cmbCriteria.SelectedIndex == 0)
            {
                dgMode = 1;

                GetData("", "", dgMode);
            }
            else
            {
                dgMode = 2;

                GetData("", "", dgMode);
            }
        }

        private void btnSelect_Click(object sender, EventArgs e)
        {
            for (int x = 0; x <= dgInvoice.Rows.Count - 1; x++)
            {

            }           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbData.SelectedIndex == 1)
            {
                dgMode = 1;

                GetData("", "", dgMode);
            }
            else if (cmbData.SelectedIndex == 2)
            {
                dgMode = 2;

                GetData("", "", dgMode);
            }
            else
            {
                dgMode = 0;

                GetData("", "", dgMode);
            }
        }

        private void checkBox1_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkSelect.Checked == true)
            {
                for (int x = 0; x <= dgInvoice.Rows.Count - 1; x++)
                {
                    if (dgInvoice.Rows[x].Cells["Status"].Value.ToString() == "New")
                    {
                        dgInvoice.Rows[x].Cells[0].Value = true;
                    }
                    else
                    {
                        dgInvoice.Rows[x].Cells[0].Value = false;
                    }
                    
                }   
            }
            else
            {
                for (int x = 0; x <= dgInvoice.Rows.Count - 1; x++)
                {
                    dgInvoice.Rows[x].Cells[0].Value = false;   
                }   
            }
        }

        private void dgInvoice_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgInvoice.EditMode = DataGridViewEditMode.EditOnEnter;

            for (int x = 0; x <= dgInvoice.Columns.Count - 1; x++)
            {
                if (x == 0)
                {
                    for (int y = 0; y <= dgInvoice.Rows.Count - 1; y++)
                    {
                        if (dgInvoice.Rows[y].Cells["Status"].Value.ToString() == "New")
                        {
                            dgInvoice.Rows[y].Cells[0].ReadOnly = false;
                        }
                        else
                        {
                            dgInvoice.Rows[y].Cells[0].ReadOnly = true;
                        }
                    }
                }
                else
                {
                    dgInvoice.Columns[x].ReadOnly = true;   
                }
            }           
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }
    }
}
