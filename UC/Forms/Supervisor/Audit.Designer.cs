﻿namespace UC.Forms.Supervisor
{
    partial class Audit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.label14 = new System.Windows.Forms.Label();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.pictureBox26 = new System.Windows.Forms.PictureBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.chkbx_noZipCode = new System.Windows.Forms.CheckBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.chkBx_NoPropertyAddress = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.button9 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.cmboBx_UspOnInvoice = new System.Windows.Forms.ComboBox();
            this.lbl_UspOnInvoice = new System.Windows.Forms.Label();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.cPropertyCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cFullAddress = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cCustomerName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cActive = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cInvestorName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cReosrcDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cReoslcDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cReosfcDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cAccountNumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cPodId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cproperty_status_change_date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cinactivedate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button_noMatchUSP = new System.Windows.Forms.Button();
            this.dgv_usp = new System.Windows.Forms.DataGridView();
            this.vdr_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vdr_name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vdr_address = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vdr_status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.vms_status = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.pictureBox22 = new System.Windows.Forms.PictureBox();
            this.pictureBox23 = new System.Windows.Forms.PictureBox();
            this.pictureBox24 = new System.Windows.Forms.PictureBox();
            this.pct_isbillreadableapprove = new System.Windows.Forms.PictureBox();
            this.pct_isbillreadablecross = new System.Windows.Forms.PictureBox();
            this.pct_isbillreadablecheck = new System.Windows.Forms.PictureBox();
            this.label8 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.checkBox20 = new System.Windows.Forms.CheckBox();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.pictureBox64 = new System.Windows.Forms.PictureBox();
            this.chkTransAmtNo = new System.Windows.Forms.CheckBox();
            this.pictureBox66 = new System.Windows.Forms.PictureBox();
            this.pictureBox44 = new System.Windows.Forms.PictureBox();
            this.chkTransAmtYes = new System.Windows.Forms.CheckBox();
            this.pictureBox56 = new System.Windows.Forms.PictureBox();
            this.label58 = new System.Windows.Forms.Label();
            this.pictureBox65 = new System.Windows.Forms.PictureBox();
            this.pictureBox45 = new System.Windows.Forms.PictureBox();
            this.pictureBox57 = new System.Windows.Forms.PictureBox();
            this.pictureBox46 = new System.Windows.Forms.PictureBox();
            this.pictureBox58 = new System.Windows.Forms.PictureBox();
            this.pictureBox53 = new System.Windows.Forms.PictureBox();
            this.pictureBox54 = new System.Windows.Forms.PictureBox();
            this.pictureBox55 = new System.Windows.Forms.PictureBox();
            this.pictureBox50 = new System.Windows.Forms.PictureBox();
            this.pictureBox51 = new System.Windows.Forms.PictureBox();
            this.pictureBox52 = new System.Windows.Forms.PictureBox();
            this.pictureBox47 = new System.Windows.Forms.PictureBox();
            this.pictureBox48 = new System.Windows.Forms.PictureBox();
            this.pictureBox49 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.checkBox27 = new System.Windows.Forms.CheckBox();
            this.pictureBox9 = new System.Windows.Forms.PictureBox();
            this.label29 = new System.Windows.Forms.Label();
            this.dateTimePicker6 = new System.Windows.Forms.DateTimePicker();
            this.checkBox28 = new System.Windows.Forms.CheckBox();
            this.checkBox29 = new System.Windows.Forms.CheckBox();
            this.label32 = new System.Windows.Forms.Label();
            this.checkBox30 = new System.Windows.Forms.CheckBox();
            this.checkBox31 = new System.Windows.Forms.CheckBox();
            this.checkBox32 = new System.Windows.Forms.CheckBox();
            this.dateTimePicker7 = new System.Windows.Forms.DateTimePicker();
            this.label35 = new System.Windows.Forms.Label();
            this.dateTimePicker8 = new System.Windows.Forms.DateTimePicker();
            this.dateTimePicker9 = new System.Windows.Forms.DateTimePicker();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.dateTimePicker10 = new System.Windows.Forms.DateTimePicker();
            this.label40 = new System.Windows.Forms.Label();
            this.checkBox33 = new System.Windows.Forms.CheckBox();
            this.label42 = new System.Windows.Forms.Label();
            this.checkBox34 = new System.Windows.Forms.CheckBox();
            this.label41 = new System.Windows.Forms.Label();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.pictureBox76 = new System.Windows.Forms.PictureBox();
            this.pictureBox73 = new System.Windows.Forms.PictureBox();
            this.pictureBox34 = new System.Windows.Forms.PictureBox();
            this.pictureBox75 = new System.Windows.Forms.PictureBox();
            this.pictureBox72 = new System.Windows.Forms.PictureBox();
            this.pictureBox38 = new System.Windows.Forms.PictureBox();
            this.pictureBox74 = new System.Windows.Forms.PictureBox();
            this.pictureBox71 = new System.Windows.Forms.PictureBox();
            this.pictureBox40 = new System.Windows.Forms.PictureBox();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtDepRef = new System.Windows.Forms.TextBox();
            this.txtDepPy = new System.Windows.Forms.TextBox();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.pictureBox41 = new System.Windows.Forms.PictureBox();
            this.pictureBox42 = new System.Windows.Forms.PictureBox();
            this.pictureBox43 = new System.Windows.Forms.PictureBox();
            this.pictureBox31 = new System.Windows.Forms.PictureBox();
            this.pictureBox32 = new System.Windows.Forms.PictureBox();
            this.pictureBox33 = new System.Windows.Forms.PictureBox();
            this.pictureBox19 = new System.Windows.Forms.PictureBox();
            this.pictureBox20 = new System.Windows.Forms.PictureBox();
            this.pictureBox21 = new System.Windows.Forms.PictureBox();
            this.pictureBox16 = new System.Windows.Forms.PictureBox();
            this.pictureBox17 = new System.Windows.Forms.PictureBox();
            this.pictureBox18 = new System.Windows.Forms.PictureBox();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.pictureBox8 = new System.Windows.Forms.PictureBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.pictureBox10 = new System.Windows.Forms.PictureBox();
            this.pictureBox11 = new System.Windows.Forms.PictureBox();
            this.pictureBox12 = new System.Windows.Forms.PictureBox();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.label45 = new System.Windows.Forms.Label();
            this.label47 = new System.Windows.Forms.Label();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.checkBox35 = new System.Windows.Forms.CheckBox();
            this.label48 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.pictureBox27 = new System.Windows.Forms.PictureBox();
            this.pictureBox28 = new System.Windows.Forms.PictureBox();
            this.pictureBox29 = new System.Windows.Forms.PictureBox();
            this.pictureBox35 = new System.Windows.Forms.PictureBox();
            this.pictureBox36 = new System.Windows.Forms.PictureBox();
            this.pictureBox37 = new System.Windows.Forms.PictureBox();
            this.pictureBox39 = new System.Windows.Forms.PictureBox();
            this.pictureBox13 = new System.Windows.Forms.PictureBox();
            this.pictureBox14 = new System.Windows.Forms.PictureBox();
            this.pictureBox15 = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox59 = new System.Windows.Forms.PictureBox();
            this.pictureBox60 = new System.Windows.Forms.PictureBox();
            this.pictureBox61 = new System.Windows.Forms.PictureBox();
            this.pictureBox63 = new System.Windows.Forms.PictureBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.grpBox_SpecialInstructions = new System.Windows.Forms.GroupBox();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.txtTransferAccount = new System.Windows.Forms.Label();
            this.btnAdd = new System.Windows.Forms.Button();
            this.cmbBillName = new System.Windows.Forms.ComboBox();
            this.label61 = new System.Windows.Forms.Label();
            this.pictureBox25 = new System.Windows.Forms.PictureBox();
            this.pictureBox30 = new System.Windows.Forms.PictureBox();
            this.pictureBox62 = new System.Windows.Forms.PictureBox();
            this.label20 = new System.Windows.Forms.Label();
            this.chkNYes = new System.Windows.Forms.CheckBox();
            this.chknNo = new System.Windows.Forms.CheckBox();
            this.pictureBox67 = new System.Windows.Forms.PictureBox();
            this.pictureBox68 = new System.Windows.Forms.PictureBox();
            this.pictureBox69 = new System.Windows.Forms.PictureBox();
            this.pictureBox70 = new System.Windows.Forms.PictureBox();
            this.label21 = new System.Windows.Forms.Label();
            this.txtUSPCity = new System.Windows.Forms.TextBox();
            this.txtUSPState = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtUSPZip = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txtUSPStrt = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.btnSearchUSP = new System.Windows.Forms.Button();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_usp)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_isbillreadableapprove)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_isbillreadablecross)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_isbillreadablecheck)).BeginInit();
            this.groupBox9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox66)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox65)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).BeginInit();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox76)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox73)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox75)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox72)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox74)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox71)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).BeginInit();
            this.grpBox_SpecialInstructions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox67)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox68)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox69)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox70)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(4, 3);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(17, 18);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(763, 834);
            this.webBrowser1.TabIndex = 4;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.ForeColor = System.Drawing.Color.Black;
            this.label14.Location = new System.Drawing.Point(784, 123);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(98, 14);
            this.label14.TabIndex = 133;
            this.label14.Text = "Account Number:";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.pictureBox3);
            this.groupBox8.Controls.Add(this.pictureBox4);
            this.groupBox8.Controls.Add(this.textBox16);
            this.groupBox8.Controls.Add(this.pictureBox5);
            this.groupBox8.Controls.Add(this.textBox17);
            this.groupBox8.Controls.Add(this.label26);
            this.groupBox8.Controls.Add(this.pictureBox26);
            this.groupBox8.Controls.Add(this.textBox4);
            this.groupBox8.Controls.Add(this.label5);
            this.groupBox8.Controls.Add(this.textBox14);
            this.groupBox8.Controls.Add(this.chkbx_noZipCode);
            this.groupBox8.Controls.Add(this.label7);
            this.groupBox8.Controls.Add(this.label6);
            this.groupBox8.Controls.Add(this.textBox1);
            this.groupBox8.Controls.Add(this.label1);
            this.groupBox8.Location = new System.Drawing.Point(779, 147);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(300, 110);
            this.groupBox8.TabIndex = 134;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Property Address";
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox3.Location = new System.Drawing.Point(274, 79);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(19, 20);
            this.pictureBox3.TabIndex = 184;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Visible = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox4.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox4.Location = new System.Drawing.Point(249, 79);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(19, 20);
            this.pictureBox4.TabIndex = 183;
            this.pictureBox4.TabStop = false;
            this.pictureBox4.Click += new System.EventHandler(this.pictureBox4_Click);
            // 
            // textBox16
            // 
            this.textBox16.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.SuggestAppend;
            this.textBox16.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.textBox16.Enabled = false;
            this.textBox16.Location = new System.Drawing.Point(63, 50);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(115, 22);
            this.textBox16.TabIndex = 123;
            this.textBox16.TextChanged += new System.EventHandler(this.textBox16_TextChanged);
            // 
            // pictureBox5
            // 
            this.pictureBox5.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox5.Location = new System.Drawing.Point(249, 79);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(19, 20);
            this.pictureBox5.TabIndex = 182;
            this.pictureBox5.TabStop = false;
            // 
            // textBox17
            // 
            this.textBox17.Enabled = false;
            this.textBox17.Location = new System.Drawing.Point(220, 50);
            this.textBox17.MaxLength = 2;
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(39, 22);
            this.textBox17.TabIndex = 122;
            this.textBox17.TextChanged += new System.EventHandler(this.textBox17_TextChanged);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(6, 25);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(54, 14);
            this.label26.TabIndex = 19;
            this.label26.Text = "House #:";
            // 
            // pictureBox26
            // 
            this.pictureBox26.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox26.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox26.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox26.Location = new System.Drawing.Point(224, 79);
            this.pictureBox26.Name = "pictureBox26";
            this.pictureBox26.Size = new System.Drawing.Size(19, 20);
            this.pictureBox26.TabIndex = 181;
            this.pictureBox26.TabStop = false;
            this.pictureBox26.Click += new System.EventHandler(this.HideCheck);
            // 
            // textBox4
            // 
            this.textBox4.Enabled = false;
            this.textBox4.Location = new System.Drawing.Point(63, 22);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(54, 22);
            this.textBox4.TabIndex = 7;
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(116, 25);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 14);
            this.label5.TabIndex = 21;
            this.label5.Text = "Street:";
            // 
            // textBox14
            // 
            this.textBox14.Enabled = false;
            this.textBox14.Location = new System.Drawing.Point(160, 22);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(99, 22);
            this.textBox14.TabIndex = 8;
            this.textBox14.TextChanged += new System.EventHandler(this.textBox14_TextChanged);
            // 
            // chkbx_noZipCode
            // 
            this.chkbx_noZipCode.AutoSize = true;
            this.chkbx_noZipCode.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkbx_noZipCode.Enabled = false;
            this.chkbx_noZipCode.Location = new System.Drawing.Point(120, 81);
            this.chkbx_noZipCode.Name = "chkbx_noZipCode";
            this.chkbx_noZipCode.Size = new System.Drawing.Size(87, 18);
            this.chkbx_noZipCode.TabIndex = 11;
            this.chkbx_noZipCode.Text = "No Zipcode";
            this.chkbx_noZipCode.UseVisualStyleBackColor = true;
            this.chkbx_noZipCode.CheckedChanged += new System.EventHandler(this.chkbx_noZipCode_CheckedChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(29, 14);
            this.label7.TabIndex = 16;
            this.label7.Text = "City:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(179, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(38, 14);
            this.label6.TabIndex = 14;
            this.label6.Text = "State:";
            // 
            // textBox1
            // 
            this.textBox1.Enabled = false;
            this.textBox1.Location = new System.Drawing.Point(63, 79);
            this.textBox1.MaxLength = 5;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(54, 22);
            this.textBox1.TabIndex = 10;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 81);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 14);
            this.label1.TabIndex = 0;
            this.label1.Text = "Zipcode:";
            // 
            // textBox10
            // 
            this.textBox10.Enabled = false;
            this.textBox10.Location = new System.Drawing.Point(893, 120);
            this.textBox10.MaxLength = 25;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(172, 22);
            this.textBox10.TabIndex = 127;
            this.textBox10.TextChanged += new System.EventHandler(this.textBox10_TextChanged);
            // 
            // chkBx_NoPropertyAddress
            // 
            this.chkBx_NoPropertyAddress.AutoSize = true;
            this.chkBx_NoPropertyAddress.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkBx_NoPropertyAddress.Enabled = false;
            this.chkBx_NoPropertyAddress.Location = new System.Drawing.Point(1083, 160);
            this.chkBx_NoPropertyAddress.Name = "chkBx_NoPropertyAddress";
            this.chkBx_NoPropertyAddress.Size = new System.Drawing.Size(138, 32);
            this.chkBx_NoPropertyAddress.TabIndex = 129;
            this.chkBx_NoPropertyAddress.Text = "no property address \r\non invoice";
            this.chkBx_NoPropertyAddress.UseVisualStyleBackColor = true;
            this.chkBx_NoPropertyAddress.CheckedChanged += new System.EventHandler(this.chkBx_NoPropertyAddress_CheckedChanged);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox6.Enabled = false;
            this.checkBox6.Location = new System.Drawing.Point(1071, 122);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(189, 18);
            this.checkBox6.TabIndex = 128;
            this.checkBox6.Text = "no account number on invoice";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.checkBox6_CheckedChanged);
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.DodgerBlue;
            this.button9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Location = new System.Drawing.Point(1087, 230);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(82, 25);
            this.button9.TabIndex = 132;
            this.button9.Text = "Reset / Clear";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.DodgerBlue;
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Location = new System.Drawing.Point(1087, 203);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(82, 25);
            this.button7.TabIndex = 131;
            this.button7.Text = "Search";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // cmboBx_UspOnInvoice
            // 
            this.cmboBx_UspOnInvoice.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.CustomSource;
            this.cmboBx_UspOnInvoice.Enabled = false;
            this.cmboBx_UspOnInvoice.FormattingEnabled = true;
            this.cmboBx_UspOnInvoice.Location = new System.Drawing.Point(1314, 163);
            this.cmboBx_UspOnInvoice.Name = "cmboBx_UspOnInvoice";
            this.cmboBx_UspOnInvoice.Size = new System.Drawing.Size(196, 22);
            this.cmboBx_UspOnInvoice.TabIndex = 136;
            this.cmboBx_UspOnInvoice.SelectedIndexChanged += new System.EventHandler(this.cmboBx_UspOnInvoice_SelectedIndexChanged);
            // 
            // lbl_UspOnInvoice
            // 
            this.lbl_UspOnInvoice.AutoSize = true;
            this.lbl_UspOnInvoice.Location = new System.Drawing.Point(1227, 166);
            this.lbl_UspOnInvoice.Name = "lbl_UspOnInvoice";
            this.lbl_UspOnInvoice.Size = new System.Drawing.Size(86, 14);
            this.lbl_UspOnInvoice.TabIndex = 135;
            this.lbl_UspOnInvoice.Text = "USP on Invoice";
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.DodgerBlue;
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Location = new System.Drawing.Point(1456, 290);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(128, 41);
            this.button6.TabIndex = 139;
            this.button6.Text = "No Match. Search Again.";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.DodgerBlue;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Location = new System.Drawing.Point(1456, 337);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(128, 42);
            this.button5.TabIndex = 138;
            this.button5.Text = "No Match Found. Save and Next Invoice";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dataGridView1);
            this.groupBox1.Location = new System.Drawing.Point(779, 259);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(661, 123);
            this.groupBox1.TabIndex = 137;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Property Information";
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Calibri", 9F);
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.cPropertyCode,
            this.cFullAddress,
            this.cCustomerName,
            this.cActive,
            this.cInvestorName,
            this.cReosrcDate,
            this.cReoslcDate,
            this.cReosfcDate,
            this.cAccountNumber,
            this.cPodId,
            this.cproperty_status_change_date,
            this.cinactivedate});
            this.dataGridView1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dataGridView1.Location = new System.Drawing.Point(6, 20);
            this.dataGridView1.MultiSelect = false;
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dataGridView1.Size = new System.Drawing.Size(649, 96);
            this.dataGridView1.TabIndex = 16;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // cPropertyCode
            // 
            this.cPropertyCode.HeaderText = "Property ID";
            this.cPropertyCode.Name = "cPropertyCode";
            this.cPropertyCode.ReadOnly = true;
            this.cPropertyCode.Visible = false;
            // 
            // cFullAddress
            // 
            this.cFullAddress.HeaderText = "Full Address";
            this.cFullAddress.Name = "cFullAddress";
            this.cFullAddress.ReadOnly = true;
            // 
            // cCustomerName
            // 
            this.cCustomerName.HeaderText = "Client";
            this.cCustomerName.Name = "cCustomerName";
            this.cCustomerName.ReadOnly = true;
            this.cCustomerName.Visible = false;
            // 
            // cActive
            // 
            this.cActive.HeaderText = "Active";
            this.cActive.Name = "cActive";
            this.cActive.ReadOnly = true;
            this.cActive.Visible = false;
            // 
            // cInvestorName
            // 
            this.cInvestorName.HeaderText = "Investor Name";
            this.cInvestorName.Name = "cInvestorName";
            this.cInvestorName.ReadOnly = true;
            this.cInvestorName.Visible = false;
            // 
            // cReosrcDate
            // 
            this.cReosrcDate.HeaderText = "REOSRC Date";
            this.cReosrcDate.Name = "cReosrcDate";
            this.cReosrcDate.ReadOnly = true;
            this.cReosrcDate.Visible = false;
            // 
            // cReoslcDate
            // 
            this.cReoslcDate.HeaderText = "REOSLC Date";
            this.cReoslcDate.Name = "cReoslcDate";
            this.cReoslcDate.ReadOnly = true;
            this.cReoslcDate.Visible = false;
            // 
            // cReosfcDate
            // 
            this.cReosfcDate.HeaderText = "REOSFC Date";
            this.cReosfcDate.Name = "cReosfcDate";
            this.cReosfcDate.ReadOnly = true;
            this.cReosfcDate.Visible = false;
            // 
            // cAccountNumber
            // 
            this.cAccountNumber.HeaderText = "Account Number";
            this.cAccountNumber.Name = "cAccountNumber";
            this.cAccountNumber.ReadOnly = true;
            // 
            // cPodId
            // 
            this.cPodId.HeaderText = "Pod Id";
            this.cPodId.Name = "cPodId";
            this.cPodId.ReadOnly = true;
            this.cPodId.Visible = false;
            // 
            // cproperty_status_change_date
            // 
            this.cproperty_status_change_date.HeaderText = "Property Status Change Date";
            this.cproperty_status_change_date.Name = "cproperty_status_change_date";
            this.cproperty_status_change_date.ReadOnly = true;
            this.cproperty_status_change_date.Visible = false;
            // 
            // cinactivedate
            // 
            this.cinactivedate.HeaderText = "Inactive Date";
            this.cinactivedate.Name = "cinactivedate";
            this.cinactivedate.ReadOnly = true;
            this.cinactivedate.Visible = false;
            // 
            // button_noMatchUSP
            // 
            this.button_noMatchUSP.BackColor = System.Drawing.Color.DodgerBlue;
            this.button_noMatchUSP.ForeColor = System.Drawing.Color.White;
            this.button_noMatchUSP.Location = new System.Drawing.Point(1458, 488);
            this.button_noMatchUSP.Name = "button_noMatchUSP";
            this.button_noMatchUSP.Size = new System.Drawing.Size(107, 35);
            this.button_noMatchUSP.TabIndex = 147;
            this.button_noMatchUSP.Text = "No Match Found";
            this.button_noMatchUSP.UseVisualStyleBackColor = false;
            this.button_noMatchUSP.Visible = false;
            this.button_noMatchUSP.Click += new System.EventHandler(this.button_noMatchUSP_Click);
            // 
            // dgv_usp
            // 
            this.dgv_usp.AllowUserToAddRows = false;
            this.dgv_usp.AllowUserToDeleteRows = false;
            this.dgv_usp.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.dgv_usp.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Calibri", 9F);
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_usp.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv_usp.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_usp.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.vdr_id,
            this.vdr_name,
            this.vdr_address,
            this.vdr_status,
            this.vms_status});
            this.dgv_usp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.dgv_usp.Enabled = false;
            this.dgv_usp.Location = new System.Drawing.Point(785, 442);
            this.dgv_usp.Name = "dgv_usp";
            this.dgv_usp.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Calibri", 9F);
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_usp.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_usp.RowHeadersVisible = false;
            this.dgv_usp.Size = new System.Drawing.Size(668, 101);
            this.dgv_usp.TabIndex = 146;
            this.dgv_usp.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_usp_CellClick);
            // 
            // vdr_id
            // 
            this.vdr_id.HeaderText = "VDR ID";
            this.vdr_id.Name = "vdr_id";
            this.vdr_id.ReadOnly = true;
            this.vdr_id.Visible = false;
            this.vdr_id.Width = 50;
            // 
            // vdr_name
            // 
            this.vdr_name.HeaderText = "Biller Name";
            this.vdr_name.Name = "vdr_name";
            this.vdr_name.ReadOnly = true;
            this.vdr_name.Width = 89;
            // 
            // vdr_address
            // 
            this.vdr_address.HeaderText = "Biller Address";
            this.vdr_address.Name = "vdr_address";
            this.vdr_address.ReadOnly = true;
            // 
            // vdr_status
            // 
            this.vdr_status.HeaderText = "VDR Status";
            this.vdr_status.Name = "vdr_status";
            this.vdr_status.ReadOnly = true;
            this.vdr_status.Width = 84;
            // 
            // vms_status
            // 
            this.vms_status.HeaderText = "VMS Status";
            this.vms_status.Name = "vms_status";
            this.vms_status.ReadOnly = true;
            this.vms_status.Width = 85;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox8.Enabled = false;
            this.checkBox8.Location = new System.Drawing.Point(589, 5);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(152, 18);
            this.checkBox8.TabIndex = 141;
            this.checkBox8.Text = "no usp addr. on invoice";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.checkBox8_CheckedChanged);
            // 
            // comboBox5
            // 
            this.comboBox5.Enabled = false;
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(263, 3);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(320, 22);
            this.comboBox5.TabIndex = 145;
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            this.comboBox5.TextChanged += new System.EventHandler(this.comboBox5_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(254, 14);
            this.label4.TabIndex = 144;
            this.label4.Text = "What\'s the Service provider name on invoice?";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.pictureBox22);
            this.groupBox2.Controls.Add(this.pictureBox23);
            this.groupBox2.Controls.Add(this.pictureBox24);
            this.groupBox2.Controls.Add(this.pct_isbillreadableapprove);
            this.groupBox2.Controls.Add(this.pct_isbillreadablecross);
            this.groupBox2.Controls.Add(this.pct_isbillreadablecheck);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.checkBox15);
            this.groupBox2.Controls.Add(this.checkBox16);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.checkBox17);
            this.groupBox2.Controls.Add(this.checkBox18);
            this.groupBox2.Controls.Add(this.checkBox19);
            this.groupBox2.Controls.Add(this.checkBox20);
            this.groupBox2.Location = new System.Drawing.Point(779, 1);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(565, 81);
            this.groupBox2.TabIndex = 87;
            this.groupBox2.TabStop = false;
            // 
            // pictureBox22
            // 
            this.pictureBox22.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox22.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox22.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox22.Location = new System.Drawing.Point(533, 39);
            this.pictureBox22.Name = "pictureBox22";
            this.pictureBox22.Size = new System.Drawing.Size(19, 20);
            this.pictureBox22.TabIndex = 153;
            this.pictureBox22.TabStop = false;
            this.pictureBox22.Visible = false;
            this.pictureBox22.Click += new System.EventHandler(this.pictureBox22_Click);
            // 
            // pictureBox23
            // 
            this.pictureBox23.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox23.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox23.Location = new System.Drawing.Point(508, 39);
            this.pictureBox23.Name = "pictureBox23";
            this.pictureBox23.Size = new System.Drawing.Size(19, 20);
            this.pictureBox23.TabIndex = 152;
            this.pictureBox23.TabStop = false;
            this.pictureBox23.Click += new System.EventHandler(this.pictureBox23_Click);
            // 
            // pictureBox24
            // 
            this.pictureBox24.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox24.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox24.Location = new System.Drawing.Point(483, 39);
            this.pictureBox24.Name = "pictureBox24";
            this.pictureBox24.Size = new System.Drawing.Size(19, 20);
            this.pictureBox24.TabIndex = 151;
            this.pictureBox24.TabStop = false;
            this.pictureBox24.Click += new System.EventHandler(this.HideCheck);
            // 
            // pct_isbillreadableapprove
            // 
            this.pct_isbillreadableapprove.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pct_isbillreadableapprove.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pct_isbillreadableapprove.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pct_isbillreadableapprove.Location = new System.Drawing.Point(396, 8);
            this.pct_isbillreadableapprove.Name = "pct_isbillreadableapprove";
            this.pct_isbillreadableapprove.Size = new System.Drawing.Size(19, 20);
            this.pct_isbillreadableapprove.TabIndex = 150;
            this.pct_isbillreadableapprove.TabStop = false;
            this.pct_isbillreadableapprove.Visible = false;
            this.pct_isbillreadableapprove.Click += new System.EventHandler(this.pct_isbillreadableapprove_Click);
            // 
            // pct_isbillreadablecross
            // 
            this.pct_isbillreadablecross.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pct_isbillreadablecross.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pct_isbillreadablecross.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pct_isbillreadablecross.Location = new System.Drawing.Point(371, 8);
            this.pct_isbillreadablecross.Name = "pct_isbillreadablecross";
            this.pct_isbillreadablecross.Size = new System.Drawing.Size(19, 20);
            this.pct_isbillreadablecross.TabIndex = 149;
            this.pct_isbillreadablecross.TabStop = false;
            this.pct_isbillreadablecross.Click += new System.EventHandler(this.pct_isbillreadable_Click);
            // 
            // pct_isbillreadablecheck
            // 
            this.pct_isbillreadablecheck.BackgroundImage = global::UC.Properties.Resources.check;
            this.pct_isbillreadablecheck.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pct_isbillreadablecheck.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pct_isbillreadablecheck.Location = new System.Drawing.Point(346, 8);
            this.pct_isbillreadablecheck.Name = "pct_isbillreadablecheck";
            this.pct_isbillreadablecheck.Size = new System.Drawing.Size(19, 20);
            this.pct_isbillreadablecheck.TabIndex = 148;
            this.pct_isbillreadablecheck.TabStop = false;
            this.pct_isbillreadablecheck.Click += new System.EventHandler(this.HideCheck);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(3, 54);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(168, 14);
            this.label8.TabIndex = 9;
            this.label8.Text = "Please specify the type of Bill";
            // 
            // comboBox1
            // 
            this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox1.Enabled = false;
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Garbage/Trash/Disposal",
            "Stormwater/Drainage/WasteWater/Sewer",
            "TaxBill/TaxSale/property bill",
            "Cut Grass/weeds removal/Lawn maintenance",
            "Removal of debris",
            "Association/assessment fee",
            "Septic",
            "Others (Non-Billing Information)"});
            this.comboBox1.Location = new System.Drawing.Point(217, 52);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(259, 22);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Enabled = false;
            this.checkBox15.Location = new System.Drawing.Point(410, 30);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(62, 18);
            this.checkBox15.TabIndex = 7;
            this.checkBox15.Text = "Others";
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.checkBox15_CheckedChanged);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Enabled = false;
            this.checkBox16.Location = new System.Drawing.Point(347, 30);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(59, 18);
            this.checkBox16.TabIndex = 6;
            this.checkBox16.Text = "Water";
            this.checkBox16.UseVisualStyleBackColor = true;
            this.checkBox16.CheckedChanged += new System.EventHandler(this.checkBox16_CheckedChanged);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 31);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(214, 14);
            this.label11.TabIndex = 5;
            this.label11.Text = "Is this an electricity, gas or water bill?";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 11);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(122, 14);
            this.label15.TabIndex = 0;
            this.label15.Text = "Is this bill readable?";
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Enabled = false;
            this.checkBox17.Location = new System.Drawing.Point(296, 30);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(47, 18);
            this.checkBox17.TabIndex = 4;
            this.checkBox17.Text = "Gas";
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.CheckedChanged += new System.EventHandler(this.checkBox17_CheckedChanged);
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Enabled = false;
            this.checkBox18.Location = new System.Drawing.Point(218, 30);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(78, 18);
            this.checkBox18.TabIndex = 3;
            this.checkBox18.Text = "Electricity";
            this.checkBox18.UseVisualStyleBackColor = true;
            this.checkBox18.CheckedChanged += new System.EventHandler(this.checkBox18_CheckedChanged);
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Enabled = false;
            this.checkBox19.Location = new System.Drawing.Point(296, 10);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(41, 18);
            this.checkBox19.TabIndex = 2;
            this.checkBox19.Text = "No";
            this.checkBox19.UseVisualStyleBackColor = true;
            this.checkBox19.CheckedChanged += new System.EventHandler(this.checkBox19_CheckedChanged);
            // 
            // checkBox20
            // 
            this.checkBox20.AutoSize = true;
            this.checkBox20.Checked = true;
            this.checkBox20.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox20.Enabled = false;
            this.checkBox20.Location = new System.Drawing.Point(218, 10);
            this.checkBox20.Name = "checkBox20";
            this.checkBox20.Size = new System.Drawing.Size(44, 18);
            this.checkBox20.TabIndex = 1;
            this.checkBox20.Text = "Yes";
            this.checkBox20.UseVisualStyleBackColor = true;
            this.checkBox20.CheckedChanged += new System.EventHandler(this.checkBox20_CheckedChanged);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.pictureBox64);
            this.groupBox9.Controls.Add(this.chkTransAmtNo);
            this.groupBox9.Controls.Add(this.pictureBox66);
            this.groupBox9.Controls.Add(this.pictureBox44);
            this.groupBox9.Controls.Add(this.chkTransAmtYes);
            this.groupBox9.Controls.Add(this.pictureBox56);
            this.groupBox9.Controls.Add(this.label58);
            this.groupBox9.Controls.Add(this.pictureBox65);
            this.groupBox9.Controls.Add(this.pictureBox45);
            this.groupBox9.Controls.Add(this.pictureBox57);
            this.groupBox9.Controls.Add(this.pictureBox46);
            this.groupBox9.Controls.Add(this.pictureBox58);
            this.groupBox9.Controls.Add(this.pictureBox53);
            this.groupBox9.Controls.Add(this.pictureBox54);
            this.groupBox9.Controls.Add(this.pictureBox55);
            this.groupBox9.Controls.Add(this.pictureBox50);
            this.groupBox9.Controls.Add(this.pictureBox51);
            this.groupBox9.Controls.Add(this.pictureBox52);
            this.groupBox9.Controls.Add(this.pictureBox47);
            this.groupBox9.Controls.Add(this.pictureBox48);
            this.groupBox9.Controls.Add(this.pictureBox49);
            this.groupBox9.Controls.Add(this.pictureBox1);
            this.groupBox9.Controls.Add(this.pictureBox2);
            this.groupBox9.Controls.Add(this.checkBox27);
            this.groupBox9.Controls.Add(this.pictureBox9);
            this.groupBox9.Controls.Add(this.label29);
            this.groupBox9.Controls.Add(this.dateTimePicker6);
            this.groupBox9.Controls.Add(this.checkBox28);
            this.groupBox9.Controls.Add(this.checkBox29);
            this.groupBox9.Controls.Add(this.label32);
            this.groupBox9.Controls.Add(this.checkBox30);
            this.groupBox9.Controls.Add(this.checkBox31);
            this.groupBox9.Controls.Add(this.checkBox32);
            this.groupBox9.Controls.Add(this.dateTimePicker7);
            this.groupBox9.Controls.Add(this.label35);
            this.groupBox9.Controls.Add(this.dateTimePicker8);
            this.groupBox9.Controls.Add(this.dateTimePicker9);
            this.groupBox9.Controls.Add(this.label36);
            this.groupBox9.Controls.Add(this.label37);
            this.groupBox9.Controls.Add(this.dateTimePicker10);
            this.groupBox9.Controls.Add(this.label40);
            this.groupBox9.Controls.Add(this.checkBox33);
            this.groupBox9.Controls.Add(this.label42);
            this.groupBox9.Controls.Add(this.checkBox34);
            this.groupBox9.Location = new System.Drawing.Point(779, 546);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(407, 257);
            this.groupBox9.TabIndex = 142;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Billing Information";
            // 
            // pictureBox64
            // 
            this.pictureBox64.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox64.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox64.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox64.Location = new System.Drawing.Point(336, 221);
            this.pictureBox64.Name = "pictureBox64";
            this.pictureBox64.Size = new System.Drawing.Size(17, 18);
            this.pictureBox64.TabIndex = 204;
            this.pictureBox64.TabStop = false;
            this.pictureBox64.Click += new System.EventHandler(this.HideCheck);
            // 
            // chkTransAmtNo
            // 
            this.chkTransAmtNo.AutoSize = true;
            this.chkTransAmtNo.Checked = true;
            this.chkTransAmtNo.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTransAmtNo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkTransAmtNo.Enabled = false;
            this.chkTransAmtNo.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTransAmtNo.Location = new System.Drawing.Point(296, 221);
            this.chkTransAmtNo.Name = "chkTransAmtNo";
            this.chkTransAmtNo.Size = new System.Drawing.Size(39, 17);
            this.chkTransAmtNo.TabIndex = 213;
            this.chkTransAmtNo.Text = "No";
            this.chkTransAmtNo.UseVisualStyleBackColor = true;
            // 
            // pictureBox66
            // 
            this.pictureBox66.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox66.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox66.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox66.Location = new System.Drawing.Point(386, 221);
            this.pictureBox66.Name = "pictureBox66";
            this.pictureBox66.Size = new System.Drawing.Size(17, 18);
            this.pictureBox66.TabIndex = 206;
            this.pictureBox66.TabStop = false;
            this.pictureBox66.Visible = false;
            this.pictureBox66.Click += new System.EventHandler(this.pictureBox66_Click);
            // 
            // pictureBox44
            // 
            this.pictureBox44.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox44.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox44.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox44.Location = new System.Drawing.Point(385, 193);
            this.pictureBox44.Name = "pictureBox44";
            this.pictureBox44.Size = new System.Drawing.Size(17, 18);
            this.pictureBox44.TabIndex = 206;
            this.pictureBox44.TabStop = false;
            this.pictureBox44.Visible = false;
            this.pictureBox44.Click += new System.EventHandler(this.pictureBox44_Click);
            // 
            // chkTransAmtYes
            // 
            this.chkTransAmtYes.AutoSize = true;
            this.chkTransAmtYes.Cursor = System.Windows.Forms.Cursors.Hand;
            this.chkTransAmtYes.Enabled = false;
            this.chkTransAmtYes.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTransAmtYes.Location = new System.Drawing.Point(256, 220);
            this.chkTransAmtYes.Name = "chkTransAmtYes";
            this.chkTransAmtYes.Size = new System.Drawing.Size(41, 17);
            this.chkTransAmtYes.TabIndex = 214;
            this.chkTransAmtYes.Text = "Yes";
            this.chkTransAmtYes.UseVisualStyleBackColor = true;
            // 
            // pictureBox56
            // 
            this.pictureBox56.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox56.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox56.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox56.Location = new System.Drawing.Point(386, 137);
            this.pictureBox56.Name = "pictureBox56";
            this.pictureBox56.Size = new System.Drawing.Size(17, 18);
            this.pictureBox56.TabIndex = 189;
            this.pictureBox56.TabStop = false;
            this.pictureBox56.Visible = false;
            this.pictureBox56.Click += new System.EventHandler(this.pictureBox56_Click);
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(10, 220);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(136, 13);
            this.label58.TabIndex = 212;
            this.label58.Text = "Is there a Balance transfer?";
            // 
            // pictureBox65
            // 
            this.pictureBox65.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox65.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox65.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox65.Location = new System.Drawing.Point(361, 221);
            this.pictureBox65.Name = "pictureBox65";
            this.pictureBox65.Size = new System.Drawing.Size(17, 18);
            this.pictureBox65.TabIndex = 205;
            this.pictureBox65.TabStop = false;
            this.pictureBox65.Click += new System.EventHandler(this.pictureBox65_Click);
            // 
            // pictureBox45
            // 
            this.pictureBox45.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox45.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox45.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox45.Location = new System.Drawing.Point(360, 193);
            this.pictureBox45.Name = "pictureBox45";
            this.pictureBox45.Size = new System.Drawing.Size(17, 18);
            this.pictureBox45.TabIndex = 205;
            this.pictureBox45.TabStop = false;
            this.pictureBox45.Click += new System.EventHandler(this.pictureBox45_Click);
            // 
            // pictureBox57
            // 
            this.pictureBox57.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox57.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox57.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox57.Location = new System.Drawing.Point(361, 137);
            this.pictureBox57.Name = "pictureBox57";
            this.pictureBox57.Size = new System.Drawing.Size(17, 18);
            this.pictureBox57.TabIndex = 188;
            this.pictureBox57.TabStop = false;
            this.pictureBox57.Click += new System.EventHandler(this.pictureBox57_Click);
            // 
            // pictureBox46
            // 
            this.pictureBox46.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox46.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox46.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox46.Location = new System.Drawing.Point(335, 193);
            this.pictureBox46.Name = "pictureBox46";
            this.pictureBox46.Size = new System.Drawing.Size(17, 18);
            this.pictureBox46.TabIndex = 204;
            this.pictureBox46.TabStop = false;
            this.pictureBox46.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox58
            // 
            this.pictureBox58.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox58.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox58.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox58.Location = new System.Drawing.Point(336, 137);
            this.pictureBox58.Name = "pictureBox58";
            this.pictureBox58.Size = new System.Drawing.Size(17, 18);
            this.pictureBox58.TabIndex = 187;
            this.pictureBox58.TabStop = false;
            this.pictureBox58.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox53
            // 
            this.pictureBox53.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox53.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox53.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox53.Location = new System.Drawing.Point(354, 79);
            this.pictureBox53.Name = "pictureBox53";
            this.pictureBox53.Size = new System.Drawing.Size(17, 18);
            this.pictureBox53.TabIndex = 186;
            this.pictureBox53.TabStop = false;
            this.pictureBox53.Visible = false;
            this.pictureBox53.Click += new System.EventHandler(this.pictureBox53_Click);
            // 
            // pictureBox54
            // 
            this.pictureBox54.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox54.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox54.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox54.Location = new System.Drawing.Point(329, 79);
            this.pictureBox54.Name = "pictureBox54";
            this.pictureBox54.Size = new System.Drawing.Size(17, 18);
            this.pictureBox54.TabIndex = 185;
            this.pictureBox54.TabStop = false;
            this.pictureBox54.Click += new System.EventHandler(this.pictureBox54_Click);
            // 
            // pictureBox55
            // 
            this.pictureBox55.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox55.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox55.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox55.Location = new System.Drawing.Point(304, 79);
            this.pictureBox55.Name = "pictureBox55";
            this.pictureBox55.Size = new System.Drawing.Size(17, 18);
            this.pictureBox55.TabIndex = 184;
            this.pictureBox55.TabStop = false;
            this.pictureBox55.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox50
            // 
            this.pictureBox50.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox50.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox50.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox50.Location = new System.Drawing.Point(354, 51);
            this.pictureBox50.Name = "pictureBox50";
            this.pictureBox50.Size = new System.Drawing.Size(17, 18);
            this.pictureBox50.TabIndex = 183;
            this.pictureBox50.TabStop = false;
            this.pictureBox50.Visible = false;
            this.pictureBox50.Click += new System.EventHandler(this.pictureBox50_Click);
            // 
            // pictureBox51
            // 
            this.pictureBox51.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox51.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox51.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox51.Location = new System.Drawing.Point(329, 51);
            this.pictureBox51.Name = "pictureBox51";
            this.pictureBox51.Size = new System.Drawing.Size(17, 18);
            this.pictureBox51.TabIndex = 182;
            this.pictureBox51.TabStop = false;
            this.pictureBox51.Click += new System.EventHandler(this.pictureBox51_Click);
            // 
            // pictureBox52
            // 
            this.pictureBox52.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox52.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox52.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox52.Location = new System.Drawing.Point(304, 51);
            this.pictureBox52.Name = "pictureBox52";
            this.pictureBox52.Size = new System.Drawing.Size(17, 18);
            this.pictureBox52.TabIndex = 181;
            this.pictureBox52.TabStop = false;
            this.pictureBox52.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox47
            // 
            this.pictureBox47.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox47.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox47.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox47.Location = new System.Drawing.Point(354, 24);
            this.pictureBox47.Name = "pictureBox47";
            this.pictureBox47.Size = new System.Drawing.Size(17, 18);
            this.pictureBox47.TabIndex = 180;
            this.pictureBox47.TabStop = false;
            this.pictureBox47.Visible = false;
            this.pictureBox47.Click += new System.EventHandler(this.pictureBox47_Click);
            // 
            // pictureBox48
            // 
            this.pictureBox48.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox48.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox48.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox48.Location = new System.Drawing.Point(329, 24);
            this.pictureBox48.Name = "pictureBox48";
            this.pictureBox48.Size = new System.Drawing.Size(17, 18);
            this.pictureBox48.TabIndex = 179;
            this.pictureBox48.TabStop = false;
            this.pictureBox48.Click += new System.EventHandler(this.pictureBox48_Click);
            // 
            // pictureBox49
            // 
            this.pictureBox49.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox49.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox49.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox49.Location = new System.Drawing.Point(304, 24);
            this.pictureBox49.Name = "pictureBox49";
            this.pictureBox49.Size = new System.Drawing.Size(17, 18);
            this.pictureBox49.TabIndex = 178;
            this.pictureBox49.TabStop = false;
            this.pictureBox49.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox1.Location = new System.Drawing.Point(386, 164);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(17, 18);
            this.pictureBox1.TabIndex = 177;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox2.Location = new System.Drawing.Point(361, 164);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(17, 18);
            this.pictureBox2.TabIndex = 176;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // checkBox27
            // 
            this.checkBox27.AutoSize = true;
            this.checkBox27.Enabled = false;
            this.checkBox27.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox27.Location = new System.Drawing.Point(183, 139);
            this.checkBox27.Name = "checkBox27";
            this.checkBox27.Size = new System.Drawing.Size(41, 17);
            this.checkBox27.TabIndex = 129;
            this.checkBox27.Text = "Yes";
            this.checkBox27.UseVisualStyleBackColor = true;
            // 
            // pictureBox9
            // 
            this.pictureBox9.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox9.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox9.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox9.Location = new System.Drawing.Point(336, 164);
            this.pictureBox9.Name = "pictureBox9";
            this.pictureBox9.Size = new System.Drawing.Size(17, 18);
            this.pictureBox9.TabIndex = 175;
            this.pictureBox9.TabStop = false;
            this.pictureBox9.Click += new System.EventHandler(this.HideCheck);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(10, 140);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(146, 13);
            this.label29.TabIndex = 128;
            this.label29.Text = "Is there a disconnection date:";
            // 
            // dateTimePicker6
            // 
            this.dateTimePicker6.Enabled = false;
            this.dateTimePicker6.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker6.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker6.Location = new System.Drawing.Point(233, 136);
            this.dateTimePicker6.Name = "dateTimePicker6";
            this.dateTimePicker6.Size = new System.Drawing.Size(97, 21);
            this.dateTimePicker6.TabIndex = 127;
            this.dateTimePicker6.ValueChanged += new System.EventHandler(this.dateTimePicker6_ValueChanged);
            // 
            // checkBox28
            // 
            this.checkBox28.AutoSize = true;
            this.checkBox28.Checked = true;
            this.checkBox28.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox28.Enabled = false;
            this.checkBox28.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox28.Location = new System.Drawing.Point(296, 167);
            this.checkBox28.Name = "checkBox28";
            this.checkBox28.Size = new System.Drawing.Size(39, 17);
            this.checkBox28.TabIndex = 84;
            this.checkBox28.Text = "No";
            this.checkBox28.UseVisualStyleBackColor = true;
            this.checkBox28.CheckedChanged += new System.EventHandler(this.checkBox28_CheckedChanged);
            // 
            // checkBox29
            // 
            this.checkBox29.AutoSize = true;
            this.checkBox29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox29.Enabled = false;
            this.checkBox29.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox29.Location = new System.Drawing.Point(256, 167);
            this.checkBox29.Name = "checkBox29";
            this.checkBox29.Size = new System.Drawing.Size(41, 17);
            this.checkBox29.TabIndex = 83;
            this.checkBox29.Text = "Yes";
            this.checkBox29.UseVisualStyleBackColor = true;
            this.checkBox29.CheckedChanged += new System.EventHandler(this.checkBox29_CheckedChanged);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(10, 168);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(220, 13);
            this.label32.TabIndex = 82;
            this.label32.Text = "Is there a comment that says \"DO NOT MAIL\" ?";
            // 
            // checkBox30
            // 
            this.checkBox30.AutoSize = true;
            this.checkBox30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox30.Enabled = false;
            this.checkBox30.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox30.Location = new System.Drawing.Point(195, 27);
            this.checkBox30.Name = "checkBox30";
            this.checkBox30.Size = new System.Drawing.Size(100, 17);
            this.checkBox30.TabIndex = 81;
            this.checkBox30.Text = "No Invoice Date";
            this.checkBox30.UseVisualStyleBackColor = true;
            this.checkBox30.CheckedChanged += new System.EventHandler(this.checkBox30_CheckedChanged);
            // 
            // checkBox31
            // 
            this.checkBox31.AutoSize = true;
            this.checkBox31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox31.Enabled = false;
            this.checkBox31.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox31.Location = new System.Drawing.Point(196, 82);
            this.checkBox31.Name = "checkBox31";
            this.checkBox31.Size = new System.Drawing.Size(97, 17);
            this.checkBox31.TabIndex = 80;
            this.checkBox31.Text = "No Billing Cycle";
            this.checkBox31.UseVisualStyleBackColor = true;
            this.checkBox31.CheckedChanged += new System.EventHandler(this.checkBox31_CheckedChanged);
            // 
            // checkBox32
            // 
            this.checkBox32.AutoSize = true;
            this.checkBox32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox32.Enabled = false;
            this.checkBox32.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox32.Location = new System.Drawing.Point(196, 55);
            this.checkBox32.Name = "checkBox32";
            this.checkBox32.Size = new System.Drawing.Size(85, 17);
            this.checkBox32.TabIndex = 79;
            this.checkBox32.Text = "No Due Date";
            this.checkBox32.UseVisualStyleBackColor = true;
            this.checkBox32.CheckedChanged += new System.EventHandler(this.checkBox32_CheckedChanged);
            // 
            // dateTimePicker7
            // 
            this.dateTimePicker7.Enabled = false;
            this.dateTimePicker7.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker7.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker7.Location = new System.Drawing.Point(92, 51);
            this.dateTimePicker7.Name = "dateTimePicker7";
            this.dateTimePicker7.Size = new System.Drawing.Size(96, 21);
            this.dateTimePicker7.TabIndex = 20;
            this.dateTimePicker7.ValueChanged += new System.EventHandler(this.dateTimePicker7_ValueChanged);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(10, 56);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 13);
            this.label35.TabIndex = 4;
            this.label35.Text = "Due Date:";
            // 
            // dateTimePicker8
            // 
            this.dateTimePicker8.Enabled = false;
            this.dateTimePicker8.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker8.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker8.Location = new System.Drawing.Point(92, 79);
            this.dateTimePicker8.Name = "dateTimePicker8";
            this.dateTimePicker8.Size = new System.Drawing.Size(97, 21);
            this.dateTimePicker8.TabIndex = 21;
            this.dateTimePicker8.ValueChanged += new System.EventHandler(this.dateTimePicker8_ValueChanged);
            // 
            // dateTimePicker9
            // 
            this.dateTimePicker9.Enabled = false;
            this.dateTimePicker9.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker9.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker9.Location = new System.Drawing.Point(91, 24);
            this.dateTimePicker9.Name = "dateTimePicker9";
            this.dateTimePicker9.Size = new System.Drawing.Size(97, 21);
            this.dateTimePicker9.TabIndex = 19;
            this.dateTimePicker9.ValueChanged += new System.EventHandler(this.dateTimePicker9_ValueChanged);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(10, 28);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(69, 13);
            this.label36.TabIndex = 78;
            this.label36.Text = "Invoice Date:";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(10, 83);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(70, 13);
            this.label37.TabIndex = 27;
            this.label37.Text = "Service From:";
            // 
            // dateTimePicker10
            // 
            this.dateTimePicker10.Enabled = false;
            this.dateTimePicker10.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker10.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker10.Location = new System.Drawing.Point(92, 105);
            this.dateTimePicker10.Name = "dateTimePicker10";
            this.dateTimePicker10.Size = new System.Drawing.Size(97, 21);
            this.dateTimePicker10.TabIndex = 22;
            this.dateTimePicker10.ValueChanged += new System.EventHandler(this.dateTimePicker10_ValueChanged);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(10, 111);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(56, 13);
            this.label40.TabIndex = 29;
            this.label40.Text = "Service To:";
            // 
            // checkBox33
            // 
            this.checkBox33.AutoSize = true;
            this.checkBox33.Checked = true;
            this.checkBox33.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox33.Enabled = false;
            this.checkBox33.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox33.Location = new System.Drawing.Point(296, 195);
            this.checkBox33.Name = "checkBox33";
            this.checkBox33.Size = new System.Drawing.Size(39, 17);
            this.checkBox33.TabIndex = 130;
            this.checkBox33.Text = "No";
            this.checkBox33.UseVisualStyleBackColor = true;
            this.checkBox33.CheckedChanged += new System.EventHandler(this.checkBox33_CheckedChanged);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(10, 195);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(205, 13);
            this.label42.TabIndex = 112;
            this.label42.Text = "Is there a comment that says \"FINAL BILL\"?";
            // 
            // checkBox34
            // 
            this.checkBox34.AutoSize = true;
            this.checkBox34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.checkBox34.Enabled = false;
            this.checkBox34.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox34.Location = new System.Drawing.Point(256, 195);
            this.checkBox34.Name = "checkBox34";
            this.checkBox34.Size = new System.Drawing.Size(41, 17);
            this.checkBox34.TabIndex = 130;
            this.checkBox34.Text = "Yes";
            this.checkBox34.UseVisualStyleBackColor = true;
            this.checkBox34.CheckedChanged += new System.EventHandler(this.checkBox34_CheckedChanged);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Enabled = false;
            this.label41.Location = new System.Drawing.Point(964, 811);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(59, 14);
            this.label41.TabIndex = 132;
            this.label41.Text = "Deposit $";
            this.label41.Visible = false;
            // 
            // textBox18
            // 
            this.textBox18.Enabled = false;
            this.textBox18.Location = new System.Drawing.Point(1029, 808);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(41, 22);
            this.textBox18.TabIndex = 131;
            this.textBox18.Text = "0";
            this.textBox18.Visible = false;
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label12);
            this.groupBox10.Controls.Add(this.label10);
            this.groupBox10.Controls.Add(this.pictureBox76);
            this.groupBox10.Controls.Add(this.pictureBox73);
            this.groupBox10.Controls.Add(this.pictureBox34);
            this.groupBox10.Controls.Add(this.pictureBox75);
            this.groupBox10.Controls.Add(this.pictureBox72);
            this.groupBox10.Controls.Add(this.pictureBox38);
            this.groupBox10.Controls.Add(this.pictureBox74);
            this.groupBox10.Controls.Add(this.pictureBox71);
            this.groupBox10.Controls.Add(this.pictureBox40);
            this.groupBox10.Controls.Add(this.textBox12);
            this.groupBox10.Controls.Add(this.textBox11);
            this.groupBox10.Controls.Add(this.label27);
            this.groupBox10.Controls.Add(this.textBox7);
            this.groupBox10.Controls.Add(this.label25);
            this.groupBox10.Controls.Add(this.txtDepRef);
            this.groupBox10.Controls.Add(this.txtDepPy);
            this.groupBox10.Controls.Add(this.textBox8);
            this.groupBox10.Controls.Add(this.label9);
            this.groupBox10.Controls.Add(this.pictureBox41);
            this.groupBox10.Controls.Add(this.pictureBox42);
            this.groupBox10.Controls.Add(this.pictureBox43);
            this.groupBox10.Controls.Add(this.pictureBox31);
            this.groupBox10.Controls.Add(this.pictureBox32);
            this.groupBox10.Controls.Add(this.pictureBox33);
            this.groupBox10.Controls.Add(this.pictureBox19);
            this.groupBox10.Controls.Add(this.pictureBox20);
            this.groupBox10.Controls.Add(this.pictureBox21);
            this.groupBox10.Controls.Add(this.pictureBox16);
            this.groupBox10.Controls.Add(this.pictureBox17);
            this.groupBox10.Controls.Add(this.pictureBox18);
            this.groupBox10.Controls.Add(this.pictureBox6);
            this.groupBox10.Controls.Add(this.pictureBox7);
            this.groupBox10.Controls.Add(this.pictureBox8);
            this.groupBox10.Controls.Add(this.textBox5);
            this.groupBox10.Controls.Add(this.textBox6);
            this.groupBox10.Controls.Add(this.label2);
            this.groupBox10.Controls.Add(this.textBox3);
            this.groupBox10.Controls.Add(this.textBox2);
            this.groupBox10.Controls.Add(this.pictureBox10);
            this.groupBox10.Controls.Add(this.pictureBox11);
            this.groupBox10.Controls.Add(this.pictureBox12);
            this.groupBox10.Controls.Add(this.textBox19);
            this.groupBox10.Controls.Add(this.label44);
            this.groupBox10.Controls.Add(this.label45);
            this.groupBox10.Controls.Add(this.label47);
            this.groupBox10.Controls.Add(this.textBox20);
            this.groupBox10.Controls.Add(this.textBox21);
            this.groupBox10.Controls.Add(this.checkBox35);
            this.groupBox10.Controls.Add(this.label48);
            this.groupBox10.Controls.Add(this.textBox22);
            this.groupBox10.Controls.Add(this.textBox23);
            this.groupBox10.Controls.Add(this.textBox24);
            this.groupBox10.Controls.Add(this.label49);
            this.groupBox10.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox10.Location = new System.Drawing.Point(1192, 546);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(392, 257);
            this.groupBox10.TabIndex = 143;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Payment Breakdown";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(172, 216);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(13, 13);
            this.label12.TabIndex = 211;
            this.label12.Text = "$";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(172, 193);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(13, 13);
            this.label10.TabIndex = 210;
            this.label10.Text = "$";
            // 
            // pictureBox76
            // 
            this.pictureBox76.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox76.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox76.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox76.Location = new System.Drawing.Point(353, 165);
            this.pictureBox76.Name = "pictureBox76";
            this.pictureBox76.Size = new System.Drawing.Size(17, 18);
            this.pictureBox76.TabIndex = 209;
            this.pictureBox76.TabStop = false;
            this.pictureBox76.Visible = false;
            this.pictureBox76.Click += new System.EventHandler(this.pictureBox76_Click);
            // 
            // pictureBox73
            // 
            this.pictureBox73.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox73.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox73.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox73.Location = new System.Drawing.Point(353, 141);
            this.pictureBox73.Name = "pictureBox73";
            this.pictureBox73.Size = new System.Drawing.Size(17, 18);
            this.pictureBox73.TabIndex = 209;
            this.pictureBox73.TabStop = false;
            this.pictureBox73.Visible = false;
            this.pictureBox73.Click += new System.EventHandler(this.pictureBox73_Click);
            // 
            // pictureBox34
            // 
            this.pictureBox34.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox34.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox34.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox34.Location = new System.Drawing.Point(353, 116);
            this.pictureBox34.Name = "pictureBox34";
            this.pictureBox34.Size = new System.Drawing.Size(17, 18);
            this.pictureBox34.TabIndex = 209;
            this.pictureBox34.TabStop = false;
            this.pictureBox34.Visible = false;
            this.pictureBox34.Click += new System.EventHandler(this.pictureBox34_Click);
            // 
            // pictureBox75
            // 
            this.pictureBox75.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox75.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox75.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox75.Location = new System.Drawing.Point(328, 165);
            this.pictureBox75.Name = "pictureBox75";
            this.pictureBox75.Size = new System.Drawing.Size(17, 18);
            this.pictureBox75.TabIndex = 208;
            this.pictureBox75.TabStop = false;
            this.pictureBox75.Click += new System.EventHandler(this.pictureBox75_Click);
            // 
            // pictureBox72
            // 
            this.pictureBox72.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox72.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox72.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox72.Location = new System.Drawing.Point(328, 141);
            this.pictureBox72.Name = "pictureBox72";
            this.pictureBox72.Size = new System.Drawing.Size(17, 18);
            this.pictureBox72.TabIndex = 208;
            this.pictureBox72.TabStop = false;
            this.pictureBox72.Click += new System.EventHandler(this.pictureBox72_Click);
            // 
            // pictureBox38
            // 
            this.pictureBox38.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox38.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox38.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox38.Location = new System.Drawing.Point(328, 116);
            this.pictureBox38.Name = "pictureBox38";
            this.pictureBox38.Size = new System.Drawing.Size(17, 18);
            this.pictureBox38.TabIndex = 208;
            this.pictureBox38.TabStop = false;
            this.pictureBox38.Click += new System.EventHandler(this.pictureBox38_Click);
            // 
            // pictureBox74
            // 
            this.pictureBox74.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox74.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox74.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox74.Location = new System.Drawing.Point(303, 165);
            this.pictureBox74.Name = "pictureBox74";
            this.pictureBox74.Size = new System.Drawing.Size(17, 18);
            this.pictureBox74.TabIndex = 207;
            this.pictureBox74.TabStop = false;
            this.pictureBox74.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox71
            // 
            this.pictureBox71.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox71.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox71.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox71.Location = new System.Drawing.Point(303, 141);
            this.pictureBox71.Name = "pictureBox71";
            this.pictureBox71.Size = new System.Drawing.Size(17, 18);
            this.pictureBox71.TabIndex = 207;
            this.pictureBox71.TabStop = false;
            this.pictureBox71.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox40
            // 
            this.pictureBox40.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox40.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox40.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox40.Location = new System.Drawing.Point(303, 116);
            this.pictureBox40.Name = "pictureBox40";
            this.pictureBox40.Size = new System.Drawing.Size(17, 18);
            this.pictureBox40.TabIndex = 207;
            this.pictureBox40.TabStop = false;
            this.pictureBox40.Click += new System.EventHandler(this.HideCheck);
            // 
            // textBox12
            // 
            this.textBox12.BackColor = System.Drawing.Color.White;
            this.textBox12.Enabled = false;
            this.textBox12.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox12.Location = new System.Drawing.Point(8, 165);
            this.textBox12.Multiline = true;
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(156, 18);
            this.textBox12.TabIndex = 206;
            this.textBox12.Text = "Deposit Refund";
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.White;
            this.textBox11.Enabled = false;
            this.textBox11.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.Location = new System.Drawing.Point(7, 141);
            this.textBox11.Multiline = true;
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(156, 18);
            this.textBox11.TabIndex = 206;
            this.textBox11.Text = "Deposit Payment";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(172, 167);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(13, 13);
            this.label27.TabIndex = 205;
            this.label27.Text = "$";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.White;
            this.textBox7.Enabled = false;
            this.textBox7.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(7, 117);
            this.textBox7.Multiline = true;
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(156, 18);
            this.textBox7.TabIndex = 206;
            this.textBox7.Text = "Disconnection Fee";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(172, 144);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(13, 13);
            this.label25.TabIndex = 205;
            this.label25.Text = "$";
            // 
            // txtDepRef
            // 
            this.txtDepRef.Enabled = false;
            this.txtDepRef.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDepRef.Location = new System.Drawing.Point(188, 165);
            this.txtDepRef.Multiline = true;
            this.txtDepRef.Name = "txtDepRef";
            this.txtDepRef.Size = new System.Drawing.Size(106, 18);
            this.txtDepRef.TabIndex = 204;
            this.txtDepRef.Text = "0.00";
            this.txtDepRef.TextChanged += new System.EventHandler(this.txtDepRef_TextChanged);
            // 
            // txtDepPy
            // 
            this.txtDepPy.Enabled = false;
            this.txtDepPy.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDepPy.Location = new System.Drawing.Point(188, 142);
            this.txtDepPy.Multiline = true;
            this.txtDepPy.Name = "txtDepPy";
            this.txtDepPy.Size = new System.Drawing.Size(106, 18);
            this.txtDepPy.TabIndex = 204;
            this.txtDepPy.Text = "0.00";
            this.txtDepPy.TextChanged += new System.EventHandler(this.txtDepPy_TextChanged);
            // 
            // textBox8
            // 
            this.textBox8.Enabled = false;
            this.textBox8.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(188, 117);
            this.textBox8.Multiline = true;
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(106, 18);
            this.textBox8.TabIndex = 204;
            this.textBox8.Text = "0.00";
            this.textBox8.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(172, 121);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(13, 13);
            this.label9.TabIndex = 205;
            this.label9.Text = "$";
            // 
            // pictureBox41
            // 
            this.pictureBox41.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox41.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox41.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox41.Location = new System.Drawing.Point(353, 212);
            this.pictureBox41.Name = "pictureBox41";
            this.pictureBox41.Size = new System.Drawing.Size(17, 18);
            this.pictureBox41.TabIndex = 203;
            this.pictureBox41.TabStop = false;
            this.pictureBox41.Visible = false;
            this.pictureBox41.Click += new System.EventHandler(this.pictureBox41_Click);
            // 
            // pictureBox42
            // 
            this.pictureBox42.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox42.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox42.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox42.Location = new System.Drawing.Point(328, 212);
            this.pictureBox42.Name = "pictureBox42";
            this.pictureBox42.Size = new System.Drawing.Size(17, 18);
            this.pictureBox42.TabIndex = 202;
            this.pictureBox42.TabStop = false;
            this.pictureBox42.Click += new System.EventHandler(this.pictureBox42_Click);
            // 
            // pictureBox43
            // 
            this.pictureBox43.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox43.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox43.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox43.Location = new System.Drawing.Point(303, 212);
            this.pictureBox43.Name = "pictureBox43";
            this.pictureBox43.Size = new System.Drawing.Size(17, 18);
            this.pictureBox43.TabIndex = 201;
            this.pictureBox43.TabStop = false;
            this.pictureBox43.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox31
            // 
            this.pictureBox31.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox31.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox31.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox31.Location = new System.Drawing.Point(353, 92);
            this.pictureBox31.Name = "pictureBox31";
            this.pictureBox31.Size = new System.Drawing.Size(17, 18);
            this.pictureBox31.TabIndex = 197;
            this.pictureBox31.TabStop = false;
            this.pictureBox31.Visible = false;
            this.pictureBox31.Click += new System.EventHandler(this.pictureBox31_Click);
            // 
            // pictureBox32
            // 
            this.pictureBox32.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox32.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox32.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox32.Location = new System.Drawing.Point(328, 92);
            this.pictureBox32.Name = "pictureBox32";
            this.pictureBox32.Size = new System.Drawing.Size(17, 18);
            this.pictureBox32.TabIndex = 196;
            this.pictureBox32.TabStop = false;
            this.pictureBox32.Click += new System.EventHandler(this.pictureBox32_Click);
            // 
            // pictureBox33
            // 
            this.pictureBox33.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox33.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox33.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox33.Location = new System.Drawing.Point(303, 92);
            this.pictureBox33.Name = "pictureBox33";
            this.pictureBox33.Size = new System.Drawing.Size(17, 18);
            this.pictureBox33.TabIndex = 195;
            this.pictureBox33.TabStop = false;
            this.pictureBox33.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox19
            // 
            this.pictureBox19.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox19.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox19.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox19.Location = new System.Drawing.Point(353, 68);
            this.pictureBox19.Name = "pictureBox19";
            this.pictureBox19.Size = new System.Drawing.Size(17, 18);
            this.pictureBox19.TabIndex = 194;
            this.pictureBox19.TabStop = false;
            this.pictureBox19.Visible = false;
            this.pictureBox19.Click += new System.EventHandler(this.pictureBox19_Click);
            // 
            // pictureBox20
            // 
            this.pictureBox20.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox20.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox20.Location = new System.Drawing.Point(328, 68);
            this.pictureBox20.Name = "pictureBox20";
            this.pictureBox20.Size = new System.Drawing.Size(17, 18);
            this.pictureBox20.TabIndex = 193;
            this.pictureBox20.TabStop = false;
            this.pictureBox20.Click += new System.EventHandler(this.pictureBox20_Click);
            // 
            // pictureBox21
            // 
            this.pictureBox21.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox21.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox21.Location = new System.Drawing.Point(303, 68);
            this.pictureBox21.Name = "pictureBox21";
            this.pictureBox21.Size = new System.Drawing.Size(17, 18);
            this.pictureBox21.TabIndex = 192;
            this.pictureBox21.TabStop = false;
            this.pictureBox21.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox16
            // 
            this.pictureBox16.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox16.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox16.Location = new System.Drawing.Point(353, 45);
            this.pictureBox16.Name = "pictureBox16";
            this.pictureBox16.Size = new System.Drawing.Size(17, 18);
            this.pictureBox16.TabIndex = 191;
            this.pictureBox16.TabStop = false;
            this.pictureBox16.Visible = false;
            this.pictureBox16.Click += new System.EventHandler(this.pictureBox16_Click);
            // 
            // pictureBox17
            // 
            this.pictureBox17.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox17.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox17.Location = new System.Drawing.Point(328, 45);
            this.pictureBox17.Name = "pictureBox17";
            this.pictureBox17.Size = new System.Drawing.Size(17, 18);
            this.pictureBox17.TabIndex = 190;
            this.pictureBox17.TabStop = false;
            this.pictureBox17.Click += new System.EventHandler(this.pictureBox17_Click);
            // 
            // pictureBox18
            // 
            this.pictureBox18.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox18.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox18.Location = new System.Drawing.Point(303, 45);
            this.pictureBox18.Name = "pictureBox18";
            this.pictureBox18.Size = new System.Drawing.Size(17, 18);
            this.pictureBox18.TabIndex = 189;
            this.pictureBox18.TabStop = false;
            this.pictureBox18.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox6.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox6.Location = new System.Drawing.Point(353, 20);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(17, 18);
            this.pictureBox6.TabIndex = 188;
            this.pictureBox6.TabStop = false;
            this.pictureBox6.Visible = false;
            this.pictureBox6.Click += new System.EventHandler(this.pictureBox6_Click);
            // 
            // pictureBox7
            // 
            this.pictureBox7.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox7.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox7.Location = new System.Drawing.Point(328, 20);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(17, 18);
            this.pictureBox7.TabIndex = 187;
            this.pictureBox7.TabStop = false;
            this.pictureBox7.Click += new System.EventHandler(this.pictureBox7_Click);
            // 
            // pictureBox8
            // 
            this.pictureBox8.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox8.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox8.Location = new System.Drawing.Point(303, 20);
            this.pictureBox8.Name = "pictureBox8";
            this.pictureBox8.Size = new System.Drawing.Size(17, 18);
            this.pictureBox8.TabIndex = 186;
            this.pictureBox8.TabStop = false;
            this.pictureBox8.Click += new System.EventHandler(this.HideCheck);
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.White;
            this.textBox5.Enabled = false;
            this.textBox5.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(7, 93);
            this.textBox5.Multiline = true;
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(156, 18);
            this.textBox5.TabIndex = 185;
            this.textBox5.Text = "Late Fee";
            // 
            // textBox6
            // 
            this.textBox6.Enabled = false;
            this.textBox6.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.Location = new System.Drawing.Point(188, 93);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(106, 18);
            this.textBox6.TabIndex = 183;
            this.textBox6.Text = "0.00";
            this.textBox6.TextChanged += new System.EventHandler(this.textBox6_TextChanged);
            this.textBox6.Leave += new System.EventHandler(this.ComputeAmount);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(172, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(13, 13);
            this.label2.TabIndex = 184;
            this.label2.Text = "$";
            // 
            // textBox3
            // 
            this.textBox3.BackColor = System.Drawing.Color.White;
            this.textBox3.Enabled = false;
            this.textBox3.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox3.Location = new System.Drawing.Point(7, 70);
            this.textBox3.Multiline = true;
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(156, 18);
            this.textBox3.TabIndex = 182;
            this.textBox3.Text = "Current Charge";
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.White;
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(7, 44);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(156, 18);
            this.textBox2.TabIndex = 181;
            this.textBox2.Text = "Payment Received";
            // 
            // pictureBox10
            // 
            this.pictureBox10.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox10.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox10.Location = new System.Drawing.Point(321, 237);
            this.pictureBox10.Name = "pictureBox10";
            this.pictureBox10.Size = new System.Drawing.Size(17, 18);
            this.pictureBox10.TabIndex = 180;
            this.pictureBox10.TabStop = false;
            this.pictureBox10.Visible = false;
            this.pictureBox10.Click += new System.EventHandler(this.pictureBox10_Click);
            // 
            // pictureBox11
            // 
            this.pictureBox11.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox11.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox11.Location = new System.Drawing.Point(296, 237);
            this.pictureBox11.Name = "pictureBox11";
            this.pictureBox11.Size = new System.Drawing.Size(17, 18);
            this.pictureBox11.TabIndex = 179;
            this.pictureBox11.TabStop = false;
            this.pictureBox11.Click += new System.EventHandler(this.pictureBox11_Click);
            // 
            // pictureBox12
            // 
            this.pictureBox12.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox12.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox12.Location = new System.Drawing.Point(271, 237);
            this.pictureBox12.Name = "pictureBox12";
            this.pictureBox12.Size = new System.Drawing.Size(17, 18);
            this.pictureBox12.TabIndex = 178;
            this.pictureBox12.TabStop = false;
            this.pictureBox12.Click += new System.EventHandler(this.HideCheck);
            // 
            // textBox19
            // 
            this.textBox19.BackColor = System.Drawing.Color.White;
            this.textBox19.Enabled = false;
            this.textBox19.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox19.Location = new System.Drawing.Point(7, 20);
            this.textBox19.Multiline = true;
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(156, 18);
            this.textBox19.TabIndex = 111;
            this.textBox19.Text = "Previous Balance";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(10, 193);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(135, 13);
            this.label44.TabIndex = 103;
            this.label44.Text = "TOTAL AMOUNT ON INVOICE:";
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(10, 216);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(133, 13);
            this.label45.TabIndex = 106;
            this.label45.Text = "TOTAL AMT AFTER DUE DATE:";
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(172, 23);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(13, 13);
            this.label47.TabIndex = 93;
            this.label47.Text = "$";
            // 
            // textBox20
            // 
            this.textBox20.Enabled = false;
            this.textBox20.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox20.Location = new System.Drawing.Point(188, 20);
            this.textBox20.Multiline = true;
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(106, 18);
            this.textBox20.TabIndex = 24;
            this.textBox20.Text = "0.00";
            this.textBox20.TextChanged += new System.EventHandler(this.textBox20_TextChanged);
            this.textBox20.Leave += new System.EventHandler(this.ComputeAmount);
            // 
            // textBox21
            // 
            this.textBox21.Enabled = false;
            this.textBox21.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox21.Location = new System.Drawing.Point(188, 44);
            this.textBox21.Multiline = true;
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(106, 18);
            this.textBox21.TabIndex = 27;
            this.textBox21.Text = "0.00";
            this.textBox21.TextChanged += new System.EventHandler(this.textBox21_TextChanged);
            this.textBox21.Leave += new System.EventHandler(this.ComputeAmount);
            // 
            // checkBox35
            // 
            this.checkBox35.AutoSize = true;
            this.checkBox35.Enabled = false;
            this.checkBox35.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox35.Location = new System.Drawing.Point(188, 237);
            this.checkBox35.Name = "checkBox35";
            this.checkBox35.Size = new System.Drawing.Size(77, 17);
            this.checkBox35.TabIndex = 25;
            this.checkBox35.Text = "no amount";
            this.checkBox35.UseVisualStyleBackColor = true;
            this.checkBox35.CheckedChanged += new System.EventHandler(this.checkBox35_CheckedChanged);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(172, 47);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(13, 13);
            this.label48.TabIndex = 97;
            this.label48.Text = "$";
            // 
            // textBox22
            // 
            this.textBox22.Enabled = false;
            this.textBox22.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox22.Location = new System.Drawing.Point(188, 213);
            this.textBox22.Multiline = true;
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(106, 18);
            this.textBox22.TabIndex = 33;
            this.textBox22.TextChanged += new System.EventHandler(this.textBox22_TextChanged);
            // 
            // textBox23
            // 
            this.textBox23.Enabled = false;
            this.textBox23.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox23.Location = new System.Drawing.Point(188, 68);
            this.textBox23.Multiline = true;
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(106, 18);
            this.textBox23.TabIndex = 98;
            this.textBox23.Text = "0.00";
            this.textBox23.TextChanged += new System.EventHandler(this.textBox23_TextChanged);
            this.textBox23.Leave += new System.EventHandler(this.ComputeAmount);
            // 
            // textBox24
            // 
            this.textBox24.BackColor = System.Drawing.SystemColors.Window;
            this.textBox24.Enabled = false;
            this.textBox24.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox24.Location = new System.Drawing.Point(188, 189);
            this.textBox24.Multiline = true;
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(106, 18);
            this.textBox24.TabIndex = 32;
            this.textBox24.Text = "0.00";
            this.textBox24.TextChanged += new System.EventHandler(this.textBox24_TextChanged);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Calibri", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(172, 73);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(13, 13);
            this.label49.TabIndex = 99;
            this.label49.Text = "$";
            // 
            // pictureBox27
            // 
            this.pictureBox27.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox27.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox27.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox27.Location = new System.Drawing.Point(1562, 163);
            this.pictureBox27.Name = "pictureBox27";
            this.pictureBox27.Size = new System.Drawing.Size(19, 20);
            this.pictureBox27.TabIndex = 169;
            this.pictureBox27.TabStop = false;
            this.pictureBox27.Visible = false;
            this.pictureBox27.Click += new System.EventHandler(this.pictureBox27_Click);
            // 
            // pictureBox28
            // 
            this.pictureBox28.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox28.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox28.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox28.Location = new System.Drawing.Point(1537, 163);
            this.pictureBox28.Name = "pictureBox28";
            this.pictureBox28.Size = new System.Drawing.Size(19, 20);
            this.pictureBox28.TabIndex = 168;
            this.pictureBox28.TabStop = false;
            this.pictureBox28.Click += new System.EventHandler(this.pictureBox28_Click);
            // 
            // pictureBox29
            // 
            this.pictureBox29.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox29.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox29.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox29.Location = new System.Drawing.Point(1537, 163);
            this.pictureBox29.Name = "pictureBox29";
            this.pictureBox29.Size = new System.Drawing.Size(19, 20);
            this.pictureBox29.TabIndex = 167;
            this.pictureBox29.TabStop = false;
            // 
            // pictureBox35
            // 
            this.pictureBox35.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox35.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox35.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox35.Location = new System.Drawing.Point(1512, 163);
            this.pictureBox35.Name = "pictureBox35";
            this.pictureBox35.Size = new System.Drawing.Size(19, 20);
            this.pictureBox35.TabIndex = 166;
            this.pictureBox35.TabStop = false;
            this.pictureBox35.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox36
            // 
            this.pictureBox36.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox36.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox36.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox36.Location = new System.Drawing.Point(1317, 120);
            this.pictureBox36.Name = "pictureBox36";
            this.pictureBox36.Size = new System.Drawing.Size(19, 20);
            this.pictureBox36.TabIndex = 174;
            this.pictureBox36.TabStop = false;
            this.pictureBox36.Visible = false;
            this.pictureBox36.Click += new System.EventHandler(this.pictureBox36_Click);
            // 
            // pictureBox37
            // 
            this.pictureBox37.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox37.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox37.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox37.Location = new System.Drawing.Point(1291, 120);
            this.pictureBox37.Name = "pictureBox37";
            this.pictureBox37.Size = new System.Drawing.Size(19, 20);
            this.pictureBox37.TabIndex = 173;
            this.pictureBox37.TabStop = false;
            this.pictureBox37.Click += new System.EventHandler(this.pictureBox37_Click);
            // 
            // pictureBox39
            // 
            this.pictureBox39.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox39.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox39.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox39.Location = new System.Drawing.Point(1266, 120);
            this.pictureBox39.Name = "pictureBox39";
            this.pictureBox39.Size = new System.Drawing.Size(19, 20);
            this.pictureBox39.TabIndex = 170;
            this.pictureBox39.TabStop = false;
            this.pictureBox39.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox13
            // 
            this.pictureBox13.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox13.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox13.Location = new System.Drawing.Point(662, 29);
            this.pictureBox13.Name = "pictureBox13";
            this.pictureBox13.Size = new System.Drawing.Size(19, 20);
            this.pictureBox13.TabIndex = 183;
            this.pictureBox13.TabStop = false;
            this.pictureBox13.Visible = false;
            this.pictureBox13.Click += new System.EventHandler(this.pictureBox13_Click);
            // 
            // pictureBox14
            // 
            this.pictureBox14.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox14.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox14.Location = new System.Drawing.Point(637, 29);
            this.pictureBox14.Name = "pictureBox14";
            this.pictureBox14.Size = new System.Drawing.Size(19, 20);
            this.pictureBox14.TabIndex = 182;
            this.pictureBox14.TabStop = false;
            this.pictureBox14.Click += new System.EventHandler(this.pictureBox14_Click);
            // 
            // pictureBox15
            // 
            this.pictureBox15.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox15.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox15.Location = new System.Drawing.Point(612, 29);
            this.pictureBox15.Name = "pictureBox15";
            this.pictureBox15.Size = new System.Drawing.Size(19, 20);
            this.pictureBox15.TabIndex = 181;
            this.pictureBox15.TabStop = false;
            this.pictureBox15.Click += new System.EventHandler(this.HideCheck);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Calibri", 20F);
            this.label3.Location = new System.Drawing.Point(1509, 4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(23, 33);
            this.label3.TabIndex = 184;
            this.label3.Text = "-";
            // 
            // pictureBox59
            // 
            this.pictureBox59.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox59.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox59.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox59.Location = new System.Drawing.Point(1506, 264);
            this.pictureBox59.Name = "pictureBox59";
            this.pictureBox59.Size = new System.Drawing.Size(19, 20);
            this.pictureBox59.TabIndex = 189;
            this.pictureBox59.TabStop = false;
            this.pictureBox59.Visible = false;
            this.pictureBox59.Click += new System.EventHandler(this.pictureBox59_Click);
            // 
            // pictureBox60
            // 
            this.pictureBox60.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox60.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox60.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox60.Location = new System.Drawing.Point(1481, 264);
            this.pictureBox60.Name = "pictureBox60";
            this.pictureBox60.Size = new System.Drawing.Size(19, 20);
            this.pictureBox60.TabIndex = 188;
            this.pictureBox60.TabStop = false;
            this.pictureBox60.Click += new System.EventHandler(this.pictureBox60_Click);
            // 
            // pictureBox61
            // 
            this.pictureBox61.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox61.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox61.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox61.Location = new System.Drawing.Point(1481, 264);
            this.pictureBox61.Name = "pictureBox61";
            this.pictureBox61.Size = new System.Drawing.Size(19, 20);
            this.pictureBox61.TabIndex = 187;
            this.pictureBox61.TabStop = false;
            // 
            // pictureBox63
            // 
            this.pictureBox63.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox63.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox63.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox63.Location = new System.Drawing.Point(1456, 264);
            this.pictureBox63.Name = "pictureBox63";
            this.pictureBox63.Size = new System.Drawing.Size(19, 20);
            this.pictureBox63.TabIndex = 186;
            this.pictureBox63.TabStop = false;
            this.pictureBox63.Click += new System.EventHandler(this.HideCheck);
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.DodgerBlue;
            this.button2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.ForeColor = System.Drawing.Color.White;
            this.button2.Location = new System.Drawing.Point(1133, 806);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(96, 26);
            this.button2.TabIndex = 190;
            this.button2.Text = "Save";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(1350, 5);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(104, 14);
            this.label13.TabIndex = 191;
            this.label13.Text = "Pending for Audit:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(1455, 5);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(58, 14);
            this.label16.TabIndex = 192;
            this.label16.Text = "loading...";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(1401, 19);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(53, 14);
            this.label17.TabIndex = 193;
            this.label17.Text = "Audited:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(1455, 19);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(58, 14);
            this.label18.TabIndex = 194;
            this.label18.Text = "loading...";
            // 
            // grpBox_SpecialInstructions
            // 
            this.grpBox_SpecialInstructions.Controls.Add(this.textBox9);
            this.grpBox_SpecialInstructions.Location = new System.Drawing.Point(1214, 195);
            this.grpBox_SpecialInstructions.Name = "grpBox_SpecialInstructions";
            this.grpBox_SpecialInstructions.Size = new System.Drawing.Size(366, 61);
            this.grpBox_SpecialInstructions.TabIndex = 195;
            this.grpBox_SpecialInstructions.TabStop = false;
            this.grpBox_SpecialInstructions.Text = "Special Instruction";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(7, 14);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.textBox9.Size = new System.Drawing.Size(353, 41);
            this.textBox9.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(1350, 61);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(120, 14);
            this.label19.TabIndex = 135;
            this.label19.Text = "Transfer Account No.:";
            // 
            // txtTransferAccount
            // 
            this.txtTransferAccount.AutoSize = true;
            this.txtTransferAccount.Location = new System.Drawing.Point(1476, 63);
            this.txtTransferAccount.Name = "txtTransferAccount";
            this.txtTransferAccount.Size = new System.Drawing.Size(11, 14);
            this.txtTransferAccount.TabIndex = 135;
            this.txtTransferAccount.Text = "-";
            // 
            // btnAdd
            // 
            this.btnAdd.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnAdd.Enabled = false;
            this.btnAdd.ForeColor = System.Drawing.Color.White;
            this.btnAdd.Location = new System.Drawing.Point(1520, 55);
            this.btnAdd.Name = "btnAdd";
            this.btnAdd.Size = new System.Drawing.Size(60, 23);
            this.btnAdd.TabIndex = 199;
            this.btnAdd.Text = "Add";
            this.btnAdd.UseVisualStyleBackColor = false;
            this.btnAdd.Visible = false;
            this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
            // 
            // cmbBillName
            // 
            this.cmbBillName.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Suggest;
            this.cmbBillName.AutoCompleteSource = System.Windows.Forms.AutoCompleteSource.ListItems;
            this.cmbBillName.Enabled = false;
            this.cmbBillName.FormattingEnabled = true;
            this.cmbBillName.Location = new System.Drawing.Point(894, 89);
            this.cmbBillName.Name = "cmbBillName";
            this.cmbBillName.Size = new System.Drawing.Size(171, 22);
            this.cmbBillName.TabIndex = 197;
            this.cmbBillName.SelectedValueChanged += new System.EventHandler(this.cmbBillName_SelectedValueChanged);
            this.cmbBillName.TextChanged += new System.EventHandler(this.cmbBillName_TextChanged);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(783, 93);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(104, 14);
            this.label61.TabIndex = 196;
            this.label61.Text = "Bill addressed to:";
            // 
            // pictureBox25
            // 
            this.pictureBox25.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox25.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox25.Location = new System.Drawing.Point(1071, 90);
            this.pictureBox25.Name = "pictureBox25";
            this.pictureBox25.Size = new System.Drawing.Size(19, 20);
            this.pictureBox25.TabIndex = 170;
            this.pictureBox25.TabStop = false;
            this.pictureBox25.Click += new System.EventHandler(this.HideCheck);
            // 
            // pictureBox30
            // 
            this.pictureBox30.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox30.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox30.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox30.Location = new System.Drawing.Point(1096, 90);
            this.pictureBox30.Name = "pictureBox30";
            this.pictureBox30.Size = new System.Drawing.Size(19, 20);
            this.pictureBox30.TabIndex = 173;
            this.pictureBox30.TabStop = false;
            this.pictureBox30.Click += new System.EventHandler(this.pictureBox30_Click);
            // 
            // pictureBox62
            // 
            this.pictureBox62.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox62.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox62.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox62.Location = new System.Drawing.Point(1122, 90);
            this.pictureBox62.Name = "pictureBox62";
            this.pictureBox62.Size = new System.Drawing.Size(19, 20);
            this.pictureBox62.TabIndex = 174;
            this.pictureBox62.TabStop = false;
            this.pictureBox62.Visible = false;
            this.pictureBox62.Click += new System.EventHandler(this.pictureBox62_Click);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(1147, 92);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(269, 14);
            this.label20.TabIndex = 196;
            this.label20.Text = "Billed to PO Box 105265, Atlanta, GA, 30348-5265?";
            // 
            // chkNYes
            // 
            this.chkNYes.AutoSize = true;
            this.chkNYes.Enabled = false;
            this.chkNYes.Location = new System.Drawing.Point(1422, 90);
            this.chkNYes.Name = "chkNYes";
            this.chkNYes.Size = new System.Drawing.Size(44, 18);
            this.chkNYes.TabIndex = 197;
            this.chkNYes.Text = "Yes";
            this.chkNYes.UseVisualStyleBackColor = true;
            this.chkNYes.CheckedChanged += new System.EventHandler(this.chkNYes_CheckedChanged);
            // 
            // chknNo
            // 
            this.chknNo.AutoSize = true;
            this.chknNo.Enabled = false;
            this.chknNo.Location = new System.Drawing.Point(1466, 90);
            this.chknNo.Name = "chknNo";
            this.chknNo.Size = new System.Drawing.Size(41, 18);
            this.chknNo.TabIndex = 197;
            this.chknNo.Text = "No";
            this.chknNo.UseVisualStyleBackColor = true;
            this.chknNo.CheckedChanged += new System.EventHandler(this.chknNo_CheckedChanged);
            // 
            // pictureBox67
            // 
            this.pictureBox67.BackgroundImage = global::UC.Properties.Resources.thumbsup;
            this.pictureBox67.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox67.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox67.Location = new System.Drawing.Point(1563, 89);
            this.pictureBox67.Name = "pictureBox67";
            this.pictureBox67.Size = new System.Drawing.Size(19, 20);
            this.pictureBox67.TabIndex = 204;
            this.pictureBox67.TabStop = false;
            this.pictureBox67.Visible = false;
            this.pictureBox67.Click += new System.EventHandler(this.pictureBox67_Click);
            // 
            // pictureBox68
            // 
            this.pictureBox68.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox68.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox68.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox68.Location = new System.Drawing.Point(1538, 89);
            this.pictureBox68.Name = "pictureBox68";
            this.pictureBox68.Size = new System.Drawing.Size(19, 20);
            this.pictureBox68.TabIndex = 203;
            this.pictureBox68.TabStop = false;
            this.pictureBox68.Click += new System.EventHandler(this.pictureBox68_Click_1);
            // 
            // pictureBox69
            // 
            this.pictureBox69.BackgroundImage = global::UC.Properties.Resources.cross;
            this.pictureBox69.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox69.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox69.Location = new System.Drawing.Point(1538, 89);
            this.pictureBox69.Name = "pictureBox69";
            this.pictureBox69.Size = new System.Drawing.Size(19, 20);
            this.pictureBox69.TabIndex = 202;
            this.pictureBox69.TabStop = false;
            // 
            // pictureBox70
            // 
            this.pictureBox70.BackgroundImage = global::UC.Properties.Resources.check;
            this.pictureBox70.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox70.Cursor = System.Windows.Forms.Cursors.Hand;
            this.pictureBox70.Location = new System.Drawing.Point(1513, 89);
            this.pictureBox70.Name = "pictureBox70";
            this.pictureBox70.Size = new System.Drawing.Size(19, 20);
            this.pictureBox70.TabIndex = 201;
            this.pictureBox70.TabStop = false;
            this.pictureBox70.Click += new System.EventHandler(this.HideCheck);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(224, 34);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(29, 14);
            this.label21.TabIndex = 205;
            this.label21.Text = "City:";
            // 
            // txtUSPCity
            // 
            this.txtUSPCity.Location = new System.Drawing.Point(262, 31);
            this.txtUSPCity.Name = "txtUSPCity";
            this.txtUSPCity.Size = new System.Drawing.Size(124, 22);
            this.txtUSPCity.TabIndex = 206;
            // 
            // txtUSPState
            // 
            this.txtUSPState.Location = new System.Drawing.Point(442, 31);
            this.txtUSPState.Name = "txtUSPState";
            this.txtUSPState.Size = new System.Drawing.Size(30, 22);
            this.txtUSPState.TabIndex = 206;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(398, 37);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(38, 14);
            this.label22.TabIndex = 205;
            this.label22.Text = "State:";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(485, 37);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(53, 14);
            this.label23.TabIndex = 205;
            this.label23.Text = "Zipcode:";
            // 
            // txtUSPZip
            // 
            this.txtUSPZip.Location = new System.Drawing.Point(540, 31);
            this.txtUSPZip.Name = "txtUSPZip";
            this.txtUSPZip.Size = new System.Drawing.Size(43, 22);
            this.txtUSPZip.TabIndex = 206;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.comboBox5);
            this.panel1.Controls.Add(this.txtUSPZip);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.txtUSPState);
            this.panel1.Controls.Add(this.checkBox8);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.pictureBox15);
            this.panel1.Controls.Add(this.txtUSPStrt);
            this.panel1.Controls.Add(this.txtUSPCity);
            this.panel1.Controls.Add(this.pictureBox14);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.pictureBox13);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Location = new System.Drawing.Point(773, 383);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(752, 57);
            this.panel1.TabIndex = 207;
            // 
            // txtUSPStrt
            // 
            this.txtUSPStrt.Location = new System.Drawing.Point(52, 30);
            this.txtUSPStrt.Name = "txtUSPStrt";
            this.txtUSPStrt.Size = new System.Drawing.Size(161, 22);
            this.txtUSPStrt.TabIndex = 206;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(8, 33);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(42, 14);
            this.label24.TabIndex = 205;
            this.label24.Text = "Street:";
            // 
            // btnSearchUSP
            // 
            this.btnSearchUSP.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnSearchUSP.ForeColor = System.Drawing.Color.White;
            this.btnSearchUSP.Location = new System.Drawing.Point(1458, 447);
            this.btnSearchUSP.Name = "btnSearchUSP";
            this.btnSearchUSP.Size = new System.Drawing.Size(107, 35);
            this.btnSearchUSP.TabIndex = 147;
            this.btnSearchUSP.Text = "Search USP";
            this.btnSearchUSP.UseVisualStyleBackColor = false;
            this.btnSearchUSP.Visible = false;
            this.btnSearchUSP.Click += new System.EventHandler(this.btnSearchUSP_Click);
            // 
            // Audit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1592, 839);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pictureBox67);
            this.Controls.Add(this.chknNo);
            this.Controls.Add(this.btnAdd);
            this.Controls.Add(this.pictureBox68);
            this.Controls.Add(this.cmbBillName);
            this.Controls.Add(this.pictureBox69);
            this.Controls.Add(this.label61);
            this.Controls.Add(this.chkNYes);
            this.Controls.Add(this.grpBox_SpecialInstructions);
            this.Controls.Add(this.pictureBox70);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.pictureBox59);
            this.Controls.Add(this.pictureBox60);
            this.Controls.Add(this.pictureBox61);
            this.Controls.Add(this.pictureBox63);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox62);
            this.Controls.Add(this.pictureBox36);
            this.Controls.Add(this.pictureBox30);
            this.Controls.Add(this.pictureBox25);
            this.Controls.Add(this.pictureBox37);
            this.Controls.Add(this.pictureBox39);
            this.Controls.Add(this.label41);
            this.Controls.Add(this.pictureBox27);
            this.Controls.Add(this.pictureBox28);
            this.Controls.Add(this.pictureBox29);
            this.Controls.Add(this.pictureBox35);
            this.Controls.Add(this.btnSearchUSP);
            this.Controls.Add(this.textBox18);
            this.Controls.Add(this.button_noMatchUSP);
            this.Controls.Add(this.dgv_usp);
            this.Controls.Add(this.groupBox10);
            this.Controls.Add(this.groupBox9);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.cmboBx_UspOnInvoice);
            this.Controls.Add(this.txtTransferAccount);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.lbl_UspOnInvoice);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.chkBx_NoPropertyAddress);
            this.Controls.Add(this.checkBox6);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.webBrowser1);
            this.Font = new System.Drawing.Font("Calibri", 9F);
            this.Name = "Audit";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Audit Form";
            this.Load += new System.EventHandler(this.Audit_Load);
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox26)).EndInit();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_usp)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox22)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox23)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox24)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_isbillreadableapprove)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_isbillreadablecross)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pct_isbillreadablecheck)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox64)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox66)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox44)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox56)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox65)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox45)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox57)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox46)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox58)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox53)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox54)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox55)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox50)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox51)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox52)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox47)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox48)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox49)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox9)).EndInit();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox76)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox73)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox34)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox75)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox72)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox38)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox74)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox71)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox40)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox41)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox42)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox43)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox31)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox32)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox33)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox19)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox20)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox21)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox16)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox17)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox18)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox12)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox27)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox28)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox29)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox35)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox36)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox37)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox39)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox15)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox59)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox60)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox61)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox63)).EndInit();
            this.grpBox_SpecialInstructions.ResumeLayout(false);
            this.grpBox_SpecialInstructions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox25)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox30)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox62)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox67)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox68)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox69)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox70)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.CheckBox chkbx_noZipCode;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.CheckBox chkBx_NoPropertyAddress;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.ComboBox cmboBx_UspOnInvoice;
        private System.Windows.Forms.Label lbl_UspOnInvoice;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPropertyCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn cFullAddress;
        private System.Windows.Forms.DataGridViewTextBoxColumn cCustomerName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cActive;
        private System.Windows.Forms.DataGridViewTextBoxColumn cInvestorName;
        private System.Windows.Forms.DataGridViewTextBoxColumn cReosrcDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cReoslcDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cReosfcDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn cAccountNumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn cPodId;
        private System.Windows.Forms.DataGridViewTextBoxColumn cproperty_status_change_date;
        private System.Windows.Forms.DataGridViewTextBoxColumn cinactivedate;
        private System.Windows.Forms.Button button_noMatchUSP;
        private System.Windows.Forms.DataGridView dgv_usp;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox22;
        private System.Windows.Forms.PictureBox pictureBox23;
        private System.Windows.Forms.PictureBox pictureBox24;
        private System.Windows.Forms.PictureBox pct_isbillreadableapprove;
        private System.Windows.Forms.PictureBox pct_isbillreadablecross;
        private System.Windows.Forms.PictureBox pct_isbillreadablecheck;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.CheckBox checkBox27;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DateTimePicker dateTimePicker6;
        private System.Windows.Forms.CheckBox checkBox28;
        private System.Windows.Forms.CheckBox checkBox29;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.CheckBox checkBox30;
        private System.Windows.Forms.CheckBox checkBox31;
        private System.Windows.Forms.CheckBox checkBox32;
        private System.Windows.Forms.DateTimePicker dateTimePicker7;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.DateTimePicker dateTimePicker8;
        private System.Windows.Forms.DateTimePicker dateTimePicker9;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.DateTimePicker dateTimePicker10;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.CheckBox checkBox33;
        private System.Windows.Forms.CheckBox checkBox34;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.CheckBox checkBox35;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.PictureBox pictureBox27;
        private System.Windows.Forms.PictureBox pictureBox28;
        private System.Windows.Forms.PictureBox pictureBox29;
        private System.Windows.Forms.PictureBox pictureBox35;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.PictureBox pictureBox26;
        private System.Windows.Forms.PictureBox pictureBox36;
        private System.Windows.Forms.PictureBox pictureBox37;
        private System.Windows.Forms.PictureBox pictureBox39;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox9;
        private System.Windows.Forms.PictureBox pictureBox10;
        private System.Windows.Forms.PictureBox pictureBox11;
        private System.Windows.Forms.PictureBox pictureBox12;
        private System.Windows.Forms.PictureBox pictureBox13;
        private System.Windows.Forms.PictureBox pictureBox14;
        private System.Windows.Forms.PictureBox pictureBox15;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox31;
        private System.Windows.Forms.PictureBox pictureBox32;
        private System.Windows.Forms.PictureBox pictureBox33;
        private System.Windows.Forms.PictureBox pictureBox19;
        private System.Windows.Forms.PictureBox pictureBox20;
        private System.Windows.Forms.PictureBox pictureBox21;
        private System.Windows.Forms.PictureBox pictureBox16;
        private System.Windows.Forms.PictureBox pictureBox17;
        private System.Windows.Forms.PictureBox pictureBox18;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.PictureBox pictureBox8;
        private System.Windows.Forms.PictureBox pictureBox41;
        private System.Windows.Forms.PictureBox pictureBox42;
        private System.Windows.Forms.PictureBox pictureBox43;
        private System.Windows.Forms.PictureBox pictureBox44;
        private System.Windows.Forms.PictureBox pictureBox45;
        private System.Windows.Forms.PictureBox pictureBox46;
        private System.Windows.Forms.PictureBox pictureBox56;
        private System.Windows.Forms.PictureBox pictureBox57;
        private System.Windows.Forms.PictureBox pictureBox58;
        private System.Windows.Forms.PictureBox pictureBox53;
        private System.Windows.Forms.PictureBox pictureBox54;
        private System.Windows.Forms.PictureBox pictureBox55;
        private System.Windows.Forms.PictureBox pictureBox50;
        private System.Windows.Forms.PictureBox pictureBox51;
        private System.Windows.Forms.PictureBox pictureBox52;
        private System.Windows.Forms.PictureBox pictureBox47;
        private System.Windows.Forms.PictureBox pictureBox48;
        private System.Windows.Forms.PictureBox pictureBox49;
        private System.Windows.Forms.PictureBox pictureBox59;
        private System.Windows.Forms.PictureBox pictureBox60;
        private System.Windows.Forms.PictureBox pictureBox61;
        private System.Windows.Forms.PictureBox pictureBox63;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.PictureBox pictureBox34;
        private System.Windows.Forms.PictureBox pictureBox38;
        private System.Windows.Forms.PictureBox pictureBox40;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.GroupBox grpBox_SpecialInstructions;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.CheckBox chkTransAmtNo;
        private System.Windows.Forms.PictureBox pictureBox66;
        private System.Windows.Forms.CheckBox chkTransAmtYes;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.PictureBox pictureBox65;
        private System.Windows.Forms.PictureBox pictureBox64;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label txtTransferAccount;
        private System.Windows.Forms.Button btnAdd;
        private System.Windows.Forms.ComboBox cmbBillName;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.PictureBox pictureBox25;
        private System.Windows.Forms.PictureBox pictureBox30;
        private System.Windows.Forms.PictureBox pictureBox62;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.CheckBox chknNo;
        private System.Windows.Forms.CheckBox chkNYes;
        private System.Windows.Forms.PictureBox pictureBox67;
        private System.Windows.Forms.PictureBox pictureBox68;
        private System.Windows.Forms.PictureBox pictureBox69;
        private System.Windows.Forms.PictureBox pictureBox70;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox txtUSPCity;
        private System.Windows.Forms.TextBox txtUSPState;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox txtUSPZip;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnSearchUSP;
        private System.Windows.Forms.TextBox txtUSPStrt;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.DataGridViewTextBoxColumn vdr_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn vdr_name;
        private System.Windows.Forms.DataGridViewTextBoxColumn vdr_address;
        private System.Windows.Forms.DataGridViewTextBoxColumn vdr_status;
        private System.Windows.Forms.DataGridViewTextBoxColumn vms_status;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.CheckBox checkBox20;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.PictureBox pictureBox76;
        private System.Windows.Forms.PictureBox pictureBox73;
        private System.Windows.Forms.PictureBox pictureBox75;
        private System.Windows.Forms.PictureBox pictureBox72;
        private System.Windows.Forms.PictureBox pictureBox74;
        private System.Windows.Forms.PictureBox pictureBox71;
        private System.Windows.Forms.TextBox txtDepRef;
        private System.Windows.Forms.TextBox txtDepPy;
    }
}