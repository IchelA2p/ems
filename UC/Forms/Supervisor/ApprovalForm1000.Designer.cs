﻿namespace UC.Forms.Supervisor
{
    partial class ApprovalForm1000
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.cmbType = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btn_Save = new System.Windows.Forms.Button();
            this.dgv_list_forapproval = new System.Windows.Forms.DataGridView();
            this.forapproval_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forapproval_clientcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forapproval_propertycode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PropStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AgefromREO = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.InactiveDate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.USP = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forapproval_vendorid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forapproval_amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forapproval_currentcharge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forapproval_previousbalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forapproval_latefee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forapproval_aging = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.FinalBill = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forapproval_link = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forapproval_userid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dateprocessed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ApprovedBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Filename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Comment = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.forapproval_approval = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripTextBox1 = new System.Windows.Forms.ToolStripTextBox();
            this.txtPropCode = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbUSP = new System.Windows.Forms.ComboBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnFilter = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_list_forapproval)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.contextMenuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.AutoSize = true;
            this.groupBox1.Controls.Add(this.webBrowser1);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.groupBox1.Size = new System.Drawing.Size(580, 754);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Invoice";
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(3, 20);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 19);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(574, 730);
            this.webBrowser1.TabIndex = 0;
            this.webBrowser1.DocumentCompleted += new System.Windows.Forms.WebBrowserDocumentCompletedEventHandler(this.webBrowser1_DocumentCompleted);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.btnFilter);
            this.groupBox2.Controls.Add(this.cmbUSP);
            this.groupBox2.Controls.Add(this.txtPropCode);
            this.groupBox2.Controls.Add(this.cmbType);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.btn_Save);
            this.groupBox2.Controls.Add(this.dgv_list_forapproval);
            this.groupBox2.Location = new System.Drawing.Point(598, 98);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(894, 668);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            // 
            // cmbType
            // 
            this.cmbType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbType.FormattingEnabled = true;
            this.cmbType.Items.AddRange(new object[] {
            "High Amount",
            "Potential Trailing OMS > 20 days",
            "Trailing OMS > 45 days",
            "Potential Trailing > 45 days",
            "Potential Trailing > 90 days",
            "Trailing Asset",
            "Inactive Property",
            "Occupied"});
            this.cmbType.Location = new System.Drawing.Point(99, 24);
            this.cmbType.Name = "cmbType";
            this.cmbType.Size = new System.Drawing.Size(244, 23);
            this.cmbType.TabIndex = 2;
            this.cmbType.SelectedIndexChanged += new System.EventHandler(this.cmbType_SelectedIndexChanged);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(10, 29);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(34, 15);
            this.label9.TabIndex = 1;
            this.label9.Text = "Type:";
            // 
            // btn_Save
            // 
            this.btn_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_Save.BackColor = System.Drawing.Color.DodgerBlue;
            this.btn_Save.ForeColor = System.Drawing.Color.White;
            this.btn_Save.Location = new System.Drawing.Point(801, 632);
            this.btn_Save.Name = "btn_Save";
            this.btn_Save.Size = new System.Drawing.Size(75, 25);
            this.btn_Save.TabIndex = 1;
            this.btn_Save.Text = "Save";
            this.btn_Save.UseVisualStyleBackColor = false;
            this.btn_Save.Click += new System.EventHandler(this.btn_Save_Click);
            // 
            // dgv_list_forapproval
            // 
            this.dgv_list_forapproval.AllowUserToAddRows = false;
            this.dgv_list_forapproval.AllowUserToDeleteRows = false;
            this.dgv_list_forapproval.AllowUserToResizeRows = false;
            this.dgv_list_forapproval.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgv_list_forapproval.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader;
            this.dgv_list_forapproval.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgv_list_forapproval.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_list_forapproval.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgv_list_forapproval.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_list_forapproval.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.forapproval_id,
            this.forapproval_clientcode,
            this.forapproval_propertycode,
            this.PropStatus,
            this.AgefromREO,
            this.InactiveDate,
            this.USP,
            this.forapproval_vendorid,
            this.forapproval_amount,
            this.forapproval_currentcharge,
            this.forapproval_previousbalance,
            this.forapproval_latefee,
            this.forapproval_aging,
            this.FinalBill,
            this.forapproval_link,
            this.forapproval_userid,
            this.dateprocessed,
            this.ApprovedBy,
            this.Filename,
            this.Comment,
            this.forapproval_approval});
            this.dgv_list_forapproval.Location = new System.Drawing.Point(3, 94);
            this.dgv_list_forapproval.Name = "dgv_list_forapproval";
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.White;
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_list_forapproval.RowHeadersDefaultCellStyle = dataGridViewCellStyle12;
            this.dgv_list_forapproval.RowHeadersVisible = false;
            this.dgv_list_forapproval.Size = new System.Drawing.Size(885, 531);
            this.dgv_list_forapproval.TabIndex = 0;
            this.dgv_list_forapproval.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_list_forapproval_CellClick);
            this.dgv_list_forapproval.CellEnter += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_list_forapproval_CellEnter);
            this.dgv_list_forapproval.CellMouseDown += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv_list_forapproval_CellMouseDown);
            // 
            // forapproval_id
            // 
            this.forapproval_id.HeaderText = "id";
            this.forapproval_id.Name = "forapproval_id";
            this.forapproval_id.Visible = false;
            this.forapproval_id.Width = 5;
            // 
            // forapproval_clientcode
            // 
            this.forapproval_clientcode.FillWeight = 82.007F;
            this.forapproval_clientcode.HeaderText = "Client";
            this.forapproval_clientcode.MinimumWidth = 50;
            this.forapproval_clientcode.Name = "forapproval_clientcode";
            this.forapproval_clientcode.Width = 50;
            // 
            // forapproval_propertycode
            // 
            this.forapproval_propertycode.FillWeight = 118.2896F;
            this.forapproval_propertycode.HeaderText = "Property ID";
            this.forapproval_propertycode.MinimumWidth = 100;
            this.forapproval_propertycode.Name = "forapproval_propertycode";
            // 
            // PropStatus
            // 
            this.PropStatus.HeaderText = "Property Status";
            this.PropStatus.MinimumWidth = 70;
            this.PropStatus.Name = "PropStatus";
            this.PropStatus.Width = 70;
            // 
            // AgefromREO
            // 
            this.AgefromREO.HeaderText = "REOS/Inactive Aging";
            this.AgefromREO.MinimumWidth = 85;
            this.AgefromREO.Name = "AgefromREO";
            this.AgefromREO.Width = 85;
            // 
            // InactiveDate
            // 
            this.InactiveDate.HeaderText = "Inactive Date";
            this.InactiveDate.MinimumWidth = 60;
            this.InactiveDate.Name = "InactiveDate";
            this.InactiveDate.Width = 60;
            // 
            // USP
            // 
            this.USP.HeaderText = "USP";
            this.USP.MinimumWidth = 50;
            this.USP.Name = "USP";
            this.USP.Width = 50;
            // 
            // forapproval_vendorid
            // 
            this.forapproval_vendorid.HeaderText = "Vendor ID";
            this.forapproval_vendorid.Name = "forapproval_vendorid";
            this.forapproval_vendorid.Visible = false;
            this.forapproval_vendorid.Width = 5;
            // 
            // forapproval_amount
            // 
            this.forapproval_amount.FillWeight = 82.007F;
            this.forapproval_amount.HeaderText = "Amount";
            this.forapproval_amount.MinimumWidth = 60;
            this.forapproval_amount.Name = "forapproval_amount";
            this.forapproval_amount.Width = 60;
            // 
            // forapproval_currentcharge
            // 
            this.forapproval_currentcharge.FillWeight = 82.007F;
            this.forapproval_currentcharge.HeaderText = "Current Charge";
            this.forapproval_currentcharge.MinimumWidth = 60;
            this.forapproval_currentcharge.Name = "forapproval_currentcharge";
            this.forapproval_currentcharge.Width = 60;
            // 
            // forapproval_previousbalance
            // 
            this.forapproval_previousbalance.FillWeight = 82.007F;
            this.forapproval_previousbalance.HeaderText = "Previous Balance";
            this.forapproval_previousbalance.MinimumWidth = 60;
            this.forapproval_previousbalance.Name = "forapproval_previousbalance";
            this.forapproval_previousbalance.Width = 60;
            // 
            // forapproval_latefee
            // 
            this.forapproval_latefee.FillWeight = 82.007F;
            this.forapproval_latefee.HeaderText = "Late Fee";
            this.forapproval_latefee.MinimumWidth = 60;
            this.forapproval_latefee.Name = "forapproval_latefee";
            this.forapproval_latefee.Width = 60;
            // 
            // forapproval_aging
            // 
            this.forapproval_aging.FillWeight = 82.007F;
            this.forapproval_aging.HeaderText = "Aging (days)";
            this.forapproval_aging.MinimumWidth = 60;
            this.forapproval_aging.Name = "forapproval_aging";
            this.forapproval_aging.Width = 60;
            // 
            // FinalBill
            // 
            this.FinalBill.HeaderText = "Final Bill";
            this.FinalBill.MinimumWidth = 60;
            this.FinalBill.Name = "FinalBill";
            this.FinalBill.Width = 60;
            // 
            // forapproval_link
            // 
            this.forapproval_link.HeaderText = "Link";
            this.forapproval_link.Name = "forapproval_link";
            this.forapproval_link.Visible = false;
            this.forapproval_link.Width = 5;
            // 
            // forapproval_userid
            // 
            this.forapproval_userid.HeaderText = "User ID";
            this.forapproval_userid.MinimumWidth = 60;
            this.forapproval_userid.Name = "forapproval_userid";
            this.forapproval_userid.Visible = false;
            this.forapproval_userid.Width = 60;
            // 
            // dateprocessed
            // 
            this.dateprocessed.HeaderText = "Date Processed";
            this.dateprocessed.MinimumWidth = 75;
            this.dateprocessed.Name = "dateprocessed";
            this.dateprocessed.Width = 75;
            // 
            // ApprovedBy
            // 
            this.ApprovedBy.FillWeight = 96.41892F;
            this.ApprovedBy.HeaderText = "Approved By";
            this.ApprovedBy.Name = "ApprovedBy";
            this.ApprovedBy.Visible = false;
            this.ApprovedBy.Width = 5;
            // 
            // Filename
            // 
            dataGridViewCellStyle10.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.Filename.DefaultCellStyle = dataGridViewCellStyle10;
            this.Filename.FillWeight = 101.3176F;
            this.Filename.HeaderText = "Attachment";
            this.Filename.MinimumWidth = 100;
            this.Filename.Name = "Filename";
            this.Filename.ReadOnly = true;
            // 
            // Comment
            // 
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.Comment.DefaultCellStyle = dataGridViewCellStyle11;
            this.Comment.FillWeight = 220.2463F;
            this.Comment.HeaderText = "Comment";
            this.Comment.MinimumWidth = 200;
            this.Comment.Name = "Comment";
            this.Comment.Width = 200;
            // 
            // forapproval_approval
            // 
            this.forapproval_approval.FillWeight = 82.007F;
            this.forapproval_approval.HeaderText = "Approval";
            this.forapproval_approval.Items.AddRange(new object[] {
            "Approve",
            "Decline",
            "Approval Requested",
            "Select..."});
            this.forapproval_approval.MinimumWidth = 75;
            this.forapproval_approval.Name = "forapproval_approval";
            this.forapproval_approval.Width = 75;
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Location = new System.Drawing.Point(598, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(894, 80);
            this.groupBox3.TabIndex = 2;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "MTD Report:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(179, 53);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(76, 15);
            this.label7.TabIndex = 7;
            this.label7.Text = "processing...";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(59, 53);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(119, 15);
            this.label8.TabIndex = 6;
            this.label8.Text = "Approval Requested:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(485, 53);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(76, 15);
            this.label6.TabIndex = 5;
            this.label6.Text = "processing...";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(371, 53);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(108, 15);
            this.label5.TabIndex = 4;
            this.label5.Text = "Disapproved MTD:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(485, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(76, 15);
            this.label4.TabIndex = 3;
            this.label4.Text = "processing...";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(371, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 15);
            this.label3.TabIndex = 2;
            this.label3.Text = "Approved MTD:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(179, 22);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(76, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "processing...";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 22);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pending for Review:";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripTextBox1});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(161, 22);
            // 
            // toolStripTextBox1
            // 
            this.toolStripTextBox1.BackColor = System.Drawing.Color.White;
            this.toolStripTextBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.toolStripTextBox1.Name = "toolStripTextBox1";
            this.toolStripTextBox1.ReadOnly = true;
            this.toolStripTextBox1.Size = new System.Drawing.Size(100, 16);
            this.toolStripTextBox1.Text = "Remove";
            this.toolStripTextBox1.Click += new System.EventHandler(this.toolStripTextBox1_Click);
            // 
            // txtPropCode
            // 
            this.txtPropCode.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.txtPropCode.Location = new System.Drawing.Point(99, 55);
            this.txtPropCode.Name = "txtPropCode";
            this.txtPropCode.Size = new System.Drawing.Size(244, 23);
            this.txtPropCode.TabIndex = 3;
            this.txtPropCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.triggerSearchButton);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(10, 59);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(86, 15);
            this.label10.TabIndex = 1;
            this.label10.Text = "Property code:";
            // 
            // cmbUSP
            // 
            this.cmbUSP.FormattingEnabled = true;
            this.cmbUSP.Location = new System.Drawing.Point(461, 54);
            this.cmbUSP.Name = "cmbUSP";
            this.cmbUSP.Size = new System.Drawing.Size(255, 23);
            this.cmbUSP.TabIndex = 4;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(417, 57);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(31, 15);
            this.label11.TabIndex = 1;
            this.label11.Text = "USP:";
            // 
            // btnFilter
            // 
            this.btnFilter.Location = new System.Drawing.Point(768, 52);
            this.btnFilter.Name = "btnFilter";
            this.btnFilter.Size = new System.Drawing.Size(69, 30);
            this.btnFilter.TabIndex = 5;
            this.btnFilter.Text = "Load";
            this.btnFilter.UseVisualStyleBackColor = true;
            this.btnFilter.Click += new System.EventHandler(this.btnFilter_Click);
            // 
            // ApprovalForm1000
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1504, 778);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Font = new System.Drawing.Font("Calibri", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.Name = "ApprovalForm1000";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Invoice Approval Form";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ApprovalForm1000_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_list_forapproval)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.contextMenuStrip1.ResumeLayout(false);
            this.contextMenuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_list_forapproval;
        private System.Windows.Forms.Button btn_Save;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripTextBox toolStripTextBox1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox cmbType;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn forapproval_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn forapproval_clientcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn forapproval_propertycode;
        private System.Windows.Forms.DataGridViewTextBoxColumn PropStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn AgefromREO;
        private System.Windows.Forms.DataGridViewTextBoxColumn InactiveDate;
        private System.Windows.Forms.DataGridViewTextBoxColumn USP;
        private System.Windows.Forms.DataGridViewTextBoxColumn forapproval_vendorid;
        private System.Windows.Forms.DataGridViewTextBoxColumn forapproval_amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn forapproval_currentcharge;
        private System.Windows.Forms.DataGridViewTextBoxColumn forapproval_previousbalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn forapproval_latefee;
        private System.Windows.Forms.DataGridViewTextBoxColumn forapproval_aging;
        private System.Windows.Forms.DataGridViewTextBoxColumn FinalBill;
        private System.Windows.Forms.DataGridViewTextBoxColumn forapproval_link;
        private System.Windows.Forms.DataGridViewTextBoxColumn forapproval_userid;
        private System.Windows.Forms.DataGridViewTextBoxColumn dateprocessed;
        private System.Windows.Forms.DataGridViewTextBoxColumn ApprovedBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn Filename;
        private System.Windows.Forms.DataGridViewTextBoxColumn Comment;
        private System.Windows.Forms.DataGridViewComboBoxColumn forapproval_approval;
        private System.Windows.Forms.Button btnFilter;
        private System.Windows.Forms.ComboBox cmbUSP;
        private System.Windows.Forms.TextBox txtPropCode;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
    }
}