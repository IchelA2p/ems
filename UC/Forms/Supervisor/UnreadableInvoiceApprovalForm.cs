﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms.Supervisor
{
    public partial class UnreadableInvoiceApprovalForm : Form
    {
        private List<Database.DB.EM_Tasks> dataList;
        private Database.DB.EM_Tasks selectedInfo;
        private string oldFilePath = string.Empty;
        private string newFilePath = string.Empty;

        private string SourceFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\";

        BackgroundWorker bwLoadList;

        public UnreadableInvoiceApprovalForm()
        {
            InitializeComponent();

            dataList = new List<Database.DB.EM_Tasks>();
            selectedInfo = new Database.DB.EM_Tasks();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Approved")
            { 
                label2.Text = "Invoice will be sent to Pat McTaggarts queue.";
            }
            else
            {
                label2.Text = "Invoice will be returned back to queue.";
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) 
            {
                return; 
            }

            string invoice = dataGridView1.Rows[e.RowIndex].Cells[0].Value.ToString();

            selectedInfo = new Database.DB.EM_Tasks();
            selectedInfo = dataList.First(p => p.Invoice == invoice);

            try
            {
                Uri file = new Uri(selectedInfo.InvoiceFolderName);

                webBrowser1.Navigate(file);

                groupBox1.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void UnreadableInvoiceApprovalForm_Load(object sender, EventArgs e)
        {
            bwLoadList = new BackgroundWorker();
            bwLoadList.DoWork += new DoWorkEventHandler(bwLoadList_DoWork);
            bwLoadList.RunWorkerAsync();
        }

        void bwLoadList_DoWork(object sender, DoWorkEventArgs e)
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            Database.DBConnection.DatabaseEmployee_Info emConn = new Database.DBConnection.DatabaseEmployee_Info();
            dataList = new List<Database.DB.EM_Tasks>();
            List<Database.DB.EM_Tasks> fullDataList = new List<Database.DB.EM_Tasks>();
            List<Database.DB.Employee_Info> emFull = new List<Database.DB.Employee_Info>();

            emFull = emConn.GetAllData();

            try
            {
                fullDataList.AddRange(conn.GetInvoicesFromPendingReason(@"Pending Blank/Unreadable/Cut Invoice", DateTime.Now.AddMonths(-1)));
            }
            catch
            { 
            
            }

            try
            {
                fullDataList.AddRange(conn.GetInvoicesFromPendingReason(@"Blank/Unreadable/Cut Invoice", DateTime.Now.AddMonths(-1)));
            }
            catch
            {

            }
            
            if (fullDataList != null)
            {
                foreach (var tempdata in fullDataList)
                {
                    Database.DB.EM_Tasks temp = new Database.DB.EM_Tasks();
                    temp = conn.GetLatestInvoiceData(tempdata.Invoice);

                    if (temp.PendingReasons == @"Pending Blank/Unreadable/Cut Invoice" ||
                        temp.PendingReasons == @"Blank/Unreadable/Cut Invoice")
                    {
                        if (dataList.Count(p => p.Invoice == temp.Invoice) < 1 && System.IO.File.Exists(temp.InvoiceFolderName))
                        {
                            dataList.Add(temp);
                        }
                    }
                }

                if (dataList != null)
                {
                    try
                    {
                        dataGridView1.Invoke((Action)delegate
                        {
                            dataGridView1.Rows.Clear();

                            foreach (var data in dataList)
                            {
                                string userid = data.userid;

                                if (userid == string.Empty)
                                {
                                    userid = emFull.First(p => p.EmployeePrimaryId == data.EmployeePrimaryId).userId;
                                }

                                dataGridView1.Rows.Add(data.Invoice, userid, (DateTime.Now - data.LastDateModified).Days);
                            }

                        });
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("An unexpected error occured. Error: " + ex.Message);
                    }
                }
                else
                {
                    dataGridView1.Invoke((Action)delegate
                    {
                        dataGridView1.Rows.Clear();

                        foreach (var data in dataList)
                        {
                            dataGridView1.Rows.Add(null, "No DATA", null);
                        }
                    });
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            string tempFolder = string.Empty;

            if (comboBox1.Text == "Approved")
            {
                selectedInfo.PendingReasons = "Approved Blank/Unreadable/Cut Invoice";
                selectedInfo.LastDateModified = DateTime.Now;
                selectedInfo.Complete = 1;
                tempFolder = selectedInfo.InvoiceFolderName;
            }
            else
            {
                selectedInfo.PendingReasons = "New Invoice";
                selectedInfo.LastDateModified = DateTime.Now;
                selectedInfo.Complete = 20;
                tempFolder = selectedInfo.InvoiceFolderName;
                selectedInfo.InvoiceFolderName = strCreateInvoiceFolderName(SourceFolder);
            }

            try
            {
                webBrowser1.Navigate("about:blank");

                while (webBrowser1.ReadyState != WebBrowserReadyState.Complete)
                {
                    Application.DoEvents();
                }

                if (tempFolder != selectedInfo.InvoiceFolderName)
                {
                    System.IO.File.Move(tempFolder, selectedInfo.InvoiceFolderName);
                }

                conn.WriteData(selectedInfo);

                MessageBox.Show("Invoice Submitted!");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public string strCreateInvoiceFolderName(string folder)
        {
            string strDate = DateTime.Now.ToString("MM-dd-yyyy");
            if (selectedInfo.InvoiceFolderName.Contains("pdf"))
            {
                return string.Format(@"{0}{1}\{2}.pdf", folder, strDate, selectedInfo.Invoice);
            }
            else
            {
                return string.Format(@"{0}{1}\{2}.html", folder, strDate, selectedInfo.Invoice);
            }
        }

    }
}
