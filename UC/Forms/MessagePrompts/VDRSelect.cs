﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms.MessagePrompts
{
    public partial class VDRSelect : Form
    {
        Form1 _frm1;

        public VDRSelect(Form1 frm1)
        {
            InitializeComponent();

            _frm1 = frm1;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _frm1.saveVDR = false;
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            _frm1.saveVDR =  true;
            this.Close();
        }
    }
}
