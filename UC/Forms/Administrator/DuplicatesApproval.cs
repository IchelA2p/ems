﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms.Administrator
{
    public partial class DuplicatesApproval : Form
    {
        List<Database.DB.EM_Tasks> duplicates;

        Database.DB.EM_Tasks selectedInfo;
        Database.DB.EM_Tasks duplicateInfo;

        public string CompleteFolder = @"\\ascorp.com\data\Bangalore\CommonShare\Strategic$\Utility Invoices\EM_TOOL_BulkUpload\";
        public string NoActionNeededFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\NOACTIONNEEDED\";
        public string tempFile = @"\\PIV8FSASNP01\CommonShare\EMProject\tempFile.pdf";

        BackgroundWorker bwDiplayForPending;
        BackgroundWorker bwDiplaySummary;

        DataTable info = new DataTable();
        clsConnection cls = new clsConnection();

        string queue;

        public DuplicatesApproval(string mode)
        {
            InitializeComponent();

            queue = mode;
            duplicates = new List<Database.DB.EM_Tasks>();

            selectedInfo = new Database.DB.EM_Tasks();
            duplicates = new List<Database.DB.EM_Tasks>();

            btnRefresh_Click(null, null);

            //bwDiplayForPending = new BackgroundWorker();
            //bwDiplayForPending.DoWork += new DoWorkEventHandler(bwDiplayForPending_DoWork);
            //bwDiplayForPending.RunWorkerAsync();

            bwDiplaySummary = new BackgroundWorker();
            bwDiplaySummary.DoWork += new DoWorkEventHandler(bwDiplaySummary_DoWork);
            bwDiplaySummary.RunWorkerAsync();

            //Timer t = new Timer();
            //t.Interval = (1000 * 60) * 10;
            //t.Tick += new EventHandler(t_Tick);
            //t.Start();
            //t.Stop();
        }

        void bwDiplaySummary_DoWork(object sender, DoWorkEventArgs e)
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();

            int numberOfApprovedDuplicates;
            int numberOfRejectedDuplicates;
            int numberOfPending;
            int oldestinhrs;

            //int numberOfPending = conn.GetCountOfPendingReason(
            //    1,
            //    "Pending",
            //    DateTime.Today.Date.AddDays(-DateTime.Today.Day),
            //    DateTime.Today.Date.AddDays(1),
            //    0);

            numberOfPending = conn.GetCountOfPendingReason(
                1,
                queue,
                DateTime.Today.Date.AddDays(-DateTime.Today.Day),
                DateTime.Today.Date.AddDays(1),
                0);

            if (queue == "Pending Duplicate")
            {
                numberOfApprovedDuplicates = conn.GetCountOfPendingReason(
                    1,
                    "Approved Duplicate Bulk Upload",
                    DateTime.Today.Date.AddDays(-DateTime.Today.Day),
                    DateTime.Today.Date.AddDays(1),
                    1);

                numberOfRejectedDuplicates = conn.GetCountOfPendingReason(
                    1,
                    "Duplicate",
                    DateTime.Today.Date.AddDays(-DateTime.Today.Day),
                    DateTime.Today.Date.AddDays(1),
                    3);

                oldestinhrs = conn.GetCountOfPendingReason(
                    1,
                    queue,
                    DateTime.Today.Date.AddDays(-DateTime.Today.Day),
                    DateTime.Today.Date.AddDays(1),
                    5);
            }
            else
            {
                numberOfApprovedDuplicates = conn.GetCountOfPendingReason(
                    1,
                    "Approved Duplicate Bulk Upload",
                    DateTime.Today.Date.AddDays(-DateTime.Today.Day),
                    DateTime.Today.Date.AddDays(1),
                    2);

                numberOfRejectedDuplicates = conn.GetCountOfPendingReason(
                    1,
                    "Duplicate",
                    DateTime.Today.Date.AddDays(-DateTime.Today.Day),
                    DateTime.Today.Date.AddDays(1),
                    4);

                oldestinhrs = conn.GetCountOfPendingReason(
                    1,
                    queue,
                    DateTime.Today.Date.AddDays(-DateTime.Today.Day),
                    DateTime.Today.Date.AddDays(1),
                    6);
            }

            label9.Invoke((Action)delegate 
            {
                label9.Text = numberOfPending.ToString(); //dataGridView1.RowCount.ToString();
            });

            label10.Invoke((Action)delegate 
            {
                label10.Text = numberOfApprovedDuplicates.ToString();
            });

            label11.Invoke((Action)delegate
            {
                label11.Text = numberOfRejectedDuplicates.ToString();
            });

            label13.Invoke((Action)delegate
            {
                label13.Text = oldestinhrs.ToString();
            });
        }

        void t_Tick(object sender, EventArgs e)
        {
            bwDiplayForPending_DoWork(null, null);
        }

        void bwDiplayForPending_DoWork(object sender, DoWorkEventArgs e)
        {
            
            DiplayForPending();
        }

        public void DiplayForPending()
        {
                       
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            List<Database.DB.EM_Tasks> temp = new List<Database.DB.EM_Tasks>();
            duplicates = new List<Database.DB.EM_Tasks>();

            temp = conn.GetDuplicates(1, queue);

            if (temp != null)
            {
                duplicates.AddRange(temp);
            }

            if (duplicates != null)
            {
                try
                {
                    dataGridView1.Invoke((Action)delegate
                    {
                        dataGridView1.Rows.Clear();

                        foreach (Database.DB.EM_Tasks info in duplicates)
                        {
                            dataGridView1.Rows.Add(
                                info.property_code,
                                info.full_address,
                                info.Age,
                                info.property_status,
                                info.vendor_group,
                                info.usp_name,
                                info.AccountNumber,
                                (info.ServiceFrom.ToString() == "1/1/0001 12:00:00 AM" ? "" : info.ServiceFrom.ToString("MM/dd/yyyy")),
                                (info.ServiceTo.ToString() == "1/1/0001 12:00:00 AM" ? "" : info.ServiceTo.ToString("MM/dd/yyyy")),
                                string.Format("{0:N2}", info.Amount),
                                info.Invoice,
                                null,
                                info.PendingReasons);
                        }
                    });


                }
                catch 
                { 
                }
            }

            if (queue == "Pre-work Item Review")
            {
                dataGridView1.Columns["Category"].Visible = true;
                label15.Visible = true;
                dgPrevWIs.Visible = true;
            }
            else
            {
                dataGridView1.Columns["Category"].Visible = false;

                dgPrevWIs.Rows.Clear();
                label15.Visible = false;
                dgPrevWIs.Visible = false;
            }
        }


        public void PopulatePossibleDuplicates(string propertyCode, string Invoice, string accountnum)
        {
            //            string qry_Duplicate = string.Format(@"select a.work_item as 'Work Item', a.work_item_status as 'Work Item Status', a.line_item as 'Line Item', a.account_num as 'Line Number', a.service_frm as 'Service From', a.service_to as 'Service To', a.issued_date as 'Service Date', a.vendor_price as 'Vendor Price', c.Vendor_Name as 'Vendor Name', b.InvoiceFolderName as 'Invoice Folder Name'
            //                                                    from tbl_EM_PayAppWorkItems a
            //                                                    left join tbl_ADHOC_EM_Tasks_UAT b
            //                                                    on a.work_item = b.work_item
            //                                                    left join tbl_EM_VDR c
            //                                                    on a.vendor_code = c.Vendor_ID
            //                                                    where a.property_code = '{0}' 
            //                                                    and line_item not like '%deposit%'
            //                                                    and work_item_status in ('Payment Approved', 'Closed')
            //                                                    and DATEDIFF(day,issued_date, GETDATE()) <= 30", propertyCode);


            string qry_Duplicate = string.Format(@"select distinct work_item from 
                                                (
                                                select distinct a.work_item, work_item_status, line_item, account_num, service_frm, service_to, issued_date, vendor_price, c.Vendor_Name, payment_date_processed, check_no, check_date, check_status, check_cashed_date, b.InvoiceFolderName
                                                from tbl_EM_PayAppWorkItems a
                                                left join tbl_ADHOC_EM_Tasks b
                                                on a.work_item = b.work_item
                                                left join tbl_EM_VDR c
                                                on a.vendor_code = c.Vendor_ID
                                                where line_item not like '%deposit%'
                                                and ((work_item_status in ('Payment Approved', 'Closed') and ecp_date is not null)
                                                or (work_item_status in ('Payment Approved', 'Closed') and (ecp_remarks is null or ecp_remarks like '%approve%')) and work_item_action = 1)
                                                and a.property_code = '{0}' and account_num = '{2}'
                                                union all
                                                select distinct work_item, WorkItemStatus, vendor_group, AccountNumber, ServiceFrom, ServiceTo, date_bulkuploaded, Amount, usp_name, NULL, NULL, NULL, NULL, NULL, InvoiceFolderName
                                                from tbl_ADHOC_EM_Tasks
                                                where work_item <> '' 
                                                and property_code = '{0}'
                                                and AccountNumber = '{2}'
                                                and Invoice <> '{1}'
                                                and WorkItemStatus not in ('Payment Approved', 'Closed', 'Cancelled')
                                                ) as tbl_dup
                                                where DATEDIFF(day,issued_date, GETDATE()) <= 60", propertyCode, Invoice, accountnum);

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(qry_Duplicate, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);
            conn.Close();

            DataGridViewComboBoxCell cbcell = (DataGridViewComboBoxCell)dataGridView1.CurrentRow.Cells["c_Comments"];

            cbcell.Items.Clear();
            cbcell.Items.Add("");

            foreach (DataRow row in dt.Rows)
            {
                cbcell.Items.Add(row[0]);
            }            
        }

        public void loadPossibleDuplicates(string propertyCode, string Invoice, string accountnum, string utility)
        {
            
            string qry_Duplicate = string.Format(@"
                                                select distinct * from 
                                                (
                                                select b.work_item [Work Order #], b.work_item_status [work_item_status], b.line_item, b.account_num, b.service_frm, b.service_to, c.LastDateModified [dup_queue_date], b.ecp_date, cast(b.vendor_price as varchar(25)) vendor_price, d.Vendor_Name, b.payment_date_processed, b.check_no, b.check_date, b.check_status, b.check_cashed_date, ISNULL(c.InvoiceFolderName, b.invoicelink) InvoiceFolderName, b.issued_date
                                                from tbl_EM_PayAppWorkItems b
                                                outer apply
                                                (select top 1 InvoiceFolderName, LastDateModified from tbl_ADHOC_EM_Tasks x where x.work_item = b.work_item) c
                                                left join vw_VID d
                                                on b.vendor_code = d.Vendor_ID
                                                where b.property_code = '{0}' and b.account_num = '{2}'
                                                and ((b.ecp_date is not null and DATEDIFF(day,b.ecp_date,GETDATE()) <= 22) or (b.issued_date is not null and DATEDIFF(day,b.issued_date,GETDATE()) <= 22 and b.work_item_action in (1, 3)))
                                                union all
                                                select cast(a.id as varchar(10)), 'Pending', CASE WHEN [USP type] = 'E' THEN 'Electricity' WHEN [USP type] = 'G' THEN 'Gas' ELSE 'WATER' END, a.AccountNumber, [Service From], [Service To], a.LastDateModified, null, [Total Charges], b.usp_name, NULL, NULL, NULL, NULL, NULL, b.InvoiceFolderName, NULL
                                                from vw_EMS_BulkUploadQuery a
                                                left join tbl_ADHOC_EM_Tasks b
                                                on a.id = b.id
                                                where [Property ID] = '{0}' and a.AccountNumber = '{2}'
                                                union all
                                                select cast(a.id as varchar(10)), 'Pending', CASE WHEN a.vendor_group = 'E' THEN 'Electricity' WHEN a.vendor_group = 'G' THEN 'Gas' ELSE 'WATER' END, a.AccountNumber, a.ServiceFrom, a.ServiceTo, a.LastDateModified, null, a.Amount, a.usp_name, NULL, NULL, NULL, NULL, NULL, a.InvoiceFolderName, NULL
                                                from vw_EM_ManualWIQueue a
                                                where a.property_code = '{0}' and a.AccountNumber = '{2}'
                                                union all
                                                select cast(a.id as varchar(10)), 'Pending', CASE WHEN a.vendor_group = 'E' THEN 'Electricity' WHEN a.vendor_group = 'G' THEN 'Gas' ELSE 'WATER' END, b.AccountNumber, b.ServiceFrom, b.ServiceTo, a.LastDateModified, null, b.Amount, b.usp_name, NULL, NULL, NULL, NULL, NULL, b.InvoiceFolderName, NULL
                                                from dbo.vw_EM_VendorAssignmentQueue a
                                                inner join tbl_ADHOC_EM_Tasks b
                                                on a.id = b.id
                                                where a.property_code = '{0}' and b.AccountNumber = '{2}'
                                                union all
                                                select case when ISNULL(a.work_item, '') = '' then cast(a.id as varchar(50)) else a.work_item end work_item, case when isnull(WorkItemStatus, '') = '' then 'Created' else WorkItemStatus end, CASE WHEN a.vendor_group = 'E' THEN 'Electricity' WHEN a.vendor_group = 'G' THEN 'Gas' ELSE 'WATER' END, a.AccountNumber, a.ServiceFrom, a.ServiceTo, a.LastDateModified, case when a.SentToExpeditedPaymentDate is null then NULL else a.SentToExpeditedPaymentDate end, a.Amount, a.usp_name, NULL, NULL, NULL, NULL, NULL, a.InvoiceFolderName, DATEADD(HOUR, -12, a.date_bulkuploaded)
                                                from tbl_ADHOC_EM_Tasks a
                                                left join tbl_EM_PayAppWorkItems b
                                                on a.work_item = b.work_item
                                                where a.property_code = '{0}'
                                                and a.AccountNumber = '{2}'
                                                and (a.PendingReasons like 'Approve%' or a.PendingReasons like '%Bulk Upload%' or a.PendingReasons like '%Manual WI%')
                                                and a.date_bulkuploaded is not null
                                                and DATEDIFF(day, a.date_bulkuploaded,GETDATE()) <= 22
                                                and b.work_item is null
                                                union all
                                                select a.work_item, 'For review', CASE WHEN a.vendor_group = 'E' THEN 'Electricity' WHEN a.vendor_group = 'G' THEN 'Gas' ELSE 'WATER' END, a.AccountNumber, a.ServiceFrom, a.ServiceTo, a.LastDateModified, null, a.Amount, a.usp_name, NULL, NULL, NULL, NULL, NULL, a.InvoiceFolderName, NULL
                                                from tbl_ADHOC_EM_Tasks a
                                                where a.property_code = '{0}'
                                                and a.AccountNumber = '{2}'
                                                and a.PendingReasons in ('Pending Duplicate', 'OCR Pending Duplicate', 'Pre-work item Review', 'Not a Duplicate', 'Approved in Duplicate Review')
                                                and a.isLatestData = 1
                                                and a.Invoice <> '{1}'
                                                ) as tbl_dup
                                                outer apply
                                                (select top 1 full_address from tbl_REO_Properties where property_code = '{0}') prop
                                                order by ecp_date desc, issued_date desc", propertyCode, Invoice, accountnum, utility);

            if (utility == "E")
            {
                utility = "elec";
            }
            else if (utility == "G")
            {
                utility = "gas";
            }
            else
            {
                utility = "water";
            }

            string qry_prevwis = string.Format(@"select top 5 a.work_item, work_item_status, line_item, account_num, service_frm, service_to, null [dup_queue_date], a.ecp_date, a.vendor_price, c.Vendor_Name,
                                                a.payment_date_processed, a.check_no, a.check_date, a.check_status, a.check_cashed_date, b.InvoiceFolderName, a.issued_date
                                                from tbl_EM_PayAppWorkItems a
                                                left join tbl_ADHOC_EM_Tasks b
                                                on a.work_item = b.work_item
                                                left join vw_VID c
                                                on a.vendor_code = c.Vendor_ID
                                                where a.property_code = '{0}'
                                                and a.line_item like '%{1}%'
                                                and a.work_item_status not like '%cancel%' 
                                                and a.work_item_status not like '%reject%'
                                                order by a.issued_date desc", propertyCode, utility);

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            DataTable dtp = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(qry_Duplicate, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);
            dataGridView2.DataSource = dt;

            dataGridView2.Columns["full_address"].Visible = false;

            conn.Close();

            if (queue == "Pre-work Item Review")
            {
                label15.Visible = true;
                dgPrevWIs.Visible = true;
                da = new SqlDataAdapter(qry_prevwis, conn);
                conn.Open();
                da.SelectCommand.CommandTimeout = 1800;
                da.Fill(dtp);
                dgPrevWIs.DataSource = dtp;

                conn.Close();
            }
            else
            {
                dgPrevWIs.Rows.Clear();
                label15.Visible = false;
                dgPrevWIs.Visible = false;
            }
        }

        
        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1)
            {
                
                string propertyCode = dataGridView1.Rows[e.RowIndex].Cells["c_property_code"].Value.ToString().Trim();
                string invoice = dataGridView1.Rows[e.RowIndex].Cells["c_invoice"].Value.ToString().Trim();
                string accountNumber = dataGridView1.Rows[e.RowIndex].Cells["c_account_number"].Value.ToString().Trim();
                string utility = dataGridView1.Rows[e.RowIndex].Cells["c_vendor_group"].Value.ToString().Trim();

                   
                    
                    selectedInfo = duplicates.First(p => p.property_code == propertyCode &&
                        p.Invoice == invoice && p.AccountNumber == accountNumber);

                    label5.Text = selectedInfo.Invoice;

                    webBrowser2.Navigate(string.Empty);

                    try
                    {
                        if (dataGridView1.Columns[dataGridView1.CurrentCell.ColumnIndex].Name != "c_Comments" &&
                            dataGridView1.Columns[dataGridView1.CurrentCell.ColumnIndex].Name != "c_amount")
                        {                         
                            if (System.IO.File.Exists(selectedInfo.InvoiceFolderName))
                            {
                                Uri tempUri = new Uri(selectedInfo.InvoiceFolderName);
                                webBrowser1.Navigate(tempUri);
                            }
                            else if (selectedInfo.Invoice.Contains("Urjanet"))
                            {
                                Uri tempUri = new Uri(selectedInfo.InvoiceFolderName);
                                webBrowser1.Navigate(tempUri);
                            }
                            else
                            {
                                Uri tempUri = new Uri(tempFile);
                                webBrowser1.Navigate(tempUri);
                            }                                                      
                        }
                    }
                    catch
                    {
                        Uri tempUri = new Uri(tempFile);
                        webBrowser1.Navigate(tempUri);

                        if (duplicateInfo != null)
                        {
                            //txtNote.Text = duplicateInfo.work_item.ToString();
                        }
                        else
                        {
                            // txtNote.Text = string.Empty;
                        }

                    }

                    loadPossibleDuplicates(propertyCode, invoice, accountNumber, utility);
                //}
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == string.Empty)
            {
                button1.Enabled = false;
            }
            else
            {
                button1.Enabled = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text != string.Empty)
            {

                if (CheckifProcessed(selectedInfo.id.ToString()) == true)
                {
                    MessageBox.Show("Invoice was already processed.", "Duplicate Invoice Approval");

                    btnRefresh_Click(null, null);

                    return;
                }

                Database.DBConnection.DatabaseREO_Properties propconn = new Database.DBConnection.DatabaseREO_Properties();
                Database.DB.REO_Properties propdata = new Database.DB.REO_Properties();
                propdata = propconn.GetData2(selectedInfo.property_code);
                string dupWIs = string.Empty;

                foreach (DataGridViewRow dr in dataGridView2.Rows)
                {
                    dupWIs += dr.Cells[0].Value + "; ";
                }

                selectedInfo.DupWIReview = dupWIs;

                if (comboBox1.Text == "Approve" && selectedInfo != null)
                {

                    if (selectedInfo.Amount.ToString() != dataGridView1.CurrentRow.Cells["c_amount"].Value.ToString())
                    {
                        selectedInfo.Amount = dataGridView1.CurrentRow.Cells["c_amount"].Value.ToString();

                        if (dataGridView1.CurrentRow.Cells["c_Comments"].Value == null)
                        {
                            MessageBox.Show("You have changed the amount information. Please provide comments for documentation purposes.");

                            //btnRefresh_Click(null, null);

                            return;
                        }
                    }

                    if (selectedInfo.SpecialInstruction.ToString() != "")
                    {
                        selectedInfo.SpecialInstruction += "; " + dataGridView1.CurrentRow.Cells["c_Comments"].Value;
                    }
                    else
                    {
                        if (dataGridView1.CurrentRow.Cells["c_Comments"].Value != null)
                        {
                            selectedInfo.SpecialInstruction = dataGridView1.CurrentRow.Cells["c_Comments"].Value.ToString();
                        }
                    }

                    if (selectedInfo.PendingReasons == "Pending Duplicate")
                    {
                        if (selectedInfo.BillTo != string.Empty && selectedInfo.BillTo != "ASFI or c/o ASFI")
                        {

                            if (propdata.customer_name == "PFC")
                            {
                                selectedInfo.Complete = 1;
                                //selectedInfo.PendingReasons = "Pre-work item Review";
                                selectedInfo.PendingReasons = "Approved in Duplicate Review";
                            }
                            else if (propdata.customer_name == "OLSR")
                            {

                                if (selectedInfo.SpecialInstruction != string.Empty)
                                {
                                    selectedInfo.Complete = 1;
                                    //selectedInfo.PendingReasons = "Pre-work item Review";
                                    selectedInfo.PendingReasons = "Approved in Duplicate Review";
                                }
                                else
                                {
                                    selectedInfo.Complete = 1;
                                    //selectedInfo.PendingReasons = "Pre-work item Review";
                                    selectedInfo.PendingReasons = "Approved in Duplicate Review";
                                }
                            }
                            else
                            {
                                selectedInfo.Complete = 6;
                                selectedInfo.PendingReasons = "Non-ASFI Invoice";
                            }
                        }
                        else
                        {
                            if (selectedInfo.SpecialInstruction != string.Empty)
                            {
                                selectedInfo.Complete = 1;
                                //selectedInfo.PendingReasons = "Pre-work item Review";
                                selectedInfo.PendingReasons = "Approved in Duplicate Review";
                            }
                            else
                            {
                                if (propdata.customer_name == "PFC")
                                {
                                    selectedInfo.Complete = 1;
                                    //selectedInfo.PendingReasons = "Pre-work item Review";
                                    selectedInfo.PendingReasons = "Approved in Duplicate Review";
                                }
                                else
                                {
                                    selectedInfo.Complete = 1;
                                    //selectedInfo.PendingReasons = "Pre-work item Review";
                                    selectedInfo.PendingReasons = "Approved in Duplicate Review";
                                }
                            }
                        }
                    }
                    else
                    {
                        if (selectedInfo.SpecialInstruction == string.Empty && selectedInfo.AttachmentFile == string.Empty)
                        {
                            selectedInfo.Complete = 1;
                            selectedInfo.PendingReasons = "Approved Duplicate Bulk Upload";
                        }
                        else
                        {
                            selectedInfo.Complete = 1;
                            selectedInfo.PendingReasons = "Approved Duplicate Manual WI";
                        }
                    }


                    selectedInfo.LastDateModified = DateTime.Now;
                    selectedInfo.userid = Environment.UserName;
                    selectedInfo.remarks = string.Empty;
                    selectedInfo.VendorAssignedStatus = "New";
                }
                else
                {
                    selectedInfo.Complete = 1;
                    selectedInfo.PendingReasons = "Duplicate Invoice";
                    selectedInfo.LastDateModified = DateTime.Now;
                    selectedInfo.userid = Environment.UserName;
                }

                Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();

                try
                {
                    if (selectedInfo.Amount == string.Empty || selectedInfo.Amount == "" || selectedInfo.Amount == "0" || selectedInfo.Amount == "0.00")
                    {
                        selectedInfo.PendingReasons = "Zero or Negative value";
                    }

                    conn.UpdateLatestData(selectedInfo.id);
                    conn.WriteData(selectedInfo);

                    MessageBox.Show("Updated Successfully!");

                    DiplayForPending();

                    selectedInfo = new Database.DB.EM_Tasks();
                    duplicateInfo = new Database.DB.EM_Tasks();

                    Uri myUri = new Uri(tempFile);
                    webBrowser1.Navigate(myUri);
                    webBrowser2.Navigate(myUri);

                    comboBox1.Text = string.Empty;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void SendToRSC(string propaddress, string propstatus, string amount)
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"insert into tbl_EM_RSCApproval(created_by,date_created,date_modified,invoice,modified_by,notes,[status],
                                            property_code,full_address,vendor_code,vendor_name,amount,vendor_type,property_status,invoice_link,asset_manager)
                                            values('{0}', '{10}', '{10}', '{1}', '{0}', 'New Request ' + CAST(CAST(GETDATE() as date) as varchar(15)), 'Pending',
                                            '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', 
                                            (select top 1 asset_manager from MIS_Alti.dbo.tbl_EM_AssetManagers WHERE (property_code = '{2}' or property_code = '{2}' + '1'))) ",
                                            Environment.UserName.ToLower(), selectedInfo.Invoice, selectedInfo.property_code, propaddress, selectedInfo.VendorId, selectedInfo.usp_name, amount.Replace(",",""), selectedInfo.vendor_group, propstatus, selectedInfo.InvoiceFolderName, DateTime.Now.ToString());

            SqlCommand cmd = new SqlCommand(query, conn);

            int rowsAffected = cmd.ExecuteNonQuery();

            conn.Close();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {

            dataGridView1.Rows.Clear();
            dataGridView2.DataSource = null;
            Uri tempUri = new Uri(tempFile);
            webBrowser2.Navigate(tempUri);
            webBrowser1.Navigate(tempUri);

            DiplayForPending();            
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private static bool IsDate(string val)
        {
            string strDate = val;
            try
            {
                DateTime dt = DateTime.Parse(strDate);
                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        private bool CheckifTrailingAsset(string investor, string outofREOdate)
        {
            bool isTrailingAsset = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"if (DATEDIFF(day, cast('{1}' as date), cast(GETDATE() as date)) > (select top 1 number_of_days from tbl_EM_property_investors where (investor_code1 = '{0}' or investor_code2 = '{0}'))) 
                                            or (DATEDIFF(day, cast('{1}' as date), cast(GETDATE() as date)) > 120)
                                            begin select 'True' end", investor, outofREOdate);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    isTrailingAsset = true;
                }
                catch
                {
                    isTrailingAsset = false;
                }
            }
            else
            {
                isTrailingAsset = false;
            }

            conn.Close();

            return isTrailingAsset;
        }

        private bool CheckifProcessed(string id)
        {
            bool isProcessed = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select b.userid from tbl_ADHOC_EM_Tasks a
                                            outer apply
                                            (select top 1 * from tbl_ADHOC_EM_Tasks x where x.Invoice = a.Invoice and x.LastDateModified > a.LastDateModified order by x.LastDateModified) b
                                            where a.id = {0}", id);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    if (rd[0].ToString() == "")
                    {
                        isProcessed = false;
                    }
                    else
                    {
                        isProcessed = true;
                    }
                }
                catch
                {
                    isProcessed = false;
                }
            }
            else
            {
                isProcessed = false;
            }

            conn.Close();

            return isProcessed;
        }

        private void SendToDeactivation(string propertycode, string utility, int mode, string outofREODate, string client_code, Int32 id, string usp_name, string AccountNumber, string full_address)
        {
            SqlConnection cnn = new SqlConnection(Constants.connectionString); ;
            SqlCommand cmd;
            string sql = string.Empty;

            if (mode == 1)
            {
                sql += string.Format(@"insert into tbl_EM_DeactivationfromEM (EM_ID, PropertyCode, Utility, DeactivationDate, DeactivatedBy, DateEMValidated) 
                                    select TOP 1 {0}, '{1}', '{2}', PostedDate, PostedBy, GETDATE() from tbl_UC_PropertyDeact where ([Property ID] = '{1}' or [Property ID] = '{1}' + '1') and SUBSTRING(LTRIM(Utility),1,1) = '{2}' and [Status] = 'Deactivated' and DATEDIFF(day,PostedDate,GETDATE()) > 45 order by Posteddate desc  " + Environment.NewLine, id, propertycode, utility);
                sql += string.Format(@"update tbl_UC_PropertyDeact set [Status] = 'Rework', UpdateFlag = 0, UpdatedVMS = 0, VMSPostedDate = NULL where ([Property ID] = '{0}' or [Property ID] = '{0}' + '1') and SUBSTRING(LTRIM(Utility),1,1) = '{1}' and [Status] = 'Deactivated' and DATEDIFF(day,PostedDate,GETDATE()) > 45 ", propertycode, utility);
            }
            else
            {



                if (client_code != "PFC" && (utility == "E" || utility == "G" || utility == "W"))
                {

                    sql += string.Format(@"insert into tbl_EM_DeactivationfromEM (EM_ID, PropertyCode, Utility, DeactivationDate, DateEMValidated) 
                                       values ({0},'{1}','{2}','{3}',GETDATE())", id, propertycode, utility, outofREODate.ToString());

                    string vg = string.Empty;
                    string cl = string.Empty;

                    if (client_code == "OLSR")
                    {
                        cl = "REO";
                    }
                    else
                    {
                        cl = client_code;
                    }

                    if (utility == "E")
                    {
                        vg = "Elec";
                    }
                    else if (utility == "G")
                    {
                        vg = "Gas";
                    }
                    else if (utility == "W")
                    {
                        vg = "Water";
                    }

                    sql += string.Format(@"if (select top 1 [Status] from tbl_UC_PropertyDeact where [Property ID] = '{0}' and Utility = '{1}') = 'Deactivated'
                                        begin
	                                        update tbl_UC_PropertyDeact set [Status] = 'Rework', UpdateFlag = 0, UpdatedVMS = 0, VMSPostedDate = NULL where [Property ID] = '{0}' and Utility = '{1}'
                                        end
                                        else
                                        begin
                                        	
	                                        if (select COUNT(*) from tbl_UC_PropertyDeact where [Property ID] = '{0}' and Utility = '{1}') = 0
	                                        begin
		                                        print 'Insert'
                                        		
		                                        if (select COUNT(*) from tbl_UC_PropertyInFlow where Origin = 'EMS' and PropertyCode = '{0}' and Utility = '{1}' and Uploaded = 0) = 0
                                                    begin
                                                    
                                                    insert into tbl_UC_PropertyInFlow (USPName, PropertyCode, AccountNo, [Address], Category, Task, Utility, Origin, DateUploaded)
                                                    values('{2}', '{0}', '{3}', '{4}', '{5}', 'Deactivation', '{1}', 'EMS', CAST(GETDATE() AS DATE))
                                                    end

                                                update tbl_UC_ParameterReference set REFVALUE = '3' where REFKEY = 'Update_OpenRpt'
	                                        end
                                        end", propertycode, vg, usp_name, AccountNumber, full_address, cl);
                }

            }


            try
            {
                cnn.Open();
                cmd = new SqlCommand(sql, cnn);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                cnn.Close();
                //MessageBox.Show("ExecuteNonQuery in SqlCommand executed !!");
            }
            catch (Exception ex)
            {
            }
        }


        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && dataGridView2.Rows.Count > 0)
            {
                string dupFolderName = "";

                
                dupFolderName = dataGridView2.Rows[e.RowIndex].Cells["InvoiceFolderName"].Value.ToString().Trim();

                try
                {

                    if (dupFolderName != "")
                    {
                        Uri tempUri = new Uri(dupFolderName);
                        webBrowser2.Navigate(tempUri);
                    }
                    else
                    {
                        Uri tempUri = new Uri(tempFile);
                        webBrowser2.Navigate(tempUri);
                    }
                }
                catch
                {
                    Uri tempUri = new Uri(tempFile);
                    webBrowser2.Navigate(tempUri);
                }
            }
        }

        private void dataGridView1_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dataGridView1.Columns["c_property_code"].ReadOnly = true;
            dataGridView1.Columns["c_full_address"].ReadOnly = true;
            dataGridView1.Columns["Age"].ReadOnly = true;
            dataGridView1.Columns["c_propstat"].ReadOnly = true;
            dataGridView1.Columns["c_vendor_group"].ReadOnly = true;
            dataGridView1.Columns["c_account_number"].ReadOnly = true;
            dataGridView1.Columns["c_service_from"].ReadOnly = true;
            dataGridView1.Columns["c_service_to"].ReadOnly = true;

            //dataGridView1.Columns["c_amount"].ReadOnly = false;
            //dataGridView1.Columns["c_Comments"].ReadOnly = false;
        }

        private void dataGridView1_CellMouseUp(object sender, DataGridViewCellMouseEventArgs e)
        {
            //if (dataGridView1.IsCurrentRowDirty)
            //{
            //    dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
            //}
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            List<Database.DB.EM_Tasks> temp = new List<Database.DB.EM_Tasks>();
            duplicates = new List<Database.DB.EM_Tasks>();

            temp = conn.SearchDuplicates(queue, txtSearch.Text.Trim());

            if (temp != null)
            {
                duplicates.AddRange(temp);
            }

            if (duplicates != null)
            {
                try
                {
                    dataGridView1.Invoke((Action)delegate
                    {
                        dataGridView1.Rows.Clear();
                        dataGridView2.DataSource = null;
                        dgPrevWIs.DataSource = null;

                        foreach (Database.DB.EM_Tasks info in duplicates)
                        {
                            dataGridView1.Rows.Add(
                                info.property_code,
                                info.full_address,
                                info.Age,
                                info.property_status,
                                info.vendor_group,
                                info.usp_name,
                                info.AccountNumber,
                                (info.ServiceFrom.ToString() == "1/1/0001 12:00:00 AM" ? "" : info.ServiceFrom.ToString("MM/dd/yyyy")),
                                (info.ServiceTo.ToString() == "1/1/0001 12:00:00 AM" ? "" : info.ServiceTo.ToString("MM/dd/yyyy")),
                                string.Format("{0:N2}", info.Amount),
                                info.Invoice,
                                null,
                                info.PendingReasons);
                        }
                    });
                }
                catch
                {
                }
            }

            if (queue == "Pre-work Item Review")
            {
                dataGridView1.Columns["Category"].Visible = true;
            }
            else
            {
                dataGridView1.Columns["Category"].Visible = false;
            }
        }

        private void dgPrevWIs_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && dgPrevWIs.Rows.Count > 0)
            {
                string dupFolderName = "";

                dupFolderName = dgPrevWIs.Rows[e.RowIndex].Cells["InvoiceFolderName"].Value.ToString().Trim();

                try
                {

                    if (dupFolderName != "")
                    {
                        Uri tempUri = new Uri(dupFolderName);
                        webBrowser2.Navigate(tempUri);
                    }
                    else
                    {
                        Uri tempUri = new Uri(tempFile);
                        webBrowser2.Navigate(tempUri);
                    }
                }
                catch
                {
                    Uri tempUri = new Uri(tempFile);
                    webBrowser2.Navigate(tempUri);
                }
            }
        }

        




        //private void dataGridView1_DataError(object sender, DataGridViewDataErrorEventArgs e)
        //{
        //    try
        //    {
        //        if (e.Exception.Message == "DataGridViewComboBoxCell value is not valid.")
        //        {
        //            object value = dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value;
        //            if (!((DataGridViewComboBoxColumn)dataGridView1.Columns[e.ColumnIndex]).Items.Contains(value))
        //            {
        //                ((DataGridViewComboBoxColumn)dataGridView1.Columns[e.ColumnIndex]).Items.Add(value);
        //            }
        //        }

        //        throw e.Exception;
        //    }
        //    catch (Exception ex)
        //    {
        //        DataGridViewComboBoxCell cbcell = (DataGridViewComboBoxCell)dataGridView1.CurrentRow.Cells["c_Comments"];
        //        cbcell.DataSource = null;

        //        //PopulatePossibleDuplicates(dataGridView1.CurrentRow.Cells[7].Value.ToString(), dataGridView1.CurrentRow.Cells[6].Value.ToString(), dataGridView1.CurrentRow.Cells[2].Value.ToString());
        //    }
        //}
    }
}
