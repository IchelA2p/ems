﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms.Administrator
{
    public partial class AssignQueue : Form
    {
        List<Database.DB.AssignQueue> q;

        public AssignQueue()
        {
            InitializeComponent();

            q = new List<Database.DB.AssignQueue>();
        }

        private void AssignQueue_Load(object sender, EventArgs e)
        {
            LoadQueueData();
        }

        private void LoadQueueData()
        {
            q = new List<Database.DB.AssignQueue>();
            dataGridView1.Rows.Clear();

            Database.DBConnection.DatabaseAssignQueue conn = new Database.DBConnection.DatabaseAssignQueue();

            try
            {
                q = conn.GetAllData();
            }
            catch
            {
                q = null;
            }

            if (q != null)
            {
                foreach (Database.DB.AssignQueue que in q)
                {
                    dataGridView1.Rows.Add(
                        que.queue_id,
                        que.userid,
                        que.current_queue,
                        que.assignee,
                        que.unchangeable == 1 ? true : false);
                }
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            SaveData();
        }

        private void SaveData()
        {
            if (q != null)
            {
                try
                {
                    foreach (DataGridViewRow dr in dataGridView1.Rows)
                    {
                        Database.DB.AssignQueue data = new Database.DB.AssignQueue();
                        Database.DBConnection.DatabaseAssignQueue conn = new Database.DBConnection.DatabaseAssignQueue();
                        data = q.First(p => p.queue_id == int.Parse(dr.Cells["queue_id"].Value.ToString()));

                        string current_queue = dr.Cells["queue_status"].Value.ToString();
                        int isFixed = Convert.ToBoolean(dr.Cells["queue_fixed"].Value) == true ? 1 : 0;

                        if (data.current_queue != current_queue || data.unchangeable != isFixed)
                        {
                            data.current_queue = current_queue;
                            data.unchangeable = isFixed;
                            data.assignee = Environment.UserName;
                            conn.UpdateData(data);
                        }
                    }

                    MessageBox.Show("Data is saved.");
                    LoadQueueData();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                MessageBox.Show("There is no data to saved.");
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
