﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms.Queue
{
    public partial class RealResolutionQueue : Form
    {
        private Database.DB.EM_Tasks currentData;

        // RR folders
        public string RR_NEW = @"\\PIV8FSASNP01\CommonShare\EMProject\RR\NEW\";
        public string RR_PENDING = @"\\PIV8FSASNP01\CommonShare\EMProject\RR\PENDING\";
        public string RR_DONE = @"\\PIV8FSASNP01\CommonShare\EMProject\RR\DONE\";
        public string TempFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\ONGOING\";

        public RealResolutionQueue()
        {
            InitializeComponent();

            currentData = new Database.DB.EM_Tasks();
        }

        private bool GetLatestRRinQueue()
        {
            Database.DBConnection.DatabaseEM_Tasks emDb = new Database.DBConnection.DatabaseEM_Tasks();

            string[] pdfs = System.IO.Directory.GetFiles(RR_NEW, "*.pdf", System.IO.SearchOption.AllDirectories);

            if (pdfs.Count() == 0)
            {
                pdfs = System.IO.Directory.GetFiles(RR_PENDING, "*.pdf", System.IO.SearchOption.AllDirectories);
            }

            string newFilePath = System.IO.Path.Combine(TempFolder, System.IO.Path.GetFileNameWithoutExtension(pdfs[0]) + "_" + Environment.UserName + "_lock.pdf");


            try
            {
                System.IO.File.Move(pdfs[0], newFilePath);

                string invoiceName = System.IO.Path.GetFileNameWithoutExtension(newFilePath).Split('_')[0];

                currentData = emDb.GetLatestInvoiceData(invoiceName);

                webBrowser1.Navigate(newFilePath);

                return true;
            }
            catch
            {
                currentData = new Database.DB.EM_Tasks();
                return false;
            }
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Completed")
            {
                textBox1.Enabled = true;

                comboBox2.Visible = true;
                label4.Visible = true;
                comboBox2.Items.Clear();
                comboBox2.Items.Add("RR - Expoor / Extrail");
                comboBox2.Items.Add("RR - Expc");
            }
            else if (comboBox1.Text == "Declined")
            {
                textBox1.Enabled = false;

                comboBox2.Visible = true;
                label4.Visible = true;
                comboBox2.Items.Clear();
                comboBox2.Items.Add("NON MKTBLE/NON PPI");
            }
            else
            {
                textBox1.Enabled = false;
                comboBox2.Items.Clear();
                comboBox2.Visible = false;
                label4.Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Completed")
            {
                if (comboBox1.Text != string.Empty && textBox1.Text != string.Empty)
                {
                    SaveInformation();
                }
                else
                {
                    MessageBox.Show("Status and Work Item should not be blank");
                }
            }
            else
            {
                SaveInformation();
            }
        }

        private void SaveInformation()
        {
            Database.DBConnection.DatabaseEM_Tasks emDb = new Database.DBConnection.DatabaseEM_Tasks();
            //currentData

            if (comboBox1.Text == "Completed" || comboBox1.Text == "Declined")
            {
                currentData.Complete = 1;
                currentData.PendingReasons = comboBox2.Text;
                currentData.work_item = textBox1.Text;
                currentData.LastDateModified = DateTime.Now;
                currentData.userid = Environment.UserName;
                currentData.InvoiceFolderName = System.IO.Path.Combine(RR_DONE, currentData.Invoice + ".pdf");

            }
            else if (comboBox1.Text == "Contacted Asset Manager")
            {
                currentData.Complete = 1;
                currentData.PendingReasons = "RR - Pending";
                currentData.LastDateModified = DateTime.Now;
                currentData.userid = Environment.UserName;
                currentData.InvoiceFolderName = System.IO.Path.Combine(RR_PENDING, currentData.Invoice + ".pdf");
            }

            try
            {
                emDb.WriteData(currentData);
                MessageBox.Show("Data Saved!");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Save Error! " + ex.Message);
            }

            RealResolutionQueue_Load(null, null);
        }

        private void RealResolutionQueue_Load(object sender, EventArgs e)
        {
            bool fileLoaded = false;

            while (!fileLoaded)
            {
                fileLoaded = GetLatestRRinQueue();
            }
        }

    }

}
