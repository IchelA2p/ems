﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms.Reports
{
    public partial class frmSLA : Form
    {
        public frmSLA()
        {
            InitializeComponent();
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {

            dgList.DataSource = null;
            dgList.Columns.Clear();

            string qry = "";
                       
            qry = string.Format(@"EXEC sp_EM_SLAReport '{0} 20:00', '{1} 20:00', '{2}'", dtFrom.Value.Date.ToString("yyyy-MM-dd"), dtTo.Value.Date.ToString("yyyy-MM-dd"), txtParam.Text.Trim());
            
            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            DataTable dtf = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(qry, conn);

            conn.Open();

            da.SelectCommand.CommandTimeout = 600;
            da.Fill(dt);

            conn.Close();

            dgList.DataSource = dt;

            //dgList.Columns["sla2_rem"].DisplayIndex = 25;
            //dgList.Columns["sla2_rem"].Visible = false;
            //DataGridViewComboBoxColumn cmbCol = new DataGridViewComboBoxColumn();
            //cmbCol.HeaderText = "SLA 3 Reason";
            //cmbCol.Name = "sla3r";
            //cmbCol.Items.Add("VMS Error");
            //cmbCol.Items.Add("EMS Error");
            //cmbCol.Width = 120;
            //cmbCol.FlatStyle = FlatStyle.Flat;
            //dgList.Columns.Add(cmbCol);
            //dgList.Columns["sla3r"].DisplayIndex = 15;

            //dgList.Columns["SLA3_Reason"].Visible = false;

            ////dgList.Columns["sla4_rem"].DisplayIndex = 24;
            //dgList.Columns["sla4_rem"].Visible = false;
            //DataGridViewComboBoxColumn cmbCol2 = new DataGridViewComboBoxColumn();
            //cmbCol2.HeaderText = "sla4_reason";
            //cmbCol2.Name = "sla4_reason";
            //cmbCol2.Items.Add("Status Discrepancy");
            //cmbCol2.Items.Add("System Error");
            //cmbCol2.Width = 120;
            //cmbCol2.FlatStyle = FlatStyle.Flat;
            //dgList.Columns.Add(cmbCol2);
            //dgList.Columns["sla4_reason"].DisplayIndex = 23;


            //DataGridViewTextBoxColumn txtCol = new DataGridViewTextBoxColumn();
            //txtCol.HeaderText = "val3";
            //txtCol.Name = "val3";
            //txtCol.Width = 120;
            //dgList.Columns.Add(txtCol);
            //dgList.Columns["val3"].DisplayIndex = 19;


            //dgList.Columns["SLA 1"].HeaderText = "SLA 1 (1 day)";
            //dgList.Columns["SLA 2"].HeaderText = "SLA 2 (1 day)";
            //dgList.Columns["SLA 3"].HeaderText = "SLA 3 (1 day)";
            //dgList.Columns["SLA 4"].HeaderText = "SLA 4 (2 days)";

            dgList.Columns["Approve / Decline I"].HeaderText = "Approved / Declined";
            dgList.Columns["Approve / Decline II"].HeaderText = "Approved / Declined";
            dgList.Columns["LastDateModified"].Visible = false;
            dgList.Columns["REFDESC"].Visible = false;
            dgList.Columns["Previous Status"].Visible = false;
            dgList.Columns["QC Status"].Visible = false;

            FormatCells();

            //foreach (DataGridViewRow row in dgList.Rows)
            //{
            //    row.Cells["sla3r"].Value = row.Cells["SLA3_Reason"].Value;
            //}            

           

        }

        private void FormatCells()
        {

            foreach (DataGridViewColumn c in dgList.Columns)
            {

                if (c.Index >= 0 && c.Index <= 6)
                {
                    c.DefaultCellStyle.BackColor = Color.LightBlue;
                }

                if (c.Index > 6 && c.Index <= 10)
                {
                    c.DefaultCellStyle.BackColor = Color.LightPink;
                }

                if (c.Index > 10  && c.Index <= 14)
                {
                    c.DefaultCellStyle.BackColor = Color.LightGreen;
                }

                if (c.Index > 14 && c.Index <= 17)
                {
                    c.DefaultCellStyle.BackColor = Color.LightSalmon;
                }

                if (c.Index > 17 && c.Index <= 23)
                {
                    c.DefaultCellStyle.BackColor = Color.PaleGoldenrod;
                }

                
            }

            for (int x = 0; x <= dgList.Rows.Count - 1; x++)
            {

                try
                {
                    if (Convert.ToInt16(dgList.Rows[x].Cells["Aging"].Value) > 4)
                    {
                        dgList.Rows[x].Cells["Aging"].Style.BackColor = Color.LightCoral;
                    }
                }
                catch
                {

                    dgList.Rows[x].Cells["Aging"].Style.BackColor = Color.White;
                    
                }

                

                try
                {
                    if (Convert.ToInt16(dgList.Rows[x].Cells["Total SLA"].Value) > 4)
                    {
                        dgList.Rows[x].Cells["Total SLA"].Style.BackColor = Color.LightCoral;
                    }
                }
                catch
                {
                    dgList.Rows[x].Cells["Total SLA"].Style.BackColor = Color.White;
                }
              
            }

             //for (int x = 0; x <= dgList.Rows.Count - 1; x++)
             //{

             //    if (dgList.Rows[x].Cells["SLA 1"].Value.ToString() == "Outside SLA")
             //    {
             //        dgList.Rows[x].Cells["SLA 1"].Style.BackColor = Color.LightCoral;
             //    }


             //    if (dgList.Rows[x].Cells["SLA 2"].Value.ToString() == "Outside SLA")
             //    {
             //        dgList.Rows[x].Cells["SLA 2"].Style.BackColor = Color.LightCoral;
             //    }


             //    if (dgList.Rows[x].Cells["SLA 3"].Value.ToString() == "Outside SLA")
             //    {
             //        dgList.Rows[x].Cells["SLA 3"].Style.BackColor = Color.LightCoral;
             //    }


             //    if (dgList.Rows[x].Cells["SLA 4"].Value.ToString() == "Outside SLA")
             //    {
             //        dgList.Rows[x].Cells["SLA 4"].Style.BackColor = Color.LightCoral;
             //    }

             //}
        }

        private void dgList_CellEnter(object sender, DataGridViewCellEventArgs e)
        {

            //if (dgList.Rows.Count > 0)
            //{
            //    dgList.EditMode = DataGridViewEditMode.EditOnEnter;

            //    for (int x = 0; x <= dgList.Columns.Count - 1; x++)
            //    {
            //        if (dgList.Columns[x].Name == "sla3r")
            //        {
            //            for (int y = 0; y <= dgList.Rows.Count - 1; y++)
            //            {
            //                if (dgList.Rows[y].Cells["SLA 3"].Value.ToString() == "Outside SLA")
            //                {
            //                    dgList.Rows[y].Cells["sla3r"].ReadOnly = false;
            //                }
            //                else
            //                {
            //                    dgList.Rows[y].Cells["sla3r"].ReadOnly = true;
            //                    dgList.Rows[y].Cells["sla3r"].ReadOnly = false;
            //                }

            //                //dgList.Rows[y].Cells["val3"].Value = dgList.Rows[y].Cells["sla3r"].Value;
            //            }
                       
            //        }
            //        //else if (dgList.Columns[x].Name == "sla4_reason")
            //        //{
            //        //    for (int y = 0; y <= dgList.Rows.Count - 1; y++)
            //        //    {
            //        //        if (dgList.Rows[y].Cells["SLA 4"].Value.ToString() == "Outside SLA")
            //        //        {
            //        //            dgList.Rows[y].Cells["sla4_reason"].ReadOnly = false;
            //        //        }
            //        //        else
            //        //        {
            //        //            dgList.Rows[y].Cells["sla4_reason"].ReadOnly = true;
            //        //        }
            //        //    }

            //        //}
            //        else
            //        {
            //            dgList.Columns[x].ReadOnly = true;
            //        }                   
            //    }
            //}
        }

        private void dgList_Sorted(object sender, EventArgs e)
        {
            FormatCells();
        }

        private void dgList_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            //if (e.Control.GetType() == typeof(DataGridViewComboBoxEditingControl))
            //{

            //    DataGridViewComboBoxEditingControl cbo = e.Control as DataGridViewComboBoxEditingControl;

            //    cbo.DropDownStyle = ComboBoxStyle.DropDown;
            //    cbo.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            //    cbo.AutoCompleteSource = AutoCompleteSource.ListItems;

            //    cbo.Validating += new CancelEventHandler(cbo_Validating);

            //}
        }

        void cbo_Validating(object sender, CancelEventArgs e)
        {

            DataGridViewComboBoxEditingControl cbo = sender as DataGridViewComboBoxEditingControl;

            DataGridView grid = cbo.EditingControlDataGridView;

            object value = cbo.Text;

            // Add value to list if not there


            if (cbo.Items.IndexOf(value) == -1)
            {

                DataGridViewComboBoxColumn cboCol = grid.Columns[grid.CurrentCell.ColumnIndex] as DataGridViewComboBoxColumn;

                // Must add to both the current combobox as well as the template, to avoid duplicate entries...


                //cbo.Items.Add(value);

                //cboCol.Items.Add(value);

                grid.CurrentCell.Value = value;                
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {

            string qry = "";

            for (int y = 0; y <= dgList.Rows.Count - 1; y++)
            {
                if (dgList.Rows[y].Cells["sla3r"].Value != "" && (dgList.Rows[y].Cells["sla3r"].Value.ToString() == "VMS Error" || dgList.Rows[y].Cells["sla3r"].Value.ToString() == "EMS Error") && dgList.Rows[y].Cells["SLA3_Reason"].Value.ToString() != dgList.Rows[y].Cells["sla3r"].Value.ToString())
                {
                    qry += Environment.NewLine + string.Format(@"update tbl_ADHOC_EM_Tasks set SLA3_Reason = '{0}' where Invoice = '{2}' and work_item = '{1}'", dgList.Rows[y].Cells["sla3r"].Value.ToString(), dgList.Rows[y].Cells["Work Item"].Value.ToString(), dgList.Rows[y].Cells["Invoice"].Value.ToString());
                }
            }

            if (qry != "")
            {

                SqlConnection conn = new SqlConnection(Constants.connectionString);                                
                SqlCommand cmd = new SqlCommand();
                int i = 0;

                conn.Open();

                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = qry;

                try
                {
                    i = cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    conn.Close();

                    if (i > 0)
                    {
                        MessageBox.Show("Update successful!");

                        btnLoad_Click(null, null);
                    }
                    else
                    {
                        MessageBox.Show("Update failed!");

                        return;
                    }

                }
                catch
                {
                    MessageBox.Show("Update failed!");

                    return;
                }                
            }
        }
        
    }
}
