﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms.Reports
{
    public partial class frmNewFunnel : Form
    {
                
        Int32 tot_PB = 0;
        Int32 tot_EB = 0;
        Int32 tot_UJ = 0;
        Int32 tot_MB = 0;
        
        public frmNewFunnel()
        {
            InitializeComponent();                        
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            dgInflow.Rows.Clear();
            LoadInflowData();
            dgPending.Rows.Clear();
            LoadNewInvoiceData();
            dgCompleted.Rows.Clear();
            LoadOutflowData();
            dgSummary.Rows.Clear();
            LoadSummary();
            dgWISummary.Rows.Clear();
            LoadWIBreakdown();
        }

        private void LoadList(string PendingReason, int mode)
        {

            string query = string.Empty;

            if (mode == 0)
            {
                query = string.Format(@"select a.Invoice,case when a.Invoice like '%EBILL%' then 'E-Bill' when a.Invoice like '%Urjanet%' then 'Urjanet' else 'Paper bill' end [Invoice Type], 
                                            b.client_code [Client Name], b.usp_name [USP], b.property_code [Property Code], b.vendor_group [Utility], b.ServiceFrom [Service From], b.ServiceTo [Service To], 
                                            b.invoice_date [Invoice Date], b.DueDate [Due Date], b.Amount [Amount Due], b.PreviousBalance [Previous Balance], b.PaymentReceived [Payment Received], 
                                            b.LateFee [Late Fee], b.DisconnectionFee [Disconnection Fee], b.AmountAfterDue [Amount After Due], b.final_bill [Final Bill], b.SpecialInstruction [Notes], b.HasTransferAmount [Has Transfer Amount], 
                                            b.userid [Processed By], CAST(DATEADD(HH, -12, b.LastDateModified) AS DATE) [Date Processed], b.AuditedBy [Audited By], b.AuditDate [Audit Date], 
                                            b.work_item [Work Item], b.date_bulkuploaded [Date Bulk Uploaded], b.remarks [Work Order Remarks], b.WorkItemIssueDate [WI Issue Date], b.WorkItemStatus [WI Status], 
                                            b.VendorAssignedStatus [Vendor Assignment Remarks], b.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            outer apply
                                            (select top 1 case when x.Complete = 21 then 'Pending for Audit' else x.PendingReasons end PendingReasons2, x.* from tbl_ADHOC_EM_Tasks x where x.Invoice = a.Invoice and x.Complete <> 20 order by x.LastDateModified desc) b
                                            left join tbl_EM_ParameterReference c
                                            on b.PendingReasons2 = c.RefValue and c.REFKEY = 'EMS_Status' 
                                            where a.Complete = 20
                                            and a.LastDateModified between '{0}' and '{1}'
                                            and c.RefValue is not null
                                            and b.PendingReasons2 = '{2}'", dtpFrom.Value.ToString("yyyy-MM-dd HH:mm"), dtpTo.Value.ToString("yyyy-MM-dd HH:mm"), PendingReason);
            }
            else if (mode == 1)
            {
                query = string.Format(@"select a.Invoice,case when a.Invoice like '%EBILL%' then 'E-Bill' when a.Invoice like '%Urjanet%' then 'Urjanet' else 'Paper bill' end [Invoice Type], 
                                            b.client_code [Client Name], b.usp_name [USP], b.property_code [Property Code], b.vendor_group [Utility], b.ServiceFrom [Service From], b.ServiceTo [Service To], 
                                            b.invoice_date [Invoice Date], b.DueDate [Due Date], b.Amount [Amount Due], b.PreviousBalance [Previous Balance], b.PaymentReceived [Payment Received], 
                                            b.LateFee [Late Fee], b.DisconnectionFee [Disconnection Fee], b.AmountAfterDue [Amount After Due], b.final_bill [Final Bill], b.SpecialInstruction [Notes], b.HasTransferAmount [Has Transfer Amount], 
                                            b.userid [Processed By], b.LastDateModified [Date Processed], b.AuditedBy [Audited By], b.AuditDate [Audit Date], 
                                            b.work_item [Work Item], b.date_bulkuploaded [Date Bulk Uploaded], b.remarks [Work Order Remarks], b.WorkItemIssueDate [WI Issue Date], b.WorkItemStatus [WI Status], 
                                            b.VendorAssignedStatus [Vendor Assignment Remarks], b.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            outer apply
                                            (select top 1 case when x.Complete = 21 then 'Pending for Audit' when x.Complete = 20 then 'New Invoice' else x.PendingReasons end PendingReasons2, x.* from tbl_ADHOC_EM_Tasks x where x.Invoice = a.Invoice and x.Complete <> 20 order by x.LastDateModified desc) b
                                            left join tbl_EM_ParameterReference c
                                            on b.PendingReasons2 = c.RefValue 
                                            and c.REFKEY = 'EMS_Status' 
                                            where a.Complete = 20
                                            and a.LastDateModified between '{0}' and '{1}'
                                            and c.REFDESC in ('Pending for Encoding', 'Invoices for Callout', 'Invoices for review', 'Pending for Audit')
                                            and b.PendingReasons2 = '{2}'", dtpFrom.Value.ToString("yyyy-MM-dd HH:mm"), dtpTo.Value.ToString("yyyy-MM-dd HH:mm"), PendingReason);
            }
            else if (mode == 10)
            {
                query = string.Format(@"select a.Invoice,case when a.Invoice like '%EBILL%' then 'E-Bill' when a.Invoice like '%Urjanet%' then 'Urjanet' else 'Paper bill' end [Invoice Type], 
                                            b.client_code [Client Name], b.usp_name [USP], b.property_code [Property Code], b.vendor_group [Utility], b.ServiceFrom [Service From], b.ServiceTo [Service To], 
                                            b.invoice_date [Invoice Date], b.DueDate [Due Date], b.Amount [Amount Due], b.PreviousBalance [Previous Balance], b.PaymentReceived [Payment Received], 
                                            b.LateFee [Late Fee], b.DisconnectionFee [Disconnection Fee], b.AmountAfterDue [Amount After Due], b.final_bill [Final Bill], b.SpecialInstruction [Notes], b.HasTransferAmount [Has Transfer Amount], 
                                            b.userid [Processed By], b.LastDateModified [Date Processed], b.AuditedBy [Audited By], b.AuditDate [Audit Date], 
                                            b.work_item [Work Item], b.date_bulkuploaded [Date Bulk Uploaded], b.remarks [Work Order Remarks], b.WorkItemIssueDate [WI Issue Date], b.WorkItemStatus [WI Status], 
                                            b.VendorAssignedStatus [Vendor Assignment Remarks], b.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            outer apply
                                            (select top 1 case when x.Complete = 21 then 'Pending for Audit' else x.PendingReasons end PendingReasons2, x.* from tbl_ADHOC_EM_Tasks x where x.Invoice = a.Invoice and x.Complete <> 20 order by x.LastDateModified desc) b
                                            left join tbl_EM_ParameterReference c
                                            on b.PendingReasons2 = c.RefValue and c.REFKEY = 'EMS_Status' 
                                            where a.Complete = 20
                                            and a.LastDateModified between '{0}' and '{1}'
                                            and ISNULL(c.REFDESC, '') in ('Work item creation')
                                            and (isnull(b.work_item, '') <> '' or (b.BulkUploadTransId is not null and isnull(b.work_item, '') = '' and isnull(b.remarks, '') = ''))", dtpFrom.Value.ToString("yyyy-MM-dd HH:mm"), dtpTo.Value.ToString("yyyy-MM-dd HH:mm"), PendingReason);
            }
            else if (mode == 11)
            {
                query = string.Format(@"select a.Invoice,case when a.Invoice like '%EBILL%' then 'E-Bill' when a.Invoice like '%Urjanet%' then 'Urjanet' else 'Paper bill' end [Invoice Type], 
                                            b.client_code [Client Name], b.usp_name [USP], b.property_code [Property Code], b.vendor_group [Utility], b.ServiceFrom [Service From], b.ServiceTo [Service To], 
                                            b.invoice_date [Invoice Date], b.DueDate [Due Date], b.Amount [Amount Due], b.PreviousBalance [Previous Balance], b.PaymentReceived [Payment Received], 
                                            b.LateFee [Late Fee], b.DisconnectionFee [Disconnection Fee], b.AmountAfterDue [Amount After Due], b.final_bill [Final Bill], b.SpecialInstruction [Notes], b.HasTransferAmount [Has Transfer Amount], 
                                            b.userid [Processed By], b.LastDateModified [Date Processed], b.AuditedBy [Audited By], b.AuditDate [Audit Date], 
                                            b.work_item [Work Item], b.date_bulkuploaded [Date Bulk Uploaded], b.remarks [Work Order Remarks], b.WorkItemIssueDate [WI Issue Date], b.WorkItemStatus [WI Status], 
                                            b.VendorAssignedStatus [Vendor Assignment Remarks], b.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            outer apply
                                            (select top 1 case when x.Complete = 21 then 'Pending for Audit' else x.PendingReasons end PendingReasons2, x.* from tbl_ADHOC_EM_Tasks x where x.Invoice = a.Invoice and x.Complete <> 20 order by x.LastDateModified desc) b
                                            left join tbl_EM_ParameterReference c
                                            on b.PendingReasons2 = c.RefValue and c.REFKEY = 'EMS_Status' 
                                            where a.Complete = 20
                                            and a.LastDateModified between '{0}' and '{1}'
                                            and ISNULL(c.REFDESC, '') in ('Work item creation')
                                            and (b.VendorAssignedStatus in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned') or b.remarks = 'Vendor ID is inactive/invalid.')", dtpFrom.Value.ToString("yyyy-MM-dd HH:mm"), dtpTo.Value.ToString("yyyy-MM-dd HH:mm"), PendingReason);
            }
            else if (mode == 12)
            {
                query = string.Format(@"select a.Invoice,case when a.Invoice like '%EBILL%' then 'E-Bill' when a.Invoice like '%Urjanet%' then 'Urjanet' else 'Paper bill' end [Invoice Type], 
                                            b.client_code [Client Name], b.usp_name [USP], b.property_code [Property Code], b.vendor_group [Utility], b.ServiceFrom [Service From], b.ServiceTo [Service To], 
                                            b.invoice_date [Invoice Date], b.DueDate [Due Date], b.Amount [Amount Due], b.PreviousBalance [Previous Balance], b.PaymentReceived [Payment Received], 
                                            b.LateFee [Late Fee], b.DisconnectionFee [Disconnection Fee], b.AmountAfterDue [Amount After Due], b.final_bill [Final Bill], b.SpecialInstruction [Notes], b.HasTransferAmount [Has Transfer Amount], 
                                            b.userid [Processed By], b.LastDateModified [Date Processed], b.AuditedBy [Audited By], b.AuditDate [Audit Date], 
                                            b.work_item [Work Item], b.date_bulkuploaded [Date Bulk Uploaded], b.remarks [Work Order Remarks], b.WorkItemIssueDate [WI Issue Date], b.WorkItemStatus [WI Status], 
                                            b.VendorAssignedStatus [Vendor Assignment Remarks], b.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            outer apply
                                            (select top 1 case when x.Complete = 21 then 'Pending for Audit' else x.PendingReasons end PendingReasons2, x.* from tbl_ADHOC_EM_Tasks x where x.Invoice = a.Invoice and x.Complete <> 20 order by x.LastDateModified desc) b
                                            left join tbl_EM_ParameterReference c
                                            on b.PendingReasons2 = c.RefValue and c.REFKEY = 'EMS_Status' 
                                            where a.Complete = 20
                                            and a.LastDateModified between '{0}' and '{1}'
                                            and ISNULL(c.REFDESC, '') in ('Work item creation')
                                            and b.remarks = '{2}'", dtpFrom.Value.ToString("yyyy-MM-dd HH:mm"), dtpTo.Value.ToString("yyyy-MM-dd HH:mm"), PendingReason);
            }
            else if (mode == 13)
            {
                query = string.Format(@"select a.Invoice,case when a.Invoice like '%EBILL%' then 'E-Bill' when a.Invoice like '%Urjanet%' then 'Urjanet' else 'Paper bill' end [Invoice Type], 
                                            b.client_code [Client Name], b.usp_name [USP], b.property_code [Property Code], b.vendor_group [Utility], b.ServiceFrom [Service From], b.ServiceTo [Service To], 
                                            b.invoice_date [Invoice Date], b.DueDate [Due Date], b.Amount [Amount Due], b.PreviousBalance [Previous Balance], b.PaymentReceived [Payment Received], 
                                            b.LateFee [Late Fee], b.DisconnectionFee [Disconnection Fee], b.AmountAfterDue [Amount After Due], b.final_bill [Final Bill], b.SpecialInstruction [Notes], b.HasTransferAmount [Has Transfer Amount], 
                                            b.userid [Processed By], b.LastDateModified [Date Processed], b.AuditedBy [Audited By], b.AuditDate [Audit Date], 
                                            b.work_item [Work Item], b.date_bulkuploaded [Date Bulk Uploaded], b.remarks [Work Order Remarks], b.WorkItemIssueDate [WI Issue Date], b.WorkItemStatus [WI Status], 
                                            b.VendorAssignedStatus [Vendor Assignment Remarks], b.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            outer apply
                                            (select top 1 case when x.Complete = 21 then 'Pending for Audit' else x.PendingReasons end PendingReasons2, x.* from tbl_ADHOC_EM_Tasks x where x.Invoice = a.Invoice and x.Complete <> 20 order by x.LastDateModified desc) b
                                            left join tbl_EM_ParameterReference c
                                            on b.PendingReasons2 = c.RefValue and c.REFKEY = 'EMS_Status' 
                                            where a.Complete = 20
                                            and a.LastDateModified between '{0}' and '{1}'
                                            and ISNULL(c.REFDESC, '') in ('Work item creation')
                                            and isnull(b.work_item, '') = '' 
                                            and a.Invoice not like '%Urjanet%' 
                                            and b.dateVendorAssigned is null", dtpFrom.Value.ToString("yyyy-MM-dd HH:mm"), dtpTo.Value.ToString("yyyy-MM-dd HH:mm"), PendingReason);
            }
            else if (mode == 14)
            {
                query = string.Format(@"select a.Invoice,case when a.Invoice like '%EBILL%' then 'E-Bill' when a.Invoice like '%Urjanet%' then 'Urjanet' else 'Paper bill' end [Invoice Type], 
                                            b.client_code [Client Name], b.usp_name [USP], b.property_code [Property Code], b.vendor_group [Utility], b.ServiceFrom [Service From], b.ServiceTo [Service To], 
                                            b.invoice_date [Invoice Date], b.DueDate [Due Date], b.Amount [Amount Due], b.PreviousBalance [Previous Balance], b.PaymentReceived [Payment Received], 
                                            b.LateFee [Late Fee], b.DisconnectionFee [Disconnection Fee], b.AmountAfterDue [Amount After Due], b.final_bill [Final Bill], b.SpecialInstruction [Notes], b.HasTransferAmount [Has Transfer Amount], 
                                            b.userid [Processed By], b.LastDateModified [Date Processed], b.AuditedBy [Audited By], b.AuditDate [Audit Date], 
                                            b.work_item [Work Item], b.date_bulkuploaded [Date Bulk Uploaded], b.remarks [Work Order Remarks], b.WorkItemIssueDate [WI Issue Date], b.WorkItemStatus [WI Status], 
                                            b.VendorAssignedStatus [Vendor Assignment Remarks], b.InvoiceFolderName
                                            from tbl_ADHOC_EM_Tasks a
                                            outer apply
                                            (select top 1 case when x.Complete = 21 then 'Pending for Audit' else x.PendingReasons end PendingReasons2, x.* from tbl_ADHOC_EM_Tasks x where x.Invoice = a.Invoice and x.Complete <> 20 order by x.LastDateModified desc) b
                                            left join tbl_EM_ParameterReference c
                                            on b.PendingReasons2 = c.RefValue and c.REFKEY = 'EMS_Status' 
                                            where a.Complete = 20
                                            and a.LastDateModified between '{0}' and '{1}'
                                            and ISNULL(c.REFDESC, '') in ('Work item creation')
                                            and (case when (isnull(b.work_item, '') <> '' or (b.BulkUploadTransId is not null and isnull(b.work_item, '') = '' and isnull(b.remarks, '') = '')) then 'Contract Order Created Successfully'
                                            when isnull(b.work_item, '') = '' and (isnull(b.VendorAssignedStatus, '') in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned') or b.remarks = 'Vendor ID is inactive/invalid.') then 'Vendor ID tagging issue'
                                            when isnull(b.work_item, '') = '' and isnull(b.VendorAssignedStatus, '') not in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned')
                                            and (b.remarks like '%More than one Line Item is Mapped for the Property at Investor Level%'
                                            or b.remarks like '%Specified line item is already in progress for this property%'
                                            or b.remarks like '%Work item already exists%'
                                            or b.remarks like '%Work order already exists%'
                                            or b.remarks like '%Vendor is in Suspended%'
                                            or b.remarks like '%Account Number is empty%'
                                            or b.remarks like '%Total Charge contains negative value%') then b.remarks
                                            when isnull(b.work_item, '') = '' and isnull(b.VendorAssignedStatus, '') not in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned')
                                            and (b.remarks in ('Work Item cannot be created', 'No vendor ID assigned', 'No client approval', 'Duplicate work item', '; Source not found', 'Zero or Negative Value')) then b.remarks
                                            when isnull(b.work_item, '') = '' and a.Invoice not like '%Urjanet%' and b.dateVendorAssigned is null then 'Pending for VID tagging'
                                            else case when b.remarks = '' then 'Bulk Upload' else ISNULL(b.remarks, 'Bulk Upload')  end end) = 'Bulk Upload'", dtpFrom.Value.ToString("yyyy-MM-dd HH:mm"), dtpTo.Value.ToString("yyyy-MM-dd HH:mm"), PendingReason);
            }
            

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);          

            conn.Close();

            dgList.DataSource = dt;
            dgList.Columns["Invoice"].Visible = false;

            label8.Text = dgList.RowCount.ToString();
        }

        private void LoadSummary()
        {

            if (dgCompleted.Rows.Count > 0)
            {
                dgSummary.Rows.Add(tot_PB + tot_EB + tot_UJ, dgCompleted.Rows[0].Cells[2].Value, 0);
            }
            else
            {
                dgSummary.Rows.Add(tot_PB + tot_EB + tot_UJ, 0, 0);
            }
            
        }

        private void LoadOutflowData()
        {
            string query = string.Format(@"select c.REFDESC, b.PendingReasons2, COUNT(a.Invoice) [Count]
                                            from tbl_ADHOC_EM_Tasks a
                                            outer apply
                                            (select top 1 case when x.Complete = 21 then 'Pending for Audit' else x.PendingReasons end PendingReasons2 from tbl_ADHOC_EM_Tasks x where x.Invoice = a.Invoice and x.Complete <> 20 order by x.LastDateModified desc) b
                                            left join tbl_EM_ParameterReference c
                                            on b.PendingReasons2 = c.RefValue 
                                            and c.REFKEY = 'EMS_Status' 
                                            where a.Complete = 20
                                            and a.LastDateModified between '{0}' and '{1}'
                                            and c.REFDESC in ('Declined Invoices for payment', 'Work item creation', 'Non-workable invoices')
                                            group by rollup(c.REFDESC, b.PendingReasons2)
                                            order by REFDESC, b.PendingReasons2 asc, [Count] desc", dtpFrom.Value.ToString("yyyy-MM-dd HH:mm"), dtpTo.Value.ToString("yyyy-MM-dd HH:mm"));

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].GetType() == typeof(DBNull) && dt.Rows[i][1].GetType() == typeof(DBNull) && i == 0)
                {
                    dgCompleted.Rows.Add("TOTAL", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                }
                else if (dt.Rows[i][1].ToString() != string.Empty && dt.Rows[i][2].ToString() != string.Empty)
                {
                    dgCompleted.Rows.Add("", dt.Rows[i][1], string.Format("{0:N0}", (int)dt.Rows[i][2]));
                }
                else if (dt.Rows[i][0].ToString() != string.Empty && dt.Rows[i][1].GetType() == typeof(DBNull))
                {
                    if (dt.Rows[i][0].ToString() == "Non-workable invoices")
                    {
                        dgCompleted.Rows.Add("Non-workable invoices", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() == "Declined invoices for Payment")
                    {
                        dgCompleted.Rows.Add("Declined invoices for Payment", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() == "Work item creation")
                    {
                        dgCompleted.Rows.Add("Work item creation", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                }
            }

            conn.Close();

            //dgInflow.Rows[dgInflow.Rows.Count - 1].Frozen = true;

        }
        
        private void LoadNewInvoiceData()
        {
            string query = string.Format(@"select REFDESC, PendingReasons, COUNT(Invoice) [Count]
                                            from
                                            (
                                            select c.REFDESC, b.PendingReasons, a.Invoice
                                            from tbl_ADHOC_EM_Tasks a
                                            outer apply
                                            (select top 1 x.isLatestData, case when x.Complete = 21 then 'Pending for Audit' when x.Complete = 20 and x.isLatestData = 1 then x.PendingReasons else x.PendingReasons end PendingReasons, Invoice from tbl_ADHOC_EM_Tasks x where x.Invoice = a.Invoice and x.Complete <> 20 order by x.LastDateModified desc) b
                                            left join tbl_EM_ParameterReference c
                                            on b.PendingReasons = c.RefValue
                                            and c.REFKEY = 'EMS_Status'
                                            where a.Complete = 20
                                            and a.LastDateModified between '{0}' and '{1}'
                                            and c.REFDESC in ('System Error', 'Invoices for Callout', 'Invoices for review', 'Pending for Audit')

                                            union all

                                            select c.REFDESC, a.PendingReasons, a.Invoice
                                            from tbl_ADHOC_EM_Tasks a
                                            left join tbl_EM_ParameterReference c
                                            on a.PendingReasons = c.RefValue
                                            and c.REFKEY = 'EMS_Status'
                                            where a.Complete = 20
                                            and a.isLatestData = 1
                                            ) tbl_src

                                            group by rollup(REFDESC, PendingReasons)
                                            order by  REFDESC asc, PendingReasons asc, [Count] desc", dtpFrom.Value.ToString("yyyy-MM-dd HH:mm"), dtpTo.Value.ToString("yyyy-MM-dd HH:mm"));

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].GetType() == typeof(DBNull) && dt.Rows[i][1].GetType() == typeof(DBNull))
                {
                    dgPending.Rows.Add("TOTAL", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                }
                else if (dt.Rows[i][0].ToString() != string.Empty && dt.Rows[i][1].ToString() != string.Empty)
                {
                    dgPending.Rows.Add("", dt.Rows[i][1], string.Format("{0:N0}", (int)dt.Rows[i][2]));
                }
                else if (dt.Rows[i][0].ToString() != string.Empty && dt.Rows[i][1].GetType() == typeof(DBNull))
                {
                    if (dt.Rows[i][0].ToString() == "Pending for Encoding")
                    {
                        dgPending.Rows.Add("Pending for Encoding", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() == "Invoices for Callout")
                    {
                        dgPending.Rows.Add("Invoices for Callout", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() == "Invoices for review")
                    {
                        dgPending.Rows.Add("Invoices for Review", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() == "Pending for Audit")
                    {
                        dgPending.Rows.Add("Pending for Audit", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                }
            }

            conn.Close();

            //dgInflow.Rows[dgInflow.Rows.Count - 1].Frozen = true;

        }

        private void LoadWIBreakdown()
        {

            string query = string.Format(@"select 
--c.RefKey, b.PendingReasons, b.work_item, b.remarks, b.VendorAssignedStatus, b.remarks2, b.BulkUploadTransId, b.id, b.Invoice
case when (isnull(b.work_item, '') <> '' or (b.BulkUploadTransId is not null and isnull(b.work_item, '') = '' and isnull(b.remarks, '') = '')) then '10'
when (isnull(b.work_item, '') = '' and (isnull(b.VendorAssignedStatus, '') in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned') or b.remarks = 'Vendor ID is inactive/invalid.')
or (isnull(b.work_item, '') = '' and isnull(b.VendorAssignedStatus, '') not in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned')
and (b.remarks like '%More than one Line Item is Mapped for the Property at Investor Level%'
or b.remarks like '%Specified line item is already in progress for this property%'
or b.remarks like '%Work item already exists%'
or b.remarks like '%Work order already exists%'
or b.remarks like '%Vendor is in Suspended%'
or b.remarks like '%Account Number is empty%'
or b.remarks like '%Total Charge contains negative value%'))) then '11'
when isnull(b.work_item, '') = '' and isnull(b.VendorAssignedStatus, '') not in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned')
and (b.remarks in ('Work Item cannot be created', 'No vendor ID assigned', 'No client approval', 'Duplicate work item', '; Source not found', 'Zero or Negative Value')) then '12'
when isnull(b.work_item, '') = '' and a.Invoice not like '%Urjanet%' and b.dateVendorAssigned is null then '13'
else '14' end RefKey, 
case when (isnull(b.work_item, '') <> '' or (b.BulkUploadTransId is not null and isnull(b.work_item, '') = '' and isnull(b.remarks, '') = '')) then 'Contract Order Created Successfully'
when isnull(b.work_item, '') = '' and (isnull(b.VendorAssignedStatus, '') in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned') or b.remarks = 'Vendor ID is inactive/invalid.') then 'Vendor ID tagging issue'
when isnull(b.work_item, '') = '' and isnull(b.VendorAssignedStatus, '') not in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned')
and (b.remarks like '%More than one Line Item is Mapped for the Property at Investor Level%'
or b.remarks like '%Specified line item is already in progress for this property%'
or b.remarks like '%Work item already exists%'
or b.remarks like '%Work order already exists%'
or b.remarks like '%Vendor is in Suspended%'
or b.remarks like '%Account Number is empty%'
or b.remarks like '%Total Charge contains negative value%') then b.remarks
when isnull(b.work_item, '') = '' and isnull(b.VendorAssignedStatus, '') not in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned')
and (b.remarks in ('Work Item cannot be created', 'No vendor ID assigned', 'No client approval', 'Duplicate work item', '; Source not found', 'Zero or Negative Value')) then b.remarks
when isnull(b.work_item, '') = '' and a.Invoice not like '%Urjanet%' and b.dateVendorAssigned is null then 'Pending for VID tagging'
else case when b.remarks = '' then 'Bulk Upload' else ISNULL(b.remarks, 'Bulk Upload')  end end [remarks], COUNT(a.Invoice) [Count]
from tbl_ADHOC_EM_Tasks a
outer apply
(select top 1 x.* from tbl_ADHOC_EM_Tasks x where x.Invoice = a.Invoice and x.isLatestData = 1 order by x.LastDateModified desc) b
left join tbl_EM_ParameterReference c
on b.PendingReasons = c.RefValue 
and c.REFKEY = 'EMS_Status' 
where a.Complete = 20
and a.LastDateModified between '{0}' and '{1}'
and ISNULL(c.REFDESC, '') in ('Work item creation')
group by rollup
(case when (isnull(b.work_item, '') <> '' or (b.BulkUploadTransId is not null and isnull(b.work_item, '') = '' and isnull(b.remarks, '') = '')) then '10'
when (isnull(b.work_item, '') = '' and (isnull(b.VendorAssignedStatus, '') in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned') or b.remarks = 'Vendor ID is inactive/invalid.')
or (isnull(b.work_item, '') = '' and isnull(b.VendorAssignedStatus, '') not in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned')
and (b.remarks like '%More than one Line Item is Mapped for the Property at Investor Level%'
or b.remarks like '%Specified line item is already in progress for this property%'
or b.remarks like '%Work item already exists%'
or b.remarks like '%Work order already exists%'
or b.remarks like '%Vendor is in Suspended%'
or b.remarks like '%Account Number is empty%'
or b.remarks like '%Total Charge contains negative value%'))) then '11'
when isnull(b.work_item, '') = '' and isnull(b.VendorAssignedStatus, '') not in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned')
and (b.remarks in ('Work Item cannot be created', 'No vendor ID assigned', 'No client approval', 'Duplicate work item', '; Source not found', 'Zero or Negative Value')) then '12'
when isnull(b.work_item, '') = '' and a.Invoice not like '%Urjanet%' and b.dateVendorAssigned is null then '13'
else '14' end, 
case when (isnull(b.work_item, '') <> '' or (b.BulkUploadTransId is not null and isnull(b.work_item, '') = '' and isnull(b.remarks, '') = '')) then 'Contract Order Created Successfully'
when isnull(b.work_item, '') = '' and (isnull(b.VendorAssignedStatus, '') in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned') or b.remarks = 'Vendor ID is inactive/invalid.') then 'Vendor ID tagging issue'
when isnull(b.work_item, '') = '' and isnull(b.VendorAssignedStatus, '') not in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned')
and (b.remarks like '%More than one Line Item is Mapped for the Property at Investor Level%'
or b.remarks like '%Specified line item is already in progress for this property%'
or b.remarks like '%Work item already exists%'
or b.remarks like '%Work order already exists%'
or b.remarks like '%Vendor is in Suspended%'
or b.remarks like '%Account Number is empty%'
or b.remarks like '%Total Charge contains negative value%') then b.remarks
when isnull(b.work_item, '') = '' and isnull(b.VendorAssignedStatus, '') not in ('Unable to save the record. USP not found.', 'Vendor ID is inactive/invalid.', 'No vendor ID assigned')
and (b.remarks in ('Work Item cannot be created', 'No vendor ID assigned', 'No client approval', 'Duplicate work item', '; Source not found', 'Zero or Negative Value')) then b.remarks
when isnull(b.work_item, '') = '' and a.Invoice not like '%Urjanet%' and b.dateVendorAssigned is null then 'Pending for VID tagging'
else  case when b.remarks = '' then 'Bulk Upload' else ISNULL(b.remarks, 'Bulk Upload')  end end)
order by RefKey, remarks asc, [Count] desc", dtpFrom.Value.ToString("yyyy-MM-dd HH:mm"), dtpTo.Value.ToString("yyyy-MM-dd HH:mm"));

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].GetType() == typeof(DBNull) && dt.Rows[i][1].GetType() == typeof(DBNull) && i == 0)
                {
                    dgWISummary.Rows.Add("TOTAL", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                }
                else if (dt.Rows[i][0].ToString() != string.Empty && dt.Rows[i][1].ToString() != string.Empty)
                {
                    dgWISummary.Rows.Add("", dt.Rows[i][1], string.Format("{0:N0}", (int)dt.Rows[i][2]));
                }
                else if (dt.Rows[i][0].ToString() != string.Empty && dt.Rows[i][1].GetType() == typeof(DBNull))
                {
                    if (dt.Rows[i][0].ToString() == "10")
                    {
                        dgWISummary.Rows.Add("Work items created", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() == "11")
                    {   
                        dgWISummary.Rows.Add("Invoices stopped by VMS", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() == "12")
                    {
                        dgWISummary.Rows.Add("Work item creation error", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() == "13")
                    {
                        dgWISummary.Rows.Add("Pending for VID tagging", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() == "14")
                    {
                        dgWISummary.Rows.Add("Pending for work item creation", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                }
            }

            conn.Close();
        }

        private void LoadInflowData()
        {


            string query = string.Format(@"select dtRecvd, ISNULL([Paper bill],0) [Paper bill], ISNULL([E-Bill],0) [E-Bill], ISNULL([Urjanet],0) [Urjanet], ISNULL([Mailbox],0) [Maibox] from
                                            (
                                            select case when Invoice like '%EBILL%' then 'E-Bill' when Invoice like '%Urjanet%' then 'Urjanet' when (SourceInvoicePath like '\\corp.ocwen.com\data\Bangalore\CommonShare\ExpenseManagement\Expenses\Mailbox\PDF\%' 
                                            or SourceInvoicePath like '\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\Mailbox\PDF\%') then 'Mailbox' else 'Paper bill' end [Invoice], CAST(DATEADD(HH, -12, LastDateModified) as date) [dtRecvd], COUNT(*) [Count] 
                                            from tbl_ADHOC_EM_Tasks
                                            where Complete = 20
                                            and (CAST(DATEADD(HH, -12, LastDateModified) AS DATE)) between (CAST(DATEADD(HH, -12, '{0}') AS DATE)) and (CAST(DATEADD(HH, -12, '{1}') AS DATE))
                                            group by case when Invoice like '%EBILL%' then 'E-Bill' when Invoice like '%Urjanet%' then 'Urjanet' when (SourceInvoicePath like '\\corp.ocwen.com\data\Bangalore\CommonShare\ExpenseManagement\Expenses\Mailbox\PDF\%' 
                                            or SourceInvoicePath like '\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\Mailbox\PDF\%') then 'Mailbox' else 'Paper bill' end, CAST(DATEADD(HH, -12, LastDateModified) AS DATE)
                                            ) tbl_Inflow
                                            pivot
                                            (
                                            sum([Count])
                                            for Invoice
                                            in ([Paper bill],[E-Bill],[Urjanet],[Mailbox])
                                            ) tbl_pvtInflow
                                            order by dtRecvd", dtpFrom.Value.ToString("yyyy-MM-dd HH:mm"), dtpTo.Value.ToString("yyyy-MM-dd HH:mm"));

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);

            tot_PB = 0;
            tot_EB = 0;
            tot_UJ = 0;
            tot_MB = 0;

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                dgInflow.Rows.Add(Convert.ToDateTime(dt.Rows[i][0]).ToString("yyyy-MM-dd"), dt.Rows[i][1].ToString(), dt.Rows[i][2].ToString(), dt.Rows[i][3].ToString(), dt.Rows[i][4].ToString(),
                                  Convert.ToInt32(dt.Rows[i][1]) + Convert.ToInt32(dt.Rows[i][2]) + Convert.ToInt32(dt.Rows[i][3]) + Convert.ToInt32(dt.Rows[i][4])); 

                tot_PB += Convert.ToInt32(dt.Rows[i][1]);
                tot_EB += Convert.ToInt32(dt.Rows[i][2]);
                tot_UJ += Convert.ToInt32(dt.Rows[i][3]);
                tot_MB += Convert.ToInt32(dt.Rows[i][4]);
            }

            dgInflow.Rows.Add("Total", tot_PB.ToString(), tot_EB.ToString(), tot_UJ.ToString(), tot_MB.ToString(), tot_PB + tot_EB + tot_UJ + tot_MB);

            conn.Close();

            //dgInflow.Rows[dgInflow.Rows.Count - 1].Frozen = true;
        }

        private void dgCompleted_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((dgCompleted.CurrentCell.ColumnIndex == 1 || dgCompleted.CurrentCell.ColumnIndex == 2) && dgCompleted.CurrentCell.Value.ToString() != string.Empty)
            {
                LoadList(dgCompleted.Rows[dgCompleted.CurrentCell.RowIndex].Cells[1].Value.ToString(), 0);
            }
        }

        private void dgPending_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((dgPending.CurrentCell.ColumnIndex == 1 || dgPending.CurrentCell.ColumnIndex == 2) && dgPending.CurrentCell.Value.ToString() != string.Empty)
            {
                LoadList(dgPending.Rows[dgPending.CurrentCell.RowIndex].Cells[1].Value.ToString(), 1);
            }
        }

        private void dgList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgList.CurrentCell.ColumnIndex == 0 || dgList.Columns[dgList.CurrentCell.ColumnIndex].Name == "InvoiceFoldername")
            {

                string pdfpath = dgList.CurrentRow.Cells["InvoiceFoldername"].Value.ToString();

                if (System.IO.File.Exists(pdfpath))
                {
                    panel1.Visible = true;
                    Uri myUri = new Uri(pdfpath);
                    webBrowser1.Navigate(myUri);
                    webBrowser1.Refresh();  
                }
                else
                {
                    panel1.Visible = false;
                    MessageBox.Show("File not found");
                }
            }
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            panel1.Visible = false;
        }

        private void frmNewFunnel_Load(object sender, EventArgs e)
        {
            //dtpFrom.Value = Convert.ToDateTime(TimeZoneInfo.ConvertTime(DateTime.Now.Date, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time")) + " 20:00");
        }

        private void dgWISummary_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if ((dgWISummary.CurrentCell.ColumnIndex == 1 || dgWISummary.CurrentCell.ColumnIndex == 2) && dgWISummary.CurrentCell.Value.ToString() != string.Empty)
            {
                if (dgWISummary.Rows[dgWISummary.CurrentCell.RowIndex].Cells[1].Value.ToString() == "Contract Order Created Successfully")
                {
                    LoadList(dgWISummary.Rows[dgWISummary.CurrentCell.RowIndex].Cells[1].Value.ToString(), 10);
                }
                else if (dgWISummary.Rows[dgWISummary.CurrentCell.RowIndex].Cells[1].Value.ToString() == "Vendor ID tagging issue")
                {
                    LoadList(dgWISummary.Rows[dgWISummary.CurrentCell.RowIndex].Cells[1].Value.ToString(), 11);
                }
                else if (dgWISummary.Rows[dgWISummary.CurrentCell.RowIndex].Cells[1].Value.ToString() == "Pending for VID tagging")
                {
                    LoadList(dgWISummary.Rows[dgWISummary.CurrentCell.RowIndex].Cells[1].Value.ToString(), 13);
                }
                else if (dgWISummary.Rows[dgWISummary.CurrentCell.RowIndex].Cells[1].Value.ToString() == "Bulk Upload")
                {
                    LoadList(dgWISummary.Rows[dgWISummary.CurrentCell.RowIndex].Cells[1].Value.ToString(), 14);
                }
                else
                {
                    LoadList(dgWISummary.Rows[dgWISummary.CurrentCell.RowIndex].Cells[1].Value.ToString(), 12);
                }

            }
        }
    }
}
