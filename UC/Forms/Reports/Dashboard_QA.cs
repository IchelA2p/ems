﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms.Reports
{
    public partial class Dashboard_QA : Form
    {
        DateTime start, end;
        string userid, auditor, error;

        public Dashboard_QA()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            start = dateTimePicker1.Value;
            dateTimePicker2.Value = dateTimePicker1.Value.AddHours(9);
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            end = dateTimePicker2.Value;

            LoadUsers();
            cmbUsers.SelectedIndex = -1;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dgView.DataSource = null;
            dgRaw.DataSource = null;
            cmbAuditors.DataSource = null;

            BackgroundWorker bwLoadDashboard = new BackgroundWorker();
            bwLoadDashboard.DoWork += new DoWorkEventHandler(bwLoadDashboard_DoWork);
            bwLoadDashboard.RunWorkerAsync();

            LoadRawData();
        }

        void bwLoadDashboard_DoWork(object sender, DoWorkEventArgs e)
        {
            List<string> errors = new List<string>();

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

           string query = string.Format(@"EXEC sp_EMS_AuditDashboard_temp '{0}', '{1}', 3, '{2}', '{3}', ''", start, end, userid, auditor); 

            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            SqlCommandBuilder cb = new SqlCommandBuilder(da);

            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            da.Fill(ds);

            cb = null;
            da = null;
            conn.Close();

           
            dgView.Invoke((Action)delegate
            {
                dgView.DataSource = ds.Tables[0];

                foreach (DataGridViewColumn dc in dgView.Columns)
                {
                    foreach (DataGridViewRow dr in dgView.Rows)
                    {
                        try
                        {
                            if (dgView[dc.Index, dr.Index].Value.ToString() != "0" && dc.Index != 0)
                            {
                                if (Convert.ToInt32(dgView[dc.Index, dr.Index].Value.ToString()) < 5)
                                {
                                    dgView[dc.Index, dr.Index].Style.BackColor = Color.Yellow;
                                }
                                else
                                {
                                    dgView[dc.Index, dr.Index].Style.BackColor = Color.Red;
                                }
                            }
                        }
                        catch
                        { }
                    }
                }
            });
        }

        private void LoadRawData()
        {
            string query = string.Format(@"EXEC sp_EMS_AuditDashboard_temp '{0}', '{1}', 4, '{2}', '{3}', '{4}'", start, end, cmbUsers.Text, cmbAuditors.Text, cmbErrors.Text); 

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            //DataTable dt_raw = new DataTable();
            DataSet ds = new DataSet();
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(ds);            

            conn.Close();

            if (ds.Tables.Count > 0)
            {
                dgRaw.DataSource = ds.Tables[0];

                dgRaw.Columns["InvoiceFolderName"].Visible = false;
                dgRaw.Columns["Invoice"].Visible = false;

                cmbUsers.DataSource = ds.Tables[1];
                cmbUsers.DisplayMember = "UserID";

                cmbAuditors.DataSource = ds.Tables[2];
                cmbAuditors.DisplayMember = "AuditedBy";

                cmbErrors.DataSource = ds.Tables[3];
                cmbErrors.DisplayMember = "result";
                
            }
        }


        private void LoadUsers()
        {
            string query = string.Format(@"EXEC sp_EMS_AuditDashboard '{0}', '{1}', 5", start, end);

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);

            conn.Close();

            if (dt.Rows.Count > 0)
            {
                cmbUsers.DataSource = dt;
                cmbUsers.DisplayMember = "UserID";
            }
            else
            {
                cmbUsers.DataSource = null;
                cmbUsers.Refresh();
            }
        }

        private void dgRaw_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgRaw.CurrentCell.ColumnIndex == 0)
            {

                string pdfpath = dgRaw.CurrentRow.Cells["InvoiceFoldername"].Value.ToString();

                if (System.IO.File.Exists(pdfpath))
                {   
                    Uri myUri = new Uri(pdfpath);
                    wbPDF.Navigate(myUri);
                    wbPDF.Refresh();

                    wbPDF.BringToFront();
                    dgRaw.SendToBack();
                    btnClose.BringToFront();
                }
                else
                {
                    //panel1.Visible = false;
                    MessageBox.Show("File not found");
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            wbPDF.SendToBack();
            btnClose.SendToBack();
            dgRaw.BringToFront();
        }

        private void cmbUsers_SelectedIndexChanged(object sender, EventArgs e)
        {
            userid = cmbUsers.Text;
        }

        private void cmbAuditors_SelectedIndexChanged(object sender, EventArgs e)
        {
            auditor = cmbAuditors.Text;
        }

        private void cmbErrors_SelectedIndexChanged(object sender, EventArgs e)
        {
            error = cmbErrors.Text;
        }

        private void btnFilter_Click(object sender, EventArgs e)
        {
            LoadRawData();
        }
    }
}
