﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms
{
    public partial class Dashboard : Form
    {
        public List<Database.DB.EM_Tasks> processed;
        public List<Database.DB.EM_Tasks> audits;
        //public List<Database.DB.Employee_Info> employeeinfo;
        public List<string> employeePrimaryIdList;

        public Dashboard()
        {
            InitializeComponent();

            processed = new List<Database.DB.EM_Tasks>();
            employeePrimaryIdList = new List<string>();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dateTimePicker2.Value > dateTimePicker1.Value)
            {
                GetProductivity();

                GetData();
                GetAudits();
                CreateSummaryReport();
                //////CreateAgentProd();
                //////CreateDetailedReport();
                
                //////CreateSubcategoryReport(1, dataGridView4);
                //////CreateSubcategoryReport(12, dataGridView5);
                //////CreateSubcategoryReport(0, dataGridView6);
                //////CreateSubcategoryReport(9, dataGridView7);
            }
            else
            { 
                foreach (Control c in this.Controls)
                {
                    if (c.GetType() == typeof(DataGridView))
                    {
                        ((DataGridView)c).Rows.Clear();
                    }
                }
                MessageBox.Show("No data to show.");
            }
        }

        private void GetProductivity()
        {
            string query = string.Format(@"declare @dtfrom as datetime = '{0}' -- change
                                           declare @dtto as datetime = '{1}' -- change

                                            select userid, [Data Entry],[Callout],[Audit],[Duplicate Review (1st level)],[Duplicate Review (2nd level)],[High Amount Invoice Review],[Trailing Asset Review (> 150 days)],[Trailing Asset Review (> 90 days)],[Trailing Asset Review (> 45 days)],[Inactive Property Review],[Occupied Property Review],[POD OMS Review], [OMS Review (> 20 days)],[OMS Review (> 45 days)],[Pre-work item Review],
                                            [Data Entry]+ [Audit]+ [Callout]+ [Duplicate Review (1st level)] + [Duplicate Review (2nd level)]+ [High Amount Invoice Review] + [Trailing Asset Review (> 150 days)] + [Trailing Asset Review (> 90 days)] + [Trailing Asset Review (> 45 days)] + [Inactive Property Review] + [Occupied Property Review] + [POD OMS Review] +  [OMS Review (> 20 days)] + [OMS Review (> 45 days)] + [Pre-work item Review] [Total]
                                            from 
                                            (
                                            select userid, [Process] from fn_EM_Prod(@dtfrom, @dtto)
                                            ) tbl_src
                                            pivot
                                            (count(Process)
                                            for Process in (
                                            [Data Entry],[Callout],[Audit],[Duplicate Review (1st level)],[Duplicate Review (2nd level)],[High Amount Invoice Review],[Trailing Asset Review (> 150 days)],[Trailing Asset Review (> 90 days)],[Trailing Asset Review (> 45 days)],[Inactive Property Review],[Occupied Property Review],[POD OMS Review],[OMS Review (> 20 days)],[OMS Review (> 45 days)],[Pre-work item Review]
                                            )
                                            ) tbl_pvt",
                                              dateTimePicker1.Value,
                                              dateTimePicker2.Value);

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();
            
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            da.SelectCommand.CommandTimeout = 60000;

            da.Fill(dt);

            dgProd.DataSource = dt;

            conn.Close();
        }


        private void GetData()
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            //Database.DBConnection.DatabaseEmployee_Info conn1 = new Database.DBConnection.DatabaseEmployee_Info();
            List<Database.DB.EM_Tasks> cleanProcessed = new List<Database.DB.EM_Tasks>();

            try
            {
                processed = conn.GetDataForDates(dateTimePicker1.Value, dateTimePicker2.Value);
                //employeeinfo = conn1.GetAllData();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to retrieve data. Error: " + ex.Message);
            }

            /* Update 5/5/2015 - Inclusion of isLatestData = 1
            List<string> invoiceList = new List<string>();
            if (processed != null)
            {
                foreach (Database.DB.EM_Tasks proc in processed)
                {
                    if (!invoiceList.Contains(proc.Invoice))
                    {
                        invoiceList.Add(proc.Invoice);
                    }
                }
            }
            if (invoiceList != null)
            {
                foreach (string invList in invoiceList)
                {
                    if (processed.Count(p => p.Invoice == invList) > 1)
                    {
                        List<Database.DB.EM_Tasks> duplicates = new List<Database.DB.EM_Tasks>();
                        duplicates.AddRange(processed.FindAll(p => p.Invoice == invList));

                        for (int i = 0; i < duplicates.Count() - 1; i++)
                        {
                            if (duplicates[i].LastDateModified < duplicates[i + 1].LastDateModified)
                            {
                                processed.Remove(duplicates[i]);
                            }
                            else
                            {
                                processed.Remove(duplicates[i + 1]);
                            }
                        }
                    }
                }
            }*/
        }


        private void GetAudits()
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            List<Database.DB.EM_Tasks> cleanProcessed = new List<Database.DB.EM_Tasks>();

            try
            {
                audits = conn.GetAuditsForDates(dateTimePicker1.Value, dateTimePicker2.Value);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to retrieve data. Error: " + ex.Message);
            }
        }

        public void CreateSummaryReport()
        {
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += new DoWorkEventHandler(bw_DoWork);
            bw.RunWorkerAsync();
        }

        void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);
            conn.Open();

            string query = string.Format(@"select COUNT(a.id) from tbl_ADHOC_EM_Tasks a
                                            where a.isLatestData = 1
                                            and a.Complete = 20 
                                            --and a.PendingReasons = 'New Invoice'");

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            int backlog = Convert.ToInt32(rd[0]);
            int pending = processed.Count(p => p.Complete == 20);
            int totalproc = processed.Count() - pending;
            int complete = processed.Count(p => p.Complete == 1) +
                processed.Count(p => p.Complete == 12);
            int pnf = processed.Count(p => p.Complete == 3);
            int callout = processed.Count(p => p.Complete == 0);
            int vir = processed.Count(p => p.Complete == 9);
            int zeroornegative = processed.Count(p => p.PendingReasons == "Zero or Negative Value");
            int notutilitybill = processed.Count(p => p.Complete == 6);
            float hours = processed.Sum(p => p.Duration) / 3600;
            float pph = processed.Count(p => p.Complete != 21 && p.Complete != 20) / (float)hours;
            float cph = processed.Count(p => (p.Complete == 1 || p.Complete == 12)) / (float)hours;

            dataGridView1.Invoke((Action)delegate {
                dataGridView1.Rows.Clear();
                dataGridView1.Rows.Add(backlog, pending, totalproc,
                    Math.Round(((double)complete / (double)totalproc * 100), 0) + @"%",
                    Math.Round(((double)pnf / (double)totalproc * 100), 0) + @"%",
                    Math.Round(((double)callout / (double)totalproc * 100), 0) + @"%",
                    Math.Round(((double)vir / (double)totalproc * 100), 0) + @"%",
                    Math.Round(((double)notutilitybill / (double)totalproc * 100), 0) + @"%",
                    Math.Round(pph, 0),
                    Math.Round(cph, 0));
            });
        }

        public void CreateAgentProd()
        {
            dataGridView3.Rows.Clear();

            foreach (Database.DB.EM_Tasks prod in processed)
            {
                if (!employeePrimaryIdList.Contains(prod.userid))
                {
                    if (prod.userid != "auto")
                    {
                        employeePrimaryIdList.Add(prod.userid);
                    }
                }
            }

            foreach (string id in employeePrimaryIdList)
            {
                //string userid = employeeinfo.Find(p => p.userId == id).userId;
                string userid = id;

                List<Database.DB.EM_Tasks> taskOfEmployee = processed.FindAll(p => p.userid == userid && p.Complete != 20);

                int prodCount = taskOfEmployee.Count();

                int complete = taskOfEmployee.Count(p => p.Complete == 1) + taskOfEmployee.Count(p => p.Complete == 12);
                decimal completerate = 0;

                float hours = taskOfEmployee.Sum(p => p.Duration)/3600;
                double processedperhour = Math.Round((float)prodCount / (float)hours,0);
                double completeperhour = Math.Round((float)complete / (float)hours, 0);

                try
                {
                    completerate = Math.Round(((decimal)complete / (decimal)prodCount) * 100, 0);
                }
                catch
                {
                    completerate = 0;
                }

                string completionrate = completerate.ToString() + @"%";

                dataGridView3.Rows.Add(userid, prodCount, Math.Round(hours, 1), processedperhour, completeperhour, completionrate);
            }
        }

        public void CreateDetailedReport()
        {
            dataGridView2.Rows.Clear();

            foreach (string employee in employeePrimaryIdList)
            {
                List<Database.DB.EM_Tasks> taskOfEmployee = new List<Database.DB.EM_Tasks>();
                taskOfEmployee.AddRange(processed.FindAll(p => p.userid == employee));

                //string userid = employeeinfo.Find(p => p.userId == employee).userId;
                string userid = employee;
                double complete = taskOfEmployee.Count(p => p.Complete == 1);
                double notutilitybuill = taskOfEmployee.Count(p => p.Complete == 6);
                double callout = taskOfEmployee.Count(p => p.Complete == 0);
                double propertynotfound = taskOfEmployee.Count(p => p.Complete == 3);
                double vendoridrequest = taskOfEmployee.Count(p => p.Complete == 9);
                double manualwicreated = taskOfEmployee.Count(p => p.Complete == 12);

                dataGridView2.Rows.Add(userid, callout, complete, manualwicreated, 
                    notutilitybuill, propertynotfound, vendoridrequest);
            }

            dataGridView2.Rows.Add("Total",
                processed.Count(p => p.Complete == 0),
                processed.Count(p => p.Complete == 1),
                processed.Count(p => p.Complete == 12),
                processed.Count(p => p.Complete == 6),
                processed.Count(p => p.Complete == 3),
                processed.Count(p => p.Complete == 9));
        }

        public void CreateSubcategoryReport(int status, DataGridView dgv)
        {
            dgv.Rows.Clear();
            List<string> pendingreasons = new List<string>();

            foreach (Database.DB.EM_Tasks prod in processed)
            {
                if (prod.Complete == status)
                {
                    if (!pendingreasons.Contains(prod.PendingReasons))
                    {
                        pendingreasons.Add(prod.PendingReasons);
                    }
                }
            }

            foreach (string reasons in pendingreasons)
            {
                string strReason = reasons;

                List<Database.DB.EM_Tasks> CompleteList = new List<Database.DB.EM_Tasks>();

                CompleteList.AddRange(processed.FindAll(p => p.Complete == status));

                int intCount = CompleteList.Count(p => p.PendingReasons == reasons);

                dgv.Rows.Add(strReason, intCount);
            }
        }


    }
}
