﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.DirectoryServices.AccountManagement;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Forms;


namespace UC
{
    public partial class Form1 : Form
    {

        public bool isLoggedIn = false;
        public int numberOfInvoices = 0;

        Timer timer;
        int counter;
        bool isOutOfREO = false;
        bool isTrailingAsset = false;
        bool isTimerInitialized = false;
        bool isBalTransfer = false;
        public string SourceFolderPathName;
        public string CurrentLoadedInvoice;
        public string frmText;
        public string ref_vidreq;
        int invoicecount = 0;
        string ini_invoice = string.Empty;
        string ini_vendor_group = string.Empty;
        string ini_vendorid = string.Empty;
        string ini_invdate = string.Empty;
        string ini_duedate = string.Empty;
        decimal ini_InvAmount = 0;
        int queue_code = 100;
        
        //Network Path
        public string CompleteFolder = @"\\ascorp.com\data\Bangalore\CommonShare\Strategic$\Utility Invoices\EM_TOOL_BulkUpload\";
        public string CallOutFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\CALLOUT\";
        public string SourceFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\SOURCE\";
        public string TempFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\ONGOING\";
        public string TrailingAssetFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\TRAILINGASSET\";
        public string PropertyNotFoundFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\PROPERTYNOTFOUND\";
        public string VendorIdRequestFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\VENDORIDREQUEST\";
        public string DoneFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\DONE\";
        public string NoActionNeededFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\NOACTIONNEEDED\";
        public string PendingActionFromUCFolder = @"\\ascorp.com\data\Bangalore\CommonShare\Strategic$\Utility Invoices\EM_TOOL_BulkUpload\"; //Update 07/30/14 - Moved to bulk upload
        public string ForwardToUCFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\FORWARDTOUC\";
        public string ResiFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\RESI\";
        public string InactiveFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\INACTIVE\";
        public string ResiCompleteFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\RESICOMPLETE\";
        public string ManualWIFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\ManualWI\";
        public string TestFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\TEST\";
        public string RSCFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\RSCAPPROVAL\";
        public string UnreadableInvoiceFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\Unreadable_Invoices\";
        public string EscalationsFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\ESCALATIONS\";
        public string AttachmentsFolder = @"\\PIV8FSASNP01\CommonShare\EMProject\ATTACHMENTS\";
        
        // E-Bills folders
        public string EBills_NEW = @"\\PIV8FSASNP01\CommonShare\EMProject\EBILLS\NEW\";
        public string EBills_PROCESSEDBILLS = @"\\PIV8FSASNP01\CommonShare\EMProject\EBILLS\PROCESSEDBILLS\";
        public string EBills_NONBILLS = @"\\PIV8FSASNP01\CommonShare\EMProject\EBILLS\NONBILLS\";

        // RR folders
        public string RR_NEW = @"\\PIV8FSASNP01\CommonShare\EMProject\RR\NEW\";
        public string RR_PENDING = @"\\PIV8FSASNP01\CommonShare\EMProject\RR\PENDING\";
        public string RR_DONE = @"\\PIV8FSASNP01\CommonShare\EMProject\RR\DONE\";

        // HSBC folders
        public string HSBC_NEW = @"\\PIV8FSASNP01\CommonShare\EMProject\HSBC\NEW\";
        public string HSBC_PENDING = @"\\PIV8FSASNP01\CommonShare\EMProject\HSBC\PENDING\";
        public string HSBC_DONE = @"\\PIV8FSASNP01\CommonShare\EMProject\HSBC\DONE\";

        //Prioritization Key
        private bool hasDoneHouseNoZipSearch = false;
        private bool hasDoneHouseNoCitySearch = false;
        private bool hasDoneHouseNoStreetSearch = false;
        private bool hasDoneHouseNoStateSearch = false;
        private bool hasDoneAcctSearch = false;

        private bool HouseNoZipSearchHasResults = false;
        private bool HouseNoCitySearcHasResults = false;
        private bool HouseNoStreetSearchHasResults = false;
        private bool HouseNoStateSearchHasResults = false;

        private bool hasZipcodeOnInvoice = false;
        private bool acctSearchHasResults = false;
        private bool isSearchHasResults = false;
        private bool uspSearchHasResults = false;

        private bool hasMultiplePropertyOnInvoice = false;
        private int multiplePropertyCount = 0;
        private bool isTestingOnly = false;
        private bool isEscalQueue = false;

        Database.DB.ZipcodeStateCity listOfZipcodeStateCity;

        private bool uspHasNoMatch = false;
        private string VendorIDRequestType = string.Empty;    //Activation or CreateNew
        private bool isInactive = false;
        public bool saveVDR = false;

        public List<PaymentClass> paymentBreakdown;
        public List<Database.DB.VDR> listOfVDR;
        double depositAmount = 0;
        double depositRefund = 0;

        bool isWebControlDocumentCompleted = false;

        BackgroundWorker bwRscApproval;

        public DateTime Property_Status_Change_Date;
        public DateTime Inactive_Date;

        public string tempFile = @"\\PIV8FSASNP01\CommonShare\EMProject\tempFile.pdf";
        public string HiddenTempFile = string.Empty;

        public Database.DB.EM_Tasks info;

        int numOftries = 0;
        int numOfSearch = 0;
        bool isDoneSearch = false;
        string iniComplete = string.Empty;
        string iniPendingReason = string.Empty;
        string iniLastModDate = string.Empty;

        public Database.DB.EM_Tasks data = new Database.DB.EM_Tasks();

        public Form1()
        {
            InitializeComponent();

            Property_Status_Change_Date = new DateTime();
            Inactive_Date = new DateTime();

            listOfVDR = new List<Database.DB.VDR>();

            BackgroundWorker bwGetVdrList = new BackgroundWorker();
            bwGetVdrList.DoWork += new DoWorkEventHandler(bwGetVdrList_DoWork);
            bwGetVdrList.RunWorkerAsync();

            bwRscApproval = new BackgroundWorker();
            bwRscApproval.DoWork += new DoWorkEventHandler(bwRscApproval_DoWork);

            BackgroundWorker bwLoadUSP = new BackgroundWorker();
            bwLoadUSP.DoWork += new DoWorkEventHandler(bwLoadUSP_DoWork);
            bwLoadUSP.RunWorkerAsync();

            pdfviewer.DocumentCompleted += new WebBrowserDocumentCompletedEventHandler(pdfviewer_DocumentCompleted);

            CreateHiddenTempFile();

        }

        void bwLoadUSP_DoWork(object sender, DoWorkEventArgs e)
        {
            preloadUSP();
        }

        void pdfviewer_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            isWebControlDocumentCompleted = true;
        }

        public void CreateHiddenTempFile()
        {
            HiddenTempFile = Path.Combine(Path.GetTempPath(), "temp.pdf");

            if (!File.Exists(HiddenTempFile))
            {
                File.Copy(tempFile, HiddenTempFile);    
            }
        }

        private void bwGetVdrList_DoWork(object sender, DoWorkEventArgs e)
        {
            Database.DBConnection.DatabaseVDR_Data conn = new Database.DBConnection.DatabaseVDR_Data();
            listOfVDR = conn.GetAllData();

            comboBox5.Invoke((Action)delegate
            {
                comboBox5.Items.Clear();
                comboBox5.Items.Add("--------------------------------------------");
                comboBox5.Items.Add("No Matching Biller Name");
                comboBox5.Items.Add("--------------------------------------------");
            });

            if (listOfVDR != null)
            {
                foreach (Database.DB.VDR data in listOfVDR)
                {
                    comboBox5.Invoke((Action)delegate 
                    {
                        if (!comboBox5.Items.Contains(data.Vendor_Name))
                        {
                            comboBox5.Items.Add(data.Vendor_Name.Replace("'", ""));
                        }
                    });
                }

                comboBox5.Invoke((Action)delegate
                {
                    comboBox5.Items.Add("--------------------------------------------");
                    comboBox5.Items.Add("No Matching Biller Name");
                    comboBox5.Items.Add("--------------------------------------------");

                    //comboBox5.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
                    //comboBox5.AutoCompleteSource = AutoCompleteSource.ListItems;
                });
            }
        }

        public void Login()
        {
            Database.DBConnection.DatabaseAssignQueue conn = new Database.DBConnection.DatabaseAssignQueue();
            Database.DB.AssignQueue queue_info = new Database.DB.AssignQueue();
            string username = Environment.UserName.ToLower();

            if (!isTimerInitialized)
            {
                InitializeTimer();
            }

            menuStrip1.Visible = true;

            queue_info = conn.GetDataByUserid(username);

            if (queue_info == null)
            {
                conn.WriteData(new Database.DB.AssignQueue()
                {
                    userid = username,
                    assignee = "auto",
                    current_queue = "New Invoice",
                    unchangeable = 0
                });
            }
            else
            {
                if (queue_info.unchangeable == 0)
                {
                    //queue_info.current_queue = "New Invoice";
                    queue_info.assignee = "auto";
                    conn.UpdateData(queue_info);
                }
            }

            //var load = Program.CreateLoadingScreen("Logging in...");
            //try
            //{
            //    
            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show(ex.Message);
            //}
            //finally
            //{
            //    load.Abort();    
            //}

            LoadInvoice();

            //BackgroundWorker worker1 = new BackgroundWorker();
            //worker1.DoWork += new DoWorkEventHandler(BW_LoadTasksCompleted_DoWork);
            //worker1.RunWorkerAsync();
        }


        public void LoadBillName()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);
            conn.Open();

            string query = @"SELECT distinct RTRIM(BillName) BillName FROM [MIS_ALTI].[dbo].[tbl_EM_BillName] ORDER BY [BillName]";

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            if (cmbBillName.InvokeRequired)
            {
                cmbBillName.Invoke((Action)delegate
                {
                    cmbBillName.Items.Clear();
                    cmbBillName.Items.Add("---- Not Listed ----");

                    try
                    {

                        while (rd.Read())
                        {
                            cmbBillName.Items.Add(rd[0].ToString());
                        }

                        cmbBillName.Items.Add("---- Not Listed ----");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Unable to pull up list of USP. Error: " + ex.Message);
                    }
                });
            }
            else
            {
                cmbBillName.Items.Clear();
                cmbBillName.Items.Add("---- Not Listed ----");

                try
                {
                    while (rd.Read())
                    {
                        cmbBillName.Items.Add(rd[0].ToString());
                    }

                    cmbBillName.Items.Add("---- Not Listed ----");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to pull up list. Error: " + ex.Message);
                }
            }

            conn.Close();

        }


        public void preloadUSP()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);
            conn.Open();

            string query = @"SELECT RTRIM(USPGroupName) [USP Name] FROM [MIS_ALTI].[dbo].[tbl_UC_USPGroup] ORDER BY [USP Name]";

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            if (cmboBx_UspOnInvoice.InvokeRequired)
            {
                cmboBx_UspOnInvoice.Invoke((Action)delegate
                {
                    cmboBx_UspOnInvoice.Items.Clear();
                    cmboBx_UspOnInvoice.Items.Add("----------------------------");
                    cmboBx_UspOnInvoice.Items.Add("USP Not Listed");
                    cmboBx_UspOnInvoice.Items.Add("----------------------------");

                    try
                    {
                        //foreach (Database.DB.USPGroupSimple data in conn.GetAllData())
                        //{
                        //    cmboBx_UspOnInvoice.Items.Add(data.usp_name);
                        //}

                        while (rd.Read())
                        {
                            cmboBx_UspOnInvoice.Items.Add(rd[0].ToString());
                        }

                        cmboBx_UspOnInvoice.Items.Add("----------------------------");
                        cmboBx_UspOnInvoice.Items.Add("USP Not Listed");
                        cmboBx_UspOnInvoice.Items.Add("----------------------------");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Unable to pull up list of USP. Error: " + ex.Message);
                    }
                });
            }
            else
            {
                cmboBx_UspOnInvoice.Items.Clear();
                cmboBx_UspOnInvoice.Items.Add("----------------------------");
                cmboBx_UspOnInvoice.Items.Add("USP Not Listed");
                cmboBx_UspOnInvoice.Items.Add("----------------------------");

                try
                {
                    //foreach (Database.DB.USPGroupSimple data in conn.GetAllData())
                    //{
                    //    cmboBx_UspOnInvoice.Items.Add(data.usp_name);
                    //}

                    while (rd.Read())
                    {
                        cmboBx_UspOnInvoice.Items.Add(rd[0].ToString());
                    }

                    cmboBx_UspOnInvoice.Items.Add("----------------------------");
                    cmboBx_UspOnInvoice.Items.Add("USP Not Listed");
                    cmboBx_UspOnInvoice.Items.Add("----------------------------");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to pull up list of USP. Error: " + ex.Message);
                }
            }

            conn.Close();
        }

        public void InitializeTimer()
        {
            isTimerInitialized = true;
            timer = new Timer();
            timer.Tick += new EventHandler(timer_Tick);
            timer.Interval = 1000;
            timer.Enabled = true;
            timer.Start();

            this.counter = 0;
        }

        public void timer_Tick(object sender, EventArgs e)
        {
            this.counter = counter + 1;
            label17.Text = counter.ToString();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                Login();

                // Enables the functions on Top Menu bar
                EnableAdminTopMenuOptions();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }        

        private void EnableAdminTopMenuOptions()
        {
            List<string> adminUsers = new List<string>();

            SqlConnection sqlconnection = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a");
            sqlconnection.Open();
            SqlCommand cmd = new SqlCommand(@"SELECT role FROM [MIS_ALTI].[dbo].[tbl_EM_Admin] where userid = '" + Environment.UserName + "'", sqlconnection);
            SqlDataReader dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                adminUsers.Add(dr[0].ToString().ToLower());
            }

            sqlconnection.Close();

            if (adminUsers.Count > 0)
            {

                if (adminUsers[0].ToString().ToLower() == "assoc" || adminUsers[0].ToString().ToLower() == string.Empty)
                {
                    functionsToolStripMenuItem.Visible = false;

                    auditToolStripMenuItem.Visible = true;
                    auditFormToolStripMenuItem.Visible = true;
                    duplicateApprovalFormToolStripMenuItem.Visible = true;
                    forApprovalAmount1000ToolStripMenuItem.Visible = false;
                    unreadableInvoiceApprovalFormToolStripMenuItem.Visible = false;

                    dashboardToolStripMenuItem.Visible = true;
                    funnelReportToolStripMenuItem.Visible = true;
                    dailyReportToolStripMenuItem.Visible = false;
                    qADashboardToolStripMenuItem.Visible = false;
                    auditDashboardToolStripMenuItem.Visible = false;


                    toolsToolStripMenuItem.Visible = false;
                    assignTaskFormToolStripMenuItem1.Visible = false;
                    skipToolStripMenuItem1.Visible = false;
                    invoiceInquiryToolStripMenuItem.Visible = false;
                    uSPsForMPDToolStripMenuItem.Visible = false;
                    uSPWebsiteInformationToolStripMenuItem.Visible = false;

                    queueToolStripMenuItem.Visible = false;
                }

                if (adminUsers.ToString().ToLower() == "sup")
                {
                    functionsToolStripMenuItem.Visible = false;

                    auditToolStripMenuItem.Visible = true;

                    dashboardToolStripMenuItem.Visible = true;

                    toolsToolStripMenuItem.Visible = false;

                    queueToolStripMenuItem.Visible = false;
                }

                //if (adminUsers.Contains(Environment.UserName.ToLower()))
                //{
                //    functionsToolStripMenuItem.Visible = true;
                //    teamLeaderToolToolStripMenuItem.Visible = true;
                //    dashboardToolStripMenuItem.Visible = true;
                //}
                //else
                //{
                //    functionsToolStripMenuItem.Visible = false;
                //    teamLeaderToolToolStripMenuItem.Visible = false;
                //    dashboardToolStripMenuItem.Visible = false;
                //}
            }
            else
            {
                functionsToolStripMenuItem.Visible = false;

                auditToolStripMenuItem.Visible = true;
                auditFormToolStripMenuItem.Visible = true;
                duplicateApprovalFormToolStripMenuItem.Visible = true;
                forApprovalAmount1000ToolStripMenuItem.Visible = false;
                unreadableInvoiceApprovalFormToolStripMenuItem.Visible = false;

                dashboardToolStripMenuItem.Visible = true;
                funnelReportToolStripMenuItem.Visible = true;
                dailyReportToolStripMenuItem.Visible = false;
                qADashboardToolStripMenuItem.Visible = false;
                auditDashboardToolStripMenuItem.Visible = false;


                toolsToolStripMenuItem.Visible = false;
                assignTaskFormToolStripMenuItem1.Visible = false;
                skipToolStripMenuItem1.Visible = false;
                invoiceInquiryToolStripMenuItem.Visible = false;
                uSPsForMPDToolStripMenuItem.Visible = false;
                uSPWebsiteInformationToolStripMenuItem.Visible = false;

                queueToolStripMenuItem.Visible = false;
            }
        }

        private bool isDoNotMailSelected()
        {
            if (checkBox11.Checked == false && checkBox12.Checked == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool isFinalBillSelected()
        {
            if (checkBox23.Checked == false && checkBox24.Checked == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private bool isHasTransAmtSelected()
        {
            if (chkTransAmtYes.Checked == false && chkTransAmtNo.Checked == false)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private int GetInvoiceCount(string Invoice)
        {

            int count = -1;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select top 1 InvoiceCount from tbl_ADHOC_EM_Tasks where Invoice = '{0}' order by LastDateModified desc", Invoice);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    count = rd.GetInt32(0);
                }
                catch
                {
                    count = -1;
                }
            }
            else
            {
                count = 1;
            }

            conn.Close();

            return count;
        }

        private string GetParentInvoice(string Invoice)
        {

            string p_invoice = string.Empty;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select top 1 ParentAccount from tbl_ADHOC_EM_Tasks where Invoice = '{0}' order by LastDateModified desc", Invoice);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    p_invoice = rd[0].ToString();
                }
                catch
                {
                    p_invoice = string.Empty;
                }
            }
            else
            {
                p_invoice = string.Empty;
            }

            conn.Close();

            return p_invoice;
        }

        private void SaveItem(bool isToBeClosed)
        {


            string invoicepath = string.Empty;
            double previousCharge = 0, lateFees = 0, totalCharge = 0, paymentReceived = 0, currentCharge = 0, disconnectionFee = 0, actAmount = 0;

            //-------------------------------------------------
            // Bug Fix for missing Service From and Service To
            //-------------------------------------------------
            if (info.invoice_date == null)
            {
                info.invoice_date = DateTime.MinValue;
            }
            if (info.DueDate == null)
            {
                info.DueDate = DateTime.MinValue;
            }
            if (info.ServiceFrom == null)
            {
                info.ServiceFrom = DateTime.MinValue;
            }
            if (info.ServiceTo == null)
            {
                info.ServiceTo = DateTime.MinValue;
            }
            //-------------------------------------------------

            if (!string.IsNullOrEmpty(textBox11.Text)) 
            {
                // If there is Total AMT After Due Date, Ignore Total Computed Amount
                totalCharge = textBox12.Text != string.Empty ? Convert.ToDouble(textBox12.Text) : Convert.ToDouble(textBox11.Text);

                foreach (PaymentClass pc in paymentBreakdown)
                {
                    switch (pc.PaymentBreakdown)
                    {
                        case "Previous Balance":
                            previousCharge += pc.Amount; break;
                        case "Late Fee":
                            lateFees += pc.Amount; break;
                        case "Payment Received":
                            paymentReceived += pc.Amount; break;
                        case "Current Charge":
                            currentCharge += pc.Amount; break;
                        case "Disconnection Fee":
                            disconnectionFee += pc.Amount; break;
                        case "Deposit Payment":
                            depositAmount += pc.Amount; break;
                        case "Deposit Refund":
                            depositRefund += pc.Amount; break;
                        default: break;
                    }
                }

                info.CurrentCharge = (decimal)currentCharge;
                info.DisconnectionFee = (decimal)disconnectionFee;
                info.LateFee = (decimal)lateFees;
                info.PaymentReceived = (decimal)paymentReceived;
                info.PreviousBalance = (decimal)previousCharge;
                info.Deposit = (decimal)depositAmount;
                info.DepositRefund = (decimal)depositRefund;
                //info.ActAmount = (decimal)actAmount;

                //-----------------------------------------
                // Remove previous balance if already paid
                //-----------------------------------------
                if (info.PreviousBalance != info.PaymentReceived)
                {
                    decimal balanceForwarded = info.PreviousBalance - info.PaymentReceived;

                    CheckIfPartialDuplicate(info.property_code, info.VendorId, balanceForwarded, info.Amount, info.vendor_group);
                }

                //RemoveDepositAmountIfThereIsAny();             
            }

            if (chkASFIYes.Checked)
            {
                info.isAddressedToASFI = true;
                info.BillTo = string.Empty;
            }
            else if (chkASFINo.Checked == true && chknYes.Checked == true)
            {
                info.isAddressedToASFI = true;
                info.BillTo = cmbBillName.Text.Trim();
            }
            else if (chkASFINo.Checked == true && chknNo.Checked == true)
            {
                info.isAddressedToASFI = false;
                info.BillTo = cmbBillName.Text.Trim();
            }


            info.userid = Environment.UserName;
            info.Duration = this.counter;
            info.LastDateModified = DateTime.Now;

            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();

            if (info.PendingReasons == "")
            {
                MessageBox.Show(string.Format("An error occured while saving the data. Please contact your system administrator."));
                return;
            }

            if (info.Invoice == "")
            {
                MessageBox.Show(string.Format("An error occured while saving the data. Please contact your system administrator."));
                return;
            }

            if (info.VendorId == string.Empty && info.PendingReasons == "Vendor ID Request")
            {
                MessageBox.Show(string.Format("An error occured while saving the VID request. Please contact your system administrator."));
                return;
            }

            if (info.Complete == 20)
            {
                MessageBox.Show(string.Format("An error occured while saving the data. Please contact your system administrator."));
                return;
            }
            else
            {
                Database.DBConnection.DatabaseAssignQueue q_conn = new Database.DBConnection.DatabaseAssignQueue();
                Database.DB.AssignQueue queueInfo = new Database.DB.AssignQueue();
                queueInfo = new Database.DB.AssignQueue();
                queueInfo = q_conn.GetDataByUserid(Environment.UserName);

                if (queueInfo.current_queue.ToString().ToLower().Trim() == "escalations")
                {
                    if (((Convert.ToDouble(info.Amount) > 2000 && info.client_code == "RESI") || (Convert.ToDouble(info.Amount) > 10000 && info.client_code == "OLSR") || (Convert.ToDouble(info.Amount) > 10000 && info.client_code == "PFC")) && info.AttachmentFile == null)
                    {
                        MessageBox.Show("This is a high amount invoice. Please attach the client approval.");

                        return;
                    }

                    Database.DBConnection.DatabaseREO_Properties propconn = new Database.DBConnection.DatabaseREO_Properties();
                    Database.DB.REO_Properties propdata = new Database.DB.REO_Properties();
                    propdata = propconn.GetData2(info.property_code);

                    string outofREOdate = string.Empty;

                    if (IsDate(propdata.reosrc_date.ToString()))
                    {
                        outofREOdate = propdata.reosrc_date.ToString();
                    }

                    if (IsDate(propdata.reoslc_date.ToString()))
                    {
                        outofREOdate = propdata.reoslc_date.ToString();
                    }

                    if (IsDate(propdata.reosfc_date.ToString()))
                    {
                        outofREOdate = propdata.reosfc_date.ToString();
                    }

                    if (outofREOdate == "1/1/0001 12:00:00 AM")
                    {
                        outofREOdate = string.Empty;
                    }
                    
                    int outofREOAge = 0;

                    if (outofREOdate != "")
                    {
                        CheckifTrailingAsset(propdata.investor_name, outofREOdate);
                    }

                    if (outofREOAge > 120 && info.AttachmentFile == null)
                    {
                        MessageBox.Show("This is a payment for a trailing asset. Please attach the client approval.");

                        return;
                    }

                    if (info.AttachmentFile == null)
                    {
                        info.PendingReasons = "Bulk Upload";
                    }
                    else
                    {
                        info.PendingReasons = "Manual WI";
                    }
                    
                    info.Complete = 1;
                }

                if (CheckifProcessed(info.Invoice, iniLastModDate, info.id) == false)
                {
                    conn.UpdateLatestData(info.id);

                    conn.WriteData(info);

                    isEscalQueue = false;

                    button8.Enabled = true;
                    invoicepath = info.InvoiceFolderName;
                    this.counter = 0;

                    chkASFIYes.Checked = true;

                    //MessageBox.Show("Task was saved");

                    //---------------------------------------------
                    //          Unlock file from user
                    //---------------------------------------------

                    string unlockedName = CurrentLoadedInvoice.Replace("_lock", "");
                    string newFileName = string.Empty;
                    int transfercount = 0;

                    //With Transferred balance
                    if (chkTransAmtYes.Checked && numBalTransfer.Value > 0)
                    {

                        button3.Enabled = false;
                        transfercount = Convert.ToInt32(numBalTransfer.Value);

                        try
                        {
                            ini_InvAmount += Convert.ToDecimal(info.Amount);
                        }
                        catch
                        {
                            ini_InvAmount += 0;
                        }


                        Database.DBConnection.DatabaseEM_Tasks conn1 = new Database.DBConnection.DatabaseEM_Tasks();

                        if (invoicecount == 0)
                        {
                            ini_invoice = info.Invoice;

                            ini_vendor_group = info.vendor_group;
                            ini_vendorid = info.VendorId;
                            ini_invdate = info.invoice_date.ToString();
                            ini_duedate = info.DueDate.ToString();
                        }
                        else
                        {
                            ini_invoice = GetParentInvoice(info.Invoice);

                            if (ini_invoice == "" || ini_invoice == string.Empty)
                            {
                                ini_invoice = DateTime.Now.ToString("yyyyMMddhhmmss") + "_" + info.property_code + info.vendor_group;
                            }
                        }

                        if (chkBalInfoYes.Checked && invoicecount < numBalTransfer.Value && iniComplete == "20") //&& chkBalAmtYes.Checked && invoicecount < numBalTransfer.Value)
                        {
                            invoicecount = GetInvoiceCount(info.Invoice) + 1;

                            MessageBox.Show(string.Format("Task was saved. Please process the balance transfer account no. {0}.", invoicecount));

                            newFileName = string.Format("{0}{1}", ini_invoice + "TRANSFER", invoicecount);

                            // Create a new copy of the pdf

                            try
                            {
                                System.IO.File.Copy(CurrentLoadedInvoice, System.IO.Path.Combine(TempFolder, newFileName + ".pdf"), true);
                            }
                            catch
                            {
                            }

                            conn1.WriteMultiplePropertyData(new Database.DB.EM_Tasks()
                            {
                                Invoice = newFileName,
                                InvoiceFolderName = System.IO.Path.Combine(TempFolder, newFileName + ".pdf"),
                                userid = Environment.UserName.ToLower(),
                                PendingReasons = "New Invoice",
                                InvoiceCount = invoicecount,
                                Complete = 20,
                                HasTransferAmount = info.HasTransferAmount,
                                isLatestData = true,
                                lockedto = Environment.UserName,
                                ParentAccount = ini_invoice
                            });

                            if (isToBeClosed == false)
                            {

                                Clear(false, new Control[] {label9, checkBox2, checkBox3, label30, checkBox2, checkBox4, checkBox5, checkBox13, checkBox14, label60, chkASFIYes, chkASFINo,
                                                        label58, chkTransAmtYes, chkTransAmtNo, lblCount, lblOf, numBalTransfer, label62, chkBalInfoYes, chkBalInfoNo, 
                                                        label63, chkBalAmtYes, chkBalInfoNo});

                                LoadInvoice();

                                //checkBox2.Checked = true;
                                //checkBox4.Checked = false;
                                //checkBox5.Checked = false;
                                //checkBox13.Checked = false;
                                //checkBox14.Checked = false;
                                //numBalTransfer.Enabled = false;

                                //chkBx_NoPropertyAddress.Visible = false;
                                //pictureBox5.Visible = false;
                                //cmbBillName.Text = string.Empty;
                                //checkBox8.Checked = false;
                                //checkBox8.Visible = false;

                                chkASFIYes.Checked = true;
                                //chkTransAmtYes.Checked = true;

                                //lblCount.Text = invoicecount.ToString();
                                //lblCount.Visible = true;
                                //lblOf.Visible = true;
                                //label59.Visible = false;

                                numBalTransfer.Value = transfercount;
                                numBalTransfer.Enabled = false;

                                //txtTotal.Visible = true;
                                //txtTotal.Text = "0";
                                //txtTotal.Text = (Convert.ToDecimal(txtTotal.Text) + ini_InvAmount).ToString();

                                //chkBalInfoYes.Checked = true;
                                //chkBalAmtYes.Checked = true;                        

                                //if (ini_vendor_group == "E")
                                //{
                                //    checkBox4.Checked = true;
                                //}
                                //else if (ini_vendor_group == "W")
                                //{
                                //    checkBox13.Checked = true;
                                //}
                                //else if (ini_vendor_group == "G")
                                //{
                                //    checkBox5.Checked = true;
                                //}

                                hasDoneAcctSearch = false;

                                label14.Visible = false;
                                textBox10.Visible = false;
                                checkBox6.Visible = false;
                                pictureBox5.Visible = false;

                                groupBox8.Visible = false;
                                chkBx_NoPropertyAddress.Visible = false;
                                chkBx_NoPropertyAddress.Checked = false;
                                textBox10.Clear();
                                textBox4.Clear();
                                textBox1.Clear();
                                textBox14.Clear();
                                textBox17.Clear();
                                textBox16.Clear();
                                chkbx_noZipCode.Checked = false;
                                pnlAddress.Visible = false;
                                dataGridView1.Visible = false;
                                checkBox8.Visible = false;
                                checkBox6.Checked = false;
                                numOftries = 0;
                                numOfSearch = 0;
                                isDoneSearch = false;
                                label57.Text = numOftries.ToString();

                            }
                            else
                            {
                                this.Close();
                            }
                        }
                        else if (chkBalInfoNo.Checked && iniComplete == "20") //&& chkBalAmtYes.Checked)
                        {
                            MessageBox.Show(string.Format("Task was saved. The transfer accounts will be processed in Callout.", invoicecount));

                            for (int i = 1; i <= numBalTransfer.Value; i++)
                            {
                                invoicecount = GetInvoiceCount(info.Invoice) + 1;

                                newFileName = string.Format("{0} - {1}", ini_invoice.Trim() == "" ? info.Invoice : ini_invoice, invoicecount);

                                // Create a new copy of the pdf
                                try
                                {
                                    System.IO.File.Copy(CurrentLoadedInvoice, System.IO.Path.Combine(TempFolder, newFileName + ".pdf"), true);
                                }
                                catch
                                {
                                }

                                conn1.WriteMultiplePropertyData(new Database.DB.EM_Tasks()
                                {
                                    Invoice = newFileName,
                                    InvoiceFolderName = System.IO.Path.Combine(TempFolder, newFileName + ".pdf"),
                                    userid = Environment.UserName.ToLower(),
                                    PendingReasons = "New Invoice",
                                    InvoiceCount = invoicecount,
                                    Complete = 20,
                                    usp_name = info.usp_name,
                                    VendorId = info.VendorId,
                                    HasTransferAmount = info.HasTransferAmount,
                                    isLatestData = false,
                                    lockedto = null,
                                    ParentAccount = ini_invoice,
                                    vendor_group = ini_vendor_group
                                });

                                conn1.WriteMultiplePropertyData(new Database.DB.EM_Tasks()
                                {
                                    Invoice = newFileName,
                                    InvoiceFolderName = System.IO.Path.Combine(TempFolder, newFileName + ".pdf"),
                                    userid = Environment.UserName.ToLower(),
                                    PendingReasons = "Pending for Audit",
                                    InvoiceCount = invoicecount,
                                    Complete = 21,
                                    usp_name = info.usp_name,
                                    VendorId = info.VendorId,
                                    HasTransferAmount = info.HasTransferAmount,
                                    isLatestData = true,
                                    lockedto = null,
                                    ParentAccount = ini_invoice,
                                    AccountNumber = "None"
                                });

                                lblCount.Text = invoicecount.ToString();
                                lblCount.Visible = true;
                                lblOf.Visible = true;
                                label59.Visible = false;
                            }

                            TransferInvoice(CurrentLoadedInvoice, unlockedName);
                            CurrentLoadedInvoice = unlockedName;
                            Clear(false, new Control[] { label9, checkBox2, checkBox3 });

                            if (isToBeClosed == false)
                            {
                                LoadInvoice();

                                checkBox2.Checked = true;
                                checkBox4.Checked = false;
                                checkBox5.Checked = false;
                                checkBox13.Checked = false;
                                checkBox14.Checked = false;

                                label60.Visible = false;
                                chkASFIYes.Visible = false;
                                chkASFINo.Visible = false;

                                label58.Visible = false;
                                chkTransAmtYes.Visible = false;
                                chkTransAmtNo.Visible = false;


                                label62.Visible = false;
                                chkBalInfoYes.Visible = false;
                                chkBalInfoYes.Checked = false;
                                //chkBalInfoYes.Enabled = false;
                                chkBalInfoNo.Visible = false;
                                chkBalInfoNo.Checked = false;
                                //chkBalInfoNo.Enabled = false;

                                label63.Visible = false;
                                chkBalAmtYes.Visible = false;
                                chkBalAmtYes.Checked = false;
                                //chkBalAmtYes.Enabled = false;
                                chkBalAmtNo.Visible = false;
                                chkBalAmtNo.Checked = false;
                                //chkBalAmtNo.Enabled = false;


                                label47.Visible = false;
                                checkBox15.Visible = false;
                                //checkBox15.Checked = true;
                                checkBox16.Visible = false;

                                chkBx_NoPropertyAddress.Visible = false;
                                pictureBox5.Visible = false;
                                //cmbBillName.Text = string.Empty;
                                checkBox8.Checked = false;
                                checkBox8.Visible = false;

                                label61.Visible = false;
                                cmbBillName.Visible = false;
                                label64.Visible = false;
                                label65.Visible = false;
                                chknYes.Visible = false;
                                chknNo.Visible = false;
                                lblCount.Text = "-";
                                lblCount.Visible = false;
                                lblOf.Visible = false;
                                txtTotal.Visible = false;
                                txtTotal.Text = "0";
                                label59.Visible = false;
                                numBalTransfer.Visible = false;
                                numBalTransfer.Value = 0;
                                lblAdrsNotif.Visible = false;
                            }
                            else
                            {
                                this.Close();
                            }
                        }
                        //else if (chkBalInfoYes.Checked && chkBalAmtNo.Checked)
                        //{

                        //    if (numBalTransfer.Value > 1)
                        //    {
                        //        invoicecount = GetInvoiceCount(info.Invoice) + 1;

                        //        MessageBox.Show(string.Format("Task was saved. Please process the balance transfer account no. {1}.", invoicecount + 1));

                        //        newFileName = string.Format("{0} - {1}", ini_invoice, invoicecount);

                        //        // Create a new copy of the pdf
                        //        System.IO.File.Copy(CurrentLoadedInvoice, System.IO.Path.Combine(TempFolder, newFileName + ".pdf"));
                        //        conn1.WriteMultiplePropertyData(new Database.DB.EM_Tasks()
                        //        {
                        //            Invoice = newFileName,
                        //            InvoiceFolderName = System.IO.Path.Combine(TempFolder, newFileName + ".pdf"),
                        //            userid = Environment.UserName.ToLower(),
                        //            PendingReasons = "New Invoice",
                        //            InvoiceCount = invoicecount,
                        //            Complete = 20,
                        //            HasTransferAmount = info.HasTransferAmount,
                        //            isLatestData = true,
                        //            lockedto = Environment.UserName,
                        //            ParentAccount = ini_invoice
                        //        });

                        //        if (isToBeClosed == false)
                        //        {

                        //            Clear(false, new Control[] {label9, checkBox2, checkBox3, label30, checkBox2, checkBox4, checkBox5, checkBox13, checkBox14, label60, chkASFIYes, chkASFINo,
                        //                                    label58, chkTransAmtYes, chkTransAmtNo, lblCount, lblOf, numBalTransfer, label62, chkBalInfoYes, chkBalInfoNo, 
                        //                                    label63, chkBalAmtYes, chkBalInfoNo});

                        //            LoadInvoice();

                        //            lblCount.Text = invoicecount.ToString();
                        //            lblCount.Visible = true;
                        //            lblOf.Visible = true;

                        //            checkBox2.Checked = true;
                        //            checkBox4.Checked = false;
                        //            checkBox5.Checked = false;
                        //            checkBox13.Checked = false;
                        //            checkBox14.Checked = false;
                        //            numBalTransfer.Enabled = false;

                        //            chkBx_NoPropertyAddress.Visible = false;
                        //            pictureBox5.Visible = false;
                        //            cmbBillName.Text = string.Empty;
                        //            checkBox8.Checked = false;
                        //            checkBox8.Visible = false;
                        //        }
                        //        else
                        //        {
                        //            this.Close();
                        //        }
                        //    }
                        //    else
                        //    {
                        //        MessageBox.Show("Task was saved");

                        //        TransferInvoice(CurrentLoadedInvoice, unlockedName);
                        //        CurrentLoadedInvoice = unlockedName;
                        //        Clear(false, new Control[] { label9, checkBox2, checkBox3 });

                        //        if (isToBeClosed == false)
                        //        {
                        //            LoadInvoice();

                        //            checkBox2.Checked = true;
                        //            checkBox4.Checked = false;
                        //            checkBox5.Checked = false;
                        //            checkBox13.Checked = false;
                        //            checkBox14.Checked = false;

                        //            label60.Visible = false;
                        //            chkASFIYes.Visible = false;
                        //            chkASFINo.Visible = false;

                        //            label58.Visible = false;
                        //            chkTransAmtYes.Visible = false;
                        //            chkTransAmtNo.Visible = false;


                        //            label62.Visible = false;
                        //            chkBalInfoYes.Visible = false;
                        //            chkBalInfoYes.Checked = false;
                        //            //chkBalInfoYes.Enabled = false;
                        //            chkBalInfoNo.Visible = false;
                        //            chkBalInfoNo.Checked = false;
                        //            //chkBalInfoNo.Enabled = false;

                        //            label63.Visible = false;
                        //            chkBalAmtYes.Visible = false;
                        //            chkBalAmtYes.Checked = false;
                        //            //chkBalAmtYes.Enabled = false;
                        //            chkBalAmtNo.Visible = false;
                        //            chkBalAmtNo.Checked = false;
                        //            //chkBalAmtNo.Enabled = false;


                        //            label47.Visible = false;
                        //            checkBox15.Visible = false;
                        //            //checkBox15.Checked = true;
                        //            checkBox16.Visible = false;

                        //            chkBx_NoPropertyAddress.Visible = false;
                        //            pictureBox5.Visible = false;
                        //            //cmbBillName.Text = string.Empty;
                        //            checkBox8.Checked = false;
                        //            checkBox8.Visible = false;

                        //            label61.Visible = false;
                        //            cmbBillName.Visible = false;
                        //            lblCount.Text = "-";
                        //            lblCount.Visible = false;
                        //            txtTotal.Visible = false;
                        //            txtTotal.Text = "0";
                        //            lblOf.Visible = false;
                        //            label59.Visible = false;
                        //            numBalTransfer.Visible = false;
                        //            numBalTransfer.Value = 0;
                        //            lblAdrsNotif.Visible = false;
                        //        }
                        //        else
                        //        {
                        //            this.Close();
                        //        }
                        //    }
                        //}
                        else
                        {

                            invoicecount = 0;
                            ini_invoice = string.Empty;
                            ini_vendor_group = string.Empty;
                            ini_invdate = string.Empty;
                            ini_duedate = string.Empty;
                            ini_InvAmount = 0;
                            ini_vendorid = string.Empty;
                            transfercount = 0;
                            isBalTransfer = false;

                            TransferInvoice(CurrentLoadedInvoice, unlockedName);
                            CurrentLoadedInvoice = unlockedName;
                            Clear(false, new Control[] { label9, checkBox2, checkBox3 });

                            if (isToBeClosed == false)
                            {
                                LoadInvoice();
                                checkBox2.Checked = true;
                                checkBox4.Checked = false;
                                checkBox5.Checked = false;
                                checkBox13.Checked = false;
                                checkBox14.Checked = false;

                                label60.Visible = false;
                                chkASFIYes.Visible = false;
                                chkASFINo.Visible = false;

                                label58.Visible = false;
                                chkTransAmtYes.Visible = false;
                                chkTransAmtNo.Visible = false;


                                label62.Visible = false;
                                chkBalInfoYes.Visible = false;
                                chkBalInfoYes.Checked = false;
                                //chkBalInfoYes.Enabled = false;
                                chkBalInfoNo.Visible = false;
                                chkBalInfoNo.Checked = false;
                                //chkBalInfoNo.Enabled = false;

                                label63.Visible = false;
                                chkBalAmtYes.Visible = false;
                                chkBalAmtYes.Checked = false;
                                //chkBalAmtYes.Enabled = false;
                                chkBalAmtNo.Visible = false;
                                chkBalAmtNo.Checked = false;
                                //chkBalAmtNo.Enabled = false;


                                label47.Visible = false;
                                checkBox15.Visible = false;
                                //checkBox15.Checked = true;
                                checkBox16.Visible = false;

                                checkBox6.Visible = false;
                                chkBx_NoPropertyAddress.Visible = false;
                                pictureBox5.Visible = false;
                                cmbBillName.Text = string.Empty;
                                checkBox8.Checked = false;
                                checkBox8.Visible = false;

                                label61.Visible = false;
                                cmbBillName.Visible = false;
                                label64.Visible = false;
                                label65.Visible = false;
                                chknYes.Visible = false;
                                chknNo.Visible = false;
                                lblCount.Text = "-";
                                lblCount.Visible = false;
                                lblOf.Visible = false;
                                txtTotal.Visible = false;
                                txtTotal.Text = "0";
                                label59.Visible = false;
                                numBalTransfer.Visible = false;
                                numBalTransfer.Value = 0;
                            }
                            else
                            {
                                this.Close();
                            }
                        }

                    }
                    else
                    {

                        MessageBox.Show("Task was saved");
                        isBalTransfer = false;

                        if (checkBox15.Checked == true && chkTransAmtYes.Checked == false)
                        {


                            var res = MessageBox.Show("Do you want to process a different property address listed in the same invoice?",
                                "Multiple Property Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Question);


                            if (invoicecount == 0)
                            {
                                ini_invoice = info.Invoice;
                            }
                            else
                            {
                                ini_invoice = GetParentInvoice(info.Invoice);
                            }

                            if (res == DialogResult.Yes)
                            {

                                invoicecount = GetInvoiceCount(info.Invoice) + 1;
                                newFileName = string.Format("{0} - {1}", ini_invoice.Trim() == "" ? info.Invoice : ini_invoice, invoicecount);

                                // Create a new copy of the pdf

                                if (queue_code == 0)
                                {
                                    try
                                    {
                                        System.IO.File.Copy(CurrentLoadedInvoice, System.IO.Path.Combine(TempFolder, newFileName + ".pdf"), true);
                                    }
                                    catch
                                    {
                                    }

                                    Database.DBConnection.DatabaseEM_Tasks conn1 = new Database.DBConnection.DatabaseEM_Tasks();

                                    conn1.WriteMultiplePropertyData(new Database.DB.EM_Tasks()
                                    {
                                        Invoice = newFileName,
                                        InvoiceFolderName = System.IO.Path.Combine(TempFolder, newFileName + ".pdf"),
                                        userid = Environment.UserName.ToLower(),
                                        PendingReasons = iniPendingReason,
                                        InvoiceCount = invoicecount,
                                        Complete = 0,
                                        HasTransferAmount = "No",
                                        isLatestData = true,
                                        lockedto = Environment.UserName,
                                        ParentAccount = ini_invoice
                                    });
                                }
                                else if (queue_code == 70)
                                {

                                    try
                                    {
                                        System.IO.File.Copy(CurrentLoadedInvoice, System.IO.Path.Combine(TempFolder, newFileName + ".pdf"), true);
                                    }
                                    catch
                                    {
                                    }

                                    Database.DBConnection.DatabaseEM_Tasks conn1 = new Database.DBConnection.DatabaseEM_Tasks();


                                    conn1.WriteMultiplePropertyData(new Database.DB.EM_Tasks()
                                    {
                                        Invoice = newFileName,
                                        InvoiceFolderName = System.IO.Path.Combine(TempFolder, newFileName + ".pdf"),
                                        userid = Environment.UserName.ToLower(),
                                        PendingReasons = "New Escalation",
                                        InvoiceCount = invoicecount,
                                        Complete = 20,
                                        HasTransferAmount = "No",
                                        isLatestData = true,
                                        lockedto = Environment.UserName,
                                        ParentAccount = ini_invoice
                                    });
                                }
                                else
                                {

                                    try
                                    {
                                        System.IO.File.Copy(CurrentLoadedInvoice, System.IO.Path.Combine(TempFolder, newFileName + ".pdf"), true);
                                    }
                                    catch
                                    {
                                    }

                                    Database.DBConnection.DatabaseEM_Tasks conn1 = new Database.DBConnection.DatabaseEM_Tasks();


                                    conn1.WriteMultiplePropertyData(new Database.DB.EM_Tasks()
                                    {
                                        Invoice = newFileName,
                                        InvoiceFolderName = System.IO.Path.Combine(TempFolder, newFileName + ".pdf"),
                                        userid = Environment.UserName.ToLower(),
                                        PendingReasons = "New Invoice",
                                        InvoiceCount = invoicecount,
                                        Complete = 20,
                                        HasTransferAmount = "No",
                                        isLatestData = true,
                                        lockedto = Environment.UserName,
                                        ParentAccount = ini_invoice
                                    });
                                }


                                TransferInvoice(CurrentLoadedInvoice, unlockedName);
                                Clear(false, new Control[] { label9, checkBox2, checkBox3 });

                                if (isToBeClosed == false)
                                {
                                    LoadInvoice();

                                    checkBox2.Checked = true;
                                    checkBox4.Checked = false;
                                    checkBox5.Checked = false;
                                    checkBox13.Checked = false;
                                    checkBox14.Checked = false;

                                    label60.Visible = false;
                                    chkASFIYes.Visible = false;
                                    chkASFINo.Visible = false;

                                    label58.Visible = false;
                                    chkTransAmtYes.Visible = false;
                                    chkTransAmtNo.Visible = false;


                                    label62.Visible = false;
                                    chkBalInfoYes.Visible = false;
                                    chkBalInfoYes.Checked = false;
                                    //chkBalInfoYes.Enabled = false;
                                    chkBalInfoNo.Visible = false;
                                    chkBalInfoNo.Checked = false;
                                    //chkBalInfoNo.Enabled = false;

                                    label63.Visible = false;
                                    chkBalAmtYes.Visible = false;
                                    chkBalAmtYes.Checked = false;
                                    //chkBalAmtYes.Enabled = false;
                                    chkBalAmtNo.Visible = false;
                                    chkBalAmtNo.Checked = false;
                                    //chkBalAmtNo.Enabled = false;


                                    label47.Visible = false;
                                    checkBox15.Visible = false;
                                    //checkBox15.Checked = true;
                                    checkBox16.Visible = false;

                                    chkBx_NoPropertyAddress.Visible = false;
                                    pictureBox5.Visible = false;
                                    //cmbBillName.Text = string.Empty;
                                    checkBox8.Checked = false;
                                    checkBox8.Visible = false;

                                    label61.Visible = false;
                                    cmbBillName.Visible = false;
                                    label64.Visible = false;
                                    label65.Visible = false;
                                    chknYes.Visible = false;
                                    chknNo.Visible = false;
                                    lblCount.Text = "-";
                                    lblCount.Visible = false;
                                    lblOf.Visible = false;
                                    txtTotal.Visible = false;
                                    txtTotal.Text = "0";
                                    label59.Visible = false;
                                    numBalTransfer.Visible = false;
                                    numBalTransfer.Value = 0;
                                    lblAdrsNotif.Visible = false;
                                    checkBox6.Visible = false;

                                    label66.Visible = false;
                                    txtUSPCity.Visible = false;
                                    label67.Visible = false;
                                    txtUSPState.Visible = false;
                                    label68.Visible = false;
                                    txtUSPZipcode.Visible = false;
                                    checkBox8.Visible = false;
                                    txtUSPStrt.Visible = false;
                                    label69.Visible = false;
                                    btnSearchUSP.Visible = false;
                                }
                                else
                                {
                                    this.Close();
                                }
                            }
                            else
                            {

                                invoicecount = 0;
                                ini_invoice = string.Empty;

                                TransferInvoice(CurrentLoadedInvoice, unlockedName);
                                CurrentLoadedInvoice = unlockedName;
                                Clear(false, new Control[] { label9, checkBox2, checkBox3 });

                                if (isToBeClosed == false)
                                {
                                    LoadInvoice();

                                    checkBox2.Checked = true;
                                    checkBox4.Checked = false;
                                    checkBox5.Checked = false;
                                    checkBox13.Checked = false;
                                    checkBox14.Checked = false;

                                    label60.Visible = false;
                                    chkASFIYes.Visible = false;
                                    chkASFINo.Visible = false;

                                    label58.Visible = false;
                                    chkTransAmtYes.Visible = false;
                                    chkTransAmtNo.Visible = false;


                                    label62.Visible = false;
                                    chkBalInfoYes.Visible = false;
                                    chkBalInfoYes.Checked = false;
                                    //chkBalInfoYes.Enabled = false;
                                    chkBalInfoNo.Visible = false;
                                    chkBalInfoNo.Checked = false;
                                    //chkBalInfoNo.Enabled = false;

                                    label63.Visible = false;
                                    chkBalAmtYes.Visible = false;
                                    chkBalAmtYes.Checked = false;
                                    //chkBalAmtYes.Enabled = false;
                                    chkBalAmtNo.Visible = false;
                                    chkBalAmtNo.Checked = false;
                                    //chkBalAmtNo.Enabled = false;


                                    label47.Visible = false;
                                    checkBox15.Visible = false;
                                    //checkBox15.Checked = true;
                                    checkBox16.Visible = false;

                                    chkBx_NoPropertyAddress.Visible = false;
                                    pictureBox5.Visible = false;
                                    //cmbBillName.Text = string.Empty;
                                    checkBox8.Checked = false;
                                    checkBox8.Visible = false;

                                    label61.Visible = false;
                                    cmbBillName.Visible = false;
                                    label64.Visible = false;
                                    label65.Visible = false;
                                    chknYes.Visible = false;
                                    chknNo.Visible = false;
                                    lblCount.Text = "-";
                                    lblCount.Visible = false;
                                    lblOf.Visible = false;
                                    txtTotal.Visible = false;
                                    txtTotal.Text = "0";
                                    label59.Visible = false;
                                    numBalTransfer.Visible = false;
                                    numBalTransfer.Value = 0;
                                    lblAdrsNotif.Visible = false;

                                    label66.Visible = false;
                                    txtUSPCity.Visible = false;
                                    label67.Visible = false;
                                    txtUSPState.Visible = false;
                                    label68.Visible = false;
                                    txtUSPZipcode.Visible = false;
                                    checkBox8.Visible = false;
                                    txtUSPStrt.Visible = false;
                                    label69.Visible = false;
                                    btnSearchUSP.Visible = false;
                                }
                                else
                                {
                                    this.Close();
                                }
                            }
                        }
                        else
                        {
                            TransferInvoice(CurrentLoadedInvoice, unlockedName);
                            CurrentLoadedInvoice = unlockedName;
                            Clear(false, new Control[] { label9, checkBox2, checkBox3 });

                            if (isToBeClosed == false)
                            {
                                LoadInvoice();

                                checkBox2.Checked = true;
                                checkBox4.Checked = false;
                                checkBox5.Checked = false;
                                checkBox13.Checked = false;
                                checkBox14.Checked = false;

                                label60.Visible = false;
                                chkASFIYes.Visible = false;
                                chkASFINo.Visible = false;

                                label58.Visible = false;
                                chkTransAmtYes.Visible = false;
                                chkTransAmtNo.Visible = false;


                                label62.Visible = false;
                                chkBalInfoYes.Visible = false;
                                chkBalInfoYes.Checked = false;
                                //chkBalInfoYes.Enabled = false;
                                chkBalInfoNo.Visible = false;
                                chkBalInfoNo.Checked = false;
                                //chkBalInfoNo.Enabled = false;

                                label63.Visible = false;
                                chkBalAmtYes.Visible = false;
                                chkBalAmtYes.Checked = false;
                                //chkBalAmtYes.Enabled = false;
                                chkBalAmtNo.Visible = false;
                                chkBalAmtNo.Checked = false;
                                //chkBalAmtNo.Enabled = false;


                                label47.Visible = false;
                                checkBox15.Visible = false;
                                //checkBox15.Checked = true;
                                checkBox16.Visible = false;

                                chkBx_NoPropertyAddress.Visible = false;
                                pictureBox5.Visible = false;
                                //cmbBillName.Text = string.Empty;
                                checkBox8.Checked = false;
                                checkBox8.Visible = false;

                                label61.Visible = false;
                                cmbBillName.Visible = false;
                                label64.Visible = false;
                                label65.Visible = false;
                                chknYes.Visible = false;
                                chknNo.Visible = false;
                                lblCount.Text = "-";
                                lblCount.Visible = false;
                                lblOf.Visible = false;
                                txtTotal.Visible = false;
                                txtTotal.Text = "0";
                                label59.Visible = false;
                                numBalTransfer.Enabled = true;
                                numBalTransfer.Visible = false;
                                numBalTransfer.Value = 0;
                                lblAdrsNotif.Visible = false;

                                invoicecount = 0;
                                ini_duedate = string.Empty;
                                ini_InvAmount = 0;
                                ini_invoice = string.Empty;
                                ini_vendor_group = string.Empty;
                                ini_vendorid = string.Empty;

                                label66.Visible = false;
                                txtUSPCity.Visible = false;
                                label67.Visible = false;
                                txtUSPState.Visible = false;
                                label68.Visible = false;
                                txtUSPZipcode.Visible = false;
                                checkBox8.Visible = false;
                                txtUSPStrt.Visible = false;
                                label69.Visible = false;
                                btnSearchUSP.Visible = false;
                            }
                            else
                            {
                                this.Close();
                            }
                        }
                    }

                }
                else
                {
                    conn.UpdateLatestData(info.id);

                    LoadInvoice();

                    checkBox2.Checked = true;
                    checkBox4.Checked = false;
                    checkBox5.Checked = false;
                    checkBox13.Checked = false;
                    checkBox14.Checked = false;

                    label60.Visible = false;
                    chkASFIYes.Visible = false;
                    chkASFINo.Visible = false;

                    label58.Visible = false;
                    chkTransAmtYes.Visible = false;
                    chkTransAmtNo.Visible = false;


                    label62.Visible = false;
                    chkBalInfoYes.Visible = false;
                    chkBalInfoYes.Checked = false;
                    //chkBalInfoYes.Enabled = false;
                    chkBalInfoNo.Visible = false;
                    chkBalInfoNo.Checked = false;
                    //chkBalInfoNo.Enabled = false;

                    label63.Visible = false;
                    chkBalAmtYes.Visible = false;
                    chkBalAmtYes.Checked = false;
                    //chkBalAmtYes.Enabled = false;
                    chkBalAmtNo.Visible = false;
                    chkBalAmtNo.Checked = false;
                    //chkBalAmtNo.Enabled = false;


                    label47.Visible = false;
                    checkBox15.Visible = false;
                    //checkBox15.Checked = true;
                    checkBox16.Visible = false;

                    chkBx_NoPropertyAddress.Visible = false;
                    pictureBox5.Visible = false;
                    //cmbBillName.Text = string.Empty;
                    checkBox8.Checked = false;
                    checkBox8.Visible = false;

                    label61.Visible = false;
                    cmbBillName.Visible = false;
                    label64.Visible = false;
                    label65.Visible = false;
                    chknYes.Visible = false;
                    chknNo.Visible = false;
                    lblCount.Text = "-";
                    lblCount.Visible = false;
                    lblOf.Visible = false;
                    txtTotal.Visible = false;
                    txtTotal.Text = "0";
                    label59.Visible = false;
                    numBalTransfer.Enabled = true;
                    numBalTransfer.Visible = false;
                    numBalTransfer.Value = 0;
                    lblAdrsNotif.Visible = false;

                    invoicecount = 0;
                    ini_duedate = string.Empty;
                    ini_InvAmount = 0;
                    ini_invoice = string.Empty;
                    ini_vendor_group = string.Empty;
                    ini_vendorid = string.Empty;

                    label66.Visible = false;
                    txtUSPCity.Visible = false;
                    label67.Visible = false;
                    txtUSPState.Visible = false;
                    label68.Visible = false;
                    txtUSPZipcode.Visible = false;
                    checkBox8.Visible = false;
                    txtUSPStrt.Visible = false;
                    label69.Visible = false;
                    btnSearchUSP.Visible = false;

                    Uri myUri = new Uri("about:blank");

                    pdfviewer.Navigate(myUri);                   
                }

                // Call function asynchronously
                BackgroundWorker worker1 = new BackgroundWorker();
                worker1.DoWork += new DoWorkEventHandler(BW_LoadTasksCompleted_DoWork);
                worker1.RunWorkerAsync();
            }
        }

        private void InitializeObjects()
        {
            checkBox4.Checked = false;
            checkBox5.Checked = false;
            checkBox13.Checked = false;
            checkBox14.Checked = false;
            checkBox2.Checked = true;
            chkBx_NoPropertyAddress.Visible = false;
            checkBox8.Checked = false;
            checkBox8.Visible = false;
            label60.Visible = false;
            chkASFIYes.Visible = false;
            chkASFINo.Visible = false;
            label47.Visible = false;
            label58.Visible = false;
            checkBox15.Visible = false;
            checkBox15.Checked = true;
            checkBox16.Visible = false;
            chkTransAmtYes.Visible = false;
            chkTransAmtNo.Visible = false;
            pictureBox5.Visible = false;
            cmbBillName.Text = string.Empty;

            label62.Visible = false;
            chkBalInfoYes.Visible = false;
            chkBalInfoYes.Checked = false;
            chkBalInfoNo.Visible = false;
            chkBalInfoNo.Checked = false;

            label63.Visible = false;
            chkBalAmtYes.Visible = false;
            chkBalAmtYes.Checked = false;
            chkBalAmtYes.Enabled = true;
            chkBalAmtNo.Visible = false;
            chkBalAmtNo.Checked = false;
            chkBalAmtNo.Enabled = true;

        }


        private void RemoveDepositAmountIfThereIsAny()
        {
            if (info.final_bill == "Yes")
            {
                info.Amount = (Convert.ToDouble(info.Amount) - depositAmount).ToString();
            }
        }

        private bool CheckIfMoreThan1000()
        {
            bool result = false;

            if (info.AmountAfterDue != 0)
            {
                if ((info.client_code == "RESI") && (info.AmountAfterDue >= (decimal)500 && info.AmountAfterDue < (decimal)1000))
                {
                    info.Complete = 1;
                    info.PendingReasons = "For Review RESI Amount > $500";
                    info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                    info.VendorAssignedStatus = string.Empty;
                    result = true;
                }

                if (info.AmountAfterDue >= 1000)
                {
                    info.Complete = 1;
                    info.PendingReasons = "For Review Amount > $1,000";
                    info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                    info.VendorAssignedStatus = string.Empty;
                    result = true;
                }

                if (info.AmountAfterDue >= 10000 && (info.client_code == "OLSR" || info.client_code == "PFC"))
                {
                    info.Complete = 1;
                    info.PendingReasons = string.Format("For Review {0} Amount > $10,000", info.client_code);
                    info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                    info.VendorAssignedStatus = string.Empty;
                }

                // Check if PROPERTY_CODE is OLSR and AMOUNT > $2,500
                if (info.client_code == "OLSR" && Convert.ToDouble(info.Amount) >= 2500 && Convert.ToDouble(info.Amount) < 10000)
                {
                    info.Complete = 14;
                    info.PendingReasons = "RSC Approval - High Amount";
                    info.InvoiceFolderName = strCreateInvoiceFolderName(RSCFolder);
                    info.VendorAssignedStatus = string.Empty;

                    bwRscApproval.RunWorkerAsync();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(info.Amount))
                {
                    if ((info.client_code == "RESI") && (Convert.ToDecimal(info.Amount) >= (decimal)500 && Convert.ToDecimal(info.Amount) < (decimal)1000))
                    {
                        info.Complete = 1;
                        info.PendingReasons = "For Review RESI Amount > $500";
                        info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                        info.VendorAssignedStatus = string.Empty;
                        result = true;
                    }

                    if (Convert.ToDecimal(info.Amount) >= 1000)
                    {
                        info.Complete = 1;
                        info.PendingReasons = "For Review Amount > $1,000";
                        info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                        info.VendorAssignedStatus = string.Empty;
                        result = true;
                    }

                    if (Convert.ToDecimal(info.Amount) >= 10000 && (info.client_code == "OLSR" || info.client_code == "PFC"))
                    {
                        info.Complete = 1;
                        info.PendingReasons = string.Format("For Review {0} Amount > $10,000", info.client_code);
                        info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                        info.VendorAssignedStatus = string.Empty;
                        result = true;
                    }

                    // Check if PROPERTY_CODE is OLSR and AMOUNT > $2,500
                    if (info.client_code == "OLSR" && Convert.ToDouble(info.Amount) >= 2500 && Convert.ToDouble(info.Amount) < 10000)
                    {
                        info.Complete = 14;
                        info.PendingReasons = "RSC Approval - High Amount";
                        info.InvoiceFolderName = strCreateInvoiceFolderName(RSCFolder);
                        info.VendorAssignedStatus = string.Empty;

                        bwRscApproval.RunWorkerAsync();
                    }
                }
            }

            return result;
        }

        private void CheckIf21Days()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select * from tbl_EM_WorkItemsLast30Days a
                                            where a.property_code = '{0}' 
                                            and a.vendor_code = '{1}'
                                            and SUBSTRING(a.wo_task,0,1) = '{2}'
                                            and DATEDIFF(DAY, a.issued_date, GETDATE()) <= 21",
                                            info.property_code,
                                            info.VendorId,
                                            info.vendor_group);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                info.VendorAssignedStatus = "NA";
                info.Complete = 1;
                info.PendingReasons = "Filtered by 21 Days";
                info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
            }

            rd.Close();
            conn.Close();
        }


        private bool isUnderASFI(string propcode)
        {

            bool isASFI = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select top 1 a.PropertyCode from tbl_EM_RESI_ASFI_Properties a
                                            where a.PropertyCode = '{0}'", info.property_code);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    isASFI = true;
                }
                catch
                {
                    isASFI = false;
                }
            }
            else
            {
                isASFI = false;
            }

            conn.Close();

            return isASFI;
        }

        private bool isExists(string Invoice)
        {

            bool isExists = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select id from tbl_ADHOC_EM_Tasks where Invoice = '{0}' and Complete = 20 ", Invoice);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    isExists = true;
                }
                catch
                {
                    isExists = false;
                }
            }
            else
            {
                isExists = false;
            }

            conn.Close();

            return isExists;
        }


        private void CheckIfPayableInactive()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select top 1 a.inactive_date from tbl_REO_Properties a
                                            where a.property_code = '{0}'
                                            order by a.inactive_date desc", info.property_code);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    Inactive_Date = Convert.ToDateTime(rd[0].ToString());
                }
                catch
                {
                    Inactive_Date = DateTime.MinValue;
                }
            }

            conn.Close();

            if (Inactive_Date != DateTime.MinValue)
            {
                if (info.invoice_date != DateTime.MinValue)
                {
                    if (Inactive_Date < info.invoice_date)
                    {

                        if (info.vendor_group == "W" && isUnderASFI(info.property_code) == true)
                        {
                            info.Complete = 1;
                            info.PendingReasons = "Manual WI - ASFI";
                            info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                        }
                        else
                        {
                            info.VendorAssignedStatus = "Dont Assign - Inactive Property";
                            info.Complete = 12;
                            info.PendingReasons = "Inactive Property";
                            info.InvoiceFolderName = strCreateInvoiceFolderName(InactiveFolder);
                        }                       
                    }
                    else
                    {
                        info.VendorAssignedStatus = "New";
                        if ((info.client_code == "RESI" || info.client_code == "OLSR" || info.client_code == "PFC") && info.Complete != 9)
                        {
                            //info.VendorAssignedStatus = "ForAudit";
                        }
                        info.Complete = 1;
                        info.PendingReasons = "Manual WI";
                        info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                    }
                }

                if (info.ServiceFrom != info.ServiceTo)
                {
                    if (Inactive_Date < info.ServiceTo)
                    {
                        info.VendorAssignedStatus = "New";
                        if ((info.client_code == "RESI" || info.client_code == "OLSR" || info.client_code == "PFC") && info.Complete != 9)
                        {
                            //info.VendorAssignedStatus = "ForAudit";
                        }
                        info.Complete = 1;
                        info.PendingReasons = "Manual WI";
                        info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                    }
                    else
                    {
                        if (info.vendor_group == "W" && isUnderASFI(info.property_code) == true)
                        {
                            info.Complete = 1;
                            info.PendingReasons = "Manual WI - ASFI";
                            info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                        }
                        else
                        {
                            info.VendorAssignedStatus = "Dont Assign - Inactive Property";
                            info.Complete = 12;
                            info.PendingReasons = "Inactive Property";
                            info.InvoiceFolderName = strCreateInvoiceFolderName(InactiveFolder);
                        }
                    }
                }
            }
        }

        void bwRscApproval_DoWork(object sender, DoWorkEventArgs e)
        {
            if (!isTestingOnly)
            {
                try
                {
                    if (!string.IsNullOrEmpty(info.Invoice) &&
                        !string.IsNullOrEmpty(info.property_code) &&
                        !string.IsNullOrEmpty(info.VendorId) &&
                        !string.IsNullOrEmpty(info.Amount) &&
                        !string.IsNullOrEmpty(info.vendor_group) &&
                        !string.IsNullOrEmpty(info.InvoiceFolderName))
                    {
                        createRSCapprovalRequest(
                                           info.Invoice,
                                           info.property_code,
                                           info.VendorId,
                                           info.Amount,
                                           info.vendor_group,
                                           info.InvoiceFolderName);
                    }
                }
                catch
                {
                }
            }
        }

        private void createRSCapprovalRequest(string invoice, string property_code, string vendor_code, string amount,
            string vendor_type, string invoice_link)
        {
            Database.DBConnection.DatabaseRSCApproval conn = new Database.DBConnection.DatabaseRSCApproval();
            Database.DBConnection.DatabaseREO_Properties conn1 = new Database.DBConnection.DatabaseREO_Properties();
            Database.DBConnection.DatabaseUSP_Contact conn2 = new Database.DBConnection.DatabaseUSP_Contact();
            Database.DBConnection.DatabaseAssetManager conn3 = new Database.DBConnection.DatabaseAssetManager();
            Database.DB.REO_Properties prop_info = new Database.DB.REO_Properties();
            Database.DB.USP_Contact usp_info = new Database.DB.USP_Contact();
            Database.DB.AssetManager assetMgr = new Database.DB.AssetManager();

            prop_info = conn1.GetData(property_code);
            usp_info = conn2.GetUspData(vendor_code);
            assetMgr = conn3.GetDataProcessor(property_code);

            string full_address, Vendor_Name, property_status, asset_manager;

            try
            {
                asset_manager = assetMgr.asset_manager;
            }
            catch
            {
                asset_manager = string.Empty;
            }
            try
            {
                full_address = prop_info.full_address;
            }
            catch
            {
                full_address = string.Empty;
            }

            try
            {
                Vendor_Name = usp_info.Name;
            }
            catch
            {
                Vendor_Name = string.Empty;
            }
            try
            {
                property_status = prop_info.property_status;
            }
            catch
            {
                property_status = string.Empty;
            }


            try
            {
                conn.WriteData(new Database.DB.RSCApproval()
                {
                    amount = Convert.ToDouble(amount),
                    created_by = Environment.UserName,
                    date_created = DateTime.Now,
                    date_modified = DateTime.Now,
                    full_address = full_address,
                    invoice = invoice,
                    modified_by = Environment.UserName,
                    notes = "New Request " + DateTime.Now.ToShortDateString(),
                    property_code = property_code,
                    status = "Pending",
                    vendor_code = vendor_code,
                    vendor_name = Vendor_Name,
                    vendor_type = vendor_type,
                    property_status = property_status,
                    invoice_link = invoice_link,
                    asset_manager = asset_manager
                });
            }
            catch
            {
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (checkBox2.Checked == false && checkBox3.Checked == false)
            {
                var result = MessageBox.Show("Is this bill readable",
                                   "Is this bill readable",
                                   MessageBoxButtons.YesNo,
                                   MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    checkBox2.Checked = true;
                    checkBox3.Checked = false;
                }
                else
                {
                    checkBox2.Checked = false;
                    checkBox3.Checked = true;
                }

                return;
            }
            else
            {
                if (checkBox2.Checked == true && checkBox3.Checked == false)
                {
                    if (checkBox4.Checked == false && checkBox5.Checked == false && checkBox13.Checked == false && checkBox14.Checked == false)
                    {
                        MessageBox.Show("Please select if bill is Electricity, Gas, Water or Others");
                        SaveButtonsEnabled(true);
                        return;
                    }
                }
            }


            if (info.property_code != string.Empty && info.vendor_group != "None")
            {
                if (!isVendorGroupCompleted())
                {
                    MessageBox.Show("Please select if bill is Electricity, Gas or Water");
                    SaveButtonsEnabled(true);
                    return;
                }


                if (!isAccountNumberCompleted())
                {
                    MessageBox.Show("Please enter the account number. Enter 'none' if there is no any.");
                    SaveButtonsEnabled(true);
                    return;
                }


                if (!isDoNotMailSelected() && !checkBox8.Checked)
                {
                    MessageBox.Show("Please select if bill is Do Not Mail");
                    SaveButtonsEnabled(true);
                    return;
                }


                if (!isFinalBillSelected() && !checkBox8.Checked)
                {
                    MessageBox.Show("Please select if bill is Final Bill");
                    SaveButtonsEnabled(true);
                    return;
                }

                
                //if (!isHasTransAmtSelected())
                //{
                //    MessageBox.Show("Please select if bill has Transfer Amount");
                //    SaveButtonsEnabled(true);
                //    return;
                //}


                if (!isPreviousBalanceComplete())
                {
                    if (iniComplete != "0" && info.Complete != 3 && info.Complete != 32 && info.Complete != 31)
                    {
                        MessageBox.Show("Please enter previous balance. Enter '0.00' if there is no any.");
                        SaveButtonsEnabled(true);
                        return;
                    }
                }

                string sFrom = string.Empty;
                string sTo = string.Empty;

                if (info.ServiceFrom.ToShortDateString() == "1/1/0001")
                {
                    sFrom = string.Empty;
                }
                else
                {
                    sFrom = info.ServiceFrom.ToShortDateString();
                }

                if (info.ServiceTo.ToShortDateString() == "1/1/0001")
                {
                    sTo = string.Empty;
                }
                else
                { 
                    sTo = info.ServiceTo.ToShortDateString();
                }

                string summary = string.Format(@"Account Number: {0} Service Period: {1} - {2} Amount: {3} Type of Service: {4} USP: {5}",
                        info.AccountNumber + Environment.NewLine,
                        sFrom.ToString(), sTo.ToString() + Environment.NewLine,
                        info.Amount + Environment.NewLine,
                        info.vendor_group + Environment.NewLine,
                        info.usp_name);
                
                var DiagResult = MessageBox.Show(summary, "Do you want to proceed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                
                if (DiagResult == DialogResult.No)
                {
                    return;
                }
            }

            numOftries = 0;
            numOfSearch = 0;
            isDoneSearch = false;


            //MessageBox.Show(info.AccountNumber + " " + info.vendor_group);


            SaveItem(false);

        }

        private void GetVIDReqDtls()
        {

            if (dgv_usp.Rows.Count == 0)
            {

                string query = string.Format(@"select top 1 request_id, name, street_name + ', ' + city + ', ' + [state] + ', ' + postal_code [Address] from tbl_EM_VendorIDRequest where source_of_information = '{0}' order by date_requested desc", info.Invoice);

                SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);

                conn.Open();
                da.SelectCommand.CommandTimeout = 1800;
                da.Fill(dt);

                conn.Close();

                if (dt.Rows.Count > 0)
                {

                    info.VendorId = dt.Rows[0][0].ToString();
                    info.usp_name = dt.Rows[0][1].ToString();
                    info.vendor_address = dt.Rows[0][2].ToString();

                    dgv_usp.Rows.Add(dt.Rows[0][1].ToString(), dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString(), "Pending", "Pending");
                }
                else
                {


                    query = string.Format(@"select top 1 b.request_id, b.name, b.street_name + ', ' + b.city + ', ' + b.[state] + ', ' + b.postal_code [Address]
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_EM_VendorIDRequest b
                                            on a.vendor_account_requested = b.request_id
                                            where a.invoice_name = '{0}' order by a.date_requested desc", info.Invoice);
                    
                    da = new SqlDataAdapter(query, conn);

                    conn.Open();
                    
                    da.Fill(dt);

                    conn.Close();

                    if (dt.Rows.Count > 0)
                    {
                        info.VendorId = dt.Rows[0][0].ToString();
                        info.usp_name = dt.Rows[0][1].ToString();
                        info.vendor_address = dt.Rows[0][2].ToString();

                        dgv_usp.Rows.Add(dt.Rows[0][1].ToString(), dt.Rows[0][1].ToString(), dt.Rows[0][2].ToString(), "Pending", "Pending");
                    }
                    else
                    {
                        MessageBox.Show("USP is mandatory.");

                        return;
                    }
                }
            }
        }

        private bool isVendorGroupCompleted()
        {
            bool result = true;

            if (info.vendor_group != null && info.vendor_group != "")
            {
                if (info.vendor_group == string.Empty)
                {
                    result = false;
                }
            }
            else
            {

                if (checkBox4.Checked)
                {
                    info.vendor_group = "E";
                }
                else if (checkBox5.Checked)
                {
                    info.vendor_group = "G";
                }
                else if (checkBox13.Checked)
                {
                    info.vendor_group = "W";
                }
                else if (comboBox2.Text != string.Empty)
                {
                    switch (comboBox2.Text)
                    {
                        case "Garbage/Trash/Disposal":
                            info.vendor_group = "T";
                            break;

                        case "Stormwater/Drainage/WasteWater/Sewer":
                            info.vendor_group = "D";
                            break;

                        case "Septic":
                            info.vendor_group = "S";
                            break;
                    }
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        private bool isAccountNumberCompleted()
        {
            info.AccountNumber = textBox10.Text;

            bool result = true;

            if (info.AccountNumber != null)
            {
                if (info.AccountNumber == string.Empty)
                {
                    result = false;
                }
            }
            else
            {
                result = false;
            }

            return result;
        }

        private bool isPreviousBalanceComplete()
        {
            bool result = true;

            if (textBox2.Text == string.Empty)
            {
                result = false;
            }

            return result;
        }

        public void ComputeCompleted()
        {
            DateTime DateFrom = new DateTime();
            DateTime DateTo = new DateTime();
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            
            DateFrom = DateTime.Now.AddHours(-12);
            DateTo = DateTime.Now;

            label16.Invoke((Action)delegate
            {
                label16.Text = conn.GetProdOfUser(DateFrom, DateTo, Environment.UserName.ToLower()).ToString();
            });

            label46.Invoke((Action)delegate
            {
                decimal hours = conn.GetHoursOfUser(DateFrom, DateTo, Environment.UserName.ToLower());

                if (hours == 0)
                {
                    label46.Text = string.Empty;
                }
                else
                {
                    label46.Text = string.Format("{0:N2}", Convert.ToDecimal(label16.Text) / hours);
                }
            });

            label19.Invoke((Action)delegate
            {
                try
                {
                    label19.Text = conn.GetHighestCompleted(DateFrom, DateTo).ToString();
                }
                catch
                { }
            });

            label45.Invoke((Action)delegate
            {
                decimal hours = 0;

                try
                {
                    hours = conn.GetHoursOfHighestUser(DateFrom, DateTo);
                }
                catch
                { }

                if (hours == 0)
                {
                    label45.Text = string.Empty;
                }
                else
                {
                    label45.Text = string.Format("{0:N2}", Convert.ToDecimal(label19.Text) / hours);
                }
            });
        }
        
        public void TransferInvoice(string previousinvoicelocation, string nextinvoicelocation)
        {
            bool hasError = true;
            int i = 0;

            string error = string.Empty;
           
            while (hasError)
            {
                try
                {
                    Uri myUri = new Uri(HiddenTempFile);

                    if (pdfviewer.InvokeRequired)
                    {
                        pdfviewer.Invoke((Action)delegate
                        {
                            pdfviewer.AllowNavigation = true;
                            pdfviewer.Navigate(myUri);
                            pdfviewer.Refresh();
                        });
                    }
                    else
                    {
                        pdfviewer.AllowNavigation = true;
                        pdfviewer.Navigate(myUri);
                        pdfviewer.Refresh();
                    }
                    
                    waitLoadPDF();
                    
                    File.Move(previousinvoicelocation, nextinvoicelocation);
                    hasError = false;
                }
                catch (Exception ex)
                {
                    hasError = true;
                    error += string.Format("Error: {0}{1}", ex.Message, Environment.NewLine + Environment.NewLine);

                    System.Threading.Thread.Sleep(500);
                    i++;
                }
                finally
                {
                    if (i > 20)
                    {
                        hasError = false;

                        //MessageBox.Show("Unable to unlock invoice. Error: " + error);
                    }
                }
            }
        }

        public void Clear(bool refreshonly, Control[] doNotTouch)
        {
            Control[] controlsToBeHidden = new Control[] { chkBx_NoPropertyAddress, checkBox8, checkBox9,
                checkBox10, checkBox7, checkBox6, checkBox2, checkBox3, checkBox15, checkBox16, chkTransAmtYes, chkTransAmtNo,
                dataGridView1, groupBox7, groupBox5, groupBox8,
                label14, lbl_UspOnInvoice, label2, label12, label13, label39, label4, label47, label58, label38,
                textBox2, textBox6, textBox8, textBox11, textBox2, textBox10, textBox33,
                cmboBx_UspOnInvoice, comboBox8, comboBox5, comboBox2, 
                dateTimePicker1, dateTimePicker2, dateTimePicker3, 
                button5, btn_AddMorePaymentBreakdown, button6, button_noMatchUSP, button7, button9, button11,
                dgv_usp, pictureBox5, label60, chkASFIYes, chkASFINo,
                label58, chkTransAmtYes, chkTransAmtNo, lblCount, lblOf, label62, chkBalInfoYes, chkBalInfoNo, 
                label63, chkBalAmtYes, chkBalInfoNo
            };

            button4.Enabled = false;    //button for retrieve E-Bills

            //---------------------------------------------------
            //          Set DateTimePicker To Date Today
            //---------------------------------------------------
            dateTimePicker1.Value = DateTime.Today.AddDays(-1);
            dateTimePicker2.Value = DateTime.Today.AddDays(-1);
            dateTimePicker3.Value = DateTime.Today.AddDays(-1);
            dateTimePicker4.Value = DateTime.Today.AddDays(-1);

            //---------------------------------------------------
            //              Clear the Search Variables
            //---------------------------------------------------
            hasDoneHouseNoZipSearch = false;
            hasDoneAcctSearch = false;
            hasDoneHouseNoStateSearch = false;
            hasDoneHouseNoStreetSearch = false;
            hasDoneHouseNoCitySearch = false;
            HouseNoCitySearcHasResults = false;
            HouseNoStateSearchHasResults = false;
            HouseNoStreetSearchHasResults = false;
            HouseNoZipSearchHasResults = false;
            hasZipcodeOnInvoice = false;
            acctSearchHasResults = false;
            isSearchHasResults = false;
            uspSearchHasResults = false;
            HouseNoZipSearchHasResults = false;
            //numOftries = 0;
            //numOfSearch = 0;
            //isDoneSearch = false;

            //---------------------------------------------------
            //              Clear remaining switch
            //---------------------------------------------------
            isOutOfREO = false;
            isTrailingAsset = false;
            uspHasNoMatch = false;
            VendorIDRequestType = string.Empty;    //Activation or CreateNew
            isInactive = false;
            saveVDR = false;
            hasMultiplePropertyOnInvoice = false;
            isBalTransfer = false;

            chkASFIYes.Checked = false;
            chkASFINo.Checked = false;
            chkTransAmtYes.Checked = false;
            chkTransAmtNo.Checked = false;
            chkBalInfoYes.Checked = false;
            chkBalInfoNo.Checked = false;
            chkBalAmtYes.Checked = false;
            chkBalInfoNo.Checked = false;
            numBalTransfer.Value = 0;

            //---------------------------------------------------
            //                  Hide controls
            //---------------------------------------------------
            foreach (Control ctr in controlsToBeHidden)
            {
                if(!doNotTouch.Contains(ctr))
                {
                    if (ctr.HasChildren)
                    {
                        foreach (Control c in ctr.Controls)
                        {
                            if (!doNotTouch.Contains(c))
                            {
                                c.Visible = false;

                                if (c.GetType() == typeof(TextBox) || c.GetType() == typeof(ComboBox))
                                {
                                    c.Text = string.Empty;
                                }

                                if (c.GetType() == typeof(CheckBox))
                                {
                                    ((CheckBox)c).Checked = false;
                                }

                                if (c.GetType() == typeof(ComboBox))
                                {
                                    ((ComboBox)c).SelectedIndex = -1;
                                }
                            }
                        }

                        ctr.Visible = false;
                    }
                    else
                    {
                        ctr.Visible = false;

                        if (ctr.GetType() == typeof(TextBox) || ctr.GetType() == typeof(ComboBox))
                        {
                            ctr.Text = string.Empty;
                        }

                        if (ctr.GetType() == typeof(CheckBox))
                        {
                            ((CheckBox)ctr).Checked = false;
                        }
                    }
                }
            }

            checkBox2.Visible = true;
            checkBox3.Visible = true;

            if (!refreshonly)
            {
                //---------------------------------------------------
                //          Refresh EM Data
                //---------------------------------------------------
                info = new Database.DB.EM_Tasks();
                info.userid = Environment.UserName;
                info.AccountNumber = "None";
                info.vendor_group = "None";
                info.final_bill = "No";
                info.HasTransferAmount = "No";
                info.PreviousBalance = 0;
                this.counter = 0;
                multiplePropertyCount = 0;
                Inactive_Date = new DateTime();
                depositAmount = 0;
                depositRefund = 0;
                

                //---------------------------------------------------
                //              Clear the Payment Breakdown
                //---------------------------------------------------
                paymentBreakdown = new List<PaymentClass>();

                //---------------------------------------------------
                //              Clear Browser
                //---------------------------------------------------
                Uri myUri = new Uri(HiddenTempFile);
                pdfviewer.Navigate(myUri);
                pdfviewer.ScriptErrorsSuppressed = true;
                pdfviewer.Refresh();

                if (pdfviewer.ReadyState != WebBrowserReadyState.Complete &&
                    pdfviewer.ReadyState != WebBrowserReadyState.Uninitialized)
                {
                    while (pdfviewer.ReadyState != WebBrowserReadyState.Complete)
                    {
                        Application.DoEvents();
                    }
                }
            }
        }

        public void ResetBillingInformation()
        {
            Control[] controlsToShow = new Control[] { groupBox5, label22, dateTimePicker4, checkBox1, pictureBox6 };
            Control[] controlsToHide = new Control[] { label2, label12, label13, label34, dateTimePicker1, dateTimePicker2, 
                                                        dateTimePicker3, checkBox9, checkBox10, checkBox11, checkBox12 };

            foreach (Control c in controlsToShow)
            {
                c.Visible = true;
            }

            foreach (Control c in controlsToHide)
            {
                c.Visible = false;
            }

            //checkBox12.Checked = true;

            dateTimePicker1.MinDate = DateTime.Now.AddYears(-2);
            dateTimePicker1.MaxDate = DateTime.Now.AddYears(2);

            dateTimePicker2.MinDate = DateTime.Now.AddYears(-2);
            dateTimePicker2.MaxDate = DateTime.Now.AddYears(2);

            dateTimePicker3.MinDate = DateTime.Now.AddYears(-2);
            dateTimePicker3.MaxDate = DateTime.Now.AddYears(2);

            dateTimePicker4.MinDate = DateTime.Now.AddYears(-2);
            dateTimePicker4.MaxDate = DateTime.Now.AddYears(2);

            dateTimePicker5.MinDate = DateTime.Now.AddYears(-2);
            dateTimePicker5.MaxDate = DateTime.Now.AddYears(2);
        }

        public DateTime GetUtilityTurnOffDate(string property_code, string vendorType)
        {
            Database.DBConnection.DatabasePropertyToUSP conn = new Database.DBConnection.DatabasePropertyToUSP();
            Database.DB.PropertyToUSP data = new Database.DB.PropertyToUSP();
            DateTime TurnOffDate = new DateTime();

            try
            {
                data = conn.GetDataProcessor(property_code);

                switch (vendorType)
                {
                    case "E": TurnOffDate = data.electricity_turnoff_date; break;
                    case "W": TurnOffDate = data.water_turnoff_date; break;
                    case "G": TurnOffDate = data.gas_turnoff_date; break;
                    default: TurnOffDate = DateTime.MinValue; break;
                }
            }
            catch
            {
                TurnOffDate = DateTime.MinValue;
            }

            return TurnOffDate;
        }

        private bool CheckIfTrailingAsset(string investor_code, double numberOfDaysOREO)
        {
            Database.DBConnection.DatabaseProperty_Investors conn = new Database.DBConnection.DatabaseProperty_Investors();
            Database.DB.Property_Investors data = new Database.DB.Property_Investors();

            try
            {
                data = conn.GetDataProcessor(investor_code);
                if (data != null)
                {
                    float RequiredNumberOfDays = data.number_of_days;

                    if (numberOfDaysOREO > RequiredNumberOfDays)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else { return false; }
            }
            catch
            {
                return false;
            }
        }

        public void CheckIfOutOfREO(int RowIndex)
        {
            DateTime rcDate = new DateTime();

            DateTime reosrcDate = (DateTime)dataGridView1.Rows[RowIndex].Cells["cReosrcDate"].Value;
            DateTime reoslcDate = (DateTime)dataGridView1.Rows[RowIndex].Cells["cReoslcDate"].Value;
            DateTime reosfcDate = (DateTime)dataGridView1.Rows[RowIndex].Cells["cReosfcDate"].Value;
            string investorName = dataGridView1.Rows[RowIndex].Cells["cInvestorName"].Value.ToString().ToString();

            if (reosrcDate != DateTime.MinValue)
            {
                rcDate = reosrcDate;
            }

            if (reoslcDate != DateTime.MinValue)
            {
                rcDate = reoslcDate;
            }

            if (reosfcDate != DateTime.MinValue)
            {
                rcDate = reosfcDate;
            }

            if (rcDate == null || rcDate == DateTime.MinValue)
            {
                isOutOfREO = false;
            }
            else
            {
                TimeSpan OutOfREO = DateTime.Today.Date - rcDate;

                // Check if property is a trailing asset
                isTrailingAsset = false;
                isTrailingAsset = CheckIfTrailingAsset(investorName, OutOfREO.Days);

                if (isTrailingAsset == true)
                {
                    isOutOfREO = true;
                }
                else if (OutOfREO.Days > 45)
                {
                    //--------------------------------------
                    // Need process flow for OREO > 45 days
                    //--------------------------------------
                    isOutOfREO = true;
                }
                else
                {
                    isOutOfREO = false;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {

            //if (chkbx_noZipCode.Checked == true)
            //{
            //    chkbx_noZipCode.Enabled = false;
            //}

            if (!hasDoneAcctSearch)
            {
                hasDoneAcctSearch = true;

                SearchPropertyUsingAccountNumber();

                ResetPropertyInformation();

                if (acctSearchHasResults)
                {
                    button6.Visible = true;
                }
                else
                {
                    button7.Visible = true;
                }
            }
            else
            {
                isSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, textBox16.Text, textBox17.Text, textBox1.Text);

                if (!isSearchHasResults)
                {
                    numOftries += 1;
                    label57.Text = numOftries.ToString();
                }
                else
                {
                    button6.Visible = true;
                }
                                
                if (numOftries == 1)
                {
                    if (chkbx_noZipCode.Checked == false)
                    {
                        pnlAddress.Visible = true;
                    }

                    textBox14.Text = string.Empty;

                    label6.Visible = false;
                    textBox17.Visible = false;

                    label7.Visible = false;
                    textBox16.Visible = false;
                }

                if (numOftries == 2)
                {

                    textBox17.Text = string.Empty;
                    label6.Visible = true;
                    textBox17.Visible = true;

                    textBox16.Text = string.Empty;
                    label7.Visible = true;
                    textBox16.Visible = true;
                }

                if (numOftries == 4)
                {

                    numOfSearch += 1;

                    if (numOfSearch == 3 && isDoneSearch == false)
                    {

                        numOfSearch = 0;
                        isDoneSearch = true;

                        if (isSearchHasResults == false)
                        {
                            var res = MessageBox.Show("No match found. Do you want to search again?",
                            "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                            if (res == DialogResult.No)
                            {

                                button5.Visible = true;
                                //info.InvoiceFolderName = strCreateInvoiceFolderName(PropertyNotFoundFolder);

                                if (info.Complete == 0)
                                {
                                    info.Complete = 31;
                                    info.PendingReasons = "PNF1";
                                    return;
                                }

                                if (info.Complete == 20)
                                {
                                    info.Complete = 31;
                                    info.PendingReasons = "PNF1";
                                    return;
                                }

                                if (info.Complete == 31)
                                {
                                    info.Complete = 32;
                                    info.PendingReasons = "PNF2";
                                    return;
                                }

                                if (info.Complete == 32)
                                {
                                    info.Complete = 21;
                                    info.PendingReasons = "Pending for Audit";
                                    return;
                                }
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("No match found. Please try searching again.", "Property Search", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        //if (!chkTransAmtYes.Checked)
                        //{
                        //    Clear(true,  new Control[] { label14, textBox10, checkBox6, chkBx_NoPropertyAddress, checkBox2, checkBox3, pictureBox5, label47, label58, chkASFIYes, chkASFINo,
                        //    chkTransAmtYes, chkTransAmtNo, label59, numBalTransfer, label60, label62, label63, chkBalInfoYes, chkBalInfoNo, label63, chkBalAmtYes, chkBalAmtNo, checkBox15, 
                        //    checkBox16, groupBox8});
                           
                        //}

                        label14.Visible = true;
                        textBox10.Visible = true;
                        checkBox6.Visible = true;
                        pictureBox5.Visible = true;

                        textBox4.Clear();
                        textBox1.Clear();
                        textBox14.Clear();
                        textBox17.Clear();
                        textBox16.Clear();
                        chkbx_noZipCode.Checked = false;
                        pnlAddress.Visible = false;
                        dataGridView1.Visible = false;

                        dataGridView1.Enabled = true;

                        numOftries = 0;
                    }                  
                }                
            }
           

            
            //if (hasDoneAcctSearch)
            //{
            //    //if (textBox1.Text == string.Empty || textBox4.Text == string.Empty) { hasDoneHouseNoZipSearch = true; }

            //    if (hasDoneHouseNoZipSearch)
            //    {
            //        //if (textBox4.Text == string.Empty || textBox14.Text == string.Empty) { hasDoneHouseNoStreetSearch = true; }

            //        if (hasDoneHouseNoStreetSearch)
            //        {
            //            //if (textBox4.Text == string.Empty || textBox16.Text == string.Empty) { hasDoneHouseNoCitySearch = true; }

            //            if (hasDoneHouseNoCitySearch)
            //            {
            //                //if (textBox4.Text == string.Empty || textBox17.Text == string.Empty) { hasDoneHouseNoStateSearch = true; }

            //                if (hasDoneHouseNoStateSearch)
            //                {
            //                    if (string.IsNullOrEmpty(textBox4.Text) &&
            //                        string.IsNullOrEmpty(textBox14.Text) &&
            //                        string.IsNullOrEmpty(textBox16.Text) &&
            //                        string.IsNullOrEmpty(textBox17.Text) &&
            //                        string.IsNullOrEmpty(textBox1.Text))
            //                    {
            //                        MessageBox.Show("Please enter address to search", "Search Property Address");
            //                        textBox4.Focus();
            //                    }
            //                    else
            //                    {
            //                        SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, textBox16.Text, textBox17.Text, textBox1.Text);

            //                        //button5.Visible = true;

            //                        //var res = MessageBox.Show("Would you like to change the information you put in the address field?",
            //                        //    "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            //                        //if (res == DialogResult.Yes)
            //                        //{
            //                        //    button9_Click(null, null);
            //                        //}
            //                        //else
            //                        //{
            //                        //    info.Complete = 3;
            //                        //    info.InvoiceFolderName = strCreateInvoiceFolderName(PropertyNotFoundFolder);
            //                        //}
            //                    }
            //                }
            //                else
            //                {
            //                    HouseNoStateSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, textBox16.Text, textBox17.Text, textBox1.Text);
            //                    hasDoneHouseNoStateSearch = true;

            //                    //if (!HouseNoStreetSearchHasResults)
            //                    //{
            //                    //    button5.Visible = true;

            //                    //    var res = MessageBox.Show("Would you like to change the information you put in the address field?",
            //                    //        "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            //                    //    if (res == DialogResult.Yes)
            //                    //    {
            //                    //        ResetPropertyInformation();
            //                    //    }
            //                    //    else
            //                    //    {
            //                    //        info.Complete = 3;
            //                    //        info.InvoiceFolderName = strCreateInvoiceFolderName(PropertyNotFoundFolder);

            //                    //        MessageBox.Show("Please click on SAVE!");
            //                    //    }
            //                    //}
            //                }
            //            }
            //            else
            //            {
            //                HouseNoCitySearcHasResults = SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, textBox16.Text, textBox17.Text, textBox1.Text);
            //                hasDoneHouseNoCitySearch = true;

            //                //if (!HouseNoCitySearcHasResults)
            //                //{
            //                //    HouseNoStreetSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, "", "", "");
            //                //    hasDoneHouseNoStreetSearch = true;

            //                //    if (!HouseNoStreetSearchHasResults)
            //                //    {
            //                //        HouseNoStateSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, "", "", textBox17.Text, "");
            //                //        hasDoneHouseNoStateSearch = true;

            //                //        if (!HouseNoStreetSearchHasResults)
            //                //        {
            //                //            button5.Visible = true;

            //                //            var res = MessageBox.Show("Would you like to change the information you put in the address field?",
            //                //                "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            //                //            if (res == DialogResult.Yes)
            //                //            {
            //                //                ResetPropertyInformation();
            //                //            }
            //                //            else
            //                //            {
            //                //                info.Complete = 3;
            //                //                info.InvoiceFolderName = strCreateInvoiceFolderName(PropertyNotFoundFolder);

            //                //                MessageBox.Show("Please click on SAVE!");
            //                //            }
            //                //        }
            //                //    }
            //                //}
            //            }
            //        }
            //        else
            //        {
            //            HouseNoStreetSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, textBox16.Text, textBox17.Text, textBox1.Text);
            //            hasDoneHouseNoStreetSearch = true;

            //            //if (!HouseNoStreetSearchHasResults)
            //            //{
            //            //    HouseNoStateSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, "", "", textBox17.Text, "");
            //            //    hasDoneHouseNoStateSearch = true;

            //            //    if (!HouseNoStreetSearchHasResults)
            //            //    {
            //            //        button5.Visible = true;

            //            //        var res = MessageBox.Show("Would you like to change the information you put in the address field?",
            //            //            "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            //            //        if (res == DialogResult.Yes)
            //            //        {
            //            //            ResetPropertyInformation();
            //            //        }
            //            //        else
            //            //        {
            //            //            info.Complete = 3;
            //            //            info.InvoiceFolderName = strCreateInvoiceFolderName(PropertyNotFoundFolder);

            //            //            MessageBox.Show("Please click on SAVE!");
            //            //        }
            //            //    }
            //            //}
            //        }
                    
            //    }
            //    else
            //    {
            //        HouseNoZipSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, "", "", "", textBox1.Text);
            //        hasDoneHouseNoZipSearch = true;

            //        if (dataGridView1.Rows.Count == -1)
            //        {
            //            pnlAddress.Visible = true;
            //            label5.Visible = true;
            //            textBox14.Visible = true;
            //            pictureBox4.Visible = true;

            //            label6.Visible = false;
            //            textBox17.Visible = false;

            //            label7.Visible = false;
            //            textBox16.Visible = false;

            //            numOftries += 1;
            //            return;
            //        }


            //        if (dataGridView1.Rows.Count > 0)
            //        {
            //            if (dataGridView1.Rows[0].Cells[0].Value == "PROPERTY NOT FOUND")
            //            {
            //                pnlAddress.Visible = true;
            //                label5.Visible = true;
            //                textBox14.Visible = true;
            //                pictureBox4.Visible = true;

            //                label6.Visible = false;
            //                textBox17.Visible = false;

            //                label7.Visible = false;
            //                textBox16.Visible = false;

            //                numOftries += 1;
            //                return;
            //            }                        
            //        }

            //        //if (!HouseNoZipSearchHasResults)
            //        //{
            //        //    HouseNoCitySearcHasResults = SearchPropertyUsingAddress(textBox4.Text, "", textBox16.Text, "", "");
            //        //    hasDoneHouseNoCitySearch = true;

            //        //    if (!HouseNoCitySearcHasResults)
            //        //    {
            //        //        HouseNoStreetSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, "", "", "");
            //        //        hasDoneHouseNoStreetSearch = true;

            //        //        if (!HouseNoStreetSearchHasResults)
            //        //        {
            //        //            HouseNoStateSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, "", "", textBox17.Text, "");
            //        //            hasDoneHouseNoStateSearch = true;

            //        //            if (!HouseNoStreetSearchHasResults)
            //        //            {
            //        //                button5.Visible = true;

            //        //                var res = MessageBox.Show("Would you like to change the information you put in the address field?",
            //        //                    "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            //        //                if (res == DialogResult.Yes)
            //        //                {
            //        //                    ResetPropertyInformation();
            //        //                }
            //        //                else
            //        //                {
            //        //                    info.Complete = 3;
            //        //                    info.InvoiceFolderName = strCreateInvoiceFolderName(PropertyNotFoundFolder);

            //        //                    MessageBox.Show("Please click on SAVE!");
            //        //                }
            //        //            }
            //        //        }
            //        //    }
            //        //}
            //    }
            //}
            //else
            //{
            //    if (!string.IsNullOrEmpty(textBox10.Text))
            //    {
            //        SearchPropertyUsingAccountNumber();
            //        hasDoneAcctSearch = true;

            //        ResetPropertyInformation();

            //        if (acctSearchHasResults)
            //        {
            //            button6.Visible = true;
            //        }
            //        else
            //        {
            //            button7.Visible = true;
            //        }
            //    }
            //    else
            //    {
            //        MessageBox.Show("Please type the Account Number", "Account Number Search");
            //        textBox10.Focus();
            //    }
            //}

            //if (hasDoneAcctSearch && hasDoneHouseNoZipSearch && hasDoneHouseNoCitySearch && hasDoneHouseNoStreetSearch && hasDoneHouseNoStateSearch && numOftries < 3)
            //{
            //    numOftries += 1;
            //    MessageBox.Show("No. of search attempt : " + numOftries.ToString() + " Please try searching again.", "Property Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    ResetPropertyInformation();
            //}

            //if (hasDoneAcctSearch && hasDoneHouseNoZipSearch && hasDoneHouseNoCitySearch && hasDoneHouseNoStreetSearch && hasDoneHouseNoStateSearch && numOftries == 3)
            //{
            //    numOftries = 0;
            //    //info.Complete = 3;
            //    //info.InvoiceFolderName = strCreateInvoiceFolderName(PropertyNotFoundFolder);
            //    //MessageBox.Show("Please click on SAVE!");
            //    button5.Visible = true;
            //}

        }

        void bwLoadingPicture_DoWork(object sender, DoWorkEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void ResetPropertyInformation()
        {
            groupBox8.Visible = true;

            foreach (Control c in groupBox8.Controls)
            {
                //c.Visible = true;
                if (c.GetType() == typeof(TextBox) || c.GetType() == typeof(ComboBox))
                {
                    c.Text = string.Empty;
                }
            }


            label26.Visible = true;
            textBox4.Visible = true;
            pictureBox3.Visible = true;
            label1.Visible = true;
            textBox1.Visible = true;
            chkbx_noZipCode.Checked = false;
            chkbx_noZipCode.Visible = true;
            chkbx_noZipCode.Enabled = true;
            pnlAddress.Visible = false;
            chkBx_NoPropertyAddress.Visible = true;
            //checkBox8.Visible = true;
        }

        private void SearchPropertyUsingAccountNumber()
        {
            Database.DBConnection.DatabaseAccountNumber conn = new Database.DBConnection.DatabaseAccountNumber();
            Database.DBConnection.DatabaseREO_Properties conn1 = new Database.DBConnection.DatabaseREO_Properties();
            List<string> listOfPropertyCodes = new List<string>();
            List<Database.DB.REO_Properties> listOfPropDisplay = new List<Database.DB.REO_Properties>();
            List<Database.DB.AccountNumber> listOfAcctProp = new List<Database.DB.AccountNumber>();

            bool ShowData = true;

            // Clear the properties datagridview
            dataGridView1.Rows.Clear();

            if (textBox10.Text != string.Empty)
            {
                try { listOfAcctProp = conn.GetDataProcessor(textBox10.Text.Trim(), info.vendor_group); }
                catch (Exception ex)
                {
                    listOfAcctProp = null;
                    MessageBox.Show("An error occurred when retrieving data for account numbers. " +
                        Environment.NewLine + "Error: " + ex.Message);
                }
            }

            dataGridView1.Visible = true;

            if (listOfAcctProp != null)
            {
                dataGridView1.Rows.Clear();

                foreach (Database.DB.AccountNumber acctData in listOfAcctProp)
                {
                    Database.DB.REO_Properties propInfo = new Database.DB.REO_Properties();
                    propInfo = conn1.GetData(acctData.property_code);

                    if (propInfo != null)
                    {
                        listOfPropDisplay.Add(propInfo);
                    }
                }

                foreach (Database.DB.REO_Properties propInfo in listOfPropDisplay)
                {
                    if (listOfPropDisplay.Count(p => p.full_address.ToLower().Replace(" ", "") == propInfo.full_address.ToLower().Replace(" ", "")) > 1)
                    {
                        if (propInfo.active.Trim() == "Active")
                        {
                            ShowData = true;
                        }
                        else
                        {
                            ShowData = false;
                        }
                    }
                    else
                    {
                        ShowData = true;
                    }

                    if (ShowData == true)
                    {
                        //DisplayPropertyInDGV(propInfo, listOfAcctProp.First(p => p.property_code == propInfo.property_code));

                        DisplayPropertyInDGV(propInfo, listOfAcctProp.First(p => p.property_code == propInfo.property_code.ToString().Substring(0, p.property_code.Length)));

                        acctSearchHasResults = true;
                    }
                }
            }
            else
            {
                acctSearchHasResults = false;

                dataGridView1.Visible = true;
                dataGridView1.Rows.Clear();
                dataGridView1.Rows.Add(null, "NO PROPERTY FOUND", null, null, null, null, null, null);
                dataGridView1.Enabled = false;
            }
            hasDoneAcctSearch = true;
        }

        private bool SearchPropertyUsingAddress(string houseNo, string streetName, string cityName, string stateName, string zipcodeNumber)
        {
            Database.DBConnection.DatabaseREO_Properties conn = new Database.DBConnection.DatabaseREO_Properties();
            Database.DBConnection.DatabaseRRProperties conn1 = new Database.DBConnection.DatabaseRRProperties();
            List<Database.DB.REO_Properties> listOfProperties = new List<Database.DB.REO_Properties>();
            List<Database.DB.RRProperties> listOfRRProperties = new List<Database.DB.RRProperties>();
            List<string> listOfDisplayedProperties = new List<string>();

            //----------------------------------------------
            //          Remove 0 from zipcode
            //----------------------------------------------
            if (zipcodeNumber != string.Empty)
            {
                if (zipcodeNumber.StartsWith("0"))
                {
                    zipcodeNumber = textBox1.Text.Substring(1);
                }
                else
                {
                    zipcodeNumber = textBox1.Text;
                }
            }

            try
            {
                listOfProperties = conn.SearchActiveProperty(houseNo, streetName, cityName, stateName, zipcodeNumber);
                //listOfRRProperties = conn1.SearchProperty(houseNo, streetName, cityName, zipcodeNumber);
            }
            catch
            {
                listOfProperties = null;
                //listOfRRProperties = null;
            }

            bool result = false;

            //----------------------------------------------------
            //          Enable property list controls
            //----------------------------------------------------
            dataGridView1.Visible = true;
            dataGridView1.Enabled = true;
            dataGridView1.Visible = true;
            dataGridView1.Enabled = true;

            // Clear the properties datagridview
            dataGridView1.Rows.Clear();

            if (listOfProperties != null)
            {
                foreach (Database.DB.REO_Properties p in listOfProperties)
                {
                    if (listOfDisplayedProperties.Count(s => s.ToLower().Replace(" ", "") == p.full_address.ToLower().Replace(" ", "")) == 0)
                    {
                        DisplayPropertyInDGV(p, info.vendor_group);

                        listOfDisplayedProperties.Add(p.full_address);

                        result = true;
                    }
                }

                //if (listOfRRProperties != null)
                //{
                //    foreach (Database.DB.RRProperties r in listOfRRProperties)
                //    {
                //        DisplayPropertyInDGV(r);
                //        result = true;
                //    }
                //}
            }
            //else if (listOfRRProperties != null)
            //{
            //    if (listOfProperties == null)
            //    {
            //        foreach (Database.DB.RRProperties r in listOfRRProperties)
            //        {
            //            DisplayPropertyInDGV(r);
            //            result = true;
            //        }
            //    }
            //    else
            //    {
            //        try
            //        {
            //            dataGridView1.Rows.Add(
            //                null,
            //                "NO PROPERTY FOUND",
            //                null,
            //                null,
            //                null,
            //                null,
            //                null,
            //                null
            //                );

            //            result = false;
            //        }
            //        catch
            //        { }
            //    }
            //}
            else
            {
                try
                {
                    dataGridView1.Rows.Add(
                        null,
                        "NO PROPERTY FOUND",
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                        );

                    result = false;
                }
                catch
                { }
            }

            if (result == true)
            {
                label39.Visible = true;
                //button6.Visible = true;
            }
            else
            {
                label39.Visible = false;
                button6.Visible = false;
            }

            return result;
        }

        private void DisplayPropertyInDGV(Database.DB.REO_Properties p, string vendor_group)
        {
            Database.DBConnection.DatabaseAccountNumber conn = new Database.DBConnection.DatabaseAccountNumber();
            List<Database.DB.AccountNumber> acctNos = new List<Database.DB.AccountNumber>();
            acctNos = conn.GetAccountNumbers(p.property_code, vendor_group);

            string strAcctNos = string.Empty;

            try
            {
                if (acctNos != null)
                {
                    foreach (Database.DB.AccountNumber acct in acctNos)
                    {
                        if (strAcctNos == string.Empty)
                        {
                            strAcctNos = acct.account_number;
                        }
                        else
                        {
                            strAcctNos = strAcctNos + ", " + acct.account_number;
                        }
                    }
                }

                dataGridView1.Invoke((Action)delegate
                {
                    bool display = true;

                    foreach (DataGridViewRow dr in dataGridView1.Rows)
                    {
                        if (dr.Cells[0].Value.ToString().Trim() == p.property_code &&
                            dr.Cells[1].Value.ToString().Trim() == p.full_address &&
                            dr.Cells[3].Value.ToString().Trim() == p.active)
                        {
                            display = false;
                        }
                    }

                    if (display)                    
                    {
                        dataGridView1.Rows.Add(
                            p.property_code,
                            p.full_address,
                            p.customer_name,
                            p.active,
                            p.investor_name,
                            p.reosrc_date,
                            p.reoslc_date,
                            p.reosfc_date,
                            strAcctNos,
                            p.pod,
                            p.property_status_change_date,
                            p.inactive_date
                            );
                    }
                });
            }
            catch
            {
                // do nothing
            }
        }

        private void DisplayNullPropertyInDGV(Database.DB.REO_Properties p)
        {
            try
            {
                dataGridView1.Invoke((Action)delegate
                {
                    dataGridView1.Rows.Add(
                        null,
                        "NO PROPERTY FOUND",
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                        );
                });
            }
            catch
            {
                // do nothing
            }
        }
        

        private void DisplayPropertyInDGV(Database.DB.REO_Properties p, Database.DB.AccountNumber a)
        {
            try
            {
                dataGridView1.Rows.Add(
                    p.property_code,
                    p.full_address,
                    p.customer_name,
                    p.active,
                    p.investor_name,
                    p.reosrc_date,
                    p.reoslc_date,
                    p.reosfc_date,
                    a.account_number,
                    p.pod,
                    p.property_status_change_date,
                    p.inactive_date
                    );
            }
            catch
            {
                // do nothing
            }
        }

        private void DisplayPropertyInDGV(Database.DB.RRProperties p)
        {
            try
            {
                dataGridView1.Rows.Add(p.property_id, p.street_name, "RR", "Active", string.Empty, null, null, null,null,null,null);
            }
            catch
            {
                // do nothing
            }
        }

        private bool SearchPropertyFromRR()
        {
            bool result = true;
            Database.DBConnection.DatabaseRRProperties conn = new Database.DBConnection.DatabaseRRProperties();
            List<Database.DB.RRProperties> listOfRRProperties = new List<Database.DB.RRProperties>();

            // Clear the properties datagridview
            dataGridView1.Rows.Clear();

            listOfRRProperties = conn.SearchProperty(textBox4.Text, textBox14.Text, textBox16.Text, textBox1.Text);

            if (listOfRRProperties != null)
            {
                foreach (Database.DB.RRProperties rrProp in listOfRRProperties)
                {
                    try
                    {
                        string fulladdress = string.Format("{0}, {1}, {2}, {3}",
                            rrProp.street_number,
                            rrProp.street_name,
                            rrProp.city,
                            rrProp.zip);

                        dataGridView1.Rows.Add(
                            rrProp.property_id,
                            fulladdress,
                            "RR",
                            "Active",
                            "",
                            DateTime.MinValue,
                            DateTime.MinValue,
                            DateTime.MinValue,
                            null,
                            null,
                            null,
                            null
                            );
                    }
                    catch
                    {
                        // do nothing
                    }
                }
            }
            else
            {
                result = false;
            }
            return result;
        }

        private void button9_Click(object sender, EventArgs e)
        {


            //Clear(true, new Control[] { label14, label30, label38, label47, label58,
            //    textBox10, textBox14, textBox16, textBox17,
            //    checkBox6, checkBox2, checkBox3, checkBox4, checkBox5, checkBox13, checkBox14,
            //    checkBox15, checkBox16, chkTransAmtYes, chkTransAmtNo,
            //    button7, button9, button11,
            //    comboBox2,
            //    pictureBox5,
            //    label62, label63,
            //    chkBalInfoYes, chkBalInfoNo,
            //    chkBalAmtYes, chkBalAmtNo,
            //    label61, cmbBillName
            //});


            hasDoneAcctSearch = false;

            label14.Visible = true;
            textBox10.Visible = true;
            checkBox6.Visible = true;
            pictureBox5.Visible = true;

            groupBox8.Visible = false;
            chkBx_NoPropertyAddress.Visible = false;
            chkBx_NoPropertyAddress.Checked = false;
            textBox10.Clear();
            textBox4.Clear();
            textBox1.Clear();
            textBox14.Clear();
            textBox17.Clear();
            textBox16.Clear();
            chkbx_noZipCode.Checked = false;
            pnlAddress.Visible = false;
            dataGridView1.Visible = false;
            checkBox8.Visible = false;
            checkBox6.Checked = false;
            dataGridView1.Enabled = true;
            numOftries = 0;
            numOfSearch = 0;
            isDoneSearch = false;
            label57.Text = numOftries.ToString();
        }

        private void LoadInvoice()
        {
            ComputeCompleted();

            Database.DBConnection.DatabaseAssignQueue conn = new Database.DBConnection.DatabaseAssignQueue();
            Database.DBConnection.DatabaseEM_Tasks conn_em = new Database.DBConnection.DatabaseEM_Tasks();
            Database.DB.EM_Tasks em_info = new Database.DB.EM_Tasks();
            Database.DB.AssignQueue queueInfo = new Database.DB.AssignQueue();
            queueInfo = new Database.DB.AssignQueue();
            info = new Database.DB.EM_Tasks();

            int[] listOfTask = new int[] {20,0,50,40,60,100};
            string[] listOfTaskName = new string[] {"New Invoice", "Call Out", "Escalations", "Invoice Upload", "E-Bills", "QA 1", "QA 2"};
            string invoiceSourceFile = string.Empty;

            textBox19.Text = TempFolder;

            //---------------------------------------
            //  Hide CallOut panel if not for callout
            //---------------------------------------
            panel_callout.Visible = false;
            panel_specialInstructions.Visible = false;
            groupBox9.Visible = false;

            try
            {
                queueInfo = conn.GetDataByUserid(Environment.UserName.ToLower());
            }
            catch
            {
                conn.WriteData(new Database.DB.AssignQueue() { 
                    assignee = "auto", current_queue = "NewInvoice", unchangeable = 0, userid = Environment.UserName
                });

                queueInfo = conn.GetDataByUserid(Environment.UserName);
            }

            string queue = queueInfo.current_queue;
            

            if (queue == "New Invoice")
            {
                queue_code = 20;
            }
            else if (queue == "Call Out")
            {
                queue_code = 0;
            }
            else if (queue == "E-Bills")
            {
                queue_code = 40;
            }
            else if (queue == "QA 1")
            {
                queue_code = 50;
            }
            else if (queue == "QA 2")
            {
                queue_code = 60;
            }
            else if (queue == "Escalations")
            {
                queue_code = 70;
                isEscalQueue = true;
            }
            else if (queue == "Invoice Upload")
            {
                queue_code = 80;
            }

           if (queue != "E-Bills")
            {
               
                //if (queue != "Escalations")
                //{

                    if (queue == "QA 1" || queue == "QA 2")
                    {
                        Form auditForm = new Forms.Supervisor.Audit();
                        auditForm.WindowState = FormWindowState.Maximized;
                        if (!auditForm.IsDisposed)
                        {
                            auditForm.Show();
                        }
                    }
                    else
                    {

                        //if ((queue != "Escalations" && queue != "Invoice Upload"))
                        //{
                            int tryGetFiles = 0;

                            while (invoiceSourceFile == string.Empty && tryGetFiles < 2) // && tryGetFiles < 1
                            {
                                info = new Database.DB.EM_Tasks();
                                info = null;
                                invoiceSourceFile = string.Empty;

                                List<Database.DB.EM_Tasks> listOfLockTasks = new List<Database.DB.EM_Tasks>();
                                List<Database.DB.EM_Tasks> listOfTasks = new List<Database.DB.EM_Tasks>();
                                listOfLockTasks = null;

                                listOfLockTasks = conn_em.GetLockedInvoiceForProcessing(Environment.UserName);

                                if (listOfLockTasks != null)
                                {
                                    foreach (Database.DB.EM_Tasks task in listOfLockTasks)
                                    {
                                        if (System.IO.File.Exists(task.InvoiceFolderName))
                                        {
                                            info = task;
                                        }
                                        else if (System.IO.File.Exists(System.IO.Path.Combine(TempFolder, System.IO.Path.GetFileName(task.InvoiceFolderName))))
                                        {
                                            info = task;
                                            info.InvoiceFolderName = System.IO.Path.GetFileName(task.InvoiceFolderName);
                                        }
                                        else
                                        {
                                            conn_em.UnlockInvoice(task.id);
                                        }
                                    }
                                }

                                if (info == null && queue != "Escalations" && queue != "Invoice Upload")
                                {
                                    listOfTasks = conn_em.GetDataFromQueue(queue_code, Environment.UserName.ToLower(), queue_code);

                                    if (listOfTasks != null)
                                    {
                                        foreach (Database.DB.EM_Tasks task in listOfTasks)
                                        {
                                            if (System.IO.File.Exists(task.InvoiceFolderName))
                                            {
                                                info = task;
                                            }

                                            if (System.IO.File.Exists(System.IO.Path.Combine(TempFolder, System.IO.Path.GetFileName(task.InvoiceFolderName))))
                                            {
                                                task.InvoiceFolderName = System.IO.Path.Combine(TempFolder, System.IO.Path.GetFileName(task.InvoiceFolderName));
                                               
                                                info = task;
                                            }
                                        }
                                    }

                                    if (info != null)
                                    {
                                        int affectedRows = 0;

                                        if (queue == "Call Out")
                                        {

                                            affectedRows = conn_em.LockInvoicesToUser(info.usp_name, Environment.UserName.ToLower());
                                        }
                                        else
                                        {
                                            affectedRows = conn_em.LockInvoiceToUser(info.Invoice, Environment.UserName.ToLower(), info.id);
                                        }

                                        if (affectedRows == 0)
                                        {
                                            invoiceSourceFile = string.Empty;
                                            info = new Database.DB.EM_Tasks();
                                        }
                                        else
                                        {
                                            listOfLockTasks = conn_em.GetLockedInvoiceForProcessing(Environment.UserName);

                                            if (listOfLockTasks != null)
                                            {
                                                foreach (Database.DB.EM_Tasks task in listOfLockTasks)
                                                {
                                                    if (System.IO.File.Exists(task.InvoiceFolderName))
                                                    {
                                                        info = task;
                                                    }
                                                    else if (System.IO.File.Exists(System.IO.Path.Combine(TempFolder, System.IO.Path.GetFileName(task.InvoiceFolderName))))
                                                    {
                                                        info = task;
                                                        info.InvoiceFolderName = System.IO.Path.GetFileName(task.InvoiceFolderName);
                                                    }
                                                    else
                                                    {
                                                        conn_em.UnlockInvoice(task.id);
                                                    }
                                                }
                                            }

                                            if (System.IO.File.Exists(info.InvoiceFolderName))
                                            {
                                                invoiceSourceFile = info.InvoiceFolderName;

                                                queueInfo.current_queue = queue;

                                                conn.UpdateData(queueInfo);
                                            }
                                        }
                                    }
                                    else
                                    {

                                        if (queue != "Escalations" && queue != "Invoice Upload")
                                        {
                                            int index = listOfTask.First(p => p == queue_code);
                                            int index1 = 0;

                                            clsConnection cls = new clsConnection();
                                            DataTable dt = new DataTable();

                                            dt = cls.GetData(@"select SupervisorId from tbl_HR_EmployeeMaster where NTID = '" + Environment.UserName + "'");

                                            if (dt.Rows.Count > 0)
                                            {
                                                if (dt.Rows[0][0].ToString() == "floremer" || dt.Rows[0][0].ToString() == "padayaoa" || dt.Rows[0][0].ToString() == "jamolinc")
                                                {
                                                    for (int i = 0; i < 3; i++)
                                                    {
                                                        if (listOfTask[i] == index)
                                                        {
                                                            index1 = i;
                                                        }
                                                    }

                                                    queue_code = listOfTask[index1 + 1];

                                                    if (queue_code == 20)
                                                    {
                                                        queue = "NewInvoice";
                                                    }
                                                    else if (queue_code == 0)
                                                    {
                                                        queue = "Call Out";
                                                    }
                                                    else if (queue_code == 70)
                                                    {
                                                        queue = "Escalations";
                                                    }
                                                    else if (queue_code == 80)
                                                    {
                                                        queue = "Invoice Upload";
                                                    }
                                                }
                                                else
                                                {
                                                    MessageBox.Show("No invoice were loaded");

                                                    return;
                                                }                                                
                                            }
                                            else
                                            {
                                                    MessageBox.Show("No invoice were loaded");

                                                    return;
                                            } 
                                        }
                                    }
                                }
                                else
                                {
                                    //if (queue == "Escalations" || queue == "Invoice Upload" && info == null)
                                    //{
                                    //    invoiceSourceFile = string.Empty;
                                    //}
                                    //else
                                    //{

                                    if (info != null)
                                    {
                                        if (System.IO.File.Exists(info.InvoiceFolderName))
                                        {
                                            invoiceSourceFile = info.InvoiceFolderName;
                                        }
                                        else
                                        {
                                            invoiceSourceFile = string.Empty;
                                        }
                                    }
                                    else
                                    {
                                        invoiceSourceFile = string.Empty;
                                    }

                                    //}
                                }

                                tryGetFiles++;
                            }
                        //}
                    }
                //}

                if (queue == "QA 1" || queue == "QA 2")
                {
                    return;
                }
                else
                {
                    if (invoiceSourceFile == string.Empty)
                    {
                        if (queue != "Escalations" && queue != "Invoice Upload")
                        {                           
                            MessageBox.Show("No invoice were loaded");
                        }
                        else
                        {
                            groupBox6.Visible = false;
                            groupBox9.Visible = true;
                            button8.Enabled = true;
                            button10.Enabled = true;
                            button12.Enabled = true;
                        }
                    }
                    else
                    {

                        if (queue == "Escalations" || queue == "Invoice Upload")
                        {
                            //groupBox6.Visible = false;

                            if (info.InvoiceFolderName != null)
                            {
                                button8.Enabled = false;
                            }
                            else
                            {
                                button8.Enabled = true;
                            }

                            groupBox9.Visible = true;
                            button10.Enabled = true;
                            button12.Enabled = true;
                        }

                        if (info.Complete == 0)
                        {
                            panel_callout.Visible = true;
                            panel_specialInstructions.Visible = true;
                            lblTransferCount.Visible = true;
                            ClearChildControls(panel_specialInstructions);
                            SaveButtonsEnabled(false);
                        }

                        string temporaryFile = System.IO.Path.Combine(System.IO.Path.GetTempPath(), Guid.NewGuid().ToString() + ".pdf");

                        try
                        {
                            System.IO.File.Copy(invoiceSourceFile, temporaryFile);
                        }
                        catch
                        { 
                        }

                        try
                        {
                            Uri myUri = new Uri(temporaryFile);
                            pdfviewer.Navigate(myUri);
                            textBox19.Text = myUri.AbsoluteUri.ToString();
                           pdfviewer.Refresh();                            
                        }
                        catch
                        {
                            MessageBox.Show("Unable to load invoice.", "Invoice Not Found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        CurrentLoadedInvoice = invoiceSourceFile;

                        string cleanInvoiceName = Path.GetFileNameWithoutExtension(CurrentLoadedInvoice); //Path.GetFileNameWithoutExtension(CurrentLoadedInvoice).Split('_')[0];
                        this.info = new Database.DB.EM_Tasks();

                        //--------------------------check later
                        groupBox6.Visible = true;
                        LoadDataEMTask(cleanInvoiceName);
                    }
                }
                
            }
            else
            { 
                
            }

            try
            {
                iniComplete = info.Complete.ToString();
                iniPendingReason = info.PendingReasons;
                iniLastModDate = info.LastDateModified.ToString();
                try

                {
                    invoicecount = info.InvoiceCount;
                }
                catch
                {
                    invoicecount = 0;
                }
                
            }
            catch
            { 
            }
           
        }

        private void ClearChildControls(Control ctr)
        {
            if (ctr.HasChildren)
            {
                foreach (Control c in ctr.Controls)
                {
                    ClearChildControls(c);
                }
            }
            else
            {
                try
                {
                    if (ctr.GetType() == typeof(TextBox))
                    {
                        ((TextBox)ctr).Text = string.Empty;
                    }

                    if (ctr.GetType() == typeof(ComboBox))
                    {
                        ((ComboBox)ctr).Text = string.Empty;
                    }

                    if (ctr.GetType() == typeof(CheckBox))
                    {
                        ((CheckBox)ctr).CheckState = CheckState.Unchecked;
                    }
                }
                catch
                { }
            }
        }

        private bool LoadLockedInvoice(ref string selectedFile)
        {
            string[] filesLocked = System.IO.Directory.GetFiles(TempFolder, string.Format("*_{0}_lock*",Environment.UserName),SearchOption.TopDirectoryOnly);

            if (filesLocked.Count() == 0)
            {
                return false;
            }
            else
            {
                string[] fullname = System.IO.Path.GetFileNameWithoutExtension(filesLocked[0]).Split('_'); //System.IO.Path.GetFileNameWithoutExtension(filesLocked[0]).Split('_');

                selectedFile = filesLocked[0];
                return true;
            }
        }

        private void LoadDataEMTask(string invoice)
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            Database.DB.EM_Tasks DataToBeLoaded = new Database.DB.EM_Tasks();

            if (queue_code == 0)
            {
                DataToBeLoaded = conn.GetLatestInvoiceDataCallout(invoice);
            }
            else
            {
                DataToBeLoaded = conn.GetLatestInvoiceDataNew(invoice);
            }

            if (DataToBeLoaded != null)
            {
                try
                {
                    info = DataToBeLoaded;

                    if (info.Complete == 0 && queue_code == 0)
                    {

                        textBox10.Text = info.AccountNumber.ToString().Replace(" ", "");
                        textBox11.Text = info.Amount.ToString();
                        dateTimePicker4.Value = info.invoice_date == DateTime.MinValue ? DateTime.Today : info.invoice_date;
                        dateTimePicker1.Value = info.DueDate == DateTime.MinValue ? DateTime.Today : info.DueDate; ;
                        dateTimePicker2.Value = info.ServiceFrom == DateTime.MinValue ? DateTime.Today : info.ServiceFrom; ;
                        dateTimePicker3.Value = info.ServiceTo == DateTime.MinValue ? DateTime.Today : info.ServiceTo; ;

                        if (info.vendor_group == "E")
                        {
                            checkBox4.Checked = true;
                        }
                        else if (info.vendor_group == "W")
                        {
                            checkBox13.Checked = true;
                        }
                        else if (info.vendor_group == "G")
                        {
                            checkBox5.Checked = true;
                        }


                        if (info.Complete != 0)
                        {
                            if (info.HasTransferAmount == "Yes")
                            {
                                chkTransAmtYes.Checked = true;
                            }
                            else
                            {
                                chkTransAmtNo.Checked = true;
                            }
                        }
                        

                        groupBox7.Visible = false;

                        if (info.property_code != string.Empty)
                        {
                            LoadPropertyInDGV(new List<string>() { info.property_code });
                        }
                                               
                        chkASFIYes.Checked = true;


                        if (info.InvoiceCount > 0)
                        {
                            SqlConnection dconn = new SqlConnection(Constants.connectionString);

                            dconn.Open();

                            string query = string.Format(@"select MAX(InvoiceCount) from tbl_ADHOC_EM_Tasks where Invoice like '' + replace('{0}', ' - {1}', '') + '%'",
                                                            info.Invoice,
                                                            info.InvoiceCount);

                            SqlCommand cmd = new SqlCommand(query, dconn);

                            SqlDataReader rd = cmd.ExecuteReader();

                            rd.Read();

                            if (rd.HasRows)
                            {
                                numBalTransfer.Value = Convert.ToInt32(rd[0]);
                                lblTransferCount.Text = info.InvoiceCount.ToString() + " of " + rd[0].ToString() + " transfer account(s)";
                            }

                            rd.Close();
                            dconn.Close();
                        }

                        if (info.HasTransferAmount == "Yes")
                        {
                            chkTransAmtYes.Checked = true;
                            chkTransAmtNo.Checked = false;

                            chkBalInfoYes.Checked = true;
                            chkBalInfoNo.Checked = false;

                            chkBalAmtYes.Checked = true;
                            chkBalAmtNo.Checked = false;

                        }
                        else
                        {
                            chkTransAmtYes.Checked = false;
                            chkTransAmtNo.Checked = true;

                            chkBalInfoYes.Checked = false;
                            chkBalInfoNo.Checked = true;

                            chkBalAmtYes.Checked = true;
                            chkBalAmtNo.Checked = false;

                            numBalTransfer.Value = 0;
                        }

                        //textBox10.Text = info.AccountNumber;
                                                
                        



                        //------------------------------------
                        //  Loads pending reason of callout
                        //------------------------------------

                        if (info.Complete == 0)
                        {
                            string reasonForCalling = string.Empty;

                            switch (info.PendingReasons.Trim())
                            {
                                case "No Account Number":
                                    reasonForCalling = "Account Number"; break;
                                case "No Amount":
                                    reasonForCalling = "Amount"; break;
                                case "No Property/Service Address":
                                    reasonForCalling = "Property / Service Address"; break;
                                case "Do Not Mail":
                                    reasonForCalling = "USP Physical Address"; break;
                                case "No USP Address on Invoice":
                                    reasonForCalling = "USP Address";
                                    textBox13.ReadOnly = false;
                                    textBox9.ReadOnly = false;
                                    textBox13.BackColor = Color.White;
                                    textBox9.BackColor = Color.White;
                                    break;
                                default:
                                    reasonForCalling = info.PendingReasons;
                                    textBox13.ReadOnly = true;
                                    textBox9.ReadOnly = true;
                                    textBox13.BackColor = Color.Gainsboro;
                                    textBox9.BackColor = Color.Gainsboro;
                                    break;
                            }

                            SqlConnection sqlcon = new SqlConnection(Constants.connectionString);
                            sqlcon.Open();

                            string query = string.Format(@"SELECT a.USPGroupName, a.PhoneNo  FROM [MIS_ALTI].[dbo].[tbl_UC_USPGroup] a
                                                        WHERE USPGroupName = '{0}'", info.usp_name);

                            SqlCommand cmd = new SqlCommand(query, sqlcon);

                            SqlDataReader rd = cmd.ExecuteReader();

                            rd.Read();

                            label21.Text = reasonForCalling;

                            if (rd.HasRows)
                            {
                                lbl_serviceProviderName.Text = rd[0].ToString();
                                lbl_phoneNumber.Text = rd[1].ToString();
                                textBox13.Text = rd[0].ToString();
                                textBox9.Text = rd[1].ToString();
                            }
                            else
                            {
                                lbl_serviceProviderName.Text = info.usp_name;
                                lbl_phoneNumber.Text = "no info";
                                textBox13.Text = info.usp_name;
                                textBox9.Text = "no info";
                            }

                            rd.Close();
                            cmd = null;
                            sqlcon.Close();

                            textBox15.Text = string.Format(@"Hi! This is {0}, I'm the property manager for Altisource Single Family. I need to find out some information on the invoice we received from your company.", UserPrincipal.Current.Name);
                            textBox7.Text = reasonForCalling;
                            panel_callout.Visible = true;
                            panel_specialInstructions.Visible = true;
                            groupBox3.Enabled = false;

                            if (info.AccountNumber != string.Empty)
                            {
                                label14.Visible = true;
                                textBox10.Visible = true;
                                textBox10.Text = info.AccountNumber;
                            }


                            if (info.property_code != string.Empty)
                            {
                                LoadPropertyInDGV(new List<string>() { info.property_code });
                            }
                            button7.Visible = true;
                            button9.Visible = true;

                            if (info.usp_name == string.Empty)
                            {
                                label56.Text = "1 out of 1 invoice(s)";
                            }
                            else
                            {
                                sqlcon.Open();

                                query = string.Format(@"select COUNT(*) from tbl_ADHOC_EM_Tasks a
                                                where a.Complete = 0
                                                and a.lockedto = '{0}'
                                                and a.usp_name = '{1}'
                                                and a.isLatestData = 1",
                                                        Environment.UserName.ToLower(),
                                                        DataToBeLoaded.usp_name);

                                cmd = new SqlCommand(query, sqlcon);

                                rd = cmd.ExecuteReader();

                                rd.Read();

                                int numberOfInvoice = (int)rd[0];

                                if (numberOfInvoice == 0)
                                {
                                    label56.Text = "1 out of 1 invoice(s)";
                                }
                                else
                                {
                                    label56.Text = string.Format("1 out of {0} invoice(s)", numberOfInvoice);
                                }

                                panel_callout.Visible = true;

                                rd.Close();
                                cmd = null;
                                sqlcon.Close();
                            }

                        }
                    }
                    else
                    {


                        lbl_serviceProviderName.Text = string.Empty;
                        lbl_phoneNumber.Text = string.Empty;
                        textBox13.Text = string.Empty;
                        textBox9.Text = string.Empty;
                        textBox7.Text = string.Empty;
                        panel_callout.Visible = false;
                        
                    }


                }
                catch
                { 
                    //do nothing
                }

                if (string.IsNullOrEmpty(textBox2.Text))
                {
                    textBox2.Text = "0";
                }
            }
        }

        private void LoadPropertyInDGV(List<String> propertyCodes)
        {
            Database.DBConnection.DatabaseREO_Properties conn1 = new Database.DBConnection.DatabaseREO_Properties();
            Database.DB.REO_Properties propInfo = new Database.DB.REO_Properties();

            propInfo = conn1.GetData(info.property_code);
            if (propertyCodes != null)
            {
                dataGridView1.Rows.Clear();

                foreach (string prop in propertyCodes)
                {
                    dataGridView1.Rows.Add(
                            propInfo.property_code,
                            propInfo.full_address,
                            propInfo.customer_name,
                            propInfo.active,
                            propInfo.investor_name,
                            propInfo.reosrc_date,
                            propInfo.reoslc_date,
                            propInfo.reosfc_date
                            );
                }
            }
        }

        private void BW_LoadTasksCompleted_DoWork(object sender, DoWorkEventArgs e)
        {
            ComputeCompleted();
        }

        private void dailyReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form dashboard = new Forms.Dashboard();
            dashboard.Show();
        }

        private void assignQueueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            string[] adminUsers = new string[] { "test", "estellar", "elayronj", 
                                                 "martinat", "jocsonda", "reyesjoh" };

            if (adminUsers.Contains(Environment.UserName.ToLower()))
            {
            }
            else
            {
                MessageBox.Show("Access Denied", "Unauthorized Access");
            }
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            string htmlfile = this.CurrentLoadedInvoice;
            if (System.IO.File.Exists(textBox19.Text.Trim()))
            {
                this.CurrentLoadedInvoice = textBox19.Text.Trim();

                Uri myUri = new Uri(textBox19.Text.Trim());
                pdfviewer.Navigate(myUri);
                
                info.Invoice = System.IO.Path.GetFileNameWithoutExtension(textBox19.Text.Trim());

                try
                {
                    File.Move(htmlfile, EBills_PROCESSEDBILLS + Path.GetFileNameWithoutExtension(htmlfile).Split('_')[0] + "_" + Path.GetFileNameWithoutExtension(textBox19.Text.Trim()).Split('_')[0] + ".html");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to move email file. Error: " + ex.Message, "Load Invoice");
                }
            }
        }

        private void dateTimePicker4_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker4.Value.Date != DateTime.Today.AddDays(-1) && info.PendingReasons == "Vendor ID Request")
            {
                GetVIDReqDtls();
            }

            if (dateTimePicker4.Value.Date > DateTime.Now.Date)
            {
                MessageBox.Show("Invalid invoice date");

                dateTimePicker1.Visible = false;
                checkBox9.Visible = false;
                label2.Visible = false;
                pictureBox7.Visible = false;
            }
            else
            {
             
                info.invoice_date = dateTimePicker4.Value.Date;
                dateTimePicker1.Visible = true;
                checkBox9.Visible = true;
                label2.Visible = true;
                pictureBox7.Visible = true;
            }            
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            info.DueDate = dateTimePicker1.Value.Date;
            dateTimePicker2.Visible = true;
            checkBox10.Visible = true;
            label12.Visible = true;
            pictureBox8.Visible = true;
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            info.ServiceFrom = dateTimePicker2.Value.Date;
            dateTimePicker3.Visible = true;
            label13.Visible = true;

            dateTimePicker3.MinDate = dateTimePicker2.Value;
        }

        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {
            info.ServiceTo = dateTimePicker3.Value.Date;

            label34.Visible = true;
            checkBox11.Visible = true;
            checkBox12.Visible = true;

            label53.Visible = true;
            dateTimePicker5.Visible = true;
            checkBox22.Visible = true;

            ResetAmountInformation();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            info.Amount = textBox2.Text.Trim();
        }
        
        private void comboBox4_TextChanged(object sender, EventArgs e)
        {
            try { info.usp_name = cmboBx_UspOnInvoice.Text; }
            catch { }
        }

        public string strCreateInvoiceFolderName(string folder)
        {
            string strDate = DateTime.Now.ToString("MM-dd-yyyy");
            if (CurrentLoadedInvoice.Contains("pdf"))
            {
                return string.Format(@"{0}{1}\{2}.pdf", folder, strDate, info.Invoice);
            }
            else
            {
                return string.Format(@"{0}{1}\{2}.html", folder, strDate, info.Invoice);
            }
        }

        private void uSPWebsiteInformationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form frm = new Forms.Tools.USPWebInfo();
            frm.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (panel_specialInstructions.Visible == true)
            {
                MessageBox.Show("Process the invoice using the gathered information from Call-Out");
                return;
            }

            try
            {
                button2.Invoke((Action)delegate
                {
                    button2.Enabled = false;
                });

                if (checkBox2.Checked == false && checkBox3.Checked == false)
                {
                    var result = MessageBox.Show("Is this bill readable",
                                       "Is this bill readable",
                                       MessageBoxButtons.YesNo,
                                       MessageBoxIcon.Question);

                    if (result == DialogResult.Yes)
                    {
                        checkBox2.Checked = true;
                        checkBox3.Checked = false;
                    }
                    else
                    {
                        checkBox2.Checked = false;
                        checkBox3.Checked = true;
                    }

                    return;
                }
                else
                {
                    if (checkBox2.Checked == true && checkBox3.Checked == false && chkASFIYes.Checked == true)
                    {
                        if (checkBox4.Checked == false && checkBox5.Checked == false && checkBox13.Checked == false && checkBox14.Checked == false)
                        {
                            MessageBox.Show("Please select if bill is Electricity, Gas, Water or Others");
                            SaveButtonsEnabled(true);
                            return;
                        }
                    }
                }


                if (info.property_code != "" && chkASFIYes.Checked == true)
                {
                    if (!isVendorGroupCompleted())
                    {
                        MessageBox.Show("Please select if bill is Electricity, Gas or Water");
                        SaveButtonsEnabled(true);
                        return;
                    }


                    if (!isAccountNumberCompleted())
                    {
                        MessageBox.Show("Please enter the account number. Enter 'none' if there is no any.");
                        SaveButtonsEnabled(true);
                        return;
                    }


                    if (!isDoNotMailSelected() && !checkBox8.Checked)
                    {
                        MessageBox.Show("Please select if bill is Do Not Mail");
                        SaveButtonsEnabled(true);
                        return;
                    }


                    if (!isFinalBillSelected() && !checkBox8.Checked)
                    {
                        MessageBox.Show("Please select if bill is Final Bill");
                        SaveButtonsEnabled(true);
                        return;
                    }


                    //if (!isHasTransAmtSelected())
                    //{
                    //    MessageBox.Show("Please select if bill has Transfer Amount");
                    //    SaveButtonsEnabled(true);
                    //    return;
                    //}


                    if (!isPreviousBalanceComplete())
                    {
                        if (iniComplete != "0" && info.Complete != 3 && info.Complete != 32 && info.Complete != 31)
                        {
                            MessageBox.Show("Please enter previous balance. Enter '0.00' if there is no any.");
                            SaveButtonsEnabled(true);
                            return;
                        }
                    }

                    string sFrom = string.Empty;
                    string sTo = string.Empty;

                    if (info.ServiceFrom.ToShortDateString() == "1/1/0001")
                    {
                        sFrom = string.Empty;
                    }
                    else
                    {
                        sFrom = info.ServiceFrom.ToShortDateString();
                    }

                    if (info.ServiceTo.ToShortDateString() == "1/1/0001")
                    {
                        sTo = string.Empty;
                    }
                    else
                    {
                        sTo = info.ServiceTo.ToShortDateString();
                    }

                    string summary = string.Format(@"Account Number: {0} Service Period: {1} - {2} Amount: {3} Type of Service: {4} USP: {5}",
                            info.AccountNumber + Environment.NewLine,
                            sFrom.ToString(), sTo.ToString() + Environment.NewLine,
                            info.Amount + Environment.NewLine,
                            info.vendor_group + Environment.NewLine,
                            info.usp_name);

                    var DiagResult = MessageBox.Show(summary, "Do you want to proceed?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (DiagResult == DialogResult.No)
                    {
                        return;
                    }
                }

                numOftries = 0;
                numOfSearch = 0;
                isDoneSearch = false;


                //MessageBox.Show(info.AccountNumber + " " + info.vendor_group);

                SaveItem(true);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save. Error:" + ex.Message);
            }
            finally 
            {
                button2.Invoke((Action)delegate
                {
                    button2.Enabled = true;
                });
            }
        }

        private void chkBx_NoPropertyAddress_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBx_NoPropertyAddress.Checked == true)
            {
                MessageBox.Show("Please select USP then click Save", "Instructions");

                //if (!chkTransAmtYes.Checked)
                //{
                //    Clear(true, new Control[] { label14, textBox10, checkBox6, chkBx_NoPropertyAddress, checkBox2, checkBox3, pictureBox5, label47, label58, chkASFIYes, chkASFINo,
                //    chkTransAmtYes, chkTransAmtNo, label59, numBalTransfer, label60, label62, label63, chkBalInfoYes, chkBalInfoNo, label63, chkBalAmtYes, chkBalAmtNo, checkBox15, 
                //    checkBox16, groupBox8});

                //    label26.Visible = true;
                //    label26.Enabled = false;
                //    textBox4.Visible = true;
                //    textBox4.Enabled = false;
                //}


                //chkBx_NoPropertyAddress.Visible = true;
                //chkBx_NoPropertyAddress.Checked = true;

                lbl_UspOnInvoice.Visible = true;
                cmboBx_UspOnInvoice.Visible = true;

                //info.Complete = 0;
                //info.PendingReasons = "No Property/Service Address";
                //info.InvoiceFolderName = strCreateInvoiceFolderName(CallOutFolder);

                info.Complete = 21;
                info.PendingReasons = "No Property/Service Address";
            }
            else
            {
                //Clear(true, new Control[] { label14, textBox10, checkBox6, chkBx_NoPropertyAddress, checkBox2, checkBox3, pictureBox5, label47, label58, chkTransAmtYes, chkTransAmtNo, checkBox15, checkBox16 });

                //label1.Visible = true;
                //label1.Enabled = true;
                //label26.Visible = true;
                //label26.Enabled = true;
                //textBox1.Visible = true;
                //textBox1.Enabled = true;
                //textBox4.Visible = true;
                //textBox4.Enabled = true;
                //chkBx_NoPropertyAddress.Visible = true;
                //chkbx_noZipCode.Visible = true;
                //chkbx_noZipCode.Enabled = true;

                lbl_UspOnInvoice.Visible = false;
                cmboBx_UspOnInvoice.Visible = false;

                groupBox8.Visible = true;
                button7.Visible = true;
                button9.Visible = true;
            }
        }

        private void cmboBx_UspOnInvoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            button3.Enabled = false;
            button2.Enabled = false;

            if (cmboBx_UspOnInvoice.Text == "USP Not Listed" || cmboBx_UspOnInvoice.Text == "----------------------------")
            {
                Form addusp = new Forms.AddUSP();
                addusp.ShowDialog(this);
                preloadUSP();

                cmboBx_UspOnInvoice.Text = string.Empty;
            }

            if (cmboBx_UspOnInvoice.Text != string.Empty)
            {
                button3.Enabled = true;
                button2.Enabled = true;

                //info.Complete = 0;
                //info.PendingReasons = "No Property/Service Address";
                //info.InvoiceFolderName = strCreateInvoiceFolderName(CallOutFolder);

                info.Complete = 21;
                info.PendingReasons = "No Property/Service Address";

                SaveButtonsEnabled(true);
                
                MessageBox.Show("Please click SAVE.");
            }
        }

        private void triggerSearchButton(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                info.AccountNumber = textBox10.Text;
                button7_Click(null, null);
            }
            else if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar) || char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {

            if (checkBox6.Checked)
            {
                textBox10.Text = "None";
                //button7_Click(null, null);
                hasDoneAcctSearch = true;
                groupBox8.Visible = true;
                textBox4.Visible = true;
                textBox1.Visible = true;
                chkbx_noZipCode.Visible = true;
                label26.Visible = true;
                label1.Visible = true;
                pictureBox3.Visible = true;
            }
            else
            {
                textBox10.Text = string.Empty;
                button9_Click(null, null);
            }
            

            //if (checkBox6.Checked)
            //{
            //    textBox10.Text = "None";
            //    textBox10.Enabled = false;
            //    label14.Enabled = false;

            //    if (!chkTransAmtYes.Checked)
            //    {
            //        Clear(true, new Control[] { label14, textBox10, checkBox6, chkBx_NoPropertyAddress, checkBox2, checkBox3, pictureBox5, label47, label58, chkASFIYes, chkASFINo,
            //        chkTransAmtYes, chkTransAmtNo, label59, numBalTransfer, label60, label62, label63, chkBalInfoYes, chkBalInfoNo, label63, chkBalAmtYes, chkBalAmtNo, checkBox15, 
            //        checkBox16, groupBox8});
            //    }                

            //    //info.Complete = 0;
            //    //info.PendingReasons = "No Account Number";
            //    //info.InvoiceFolderName = strCreateInvoiceFolderName(CallOutFolder);

            //    info.Complete = 21;
            //    info.PendingReasons = "Pending for Audit";

            //    MessageBox.Show("Please click save!");

            //    SaveButtonsEnabled(true);
            //}
            //else
            //{
            //    textBox10.Text = string.Empty;
            //    textBox10.Enabled = true;
            //    label14.Enabled = true;

            //    Clear(true, new Control[] { label14, textBox10, checkBox6, checkBox2, checkBox3, pictureBox5, label47, label58, chkTransAmtYes, chkTransAmtNo, checkBox15, checkBox16 });

            //    ResetPropertyInformation();

            //    button7.Visible = true;
            //    button9.Visible = true;
            //}
        }

        //private void SearchUSP(object sender, DataGridViewCellEventArgs e)
        //{
        //    Control[] controlsToHide = new Control[] { groupBox2, groupBox5, groupBox3, groupBox7};

        //    if (e.RowIndex > -1)
        //    {
        //        //--------------------------------------
        //        //  Hide USP Table
        //        //--------------------------------------
        //        groupBox_uspNoMatch.Visible = true;
        //        checkBox_uspNoMatch.Visible = true;

        //        foreach (Control c in controlsToHide)
        //        {
        //            c.Visible = false;
        //        }

        //        info.property_code = dataGridView1.Rows[e.RowIndex].Cells["cPropertyCode"].Value.ToString().Trim();
        //        info.client_code = dataGridView1.Rows[e.RowIndex].Cells["cCustomerName"].Value.ToString().Trim();
        //        info.isActive = dataGridView1.Rows[e.RowIndex].Cells["cActive"].Value.ToString().Trim() == "Active" ? true : false;

        //        //--------------------------------------
        //        //  Check if property is Real Resolution
        //        //--------------------------------------
        //        if (info.client_code == "RR")
        //        {
        //            info.Complete = 14;
        //            info.PendingReasons = "Real Resolution";
        //            info.InvoiceFolderName = strCreateInvoiceFolderName(RR_NEW);

        //            MessageBox.Show("Please click Save.");

        //            return;
        //        }

        //        //----------------------------------
        //        //  Check if property is Inactive
        //        //----------------------------------
        //        string active = dataGridView1.Rows[e.RowIndex].Cells["cActive"].Value.ToString();
        //        if (active.ToLower().Trim() == "inactive")
        //        {
        //            isInactive = true;

        //            ResetBillingInformation();

        //            MessageBox.Show("Fill out the billing information.", "Instructions");

        //            groupBox2.Visible = false;  // Hide USP list
        //            groupBox_uspNoMatch.Visible = false;    //groupBox_uspNoMatch
        //            groupBox5.Visible = true;   // Show billing information

        //            info.Complete = 12;
        //            info.InvoiceFolderName = strCreateInvoiceFolderName(InactiveFolder);

        //            Form vendorGroupSelect = new Forms.VendorGroupSelect(this);
        //            vendorGroupSelect.Show();

        //            button2.Enabled = true;
        //            button3.Enabled = true;

        //            return;
        //        }

        //        //-----------------------------------------
        //        // Check if property is Trailing Asset
        //        //-----------------------------------------
        //        CheckIfOutOfREO(e.RowIndex);
        //        if (this.isTrailingAsset)
        //        {
        //            MessageBox.Show("Trailing Asset! Please input details and click save");

        //            info.Complete = 1;
        //            info.PendingReasons = "Trailing Asset";
        //            info.InvoiceFolderName = strCreateInvoiceFolderName(TrailingAssetFolder);

        //            //-------------------------------------
        //            // Show Payment Breakdown Information,
        //            // Final Bill GroupBox, Billing Info
        //            //-------------------------------------
        //            Control[] amountTopInfo = new Control[] { label31, label33, textBox11, 
        //                textBox12, groupBox9, groupBox5};

        //            foreach (Control ctrAmt in amountTopInfo)
        //            {
        //                ctrAmt.Visible = true;
        //                ctrAmt.Enabled = true;
        //            }

        //            // Hide USP groupbox
        //            groupBox2.Visible = false;

        //            button2.Enabled = true;
        //            button3.Enabled = true;
        //        }

        //        Database.DBConnection.DatabaseUSP_Contact conn1 = new Database.DBConnection.DatabaseUSP_Contact();
        //        Database.DBConnection.DatabasePropertyToUSP conn = new Database.DBConnection.DatabasePropertyToUSP();
        //        Database.DB.PropertyToUSP propUspInfo = new Database.DB.PropertyToUSP();
        //        Database.DB.USP_Contact uspInfo = new Database.DB.USP_Contact();

        //        dataGridView2.Rows.Clear();

        //        propUspInfo = conn.GetDataProcessor(info.property_code);

        //        uspSearchHasResults = false;

        //        if (propUspInfo != null)
        //        {
        //            if (propUspInfo.gas_vendor_code != string.Empty)
        //            {
        //                DisplayUSPtoDataGridView(
        //                    propUspInfo.gas_vendor_code,
        //                    propUspInfo.gas_turnoff_date,
        //                    info.property_code,
        //                    "G");

        //                uspSearchHasResults = true;
        //            }

        //            if (propUspInfo.water_vendor_code != string.Empty)
        //            {
        //                DisplayUSPtoDataGridView(
        //                    propUspInfo.water_vendor_code,
        //                    propUspInfo.water_turnoff_date,
        //                    info.property_code,
        //                    "W");

        //                uspSearchHasResults = true;
        //            }

        //            if (propUspInfo.electricity_vendor_code != string.Empty)
        //            {
        //                DisplayUSPtoDataGridView(
        //                    propUspInfo.electricity_vendor_code,
        //                    propUspInfo.electricity_turnoff_date,
        //                    info.property_code,
        //                    "E");

        //                uspSearchHasResults = true;
        //            }

        //            //----------------------------------------
        //            // Show USP box if there is data pulled-up
        //            //----------------------------------------
        //            groupBox_uspNoMatch.Visible = uspSearchHasResults;

        //            if (!uspSearchHasResults)
        //            {
        //                MessageBox.Show("Fill up the Billing Information.", "Instructions");
        //                ResetBillingInformation();
        //            }
        //        }
        //        else
        //        {
        //            MessageBox.Show("Fill up the Billing Information.", "Instructions");
        //            ResetBillingInformation();
        //        }

        //        if (!textBox19.Text.Contains(info.property_code))
        //        {
        //            textBox19.Text = textBox19.Text + DateTime.Now.ToString("yyyyMMdd") + info.property_code + ".pdf";
        //            button4.Enabled = true;
        //        }
        //    }
        //}

        //private void DisplayUSPtoDataGridView(string strVendorCode, DateTime dtTurnOff, string strPropertyCode, string strVendorGroup)
        //{
        //    Database.DBConnection.DatabaseVDR_Data conn = new Database.DBConnection.DatabaseVDR_Data();
        //    Database.DBConnection.DatabaseUSP_Contact conn1 = new Database.DBConnection.DatabaseUSP_Contact();
        //    Database.DBConnection.DatabaseVendorIdRequest conn2 = new Database.DBConnection.DatabaseVendorIdRequest();
        //    Database.DB.VDR info = new Database.DB.VDR();
        //    Database.DB.USP_Contact uspInfo = new Database.DB.USP_Contact();

        //    info = conn.GetDataProcessor(strVendorCode.Trim());
        //    uspInfo = conn1.GetUspData(strVendorCode);

        //    string action = string.Empty;

        //    groupBox2.Visible = true;
        //    dataGridView2.Visible = true;
        //    groupBox2.Enabled = true;
        //    dataGridView2.Enabled = true;

        //    if (uspInfo != null)
        //    {
        //        //-------------------------------
        //        //      USP is Active
        //        //-------------------------------
        //        if (info != null)
        //        {
        //            if (info.isActive == true)
        //            {
        //                //-------------------------------
        //                //      USP is Active in VDR
        //                //-------------------------------
        //                action = "BULK UPLOAD";
        //            }
        //            else
        //            {
        //                //-------------------------------
        //                //      USP is Active in VDR
        //                //-------------------------------
        //                action = "ACTIVATION";
        //            }
        //        }
        //        else
        //        {
        //            //-------------------------------
        //            //      USP is not in VDR
        //            //-------------------------------
        //            action = "CREATE NEW";
        //        }

        //        dataGridView2.Rows.Add(
        //                    uspInfo.VendorId,
        //                    uspInfo.Name,
        //                    uspInfo.Address,
        //                    uspInfo.PhoneNumber,
        //                    strVendorGroup,
        //                    dtTurnOff,
        //                    strPropertyCode,
        //                    action
        //                    );
        //        uspSearchHasResults = true;
        //    }
        //    else
        //    {
        //        //-------------------------------
        //        //      USP is Inactive
        //        //-------------------------------
        //        if (info != null)
        //        {
        //            action = "CHOOSE VDR";

        //            dataGridView2.Rows.Add(
        //                info.Vendor_ID,
        //                info.VDR_Vendor_Name,
        //                info.Address,
        //                "",
        //                strVendorGroup,
        //                null,
        //                strPropertyCode,
        //                action
        //                );

        //            uspSearchHasResults = true;
        //        }
        //    }
        //}

        //private void SrchVDR()
        //{
        //    Database.DBConnection.DatabaseVDR_Data conn = new Database.DBConnection.DatabaseVDR_Data();
        //    List<Database.DB.VDR> listOfUspOnVdr = new List<Database.DB.VDR>();

        //    try
        //    {
        //        listOfUspOnVdr = conn.SearchOnVDR(textBox3.Text, textBox5.Text, textBox7.Text, comboBox6.Text);

        //        dataGridView_VDR.Enabled = true;
        //        dataGridView_VDR.Rows.Clear();

        //        foreach (Database.DB.VDR vdrData in listOfUspOnVdr)
        //        {
        //            dataGridView_VDR.Rows.Add(
        //                vdrData.Vendor_ID,
        //                vdrData.VDR_Vendor_Name,
        //                vdrData.Address,
        //                vdrData.isActive == true ? "Active" : "Inactive");
        //        }
        //    }
        //    catch
        //    {
        //        dataGridView_VDR.Rows.Clear();
        //        dataGridView_VDR.Rows.Add("", "NO VENDOR HAS BEEN FOUND", "");
        //        dataGridView_VDR.Enabled = false;

        //        var res = MessageBox.Show("Request for a new Vendor ID? [Y/N]", "Create New Vendor ID", 
        //            MessageBoxButtons.YesNo, MessageBoxIcon.Question);

        //        if (res == DialogResult.Yes)
        //        {
        //            Form vdrFrm = new Forms.VendorIdRequest(info.Invoice, "", "", "","",false);
        //            vdrFrm.Show();

        //            info.Complete = 9;
        //            info.InvoiceFolderName = strCreateInvoiceFolderName(VendorIdRequestFolder);
        //        }

        //    }
        //    button10.Visible = true;
        //}

        private void ClearTextbox(object sender, MouseEventArgs e)
        {
            string[] keywords = new string[] { "e.g. 1003767...", "e.g. 21...", "0", "0.00" };

            if (sender.GetType() == typeof(TextBox))
            {
                foreach (string key in keywords)
                {
                    if (((TextBox)sender).Text == key)
                    {
                        ((TextBox)sender).Text = string.Empty;
                    }    
                }
            }
        }

        private void ShowAddMore(object sender, EventArgs e)
        {
            Control[] ctr = new Control[] { textBox2, comboBox4, textBox6, comboBox8, textBox8 };

            bool hasEmptyBox = false;

            foreach (Control ct in ctr)
            {
                if (string.IsNullOrEmpty(ct.Text))
                {
                    hasEmptyBox = true;
                }
            }

            if (hasEmptyBox == false)
            {
                btn_AddMorePaymentBreakdown.Visible = true;
            }

            if (textBox2.Text != string.Empty)
            {
                comboBox4.Visible = true;
                textBox6.Visible = true;
            }
            else
            {
                comboBox4.Visible = false;
                textBox6.Visible = false;
                comboBox8.Visible = false;
                textBox8.Visible = false;
            }

            if (comboBox4.Text != string.Empty && textBox6.Text != string.Empty)
            {
                comboBox8.Visible = true;
                textBox8.Visible = true;
            }
            else
            {
                comboBox8.Visible = false;
                textBox8.Visible = false;
            }

            UpdatePaymentBreakdown();
            ComputeTotalAmount();
        }

        private void ComputeTotalAmount()
        {
            double totalAmount = 0;

            foreach (PaymentClass pc in this.paymentBreakdown)
            {
                switch (pc.PaymentBreakdown)
                {
                    case "Previous Balance":
                        totalAmount += pc.Amount; break;
                    case "Payment Received":
                        totalAmount -= pc.Amount; break;
                    case "Late Fee":
                        totalAmount += pc.Amount; break;
                    case "Current Charge":
                        totalAmount += pc.Amount; break;
                    case "Disconnection Fee":
                        totalAmount += pc.Amount; break;
                    case "Deposit Payment":
                        totalAmount += pc.Amount; break;
                    case "Deposit Refund":
                        totalAmount -= pc.Amount; break;
                }
            }

            textBox11.Text = string.Format("{0:N2}", totalAmount);
        }

        private void UpdatePaymentBreakdown()
        {
            this.paymentBreakdown = new List<PaymentClass>();

            if (textBox2.Text != string.Empty)
            {
                try
                {
                    paymentBreakdown.Add(new PaymentClass() { PaymentBreakdown = "Previous Balance", Amount = Convert.ToDouble(textBox2.Text) });
                }
                catch
                { }
            }

            if (comboBox4.Text != string.Empty && textBox6.Text != string.Empty)
            {
                try
                {
                    paymentBreakdown.Add(new PaymentClass() { PaymentBreakdown = comboBox4.Text, Amount = Convert.ToDouble(textBox6.Text) });
                }
                catch
                { }
            }

            if (comboBox8.Text != string.Empty && textBox8.Text != string.Empty)
            {
                try
                {
                    paymentBreakdown.Add(new PaymentClass() { PaymentBreakdown = comboBox8.Text, Amount = Convert.ToDouble(textBox8.Text) });
                }
                catch
                { }
            }
        }

        private void checkBox7_CheckedChanged(object sender, EventArgs e)
        {
            Control[] ctrList = new Control[] { label3, label10, label23, label24, label25,
                                                comboBox4, comboBox8,
                                                textBox2, textBox6, textBox8,
                                                btn_AddMorePaymentBreakdown, linkLabel2,
                                                label31, label33, textBox11, textBox12 };

            if (checkBox7.Checked == true)
            {
                //info.Complete = 0;
                //info.PendingReasons = "No Amount";
                //info.InvoiceFolderName = strCreateInvoiceFolderName(CallOutFolder);

                info.Complete = 21;
                info.PendingReasons = "No Amount";

                textBox11.Text = "0.00";

                DisableControls(ctrList, false);

                SaveButtonsEnabled(true);

                MessageBox.Show("Please click SAVE.", "Instructions", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                info.PendingReasons = "";
                DisableControls(ctrList, true);
            }
        }

        private void SaveButtonsEnabled(bool enabled)
        {
            button2.Enabled = enabled;
            button3.Enabled = enabled;
        }

        private void DisableControls(Control[] listOfControls, bool isEnabled)
        {
            foreach (Control ctr in listOfControls)
            {
                ctr.Enabled = isEnabled;
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Form paymentForm = new Forms.Payment(this,false);
            paymentForm.ShowDialog();
        }

        private void textBox11_TextChanged(object sender, EventArgs e)
        {
            info.Amount = textBox11.Text;

            if (textBox11.Text == string.Empty)
            {
                info.ActAmount = 0;
            }
            else
            {
                info.ActAmount = Convert.ToDecimal(textBox11.Text);
            }

            if (txtTotal.Text != "0" && txtTotal.Text != string.Empty)
            {
                txtTotal.Text = (ini_InvAmount + Convert.ToDecimal(textBox11.Text)).ToString();
            }
            
            if (!string.IsNullOrEmpty(textBox11.Text))
            {
                SaveButtonsEnabled(true);
            }
            else
            {
                SaveButtonsEnabled(false);
            }
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox8.Checked == true)
            {
                //info.Complete = 0;
                //info.PendingReasons = "No USP Address on Invoice";
                //info.InvoiceFolderName = strCreateInvoiceFolderName(CallOutFolder);

                textBox2.Text = "0";
                info.usp_name = "";
                info.VendorId = "";
                info.vendor_address = "";
                info.Complete = 21;
                info.PendingReasons = "No USP Address on Invoice";

                MessageBox.Show("Please click save");

                SaveButtonsEnabled(true);
            }
            else 
            {
                info.Complete = 21;
                info.PendingReasons = string.Empty;
                dgv_usp.Rows.Clear();
                comboBox5.SelectedIndex = -1;
                label4.Visible = true;
                button_noMatchUSP.Visible = true;
                SaveButtonsEnabled(false);
            }
        }

        private void CreateMessageWithAttachment(string errorMessage)
        {
            string file = CurrentLoadedInvoice;
            string server = @"Internal-Mail.ascorp.com";

            try
            {
                MailMessage message = new MailMessage();
                message.To.Add("richel.atup@altisource.com");
                message.Subject = "Error in EM Software";
                message.Body = "Error: " + errorMessage + Environment.NewLine + Environment.NewLine +
                    "AccountNumber: " + info.AccountNumber + Environment.NewLine +
                    "Amount: " + info.Amount + Environment.NewLine +
                    "client_code: " + info.client_code + Environment.NewLine +
                    "Complete: " + info.Complete + Environment.NewLine +
                    "date_bulkuploaded: " + info.date_bulkuploaded + Environment.NewLine +
                    "DueDate: " + info.DueDate + Environment.NewLine +
                    "Duration: " + info.Duration + Environment.NewLine +
                    "EmployeePrimaryId: " + info.EmployeePrimaryId + Environment.NewLine +
                    "final_bill: " + info.final_bill + Environment.NewLine +
                    "Invoice: " + info.Invoice + Environment.NewLine +
                    "invoice_date: " + info.invoice_date + Environment.NewLine +
                    "InvoiceFolderName: " + info.InvoiceFolderName + Environment.NewLine +
                    "LastDateModified: " + info.LastDateModified + Environment.NewLine +
                    "PendingReasons: " + info.PendingReasons + Environment.NewLine +
                    "property_code: " + info.property_code + Environment.NewLine +
                    "ServiceFrom: " + info.ServiceFrom + Environment.NewLine +
                    "ServiceTo: " + info.ServiceTo + Environment.NewLine +
                    "userid: " + info.userid + Environment.NewLine +
                    "usp_name: " + info.usp_name + Environment.NewLine +
                    "vendor_group: " + info.vendor_group + Environment.NewLine +
                    "VendorId: " + info.VendorId + Environment.NewLine +
                    "work_item: " + info.work_item;

                Attachment data = new Attachment(file, MediaTypeNames.Application.Octet);

                message.Attachments.Add(data);

                SmtpClient client = new SmtpClient(server);
                client.Credentials = CredentialCache.DefaultNetworkCredentials;
                client.Send(message);

                data.Dispose();
            }
            catch
            {
            }
        }

        private void SendEmail(string strAttachment, string strSubject, string[] strMessage, string strSendTo)
        { 
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(UserPrincipal.Current.EmailAddress);
                message.To.Add(strSendTo);
                message.CC.Add("DEV_MNL@altisource.com");
                message.Subject = strSubject;
                message.Body = string.Empty;

                foreach (string msg in strMessage)
                {
                    message.Body += msg + Environment.NewLine;
                }

                Attachment data = new Attachment(strAttachment, MediaTypeNames.Application.Octet);

                message.Attachments.Add(data);
                message.Attachments[0].Name = System.IO.Path.GetFileNameWithoutExtension(strAttachment).Split('_')[0] + ".pdf";

                SmtpClient client = new SmtpClient("Internal-Mail.ascorp.com");
                client.Credentials = CredentialCache.DefaultNetworkCredentials;
                client.Send(message);

                data.Dispose();
            }
            catch
            {
            }
        }

        private void zipcodeSearch(object sender, KeyPressEventArgs e)
        {
            if (char.IsDigit(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;

                if (e.KeyChar == (char)13)
                {
                    button7_Click(null, null);
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {
            textBox10.Text = textBox10.Text.Replace(" ", "");
            info.AccountNumber = textBox10.Text;
        }

        private void btn_AddMorePaymentBreakdown_Click(object sender, EventArgs e)
        {
            if (comboBox4.Text != string.Empty &&
                comboBox8.Text != string.Empty &&
                textBox2.Text != string.Empty &&
                textBox6.Text != string.Empty &&
                textBox8.Text != string.Empty)
            {
                Forms.Payment pform = new UC.Forms.Payment(this);
                pform.ShowDialog();

                if (paymentBreakdown[0] != null)
                {
                    textBox2.Text = paymentBreakdown[0].Amount.ToString();
                }

                if (paymentBreakdown[1] != null)
                {
                    comboBox4.Text = paymentBreakdown[1].PaymentBreakdown;
                    textBox6.Text = paymentBreakdown[1].Amount.ToString();
                }

                if (paymentBreakdown[2] != null)
                {
                    comboBox8.Text = paymentBreakdown[2].PaymentBreakdown;
                    textBox8.Text = paymentBreakdown[2].Amount.ToString();
                }

                ComputeTotalAmount();

                linkLabel2.Visible = true;
            }
        }

        private void eMSDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form p = new Forms.Administrator.EMS_data(info, paymentBreakdown);
            p.Show();
        }

        private void checkBox9_CheckedChanged(object sender, EventArgs e)
        {
            info.DueDate = DateTime.Now.Date;

            if (checkBox9.Checked == true)
            {
                label12.Visible = true;
                dateTimePicker2.Visible = true;
                checkBox10.Visible = true;

                dateTimePicker2.Enabled = true;
                dateTimePicker1.Enabled = false;

                info.DueDate = DateTime.MinValue;
            }
            else
            {
                dateTimePicker1.Enabled = true;
                dateTimePicker1.Value = DateTime.Now;   

                checkBox10.Checked = false;
                checkBox10.Visible = false;

                label12.Visible = false;
                label13.Visible = false;

                dateTimePicker2.Visible = false;
                dateTimePicker3.Visible = false;

                groupBox7.Visible = false;
            }
        }

        private void checkBox10_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox10.Checked == true)
            {
                label12.Visible = true;
                label13.Visible = true;

                label34.Visible = true;
                checkBox11.Visible = true;
                checkBox12.Visible = true;

                dateTimePicker2.Visible = true;
                dateTimePicker3.Visible = true;

                label12.Enabled = false;
                label13.Enabled = false;

                dateTimePicker2.Enabled = false;
                dateTimePicker3.Enabled = false;

                info.ServiceFrom = DateTime.MinValue;
                info.ServiceTo = DateTime.MinValue;

                label53.Visible = true;
                dateTimePicker5.Visible = true;
                checkBox22.Visible = true;

                ResetAmountInformation();
            }
            else
            {
                label12.Visible = true;
                label13.Visible = false;

                dateTimePicker2.Visible = true;
                dateTimePicker3.Visible = false;

                label12.Enabled = true;
                label13.Enabled = true;

                label34.Visible = false;
                checkBox11.Visible = false;
                checkBox12.Visible = false;

                dateTimePicker2.Enabled = true;
                dateTimePicker3.Enabled = true;

                dateTimePicker2.Value = DateTime.Now.Date;
                dateTimePicker3.Value = DateTime.Now.Date;

                groupBox7.Visible = false;
            }
        }

        public void ResetAmountInformation()
        {
            groupBox7.Visible = true;
            label3.Visible = true;
            label10.Visible = true;
            label23.Visible = true;
            label31.Visible = true;
            label33.Visible = true;
            label24.Visible = false;
            label25.Visible = false;
            label54.Visible = true;
            //label58.Visible = true;
            textBox2.Visible = true;
            textBox6.Visible = false;
            textBox8.Visible = false;
            textBox11.Visible = true;
            textBox12.Visible = true;
            textBox34.Visible = true;
            textBox34.Text = "Previous Balance";
            comboBox4.Visible = false;
            comboBox8.Visible = false;
            linkLabel2.Visible = false;
            btn_AddMorePaymentBreakdown.Visible = false;
            checkBox7.Visible = true;
            checkBox23.Visible = true;
            checkBox24.Visible = true;
            //chkTransAmtYes.Visible = true;
            //chkTransAmtNo.Visible = true;
            //checkBox24.Checked = true;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            button6.Visible = false;

            //button7_Click(null, null);

            numOftries += 1;
            label57.Text = numOftries.ToString();

            if (numOftries == 1)
            {
                if (chkbx_noZipCode.Checked == false)
                {
                    pnlAddress.Visible = true;
                }

                label6.Visible = false;
                textBox17.Visible = false;

                label7.Visible = false;
                textBox16.Visible = false;
            }

            if (numOftries == 2)
            {

                label6.Visible = true;
                textBox17.Visible = true;

                label7.Visible = true;
                textBox16.Visible = true;
            }

            if (numOftries == 4)
            {
                numOfSearch += 1;

                if (numOfSearch == 3 && isDoneSearch == false)
                {

                    numOfSearch = 0;
                    isDoneSearch = true;

                    if (isSearchHasResults == false)
                    {
                        var res = MessageBox.Show("No match found. Do you want to search again?",
                        "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (res == DialogResult.No)
                        {

                            if (info.Complete == 20)
                            {
                                info.Complete = 31;

                                //info.InvoiceFolderName = strCreateInvoiceFolderName(PropertyNotFoundFolder);

                                button5.Visible = true;

                                return;
                            }

                            if (info.Complete == 31)
                            {
                                info.Complete = 32;

                                //info.InvoiceFolderName = strCreateInvoiceFolderName(PropertyNotFoundFolder);

                                button5.Visible = true;

                                return;
                            }

                            if (info.Complete == 32)
                            {
                                info.Complete = 21;

                                //info.InvoiceFolderName = strCreateInvoiceFolderName(PropertyNotFoundFolder);

                                button5.Visible = true;

                                return;
                            }
                        }
                    }
                }
                else
                {
                    MessageBox.Show("No match found. Please try searching again.", "Property Search", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    button9_Click(null, null);
                }      
            }  
        }

        private void button5_Click(object sender, EventArgs e)
        {

            numOftries = 0;
            numOfSearch = 0;
            isDoneSearch = false;

            info.client_code = string.Empty;

            if (info.Complete == 31)
            {
                info.PendingReasons = "PNF1";
            }

            if (info.Complete == 32)
            {
                info.PendingReasons = "PNF2";
            }

            if (info.Complete == 3)
            {
                info.Complete = 21;
                info.PendingReasons = "Pending for Audit";
            }

            info.property_code = string.Empty;
            info.userid = Environment.UserName;

            //info.vendor_group = "None";
            //info.Invoice = Path.GetFileNameWithoutExtension(CurrentLoadedInvoice).Split('_')[0];
            //info.InvoiceFolderName = strCreateInvoiceFolderName(PropertyNotFoundFolder);

            dataGridView1.Enabled = false;

            button2_Click(null, null);
        }

        private void chkbx_noZipCode_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbx_noZipCode.Checked)
            {

                
                //label1.Enabled = false;
                textBox1.Text = string.Empty;
                textBox1.Enabled = false;

                hasZipcodeOnInvoice = false;
                pnlAddress.Visible = true;

                textBox14.Text = string.Empty;
                textBox16.Text = string.Empty;
                textBox17.Text = string.Empty;
                //textBox16.Enabled = true;
                //textBox17.Enabled = true;
                //textBox17.ReadOnly = false;
            }
            else
            {

                pnlAddress.Visible = false;

                //label1.Enabled = true;
                textBox1.Enabled = true;

                hasZipcodeOnInvoice = true;

                //textBox16.Enabled = false;
                //textBox17.Enabled = false;
                //textBox17.ReadOnly = true;
            }
        }

        //private void button8_Click(object sender, EventArgs e)
        //{
        //    MessageBox.Show("Search VendorID on Vendors Detailed Report (Dynamics) then click Search","Instructions");

        //    button2.Enabled = true;
        //    button3.Enabled = true;

        //    ResetVDRControl();
        //}

        //private void ResetVDRControl()
        //{
        //    Form vendorSelect = new Forms.VendorGroupSelect(this);
        //    vendorSelect.Show();

        //    groupBox3.Visible = true;
        //    dataGridView_VDR.Visible = true;
        //    dataGridView_VDR.Rows.Clear();
        //    label4.Visible = true;
        //    label8.Visible = true;
        //    label27.Visible = true;
        //    label28.Visible = true;
        //    label29.Visible = true;
        //    textBox3.Visible = true;
        //    textBox5.Visible = true;
        //    textBox7.Visible = true;
        //    textBox15.Visible = true;
        //    comboBox6.Visible = true;
        //    button1.Visible = true;
        //}

        

        private void button10_Click(object sender, EventArgs e)
        {
            Form vdrSelect = new Forms.MessagePrompts.VDRSelect(this);
            vdrSelect.ShowDialog();

            if (saveVDR == true)
            {
                button2_Click(null, null);    
            }
        }

        private void ShowPic_AcctNo(object sender, EventArgs e)
        {
            panel_acctNumber.BackgroundImage = UC.Properties.Resources.sample_EM_accountnumber;
            panel_acctNumber.Visible = true;
        }

        private void HidePic_AcctNo(object sender, EventArgs e)
        {
            panel_acctNumber.Visible = false;
        }

        private void ManipulateControl(Control[] ctr, bool isShown, Color r )
        {
            if (ctr != null)
            {
                foreach (Control c in ctr)
                {
                    c.Visible = isShown;

                    ChangeFontColor(c, r);
                }
            }
        }

        private void ChangeFontColor()
        { 
            foreach(Control c in this.Controls)
            {
                if (c.GetType() == typeof(Label))
                {
                    ((Label)c).ForeColor = Color.Black;
                }
            }
        }

        private void ChangeFontColor(Control c, Color clr)
        {
            if (c.GetType() == typeof(Label))
            {
                ((Label)c).ForeColor = clr;
            }
        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox2.Checked)
            {
                Clear(true, new Control[] { checkBox2, checkBox3 });

                checkBox3.Checked = false;

                label30.Visible = true;
                checkBox4.Visible = true;
                checkBox5.Visible = true;
                checkBox13.Visible = true;
                checkBox14.Visible = true;


                label66.Visible = false;
                txtUSPCity.Visible = false;
                label67.Visible = false;
                txtUSPState.Visible = false;
                label68.Visible = false;
                txtUSPZipcode.Visible = false;
                checkBox8.Visible = false;
                txtUSPStrt.Visible = false;
                label69.Visible = false;
                btnSearchUSP.Visible = false;
            }

        }

        private void checkBox3_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox3.Checked)
            {
                checkBox2.Checked = false;

                var result = MessageBox.Show("Would you like to change your response?",
                    "Is this bill readable",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    checkBox3.Checked = false;
                    return;
                }

                Clear(true, new Control[] { checkBox3, checkBox2 });

                checkBox2.Checked = false;

                //info.AccountNumber = "None";
                //info.Amount = "0.00";
                //info.vendor_group = "None";
                                
                //info.Complete = 6;
                //info.PendingReasons = "Blank/Unreadable/Cut Invoice";
                //info.VendorAssignedStatus = "ForAudit";
                //info.InvoiceFolderName = strCreateInvoiceFolderName(NoActionNeededFolder);

                info.Complete = 21;
                info.PendingReasons = "Blank/Unreadable/Cut Invoice";          

                MessageBox.Show("Please click Save!");

                SaveButtonsEnabled(true);
            }
            else
            {
                label30.Visible = true;
                checkBox4.Visible = true;
                checkBox5.Visible = true;
                checkBox13.Visible = true;
                checkBox14.Visible = true;
            }
        }

        private void checkBox4_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox4.Checked)
            {
                //checkBox4.Checked = true;
                checkBox5.Checked = false;
                checkBox13.Checked = false;
                checkBox14.Checked = false;

                info.vendor_group = "E";

                label61.Visible = false;
                cmbBillName.Visible = false;

                label64.Visible = false;
                label65.Visible = false;
                chknYes.Visible = false;
                chknNo.Visible = false;

                Clear(true, new Control[] { checkBox2, checkBox3 });

                label60.Visible = true;
                chkASFIYes.Visible = true;
                chkASFINo.Visible = true;

                if (!isBalTransfer)
                {

                    if (queue_code == 70)
                    {
                        chkASFIYes.Checked = true;
                        //chkASFINo.Checked = false;
                        chkASFIYes.Enabled = false;
                        chkASFINo.Enabled = false;
                    }


                    if (chkASFIYes.Checked)
                    {
                        chkASFIYes_CheckedChanged(null, null);
                    }

                    if (chkASFINo.Checked)
                    {
                        chkASFINo_CheckedChanged(null, null);
                    }

                    if (info.Complete == 0)
                    {
                        label59.Visible = false;
                        numBalTransfer.Visible = false;
                    }
                }                
            }
            else
            {
                try
                {
                    info.vendor_group = null;
                }
                catch
                {
                }
                
            }
        }

        private void checkBox5_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox5.Checked)
            {
                checkBox4.Checked = false;
                //checkBox5.Checked = true;
                checkBox13.Checked = false;
                checkBox14.Checked = false;

                try
                {
                    info.vendor_group = "G";
                }
                catch
                { 
                }

                label61.Visible = false;
                cmbBillName.Visible = false;

                label64.Visible = false;
                label65.Visible = false;
                chknYes.Visible = false;
                chknNo.Visible = false;

                Clear(true, new Control[] { checkBox2, checkBox3 });

                label60.Visible = true;
                chkASFIYes.Visible = true;
                chkASFINo.Visible = true;

                if (!isBalTransfer)                {


                    if (queue_code == 70)
                    {
                        chkASFIYes.Checked = true;
                        //chkASFINo.Checked = false;
                        chkASFIYes.Enabled = false;
                        chkASFINo.Enabled = false;
                    }

                    if (chkASFIYes.Checked)
                    {
                        chkASFIYes_CheckedChanged(null, null);
                    }

                    if (chkASFINo.Checked)
                    {
                        chkASFINo_CheckedChanged(null, null);
                    }

                    if (info.Complete == 0)
                    {
                        label59.Visible = false;
                        numBalTransfer.Visible = false;
                    }
                }                
            }
            else
            {
                try
                {
                    info.vendor_group = null;
                }
                catch
                { 
                }               
            }   
        }

        private void SetDoNotMail(object sender, EventArgs e)
        {
            if (checkBox11.Checked)
            {
                if (info.SpecialInstruction == string.Empty)
                {
                    //info.Complete = 0;
                    //info.PendingReasons = "Do Not Mail";
                    //info.InvoiceFolderName = strCreateInvoiceFolderName(CallOutFolder);

                    info.Complete = 21;
                    info.PendingReasons = "Do Not Mail";

                    MessageBox.Show("Please click SAVE.", "Instructions", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                checkBox12.Checked = false;
            }
            else if (checkBox12.Checked)
            {
                checkBox11.Checked = false;
            }
        }

        private void breakdown_cb1_DrawItem(object sender, DrawItemEventArgs e)
        {
            if (e.Index == -1) { return; }
            
            ComboBox cb = sender as ComboBox;

            string[] message = new string[] { "Can be written as New charges, Current bill amount", 
                "Can be written as Past due, Balance forward, Previous bill amount, amount of your last bill, Last bill amount, last bill charge, past charge",
                "Last payment Amount, Credit, Credit received, Last credit amount",
                "Late carrying charge, Reminder late fee, Late charge, pre shut off late fee",
                "Shut off fee, disconnection charge, reconnection fee"};

            Point p = new Point(MousePosition.X, MousePosition.Y + 40);

           
            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
            {
                if (e.Index < 4)
                {
                    toolTip1.Show(message[e.Index], this, p);
                }
            }

            e.DrawBackground();

            if ((e.State & DrawItemState.Selected) == DrawItemState.Selected)
            {
                e.Graphics.DrawString(cb.Items[e.Index].ToString(), e.Font, Brushes.White, new Point(e.Bounds.X, e.Bounds.Y));
            }
            else
            {
                e.Graphics.DrawString(cb.Items[e.Index].ToString(), e.Font, Brushes.Black, new Point(e.Bounds.X, e.Bounds.Y));
            }
        }

        private void HideToolTip(object sender, EventArgs e)
        {
            toolTip1.Hide(textBox34);
        }

        private void realResolutionToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form rrQueue = new Forms.Queue.RealResolutionQueue();
            rrQueue.Show();
        }

        private void LoadCallOutData()
        {
            Database.DBConnection.DatabaseREO_Properties propCon = new Database.DBConnection.DatabaseREO_Properties();
            Database.DBConnection.DatabaseAccountNumber acctCon = new Database.DBConnection.DatabaseAccountNumber();

            if (info.AccountNumber != string.Empty)
            {
                textBox10.Visible = true;
                textBox10.Text = info.AccountNumber;
            }

            if (info.property_code != string.Empty)
            {
                Database.DB.REO_Properties propInfo = new Database.DB.REO_Properties();
                Database.DB.AccountNumber acctInfo = new Database.DB.AccountNumber();
                
                propInfo = propCon.GetData(info.property_code);
                acctInfo = acctCon.GetDataProcessor1(info.AccountNumber, info.property_code);

                if (propInfo != null)
                {
                    if (acctInfo != null)
                    {
                        DisplayPropertyInDGV(propInfo, acctInfo);
                    }
                    else
                    {
                        DisplayPropertyInDGV(propInfo, info.vendor_group);
                    }
                }
            }
        }

        private void checkBox14_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox14.Checked)
            {
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                checkBox13.Checked = false;

                Clear(true, new Control[] { checkBox2, checkBox3 });

                // Type of Bill
                label38.Visible = true;
                comboBox2.Visible = true;

                label61.Visible = false;
                cmbBillName.Visible = false;

                label64.Visible = false;
                label65.Visible = false;
                chknYes.Visible = false;
                chknNo.Visible = false;

                info.vendor_group = "None";
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string[] message = new string[] {"Greetings,", Environment.NewLine, "This is an automated email. Please do not reply back.",
            "Please contact PhpUMAL@altisource.com for queries.", Environment.NewLine, "Warm Regards,", "Expense Management Team"};

            if (comboBox2.Text != string.Empty)
            {

                //label60.Visible = true;
                //chkASFIYes.Visible = true;
                //chkASFINo.Visible = true;


                //if (chkASFIYes.Checked)
                //{
                //    chkASFIYes_CheckedChanged(null, null);
                //}

                //if (chkASFINo.Checked)
                //{
                //    chkASFINo_CheckedChanged(null, null);
                //}

                if (comboBox2.SelectedIndex == 0 || comboBox2.SelectedIndex == 1 || comboBox2.SelectedIndex == 6)
                {

                    label60.Visible = false;
                    chkASFIYes.Visible = false;
                    chkASFINo.Visible = false;

                    button11_Click(null, null);                 
                }
                else
                {
                    button11.Visible = false;

                    label14.Visible = false;
                    textBox10.Visible = false;
                    checkBox6.Visible = false;
                    button7.Visible = false;
                    button9.Visible = false;

                    button2.Enabled = false;
                    button3.Enabled = false;

                    label47.Visible = false;
                    label58.Visible = false;
                    //label59.Visible= false;
                    //txtTransferCount.Visible = false; 
                    checkBox15.Visible = false;
                    checkBox16.Visible = false;
                    chkTransAmtYes.Visible = false;
                    chkTransAmtNo.Visible = false;
                    pictureBox5.Visible = false;

                    label60.Visible = false;
                    chkASFIYes.Visible = false;
                    chkASFINo.Visible = false;

                    if (!checkBox15.Checked)
                    {
                        checkBox16.Checked = false;
                    }

                    //if (!chkTransAmtYes.Checked)
                    //{
                    //    chkTransAmtNo.Checked = true;
                    //}

                    label14.Visible = false;
                    textBox10.Visible = false;
                    checkBox6.Visible = false;
                    button7.Visible = false;
                    button9.Visible = false;
                }               

                switch (comboBox2.Text)
                { 
                    case "Garbage/Trash/Disposal":
                        info.vendor_group = "T"; 
                        //info.Complete = 1; 
                        //info.PendingReasons = "Manual WI";
                        //info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);

                        info.Complete = 21;
                        info.PendingReasons = "Pending for Audit";

                        break;

                    case "Stormwater/Drainage/WasteWater/Sewer":
                        info.vendor_group = "D";

                        //info.Complete = 1;
                        //info.PendingReasons = "Manual WI";
                        //info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder); 

                        info.Complete = 21;
                        info.PendingReasons = "Pending for Audit";

                        break;

                    case "Septic":
                        info.vendor_group = "S";

                        //info.Complete = 1;
                        //info.PendingReasons = "Manual WI";
                        //info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder); 

                        info.Complete = 21;
                        info.PendingReasons = "Pending for Audit";

                        break;

                    case "TaxBill/TaxSale/property bill":
                        var result = MessageBox.Show("Is the invoice a Tax Bill / Tax Sale / Property Bill?",
                            "Type of Bill", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result == DialogResult.Yes)
                        {
                            //SendEmail(CurrentLoadedInvoice, comboBox2.Text, message, "tax@ocwen.com");

                            info.vendor_group = "None";
                            
                            
                            //info.Complete = 6;
                            //info.PendingReasons = comboBox2.Text;
                            //info.InvoiceFolderName = strCreateInvoiceFolderName(NoActionNeededFolder);

                            info.Complete = 21;
                            info.PendingReasons = comboBox2.Text;
                            button2_Click(null, null);
                        }
                        else
                        {
                            comboBox2.Text = string.Empty;
                        }
                        break;

                    case "Cut Grass/weeds removal/Lawn maintenance":
                        var result1 = MessageBox.Show("Is the invoice a Cut Grass / Weeds Removal / Lawn Maintenance Bill?",
                            "Type of Bill", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result1 == DialogResult.Yes)
                        {
                            //SendEmail(CurrentLoadedInvoice, comboBox2.Text, message, "reocodeviolation@altisource.com");

                            info.vendor_group = "None";
                            
                            
                            //info.Complete = 6;
                            //info.PendingReasons = comboBox2.Text;
                            //info.InvoiceFolderName = strCreateInvoiceFolderName(NoActionNeededFolder);

                            info.Complete = 21;
                            info.PendingReasons = comboBox2.Text;

                            button2_Click(null, null);
                        }
                        else
                        {
                            comboBox2.Text = string.Empty;
                        }
                        break;

                    case "Removal of debris":
                        var result2 = MessageBox.Show("Is the invoice a Removal of Debris Bill?",
                            "Type of Bill", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result2 == DialogResult.Yes)
                        {
                            //SendEmail(CurrentLoadedInvoice, comboBox2.Text, message, "reocodeviolation@altisource.com");
                            
                            info.vendor_group = "None";
                            
                            //info.Complete = 6;
                            //info.PendingReasons = comboBox2.Text;
                            //info.InvoiceFolderName = strCreateInvoiceFolderName(NoActionNeededFolder);

                            info.Complete = 21;
                            info.PendingReasons = comboBox2.Text;

                            button2_Click(null, null);
                        }
                        else
                        {
                            comboBox2.Text = string.Empty;
                        }
                        break;

                    case "Association/assessment fee":
                        var result4 = MessageBox.Show("Is the invoice an Association/assessment fee?",
                            "Type of Bill", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result4 == DialogResult.Yes)
                        {
                            //SendEmail(CurrentLoadedInvoice, comboBox2.Text, message, "reocodeviolation@altisource.com");

                            info.vendor_group = "None";

                            //info.Complete = 6;
                            //info.PendingReasons = comboBox2.Text;
                            //info.InvoiceFolderName = strCreateInvoiceFolderName(NoActionNeededFolder);

                            info.Complete = 21;
                            info.PendingReasons = comboBox2.Text;

                            button2_Click(null, null);
                        }
                        else
                        {
                            comboBox2.Text = string.Empty;
                        }
                        break;

                    case "Others (Non-Billing Information)":
                        var result3 = MessageBox.Show("Is invoice not a utility bill?", "Not Utility Bill", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (result3 == DialogResult.Yes)
                        {
                            info.vendor_group = "None";
                            
                            //info.Complete = 6;

                            //if ((iniComplete == "20" || iniComplete == "32") && info.Complete != 0 && info.Complete != 12 && info.PendingReasons != "Others (Non-Billing Information)" && info.PendingReasons != "TaxBill/TaxSale/property bill" && info.PendingReasons != "Blank/Unreadable/Cut Invoice" && info.PendingReasons != "Cut Grass/Weeds removal/Lawn maintenance" && info.PendingReasons != "Removal of debris" && info.PendingReasons != "Property Status: Bankruptcy")
                            //{
                            //    info.VendorAssignedStatus = "ForAudit";
                            //}

                            //info.PendingReasons = comboBox2.Text;
                            //info.InvoiceFolderName = strCreateInvoiceFolderName(NoActionNeededFolder);

                            info.Complete = 21;
                            info.PendingReasons = comboBox2.Text;

                            button2_Click(null, null);
                        }
                        else
                        {
                            comboBox2.Text = string.Empty;
                        }
                        break;
                }
            }
            else
            {
                button11.Visible = false;
            }
        }

        private void checkBox13_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox13.Checked)
            {
                checkBox4.Checked = false;
                checkBox5.Checked = false;
                //checkBox13.Checked = true;
                checkBox14.Checked = false;

                info.vendor_group = "W";

                label61.Visible = false;
                cmbBillName.Visible = false;

                label64.Visible = false;
                label65.Visible = false;
                chknYes.Visible = false;
                chknNo.Visible = false;

                Clear(true, new Control[] { checkBox2, checkBox3 });

                label60.Visible = true;
                chkASFIYes.Visible = true;
                chkASFINo.Visible = true;

                if (!isBalTransfer)
                {

                    if (queue_code == 70)
                    {
                        chkASFIYes.Checked = true;
                        //chkASFINo.Checked = false;
                        chkASFIYes.Enabled = false;
                        chkASFINo.Enabled = false;
                    }

                    if (chkASFIYes.Checked)
                    {
                        chkASFIYes_CheckedChanged(null, null);
                    }

                    if (chkASFINo.Checked)
                    {
                        chkASFINo_CheckedChanged(null, null);
                    }

                    if (info.Complete == 0)
                    {
                        label59.Visible = false;
                        numBalTransfer.Visible = false;
                    }
                }
            }
            else
            {
                try
                {
                    info.vendor_group = null;
                }
                catch
                {
                }
            }
        }

        private void textBox4_MouseHover(object sender, EventArgs e)
        {
            panel_acctNumber.BackgroundImage = UC.Properties.Resources.sample_EM_house;
            panel_acctNumber.Visible = true;
        }

        private void textBox4_MouseLeave(object sender, EventArgs e)
        {
            panel_acctNumber.Visible = false;
        }

        private void textBox14_MouseHover(object sender, EventArgs e)
        {
            panel_acctNumber.BackgroundImage = UC.Properties.Resources.sample_EM_street;
            panel_acctNumber.Visible = true;
        }

        private void textBox14_MouseLeave(object sender, EventArgs e)
        {
            panel_acctNumber.Visible = false;
        }


        //----------------------------------------------------------------------
        //  Load Vendor Dynamics Report on DataGridView
        //----------------------------------------------------------------------

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            BackgroundWorker bwLoadVendorInDGV = new BackgroundWorker();
            bwLoadVendorInDGV.DoWork += new DoWorkEventHandler(bwLoadVendorInDGV_DoWork);
            bwLoadVendorInDGV.RunWorkerAsync(comboBox5.Text);
            
            while (bwLoadVendorInDGV.IsBusy)
            {
                System.Windows.Forms.Application.DoEvents();
            }

            if (comboBox5.Text.Contains("----") || comboBox5.Text.Contains("No Matching"))
            {
                //Form vendorIdRequestForm = new Forms.VendorIdRequest(this, "CREATE NEW");
                //vendorIdRequestForm.ShowDialog();  

                Form vendorIdRequestForm = new VendorIDRequestForm.Form1("CREATE NEW", "", "EMS", info.Invoice, info.vendor_group);

                vendorIdRequestForm.ShowDialog();
            }
        
        }

        private void bwLoadVendorInDGV_DoWork(object sender, DoWorkEventArgs e)
        {
            Database.DBConnection.DatabaseVDR_Data conn = new Database.DBConnection.DatabaseVDR_Data();
            List<Database.DB.VDR> vdrData = conn.GetData(e.Argument.ToString());

            dgv_usp.Invoke((Action)delegate
            {
                dgv_usp.Visible = true;
                dgv_usp.Rows.Clear();
            });

            if (vdrData != null)
            {
                foreach (Database.DB.VDR data in vdrData)
                {
                    dgv_usp.Invoke((Action)delegate
                    {
                        dgv_usp.Rows.Add(data.Vendor_ID, data.Vendor_Name, data.Address + ", " + data.City + ", " + data.State + ", " + data.Zipcode, data.vdr_status == 1 ? "Active" : "Inactive", GetStatus(data.vms_status.ToString()));
                    });
                }

            }
            else
            {
                dgv_usp.Invoke((Action)delegate
                {
                    dgv_usp.Rows.Add(null, "NO DATA FOUND", null, null);
                });
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //if (textBox1.TextLength > 3)
            //{
            //    BackgroundWorker bwSearchZipcode = new BackgroundWorker();
            //    bwSearchZipcode.DoWork += new DoWorkEventHandler(bwSearchZipcode_DoWork);
            //    bwSearchZipcode.RunWorkerAsync(textBox1.Text);
            //}

            info.ZipcodeEntered = textBox1.Text;
        }

        private void bwSearchZipcode_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                button7.Invoke((Action)delegate
                {
                    button7.Enabled = false;
                });

                string zipcode = (string)e.Argument;

                if (e.Argument.ToString().StartsWith("0")) 
                { 
                    zipcode = e.Argument.ToString().Substring(1, 4); 
                }

                Database.DBConnection.DatabaseZipcodeStateCity conn = new Database.DBConnection.DatabaseZipcodeStateCity();
                List<Database.DB.ZipcodeStateCity> dataList = conn.GetCityStates(zipcode);

                textBox16.Invoke((Action)delegate
                {
                    textBox16.AutoCompleteCustomSource.Clear();
                });

                if (dataList != null)
                {
                    
                    foreach (Database.DB.ZipcodeStateCity data in dataList)
                    {
                        textBox17.Invoke((Action)delegate
                        {
                            //textBox17.Enabled = true;
                            textBox17.Text = data.abbrev_state;
                        });

                        textBox16.Invoke((Action)delegate
                        {
                            //textBox16.Enabled = true;
                            textBox16.AutoCompleteCustomSource.Add(data.city_name);
                            textBox16.Text = data.city_name;
                        });
                    }
                }
                else
                {
                    textBox17.Invoke((Action)delegate
                    {
                        //textBox17.Enabled = false;
                        textBox17.Text = string.Empty;
                    });

                    textBox16.Invoke((Action)delegate
                    {
                        //textBox16.Enabled = false;
                        textBox16.AutoCompleteCustomSource.Clear();
                        textBox16.Text = string.Empty;
                    });
                }
            }
            catch 
            {
                //do nothing
            }
            finally
            {
                button7.Invoke((Action)delegate
                {
                    button7.Enabled = true;
                });
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Clear(true, new Control[] { label38, textBox10, checkBox6, comboBox2, checkBox2, checkBox3});

            if (queue_code == 70)
            {
                chkASFIYes.Checked = true;
                //chkASFINo.Checked = false;
                chkASFIYes.Enabled = false;
                chkASFINo.Enabled = false;
            }

            if (chkASFIYes.Checked)
            {
                chkASFIYes_CheckedChanged(null, null);
            }

            label60.Visible = true;
            chkASFIYes.Visible = true;
            chkASFINo.Visible = true;
            
            //if (comboBox2.SelectedIndex == 0 || comboBox2.SelectedIndex == 1 || comboBox2.SelectedIndex == 6)
            //{
            //    label14.Visible = true;
            //    textBox10.Visible = true;
            //    checkBox6.Visible = true;
            //    button7.Visible = true;
            //    button9.Visible = true;

            //    button2.Enabled = true;
            //    button3.Enabled = true;

            //    label47.Visible = true;
            //    label58.Visible = true;
                
            //    //label59.Visible = true;
            //    //txtTransferCount.Visible = true;
                
            //    checkBox15.Visible = true;
            //    checkBox16.Visible = true;
            //    chkTransAmtYes.Visible = true;
            //    chkTransAmtNo.Visible = true;
            //    pictureBox5.Visible = true;
            //    if (!checkBox15.Checked)
            //    {
            //        checkBox16.Checked = true;
            //    }

            //    if (!chkTransAmtYes.Checked)
            //    {
            //        chkTransAmtNo.Checked = true;
            //    }

            //    label14.Visible = true;
            //    textBox10.Visible = true;
            //    checkBox6.Visible = true;
            //    button7.Visible = true;
            //    button9.Visible = true;
            //}
            //else
            //{
            //    label14.Visible = false;
            //    textBox10.Visible = false;
            //    checkBox6.Visible = false;
            //    button7.Visible = false;
            //    button9.Visible = false;

            //    button2.Enabled = false;
            //    button3.Enabled = false;

            //    label47.Visible = false;
            //    label58.Visible = false;
            //    //label59.Visible = false;
            //    //txtTransferCount.Visible = false;
            //    checkBox15.Visible = false;
            //    checkBox16.Visible = false;
            //    chkTransAmtYes.Visible = false;
            //    chkTransAmtNo.Visible = false;
            //    pictureBox5.Visible = false;
            //    if (!checkBox15.Checked)
            //    {
            //        checkBox16.Checked = false;
            //    }

            //    if (!chkTransAmtYes.Checked)
            //    {
            //        chkTransAmtNo.Checked = false;
            //    }

            //    label14.Visible = false;
            //    textBox10.Visible = false;
            //    checkBox6.Visible = false;
            //    button7.Visible = false;
            //    button9.Visible = false;
            //}            
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) { return; }
            
            dataGridView1.Enabled = false;


            if ((string)dataGridView1.Rows[e.RowIndex].Cells[1].Value != "NO PROPERTY FOUND")
            {

                string full_address = (string)dataGridView1.Rows[e.RowIndex].Cells[1].Value;
                string strClientCode = (string)dataGridView1.Rows[e.RowIndex].Cells[2].Value;

                info.property_code = (string)dataGridView1.Rows[e.RowIndex].Cells[0].Value;

                if (strClientCode == "RESI" || strClientCode.Contains("Residential"))
                {
                    info.client_code = "RESI";
                }
                else
                {
                    info.client_code = strClientCode;
                }

                info.isActive = (string)dataGridView1.Rows[e.RowIndex].Cells[3].Value == "Active" ? true : false;

                BackgroundWorker LoadPreviousUsp = new BackgroundWorker();
                LoadPreviousUsp.DoWork += new DoWorkEventHandler(LoadPreviousUsp_DoWork);
                LoadPreviousUsp.RunWorkerAsync();

                info.Complete = 21;
                info.PendingReasons = "Pending for Audit";
            }            

            dataGridView1.Enabled = true;

            //if (!full_address.ToUpper().Contains("NO PROPERTY"))
            //{
            //    info.property_code = (string)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
            //    string strClientCode = (string)dataGridView1.Rows[e.RowIndex].Cells[2].Value;
            //    info.isActive = (string)dataGridView1.Rows[e.RowIndex].Cells[3].Value == "Active" ? true : false;
            //    string podId = (string)dataGridView1.Rows[e.RowIndex].Cells[9].Value;

            //    //---------------------------
            //    // Bankruptcy filter function
            //    //---------------------------
            //    if (isPropertyOnBankruptcyStatus(info.property_code))
            //    {
            //        info.AccountNumber = "None";
            //        info.Amount = "0.00";
            //        info.VendorAssignedStatus = "Dont Assign";
            //        info.Complete = 1;
            //        info.PendingReasons = "Property Status: Bankruptcy";
            //        info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);

            //        MessageBox.Show("Property status is Bankruptcy. Please click OK");

            //        button2_Click(null, null);
            //    }

            //    //------------------------------
            //    //  Solves Client Code is empty
            //    //------------------------------
            //    if (strClientCode == "RESI" || strClientCode.Contains("Residential"))
            //    {
            //        info.client_code = "RESI";
            //    }
            //    else
            //    {
            //        info.client_code = strClientCode;
            //    }

            //    BackgroundWorker LoadPreviousUsp = new BackgroundWorker();
            //    LoadPreviousUsp.DoWork += new DoWorkEventHandler(LoadPreviousUsp_DoWork);
            //    LoadPreviousUsp.RunWorkerAsync();

            //    //--------------------------
            //    // Check if Real Resolution
            //    //--------------------------
            //    if (info.client_code == "RR")
            //    {
            //        info.Complete = 1;
            //        info.PendingReasons = "Real Resolution";
            //        info.InvoiceFolderName = strCreateInvoiceFolderName(RR_NEW);
            //        info.VendorAssignedStatus = "Dont Assign - RR Property";
            //    }
            //    else
            //    {

            //        // ----------------------------
            //        // Check if Property is HSBC
            //        // ----------------------------
            //        if (info.client_code == "HSBC")
            //        {
            //            info.Complete = 1;
            //            info.PendingReasons = "HSBC Property";
            //            info.InvoiceFolderName = strCreateInvoiceFolderName(HSBC_NEW);
            //            info.VendorAssignedStatus = "Dont Assign - HSBC Property";
            //        }
            //        else
            //        {
                        //CheckIfOutOfREO(e.RowIndex);
                        //info.isOutOfREO = isOutOfREO;
                        //// Check if Trailing Asset
                        //if (isTrailingAsset)
                        //{
                        //    info.Complete = 1;
                        //    info.PendingReasons = "Trailing Asset";
                        //    info.InvoiceFolderName = strCreateInvoiceFolderName(TrailingAssetFolder);
                        //}
                        //else
                        //{
                        //    info.Complete = 1;
                        //    info.PendingReasons = "Bulk Upload";
                        //    info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                        //}

            //            //------------------------------------------------------
            //            // Solves Bulk Upload Error: Property ID does not exist
            //            //------------------------------------------------------
            //            if (info.isActive == false)
            //            {
            //                Property_Status_Change_Date = new DateTime();
            //                Property_Status_Change_Date = (DateTime)dataGridView1.Rows[e.RowIndex].Cells["cproperty_status_change_date"].Value;
            //                Inactive_Date = new DateTime();
            //                Inactive_Date = (DateTime)dataGridView1.Rows[e.RowIndex].Cells["cinactivedate"].Value;

            //                info.Complete = 1;
            //                info.PendingReasons = "Manual WI";
            //                info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
            //            }

            //            //--------------------------------------------------------------------------------------------
            //            // Solves Bulk Upload Error: The specified Line Item is not mapped at Property Investor level
            //            //--------------------------------------------------------------------------------------------
            //            if (podId == "POD GSE")
            //            {
            //                info.Complete = 1;
            //                info.PendingReasons = "Manual WI";
            //                info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
            //            }
            //        }
            //    }
            //}
            //dataGridView1.Enabled = true;
        }

        private bool isPropertyOnBankruptcyStatus(string propertycode)
        {
            try
            {
                Database.DBConnection.DatabaseREO_Properties prop_conn = new Database.DBConnection.DatabaseREO_Properties();
                Database.DB.REO_Properties prop_data = new Database.DB.REO_Properties();
                prop_data = prop_conn.GetActiveClientCode(info.property_code);
                if (prop_data != null)
                {
                    if (prop_data.property_status == "Bankruptcy")
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Bankruptcy filter encountered an error. " + ex.Message);
                return false;
            }
        }

        private void LoadPreviousUsp_DoWork(object sender, EventArgs e)
        {
            Database.DBConnection.DatabaseEM_Tasks emConn = new Database.DBConnection.DatabaseEM_Tasks();
            Database.DBConnection.DatabaseVDR_Data vdrConn = new Database.DBConnection.DatabaseVDR_Data();
            Database.DB.EM_Tasks emInfo = emConn.GetPreviousInfo(info.property_code, info.client_code, info.AccountNumber, info.vendor_group);
            Database.DB.VDR vdrInfo = new Database.DB.VDR();

            if (emInfo != null || (chkTransAmtYes.Checked && invoicecount > 0))
            {

                if ((chkTransAmtYes.Checked && invoicecount > 0))
                {
                    vdrInfo = vdrConn.GetDataProcessor(ini_vendorid);
                }
                else
                {
                    vdrInfo = vdrConn.GetDataProcessor(emInfo.VendorId);
                }
                

                if (vdrInfo != null)
                {
                    dgv_usp.Invoke((Action)delegate
                    {
                        dgv_usp.Visible = true;
                        dgv_usp.Rows.Clear();

                        dgv_usp.Rows.Add(vdrInfo.Vendor_ID, vdrInfo.Vendor_Name, vdrInfo.Address + ", " + vdrInfo.City + ", " + vdrInfo.State + ", " + vdrInfo.Zipcode, vdrInfo.vdr_status == 1 ? "Active" : "Inactive", GetStatus(vdrInfo.vms_status.ToString()));
                    });
                }
                else
                {

                    comboBox5.Invoke((Action)delegate
                    {
                        comboBox5.Visible = true;
                        comboBox5.DroppedDown = true;
                    });

                    label4.Invoke((Action)delegate
                    {
                        label4.Visible = true;
                    });

                    button_noMatchUSP.Invoke((Action)delegate
                    {
                        button_noMatchUSP.Visible = true;
                    });
                }
            }
            else
            {
                Database.DBConnection.DatabaseUtilitiesDump uConn = new Database.DBConnection.DatabaseUtilitiesDump();
                Database.DB.Utilities_Dump uDB = uConn.GetData(info.property_code, info.vendor_group);

                if (uDB != null)
                {
                    vdrInfo = vdrConn.GetDataProcessor(uDB.elec_vendor_code);

                    if (vdrInfo != null)
                    {
                        dgv_usp.Invoke((Action)delegate
                        {
                            dgv_usp.Visible = true;
                            dgv_usp.Rows.Clear();

                            dgv_usp.Rows.Add(vdrInfo.Vendor_ID, vdrInfo.Vendor_Name, vdrInfo.Address + ", " + vdrInfo.City + ", " + vdrInfo.State + ", " + vdrInfo.Zipcode, vdrInfo.vdr_status == 1 ? "Active" : "Inactive", GetStatus(vdrInfo.vms_status.ToString()));
                        });
                    }
                    else
                    {

                        comboBox5.Invoke((Action)delegate
                        {
                            comboBox5.Visible = true;
                            comboBox5.DroppedDown = true;
                        });

                        label4.Invoke((Action)delegate
                        {
                            label4.Visible = true;
                        });

                        button_noMatchUSP.Invoke((Action)delegate
                        {
                            button_noMatchUSP.Visible = true;
                        });
                    }
                }
                else
                {
                    comboBox5.Invoke((Action)delegate
                    {
                        comboBox5.Visible = true;
                        comboBox5.DroppedDown = true;
                    });

                    label4.Invoke((Action)delegate
                    {
                        label4.Visible = true;
                    });

                    button_noMatchUSP.Invoke((Action)delegate
                    {
                        button_noMatchUSP.Visible = true;
                    });

                    btnSearchUSP.Invoke((Action)delegate
                    {
                        btnSearchUSP.Visible = true;
                    });

                    txtUSPStrt.Invoke((Action)delegate
                    {
                        txtUSPStrt.Visible = true;
                    });
                    
                    txtUSPCity.Invoke((Action)delegate
                    {
                        txtUSPCity.Visible = true;
                    });

                    txtUSPState.Invoke((Action)delegate
                    {
                        txtUSPState.Visible = true;
                    });

                    txtUSPZipcode.Invoke((Action)delegate
                    {
                        txtUSPZipcode.Visible = true;
                    });


                    label66.Invoke((Action)delegate
                    {
                        label66.Visible = true;
                    });

                    label67.Invoke((Action)delegate
                    {
                        label67.Visible = true;
                    });

                    label68.Invoke((Action)delegate
                    {
                        label68.Visible = true;
                    });

                    label69.Invoke((Action)delegate
                    {
                        label69.Visible = true;
                    });

                    checkBox8.Invoke((Action)delegate
                    {
                        checkBox8.Visible = true;
                    });
                }
            }
        }

        private void dgv_usp_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) { return; }

            dgv_usp.Enabled = false;

            var result = MessageBox.Show("Is this the correct match? Please check the invoice.",
                "Utility Service Provider", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.No)
            {
                MessageBox.Show("Please choose the appropriate USP or just click on no match found",
                    "Utility Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Information);

                dgv_usp.Rows.Clear();
                comboBox5.Visible = true;
                comboBox5.DroppedDown = true;
                label4.Visible = true;
                button_noMatchUSP.Visible = true;
                label66.Visible = true;
                txtUSPCity.Visible = true;
                label67.Visible = true;
                txtUSPState.Visible = true;
                label68.Visible = true;
                txtUSPZipcode.Visible = true;
                checkBox8.Visible = true;
                txtUSPStrt.Visible = true;
                label69.Visible = true;
                btnSearchUSP.Visible = true;
            }
            else
            {

                info.usp_name = dgv_usp.Rows[e.RowIndex].Cells[1].Value.ToString();
                info.VendorId = dgv_usp.Rows[e.RowIndex].Cells[0].Value.ToString();
                info.vendor_address = dgv_usp.Rows[e.RowIndex].Cells[2].Value.ToString();

                string name = string.Empty;

                char[] delimiterChars = { ' ', '\t' };

                name = comboBox5.Text.Trim().ToLower().Replace("'", "''");

                string[] words = name.Split(delimiterChars);
                string x = "%";

                foreach (string w in words)
                {
                    x += w + "%";
                }

                //Database.DB.VDR vendor_data = listOfVDR.FirstOrDefault(p => p.Vendor_ID == info.VendorId);

                //if (vendor_data != null)
                //{
                    if (dgv_usp.Rows[e.RowIndex].Cells[3].Value.ToString() == "Active" && (dgv_usp.Rows[e.RowIndex].Cells[4].Value.ToString() == "Active" || dgv_usp.Rows[e.RowIndex].Cells[4].Value.ToString() == "-"))
                    {
                        //info.VendorAssignedStatus = "New";

                        //if ((info.client_code == "RESI" || info.client_code == "OLSR" || info.client_code == "PFC") && info.Complete != 9)
                        //{
                            //info.VendorAssignedStatus = "ForAudit";
                        //}

                        info.Complete = 21;
                        info.PendingReasons = "Pending for Audit";

                        ResetBillingInformation();
                    }
                    else if (dgv_usp.Rows[e.RowIndex].Cells[3].Value.ToString() == "Active" && dgv_usp.Rows[e.RowIndex].Cells[4].Value.ToString() == "Inactive")
                    {
                        //info.VendorAssignedStatus = "VMS ACTIVATION";

                        info.Complete = 21;
                        info.PendingReasons = "Vendor ID Request";
                        info.VendorId = dgv_usp.CurrentRow.Cells[0].Value.ToString().Trim();

                        SqlConnection cnn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;"); ;
                        SqlCommand cmd;
                        string sql = string.Format(@"if (select COUNT(*) from tbl_EM_VendorIDRequest where [type] = 'VMS ACTIVATION' and [status] = 'NEW' and vendor_account_requested = '{0}') = 0
                                                    begin
                                                    insert into tbl_EM_VendorIDRequest(vendor_account_requested, [type], date_requested, invoice_name, userid_requestor, name, postal_code, [state], city, street_name, [status], source_of_information)
                                                    select top 1 Vendor_ID, 'VMS ACTIVATION', GETDATE(), 'Invoice', '{2}', Vendor_Name, Zipcode, [State], City, [Address], 'NEW', '{1}' from tbl_EM_VDR where Vendor_ID = '{0}'
                                                    end
                                                    ",
                                                 info.VendorId,
                                                 info.Invoice,
                                                 Environment.UserName);

                        try
                        {
                            cnn.Open();
                            cmd = new SqlCommand(sql, cnn);
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                            cnn.Close();
                        }
                        catch (Exception ex)
                        {
                        }

                        ResetBillingInformation();
                    }
                    else if (dgv_usp.Rows[e.RowIndex].Cells[3].Value.ToString() == "Inactive")
                    {
                        //Form vendorIdRequestForm = new Forms.VendorIdRequest(this, "ACTIVATION");
                        //vendorIdRequestForm.Show();
                                              
                        
                        info.Complete = 21;
                        info.PendingReasons = "Vendor ID Request";

                        //Form vendorIdRequestForm = new Forms.VendorIdRequest(this, "ACTIVATION");
                        //vendorIdRequestForm.Show();

                        dgv_usp.Rows.Clear();

                        Form vendorIdRequestForm = new VendorIDRequestForm.Form1("ACTIVATION", info.VendorId, "EMS", info.Invoice, info.vendor_group);

                        vendorIdRequestForm.ShowDialog();

                        ResetBillingInformation();
                    }

                    if (chkTransAmtYes.Checked && invoicecount > 0)
                    {
                        if (ini_invdate == "1/1/0001 12:00:00 AM")
                        {
                            checkBox1.Checked = true;
                        }
                        else
                        {                            
                            dateTimePicker4.Value = Convert.ToDateTime(ini_invdate);                           
                        }

                        if (ini_duedate == "1/1/0001 12:00:00 AM")
                        {
                            checkBox9.Checked = true;
                        }
                        else
                        {
                            dateTimePicker1.Value = Convert.ToDateTime(ini_duedate);
                        }

                        checkBox10.Checked = true;
                        checkBox12.Checked = true;
                        checkBox23.Checked = true;
                    }
                //}
            }

            if (info.client_code == "RR")
            {
                info.VendorAssignedStatus = "New";
            }

            dgv_usp.Enabled = true;
        }

        private void button_noMatchUSP_Click(object sender, EventArgs e)
        {
            //Form vendorIdRequestForm = new Forms.VendorIdRequest(this, "CREATE NEW");
            //vendorIdRequestForm.Show();

            dgv_usp.Visible = true;

            dgv_usp.Rows.Clear();

            Form vendorIdRequestForm = new VendorIDRequestForm.Form1("CREATE NEW", "", "EMS", info.Invoice, info.vendor_group);

            vendorIdRequestForm.ShowDialog();

            info.Complete = 21;
            info.PendingReasons = "Vendor ID Request";

            ResetBillingInformation();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (panel_specialInstructions.Visible == true)
            {
                panel_specialInstructions.Hide();
            }
            else
            {
                panel_specialInstructions.Show();
            }
        }

        private void InputAuditData(string result)
        {
            SqlConnection sqlcon = new SqlConnection(Constants.connectionString);

            try
            {
                sqlcon.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save data. Error: {0}", ex.Message);

                sqlcon.Close();
                return;
            }


            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_Audit]
                                                   ([em_task_id]
                                                   ,[result]
                                                   ,[auditor]
                                                   ,[timestamp])
                                             VALUES
                                                   ({0}
                                                   ,'{1}'
                                                   ,'{2}'
                                                   ,CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,GETUTCDATE()),'+08:00')))",
                                                   info.id,
                                                   result,
                                                   Environment.UserName.ToLower());

            SqlCommand sqlcom = new SqlCommand(query, sqlcon);

            try
            {
                sqlcom.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save audit remark. Error: " + ex.Message);

                sqlcon.Close();

                return;
            }
        }

        private void btn_specialInstructions_Click(object sender, EventArgs e)
        {
            string instruction = string.Empty;

            if (checkBox25.Checked == true)
            {
                info.SpecialInstruction = string.Empty;
                InputAuditData("Incorrectly tagged as for CallOut");
                checkBox25.Checked = false;
                panel_specialInstructions.Visible = false;
            }
            else
            {

                if (textBox5.Text.ToString().Trim() != string.Empty)
                {
                    if (textBox13.Text.ToString().Trim() != string.Empty)
                    {
                        instruction += "Spoke with " + textBox5.Text;

                        instruction += " of " + textBox13.Text;                        
                    }
                    else
                    {
                        instruction += "Spoke with " + textBox5.Text;
                    }

                    instruction += " Confirmed the following: ";
                }
                else
                {
                    MessageBox.Show("USP Rep is a mandatory field.");
                    textBox5.Focus();
                    return;
                }

                

                if (info.PendingReasons == "No Account Number" && string.IsNullOrEmpty(textBox_accountNumber.Text.Trim()))
                {
                    MessageBox.Show("Please fill up the account number");
                    textBox_accountNumber.Focus();

                    return;
                }
                else
                {
                    if (!string.IsNullOrEmpty(textBox_accountNumber.Text.Trim()))
                    {
                        instruction += " Account Number is " + textBox_accountNumber.Text.Trim() + "; ";

                        //goto EndCallOut;
                    }                    
                }


                if (info.PendingReasons == "No Amount" && string.IsNullOrEmpty(textBox_amount.Text.Trim()))
                {
                    MessageBox.Show("Please fill up the amount");
                    textBox_amount.Focus();
                    return;
                }
                else
                {
                    if (!string.IsNullOrEmpty(textBox_amount.Text.Trim()))
                    {
                        instruction += " Amount is " + string.Format("{0:N2}", Convert.ToDouble(textBox_amount.Text) + "; ");

                        //goto EndCallOut;
                    }  
                    
                }



                if (info.PendingReasons == "No Property/Service Address" &&
                    (string.IsNullOrEmpty(textBox30.Text) ||
                    string.IsNullOrEmpty(textBox3.Text) ||
                    string.IsNullOrEmpty(textBox21.Text) ||
                    string.IsNullOrEmpty(textBox24.Text) ||
                    string.IsNullOrEmpty(textBox27.Text)))
                {
                    MessageBox.Show("Please fill up the property service address.");
                    textBox30.Focus();
                    return;
                }
                else
                {
                    if (!string.IsNullOrEmpty(textBox30.Text) ||
                    !string.IsNullOrEmpty(textBox3.Text) ||
                    !string.IsNullOrEmpty(textBox21.Text) ||
                    !string.IsNullOrEmpty(textBox24.Text) ||
                    !string.IsNullOrEmpty(textBox27.Text))
                    {
                        instruction += string.Format(" Property Service Address is {0}, {1}, {2}, {3}, {4}. ; ",
                                                    textBox30.Text, textBox3.Text, textBox21.Text, textBox24.Text, textBox27.Text);

                        //goto EndCallOut;
                    }                    
                }

                      

                if (info.PendingReasons == "No USP Address on Invoice" &&
                    (string.IsNullOrEmpty(textBox31.Text) ||
                    string.IsNullOrEmpty(textBox18.Text) ||
                    string.IsNullOrEmpty(textBox22.Text) ||
                    string.IsNullOrEmpty(textBox25.Text) ||
                    string.IsNullOrEmpty(textBox28.Text)))
                {
                    MessageBox.Show("Please fill up the USP address on invoice");
                    textBox31.Focus();
                    return;
                }
                else
                {
                    if (!string.IsNullOrEmpty(textBox31.Text) ||
                        !string.IsNullOrEmpty(textBox18.Text) ||
                        !string.IsNullOrEmpty(textBox22.Text) ||
                        !string.IsNullOrEmpty(textBox25.Text) ||
                        !string.IsNullOrEmpty(textBox28.Text))
                    {
                        instruction += string.Format(" USP Address on Invoice is {0}, {1}, {2}, {3}, {4}. ; ",
                                    textBox31.Text, textBox18.Text, textBox22.Text, textBox25.Text, textBox28.Text);

                        //goto EndCallOut;
                    }                   
                }
                        

                //EndCallOut:

                if (checkBox21.Checked)
                {
                    if (!string.IsNullOrEmpty(textBox32.Text) &&
                        !string.IsNullOrEmpty(textBox20.Text) &&
                        !string.IsNullOrEmpty(textBox23.Text) &&
                        !string.IsNullOrEmpty(textBox26.Text) &&
                        !string.IsNullOrEmpty(textBox29.Text))
                    {
                        instruction += string.Format(" USP Physical Address is {0}, {1}, {2}, {3}, {4}. ;",
                        textBox32.Text, textBox20.Text, textBox23.Text, textBox26.Text, textBox29.Text);
                    }
                    else
                    {
                        MessageBox.Show("Fill up the USP physical address");
                    }
                }
                
                info.SpecialInstruction = instruction;
                
                panel_specialInstructions.Visible = false;

                MessageBox.Show("Process the invoice with gathered information from Call-Out");
            }
        }


        private void triggerSearch(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                button7_Click(null, null);
            }
        }

        private void checkBox15_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox15.Checked)
            {
                checkBox16.Checked = false;
                hasMultiplePropertyOnInvoice = true;
            }
        }

        private void checkBox16_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox16.Checked)
            {
                checkBox15.Checked = false;
                hasMultiplePropertyOnInvoice = false;    
            }
        }

        private void testToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isTestingOnly = true;
            testToolStripMenuItem.Checked = true;
            normalToolStripMenuItem.Checked = false;
        }

        private void normalToolStripMenuItem_Click(object sender, EventArgs e)
        {
            isTestingOnly = false;
            testToolStripMenuItem.Checked = false;
            normalToolStripMenuItem.Checked = true;
        }

        private void textBox_amount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void checkBox17_CheckedChanged(object sender, EventArgs e)
        {
            textBox_accountNumber.Enabled = checkBox17.Checked;
        }

        private void checkBox18_CheckedChanged(object sender, EventArgs e)
        {
            textBox_amount.Enabled = checkBox18.Checked;
        }

        private void checkBox19_CheckedChanged(object sender, EventArgs e)
        {
            textBox30.Enabled = checkBox19.Checked;
            textBox3.Enabled = checkBox19.Checked;
            textBox21.Enabled = checkBox19.Checked;
            textBox27.Enabled = checkBox19.Checked;
            textBox24.Enabled = checkBox19.Checked;
        }

        private void checkBox20_CheckedChanged(object sender, EventArgs e)
        {
            textBox31.Enabled = checkBox20.Checked;
            textBox18.Enabled = checkBox20.Checked;
            textBox22.Enabled = checkBox20.Checked;
            textBox28.Enabled = checkBox20.Checked;
            textBox25.Enabled = checkBox20.Checked;
        }

        private void checkBox21_CheckedChanged(object sender, EventArgs e)
        {
            textBox32.Enabled = checkBox21.Checked;
            textBox20.Enabled = checkBox21.Checked;
            textBox23.Enabled = checkBox21.Checked;
            textBox29.Enabled = checkBox21.Checked;
            textBox26.Enabled = checkBox21.Checked;
        }

        private void textBox27_TextChanged(object sender, EventArgs e)
        {
            if (textBox27.TextLength > 3)
            {
                BackgroundWorker bwPropertyServiceAddress = new BackgroundWorker();
                bwPropertyServiceAddress.DoWork += new DoWorkEventHandler(bwPropertyServiceAddress_DoWork);
                bwPropertyServiceAddress.RunWorkerAsync(textBox27.Text);
            }

            textBox1.Text = textBox27.Text;
        }

        void bwPropertyServiceAddress_DoWork(object sender, DoWorkEventArgs e)
        {
            Database.DBConnection.DatabaseZipcodeStateCity conn = new Database.DBConnection.DatabaseZipcodeStateCity();
            List<Database.DB.ZipcodeStateCity> data = new List<Database.DB.ZipcodeStateCity>();
            data = conn.GetCityStates(e.Argument.ToString());

            if (data != null)
            {
                textBox24.Invoke((Action)delegate
                {
                    textBox24.Text = data[0].abbrev_state;
                });

                textBox21.Invoke((Action)delegate
                {
                    textBox21.Enabled = true;

                    foreach (var info in data)
                    {
                        textBox21.AutoCompleteCustomSource.Add(info.city_name);
                        textBox21.Text = info.city_name.ToUpper();
                    }
                });
            }
        }

        private void textBox28_TextChanged(object sender, EventArgs e)
        {
            if (textBox28.TextLength > 3)
            {
                BackgroundWorker bwUspAddress = new BackgroundWorker();
                bwUspAddress.DoWork += new DoWorkEventHandler(bwUspAddress_DoWork);
                bwUspAddress.RunWorkerAsync(textBox28.Text);
            }
        }

        void bwUspAddress_DoWork(object sender, DoWorkEventArgs e)
        {
            Database.DBConnection.DatabaseZipcodeStateCity conn = new Database.DBConnection.DatabaseZipcodeStateCity();
            List<Database.DB.ZipcodeStateCity> data = new List<Database.DB.ZipcodeStateCity>();
            data = conn.GetCityStates(e.Argument.ToString());

            if (data != null)
            {
                textBox25.Invoke((Action)delegate
                {
                    textBox25.Text = data[0].abbrev_state;
                });

                textBox22.Invoke((Action)delegate
                {
                    textBox22.Enabled = true;

                    foreach (var info in data)
                    {
                        textBox22.AutoCompleteCustomSource.Add(info.city_name);
                        textBox22.Text = info.city_name.ToUpper();
                    }
                });
            }
        }

        private void textBox29_TextChanged(object sender, EventArgs e)
        {
            if (textBox29.TextLength > 3)
            {
                BackgroundWorker bwUspPhysicalAddress = new BackgroundWorker();
                bwUspPhysicalAddress.DoWork += new DoWorkEventHandler(bwUspPhysicalAddress_DoWork);
                bwUspPhysicalAddress.RunWorkerAsync(textBox29.Text);
            }
        }

        void bwUspPhysicalAddress_DoWork(object sender, DoWorkEventArgs e)
        {
            Database.DBConnection.DatabaseZipcodeStateCity conn = new Database.DBConnection.DatabaseZipcodeStateCity();
            List<Database.DB.ZipcodeStateCity> data = new List<Database.DB.ZipcodeStateCity>();
            data = conn.GetCityStates(e.Argument.ToString());

            if (data != null)
            {
                textBox26.Invoke((Action)delegate
                {
                    textBox26.Text = data[0].abbrev_state;
                });

                textBox23.Invoke((Action)delegate
                {
                    textBox23.Enabled = true;

                    foreach (var info in data)
                    {
                        textBox23.AutoCompleteCustomSource.Add(info.city_name);
                        textBox23.Text = info.city_name.ToUpper();
                    }
                });
            }
        }

        private void textBox30_TextChanged(object sender, EventArgs e)
        {
            textBox4.Text = textBox30.Text;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            textBox14.Text = textBox3.Text;
        }

        private void textBox21_TextChanged(object sender, EventArgs e)
        {
            textBox16.Text = textBox21.Text;
        }

        private void textBox24_TextChanged(object sender, EventArgs e)
        {
            textBox17.Text = textBox24.Text;
        }

        private void duplicateApprovalFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form duplicatesForm = new Forms.Administrator.DuplicatesApproval("Pending Duplicate");
            duplicatesForm.Show();
        }

        private void unreadableInvoiceApprovalFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form unreadableInvoiceForm = new Forms.Supervisor.UnreadableInvoiceApprovalForm();
            unreadableInvoiceForm.Show();
        }

        private void textBox12_TextChanged(object sender, EventArgs e)
        {
            // Update 6/16/2015: Bug on amount after due fixed
            //info.AmountAfterDue = Convert.ToDecimal(textBox11.Text);
            //info.Amount = textBox12.Text;

            if (textBox12.Text == string.Empty)
            {
                info.AmountAfterDue = 0;
            }
            else
            {
                info.AmountAfterDue = Convert.ToDecimal(textBox12.Text);
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {                     
            
            if (checkBox1.Checked == true)
            {

                if (dateTimePicker4.Value.Date == DateTime.Today.AddDays(-1) && info.PendingReasons == "Vendor ID Request")
                {
                    //dgv_usp.Rows.Clear();

                    GetVIDReqDtls();
                }

                info.invoice_date = DateTime.MinValue;
                dateTimePicker4.Enabled = false;
                dateTimePicker1.Visible = true;
                checkBox9.Visible = true;
                label2.Visible = true;
                pictureBox7.Visible = true;
            }
            else
            {
                dateTimePicker4.Enabled = true;
                dateTimePicker1.Visible = false;
                checkBox9.Visible = false;
                label2.Visible = false;
                pictureBox7.Visible = false;
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
           
            var uploadfile = openFileDialog1.ShowDialog();
            if (uploadfile == DialogResult.OK)
            {
                if (!System.IO.File.Exists(openFileDialog1.FileName))
                {
                    MessageBox.Show("Unable to locate file. Please select again.");
                    return;
                }

                if (System.IO.Path.GetFileNameWithoutExtension(openFileDialog1.FileName).Length > 150)
                {
                    MessageBox.Show("File name is too long. Please rename file.");
                    return;
                }

                try
                {
                    
                    string newfilename = string.Format("{0}{1}.pdf",
                        DateTime.Now.ToString("yyyyMMddHHmmss"),
                        queue_code == 70 ? "Escalation" : "InvoiceUpload");
                    string newfullfilename = System.IO.Path.Combine(Constants.SourceFolder, newfilename);

                    try
                    {
                        System.IO.File.Copy(openFileDialog1.FileName, newfullfilename);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        return;
                    }

                    Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
                    Database.DB.EM_Tasks em_data = new Database.DB.EM_Tasks();


                    em_data.Invoice = newfilename.Replace(".pdf","");
                    em_data.InvoiceFolderName = newfullfilename;
                    em_data.userid = Environment.UserName;
                    em_data.lockedto = Environment.UserName;
                    em_data.isLatestData = true;
                    em_data.Complete = 20;  // new code for uploaded invoice
                    em_data.PendingReasons = "New " + (queue_code == 70 ? "Escalation" : "Invoice Upload").ToString();

                    conn.WriteSingleData(em_data);

                    pdfviewer.Navigate(openFileDialog1.FileName);

                    CurrentLoadedInvoice = newfullfilename;

                    LoadDataEMTask(em_data.Invoice);

                    groupBox6.Visible = true;

                    button8.Enabled = false;
                }
                catch ( Exception ex )
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void waitLoadPDF()
        {
            while (isWebControlDocumentCompleted != true)
            {
                Application.DoEvents();
            }

            isWebControlDocumentCompleted = false;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            info.HouseNoEntered = textBox4.Text;
        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {
            info.StreetEntered = textBox14.Text;
        }

        private void textBox16_TextChanged(object sender, EventArgs e)
        {
            info.CityEntered = textBox16.Text;
        }

        private void textBox17_TextChanged(object sender, EventArgs e)
        {
            info.StateEntered = textBox17.Text;
        }

        private void button12_Click(object sender, EventArgs e)
        {
            panel_specialinstructions2.Visible = !panel_specialinstructions2.Visible;
        }

        private void button10_Click_1(object sender, EventArgs e)
        {
            var result = openFileDialog1.ShowDialog();

            if (result == DialogResult.OK)
            {
                string filename = openFileDialog1.FileName;

                if (System.IO.File.Exists(filename))
                {
                    string newAttachmentFolder = AttachmentsFolder + DateTime.Now.ToString("MM-dd-yyyy") + @"\";

                    if (!System.IO.Directory.Exists(newAttachmentFolder))
                    {
                        try
                        {
                            System.IO.Directory.CreateDirectory(newAttachmentFolder);
                        }
                        catch
                        {
                            newAttachmentFolder = AttachmentsFolder;
                        }
                    }

                    string filepath = Path.Combine(newAttachmentFolder, System.IO.Path.GetFileName(filename));

                    try
                    {
                        try
                        {
                            System.IO.File.Copy(filename, filepath);
                        }
                        catch
                        {

                        }

                        info.AttachmentFile = filepath;

                        MessageBox.Show("File has been uploaded! Path: " + filepath);
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Unable to upload file. Error: " + ex.Message);
                    }
                }
                else
                {
                    MessageBox.Show("Please select again file.");
                }
            }
        }

        private void button13_Click(object sender, EventArgs e)
        {
            info.SpecialInstruction = textBox33.Text;
            textBox3.Tag = info.SpecialInstruction;

            MessageBox.Show("Special instruction has been saved!");

            panel_specialinstructions2.Visible = false;
        }

        //----------------------------------------------------------------------------------------------
        // This function checks if there is an exact duplicate on list of Manual WI for the last 90 days
        // and tag it as pending
        //----------------------------------------------------------------------------------------------
        public void CheckIfExactDuplicate(string property_code, string vendor_group, string servFrom, string servTo, string client_code, string amount, string account_number, string invoice)
        {
            if (property_code != null && 
                client_code != null && 
                amount != null && 
                account_number != null)
            {
                if (property_code != string.Empty &&
                    client_code != string.Empty &&
                    amount != string.Empty &&
                    Convert.ToDecimal(amount) != 0 &&
                    account_number != string.Empty)
                {
                    Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
                    Database.DBConnection.DatabaseWorkItemsLast30Days wiConn = new Database.DBConnection.DatabaseWorkItemsLast30Days();
                    Database.DB.WorkItemsLast30Days wiData = wiConn.GetDataProcessor(property_code, client_code, Convert.ToDecimal(amount), vendor_group);
                    Database.DB.EM_Tasks EM_duplicate = conn.SearchForDuplicates(property_code,vendor_group, servFrom, servTo, account_number, amount, DateTime.Now.AddMonths(-2), invoice, 0);

                    if (EM_duplicate != null)
                    {
                        if (System.IO.File.Exists(EM_duplicate.InvoiceFolderName))
                        {
                            info.PendingReasons = "Pending Duplicate";
                            info.remarks = EM_duplicate.InvoiceFolderName;
                        }
                    }
                }
            }
        }

        //---------------------------------------------------------------------------------------------------
        // This function checks if the invoice came from Call-Out queue and tags it as "Manual WI - Callout")
        //---------------------------------------------------------------------------------------------------
        private void CheckIfFromCallout(int complete, string invoice)
        {
            if (complete == 1)
            {
                Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
                Database.DB.EM_Tasks latestData = new Database.DB.EM_Tasks();
                latestData = conn.GetLatestInvoiceData(invoice);

                if (latestData != null)
                {
                    if (latestData.Complete == 0)
                    {
                        info.PendingReasons = "Manual WI - Callout";
                    }
                }
            }
        }

        private void CheckIfPartialDuplicate(string property_code, string vendor_id, decimal previousbalance, string amount, string vendor_group)
        {
            if (property_code != string.Empty &&
                vendor_id != string.Empty &&
                previousbalance != 0)
            {
                Database.DBConnection.DatabaseWorkItemsLast30Days previousWIConn = new Database.DBConnection.DatabaseWorkItemsLast30Days();
                Database.DB.WorkItemsLast30Days previousWIData = new Database.DB.WorkItemsLast30Days();

                previousWIData = previousWIConn.GetDataProcessor(property_code, vendor_id, previousbalance, vendor_group);

                if (previousWIData != null)
                {

                    info.PreviousAmtPaid = previousWIData.Amount;
                    info.Amount = (Convert.ToDecimal(amount) - previousbalance).ToString();

                    if (info.AmountAfterDue != 0)
                    {
                        info.AmountAfterDue = info.AmountAfterDue - previousbalance;
                    }

                    if (info.SpecialInstruction != string.Empty)
                    {
                        info.SpecialInstruction += string.Format(" Previous balance {0:0,0.##} already paid in {1}",
                        previousWIData.Amount, previousWIData.work_order_number);
                    }
                    else
                    {
                        info.SpecialInstruction = string.Format("Previous balance {0:0,0.##} already paid in {1}",
                            previousWIData.Amount, previousWIData.work_order_number);
                    }

                    //info.Complete = 1;
                    //info.PendingReasons = "Manual WI";
                    //info.InvoiceFolderName = strCreateInvoiceFolderName(CompleteFolder);
                }
            }
        }

        private void dateTimePicker5_ValueChanged(object sender, EventArgs e)
        {
            info.DisconnectionDate = dateTimePicker5.Value.Date;
        }

        private void checkBox22_CheckedChanged(object sender, EventArgs e)
        {
            dateTimePicker5.Enabled = checkBox22.Checked;
            dateTimePicker5.Value = DateTime.Today;
            info.DisconnectionDate = DateTime.MinValue;
        }

        private void comboBox5_TextChanged(object sender, EventArgs e)
        {

            if (comboBox5.Text.ToString().ToLower().Replace("city","").Replace("of", "").Length > 2)
            {

                btnSearchUSP_Click(null, null);

                //dgv_usp.Invoke((Action)delegate
                //{
                //    dgv_usp.Visible = true;
                //    dgv_usp.Rows.Clear();
                //});

                //List<Database.DB.VDR> listOfSearchedVdr = new List<Database.DB.VDR>();

                //try
                //{
                //    //listOfSearchedVdr.AddRange(listOfVDR.Where(p => p.Vendor_Name.ToLower().Contains(comboBox5.Text.ToLower())));

                //    listOfSearchedVdr.AddRange(listOfVDR.Where(p => p.Vendor_Name.ToLower().Contains(comboBox5.Text.ToLower())));

                //    dgv_usp.Rows.Clear();

                //    if (listOfSearchedVdr.Count() > 0)
                //    {
                //        foreach (Database.DB.VDR v in listOfSearchedVdr)
                //        {
                //            dgv_usp.Rows.Add(v.Vendor_ID, v.Vendor_Name, v.Address + ", " + v.City + ", " + v.State + ", " + v.Zipcode, v.vdr_status == 1 ? "Active" : "Inactive");
                //        }
                //    }
                //}
                //catch
                //{

                //}
            }
        }

        private void CheckIfAccountNumberHasLetters()
        {
            if (info.AccountNumber != null)
            {
                if (!string.IsNullOrEmpty(info.AccountNumber) &&
                    info.Complete == 1 &&
                    info.PendingReasons == "Bulk Upload")
                {
                    foreach (char c in info.AccountNumber)
                    {
                        if (!char.IsLetter(c))
                        {
                            info.Complete = 1;
                            info.PendingReasons = "Bulk Upload";
                        }
                    }
                }
            }
        }

        private void CheckIfNotUtilityVendor()
        {
            if (info.PendingReasons != null && info.vendor_group != null)
            { 
                if (info.Complete == 1 &&
                    info.PendingReasons == "Bulk Upload" &&
                    (info.vendor_group == "T" || info.vendor_group == "D" || info.vendor_group == "S"))
                {
                    info.PendingReasons = "Manual WI";
                }
            }
        }

        private void textBox_accountNumber_TextChanged(object sender, EventArgs e)
        {
            textBox10.Text = textBox_accountNumber.Text;
        }

        private void textBox_amount_TextChanged(object sender, EventArgs e)
        {
            textBox11.Text = textBox_amount.Text;
        }

        private void funnelReportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form funnelreport = new UC.Forms.Reports.FunnelReport();
            //Form funnelreport = new UC.Forms.Reports.frmNewFunnel();
            funnelreport.Show();
        }

        private void textBox24_Leave(object sender, EventArgs e)
        {
            if (textBox24.Text != string.Empty)
            {
                textBox24.Text = textBox24.Text.ToUpper();
            }
        }

        private void textBox25_Leave(object sender, EventArgs e)
        {
            if (textBox25.Text != string.Empty)
            {
                textBox25.Text = textBox25.Text.ToUpper();
            }
        }

        private void textBox26_Leave(object sender, EventArgs e)
        {
            if (textBox26.Text != string.Empty)
            {
                textBox26.Text = textBox26.Text.ToUpper();
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            info.SpecialInstruction = string.Empty;

            textBox5.Text = string.Empty;

            foreach (Control c in groupBox3.Controls)
            {
                try
                {
                    if (c.GetType() == typeof(TextBox))
                    {
                        ((TextBox)c).Text = string.Empty;
                    }

                    if (c.GetType() == typeof(CheckBox))
                    {
                        ((CheckBox)c).Checked = false;
                    }
                }
                catch
                { 
                
                }
            }
        }

        private void forApprovalAmount1000ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form approvalForm = new Forms.Supervisor.ApprovalForm1000();
            approvalForm.Show();
        }

        private void checkBox23_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox23.Checked)
            {
                checkBox24.Checked = false;
                info.final_bill = "Yes";

                label55.Visible = true;
                //textBox35.Visible = true;
            }
            else
            {
                info.final_bill = "No";

                label55.Visible = false;
                //textBox35.Visible = false;
                textBox35.Text = string.Empty;
            }
        }

        private void checkBox24_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox24.Checked)
            {
                checkBox23.Checked = false;
                info.final_bill = "No";

                label55.Visible = false;
                //textBox35.Visible = false;
                textBox35.Text = string.Empty;
            }
            else
            {
                info.final_bill = "Yes";

                //label55.Visible = true;
                //textBox35.Visible = true;
            }
        }

        private void ComputeAmount(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;

            string value = tb.Text;
            char[] delimiters = new char[] { '+', '-' };
            string[] parts = value.Split(delimiters);
            List<string> listOfsigns = new List<string>();

            if (value.ToString() == "")
            {
                value = "0";
            }

            foreach (char ch in value)
            {
                if (ch == '+' || ch == '-')
                {
                    listOfsigns.Add(ch.ToString());
                }
            }

            string[] signs = listOfsigns.ToArray();

            try
            {
                decimal amount = 0;

                if (parts.Count() > 1)
                {
                    amount = Convert.ToDecimal(parts[0]);
                }

                if (parts.Count() > 1)
                {
                    for (int i = 1; i < parts.Count(); i++)
                    {
                        switch (signs[i - 1])
                        {
                            case "+":
                                amount += Convert.ToDecimal(parts[i]);
                                break;

                            case "-":
                                amount -= Convert.ToDecimal(parts[i]);
                                break;
                        }
                    }
                    tb.Text = string.Format("{0:N2}", amount);
                }
            }
            catch
            {
                MessageBox.Show(string.Format("{0} is in incorrect format", tb.Text));
                tb.Text = string.Empty;
            }
        }

        private void numbersonly(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || char.IsDigit(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox35_TextChanged(object sender, EventArgs e)
        {
            try
            {
                depositAmount = Convert.ToDouble(info.Deposit);
            }
            catch
            {
                depositAmount = 0;
            }
        }

        private void incompleteVIDRequestsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form auditVID = new Forms.Supervisor.IncompleteVIDRequests();
            auditVID.Show();
        }

        private void invoiceTimelineToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form invoicetimeline = new Forms.Reports.InvoiceTimeline();
            invoicetimeline.Show();
        }

        private void partialDuplicatesReportRESIToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form partialduplicatesform = new Forms.Reports.PartialDuplicates();
            partialduplicatesform.Show();
        }

        private void auditDashboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form dashboard_audit = new Forms.Reports.Dashboard_Audit();
            dashboard_audit.Show();
        }

        private void qADashboardToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form dashboard_qa = new Forms.Reports.Dashboard_QA();
            dashboard_qa.Show();
        }

        private void triggerStateSearch(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                button7_Click(null, null);
            }
        }

        private void functionsToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void textBox16_PreviewKeyDown(object sender, PreviewKeyDownEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && e.Modifiers == Keys.None)
            {
                button7_Click(null, null);
            }
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                TextBox tb = sender as TextBox;

                string value = tb.Text;
                char[] delimiters = new char[] { '+', '-' };
                string[] parts = value.Split(delimiters);
                List<string> listOfsigns = new List<string>();

                if (value.ToString() == "")
                {
                    value = "0";
                }

                foreach (char ch in value)
                {
                    if (ch == '+' || ch == '-')
                    {
                        listOfsigns.Add(ch.ToString());
                    }
                }

                string[] signs = listOfsigns.ToArray();

                try
                {
                    decimal amount = Convert.ToDecimal(parts[0]);

                    if (parts.Count() > 1)
                    {
                        for (int i = 1; i < parts.Count(); i++)
                        {
                            switch (signs[i - 1])
                            {
                                case "+":
                                    amount += Convert.ToDecimal(parts[i]);
                                    break;

                                case "-":
                                    amount -= Convert.ToDecimal(parts[i]);
                                    break;
                            }
                        }
                        tb.Text = string.Format("{0:N2}", amount);
                    }
                }
                catch
                {
                    MessageBox.Show(string.Format("{0} is in incorrect format", tb.Text));
                    tb.Text = string.Empty;
                }
            }
        }

        private void textBox8_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                TextBox tb = sender as TextBox;

                string value = tb.Text;
                char[] delimiters = new char[] { '+', '-' };
                string[] parts = value.Split(delimiters);
                List<string> listOfsigns = new List<string>();

                if (value.ToString() == "")
                {
                    value = "0";
                }

                foreach (char ch in value)
                {
                    if (ch == '+' || ch == '-')
                    {
                        listOfsigns.Add(ch.ToString());
                    }
                }

                string[] signs = listOfsigns.ToArray();

                try
                {
                    decimal amount = Convert.ToDecimal(parts[0]);

                    if (parts.Count() > 1)
                    {
                        for (int i = 1; i < parts.Count(); i++)
                        {
                            switch (signs[i - 1])
                            {
                                case "+":
                                    amount += Convert.ToDecimal(parts[i]);
                                    break;

                                case "-":
                                    amount -= Convert.ToDecimal(parts[i]);
                                    break;
                            }
                        }
                        tb.Text = string.Format("{0:N2}", amount);
                    }
                }
                catch
                {
                    MessageBox.Show(string.Format("{0} is in incorrect format", tb.Text));
                    tb.Text = string.Empty;
                }
            }
        }

        private void pdfviewer_DocumentCompleted_1(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void chkTransAmtYes_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTransAmtYes.Checked)
            {

                isBalTransfer = true;
                chkTransAmtNo.Checked = false;
                chkTransAmtYes.Checked = true;
                info.HasTransferAmount = "Yes";
                label59.Visible = true;
                numBalTransfer.Visible = true;
                numBalTransfer.Enabled = true;
                label47.Visible = false;
                //checkBox15.Checked = true;
                checkBox15.Visible = false;
                checkBox16.Visible = false;
                checkBox16.Checked = false;

                // Show Account Number details
                label14.Visible = false;
                textBox10.Visible = false;
                pictureBox5.Visible = false;
                checkBox6.Visible = false;
                button7.Visible = false;
                button9.Visible = false;
            }
            //else
            //{
               
            //    chkTransAmtYes.Checked = false;
            //    chkTransAmtNo.Checked = true;
            //    info.HasTransferAmount = "No";
            //    label59.Visible = false;
            //    numBalTransfer.Visible = false;
            //    numBalTransfer.Value = 0;
            //    label47.Visible = true;
            //    checkBox15.Checked = false;
            //    checkBox15.Visible = true;
            //    checkBox16.Visible = true;

            //    // Show Account Number details
            //    label14.Visible = true;
            //    textBox10.Visible = true;
            //    pictureBox5.Visible = true;
            //    checkBox6.Visible = true;
            //    button7.Visible = true;
            //    button9.Visible = true;
            //}
        }

        private void chkTransAmtNo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTransAmtNo.Checked)
            {

                isBalTransfer = false;

                chkTransAmtYes.Checked = false;
                chkTransAmtNo.Checked = true;
                info.HasTransferAmount = "No";
                label59.Visible = false;
                numBalTransfer.Visible = false;
                numBalTransfer.Value = 0;
                numBalTransfer.Enabled = true;
                lblCount.Visible = false;
                lblOf.Visible = false;
                txtTotal.Visible = false;
                txtTotal.Text = "0";


                label62.Visible = true;
                chkBalInfoYes.Visible = true;
                chkBalInfoYes.Checked = false;
                chkBalInfoYes.Enabled = false;
                chkBalInfoNo.Visible = true;
                chkBalInfoNo.Checked = true;
                chkBalInfoNo.Enabled = false;

                label63.Visible = true;
                chkBalAmtYes.Visible = true;
                chkBalAmtYes.Checked = false;
                chkBalAmtYes.Enabled = false;
                chkBalAmtNo.Visible = true;
                chkBalAmtNo.Checked = true;
                chkBalAmtNo.Enabled = false;

                label47.Visible = true;
                checkBox15.Visible = true;
                checkBox16.Visible = true;
                checkBox15.Checked = false;
                checkBox16.Checked = true;

                // Show Account Number details
                label14.Visible = true;
                textBox10.Visible = true;
                pictureBox5.Visible = true;
                checkBox6.Visible = true;
                button7.Visible = true;
                button9.Visible = true;
            }
            else
            {
                chkTransAmtYes.Checked = true;
                chkTransAmtNo.Checked = false;
                info.HasTransferAmount = "Yes";
                label59.Visible = true;
                numBalTransfer.Visible = true;
                numBalTransfer.Value = 0;

                label62.Visible = false;
                chkBalInfoYes.Visible = false;
                chkBalInfoYes.Checked = false;
                chkBalInfoYes.Enabled = true;
                chkBalInfoNo.Visible = false;
                chkBalInfoNo.Checked = false;
                chkBalInfoNo.Enabled = true;

                label63.Visible = false;
                chkBalAmtYes.Visible = false;
                chkBalAmtYes.Checked = false;
                chkBalAmtYes.Enabled = true;
                chkBalAmtNo.Visible = false;
                chkBalAmtNo.Checked = false;
                chkBalAmtNo.Enabled = true;

                label47.Visible = false;
                checkBox15.Visible = false;
                checkBox16.Visible = false;

                // Show Account Number details
                label14.Visible = false;
                textBox10.Visible = false;
                pictureBox5.Visible = false;
                checkBox6.Visible = false;
                button7.Visible = false;
                button9.Visible = false;
            }
          
        }

        private void chkASFIYes_CheckedChanged(object sender, EventArgs e)
        {

            if (chkASFIYes.Checked == true)
            {
                chkASFINo.Checked = false;
                cmbBillName.Visible = false;
                label61.Visible = false;

                label64.Visible = true;
                label65.Visible = true;
                chknYes.Visible = true;
                chknYes.Checked = true;
                chknYes.Enabled = false;
                chknNo.Visible = true;
                chknNo.Enabled = false;
                label58.Visible = true;
                chkTransAmtYes.Visible = true;
                chkTransAmtNo.Visible = true;


                lblAdrsNotif.Visible = false;
                btnCancel.Visible = false;

                if (!checkBox15.Checked)
                {
                    checkBox16.Checked = true;
                }


                info.isAddressedToASFI = true;
            }
            else
            {
                LoadBillName();

                chkASFIYes.Checked = false;
                cmbBillName.Visible = false;
                label61.Visible = false;

                label64.Visible = true;
                label65.Visible = true;
                chknYes.Visible = true;
                chknNo.Visible = true;
                chknYes.Checked = false;
                chknNo.Checked = false;
                chknYes.Enabled = true;
                chknNo.Enabled = true;

                label62.Visible = false;
                chkBalInfoYes.Visible = false;
                chkBalInfoYes.Checked = false;
                //chkBalInfoYes.Enabled = false;
                chkBalInfoNo.Visible = false;
                chkBalInfoNo.Checked = false;
                //chkBalInfoNo.Enabled = false;

                label63.Visible = false;
                chkBalAmtYes.Visible = false;
                chkBalAmtYes.Checked = false;
                //chkBalAmtYes.Enabled = false;
                chkBalAmtNo.Visible = false;
                chkBalAmtNo.Checked = false;
                //chkBalAmtNo.Enabled = false;

                chkTransAmtNo.Checked = false;
                chkTransAmtYes.Checked = false;

                // Show Account Number details
                label14.Visible = false;
                textBox10.Visible = false;
                checkBox6.Visible = false;
                button7.Visible = false;
                button9.Visible = false;

                label47.Visible = false;
                label58.Visible = false;
                label59.Visible = false;
                numBalTransfer.Visible = false;
                checkBox15.Visible = false;
                checkBox16.Visible = false;
                chkTransAmtYes.Visible = false;
                chkTransAmtNo.Visible = false;
                pictureBox5.Visible = false;
                if (!checkBox15.Checked)
                {
                    checkBox16.Checked = false;
                }

                info.isAddressedToASFI = false;
            }
        }

        private void chkASFINo_CheckedChanged(object sender, EventArgs e)
        {

            if (chkASFINo.Checked == true)
            {

                LoadBillName();

                chkASFIYes.Checked = false;

                label64.Visible = true;
                label65.Visible = true;
                chknYes.Visible = true;
                chknNo.Visible = true;

                cmbBillName.Visible = true;
                label61.Visible = true;

                label62.Visible = false;
                chkBalInfoYes.Visible = false;
                chkBalInfoYes.Checked = false;
                //chkBalInfoYes.Enabled = false;
                chkBalInfoNo.Visible = false;
                chkBalInfoNo.Checked = false;
                //chkBalInfoNo.Enabled = false;

                label63.Visible = false;
                chkBalAmtYes.Visible = false;
                chkBalAmtYes.Checked = false;
                //chkBalAmtYes.Enabled = false;
                chkBalAmtNo.Visible = false;
                chkBalAmtNo.Checked = false;
                //chkBalAmtNo.Enabled = false;

                chkTransAmtNo.Checked = false;
                chkTransAmtYes.Checked = false;

                // Show Account Number details
                label14.Visible = false;
                textBox10.Visible = false;
                checkBox6.Visible = false;
                button7.Visible = false;
                button9.Visible = false;

                label47.Visible = false;
                label58.Visible = false;
                label59.Visible = false;
                numBalTransfer.Visible = false;
                checkBox15.Visible = false;
                checkBox16.Visible = false;
                chkTransAmtYes.Visible = false;
                chkTransAmtNo.Visible = false;
                pictureBox5.Visible = false;
                if (!checkBox15.Checked)
                {
                    checkBox16.Checked = false;
                }

                info.isAddressedToASFI = false;

            }
            else
            {
                chkASFINo.Checked = false;
                cmbBillName.Visible = false;
                label61.Visible = false;

                label64.Visible = false;
                label65.Visible = false;
                chknYes.Visible = false;
                chknNo.Visible = false;

                //// Show Account Number details
                //label14.Visible = true;
                //textBox10.Visible = true;
                //pictureBox5.Visible = true;
                //checkBox6.Visible = true;
                //button7.Visible = true;
                //button9.Visible = true;

                //label47.Visible = true;
                label58.Visible = true;
                //label59.Visible = true;
                //txtTransferCount.Visible = true;
                //checkBox15.Visible = true;
                //checkBox16.Visible = true;
                chkTransAmtYes.Visible = true;
                chkTransAmtNo.Visible = true;


                lblAdrsNotif.Visible = false;
                btnCancel.Visible = false;

                if (!checkBox15.Checked)
                {
                    checkBox16.Checked = true;
                }

                //if (!chkTransAmtYes.Checked)
                //{
                //    chkTransAmtNo.Checked = true;
                //}

                info.isAddressedToASFI = true;
            }
        }

        private void cmbBillName_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbBillName.Text == "---- Not Listed ----")
            {

                if (chkASFINo.Checked == true && chknNo.Checked == true)
                {
                    //MessageBox.Show("Please enter the addressee then click Save!");

                    cmbBillName.Items.Clear();
                    btnAdd.Enabled = false;
                    this.cmbBillName.DropDownStyle = ComboBoxStyle.Simple;
                    //btnAdd.Visible = true;
                    btnCancel.Visible = true;
                    lblAdrsNotif.Visible = true;
                    //button2.Enabled = false;
                    //button3.Enabled = false;

                    label58.Visible = false;
                    chkTransAmtYes.Visible = false;
                    chkTransAmtNo.Visible = false;
                }

                if (chkASFINo.Checked == true)
                {
                    info.isAddressedToASFI = false;
                    info.BillTo = cmbBillName.Text.Trim();
                }
                else
                {
                    info.isAddressedToASFI = true;
                    info.BillTo = string.Empty;
                }
            }
            else
            {
                ////LoadBillName();
                //this.cmbBillName.DropDownStyle = ComboBoxStyle.DropDown;
                //btnAdd.Visible = false;
                //btnCancel.Visible = false;

                if (chkASFINo.Checked == true)
                {
                    info.isAddressedToASFI = false;
                    info.BillTo = cmbBillName.Text.Trim();
                }
                else
                {
                    info.isAddressedToASFI = true;
                    info.BillTo = string.Empty;
                }

                //if (chkASFINo.Checked == true && chknNo.Checked == true)
                //{
                //    button3.Enabled = true;
                //    button2.Enabled = true;

                //    MessageBox.Show("Please click Save!");
                //}
                //else
                //{
                //    button3.Enabled = false;
                //    button2.Enabled = false;
                //}


                label58.Visible = true;
                chkTransAmtYes.Visible = true;
                chkTransAmtNo.Visible = true;

                info.Complete = 21;
                info.PendingReasons = "Pending for Audit";

            }

           
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            SqlConnection sqlcon = new SqlConnection(Constants.connectionString);

            try
            {
                sqlcon.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save data. Error: {0}", ex.Message);

                sqlcon.Close();
                return;
            }


            string query = string.Format(@"insert into tbl_EM_BillName (BillName, CreatedBy, CreatedDatetime)
                                            values ('{0}','{1}',getdate()) ",
                                                   cmbBillName.Text,
                                                   Environment.UserName.ToLower());

            SqlCommand sqlcom = new SqlCommand(query, sqlcon);

            try
            {
                sqlcom.ExecuteNonQuery();

                MessageBox.Show("Please click Save!");

                this.cmbBillName.DropDownStyle = ComboBoxStyle.DropDown;

                LoadBillName();

                btnAdd.Visible = false;
                btnCancel.Visible = false;

                button3.Enabled = true;
                button2.Enabled = true;

                info.isAddressedToASFI = false;
                info.BillTo = cmbBillName.Text.Trim();
                info.Complete = 21;
                info.PendingReasons = "Pending for Audit";
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save the data. Error: " + ex.Message);

                sqlcon.Close();

                return;
            }        
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            lblAdrsNotif.Visible = false;
            button2.Visible = true;
            button3.Visible = true;
            this.cmbBillName.DropDownStyle = ComboBoxStyle.DropDown;
            cmbBillName.Text = string.Empty;
            LoadBillName();
            //btnAdd.Visible = false;
            btnCancel.Visible = false;
            button2.Enabled = false;
            button3.Enabled = false;
            lblAdrsNotif.Visible = true;
        }

        private void cmbBillName_TextChanged(object sender, EventArgs e)
        {
            if (cmbBillName.Text.Trim() != string.Empty)
            {
                //btnAdd.Enabled = true;
                lblAdrsNotif.Visible = false;


                //if (chkASFINo.Checked == true && chknYes.Checked == true)
                //{
                //    button2.Enabled = false;
                //    button3.Enabled = false;
                //}
                //else
                //{
                //    button2.Enabled = true;
                //    button3.Enabled = true;
                //}

                label58.Visible = true;
                chkTransAmtYes.Visible = true;
                chkTransAmtNo.Visible = true;

            }
            else
            {
                //btnAdd.Enabled = false;

                lblAdrsNotif.Visible = true;

                button2.Enabled = false;
                button3.Enabled = false;

                label58.Visible = false;
                chkTransAmtYes.Visible = false;
                chkTransAmtNo.Visible = false;

                if (chkASFINo.Checked == true)
                {
                    info.isAddressedToASFI = false;
                    info.BillTo = cmbBillName.Text.Trim();
                }
                else
                {
                    info.isAddressedToASFI = true;
                    info.BillTo = string.Empty;
                }

            }

            info.Complete = 21;
            info.PendingReasons = "Pending for Audit";
        }

        private void numBalTransfer_ValueChanged(object sender, EventArgs e)
        {
            if (numBalTransfer.Value > 0)
            {
                label62.Visible = true;
                chkBalInfoYes.Visible = true;
                chkBalInfoNo.Visible = true;
            }
            else
            {
                label62.Visible = false;
                chkBalInfoYes.Visible = false;
                chkBalInfoNo.Visible = false;
            }

            label63.Visible = false;
            chkBalAmtYes.Visible = false;
            chkBalAmtNo.Visible = false;
        }

        private void numBalTransfer_KeyUp(object sender, KeyEventArgs e)
        {
            if (numBalTransfer.Value > 0)
            {

                label62.Visible = true;
                chkBalInfoYes.Visible = true;
                chkBalInfoYes.Checked = false;
                chkBalInfoYes.Enabled = true;
                chkBalInfoNo.Visible = true;
                chkBalInfoNo.Checked = false;
                chkBalInfoNo.Enabled = true;

            }
            else
            {

                //MessageBox.Show("Please enter the number of balance transfers.");
                numBalTransfer.Focus();

                label62.Visible = false;
                chkBalInfoYes.Visible = false;
                chkBalInfoYes.Checked = false;
                //chkBalInfoYes.Enabled = false;
                chkBalInfoNo.Visible = false;
                chkBalInfoNo.Checked = false;
                //chkBalInfoNo.Enabled = false;

            }       
            
        }

        private void chkBalInfoYes_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBalInfoYes.Checked == true)
            {
                chkBalInfoNo.Checked = false;

                label63.Visible = true;
                chkBalAmtYes.Visible = true;
                chkBalAmtYes.Checked = false;
                chkBalAmtYes.Enabled = true;
                chkBalAmtNo.Visible = true;
                chkBalAmtNo.Checked = false;
                chkBalAmtNo.Enabled = true;
            }
        }

        private void chkBalInfoNo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBalInfoNo.Checked == true)
            {
                chkBalInfoYes.Checked = false;
                label63.Visible = false;
                chkBalAmtYes.Visible = false;
                chkBalAmtNo.Visible = false;


                label63.Visible = true;
                chkBalAmtYes.Visible = true;
                chkBalAmtYes.Checked = false;
                chkBalAmtYes.Enabled = true;
                chkBalAmtNo.Visible = true;
                chkBalAmtNo.Checked = false;
                chkBalAmtNo.Enabled = true;

                // Show Account Number details
                label14.Visible = false;
                textBox10.Visible = false;
                pictureBox5.Visible = false;
                checkBox6.Visible = false;
                button7.Visible = false;
                button9.Visible = false;

                //MessageBox.Show("Please click Save!");
                //button2.Enabled = true;
                //button3.Enabled = true;
            }
        }

        private void chkBalAmtYes_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBalAmtYes.Checked == true)
            {

                chkBalAmtNo.Checked = false;

                // Show Account Number details
                label14.Visible = true;
                textBox10.Visible = true;
                pictureBox5.Visible = true;
                checkBox6.Visible = true;
                button7.Visible = true;
                button9.Visible = true;
            }
        }

        private void chkBalAmtNo_CheckedChanged(object sender, EventArgs e)
        {
            if (chkBalAmtNo.Checked == true)
            {
                chkBalAmtYes.Checked = false;

                // Show Account Number details
                label14.Visible = true;
                textBox10.Visible = true;
                pictureBox5.Visible = true;
                checkBox6.Visible = true;
                button7.Visible = true;
                button9.Visible = true;
            }
        }

        private void numBalTransfer_ValueChanged_1(object sender, EventArgs e)
        {
            if (numBalTransfer.Value > 0)
            {

                label62.Visible = true;
                chkBalInfoYes.Visible = true;
                chkBalInfoYes.Checked = false;
                chkBalInfoYes.Enabled = true;
                chkBalInfoNo.Visible = true;
                chkBalInfoNo.Checked = false;
                chkBalInfoNo.Enabled = true;

            }
            else
            {

                //MessageBox.Show("Please enter the number of balance transfers.");
                numBalTransfer.Focus();

                label62.Visible = false;
                chkBalInfoYes.Visible = false;
                chkBalInfoYes.Checked = false;
                //chkBalInfoYes.Enabled = false;
                chkBalInfoNo.Visible = false;
                chkBalInfoNo.Checked = false;
                //chkBalInfoNo.Enabled = false;

            }     
        }

        private void counterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form newFunnelForm = new Forms.Reports.frmNewFunnel();
            newFunnelForm.Show();
        }

        private void chknYes_CheckedChanged(object sender, EventArgs e)
        {

            if (chknYes.Checked)
            {

                cmbBillName.Text = "";
                info.isAddressedToASFI = true;
                chknNo.Checked = false;
                chknYes.Checked = true;

                cmbBillName.Visible = false;
                label61.Visible = false;


                label58.Visible = true;
                chkTransAmtYes.Visible = true;
                chkTransAmtNo.Visible = true;

                lblAdrsNotif.Visible = false;
            }
            //else
            //{
            //    info.isAddressedToASFI = false;
            //    chknYes.Checked = false;
            //    chknNo.Checked = true;

            //    cmbBillName.Visible = true;
            //    label61.Visible = true;


            //    label58.Visible = false;
            //    chkTransAmtYes.Visible = false;
            //    chkTransAmtNo.Visible = false;
            //}

            SaveButtonsEnabled(false);
        }

        private void chknNo_CheckedChanged(object sender, EventArgs e)
        {
            if (chknNo.Checked)
            {
                info.isAddressedToASFI = false;
                chknYes.Checked = false;
                chknNo.Checked = true;

                cmbBillName.Visible = true;
                label61.Visible = true;


                label58.Visible = false;
                chkTransAmtYes.Visible = false;
                chkTransAmtNo.Visible = false;

                lbl_UspOnInvoice.Visible = false;
                cmboBx_UspOnInvoice.Visible = false;
            }

            //else
            //{
            //    cmbBillName.Text = "";
            //    info.isAddressedToASFI = true;
            //    chknNo.Checked = false;
            //    chknYes.Checked = true;

            //    cmbBillName.Visible = false;
            //    label61.Visible = false;


            //    label58.Visible = true;
            //    chkTransAmtYes.Visible = true;
            //    chkTransAmtNo.Visible = true;
            //}

            SaveButtonsEnabled(false);
        }

        private void uSPsForMPDToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form MPDForm = new Forms.Tools.frmUSPMPD();
            MPDForm.Show();
        }

        private void panel_acctNumber_Paint(object sender, PaintEventArgs e)
        {

        }

        private static bool IsDate(string val)
        {
            string strDate = val;
            try
            {
                DateTime dt = DateTime.Parse(strDate);
                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        private int CheckifTrailingAsset(string investor, string outofREOdate)
        {
            int isTrailingAsset = 0;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            //            string query = string.Format(@"if (DATEDIFF(day, cast('{1}' as date), cast(GETDATE() as date)) > (select top 1 number_of_days from tbl_EM_property_investors where (investor_code1 = '{0}' or investor_code2 = '{0}'))) 
            //                                            or (DATEDIFF(day, cast('{1}' as date), cast(GETDATE() as date)) > 160)
            //                                            begin select 'True' end", investor, outofREOdate);

            string query = string.Format(@"select DATEDIFF(day, cast('{1}' as date), cast(GETDATE() as date))", investor, outofREOdate);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    isTrailingAsset = Convert.ToInt32(rd[0].ToString());
                }
                catch
                {
                    isTrailingAsset = 0;
                }
            }
            else
            {
                isTrailingAsset = 0;
            }

            conn.Close();

            return isTrailingAsset;
        }

        private void btnSearchUSP_Click(object sender, EventArgs e)
        {


            string name = string.Empty;

            char[] delimiterChars = { ' ', '\t' };

            name = comboBox5.Text.Trim().ToLower().Replace("'", "''");

            string[] words = name.Split(delimiterChars);
            string x = "%";

            foreach (string w in words)
            {
                x += w + "%";
            }

            Database.DBConnection.DatabaseVDR_Data conn = new Database.DBConnection.DatabaseVDR_Data();
            List<Database.DB.VDR> vdrData = conn.GetDataWithAddress(x, txtUSPStrt.Text.Trim(), txtUSPCity.Text.Trim(), txtUSPState.Text.Trim(), txtUSPZipcode.Text.Trim());

            dgv_usp.Invoke((Action)delegate
            {
                dgv_usp.Visible = true;
                dgv_usp.Rows.Clear();
            });




            if (vdrData != null)
            {
                foreach (Database.DB.VDR data in vdrData)
                {
                    dgv_usp.Invoke((Action)delegate
                    {
                        dgv_usp.Rows.Add(data.Vendor_ID, data.Vendor_Name, data.Address + ", " + data.City + ", " + data.State + ", " + data.Zipcode, data.vdr_status == 1 ? "Active" : "Inactive", GetStatus(data.vms_status.ToString()));
                    });
                }

            }
            else
            {
                dgv_usp.Invoke((Action)delegate
                {
                    dgv_usp.Rows.Add(null, "NO DATA FOUND", null, null, null);
                });
            }
        }

        string GetStatus(string val)
        {

            string x = "-";

            switch (val)
            {
                case "0":
                    x = "Inactive";
                    break;
                case "1":
                    x = "Active";
                    break;
                case "2":
                    x = "-";
                    break;
            }

            return x;
        }

        private void txtUSPCity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnSearchUSP_Click(null, null);
            }
        }

        private void txtUSPState_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnSearchUSP_Click(null, null);
            }
        }

        private void txtUSPZipcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnSearchUSP_Click(null, null);
            }
            else if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar) || char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void assignTaskFormToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form assignQueueForm = new Forms.Administrator.AssignQueue();
            assignQueueForm.ShowDialog(this);
        }

        private void auditFormToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form auditForm = new Forms.Supervisor.Audit();
            auditForm.WindowState = FormWindowState.Maximized;
            if (!auditForm.IsDisposed)
            {
                auditForm.ShowDialog();
            }
        }

        private void skipToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Form skip = new Forms.Supervisor.Skip();
            skip.Show();
        }

        private void invoiceInquiryToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form newInqForm = new Forms.frmInvoiceInquiry();
            newInqForm.Show();
        }

        private void chkTransAmtYes_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkTransAmtYes.Checked)
            {

                isBalTransfer = true;
                chkTransAmtNo.Checked = false;
                chkTransAmtYes.Checked = true;
                info.HasTransferAmount = "Yes";
                label59.Visible = true;
                numBalTransfer.Visible = true;
                numBalTransfer.Enabled = true;
                label47.Visible = false;
                //checkBox15.Checked = true;
                checkBox15.Visible = false;
                checkBox16.Visible = false;
                checkBox16.Checked = false;

                // Show Account Number details
                label14.Visible = false;
                textBox10.Visible = false;
                pictureBox5.Visible = false;
                checkBox6.Visible = false;
                button7.Visible = false;
                button9.Visible = false;
            }
            else
            {
                isBalTransfer = false;

                chkTransAmtYes.Checked = false;
                chkTransAmtNo.Checked = true;
                info.HasTransferAmount = "No";
                label59.Visible = false;
                numBalTransfer.Visible = false;
                numBalTransfer.Value = 0;
                numBalTransfer.Enabled = true;
                lblCount.Visible = false;
                lblOf.Visible = false;
                txtTotal.Visible = false;
                txtTotal.Text = "0";


                label62.Visible = true;
                chkBalInfoYes.Visible = true;
                chkBalInfoYes.Checked = false;
                chkBalInfoYes.Enabled = false;
                chkBalInfoNo.Visible = true;
                chkBalInfoNo.Checked = true;
                chkBalInfoNo.Enabled = false;

                label63.Visible = true;
                chkBalAmtYes.Visible = true;
                chkBalAmtYes.Checked = false;
                chkBalAmtYes.Enabled = false;
                chkBalAmtNo.Visible = true;
                chkBalAmtNo.Checked = true;
                chkBalAmtNo.Enabled = false;

                label47.Visible = true;
                checkBox15.Visible = true;
                checkBox16.Visible = true;
                checkBox15.Checked = false;
                checkBox16.Checked = true;

                // Show Account Number details
                label14.Visible = true;
                textBox10.Visible = true;
                pictureBox5.Visible = true;
                checkBox6.Visible = true;
                button7.Visible = true;
                button9.Visible = true;
            }
        }

        private void chkTransAmtNo_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkTransAmtNo.Checked)
            {

                isBalTransfer = false;

                chkTransAmtYes.Checked = false;
                chkTransAmtNo.Checked = true;
                info.HasTransferAmount = "No";
                label59.Visible = false;
                numBalTransfer.Visible = false;
                numBalTransfer.Value = 0;
                numBalTransfer.Enabled = true;
                lblCount.Visible = false;
                lblOf.Visible = false;
                txtTotal.Visible = false;
                txtTotal.Text = "0";


                label62.Visible = true;
                chkBalInfoYes.Visible = true;
                chkBalInfoYes.Checked = false;
                chkBalInfoYes.Enabled = false;
                chkBalInfoNo.Visible = true;
                chkBalInfoNo.Checked = true;
                chkBalInfoNo.Enabled = false;

                label63.Visible = true;
                chkBalAmtYes.Visible = true;
                chkBalAmtYes.Checked = false;
                chkBalAmtYes.Enabled = false;
                chkBalAmtNo.Visible = true;
                chkBalAmtNo.Checked = true;
                chkBalAmtNo.Enabled = false;

                label47.Visible = true;
                checkBox15.Visible = true;
                checkBox16.Visible = true;
                checkBox15.Checked = false;
                checkBox16.Checked = true;

                // Show Account Number details
                label14.Visible = true;
                textBox10.Visible = true;
                pictureBox5.Visible = true;
                checkBox6.Visible = true;
                button7.Visible = true;
                button9.Visible = true;
            }
            else
            {
                isBalTransfer = true;
                chkTransAmtNo.Checked = false;
                chkTransAmtYes.Checked = true;
                info.HasTransferAmount = "Yes";
                label59.Visible = true;
                numBalTransfer.Visible = true;
                numBalTransfer.Enabled = true;
                label47.Visible = false;
                //checkBox15.Checked = true;
                checkBox15.Visible = false;
                checkBox16.Visible = false;
                checkBox16.Checked = false;

                // Show Account Number details
                label14.Visible = false;
                textBox10.Visible = false;
                pictureBox5.Visible = false;
                checkBox6.Visible = false;
                button7.Visible = false;
                button9.Visible = false;
            }
        }

        private void ndLevelDuplicateInvoiceReviewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form duplicatesForm = new Forms.Administrator.DuplicatesApproval("Pre-work Item Review");
            duplicatesForm.Show();
        }

        private void eMSFunnelBetaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form newFunnelForm = new Forms.Reports.frmNewFunnel();
            newFunnelForm.Show();
        }

        private void btnCancelUpload_Click(object sender, EventArgs e)
        {

            if (MessageBox.Show("Are you sure you want to cancel this invoice?", "Upload Invoice", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                SqlConnection cnn = new SqlConnection(Constants.connectionString);
                SqlCommand cmd;
                string sql = string.Format(@"delete from tbl_ADHOC_EM_Tasks where id = {0}", info.id);

                try
                {
                    cnn.Open();
                    cmd = new SqlCommand(sql, cnn);
                    cmd.ExecuteNonQuery();
                    cmd.Dispose();
                    cnn.Close();
                    //MessageBox.Show("ExecuteNonQuery in SqlCommand executed !!");

                    LoadInvoice();

                    checkBox2.Checked = true;
                    checkBox4.Checked = false;
                    checkBox5.Checked = false;
                    checkBox13.Checked = false;
                    checkBox14.Checked = false;

                    label60.Visible = false;
                    chkASFIYes.Visible = false;
                    chkASFINo.Visible = false;

                    label58.Visible = false;
                    chkTransAmtYes.Visible = false;
                    chkTransAmtNo.Visible = false;


                    label62.Visible = false;
                    chkBalInfoYes.Visible = false;
                    chkBalInfoYes.Checked = false;
                    //chkBalInfoYes.Enabled = false;
                    chkBalInfoNo.Visible = false;
                    chkBalInfoNo.Checked = false;
                    //chkBalInfoNo.Enabled = false;

                    label63.Visible = false;
                    chkBalAmtYes.Visible = false;
                    chkBalAmtYes.Checked = false;
                    //chkBalAmtYes.Enabled = false;
                    chkBalAmtNo.Visible = false;
                    chkBalAmtNo.Checked = false;
                    //chkBalAmtNo.Enabled = false;


                    label47.Visible = false;
                    checkBox15.Visible = false;
                    //checkBox15.Checked = true;
                    checkBox16.Visible = false;

                    chkBx_NoPropertyAddress.Visible = false;
                    pictureBox5.Visible = false;
                    //cmbBillName.Text = string.Empty;
                    checkBox8.Checked = false;
                    checkBox8.Visible = false;

                    label61.Visible = false;
                    cmbBillName.Visible = false;
                    label64.Visible = false;
                    label65.Visible = false;
                    chknYes.Visible = false;
                    chknNo.Visible = false;
                    lblCount.Text = "-";
                    lblCount.Visible = false;
                    lblOf.Visible = false;
                    txtTotal.Visible = false;
                    txtTotal.Text = "0";
                    label59.Visible = false;
                    numBalTransfer.Enabled = true;
                    numBalTransfer.Visible = false;
                    numBalTransfer.Value = 0;
                    lblAdrsNotif.Visible = false;

                    invoicecount = 0;
                    ini_duedate = string.Empty;
                    ini_InvAmount = 0;
                    ini_invoice = string.Empty;
                    ini_vendor_group = string.Empty;
                    ini_vendorid = string.Empty;

                    label66.Visible = false;
                    txtUSPCity.Visible = false;
                    label67.Visible = false;
                    txtUSPState.Visible = false;
                    label68.Visible = false;
                    txtUSPZipcode.Visible = false;
                    checkBox8.Visible = false;
                    txtUSPStrt.Visible = false;
                    label69.Visible = false;
                    btnSearchUSP.Visible = false;

                    Uri myUri = new Uri("about:blank");

                    pdfviewer.Navigate(myUri);

                    // Call function asynchronously
                    BackgroundWorker worker1 = new BackgroundWorker();
                    worker1.DoWork += new DoWorkEventHandler(BW_LoadTasksCompleted_DoWork);
                    worker1.RunWorkerAsync();
                }
                catch (Exception ex)
                {
                }
            }
            else
            {
                return;
            }
        }

        private bool CheckifProcessed(string invoice, string iniDate, int id)
        {
            bool isProcessed = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = "";
            

            query = string.Format(@"select id from tbl_ADHOC_EM_Tasks where Invoice = '{0}' and LastDateModified > '{1}' and id <> {2}", invoice, iniDate, id);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    if (rd[0].ToString() == "")
                    {
                        isProcessed = false;
                    }
                    else
                    {
                        isProcessed = true;
                    }
                }
                catch
                {
                    isProcessed = false;
                }
            }
            else
            {
                isProcessed = false;
            }

            conn.Close();

            return isProcessed;
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

            if (textBox5.Text.Length > 2)
            {
                groupBox3.Enabled = true;
            }
            else
            {
                groupBox3.Enabled = false;
            }
        }

        private void tlQueueToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form tlQueue = new Forms.Supervisor.frmTLQueue();
            tlQueue.Show();
        }

        private void eMSSLAToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form SLA = new Forms.Reports.frmSLA();
            SLA.Show();
        }
    }
} 

