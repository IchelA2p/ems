﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabasePreviousWI : DatabaseConnector, IDatabase<DB.PreviousWI>
    {
        public List<DB.PreviousWI> GetAllData()
        {
            DatabaseSelector selector = new SelectorPreviousWI();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_PreviousWI]");

            base.ExecuteSelect(selector);

            return ((SelectorPreviousWI)selector).GetResults();
        }

        public uint WriteData(DB.PreviousWI data)
        {
            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_PreviousWI](
                                                completed_date,
                                                issued_date,
                                                customer_name,
                                                property_code,
                                                vendor_code,
                                                vendor_price,
                                                wo_task,
                                                work_order_number,
                                                work_order_status)
                                            VALUES({0},{1},'{2}','{3}','{4}',{5},'{6}','{7}','{8}')",
                                                  GetString(data.completed_date),   //0
                                                  GetString(data.issued_date),      //1
                                                  data.customer_name,   //2
                                                  data.property_code,   //3
                                                  data.vendor_code,     //4
                                                  data.vendor_price,    //5
                                                  data.wo_task,     //6
                                                  data.work_order_number,   //7
                                                  data.work_order_status);  //8

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.PreviousWI data)
        {
            /*
                DATA is READ ONLY!
             */
        }

        public DB.PreviousWI GetData(uint work_order_number)
        {
            DatabaseSelector selector = new SelectorPreviousWI();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_PreviousWI] WHERE work_order_number={0}", work_order_number));
            
            base.ExecuteSelect(selector);

            return ((SelectorPreviousWI)selector).GetResult();
        }

        public DB.PreviousWI GetWI(string property_code, string vendor_code, decimal vendor_price)
        {
            DatabaseSelector selector = new SelectorPreviousWI();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_PreviousWI] WHERE 
                                                property_code= '{0}' 
                                                AND vendor_code = '{1}'
                                                AND vendor_price = {2}", 
                                                property_code,
                                                vendor_code,
                                                vendor_price));

            base.ExecuteSelect(selector);

            return ((SelectorPreviousWI)selector).GetResult();
        }

        public List<DB.PreviousWI> GetDataProcessor(string work_order_number)
        {
            DatabaseSelector selector = new SelectorPreviousWI();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_PreviousWI] WHERE work_order_number='{0}'", work_order_number));

            base.ExecuteSelect(selector);

            return ((SelectorPreviousWI)selector).GetResults();
        }
    }
}
