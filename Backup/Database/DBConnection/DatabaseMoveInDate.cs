﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseMoveInDate : DatabaseConnector, IDatabase<DB.MoveInDate>
    {
        public List<DB.MoveInDate> GetAllData()
        {
            DatabaseSelector selector = new SelectorMoveInDate();
            selector.SetQuery("SELECT * FROM MIS_Alti.dbo.tbl_RPM_MoveInDate");

            base.ExecuteSelect(selector);

            return ((SelectorMoveInDate)selector).GetResults();
        }

        public uint WriteData(DB.MoveInDate data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_RPM_MoveInDate (
                                                property_code,
                                                move_in_date,
                                                pay_bills,
                                                full_address)
                                            VALUES('{0}','{1}',{2},'{3}')",
                                                  data.property_code,
                                                  data.move_in_date.ToString("yyyy-MM-dd HH:mm:ss"),
                                                  data.pay_bills,
                                                  data.full_address);

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.MoveInDate data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_RPM_MoveInDate
                                           SET
                                               full_address = '{0}',
                                               move_in_date = '{1}',
                                               pay_bills = '{2}',
                                           WHERE
                                               property_code = '{3}'",

                                            data.full_address,
                                            data.move_in_date.ToString("yyyy-MM-dd HH:mm:ss"),
                                            data.pay_bills,
                                            data.property_code);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.MoveInDate GetData(uint property_code)
        {
            DatabaseSelector selector = new SelectorMoveInDate();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_RPM_MoveInDate WHERE property_code = {0}", property_code));
            
            base.ExecuteSelect(selector);

            return ((SelectorMoveInDate)selector).GetResult();
        }

        public DB.MoveInDate GetDataProcessor(string property_code)
        {
            DatabaseSelector selector = new SelectorMoveInDate();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_RPM_MoveInDate WHERE property_code = '{0}'", property_code));
            base.ExecuteSelect(selector);

            return ((SelectorMoveInDate)selector).GetResult();
        }

        public void DeleteData(string property_code)
        {
            string Query = string.Format(@"DELETE FROM [MIS_ALTI].[dbo].[tbl_RPM_MoveInDate] WHERE property_code = '{0}'", property_code);
            base.ExecuteNonQuery(Query);
        }
    }
}
