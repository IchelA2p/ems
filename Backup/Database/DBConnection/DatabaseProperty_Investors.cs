﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseProperty_Investors : DatabaseConnector, IDatabase<DB.Property_Investors>
    {
        public List<DB.Property_Investors> GetAllData()
        {
            DatabaseSelector selector = new SelectorProperty_Investors();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_property_investors");

            base.ExecuteSelect(selector);

            return ((SelectorProperty_Investors)selector).GetResults();
        }

        public uint WriteData(DB.Property_Investors data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_EM_property_investors(
                                                investor_code1,
                                                investor_code2,
                                                investor_name,
                                                number_of_days)
                                            VALUES('{0}','{1}','{2}',{3})",
                                                  data.investor_code1, //0
                                                  data.investor_code2, //1
                                                  data.investor_name, //2
                                                  data.number_of_days); //3

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
            return 0;
        }

        public void UpdateData(DB.Property_Investors data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_EM_property_investors
                                           SET
                                               investor_code1 = '{0}',
                                               investor_code2 = '{1}',
                                               number_of_days = {2}
                                           WHERE
                                               investor_name = '{3}'",

                                                  data.investor_code1, //0
                                                  data.investor_code2, //1
                                                  data.number_of_days, //2
                                                  data.investor_name); //3

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.Property_Investors GetData(uint investor_name)
        {
            DatabaseSelector selector = new SelectorProperty_Investors();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_property_investors WHERE investor_name={0}", investor_name));
            
            base.ExecuteSelect(selector);

            return ((SelectorProperty_Investors)selector).GetResult();
        }

        public DB.Property_Investors GetDataProcessor(string investor_code)
        {
            DatabaseSelector selector = new SelectorProperty_Investors();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_property_investors]
                                              WHERE investor_code1 = '{0}' OR investor_code2 = '{1}'"
                                            ,investor_code, investor_code));

            base.ExecuteSelect(selector);

            return ((SelectorProperty_Investors)selector).GetResult();
        }

    }
}
