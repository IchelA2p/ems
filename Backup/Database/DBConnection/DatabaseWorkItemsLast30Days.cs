﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseWorkItemsLast30Days : DatabaseConnector, IDatabase<DB.WorkItemsLast30Days>
    {
        public List<DB.WorkItemsLast30Days> GetAllData()
        {
            DatabaseSelector selector = new SelectorWorkItemsLast30Days();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_WorkItemsLast30Days");

            base.ExecuteSelect(selector);

            return ((SelectorWorkItemsLast30Days)selector).GetResults();
        }

        public uint WriteData(DB.WorkItemsLast30Days data)
        {

            string query = string.Format(@""); 
//            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_EM_WorkItemsLast30Days(
//                                                completed_date,
//                                                customer_name,
//                                                issued_date,
//                                                property_code,
//                                                vendor_code,
//                                                vendor_price,
//                                                wo_task,
//                                                work_order_number,
//                                                work_order_status)
//                                            VALUES({0},'{1}',{2},'{3}','{4}',{5},'{6}','{7}','{8}')",
//                                                  GetString(data.completed_date),   //0
//                                                  data.customer_name,   //1
//                                                  GetString(data.issued_date),  //2
//                                                  data.property_code,   //3
//                                                  data.vendor_code, //4
//                                                  data.vendor_price,    //5
//                                                  data.wo_task, //6
//                                                  data.work_order_number,   //7
//                                                  data.work_order_status);  //8

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.WorkItemsLast30Days data)
        {
            /*  You can't update data  */
        }

        public DB.WorkItemsLast30Days GetData(uint account_number)
        {
            return null;
        }

        public DB.WorkItemsLast30Days GetDataProcessor(string property_code, string vendor_code, decimal vendor_price)
        {
            DatabaseSelector selector = new SelectorWorkItemsLast30Days();
            selector.SetQuery(String.Format(@"select top 1 property_code, VendorId, [Work Item #] as work_order_number, [Line Item] as wo_task, WorkItemStatus, Amount
                                                from vw_ExpeditedPayments
                                                where property_code = '{0}'
                                                and [Line Item] not like '%deposit%'
                                                and VendorId = '{1}'
                                                and (Amount = {2} or Amount = ({2} - .01))
                                                and DATEDIFF(DAY, [ECP Date], GETDATE()) <= 45
                                                order by [ECP Date] desc",
                                                property_code,
                                                vendor_code,
                                                vendor_price));

            base.ExecuteSelect(selector);

            return ((SelectorWorkItemsLast30Days)selector).GetResult();
        }

    }
}
