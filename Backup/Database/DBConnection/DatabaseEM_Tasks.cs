﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;
using System.Data.SqlClient;

namespace Database.DBConnection
{
    public class DatabaseEM_Tasks : DatabaseConnector, IDatabase<DB.EM_Tasks>
    {
        public List<DB.EM_Tasks> GetAllData()
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks (nolock)");

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }

        public uint WriteData(DB.EM_Tasks data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_ADHOC_EM_Tasks(
                                                AccountNumber,
                                                Amount,
                                                Complete,
                                                DueDate,
                                                Duration,
                                                EmployeePrimaryId,
                                                property_code,
                                                ServiceFrom,
                                                ServiceTo,
                                                usp_name,
                                                LastDateModified,
                                                PendingReasons,
                                                Invoice,
                                                vendor_group,
                                                VendorId,
                                                final_bill,
                                                InvoiceFolderName,
                                                client_code,
                                                work_item,
                                                date_bulkuploaded,
                                                userid,
                                                invoice_date,
                                                isActive,
                                                CurrentCharge,
                                                DisconnectionFee,
                                                LateFee,
                                                PaymentReceived,
                                                PreviousBalance,
                                                SpecialInstruction,
                                                vendor_address,
                                                AmountBeforeDue,
                                                remarks,
                                                HouseNoEntered,
                                                CityEntered,
                                                StateEntered,
                                                StreetEntered,
                                                ZipcodeEntered,
                                                FileAttachment,
                                                isOutOfREO,
                                                DisconnectionDate,
                                                isVendorAssigned,
                                                dateVendorAssigned,
                                                VendorAssignedStatus,
                                                isLatestData,
                                                AmountAfterDue,
                                                HasTransferAmount,
                                                isRPM,
                                                isASFI,
                                                InvoiceCount,
                                                isAddressedToASFI,
                                                BillTo,
                                                ParentAccount,
                                                isLienValid,
                                                AttachmentFile)
                                            VALUES('{0}','{1}',{2},{3},{4},{5},'{6}',{7},{8},'{9}',CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,GETUTCDATE()),'+08:00')),
                                                    '{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}',{18},'{19}',{20},{21},{22},{23},{24},{25},{26},'{27}','{28}',{29},'{30}',
                                                    '{31}','{32}','{33}','{34}','{35}','{36}',{37},{38},{39},{40},'{41}',1,{42},'{43}',{44},{45},'{46}','{47}','{48}','{49}','{50}','{51}')",
                                                  data.AccountNumber, //0
                                                  data.Amount, //1
                                                  data.Complete, //2
                                                  GetString(data.DueDate), //3
                                                  data.Duration, //4
                                                  data.EmployeePrimaryId, //5
                                                  data.property_code, //6
                                                  GetString(data.ServiceFrom), //7
                                                  GetString(data.ServiceTo), //8
                                                  data.usp_name, //9
                                                  data.PendingReasons, //11
                                                  data.Invoice, //12
                                                  data.vendor_group, //13
                                                  data.VendorId, //14
                                                  data.final_bill, //15
                                                  data.InvoiceFolderName,   //16
                                                  data.client_code, //17
                                                  data.work_item,   //18
                                                  GetString(data.date_bulkuploaded),    //19
                                                  data.userid,  //20
                                                  GetString(data.invoice_date), //21
                                                  data.isActive == true ? 1 : 0,    //22
                                                  data.CurrentCharge,   //23
                                                  data.DisconnectionFee,    //24
                                                  data.LateFee, //25
                                                  data.PaymentReceived, //26
                                                  data.PreviousBalance, //27
                                                  data.SpecialInstruction,  //28
                                                  data.vendor_address,  //29
                                                  data.AmountBeforeDue, //30
                                                  data.remarks,         //31
                                                  data.HouseNoEntered,  //32
                                                  data.CityEntered,     //33
                                                  data.StateEntered,    //34
                                                  data.StreetEntered,   //35
                                                  data.ZipcodeEntered,  //36
                                                  data.FileAttachment,  //37
                                                  data.isOutOfREO == true ? 1 : 0,  //38
                                                  GetString(data.DisconnectionDate),    //39
                                                  data.isVendorAssigned == true ? 1 : 0,    //40
                                                  GetString(data.dateVendorAssigned),  //41
                                                  data.VendorAssignedStatus, //42
                                                  data.AmountAfterDue,
                                                  data.HasTransferAmount,
                                                  data.isRPM == true ? 1 : 0,
                                                  data.isASFI == true ? 1 : 0,
                                                  data.InvoiceCount,
                                                  data.isAddressedToASFI,
                                                  data.BillTo,
                                                  data.ParentAccount,
                                                  data.isLienValid == true ? 1 : 0,
                                                  data.AttachmentFile); 

            //query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public uint WriteMultiplePropertyData(DB.EM_Tasks data)
        {
            string query = string.Format(@"insert into tbl_ADHOC_EM_Tasks (
                                        Invoice,
                                        InvoiceFolderName,
                                        lockedto,
                                        LastDateModified,
                                        isLatestData,
                                        Complete,
                                        EmployeePrimaryId,
                                        PendingReasons,
                                        InvoiceCount,
                                        usp_name,
                                        VendorId,
                                        HasTransferAmount,
                                        vendor_group,
                                        ParentAccount,
                                        AccountNumber,
                                        isLienValid
                                        )
                                        values
                                        (
                                        '{0}',
                                        '{1}',
                                        '{2}',
                                        GETDATE(),
                                        '{9}',
                                        {5},
                                        0,
                                        '{3}',
                                        '{4}',
                                        '{6}',
                                        '{7}',
                                        '{8}',
                                        '{10}',
                                        '{11}',
                                        '{12}',
                                        '{13}'
                                        )",
                                        data.Invoice,
                                        data.InvoiceFolderName,
                                        data.lockedto,
                                        data.PendingReasons,
                                        data.InvoiceCount,
                                        data.Complete,
                                        data.usp_name,
                                        data.VendorId,
                                        data.HasTransferAmount,
                                        data.isLatestData,
                                        data.vendor_group,
                                        data.ParentAccount,
                                        data.AccountNumber,
                                        data.isLienValid);

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public uint WriteSingleData(DB.EM_Tasks data)
        {
            string query = string.Format(@"insert into tbl_ADHOC_EM_Tasks (
                                            Invoice,
                                            InvoiceFolderName,
                                            lockedto,
                                            Complete,
                                            LastDateModified,
                                            isLatestData,
                                            PendingReasons
                                            )
                                            values
                                            (
                                            '{0}',
                                            '{1}',
                                            '{2}',
                                            {3},
                                            GETDATE(),
                                            1,
                                            '{4}'
                                            )",
                                            data.Invoice,
                                            data.InvoiceFolderName,
                                            data.userid,
                                            data.Complete,
                                            data.PendingReasons);

            //query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public List<Database.DB.EM_Tasks> GetLockedInvoiceForProcessing(string userid)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"select * from tbl_ADHOC_EM_Tasks a
                                              where a.lockedto = '{0}' and isnull(VendorAssignedStatus,'') <> 'ForAudit' and isLatestData = 1 and
                                              Complete = (select top 1 case when current_queue = 'New Invoice' then 20 when current_queue = 'Escalations' then 20 when current_queue = 'Water Lien' then 20 when current_queue = 'Call Out' then 0 end from tbl_EM_Queue where userid = '{0}') order by a.LastDateModified desc", userid));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }

        public List<Database.DB.EM_Tasks> GetLockedInvoice(string userid)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"select * from tbl_ADHOC_EM_Tasks a
                                                where a.isLatestData = 1
                                                and a.LastDateModified > '2015-04-01 00:00'
                                                and a.lockedto = '{0}'
                                                and a.VendorAssignedStatus <> 'ForAudit'
                                                and a.isVendorAssigned = 0
                                                and a.date_bulkuploaded is null
                                                and a.work_item = ''
                                                order by LastDateModified", userid));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }

        public int LockInvoicesToUser(string usp_name, string userid)
        {
//            string query = string.Format(@"update tbl_ADHOC_EM_Tasks
//                                            set lockedto = '{0}'
//                                            where isnull(usp_name , '') = '{1}'
//                                            and isLatestData = 1
//                                            and Complete = 0
//                                            and PendingReasons <> ''
//                                            and LastDateModified > '2015-04-01'
//                                            and (lockedto = '' or lockedto is null)",
//                                            userid, usp_name);

            string query = string.Format(@"WITH UpdateList_view AS 
                                            (select top 200 * from tbl_ADHOC_EM_Tasks 
                                              where isnull(usp_name , '') = '{1}'
	                                            and isLatestData = 1
	                                            and Complete = 0
	                                            and PendingReasons <> ''
	                                            and LastDateModified > '2015-04-01'
	                                            and (lockedto = '' or lockedto is null)
	                                            order by LastDateModified)

                                                update UpdateList_view 
                                                set lockedto = '{0}'",
                                            userid, usp_name);

            //query = DBHelper.toSingleSpace(query);
            return (int)base.ExecuteNonQuery(query);
        }

        public int LockInvoiceToUser(string invoice, string userid, int id)
        {
            string query = string.Format(@"update tbl_ADHOC_EM_Tasks
                                            set lockedto = '{0}' where lockedto IS NULL and id = {3}
                                            --where Invoice = '{1}'
                                            --and isLatestData = 1
                                            --and Complete in (20,31,32)
                                            --and LastDateModified > '2015-04-01'
                                            --and (lockedto = '' or lockedto is null or lockedto = '{2}')",
                                            userid, invoice, userid, id);

            //query = DBHelper.toSingleSpace(query);
            return (int)base.ExecuteNonQuery(query);
        }


        public void UpdateInvoice(DB.EM_Tasks data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_ADHOC_EM_Tasks
                                           SET
                                                Complete = {2},
                                                PendingReasons = '{10}',
                                                client_code = '{16}',
                                                VendorAssignedStatus = '{43}',
                                                property_code = '{17}',
                                                lockedto = NULL,
                                                SpecialInstruction = '{28}',
                                                Amount = '{1}'                                           
                                           WHERE
                                                id = {0}",

                                            data.id,   //0
                                            data.Amount, //1
                                            data.Complete, //2
                                            GetString(data.DueDate), //3
                                            data.Duration, //4
                                            data.EmployeePrimaryId, //5
                                            GetString(data.ServiceFrom), //6
                                            GetString(data.ServiceTo), //7
                                            data.usp_name, //8
                                            GetString(data.LastDateModified), //9
                                            data.PendingReasons, //10
                                            data.AccountNumber, //11
                                            data.vendor_group, //12
                                            data.VendorId, //13
                                            data.final_bill, //14
                                            data.InvoiceFolderName, //15
                                            data.client_code,   //16
                                            data.property_code, //17
                                            data.work_item, //18
                                            GetString(data.date_bulkuploaded),  //19
                                            data.userid,    //20
                                            GetString(data.invoice_date),   //21
                                            data.isActive == true ? 1 : 0,  //22
                                            data.CurrentCharge, //23
                                            data.DisconnectionFee,  //24
                                            data.LateFee,   //25
                                            data.PaymentReceived,   //26
                                            data.PreviousBalance,   //27
                                            data.SpecialInstruction,    //28
                                            data.vendor_address,    //29
                                            data.AmountBeforeDue,   //30
                                            data.remarks,   //31
                                            data.Invoice,   //32
                                            data.CityEntered,       //33
                                            data.HouseNoEntered,    //34
                                            data.StateEntered,      //35
                                            data.StreetEntered,     //36
                                            data.ZipcodeEntered,    //37
                                            data.FileAttachment,    //38
                                            data.isOutOfREO == true ? 1 : 0,    //39
                                            GetString(data.DisconnectionDate),  //40
                                            data.isVendorAssigned == true ? 1 : 0,  //41
                                            GetString(data.dateVendorAssigned), //42
                                            data.VendorAssignedStatus,
                                            data.AmountAfterDue);   //43

            //query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.EM_Tasks data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_ADHOC_EM_Tasks
                                           SET
                                                Amount = '{1}',
                                                Complete = {2},
                                                DueDate = {3},
                                                Duration = {4},
                                                EmployeePrimaryId = {5},
                                                ServiceFrom = {6},
                                                ServiceTo = {7},
                                                usp_name = '{8}',
                                                LastDateModified = {9},
                                                PendingReasons = '{10}',
                                                AccountNumber = '{11}',
                                                vendor_group = '{12}',
                                                VendorId = '{13}',
                                                final_bill = '{14}',
                                                InvoiceFolderName = '{15}',
                                                client_code = '{16}',
                                                property_code = '{17}',
                                                work_item = '{18}',
                                                date_bulkuploaded = {19},
                                                userid = '{20}',
                                                invoice_date = {21},
                                                isActive = {22},
                                                CurrentCharge = {23},
                                                DisconnectionFee = {24},
                                                LateFee = {25},
                                                PaymentReceived = {26},
                                                PreviousBalance = {27},
                                                SpecialInstruction = '{28}',
                                                vendor_address = '{29}',
                                                AmountBeforeDue = {30},
                                                remarks = '{31}',
                                                Invoice = '{32}',
                                                CityEntered = '{33}',
                                                HouseNoEntered = '{34}',
                                                StateEntered = '{35}',
                                                StreetEntered = '{36}',
                                                ZipcodeEntered = '{37}',
                                                FileAttachment = '{38}',
                                                isOutOfREO = {39},
                                                DisconnectionDate = {40},
                                                isVendorAssigned = {41},
                                                dateVendorAssigned = {42},
                                                VendorAssignedStatus = '{43}',
                                                AmountAfterDue = '{44}',
                                                HasTransferAmount = '{45}',
                                                isRPM = {46},
                                                isASFI = {47},
                                                isLienValid = {48}
                                           WHERE
                                                id = {0}",

                                            data.id,   //0
                                            data.Amount, //1
                                            data.Complete, //2
                                            GetString(data.DueDate), //3
                                            data.Duration, //4
                                            data.EmployeePrimaryId, //5
                                            GetString(data.ServiceFrom), //6
                                            GetString(data.ServiceTo), //7
                                            data.usp_name, //8
                                            GetString(data.LastDateModified), //9
                                            data.PendingReasons, //10
                                            data.AccountNumber, //11
                                            data.vendor_group, //12
                                            data.VendorId, //13
                                            data.final_bill, //14
                                            data.InvoiceFolderName, //15
                                            data.client_code,   //16
                                            data.property_code, //17
                                            data.work_item, //18
                                            GetString(data.date_bulkuploaded),  //19
                                            data.userid,    //20
                                            GetString(data.invoice_date),   //21
                                            data.isActive == true ? 1 : 0,  //22
                                            data.CurrentCharge, //23
                                            data.DisconnectionFee,  //24
                                            data.LateFee,   //25
                                            data.PaymentReceived,   //26
                                            data.PreviousBalance,   //27
                                            data.SpecialInstruction,    //28
                                            data.vendor_address,    //29
                                            data.AmountBeforeDue,   //30
                                            data.remarks,   //31
                                            data.Invoice,   //32
                                            data.CityEntered,       //33
                                            data.HouseNoEntered,    //34
                                            data.StateEntered,      //35
                                            data.StreetEntered,     //36
                                            data.ZipcodeEntered,    //37
                                            data.FileAttachment,    //38
                                            data.isOutOfREO == true ? 1 : 0,    //39
                                            GetString(data.DisconnectionDate),  //40
                                            data.isVendorAssigned == true ? 1 : 0,  //41
                                            GetString(data.dateVendorAssigned), //42
                                            data.VendorAssignedStatus,
                                            data.AmountAfterDue,
                                            data.HasTransferAmount,
                                            data.isRPM == true ? 1 : 0,
                                            data.isASFI == true ? 1 : 0,
                                            data.isLienValid == true ? 1 : 0);   //43

            //query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public void UnlockInvoice(int id)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a");

            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                            SET lockedto = null
                                            WHERE id = {0}", id);

            conn.Open();

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void UpdateAuditPass(int id, DateTime AuditDate, string Auditor, int Duration)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a");

            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                            SET VendorAssignedStatus = 'New',
	                                            AuditDate = CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,GETUTCDATE()),'+08:00')),
	                                            AuditedBy = '{1}',
	                                            AuditResult = 'Pass',
                                                AuditDuration = '{2}',
	                                            lockedto = null
                                            WHERE id = {0}", id, Auditor, Duration);

            conn.Open();

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void UpdateAuditFail(int id, DateTime AuditDate, string Auditor, int Duration)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a");

            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                            SET VendorAssignedStatus = null,
	                                            AuditDate = CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,GETUTCDATE()),'+08:00')),
	                                            AuditedBy = '{1}',
	                                            AuditResult = 'Fail',
                                                AuditDuration = '{2}',
	                                            lockedto = null
                                            WHERE id = {0}", id, Auditor, Duration);

            conn.Open();

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public void UpdateLatestData(int id)
        {
            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a");

            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                           SET isLatestData = 0,
                                               lockedto = null
                                         WHERE id = {0}", id);

            conn.Open();

            SqlCommand cmd = new SqlCommand(query, conn);
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        public DB.EM_Tasks GetData(uint propery_code)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks WHERE propery_code={0}", propery_code));
            
            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResult();
        }

        public List<DB.EM_Tasks> GetDataFromQueue(int queue, string userid, int mode)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
                                                                                                                                         
            if (mode == 20)
            {
                selector.SetQuery(String.Format(@"select distinct top 200 * from 
                                                (select * from tbl_ADHOC_EM_Tasks a
                                                where a.isLatestData = 1
                                                and a.Complete in (31,32)
                                                and a.LastDateModified > '2015-04-01 00:00'
                                                and (a.lockedto = '{1}' or a.lockedto is null)
                                                and a.userid <> '{1}'
                                                union all
                                                select * from tbl_ADHOC_EM_Tasks a
                                                where a.isLatestData = 1
                                                and a.Complete in ({0}) 
                                                and a.PendingReasons in ('New Invoice', 'New E-Bill', 'New Escalation')
                                                and a.LastDateModified > '2015-04-01 00:00'
                                                and (a.lockedto = '{1}' or a.lockedto is null)
                                                and (a.VendorAssignedStatus <> 'ForAudit' or isnull(a.VendorAssignedStatus,'') = '')) x
                                                order by x.LastDateModified desc", queue, userid));
            }
            else if (mode == 60)
            {
                selector.SetQuery(String.Format(@"select top 200 * from tbl_ADHOC_EM_Tasks a
                                                where a.isLatestData = 1
                                                and a.Complete in (20) 
                                                and a.PendingReasons in ('New Lien Invoice')
                                                and a.LastDateModified > '2015-04-01 00:00'
                                                and (a.lockedto = '{1}' or a.lockedto is null)
                                                and (a.VendorAssignedStatus <> 'ForAudit' or isnull(a.VendorAssignedStatus,'') = '')
                                                order by a.LastDateModified desc", queue, userid));
            }
            else
            {
                selector.SetQuery(String.Format(@"select top 100 * from tbl_ADHOC_EM_Tasks a
                                                where a.isLatestData = 1
                                                and a.Complete = {0}
                                                and a.PendingReasons <> ''
                                                and a.work_item = ''
                                                and a.LastDateModified > '2015-04-01 00:00'
                                                and (a.lockedto = '{1}' or a.lockedto is null)
                                                and (a.VendorAssignedStatus <> 'ForAudit' or isnull(a.VendorAssignedStatus,'') = '')
                                                order by a.lockedto, a.LastDateModified desc", queue, userid));
            }
            


            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }

        public DB.EM_Tasks GetDataFromId(uint id)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks WHERE id={0}", id));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResult();
        }

        public List<DB.EM_Tasks> GetDataProcessor(string propery_code)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks (nolock) WHERE property_code = '{0}'", propery_code));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }

        public Database.DB.EM_Tasks GetLatestInvoiceData(string invoice)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT top 1 * FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                              WHERE invoice = '{0}'
                                              ORDER BY LastDateModified desc", invoice));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResult();
        }


        public Database.DB.EM_Tasks GetLatestInvoiceDataNew(string invoice)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT top 1 * FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                              WHERE invoice = '{0}' and isLatestData = 1 and Complete in (20, 31, 32) and lockedto = '{1}'
                                              ORDER BY LastDateModified desc", invoice, Environment.UserName));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResult();
        }


        public Database.DB.EM_Tasks GetLatestInvoiceDataCallout(string invoice)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT top 1 * FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                              WHERE invoice = '{0}' and isLatestData = 1 and Complete = 0 and lockedto = '{1}'
                                              ORDER BY LastDateModified desc", invoice, Environment.UserName));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResult();
        }

        public Database.DB.EM_Tasks GetInvoiceFile(string property_code, string vendor_group)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT top 1 * FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                              WHERE property_code = '{0}'
                                              AND vendor_group = '{1}'
                                              ORDER BY LastDateModified desc", property_code, vendor_group));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResult();
        }

        public Database.DB.EM_Tasks GetInvoiceDataUsingPendingReasons(string pending_reason)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT TOP 1 * FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                              WHERE [PendingReasons] = '{0}'
                                              ORDER BY LastDateModified ASC", pending_reason));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResult();
        }

        public List<DB.EM_Tasks> GetInvoice(string Invoice)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks (nolock) WHERE Invoice = '{0}'", Invoice));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }

        public List<DB.EM_Tasks> GetCallOuts(uint EmployeePrimaryId)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks (nolock) WHERE EmployeePrimaryId = {0} AND Complete <> 1", EmployeePrimaryId));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }

        public List<DB.EM_Tasks> GetListOfEmTasks(DateTime dateFrom, DateTime dateTo)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks (nolock) WHERE LastDateModified > '{0}' AND LastDateModified < '{1}'",
                dateFrom,dateTo));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }

        public List<DB.EM_Tasks> GetListOfEmTasksOfEmployee(DateTime dateFrom, DateTime dateTo, int EmployeePrimaryId)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks (nolock) WHERE LastDateModified > '{0}' AND LastDateModified < '{1}' AND EmployeePrimaryId = {2}",
                dateFrom, dateTo, EmployeePrimaryId));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }

        public int GetHighestCompleted(DateTime dateFrom, DateTime dateTo)
        {
            SqlConnection conn;
            this.OpenConn(out conn);
            SqlCommand sqlcom = new SqlCommand(String.Format(@"SELECT TOP 1 COUNT(userid) as prod_count
                                                FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                                WHERE LastDateModified > '{0}' AND LastDateModified < '{1}' AND userid <> 'auto'
                                                GROUP BY userid 
                                                ORDER BY prod_count desc",
                                                dateFrom, dateTo), conn);
            SqlDataReader sqlrd = sqlcom.ExecuteReader();
            sqlrd.Read();

            return (int)sqlrd["prod_count"];
        }

        public int GetCountOfPendingReason(int status, string PendingReason, DateTime DateFrom, DateTime DateTo)
        {
            SqlConnection conn = new SqlConnection(this.connectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand(string.Format(@"SELECT COUNT(*) FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] (nolock)
	                                                        WHERE 
                                                            Complete = {0}
                                                            AND PendingReasons = '{1}'
	                                                        AND LastDateModified > '{2}'
	                                                        AND LastDateModified < '{3}'
	                                                        AND isLatestData = 1",
                                                            status,
                                                            PendingReason,
                                                            DateFrom,
                                                            DateTo),conn);
            SqlDataReader rd = cmd.ExecuteReader();
            rd.Read();

            int result = (int)rd[0];
            conn.Close();
            return result;
        }

        public int GetProdOfUser(DateTime dateFrom, DateTime dateTo, string userid)
        {
            SqlConnection conn = new SqlConnection(this.connectionString);
            conn.Open();

            SqlCommand sqlcom = new SqlCommand(String.Format(@"SELECT COUNT(id) as prod_count
                                                                FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                                                WHERE LastDateModified > '{0}' AND LastDateModified < '{1}'
                                                                AND userid = '{2}'",
                                                                dateFrom, dateTo, userid), conn);
            SqlDataReader sqlrd = sqlcom.ExecuteReader();
            sqlrd.Read();

            int result = (int)sqlrd["prod_count"];

            conn.Close();

            return result;
        }

        public decimal GetHoursOfUser(DateTime dateFrom, DateTime dateTo, string userid)
        {
            SqlConnection conn = new SqlConnection(this.connectionString);
            conn.Open();

            SqlCommand sqlcom = new SqlCommand(String.Format(@"select Cast(SUM(duration)/3600 as Decimal(8,2))[Hours] 
                                                                from tbl_ADHOC_EM_Tasks 
                                                                where userid = '{0}'
                                                                and LastDateModified > '{1}'
                                                                and LastDateModified < '{2}'",
                                                                userid, dateFrom, dateTo), conn);
            SqlDataReader sqlrd = sqlcom.ExecuteReader();
            sqlrd.Read();

            decimal result = 0;

            try
            {
                result = (decimal)sqlrd["Hours"];
            }
            catch
            {
                result = 0;
            }

            conn.Close();

            return result;
        }

        public decimal GetHoursOfHighestUser(DateTime dateFrom, DateTime dateTo)
        {
            SqlConnection conn = new SqlConnection(this.connectionString);
            conn.Open();

            SqlCommand sqlcom = new SqlCommand(String.Format(@"select top 1 userid, CAST(SUM(duration)/3600 as decimal(8,2)) [Hours]
                                                                from tbl_ADHOC_EM_Tasks 
                                                                where
                                                                LastDateModified > '{0}'
                                                                and LastDateModified < '{1}'
                                                                group by userid
                                                                order by SUM(duration) desc",
                                                                dateFrom, dateTo), conn);
            SqlDataReader sqlrd = sqlcom.ExecuteReader();
            sqlrd.Read();

            decimal result = 0;

            try
            {
                result = (decimal)sqlrd["Hours"];
            }
            catch
            {
                result = 0;
            }

            conn.Close();

            return result;
        }

        public List<DB.EM_Tasks> GetDataForDates(DateTime dateFrom, DateTime dateTo)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks (nolock) WHERE LastDateModified > '{0}' AND LastDateModified < '{1}'", dateFrom, dateTo));
            //selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks (nolock) WHERE LastDateModified > '{0}' AND LastDateModified < '{1}' AND isLatestData = 1", dateFrom, dateTo));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }


        public List<DB.EM_Tasks> GetAuditsForDates(DateTime dateFrom, DateTime dateTo)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            //selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks (nolock) WHERE LastDateModified > '{0}' AND LastDateModified < '{1}'", dateFrom, dateTo));
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks (nolock) WHERE AuditDate > '{0}' AND AuditDate < '{1}'", dateFrom, dateTo));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }


        public void UpdateBulkUploadDate(string invoice, DateTime dateBulkUploaded)
        {
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] 
                                            SET [date_bulkuploaded] = '{0}'
                                            WHERE [Invoice] = '{1}'
                                            AND [Complete] = 1",
                                            dateBulkUploaded,
                                            invoice);
            //query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public void UpdateBulkUploadWI(string workitem, string invoice)
        {
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] 
                                            SET [work_item] = '{0}'
                                            WHERE [Invoice] = '{1}'
                                            AND [Complete] = 1",
                                            workitem,
                                            invoice);
            //query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public List<DB.EM_Tasks> GetInvoicesFromPendingReason(string pending_reason)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                              WHERE [PendingReasons] = '{0}'
                                              ORDER BY LastDateModified ASC", pending_reason));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();  
        }

        public List<DB.EM_Tasks> GetInvoicesFromPendingReason(string pending_reason, DateTime lastdatemodified)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
                                              WHERE [PendingReasons] = '{0}' AND LastDateModified > '{1}'
                                              ORDER BY LastDateModified ASC", pending_reason, lastdatemodified));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }

        public Database.DB.EM_Tasks SearchForDuplicates(string property_code, string vendor_group, string servFrom, string servTo, string AccountNumber, string Amount, DateTime LastDateModified, string Invoice, int mode)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();

            if (mode == 0)
            {
//                selector.SetQuery(String.Format(@"SELECT TOP 1 * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks WHERE property_code = '{0}' AND
//                                    AccountNumber = '{1}' AND Amount = '{2}' AND LastDateModified < '{3}' AND LastDateModified > DATEADD(Month, -2, '{4}') AND isLatestData = 1 AND Invoice <> '{5}' AND work_item <> ''
//                                    ORDER BY LastDateModified DESC",
//                                  property_code, AccountNumber, Amount, LastDateModified, LastDateModified, Invoice));

                selector.SetQuery(String.Format(@"SELECT TOP 1 * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks
                                                  WHERE property_code = '{0}'
                                                    AND vendor_group = '{6}'	
                                                    --AND cast(ServiceFrom as date) = cast('{7}' as date)
                                                    --AND cast(ServiceTo as date) = cast('{8}' as date)
                                                    AND AccountNumber = '{1}' 
                                                    AND Amount = '{2}' 
                                                    AND LastDateModified between DATEADD(DAY,-60,GETDATE()) and GETDATE()
                                                    AND isLatestData = 1 
                                                    AND Invoice <> '{5}' 
                                                    AND work_item <> ''
                                                    ORDER BY LastDateModified DESC",
                                  property_code, AccountNumber, Amount, LastDateModified, LastDateModified, Invoice, vendor_group, servFrom, servTo));
            }
            else
            {


                selector.SetQuery(String.Format(@"select a.id, a.property_code, a.vendor_group, a.AccountNumber, a.ServiceFrom, a.ServiceTo, a.Amount, 
                                                    case when b.id is not null and c.id is null then b.work_item
                                                    when b.id is null and c.id is not null then c.work_item
                                                    when b.id is not null and c.id is not null and CAST(REPLACE(b.Amount, ',', '') as numeric(9,2)) > CAST(REPLACE(c.Amount, ',', '') as numeric(9,2)) then b.work_item
                                                    when b.id is not null and c.id is not null and CAST(REPLACE(c.Amount, ',', '') as numeric(9,2)) > CAST(REPLACE(b.Amount, ',', '') as numeric(9,2)) then c.work_item
                                                    end [work_item],
                                                    case when b.id is not null and c.id is null then b.InvoiceFolderName
                                                    when b.id is null and c.id is not null then c.InvoiceFolderName
                                                    when b.id is not null and c.id is not null and CAST(REPLACE(b.Amount, ',', '') as numeric(9,2)) > CAST(REPLACE(c.Amount, ',', '') as numeric(9,2)) then b.InvoiceFolderName
                                                    when b.id is not null and c.id is not null and CAST(REPLACE(c.Amount, ',', '') as numeric(9,2)) > CAST(REPLACE(b.Amount, ',', '') as numeric(9,2)) then c.InvoiceFolderName
                                                    end InvoiceFolderName,
                                                    case when b.id is not null and c.id is null then b.Complete
                                                    when b.id is null and c.id is not null then c.Complete
                                                    when b.id is not null and c.id is not null and CAST(REPLACE(b.Amount, ',', '') as numeric(9,2)) > CAST(REPLACE(c.Amount, ',', '') as numeric(9,2)) then b.Complete
                                                    when b.id is not null and c.id is not null and CAST(REPLACE(c.Amount, ',', '') as numeric(9,2)) > CAST(REPLACE(b.Amount, ',', '') as numeric(9,2)) then c.Complete
                                                    end Complete
                                                    from tbl_ADHOC_EM_Tasks a
                                                    outer apply
                                                    (select top 1 * from dbo.vw_EM_DuplicateReference x 
                                                    where (x.property_code = '{0}' or x.property_code = '{0}' + '1')
                                                    and x.vendor_group = case when '{6}' in ('T','D','S') then 'W' else '{6}' end
                                                    and isnull(x.Amount,'') <> ''
                                                    and isnull(x.Amount,'') <> 'NULL'
                                                    and ((CAST(REPLACE(x.Amount, ',', '') as numeric(9,2)) = CAST(REPLACE('{2}', ',', '') as numeric(9,2)) 
                                                    or CAST(REPLACE(x.Amount, ',', '') as numeric(9,2)) = (CAST(REPLACE('{2}', ',', '') as numeric(9,2)) - .01))
                                                    or replace(x.AccountNumber,'0','') = replace('{1}','0','')
                                                    or ((x.ServiceFrom = '{7}' and x.ServiceTo = '{8}') or (x.ServiceFrom is null and x.ServiceTo is null)))                                                    
                                                    and x.Invoice <> '{5}'
                                                    and DATEDIFF(DAY, x.WorkItemIssueDate, GETDATE()) <= 30
                                                    and x.[Source] = 'EMS'
                                                    order by x.WorkItemIssueDate desc) b
                                                    outer apply
                                                    (select top 1 y.* from dbo.vw_EM_DuplicateReference y 
                                                    where (y.property_code = '{0}' or y.property_code = '{0}' + '1')
                                                    and y.vendor_group = case when '{6}' in ('T','D','S') then 'W' else '{6}' end
                                                    and ((CAST(REPLACE(y.Amount, ',', '') as numeric(9,2)) = CAST(REPLACE('{2}', ',', '') as numeric(9,2)) 
                                                    or CAST(REPLACE(y.Amount, ',', '') as numeric(9,2)) = (CAST(REPLACE('{2}', ',', '') as numeric(9,2)) - .01)))
                                                    and DATEDIFF(DAY, y.WorkItemIssueDate, GETDATE()) <= 31
                                                    and y.[Source] = 'DUMP'
                                                    order by y.WorkItemIssueDate desc) c
                                                    where a.PendingReasons = 'Pending Duplicate'
                                                    and a.isLatestData = 1
                                                    and a.Invoice = '{5}'",
                                  property_code, AccountNumber, Amount, LastDateModified, LastDateModified, Invoice, vendor_group, servFrom, servTo));




//                selector.SetQuery(String.Format(@"select top 1 * from
//                                                    (
//                                                    select a.property_code, a.utility, a.account_num, a.service_frm, a.service_to, a.vendor_price, a.work_item, b.InvoiceFolderName, a.issued_date, cast(a.ecp_date as date) ecp_date
//                                                    from tbl_EM_PayAppWorkItems a
//                                                    outer apply
//                                                    (select top 1 x.InvoiceFolderName from tbl_ADHOC_EM_Tasks x where x.work_item = a.work_item) b
//                                                    where line_item not like '%deposit%'
//                                                    and work_item_status <> 'Cancelled'
//                                                    and issued_date > DATEADD(DAY, -31, GETDATE())
//                                                    union all
//                                                    select property_code, vendor_group, AccountNumber, ServiceFrom, ServiceTo, cast(Amount as numeric(15,2)), 'Pending for work item creation', InvoiceFolderName, LastDateModified, NULL
//                                                    from tbl_ADHOC_EM_Tasks
//                                                    where isLatestData = 1
//                                                    and (PendingReasons like '%bulk upload%' or PendingReasons like '%manual wi%')
//                                                    and (VendorAssignedStatus like '%success%' or VendorAssignedStatus = 'USP is correct.')
//                                                    and date_bulkuploaded is null
//                                                    and LastDateModified > DATEADD(DAY, -15, GETDATE())
//                                                    ) tbl_src
//                                                    where (((property_code = '{0}' or property_code = '{0}' + '1')
//                                                    and utility = case when '{6}' in ('S', 'T', 'D') then 'W' else '{6}' end
//                                                    and vendor_price >= '{2}')
//                                                    or ((property_code = '{0}' or property_code = '{0}' + '1')
//                                                    and utility <> case when '{6}' in ('S', 'T', 'D') then 'W' else '{6}' end
//                                                    and vendor_price > '{2}'
//                                                    ))
//                                                    order by ecp_date desc, issued_date desc",
//                                  property_code, AccountNumber, Amount, LastDateModified, LastDateModified, Invoice, vendor_group, servFrom, servTo));
            }

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResult();
        }

        public List<DB.EM_Tasks> GetDuplicates(int complete, string pendingreason)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_ADHOC_EM_Tasks WHERE 
                                                --Complete = {0} 
                                                --AND 
                                                PendingReasons = '{0}'
                                                AND isLatestData = 1
                                                AND isnull(work_item,'') = ''",
                                                pendingreason));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }

        public void UpdateDuplicateData(DB.EM_Tasks data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_ADHOC_EM_Tasks
                                           SET
                                                Complete = {1},
                                                LastDateModified = {2},
                                                PendingReasons = '{3}',
                                                AccountNumber = '{4}',
                                                InvoiceFolderName = '{5}',
                                                userid = '{6}'
                                           WHERE
                                                Invoice = '{0}' AND PendingReasons = 'Pending'",

                                            data.Invoice,   //0
                                            data.Complete,  //1
                                            GetString(data.LastDateModified), //2
                                            data.PendingReasons, //3
                                            data.AccountNumber, //4
                                            data.InvoiceFolderName, //5
                                            data.userid);   //6

            //query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.EM_Tasks GetLatestInvoiceFile(string propertyCode, string vendorGroup)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT TOP 1 *
                                              FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
	                                            WHERE property_code = '{0}'
		                                            AND vendor_group = '{1}'
	                                            ORDER BY LastDateModified DESC",
                                                propertyCode, vendorGroup));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResult();
        }

        public DB.EM_Tasks GetPreviousInfo(string propertyCode, string client_code, string AccountNumber, 
            string vendor_group)
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(String.Format(@"SELECT Top 1 *
                                              FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks]
	                                            WHERE property_code = '{0}'
                                                AND AccountNumber = '{2}'
	                                            AND vendor_group = case when '{3}' in ('S', 'T', 'D') then '{3}' else '{3}' end 
                                                AND VendorAssignedStatus in ('USP is correct.', 'USP successfully assigned')
                                                AND SentToExpeditedPaymentDate is not null
	                                            ORDER BY LastDateModified DESC",
                                                propertyCode,
                                                client_code,
                                                AccountNumber,
                                                vendor_group));

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResult();
        }

        public List<DB.EM_Tasks> GetLatestInformation()
        {
            DatabaseSelector selector = new SelectorEM_Tasks();
            selector.SetQuery(@"SELECT DISTINCT c.*
                                FROM [MIS_Alti].[dbo].[tbl_ADHOC_EM_Tasks] a (nolock)
                                outer apply (select top 1 *
                                       FROM [MIS_Alti].[dbo].[tbl_ADHOC_EM_Tasks] b (nolock)
                                       where a.[Invoice]=b.[Invoice]
                                       and a.LastDateModified > DATEADD(MONTH, -2, GETDATE())
	                                   ORDER BY [LastDateModified] Desc) c
                                WHERE c.EmployeePrimaryId IS NOT NULL");

            base.ExecuteSelect(selector);

            return ((SelectorEM_Tasks)selector).GetResults();
        }
    }
}
