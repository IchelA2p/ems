﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseAspect_Data : DatabaseConnector, IDatabase<DB.Aspect_Data>
    {
        public List<DB.Aspect_Data> GetAllData()
        {
            DatabaseSelector selector = new SelectorEmployee();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Aspectdata");

            base.ExecuteSelect(selector);

            return ((SelectorAspect)selector).GetResults();
        }

        public uint WriteData(DB.Aspect_Data data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_PAY_Aspectdata(
                                                userid,
                                                logindt,
                                                logoutdt,
                                                validationid)
                                            VALUES('{0}','{1}','{2}',{3})",
                                                  data.userId,
                                                  data.logindt.ToString("yyyy-MM-dd HH:mm:ss"),
                                                  data.logoutdt.ToString("yyyy-MM-dd HH:mm:ss"),
                                                  data.validationid);

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.Aspect_Data data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_PAY_Aspectdata
                                           SET
                                               userid = '{0}',
                                               logindt = '{1}',
                                               logoutdt = '{2}',
                                               workSched = '{3}',
                                               validationid = {4}
                                           WHERE
                                               aspectdataid = {5}",

                                            data.userId,
                                            data.logindt.ToString("yyyy-MM-dd HH:mm:ss"),
                                            data.logoutdt.ToString("yyyy-MM-dd HH:mm:ss"),
                                            data.workSched.ToString("yyyy-MM-dd HH:mm:ss"),
                                            data.validationid,
                                            data.AspectDataId);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.Aspect_Data GetData(uint AspectDataId)
        {
            DatabaseSelector selector = new SelectorAspect();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Aspectdata WHERE AspectDataId={0}", AspectDataId));
            
            base.ExecuteSelect(selector);

            return ((SelectorAspect)selector).GetResult();
        }

        public List<DB.Aspect_Data> GetDataProcessor(string userId)
        {
            DatabaseSelector selector = new SelectorAspect();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Aspectdata WHERE userId='{0}'", userId));

            base.ExecuteSelect(selector);

            return ((SelectorAspect)selector).GetResults();
        }

        public List<DB.Aspect_Data> GetDataBetweenDates(DateTime ref_date, string userid)
        {
            DatabaseSelector selector = new SelectorAspect();
            selector.SetQuery(String.Format(@"select * from tbl_PAY_Aspectdata a
                                                where a.logindt > DateAdd(Hour, -8, (
	                                                select top 1 b.SchedIn from tbl_PAY_Scheduleofemployee b
	                                                where a.userid = b.UserId
	                                                and b.Date = '{0}'))
                                                and a.logoutdt < DateAdd(Hour, 8, (
	                                                select top 1 b.SchedOut from tbl_PAY_Scheduleofemployee b
	                                                where a.userid = b.UserId
	                                                and b.Date = '{1}'))
                                                and a.userid = '{2}'"
                                                , ref_date
                                                , ref_date,
                                                userid));

            base.ExecuteSelect(selector);

            return ((SelectorAspect)selector).GetResults();
        }
    }
}
