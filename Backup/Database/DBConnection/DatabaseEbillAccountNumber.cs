﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseEbillAccountNumber : DatabaseConnector, IDatabase<DB.EbillAccountNumber>
    {
        public List<DB.EbillAccountNumber> GetAllData()
        {
            DatabaseSelector selector = new SelectorEbillAccountNumber();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_EbillAccountNumber]");

            base.ExecuteSelect(selector);

            return ((SelectorEbillAccountNumber)selector).GetResults();
        }

        public uint WriteData(DB.EbillAccountNumber data)
        {
            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_EbillAccountNumber](
                                                ebill_website_id,
                                                account_number)
                                            VALUES({0},'{1}')",
                                                  data.ebill_website_id, //0
                                                  data.account_number);    //1

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.EbillAccountNumber data)
        {
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_EM_EbillAccountNumber]
                                           SET
                                                account_number = '{1}',
                                                ebill_website_id = {2}
                                           WHERE
                                                acct_id = {0}",

                                            data.acct_id,    //0
                                            data.account_number,
                                            data.ebill_website_id);   //1

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.EbillAccountNumber GetData(uint acct_id)
        {
            DatabaseSelector selector = new SelectorEbillAccountNumber();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_EbillAccountNumber] WHERE acct_id={0}", acct_id));
            
            base.ExecuteSelect(selector);

            return ((SelectorEbillAccountNumber)selector).GetResult();
        }

        public List<DB.EbillAccountNumber> GetDataProcessor(uint ebill_website_id)
        {
            DatabaseSelector selector = new SelectorEbillAccountNumber();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_EbillAccountNumber] WHERE ebill_website_id={0}", ebill_website_id));

            base.ExecuteSelect(selector);

            return ((SelectorEbillAccountNumber)selector).GetResults();
        }

        public DB.EbillAccountNumber GetDataProcessor(uint ebill_website_id, string account_number)
        {
            DatabaseSelector selector = new SelectorEbillAccountNumber();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_EbillAccountNumber] WHERE ebill_website_id={0} and account_number = '{1}'", ebill_website_id,account_number));

            base.ExecuteSelect(selector);

            return ((SelectorEbillAccountNumber)selector).GetResult();
        }
    }
}
