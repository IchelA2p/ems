﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseUSPGroup : DatabaseConnector, IDatabase<DB.USPGroup>
    {
        public List<DB.USPGroup> GetAllData()
        {
            DatabaseSelector selector = new SelectorUSPGroup();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_UC_USPGroup]");

            base.ExecuteSelect(selector);

            return ((SelectorUSPGroup)selector).GetResults();
        }

        public uint WriteData(DB.USPGroup data)
        {
            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_UC_USPGroup](
                                                USPGroupName,
                                                PhoneNo)
                                            VALUES('{0}','{1}')",
                                                  data.USPGroupName); //0

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.USPGroup data)
        {
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_UC_USPGroup]
                                           SET
                                                USPGroupName = '{0}',
                                                PhoneNo = '{1}'
                                           WHERE
                                                USPGroupName = '{2}'",

                                            data.USPGroupName, //0
                                            data.PhoneNo,
                                            data.USPGroupName); //1

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.USPGroup GetData(uint USPGroupName)
        {
            DatabaseSelector selector = new SelectorUSPGroup();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_UC_USPGroup] WHERE USPGroupName={0}", USPGroupName));
            
            base.ExecuteSelect(selector);

            return ((SelectorUSPGroup)selector).GetResult();
        }

        public DB.USPGroup GetDataProcessor(string USPGroupName)
        {
            DatabaseSelector selector = new SelectorUSPGroup();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_UC_USPGroup] WHERE USPGroupName='{0}'", USPGroupName));

            base.ExecuteSelect(selector);

            return ((SelectorUSPGroup)selector).GetResult();
        }
    }
}
