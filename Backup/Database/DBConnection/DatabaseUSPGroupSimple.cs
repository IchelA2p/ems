﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseUSPGroupSimple : DatabaseConnector, IDatabase<DB.USPGroupSimple>
    {
        public List<DB.USPGroupSimple> GetAllData()
        {
            DatabaseSelector selector = new SelectorUSPGroupSimple();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_USPGroupSimple] ORDER BY usp_name");

            base.ExecuteSelect(selector);

            return ((SelectorUSPGroupSimple)selector).GetResults();
        }

        public uint WriteData(DB.USPGroupSimple data)
        {
            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_USPGroupSimple](
                                                usp_name,
                                                usp_telephone)
                                            VALUES('{0}','{1}')",
                                                  data.usp_name, //0
                                                  data.usp_telephone); //1

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.USPGroupSimple data)
        {
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_EM_USPGroupSimple]
                                           SET
                                                usp_telephone = '{0}'
                                           WHERE
                                                usp_name = '{1}'",

                                            data.usp_telephone, //0
                                            data.usp_name); //1

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.USPGroupSimple GetData(uint account_number)
        {
            DatabaseSelector selector = new SelectorUSPGroupSimple();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_USPGroupSimple] WHERE account_number={0}", account_number));
            
            base.ExecuteSelect(selector);

            return ((SelectorUSPGroupSimple)selector).GetResult();
        }

        public List<DB.USPGroupSimple> GetDataProcessor(string account_number)
        {
            DatabaseSelector selector = new SelectorUSPGroupSimple();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_USPGroupSimple] WHERE account_number='{0}'", account_number));

            base.ExecuteSelect(selector);

            return ((SelectorUSPGroupSimple)selector).GetResults();
        }
    }
}
