﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseAccountNumber : DatabaseConnector, IDatabase<DB.AccountNumber>
    {
        public List<DB.AccountNumber> GetAllData()
        {
            DatabaseSelector selector = new SelectorAccountNumber();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_AccountNumber");

            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResults();
        }

        public uint WriteData(DB.AccountNumber data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_EM_AccountNumber(
                                                property_code,
                                                account_number)
                                            VALUES('{0}','{1}')",
                                                  data.property_code, //0
                                                  data.account_number); //1

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.AccountNumber data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_EM_AccountNumber
                                           SET
                                                account_number = '{0}'
                                           WHERE
                                                property_code = '{1}'",

                                            data.account_number, //0
                                            data.property_code); //1

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.AccountNumber GetData(uint account_number)
        {
            DatabaseSelector selector = new SelectorAccountNumber();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.view_EM_ProcessedAccountNumbers with(index(account_number)) WHERE account_number={0}", account_number));
            
            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResult();
        }

        public List<DB.AccountNumber> GetDataProcessor(string account_number, string utility)
        {
            DatabaseSelector selector = new SelectorAccountNumber();
            selector.SetQuery(String.Format(@"SELECT DISTINCT TOP 1 property_code, account_number, [Source] FROM MIS_Alti.dbo.view_EM_ProcessedAccountNumbers with(index(account_number)) WHERE account_number = SUBSTRING('{0}', PATINDEX('%[^0]%', '{0}'), LEN('{0}')) and vendor_group = '{1}' order by [Source]", account_number, utility));

            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResults();
        }

        public DB.AccountNumber GetDataProcessor1(string account_number, string property_code)
        {
            DatabaseSelector selector = new SelectorAccountNumber();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.view_EM_ProcessedAccountNumbers with(index(property_code)) 
                                                WHERE account_number = '{0}' 
                                                AND property_code = '{1}'",
                                                account_number,
                                                property_code));

            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResult();
        }

        public DB.AccountNumber GetAccountNumber(string property_code)
        {
            DatabaseSelector selector = new SelectorAccountNumber();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.view_EM_ProcessedAccountNumbers with(index(property_code)) 
                                                WHERE property_code = '{0}'",
                                                property_code));

            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResult();
        }

        public List<DB.AccountNumber> GetAccountNumbers(string property_code, string vendor_group)
        {
            DatabaseSelector selector = new SelectorAccountNumber();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.view_EM_ProcessedAccountNumbers with(index(property_code)) 
                                                WHERE property_code = '{0}' and vendor_group = 'W'",
                                                property_code,
                                                vendor_group));

            base.ExecuteSelect(selector);

            return ((SelectorAccountNumber)selector).GetResults();
        }
    }
}
