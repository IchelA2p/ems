﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseUSPWebsite : DatabaseConnector, IDatabase<DB.USPWebsite>
    {
        public List<DB.USPWebsite> GetAllData()
        {
            DatabaseSelector selector = new SelectorUSPWebsite();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_USPWebsite]");

            base.ExecuteSelect(selector);

            return ((SelectorUSPWebsite)selector).GetResults();
        }

        public uint WriteData(DB.USPWebsite data)
        {
            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_USPWebsite](
                                                dateUpdated,
                                                isWorking,
                                                oldPassword,
                                                oldUsername,
                                                password,
                                                updatedBy,
                                                username,
                                                uspName,
                                                website)
                                            VALUES({0},{1},'{2}','{3}','{4}','{5}','{6}','{7}','{8}')",
                                                  GetString(data.dateUpdated),  //0
                                                  data.isWorking == true? 1:0,  //1
                                                  data.oldPassword,             //2
                                                  data.oldUsername,             //3
                                                  data.password,                //4
                                                  data.updatedBy,               //5
                                                  data.username,                //6
                                                  data.uspName,                 //7
                                                  data.website);                //8
            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.USPWebsite data)
        {
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_EM_USPWebsite]
                                           SET
                                                dateUpdated = {1},
                                                isWorking = {2},
                                                oldPassword = '{3}',
                                                oldUsername = '{4}',
                                                password = '{5}',
                                                updatedBy = '{6}',
                                                username = '{7}',
                                                uspName = '{8}',
                                                website = '{9}'
                                           WHERE
                                                uspWebsiteID = {0}",

                                            data.uspWebsiteID,  //0
                                            GetString(data.dateUpdated),   //1
                                            data.isWorking == true ? 1 : 0,     //2
                                            data.oldPassword,   //3
                                            data.oldUsername,   //4
                                            data.password,      //5
                                            data.updatedBy,     //6
                                            data.username,      //7
                                            data.uspName,       //8
                                            data.website);      //9
            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.USPWebsite GetData(uint uspWebsiteID)
        {
            DatabaseSelector selector = new SelectorUSPWebsite();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_USPWebsite] WHERE uspWebsiteID={0}", uspWebsiteID));
            
            base.ExecuteSelect(selector);

            return ((SelectorUSPWebsite)selector).GetResult();
        }

        public List<DB.USPWebsite> GetDataProcessor(string uspName)
        {
            DatabaseSelector selector = new SelectorUSPWebsite();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_USPWebsite] WHERE uspName='{0}'", uspName));

            base.ExecuteSelect(selector);

            return ((SelectorUSPWebsite)selector).GetResults();
        }


        public List<DB.USPWebsite> GetUSPs(string uspName)
        {
            DatabaseSelector selector = new SelectorUSPWebsite();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_USPWebsite] WHERE uspName LIKE '%{0}%'", uspName));

            base.ExecuteSelect(selector);

            return ((SelectorUSPWebsite)selector).GetResults();
        }
    }
}
