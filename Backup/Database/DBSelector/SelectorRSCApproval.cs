﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorRSCApproval : DatabaseSelector
    {
        private List<Database.DB.RSCApproval> list;

        public SelectorRSCApproval()
         {
             list = new List<DB.RSCApproval>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.RSCApproval info = new DB.RSCApproval();
                info.rscapproval_id = int.Parse(dataReader["rscapproval_id"].ToString());
                info.created_by = GetString(dataReader, "created_by");
                info.date_created = GetDate(dataReader, "date_created");
                info.date_modified = GetDate(dataReader, "date_modified");
                info.invoice = GetString(dataReader, "invoice");
                info.modified_by = GetString(dataReader, "modified_by");
                info.notes = GetString(dataReader, "notes");
                info.status = GetString(dataReader, "status");
                info.property_code = GetString(dataReader, "property_code");
                info.full_address = GetString(dataReader, "full_address");
                info.vendor_code = GetString(dataReader, "vendor_code");
                info.vendor_name = GetString(dataReader, "vendor_name");
                info.amount = float.Parse(dataReader["amount"].ToString());
                info.vendor_type = GetString(dataReader, "vendor_type");
                info.property_status = GetString(dataReader, "property_status");
                info.invoice_link = GetString(dataReader, "invoice_link");
                info.asset_manager = GetString(dataReader, "asset_manager");
                
                list.Add(info);
            }
        }

        public List<DB.RSCApproval> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.RSCApproval GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }

        public string GetString(SqlDataReader rd, string columnName)
        {
            try { return rd[columnName].ToString().Trim(); }
            catch { return string.Empty; }
        }

        public DateTime GetDate(SqlDataReader rd, string columnName)
        {
            try { return DateTime.Parse(rd[columnName].ToString().Trim()); }
            catch { return DateTime.MinValue; }
        }
    }
}
