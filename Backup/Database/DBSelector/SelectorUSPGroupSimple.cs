﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorUSPGroupSimple : DatabaseSelector
    {
        private List<Database.DB.USPGroupSimple> list;

        public SelectorUSPGroupSimple()
         {
             list = new List<DB.USPGroupSimple>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.USPGroupSimple info = new DB.USPGroupSimple();
                try
                {
                    info.usp_name = ((string)dataReader["usp_name"]).Trim();
                }
                catch
                {
                    info.usp_name = string.Empty;
                }

                try
                {
                    info.usp_telephone = ((string)dataReader["usp_telephone"]).Trim();
                }
                catch
                {
                    info.usp_telephone = string.Empty;
                }

                list.Add(info);
            }
        }

        public List<DB.USPGroupSimple> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.USPGroupSimple GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
