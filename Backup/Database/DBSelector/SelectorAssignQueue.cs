﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorAssignQueue : DatabaseSelector
    {
        private List<Database.DB.AssignQueue> list;

        public SelectorAssignQueue()
         {
             list = new List<DB.AssignQueue>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.AssignQueue info = new DB.AssignQueue();
                info.queue_id = int.Parse(dataReader["queue_id"].ToString());
                info.assignee = GetStrData(dataReader, "assignee");
                info.current_queue = GetStrData(dataReader, "current_queue");
                info.unchangeable = int.Parse(dataReader["unchangeable"].ToString());
                info.userid = GetStrData(dataReader, "userid");

                list.Add(info);
            }
        }

        public List<DB.AssignQueue> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.AssignQueue GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
