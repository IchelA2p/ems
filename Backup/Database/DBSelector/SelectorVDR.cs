﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorVDR : DatabaseSelector
    {
        private List<Database.DB.VDR> list;

        public SelectorVDR()
         {
             list = new List<DB.VDR>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.VDR info = new DB.VDR();

                info.Vendor_ID = GetStrData(dataReader, "Vendor_ID");
                info.Vendor_Name = GetStrData(dataReader, "Vendor_Name");
                info.VDR_Vendor_Name = GetStrData(dataReader, "VDR_Vendor_Name");
                info.VMS_Vendor_Name = GetStrData(dataReader, "VMS_Vendor_Name");
                info.Address = GetStrData(dataReader, "Address");
                //info.isActive = (bool)dataReader["isActive"];
                info.City = GetStrData(dataReader, "City");
                info.State = GetStrData(dataReader, "State");
                info.Zipcode = GetStrData(dataReader, "Zipcode");
                info.City = GetStrData(dataReader, "City");
                //info.Type = GetStrData(dataReader, "Type");

                info.vdr_status = (int)dataReader["vdr_status"]; ;
                info.vms_status = (int)dataReader["vms_status"]; ;

                list.Add(info);
            }
        }

        public List<DB.VDR> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.VDR GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
