﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorMoveInDate : DatabaseSelector
    {
        private List<Database.DB.MoveInDate> list;

        public SelectorMoveInDate()
         {
             list = new List<DB.MoveInDate>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.MoveInDate info = new DB.MoveInDate();
                try
                {
                    info.property_code = ((string)dataReader["property_code"]).Trim();
                }
                catch
                {
                    info.property_code = string.Empty;
                }
                try
                {
                    info.full_address = ((string)dataReader["full_address"]).Trim();
                }
                catch
                {
                    info.full_address = string.Empty;
                }
                try
                {
                    info.move_in_date = (DateTime)dataReader["move_in_date"];
                }
                catch
                {
                    info.move_in_date = DateTime.MinValue;
                }
                try
                {
                    info.pay_bills = (int)dataReader["pay_bills"];
                }
                catch
                {
                    info.pay_bills = 1;
                }

                list.Add(info);
            }
        }

        public List<DB.MoveInDate> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.MoveInDate GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
