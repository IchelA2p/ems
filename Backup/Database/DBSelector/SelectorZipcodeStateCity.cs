﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorZipcodeStateCity : DatabaseSelector
    {
        private List<Database.DB.ZipcodeStateCity> list;

        public SelectorZipcodeStateCity()
         {
             list = new List<DB.ZipcodeStateCity>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.ZipcodeStateCity info = new DB.ZipcodeStateCity();
                info.city_name = GetStrData(dataReader, "city_name");
                info.state = GetStrData(dataReader, "state");
                info.zip_code = GetStrData(dataReader, "zip_code");
                info.abbrev_state = GetStrData(dataReader, "abbrev_state");

                list.Add(info);
            }
        }

        public List<DB.ZipcodeStateCity> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.ZipcodeStateCity GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
