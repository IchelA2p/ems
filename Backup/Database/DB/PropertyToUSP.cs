﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class PropertyToUSP
    {
        public string property_code;
        public string gas_vendor_code;
        public string electricity_vendor_code;
        public string water_vendor_code;
        public DateTime gas_turnoff_date;
        public DateTime electricity_turnoff_date;
        public DateTime water_turnoff_date;
    }
}
