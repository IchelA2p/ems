﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class REO_Properties
    {
        public string property_code;
        public string property_number;
        public string full_address;
        public string state;
        public string city_name;
        public string zip_code;
        public string property_status;
        public string customer_name;
        public string customer_id;
        public string investor_name;
        public DateTime reosrc_date;
        public DateTime reoslc_date;
        public DateTime reosfc_date;
        public string active;
        public string pod;
        public DateTime property_status_change_date;
        public DateTime inactive_date;
        public string asset_manager;
    }
}
