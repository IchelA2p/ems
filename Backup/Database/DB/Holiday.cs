﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class Holiday
    {
        public int holiday_id;
        public DateTime date;
        public bool regularholiday;
        public bool specialholiday;
        public double rate;
        public string name;
    }
}
