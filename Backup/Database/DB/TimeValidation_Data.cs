﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class TimeValidation_Data
    {
        public int TimeValidationId;
        public DateTime StartDate;
        public DateTime EndDate;
        public int Validator_EmpId;
        public DateTime TimeValidationDate;
        public string Userid;
        public bool isCreatedColumn;
        public string NewNotReadyReasonId;
    }
}
