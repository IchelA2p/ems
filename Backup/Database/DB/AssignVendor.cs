﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class AssignVendor
    {
        public int id;
        public string property_code;
        public string vendor_code;
        public string vendor_group;
        public bool isAssigned;
        public DateTime dateAssigned;
        public DateTime dateCreated;
        public string remarks;
        public bool isActive;
        public string createdBy;
    }
}
