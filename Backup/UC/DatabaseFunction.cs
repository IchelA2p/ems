﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UC
{
    public class DatabaseFunction
    {
        public List<Database.DB.REO_Properties> Get_REO_Properties(string property_code, string zip_code, string street_number, string street_name, string state, string city_name)
        {
            Database.DBConnection.DatabaseREO_Properties conn = new Database.DBConnection.DatabaseREO_Properties();
            return conn.GetZipCode(property_code, zip_code, street_number, street_name, state, city_name);
        }

        public Database.DB.REO_Properties Get_Properties_Data(string property_code)
        {
            Database.DBConnection.DatabaseREO_Properties conn = new Database.DBConnection.DatabaseREO_Properties();
            return conn.GetData(property_code);
        }

        public List<Database.DB.USP_Contact> Get_USP(string VendorId, string VendorName, string StreetNumber, string StreetName, string CityName, string State, string PhoneNumber, string Type)
        {
            Database.DBConnection.DatabaseUSP_Contact conn = new Database.DBConnection.DatabaseUSP_Contact();
            return conn.GetDataProcessor(VendorId, VendorName, StreetNumber, StreetName, CityName, State, PhoneNumber, Type);
        }

        public Database.DB.Employee_Info Get_EmployeeInfo(string userid)
        {
            Database.DBConnection.DatabaseEmployee_Info conn = new Database.DBConnection.DatabaseEmployee_Info();
            return conn.GetDataProcessor(userid);
        }

        public Database.DB.Employee_Info Get_EmployeeInfo(int EmployeePrimaryId)
        {
            Database.DBConnection.DatabaseEmployee_Info conn = new Database.DBConnection.DatabaseEmployee_Info();
            return conn.GetDataUsingEmployeePrimaryId(EmployeePrimaryId);
        }

        public void InsertEMTask(Database.DB.EM_Tasks EM_Tasks)
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            conn.WriteData(EM_Tasks);
        }

        public Database.DB.EM_Tasks GetLatestEMTask(string invoice)
        {
            //Gets the latest data for the input invoice
            //Returns NULL if there is no data
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            List<Database.DB.EM_Tasks> listOfEmTasks = new List<Database.DB.EM_Tasks>();
            Database.DB.EM_Tasks return_emtask = new Database.DB.EM_Tasks();

            listOfEmTasks = conn.GetInvoice(invoice);

            if (listOfEmTasks != null)
            {
                if (listOfEmTasks.Count() > 1)
                {
                    //gets the latest data for the input invoice
                    int repetition = listOfEmTasks.Count() - 1;
                    return_emtask = listOfEmTasks[repetition];
                    while (repetition > 0)
                    {
                        if (return_emtask.LastDateModified < listOfEmTasks[repetition - 1].LastDateModified)
                        {
                            return_emtask = listOfEmTasks[repetition - 1];
                        }
                        repetition--;
                    }
                }
                else
                {
                    //returns data if there is only 1 data available
                    return_emtask = listOfEmTasks[0];
                }
            }
            else
            {
                //returns NULL if no data was found
                return_emtask = null;
            }
            return return_emtask;
        }

        public List<Database.DB.EM_Tasks> GetEMTask_Callouts(uint employeeprimaryid)
        {
            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
            return conn.GetCallOuts(employeeprimaryid);
        }

        public List<Database.DB.USP_Contact> GetVendorCodes(string property_code)
        {
            List<Database.DB.USP_Contact> listOfUSP = new List<Database.DB.USP_Contact>();
            Database.DB.USP_Contact USP_data = new Database.DB.USP_Contact();
            Database.DB.PropertyToUSP relationOfPropertyAndUSP = new Database.DB.PropertyToUSP();
            Database.DBConnection.DatabasePropertyToUSP conn = new Database.DBConnection.DatabasePropertyToUSP();
            Database.DBConnection.DatabaseUSP_Contact conn1 = new Database.DBConnection.DatabaseUSP_Contact();

            relationOfPropertyAndUSP = conn.GetDataProcessor(property_code);
            if (relationOfPropertyAndUSP != null)
            {
                USP_data = conn1.GetUspData(relationOfPropertyAndUSP.electricity_vendor_code);
                if (USP_data != null)
                {
                    USP_data.Type = "E";
                    listOfUSP.Add(USP_data);
                }

                USP_data = new Database.DB.USP_Contact();
                USP_data = conn1.GetUspData(relationOfPropertyAndUSP.gas_vendor_code);
                if (USP_data != null)
                {
                    USP_data.Type = "G";
                    listOfUSP.Add(USP_data);
                }

                USP_data = new Database.DB.USP_Contact();
                USP_data = conn1.GetUspData(relationOfPropertyAndUSP.water_vendor_code);
                if (USP_data != null)
                {
                    USP_data.Type = "W";
                    listOfUSP.Add(USP_data);
                }
            }

            return listOfUSP;
        }

        public double GetNumberOfDaysForInvestor(string investor_code)
        {
            Database.DBConnection.DatabaseProperty_Investors conn = new Database.DBConnection.DatabaseProperty_Investors();
            Database.DB.Property_Investors data = new Database.DB.Property_Investors();
            try
            {
                data = conn.GetDataProcessor(investor_code);
                return (double)data.number_of_days;
            }
            catch
            {
                return 0;
            }
        }

        public DateTime GetOutOfREODate(string property_code)
        {
            Database.DBConnection.DatabaseREO_Properties conn = new Database.DBConnection.DatabaseREO_Properties();
            Database.DB.REO_Properties property_data = new Database.DB.REO_Properties();

            property_data = conn.GetData(property_code);

            DateTime ReturnDate = new DateTime();

            
            if (property_data.reosfc_date != DateTime.MinValue)
            {
                ReturnDate = property_data.reosfc_date;
            }
            else if (property_data.reoslc_date != DateTime.MinValue)
            {
                ReturnDate = property_data.reoslc_date;
            }
            else if (property_data.reosrc_date != DateTime.MinValue)
            {
                ReturnDate = property_data.reosrc_date;
            }
            return ReturnDate;
        }

        public DateTime GetUtilityTurnOffDate(string property_code, string VendorId)
        {
            Database.DBConnection.DatabasePropertyToUSP conn = new Database.DBConnection.DatabasePropertyToUSP();
            Database.DB.PropertyToUSP property_usp = new Database.DB.PropertyToUSP();

            DateTime result = DateTime.MinValue;

            property_usp = conn.GetDataProcessor(property_code);

            //if electric
            if (property_usp.electricity_vendor_code == VendorId)
            {
                result = property_usp.electricity_turnoff_date;
            } // if gas
            else if (property_usp.gas_vendor_code == VendorId)
            {
                result = property_usp.gas_turnoff_date;
            } //if water
            else if (property_usp.water_vendor_code == VendorId)
            {
                result = property_usp.water_turnoff_date;
            }
            return result;
        }

        public List<Database.DB.EM_Tasks> GetListOfEmTasks(DateTime dateFrom, DateTime dateTo)
        {
            Database.DBConnection.DatabaseEM_Tasks connection = new Database.DBConnection.DatabaseEM_Tasks();
            return connection.GetListOfEmTasks(dateFrom, dateTo);
        }

        public List<Database.DB.EM_Tasks> GetListOfEmTasks(DateTime dateFrom, DateTime dateTo, int EmployeePrimaryId)
        {
            Database.DBConnection.DatabaseEM_Tasks connection = new Database.DBConnection.DatabaseEM_Tasks();
            return connection.GetListOfEmTasksOfEmployee(dateFrom, dateTo, EmployeePrimaryId);
        }

        public List<Database.DB.VDR> GetAllVDR()
        {
            Database.DBConnection.DatabaseVDR_Data conn = new Database.DBConnection.DatabaseVDR_Data();
            return conn.GetAllData();
        }

        public List<Database.DB.REO_Properties> GetAllProperties()
        {
            Database.DBConnection.DatabaseREO_Properties conn = new Database.DBConnection.DatabaseREO_Properties();
            return conn.GetAllData();
        }

        public List<Database.DB.USP_Contact> GetAllUSP()
        {
            Database.DBConnection.DatabaseUSP_Contact conn = new Database.DBConnection.DatabaseUSP_Contact();
            return conn.GetAllData();
        }

        public List<Database.DB.PropertyToUSP> GetAllRelationToUSP()
        {
            Database.DBConnection.DatabasePropertyToUSP conn = new Database.DBConnection.DatabasePropertyToUSP();
            return conn.GetAllData();
        }

        public List<Database.DB.Property_Investors> GetAllPropertyInvestors()
        {
            Database.DBConnection.DatabaseProperty_Investors conn = new Database.DBConnection.DatabaseProperty_Investors();
            return conn.GetAllData();
        }

        public bool CheckIfRESI(string property_code)
        {
            bool result = false;

            Database.DB.REO_Properties property = new Database.DB.REO_Properties();
            Database.DBConnection.DatabaseREO_Properties conn = new Database.DBConnection.DatabaseREO_Properties();

            property = conn.GetData(property_code);
            if (property != null)
            { 
                if (property.customer_id == "CL005")
                {
                    result = true;
                }
            }
            return result;
        }
    }
}
