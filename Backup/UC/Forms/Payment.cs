﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms
{
    public partial class Payment : Form
    {
        Form1 _mainForm;
        List<PaymentClass> _listOfPaymentBreakdown;

        public Payment(Form1 MainForm)
        {
            InitializeComponent();

            _mainForm = MainForm;
            _listOfPaymentBreakdown = _mainForm.paymentBreakdown;
        }

        public Payment(Form1 MainForm, bool viewOnly)
        {
            InitializeComponent();

            _mainForm = MainForm;
            _listOfPaymentBreakdown = _mainForm.paymentBreakdown;

            ComputeTotalAmount(0);

            dataGridView1.ReadOnly = true;
        }

        private void UpdatePaymentBreakdown(object sender, EventArgs e)
        {
            UpdateBreakdown();
        }

        private void UpdateBreakdown()
        {
            _listOfPaymentBreakdown = new List<PaymentClass>();

            double totalAmount = 0;

            if (dataGridView1.Rows.Count > 0)
            {
                if (dataGridView1.IsCurrentCellDirty == true)
                {
                    dataGridView1.CommitEdit(DataGridViewDataErrorContexts.Commit);
                }

                foreach (DataGridViewRow dr in dataGridView1.Rows)
                {
                    try
                    {
                        _listOfPaymentBreakdown.Add(new PaymentClass()
                        {
                            PaymentBreakdown = dr.Cells["p_PaymentBreakdown"].Value.ToString(),
                            Amount = Convert.ToDouble(dr.Cells["p_Amount"].Value)   
                        });
                    }
                    catch
                    {

                    }
                }

                //------------------------------------------------------------
                //  Disable editing of the 1st 3 data for payment breakdown
                //------------------------------------------------------------

                for (int i = 0; i < 3; i++)
                {
                    dataGridView1.Rows[i].ReadOnly = true;
                }

                ComputeTotalAmount(totalAmount);   
            }
        }

        private void ComputeTotalAmount(double totalAmount)
        {
            foreach (PaymentClass pc in _listOfPaymentBreakdown)
            {
                switch (pc.PaymentBreakdown)
                {
                    case "Previous Balance":
                        totalAmount += pc.Amount; break;
                    case "Payment Received":
                        totalAmount -= pc.Amount; break;
                    case "Late Fee":
                        totalAmount += pc.Amount; break;
                    case "Current Charge":
                        totalAmount += pc.Amount; break;
                    case "Disconnection Fee":
                        totalAmount += pc.Amount; break;
                }
            }

            textBox1.Text = totalAmount.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var closeForm = MessageBox.Show("Discard Changes ?", "Close Form", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (closeForm == DialogResult.Yes)
            {
                this.Close();    
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var askTotalAmt = MessageBox.Show("Save current changes?", "Save", 
                MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (askTotalAmt == DialogResult.Yes)
            {
                _mainForm.paymentBreakdown = _listOfPaymentBreakdown;
                this.Close();
            }
        }

        private void Payment_Load(object sender, EventArgs e)
        {
            LoadPaymentBreakdown();
        }

        private void LoadPaymentBreakdown()
        {
            foreach (PaymentClass pc in _listOfPaymentBreakdown)
            {
                dataGridView1.Rows.Add(pc.PaymentBreakdown, pc.Amount);   
            }

            //------------------------------------------------------------
            //  Disable editing of the 1st 3 data for payment breakdown
            //------------------------------------------------------------

            for (int i = 0; i < 3; i++)
            {
                dataGridView1.Rows[i].ReadOnly = true;
            }
        }
    }
}
