﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms
{
    public partial class frmInvoiceInquiry : Form
    {
        public frmInvoiceInquiry()
        {
            InitializeComponent();
        }

        private void frmInvoiceInquiry_Load(object sender, EventArgs e)
        {
            cmbState.SelectedIndex = 0;
            cmbUtility.SelectedIndex = 0;
            getVendorList();
            getUserList();
        }

        private void getVendorList()
        {

            string query = string.Format(@"select distinct rtrim(ltrim(usp_name)) usp_name from tbl_ADHOC_EM_Tasks where isnull(usp_name, '') <> '' and ISNUMERIC(usp_name) = 0 and usp_name not in ('', '', '', '', '', '', '') order by rtrim(ltrim(usp_name))");

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();

            da.Fill(dt);

            conn.Close();

            cmbUSP.DataSource = dt;
            cmbUSP.DisplayMember = "usp_name";
            cmbUSP.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbUSP.AutoCompleteSource = AutoCompleteSource.ListItems;

            cmbUSP.SelectedIndex = -1;
        }

        private void getUserList()
        {

            string query = string.Format(@"select distinct userid from tbl_EM_Queue order by userid");

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();

            da.Fill(dt);

            conn.Close();

            cmbUserid.DataSource = dt;
            cmbUserid.DisplayMember = "userid";
            cmbUserid.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbUserid.AutoCompleteSource = AutoCompleteSource.ListItems;

            cmbUserid.SelectedIndex = -1;
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            PopulateInvoices();
            PopulatePayments();
        }

        private void PopulateInvoices()
        {
            int countofval = 0;
            string obj = string.Empty;

            foreach (var groupBox in Controls.OfType<GroupBox>())
            {
                foreach (var textBox in groupBox.Controls.OfType<TextBox>())
                {
                    if (textBox.Text != string.Empty)
                    { countofval += 1; };
                }

                foreach (var combobox in groupBox.Controls.OfType<ComboBox>())
                {
                    if (combobox.Text != string.Empty)
                    { countofval += 1; };
                }
            }

            bool param_address = false;
            string query = string.Empty;

            if (txtStrNum.Text.ToString().Trim() == string.Empty && txtStrName.Text.ToString().Trim() == string.Empty && txtCity.Text.ToString().Trim() == string.Empty && cmbState.Text.ToString().Trim() == string.Empty && cmbState.Text.ToString().Trim() == "--" && txtZipCode.Text == string.Empty)
            {
                param_address = false;
            }
            else
            {
                param_address = true;
            }

            query = @"select distinct a.property_code [Property Code], 
                    c.full_address [Service Address], c.customer_name [Client], 
                    b.vendor_group [Utility], case when e.REFDESC is null and b.PendingReasons like '%Manual WI%' then 'WI Creation' else e.REFDESC end [Queue], 
                    case when e.REFDESC = 'WI Creation' and b.VendorAssignedStatus = 'Unable to save the record. USP not found.' then 'VID is not available in VMS'
                    when e.REFDESC = 'WI Creation' and b.VendorAssignedStatus in ('USP is correct.', 'USP successfully assigned', 'USP successfully assigned; Utility is Exception') and b.date_bulkuploaded is null then b.PendingReasons
                    when e.REFDESC = 'WI Creation' and b.VendorAssignedStatus in ('USP is correct.', 'USP successfully assigned', 'USP successfully assigned; Utility is Exception') and b.date_bulkuploaded is not null and b.work_item = '' then 'VMS Bulk Upload Error'
                    else b.PendingReasons end [Pending Reason],
                    case when e.REFDESC = 'WI Creation' and b.VendorAssignedStatus in ('USP is correct.', 'USP successfully assigned', 'USP successfully assigned; Utility is Exception') and b.work_item = '' then b.remarks end [Bulk Upload Error],
                    b.work_item [Work Item], b.AccountNumber [Account No.], b.usp_name [Utility Service Provider], b.VendorId [Vendor ID], 
                    b.invoice_date [Bill Date], b.ServiceFrom [Service from], b.ServiceTo [Service to], b.DueDate [Due Date], 
                    b.Amount [Amount Due], b.PreviousBalance [Previous Balance], b.PaymentReceived [Payment Received], b.CurrentCharge [Current Charge], b.LateFee [Late Fee], b.DisconnectionFee [Disconnection Fee], 
                    b.final_bill [Final bill], b.WorkItemStatus [Work Item Status], b.SentToExpeditedPaymentDate [Batch Date], 
                    b1.LastDateModified [Invoice Date Received], b.LastDateModified [Last Date Modified], b.userid [Last Processed by], b.InvoiceFolderName
                    from tbl_ADHOC_EM_Tasks a
                    outer apply
                    (select top 1 * from tbl_ADHOC_EM_Tasks  x where x.Invoice = a.Invoice and x.Complete <> 20 order by x.LastDateModified desc) b
                    outer apply
                    (select top 1 * from tbl_ADHOC_EM_Tasks  x where x.Invoice = a.Invoice and x.Complete = 20) b1
                    left join tbl_REO_Properties c
                    on a.property_code = c.property_code
                    left join (select [state], abbrev_state from tbl_EM_ZipcodeStateCity where abbrev_state is not null) d
                    on c.[state] = d.[state]
                    left join tbl_EM_ParameterReference e
                    on b.PendingReasons = e.REFVALUE
                    where ";


            if (txtPropCode.Text.ToString().Trim() != string.Empty)
            {
                query += string.Format(@"a.property_code like '{0}%'", txtPropCode.Text.ToString().Trim());
                obj = "txtPropCode";
                goto MoreFilter;
            }

            if (txtPropCode.Text.ToString().Trim() == string.Empty && param_address == true && cmbUtility.Text == string.Empty && cmbUtility.Text == "--- All ---" && txtAccountNum.Text == string.Empty && cmbUSP.Text == string.Empty)
            {
                if (txtStrNum.Text != string.Empty || txtStrName.Text != string.Empty)
                {
                    query += string.Format(@"(c.full_address like '{0}%' and c.full_address like '%{1}%') ", txtStrNum.Text, txtStrName.Text);
                }

                if (txtCity.Text != string.Empty)
                {
                    query += string.Format(@"c.city_name = '{0}' ", txtCity.Text);
                }

                if (cmbState.Text != string.Empty && cmbState.Text != "--")
                {
                    query += string.Format(@"d.abbrev_state = '{0}' ", cmbState.Text);
                }

                if (txtZipCode.Text != string.Empty)
                {
                    query += string.Format(@"c.zip_code = '{0}' ", txtZipCode.Text);
                }

                goto MoreFilter;
            }


            if (cmbUtility.Text != string.Empty)
            {
                if (cmbUtility.Text == "--- All ---")
                {
                    query += string.Format(@"a.vendor_group in ('E', 'G', 'W', 'S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Gas")
                {
                    query += string.Format(@"a.vendor_group = 'G' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Water")
                {
                    query += string.Format(@"a.vendor_group = 'W' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Electricity")
                {
                    query += string.Format(@"a.vendor_group = 'E' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Others")
                {
                    query += string.Format(@"a.vendor_group in ('S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                obj = "cmbUtility";
                goto MoreFilter;
            }

            if (txtAccountNum.Text != string.Empty)
            {
                query += string.Format(@"a.AccountNumber = '{0}' ", txtAccountNum.Text);

                obj = "txtAccountNum";
                goto MoreFilter;
            }

            if (txtWorkItem.Text != string.Empty)
            {
                query += string.Format(@"a.work_item = '{0}' ", txtWorkItem.Text);

                obj = "txtWorkItem";
                goto MoreFilter;
            }

            if (cmbUSP.Text != string.Empty)
            {
                query += string.Format(@"a.usp_name like '%{0}%' ", cmbUSP.Text);

                obj = "cmbUSP";
                goto MoreFilter;
            }

            if (dtpFrom.Value.Date != System.DateTime.Now.Date || (dtpTo.Value.Date != System.DateTime.Now.Date && dtpTo.Value.Date > dtpFrom.Value.Date))
            {
                query += string.Format(@"a.LastDateModified between '{0}' and '{1}' ", dtpFrom.Value, dtpTo.Value);

                obj = "dtp";
                goto MoreFilter;
            }

            if (cmbUserid.Text != String.Empty)
            {
                query += string.Format(@"a.userid = '{0}' ", cmbUserid.Text);

                obj = "cmbUserid";
                goto MoreFilter;
            }


            MoreFilter:

            if (txtPropCode.Text.ToString().Trim() != string.Empty && obj != "txtPropCode")
            {
                query += string.Format(@"and a.property_code like '{0}%'", txtPropCode.Text.ToString().Trim());
            }

            if (txtPropCode.Text.ToString().Trim() == string.Empty && param_address == true)
            {
                if (txtStrNum.Text != string.Empty || txtStrName.Text != string.Empty)
                {
                    query += string.Format(@"and (c.full_address like '{0}%' and c.full_address like '%{1}%') ", txtStrNum.Text, txtStrName.Text);
                }

                if (txtCity.Text != string.Empty)
                {
                    query += string.Format(@"and  c.city_name = '{0}' ", txtCity.Text);
                }

                if (cmbState.Text != string.Empty && cmbState.Text != "--")
                {
                    query += string.Format(@"and d.abbrev_state = '{0}' ", cmbState.Text);
                }

                if (txtZipCode.Text != string.Empty)
                {
                    query += string.Format(@"and c.zip_code = '{0}' ", txtZipCode.Text);
                }
            }

            if (cmbUtility.Text != string.Empty && obj != "cmbUtility")
            {
                if (cmbUtility.Text == "--- All ---")
                {
                    query += string.Format(@"and a.vendor_group in ('E', 'G', 'W', 'S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Gas")
                {
                    query += string.Format(@"and a.vendor_group = 'G' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Water")
                {
                    query += string.Format(@"and a.vendor_group = 'W' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Electricity")
                {
                    query += string.Format(@"and a.vendor_group = 'E' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Others")
                {
                    query += string.Format(@"and a.vendor_group in ('S', 'T', 'D', 'None') ", txtZipCode.Text);
                }
            }

            if (txtAccountNum.Text != string.Empty && obj != "txtAccountNum")
            {
                query += string.Format(@"and a.AccountNumber = '{0}' ", txtAccountNum.Text);
            }

            if (txtWorkItem.Text != string.Empty && obj != "txtWorkItem")
            {
                query += string.Format(@"and a.work_item = '{0}' ", txtWorkItem.Text);
            }

            if (cmbUSP.Text != string.Empty && obj != "cmbUSP")
            {
                query += string.Format(@"and a.usp_name like '%{0}%' ", cmbUSP.Text);
            }

            if (dtpFrom.Value.Date != System.DateTime.Now.Date || (dtpTo.Value.Date != System.DateTime.Now.Date && dtpTo.Value.Date > dtpFrom.Value.Date) && obj != "dtp")
            {
                query += string.Format(@"and a.LastDateModified between '{0}' and '{1}' ", dtpFrom.Value, dtpTo.Value);
            }

            if (cmbUserid.Text != String.Empty && obj != "cmbUserid")
            {
                query += string.Format(@"and a.userid = '{0}' ", cmbUserid.Text);
            }

            query += string.Format(@"order by b.LastDateModified desc");

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            dgList.DataSource = dt;

            conn.Close();
        }

        private void PopulatePayments()
        {
            int countofval = 0;
            string obj = string.Empty;

            foreach (var groupBox in Controls.OfType<GroupBox>())
            {
                foreach (var textBox in groupBox.Controls.OfType<TextBox>())
                {
                    if (textBox.Text != string.Empty)
                    { countofval += 1; };
                }

                foreach (var combobox in groupBox.Controls.OfType<ComboBox>())
                {
                    if (combobox.Text != string.Empty)
                    { countofval += 1; };
                }
            }

            bool param_address = false;
            string query = string.Empty;

            if (txtStrNum.Text.ToString().Trim() == string.Empty && txtStrName.Text.ToString().Trim() == string.Empty && txtCity.Text.ToString().Trim() == string.Empty && cmbState.Text.ToString().Trim() == string.Empty && cmbState.Text.ToString().Trim() == "--" && txtZipCode.Text == string.Empty)
            {
                param_address = false;
            }
            else
            {
                param_address = true;
            }

            query = @"select a.property_code, b.full_address, work_item, line_item, work_item_status, account_num, vendor_code, c.Vendor_Name, vendor_price, issued_date, pay_approved_datetime, ecp_date, issued_by, reviewed_by, review_datetime, ecp_remarks 
                    from tbl_EM_PayAppWorkItems a
                    outer apply
                    (select top 1 x.* from tbl_REO_Properties x where x.property_code = a.property_code order by x.client_hierarchy) b
                    left join vw_VID c
                    on a.vendor_code = c.Vendor_ID
                    left join (select [state], abbrev_state from tbl_EM_ZipcodeStateCity where abbrev_state is not null) d
                    on c.[state] = d.[state]
                    where ";

            if (txtPropCode.Text.ToString().Trim() != string.Empty)
            {
                query += string.Format(@"a.property_code like '{0}%'", txtPropCode.Text.ToString().Trim());
                obj = "txtPropCode";
                goto MoreFilter;
            }

            if (txtPropCode.Text.ToString().Trim() == string.Empty && param_address == true && cmbUtility.Text == string.Empty && cmbUtility.Text == "--- All ---" && txtAccountNum.Text == string.Empty && cmbUSP.Text == string.Empty)
            {
                if (txtStrNum.Text != string.Empty || txtStrName.Text != string.Empty)
                {
                    query += string.Format(@"(b.full_address like '{0}%' and b.full_address like '%{1}%') ", txtStrNum.Text, txtStrName.Text);
                }

                if (txtCity.Text != string.Empty)
                {
                    query += string.Format(@"b.city_name = '{0}' ", txtCity.Text);
                }

                if (cmbState.Text != string.Empty && cmbState.Text != "--")
                {
                    query += string.Format(@"d.abbrev_state = '{0}' ", cmbState.Text);
                }

                if (txtZipCode.Text != string.Empty)
                {
                    query += string.Format(@"b.zip_code = '{0}' ", txtZipCode.Text);
                }

                goto MoreFilter;
            }


            if (cmbUtility.Text != string.Empty)
            {
                if (cmbUtility.Text == "--- All ---")
                {
                    query += string.Format(@"a.utility in ('E', 'G', 'W', 'S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Gas")
                {
                    query += string.Format(@"a.utility = 'G' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Water")
                {
                    query += string.Format(@"a.utility = 'W' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Electricity")
                {
                    query += string.Format(@"a.utility = 'E' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Others")
                {
                    query += string.Format(@"a.utility in ('S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                obj = "cmbUtility";
                goto MoreFilter;
            }

            if (txtAccountNum.Text != string.Empty)
            {
                query += string.Format(@"a.account_num = '{0}' ", txtAccountNum.Text);

                obj = "txtAccountNum";
                goto MoreFilter;
            }

            if (txtWorkItem.Text != string.Empty)
            {
                query += string.Format(@"a.work_item = '{0}' ", txtWorkItem.Text);

                obj = "txtWorkItem";
                goto MoreFilter;
            }

            if (cmbUSP.Text != string.Empty)
            {
                query += string.Format(@"c.Vendor_Name like '%{0}%' ", cmbUSP.Text);

                obj = "cmbUSP";
                goto MoreFilter;
            }

            if (dtpFrom.Value.Date != System.DateTime.Now.Date || (dtpTo.Value.Date != System.DateTime.Now.Date && dtpTo.Value.Date > dtpFrom.Value.Date))
            {
                query += string.Format(@"(cast(a.ecp_date as date) between '{0}' and '{1}' or a.issued_date between '{0}' and '{1}') ", dtpFrom.Value, dtpTo.Value);

                obj = "dtp";
                goto MoreFilter;
            }

            if (cmbUserid.Text != String.Empty)
            {
                query += string.Format(@"a.issued_by = '{0}' ", cmbUserid.Text);

                obj = "cmbUserid";
                goto MoreFilter;
            }


            MoreFilter:

            if (txtPropCode.Text.ToString().Trim() != string.Empty && obj != "txtPropCode")
            {
                query += string.Format(@"and a.property_code like '{0}%'", txtPropCode.Text.ToString().Trim());
            }

            if (txtPropCode.Text.ToString().Trim() == string.Empty && param_address == true)
            {
                if (txtStrNum.Text != string.Empty || txtStrName.Text != string.Empty)
                {
                    query += string.Format(@"and (b.full_address like '{0}%' and b.full_address like '%{1}%') ", txtStrNum.Text, txtStrName.Text);
                }

                if (txtCity.Text != string.Empty)
                {
                    query += string.Format(@"and  b.city_name = '{0}' ", txtCity.Text);
                }

                if (cmbState.Text != string.Empty && cmbState.Text != "--")
                {
                    query += string.Format(@"and d.abbrev_state = '{0}' ", cmbState.Text);
                }

                if (txtZipCode.Text != string.Empty)
                {
                    query += string.Format(@"and b.zip_code = '{0}' ", txtZipCode.Text);
                }
            }

            if (cmbUtility.Text != string.Empty && obj != "cmbUtility")
            {
                if (cmbUtility.Text == "--- All ---")
                {
                    query += string.Format(@"and a.utility in ('E', 'G', 'W', 'S', 'T', 'D', 'None') ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Gas")
                {
                    query += string.Format(@"and a.utility = 'G' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Water")
                {
                    query += string.Format(@"and a.utility = 'W' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Electricity")
                {
                    query += string.Format(@"and a.utility = 'E' ", txtZipCode.Text);
                }

                if (cmbUtility.Text == "Others")
                {
                    query += string.Format(@"and a.utility in ('S', 'T', 'D', 'None') ", txtZipCode.Text);
                }
            }

            if (txtAccountNum.Text != string.Empty && obj != "txtAccountNum")
            {
                query += string.Format(@"and a.account_num = '{0}' ", txtAccountNum.Text);
            }

            if (txtWorkItem.Text != string.Empty && obj != "txtWorkItem")
            {
                query += string.Format(@"and a.work_item = '{0}' ", txtWorkItem.Text);
            }

            if (cmbUSP.Text != string.Empty && obj != "cmbUSP")
            {
                query += string.Format(@"and c.Vendor_Name like '%{0}%' ", cmbUSP.Text);
            }

            if (dtpFrom.Value.Date != System.DateTime.Now.Date || (dtpTo.Value.Date != System.DateTime.Now.Date && dtpTo.Value.Date > dtpFrom.Value.Date) && obj != "dtp")
            {
                query += string.Format(@"and (cast(a.ecp_date as date) between '{0}' and '{1}' or a.issued_date between '{0}' and '{1}') ", dtpFrom.Value, dtpTo.Value);
            }

            if (cmbUserid.Text != String.Empty && obj != "cmbUserid")
            {
                query += string.Format(@"and a.issued_by = '{0}' ", cmbUserid.Text);
            }

            query += string.Format(@"order by a.issued_date desc");

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            dgPayments.DataSource = dt;

            conn.Close();
        }

        private void dgList_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex > -1)
            {
                Uri myUri = new Uri(dgList.Rows[e.RowIndex].Cells["InvoiceFolderName"].Value.ToString());
                pdfviewer.Navigate(myUri);
            }
        }
    }
}
