﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC.Forms
{
    public partial class WelcomeScreen : Form
    {
        private Timer tmr;

        public WelcomeScreen()
        {
            InitializeComponent();
        }

        private void WelcomeScreen_Shown(object sender, EventArgs e)
        {
            tmr = new Timer();

            //set time interval 2 sec

            tmr.Interval = 2000;

            //starts the timer

            tmr.Start();

            tmr.Tick += tmr_Tick;
        }

        void tmr_Tick(object sender, EventArgs e)
        {

            //after 3 sec stop the timer

            tmr.Stop();

            //hide this form

            this.Dispose();

        }

    }
}
