﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using Outlook = Microsoft.Office.Interop.Outlook;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Reflection;

namespace UC.Forms.Supervisor
{

    
    public partial class ApprovalForm1000 : Form
    {
        BackgroundWorker bwLoadForReview;

        public ApprovalForm1000()
        {
            InitializeComponent();

            bwLoadForReview = new BackgroundWorker();
            //bwLoadForReview.DoWork += new DoWorkEventHandler(bwLoadForReview_DoWork);
        }

        private void ApprovalForm1000_Load(object sender, EventArgs e)
        {
            //bwLoadForReview.RunWorkerAsync();
          LoadForReview_DoWork();
        }

        private void LoadForReview_DoWork()
        {

            string query = string.Empty;

            if (cmbType.Text != "")
            {
                if (cmbType.Text == "High Amount")
                {

                    if (GetUserInfo().ToString().Contains("TL") || GetUserInfo().ToString().Contains("Lead") || GetUserInfo().ToString().Contains("MIS"))
                    {

                        if (Environment.UserName.ToLower() == "padayaoa" || Environment.UserName.ToLower() == "atuprich")
                        {
                            query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId, CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) Amount, a.CurrentCharge, a.PreviousBalance, a.LateFee, DATEDIFF(DAY, a.lastdatemodified, '{0}') [Aging (days)], a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons Like 'Approval Requested - {1}%' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID 
                                                                WHERE a.isLatestData = 1 AND (a.PendingReasons Like 'For Review%' or a.PendingReasons Like 'Approval Requested - {1}%') AND a.Amount <> '' AND a.VendorId <> '' order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", DateTime.Now, cmbType.Text);
                        }
                        else
                        {
                            query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId, CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) Amount, a.CurrentCharge, a.PreviousBalance, a.LateFee, DATEDIFF(DAY, a.lastdatemodified, '{0}') [Aging (days)], a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons Like 'Approval Requested - {1}%' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID 
                                            WHERE a.isLatestData = 1 AND (a.PendingReasons Like 'For Review%' or a.PendingReasons Like 'Approval Requested - {1}%') AND a.Amount <> '' AND CASE WHEN a.Amount <> '' then CAST(REPLACE(a.Amount, ',', '') as numeric(9,2))  else 0 end between 500 and 1000 AND a.VendorId <> '' order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", DateTime.Now, cmbType.Text);
                        }
                    }
                    else if (GetUserInfo().ToString().Contains("Assistant Manager"))
                    {

                        if (Environment.UserName.ToLower() == "trinidak")
                        {
                            query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId, CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) Amount, a.CurrentCharge, a.PreviousBalance, a.LateFee, DATEDIFF(DAY, a.lastdatemodified, '{0}') [Aging (days)], a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons Like 'Approval Requested - {1}%' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID
                                                                WHERE a.isLatestData = 1 AND (a.PendingReasons Like 'For Review%' or a.PendingReasons Like 'Approval Requested - {1}%') AND a.Amount <> '' AND CASE WHEN a.Amount <> '' then CAST(REPLACE(a.Amount, ',', '') as numeric(9,2))  else 0 end > 999 AND a.VendorId <> '' order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", DateTime.Now, cmbType.Text);
                        }
                        else
                        {
                            query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId, CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) Amount, a.CurrentCharge, a.PreviousBalance, a.LateFee, DATEDIFF(DAY, a.lastdatemodified, '{0}') [Aging (days)], a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons Like 'Approval Requested - {1}%' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID
                                            WHERE a.isLatestData = 1 AND (a.PendingReasons Like 'For Review%' or a.PendingReasons Like 'Approval Requested - {1}%') AND a.Amount <> '' AND CASE WHEN a.Amount <> '' then CAST(REPLACE(a.Amount, ',', '') as numeric(9,2))  else 0 end between 1001 and 2500 AND a.VendorId <> '' order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", DateTime.Now, cmbType.Text);
                        }
                    }
                    else
                    {
                        if (GetUserInfo().ToString() == "Sup") //if (Environment.UserName.ToLower() == "formaran" || Environment.UserName.ToLower() == "tadecind" || Environment.UserName.ToLower() == "sendaydi" || Environment.UserName.ToLower() == "llorenge")
                        {
                            query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId, CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) Amount, a.CurrentCharge, a.PreviousBalance, a.LateFee, DATEDIFF(DAY, a.lastdatemodified, '{0}') [Aging (days)], a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons Like 'Approval Requested - {1}%' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID
                                                                WHERE a.isLatestData = 1 AND (a.PendingReasons Like 'For Review%' or a.PendingReasons Like 'Approval Requested - High Amount%') AND a.Amount <> '' AND a.VendorId <> '' order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", DateTime.Now, cmbType.Text);
                        }
                    }

                }
                else if (cmbType.Text == "Trailing OMS > 45 days")
                {
                    query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId,
                            CASE WHEN (REPLACE(a.Amount, ',', '') = NULL or a.Amount = '') THEN '0.00' ELSE CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) END Amount, 
                            a.CurrentCharge, a.PreviousBalance, a.LateFee, 
                            DATEDIFF(DAY, a.lastdatemodified, GETDATE()) [Aging (days)], 
                            a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons = 'Approval Requested - {0}' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill
                            FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b  LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID 
                            WHERE a.isLatestData = 1 AND (a.PendingReasons = 'Trailing OMS > 45 days' or a.PendingReasons = 'Approval Requested - {0}') 
                            and a.LastDateModified > '2017-01-01'
                            AND a.Amount <> '' 
                            --AND a.VendorId <> '' 
                            order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", cmbType.Text);
                }
                else if (cmbType.Text == "Potential Trailing OMS > 20 days")
                {
                    query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId,
                            CASE WHEN (REPLACE(a.Amount, ',', '') = NULL or a.Amount = '') THEN '0.00' ELSE CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) END Amount, 
                            a.CurrentCharge, a.PreviousBalance, a.LateFee, 
                            DATEDIFF(DAY, a.lastdatemodified, GETDATE()) [Aging (days)], 
                            a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons = 'Approval Requested - {0}' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill
                            FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b  LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID 
                            WHERE a.isLatestData = 1 AND (a.PendingReasons = 'Potential Trailing OMS > 20 days' or a.PendingReasons = 'Approval Requested - {0}') 
                            and a.LastDateModified > '2017-01-01'
                            AND a.Amount <> '' 
                            --AND a.VendorId <> '' 
                            order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", cmbType.Text);
                }
                else if (cmbType.Text == "Trailing Asset")
                {
                    query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId,
                            CASE WHEN (REPLACE(a.Amount, ',', '') = NULL or a.Amount = '') THEN '0.00' ELSE CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) END Amount, 
                            a.CurrentCharge, a.PreviousBalance, a.LateFee, 
                            DATEDIFF(DAY, a.lastdatemodified, GETDATE()) [Aging (days)], 
                            a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons = 'Approval Requested - {0}' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill
                            FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b  LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID 
                            WHERE a.isLatestData = 1 AND (a.PendingReasons = 'Trailing Asset' or a.PendingReasons = 'Approval Requested - {0}') 
                            and a.LastDateModified > '2017-01-01'
                            AND a.Amount <> '' 
                            --AND a.VendorId <> '' 
                            order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", cmbType.Text);
                }
                else if (cmbType.Text == "Potential Trailing > 45 days")
                {
                    query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId,
                            CASE WHEN (REPLACE(a.Amount, ',', '') = NULL or a.Amount = '') THEN '0.00' ELSE CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) END Amount, 
                            a.CurrentCharge, a.PreviousBalance, a.LateFee, 
                            DATEDIFF(DAY, a.lastdatemodified, GETDATE()) [Aging (days)], 
                            a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons = 'Approval Requested - {0}' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill
                            FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID
                            WHERE a.isLatestData = 1 AND (a.PendingReasons = 'Potential Trailing > 45 days' or a.PendingReasons = 'Approval Requested - {0}') 
                            and a.LastDateModified > '2017-01-01'
                            AND a.Amount <> '' 
                            --AND a.VendorId <> '' 
                            order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", cmbType.Text);
                }
                else if (cmbType.Text == "Potential Trailing > 90 days")
                {
                    query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId,
                            CASE WHEN (REPLACE(a.Amount, ',', '') = NULL or a.Amount = '') THEN '0.00' ELSE CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) END Amount, 
                            a.CurrentCharge, a.PreviousBalance, a.LateFee, 
                            DATEDIFF(DAY, a.lastdatemodified, GETDATE()) [Aging (days)], 
                            a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons = 'Approval Requested - {0}' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill
                            FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID
                            WHERE a.isLatestData = 1 AND (a.PendingReasons = 'Potential Trailing > 90 days' or a.PendingReasons = 'Approval Requested - {0}') 
                            and a.LastDateModified > '2017-01-01'
                            AND a.Amount <> '' 
                            --AND a.VendorId <> '' 
                            order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", cmbType.Text);
                }
                else if (cmbType.Text == "Inactive Property")
                {
                    query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId,
                            CASE WHEN (REPLACE(a.Amount, ',', '') = NULL or a.Amount = '') THEN '0.00' ELSE CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) END Amount, 
                            a.CurrentCharge, a.PreviousBalance, a.LateFee, 
                            DATEDIFF(DAY, a.lastdatemodified, GETDATE()) [Aging (days)], 
                            a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons = 'Approval Requested - {0}' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.inactive_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill
                            FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b  LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID 
                            WHERE a.isLatestData = 1 AND (a.PendingReasons = 'Inactive Property' or a.PendingReasons = 'Approval Requested - {0}') 
                            and a.LastDateModified > '2017-01-01'
                            AND a.Amount <> '' 
                            --AND a.VendorId <> '' 
                            order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", cmbType.Text);
                }
                else if (cmbType.Text == "Occupied")
                {
                    query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId,
                            CASE WHEN (REPLACE(a.Amount, ',', '') = NULL or a.Amount = '') THEN '0.00' ELSE CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) END Amount, 
                            a.CurrentCharge, a.PreviousBalance, a.LateFee, 
                            DATEDIFF(DAY, a.lastdatemodified, GETDATE()) [Aging (days)], 
                            a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons = 'Approval Requested - {0}' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill
                            FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b LEFT JOIN tbl_EM_MPDUSP c ON a.VendorId = c.VendorID 
                            WHERE a.isLatestData = 1 AND (a.PendingReasons = 'Occupied' or a.PendingReasons = 'Approval Requested - {0}') 
                            and a.LastDateModified > '2017-01-01'
                            AND a.Amount <> '' 
                            --AND a.VendorId <> '' 
                            order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", cmbType.Text);
                }
//                else if (cmbType.Text == "POD OMS")
//                {
//                    query = string.Format(@"SELECT a.id, a.userid, a.property_code, a.client_code, a.VendorId,
//                            CASE WHEN (REPLACE(a.Amount, ',', '') = NULL or a.Amount = '') THEN '0.00' ELSE CAST(REPLACE(a.Amount, ',', '') as numeric(9,2)) END Amount, 
//                            a.CurrentCharge, a.PreviousBalance, a.LateFee, 
//                            DATEDIFF(DAY, a.lastdatemodified, GETDATE()) [Aging (days)], 
//                            a.InvoiceFolderName, a.lastdatemodified, case when a.PendingReasons = 'Approval Requested - {0}' then 'Approval Requested' else NULL end [Status], b.property_status, usp_name, DATEDIFF(DAY, b.reosfc_date, GETDATE()) [Aging from REODate (days)], b.inactive_date, final_bill
//                            FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a OUTER APPLY (SELECT TOP 1 * FROM tbl_REO_Properties x WHERE x.property_code = a.property_code ORDER BY x.client_hierarchy) b 
//                            WHERE a.isLatestData = 1 AND (a.PendingReasons = 'POD OMS' or a.PendingReasons = 'Approval Requested - {0}') 
//                            and a.LastDateModified > '2017-01-01'
//                            AND a.Amount <> '' 
//                            --AND a.VendorId <> '' 
//                            order by ISNULL(c.isMPD, 0) desc, a.client_code asc, a.LastDateModified desc", cmbType.Text);
//                }

                SqlConnection conn = new SqlConnection(Constants.connectionString);
                conn.Open();

                SqlCommand cmd = new SqlCommand(query, conn);

                SqlDataReader rd = cmd.ExecuteReader();

                int itemCount = 0;

                dgv_list_forapproval.Invoke((Action)delegate
                {

                    dgv_list_forapproval.Rows.Clear();

                    while (rd.Read())
                    {
                        dgv_list_forapproval.Rows.Add(rd[0], rd[3], rd[2], rd[13], rd[15], rd[16], rd[14], rd[4], rd[5], rd[6], rd[7], rd[8], rd[9], rd[17], rd[10], rd[1], rd[11], null, null, rd[12], "Select...");
                        itemCount++;
                    }
                });

                label2.Invoke((Action)delegate
                {
                    label2.Text = itemCount.ToString();
                });

                rd.Close();
                cmd = null;
                query = string.Format(@"SELECT COUNT(*) FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a WHERE
                                    a.PendingReasons IN ('Approved {1} - Bulk Upload', 'Approved {1} - Manual WI') AND
                                    a.isLatestData = 1 AND
                                    a.Invoice LIKE '{0}%'",
                                        DateTime.Now.ToString("yyyyMM"), cmbType.Text);

                cmd = new SqlCommand(query, conn);
                rd = cmd.ExecuteReader();
                rd.Read();

                label4.Invoke((Action)delegate
                {
                    label4.Text = rd[0].ToString();
                });

                rd.Close();
                cmd = null;
                query = string.Format(@"SELECT COUNT(*) FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a WHERE
                                    a.PendingReasons IN ('Declined {1}') AND
                                    a.isLatestData = 1 AND
                                    a.Invoice LIKE '{0}%'",
                                        DateTime.Now.ToString("yyyyMM"), cmbType.Text);

                cmd = new SqlCommand(query, conn);
                rd = cmd.ExecuteReader();
                rd.Read();

                label6.Invoke((Action)delegate
                {
                    label6.Text = rd[0].ToString();
                });


                rd.Close();

                cmd = null;
                query = string.Format(@"SELECT COUNT(*) FROM [MIS_ALTI].[dbo].[tbl_ADHOC_EM_Tasks] a WHERE
                                    a.PendingReasons IN ('Approval Requested - {1}') AND
                                    a.isLatestData = 1 AND
                                    a.Lastdatemodified > '{0}'",
                                        DateTime.Now.ToString("yyyy-MM-01"), cmbType.Text);

                cmd = new SqlCommand(query, conn);
                rd = cmd.ExecuteReader();
                rd.Read();

                label7.Invoke((Action)delegate
                {
                    label7.Text = rd[0].ToString();
                });

                rd.Close();

                conn.Close();

                dgv_list_forapproval.Columns[6].ValueType = typeof(decimal);
                dgv_list_forapproval.Columns[7].ValueType = typeof(decimal);
                dgv_list_forapproval.Columns[8].ValueType = typeof(decimal);
                dgv_list_forapproval.Columns[9].ValueType = typeof(decimal);
            }
        }

        private static string GetUserInfo()
        {

            string val = string.Empty;

            Outlook.Application application = null;

            // Check whether there is an Outlook process running.
            if (Process.GetProcessesByName("OUTLOOK").Count() > 0)
            {

                // If so, use the GetActiveObject method to obtain the process and cast it to an Application object.
                application = Marshal.GetActiveObject("Outlook.Application") as Outlook.Application;
            }
            else
            {

                // If not, create a new instance of Outlook and log on to the default profile.
                application = new Outlook.Application();
                Outlook.NameSpace nameSpace = application.GetNamespace("MAPI");
                nameSpace.Logon("", "", Missing.Value, Missing.Value);
                nameSpace = null;
            }

            Outlook.AddressEntry currentUser = application.Session.CurrentUser.AddressEntry;

            val = currentUser.GetExchangeUser().JobTitle;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"SELECT     userid, email, role
                                            FROM         tbl_EM_Admin
                                            WHERE     (role = 'Sup') AND (userid = '{0}')", Environment.UserName);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    val = "Sup";
                }
                catch
                {}
            }

            conn.Close();

            return val;
        }

        private void dgv_list_forapproval_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex != 18 && e.ColumnIndex != 19 && e.ColumnIndex != 20)
            {
            Uri myUri = new Uri(dgv_list_forapproval.Rows[e.RowIndex].Cells["forapproval_link"].Value.ToString());
                webBrowser1.Navigate(myUri);
            }

            if (e.RowIndex > -1 && e.ColumnIndex == 19)
            {
                dgv_list_forapproval.Columns["Comment"].DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            }


            if (e.RowIndex > -1 && e.ColumnIndex == 18)
            {
                
                var uploadfile = openFileDialog1.ShowDialog();

                if (uploadfile == DialogResult.OK)
                {
                    //if (!System.IO.File.Exists(openFileDialog1.FileName))
                    //{
                    //    MessageBox.Show("File already exist.");
                    //    return;
                    //}

                    if (System.IO.Path.GetFileNameWithoutExtension(openFileDialog1.FileName).Length > 150)
                    {
                        MessageBox.Show("File name is too long. Please rename file.");
                        return;
                    }

                    string filepath = System.IO.Path.Combine(@"\\PIV8FSASNP01\CommonShare\EMProject\ATTACHMENTS", dgv_list_forapproval.Rows[e.RowIndex].Cells["forapproval_id"].Value.ToString() + System.IO.Path.GetExtension(openFileDialog1.FileName));

                    try
                    {
                        try
                        {
                            System.IO.File.Copy(openFileDialog1.FileName, filepath, true);
                            dgv_list_forapproval.Rows[e.RowIndex].Cells[18].Value = filepath;

                            MessageBox.Show("File has been uploaded!");

                            dgv_list_forapproval.Rows[e.RowIndex].Cells[1].Selected = true;
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.ToString());
                        }

                        
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Unable to upload file. Error: " + ex.Message);
                        openFileDialog1.FileName = string.Empty;
                        dgv_list_forapproval.Rows[e.RowIndex].Cells[18].Value = string.Empty;
                    }
                }
                //else
                //{
                //    dgv_list_forapproval.Rows[e.RowIndex].Cells[14].Value = string.Empty;
                //}

                dgv_list_forapproval.CommitEdit(DataGridViewDataErrorContexts.Commit);
                dgv_list_forapproval.Refresh();
            }

        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            List<int> listOfItemsToSave = new List<int>();

            listOfItemsToSave = CountItemsForApproval();

            var confirmation = MessageBox.Show(string.Format("There are {0} item(s) for review. Do you want to proceed?", listOfItemsToSave.Count()),
                "Save Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (confirmation == DialogResult.Yes)
            {
                foreach (DataGridViewRow dr in dgv_list_forapproval.Rows)
                {
                    if (dr.Cells[20].Value != null && dr.Cells[0].Value != null )
                    {
                        if (dr.Cells[20].Value.ToString() != "Select...")
                        {
                            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
                            Database.DB.EM_Tasks data = conn.GetDataFromId(Convert.ToUInt32(dr.Cells[0].Value.ToString()));


                            Database.DBConnection.DatabaseREO_Properties propconn = new Database.DBConnection.DatabaseREO_Properties();
                            Database.DB.REO_Properties propdata = new Database.DB.REO_Properties();
                            propdata = propconn.GetData2(dr.Cells["forapproval_propertycode"].Value.ToString());

                            if (dr.Cells[20].Value.ToString() == "Approve")
                            {
                                data.Amount = dr.Cells[8].Value.ToString();

                                try
                                {
                                    data.CurrentCharge = Convert.ToDecimal(dr.Cells[9].Value);
                                }
                                catch
                                {
                                    data.CurrentCharge = 0;
                                }

                                
                                data.PreviousBalance = Convert.ToDecimal(dr.Cells[10].Value);
                                data.LateFee = Convert.ToDecimal(dr.Cells[11].Value);
                                
                                data.client_code = dr.Cells["forapproval_clientcode"].Value.ToString();
                                data.VendorAssignedStatus = "New";
                                
                                //if (data.client_code == "RESI")
                                //{

                                
                                if (((propdata.customer_name == "RESI" && System.Convert.ToDouble(data.Amount) > 2500) || ((propdata.customer_name == "OLSR" || propdata.customer_name == "PFC") && System.Convert.ToDouble(data.Amount) > 10000)) && dr.Cells[18].Value == null)
                                {
                                    MessageBox.Show("Please provide approval from client.");
                                    return;
                                }

                                try
                                {
                                    if (((propdata.customer_name == "RESI" && System.Convert.ToDouble(data.Amount) > 2500) || ((propdata.customer_name == "OLSR" || propdata.customer_name == "PFC") && System.Convert.ToDouble(data.Amount) > 10000)) && dr.Cells[19].Value == null)
                                    {
                                        MessageBox.Show("Please provide comments.");
                                        return;
                                    }
                                }
                                catch
                                {
                                    if (((propdata.customer_name == "RESI" && System.Convert.ToDouble(data.Amount) > 2500) || ((propdata.customer_name == "OLSR" || propdata.customer_name == "PFC") && System.Convert.ToDouble(data.Amount) > 10000)) && dr.Cells[19].Value.ToString() == string.Empty)
                                    {
                                        MessageBox.Show("Please provide comments.");
                                        return;
                                    }
                                }


                                if (cmbType.Text != "High Amount" && dr.Cells["Comment"].Value.ToString() == string.Empty)
                                {
                                    MessageBox.Show("Please provide comments.");
                                    return;
                                }

                                if (((cmbType.Text != "High Amount" && 
                                    cmbType.Text != "Potential Trailing > 45 days" && 
                                    cmbType.Text != "Potential Trailing > 90 days" &&
                                    cmbType.Text != "Occupied") || (cmbType.Text == "Inactive Property" && Convert.ToInt32(dr.Cells["AgefromREO"].Value) > 160))
                                    && dr.Cells[18].Value == null)
                                {

                                    MessageBox.Show("Please provide client approval.");
                                    return;
                                }


                                try
                                {
                                    data.AttachmentFile = dr.Cells[18].Value.ToString();
                                }
                                catch
                                {
                                    data.AttachmentFile = Convert.ToString(dr.Cells[18].Value);
                                }

                                //if (propdata.pod == "POD OMS" && cmbType.Text == "High Amount")
                                //{
                                //    data.Complete = 1;
                                //    data.PendingReasons = "POD OMS";
                                //}
                                //else
                                //{

                                    if (CheckDuplicates(data.property_code, data.vendor_group, data.Amount, data.Invoice, data.client_code, data.AccountNumber) == true)
                                    {
                                        data.Complete = 1;
                                        data.PendingReasons = "Pending Duplicate";
                                    }
                                    else
                                    {

                                        if (data.BillTo != string.Empty && data.BillTo != "ASFI or c/o ASFI")
                                        {

                                            if (propdata.customer_name == "PFC")
                                            {
                                                data.Complete = 1;
                                                //data.PendingReasons = "Approved " + cmbType.Text + " - Manual WI";

                                                data.PendingReasons = "Pre-work item Review";
                                            }

                                            if (propdata.customer_name == "OLSR")
                                            {
                                                if (data.SpecialInstruction != string.Empty || data.AttachmentFile != string.Empty)
                                                {
                                                    data.Complete = 1;
                                                    //data.PendingReasons = "Approved " + cmbType.Text + " - Manual WI";

                                                    data.PendingReasons = "Pre-work item Review";
                                                }
                                                else
                                                {
                                                    data.Complete = 1;
                                                    //data.PendingReasons = "Approved " + cmbType.Text + " - Bulk Upload";

                                                    data.PendingReasons = "Pre-work item Review";
                                                }
                                            }
                                            else
                                            {
                                                data.Complete = 6;
                                                data.PendingReasons = "Non-ASFI Invoice";
                                            }
                                        }
                                        else
                                        {
                                            if (data.SpecialInstruction != string.Empty || data.AttachmentFile != string.Empty)
                                            {
                                                data.Complete = 1;
                                                //data.PendingReasons = "Approved " + cmbType.Text + " - Manual WI";

                                                data.PendingReasons = "Pre-work item Review";
                                            }
                                            else
                                            {
                                                if (propdata.customer_name == "PFC")
                                                {
                                                    data.Complete = 1;
                                                    //data.PendingReasons = "Approved " + cmbType.Text + " - Manual WI";

                                                    data.PendingReasons = "Pre-work item Review";
                                                }
                                                else
                                                {
                                                    data.Complete = 1;
                                                    //data.PendingReasons = "Approved " + cmbType.Text + " - Bulk Upload";

                                                    data.PendingReasons = "Pre-work item Review";
                                                }
                                            }
                                        }
                                    }
                                //}
                            }
                            else if (dr.Cells[20].Value.ToString() == "Decline") //if (dr.Cells[10].Value.ToString() == "Decline")
                            {
                                data.Amount = dr.Cells[8].Value.ToString();

                                try
                                {
                                    data.CurrentCharge = Convert.ToDecimal(dr.Cells[9].Value);
                                }
                                catch
                                {
                                    data.CurrentCharge = 0;
                                }

                                
                                data.PreviousBalance = Convert.ToDecimal(dr.Cells[10].Value);
                                data.LateFee = Convert.ToDecimal(dr.Cells[11].Value);
                                data.Complete = 1;
                                data.client_code = dr.Cells[1].Value.ToString();


                                if (dr.Cells[19].Value != null)
                                {
                                    if (data.SpecialInstruction != string.Empty)
                                    {
                                        data.SpecialInstruction = data.SpecialInstruction + "; " + dr.Cells[19].Value.ToString();
                                    }
                                    else
                                    {
                                        data.SpecialInstruction = dr.Cells[19].Value.ToString();
                                    }
                                }


                                //if (data.client_code == "RESI")
                                //{
                                data.PendingReasons = "Declined " + cmbType.Text;
                                //}
                                //else
                                //{
                                //    data.PendingReasons = "Declined Amount > $1,000";
                                //}
                            }
                            else
                            {
                                data.PendingReasons = "Approval Requested - " + cmbType.Text;
                            }

                            data.LastDateModified = DateTime.Now;
                            data.userid = Environment.UserName;
                            

                            //Database.DBConnection.DatabaseWorkItemsLast30Days conn1 = new Database.DBConnection.DatabaseWorkItemsLast30Days();

                            //if (data.property_code != null && data.VendorId != null && data.PreviousBalance != 0 && data.Complete == 1)
                            //{
                            //    Database.DB.WorkItemsLast30Days wi = conn1.GetDataProcessor(data.property_code, data.VendorId, data.PreviousBalance);

                            //    if (wi != null)
                            //    {
                            //        if (wi.WorkItemStatus != "Cancelled")
                            //        {
                            //            data.Amount = (Convert.ToDecimal(data.Amount) - wi.Amount).ToString();
                            //            data.PendingReasons = "Manual WI";
                            //            data.SpecialInstruction = string.Format(@"Previous balance ${0:N2} already paid on work item {1}", wi.Amount, wi.work_order_number);
                            //        }
                            //    }
                            //}

                            try
                            {
                                conn.UpdateLatestData(data.id);
                                conn.WriteData(data);
                            }
                            catch
                            { 
                                
                            }
                        }
                    }
                }
            }

            dgv_list_forapproval.Rows.Clear();
            LoadForReview_DoWork();
        }

        private List<int> CountItemsForApproval()
        {
            List<int> listOfItemsToSave = new List<int>();

            foreach (DataGridViewRow dr in dgv_list_forapproval.Rows)
            {
                if (dr.Cells[19].Value != null && dr.Cells[0].Value != null)
                {
                    if (dr.Cells[19].Value.ToString() != "Select...")
                    {
                        listOfItemsToSave.Add(Convert.ToInt32(dr.Cells[0].Value));
                    }
                }
            }

            return listOfItemsToSave.Count() > 0 ? listOfItemsToSave : null;
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void dgv_list_forapproval_CellEnter(object sender, DataGridViewCellEventArgs e)
        {
            dgv_list_forapproval.EditMode = DataGridViewEditMode.EditOnEnter;
        }

        private static bool IsDate(string val)
        {
            string strDate = val;
            try
            {
                DateTime dt = DateTime.Parse(strDate);
                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        private bool CheckifTrailingAsset(string investor, string outofREOdate)
        {
            bool isTrailingAsset = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"if (DATEDIFF(day, cast('{1}' as date), cast(GETDATE() as date)) > (select top 1 number_of_days from tbl_EM_property_investors where (investor_code1 = '{0}' or investor_code2 = '{0}'))) 
                                            or (DATEDIFF(day, cast('{1}' as date), cast(GETDATE() as date)) > 160)
                                            begin select 'True' end", investor, outofREOdate);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    isTrailingAsset = true;
                }
                catch
                {
                    isTrailingAsset = false;
                }
            }
            else
            {
                isTrailingAsset = false;
            }

            conn.Close();

            return isTrailingAsset;
        }


        private void SendToDeactivation(string propertycode, string utility, int mode, string outofREODate, string client_code, Int32 id, string usp_name, string AccountNumber, string full_address)
        {
            SqlConnection cnn = new SqlConnection(Constants.connectionString); ;
            SqlCommand cmd;
            string sql = string.Empty;

            if (mode == 1)
            {
                sql += string.Format(@"insert into tbl_EM_DeactivationfromEM (EM_ID, PropertyCode, Utility, DeactivationDate, DeactivatedBy, DateEMValidated) 
                                    select TOP 1 {0}, '{1}', '{2}', PostedDate, PostedBy, GETDATE() from tbl_UC_PropertyDeact where ([Property ID] = '{1}' or [Property ID] = '{1}' + '1') and SUBSTRING(LTRIM(Utility),1,1) = '{2}' and [Status] = 'Deactivated' and DATEDIFF(day,PostedDate,GETDATE()) > 45 order by Posteddate desc  " + Environment.NewLine, id, propertycode, utility);
                sql += string.Format(@"update tbl_UC_PropertyDeact set [Status] = 'Rework', UpdateFlag = 0, UpdatedVMS = 0, VMSPostedDate = NULL where ([Property ID] = '{0}' or [Property ID] = '{0}' + '1') and SUBSTRING(LTRIM(Utility),1,1) = '{1}' and [Status] = 'Deactivated' and DATEDIFF(day,PostedDate,GETDATE()) > 45 ", propertycode, utility);
            }
            else
            {



                if (client_code != "PFC" && (utility == "E" || utility == "G" || utility == "W"))
                {

                    sql += string.Format(@"insert into tbl_EM_DeactivationfromEM (EM_ID, PropertyCode, Utility, DeactivationDate, DateEMValidated) 
                                       values ({0},'{1}','{2}','{3}',GETDATE())", id, propertycode, utility, outofREODate.ToString());

                    string vg = string.Empty;
                    string cl = string.Empty;

                    if (client_code == "OLSR")
                    {
                        cl = "REO";
                    }
                    else
                    {
                        cl = client_code;
                    }

                    if (utility == "E")
                    {
                        vg = "Elec";
                    }
                    else if (utility == "G")
                    {
                        vg = "Gas";
                    }
                    else if (utility == "W")
                    {
                        vg = "Water";
                    }

                    sql += string.Format(@"if (select top 1 [Status] from tbl_UC_PropertyDeact where [Property ID] = '{0}' and Utility = '{1}') = 'Deactivated'
                                        begin
	                                        update tbl_UC_PropertyDeact set [Status] = 'Rework', UpdateFlag = 0, UpdatedVMS = 0, VMSPostedDate = NULL where [Property ID] = '{0}' and Utility = '{1}'
                                        end
                                        else
                                        begin
                                        	
	                                        if (select COUNT(*) from tbl_UC_PropertyDeact where [Property ID] = '{0}' and Utility = '{1}') = 0
	                                        begin
		                                        print 'Insert'
                                        		
		                                        if (select COUNT(*) from tbl_UC_PropertyInFlow where Origin = 'EMS' and PropertyCode = '{0}' and Utility = '{1}' and Uploaded = 0) = 0
                                                    begin
                                                    
                                                    insert into tbl_UC_PropertyInFlow (USPName, PropertyCode, AccountNo, [Address], Category, Task, Utility, Origin, DateUploaded)
                                                    values('{2}', '{0}', '{3}', '{4}', '{5}', 'Deactivation', '{1}', 'EMS', CAST(GETDATE() AS DATE))
                                                    end

                                                update tbl_UC_ParameterReference set REFVALUE = '3' where REFKEY = 'Update_OpenRpt'
	                                        end
                                        end", propertycode, vg, usp_name, AccountNumber, full_address, cl);
                }

            }


            try
            {
                cnn.Open();
                cmd = new SqlCommand(sql, cnn);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                cnn.Close();
                //MessageBox.Show("ExecuteNonQuery in SqlCommand executed !!");
            }
            catch (Exception ex)
            {
            }
        }

        private void dgv_list_forapproval_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            // Ignore if a column or row header is clicked
            if (e.RowIndex != -1 && e.ColumnIndex == 16)
            {
                if (e.Button == MouseButtons.Right)
                {
                    DataGridViewCell clickedCell = (sender as DataGridView).Rows[e.RowIndex].Cells[e.ColumnIndex];

                    // Here you can do whatever you want with the cell
                    this.dgv_list_forapproval.CurrentCell = clickedCell;  // Select the clicked cell, for instance

                    // Get mouse position relative to the vehicles grid
                    var relativeMousePosition = dgv_list_forapproval.PointToClient(Cursor.Position);

                    // Show the context menu
                    this.contextMenuStrip1.Show(dgv_list_forapproval, relativeMousePosition);
                }
            }
        }

        private void toolStripTextBox1_Click(object sender, EventArgs e)
        {
            dgv_list_forapproval.CurrentCell.Value = string.Empty;
            dgv_list_forapproval.CommitEdit(DataGridViewDataErrorContexts.Commit);
            dgv_list_forapproval.Refresh();
            this.contextMenuStrip1.Hide();
        }

        private void cmbType_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgv_list_forapproval.Rows.Clear();
            LoadForReview_DoWork();
        }

        private bool CheckDuplicates(string propertycode, string utility, string amount, string invoice, string client_code, string AccountNumber)
        {
            bool isDuplicate = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

//            string query = String.Format(@"select distinct * from 
//                                            (
//                                            --select distinct a.work_item, CAST(a.ecp_date as datetime) ecp_date
//                                            --from tbl_EM_PayAppWorkItems a
//                                            --where line_item not like '%deposit%'
//                                            --and ((work_item_status in ('Payment Approved', 'Closed') and ecp_date is not null)
//                                            --or (work_item_status in ('Payment Approved', 'Closed') and (ecp_remarks is null or ecp_remarks like '%approve%')) and work_item_action = 1)
//                                            --and a.property_code = '{0}'
//                                            --and a.utility = case when '{6}' in ('T', 'D', 'S') then 'W' else '{6}' end
//                                            --union all
//                                            --select distinct a.work_item, date_bulkuploaded
//                                            --from tbl_ADHOC_EM_Tasks a
//                                            --left join tbl_EM_PayAppWorkItems b
//                                            --on a.work_item = b.work_item
//                                            --where a.work_item <> '' 
//                                            --and a.property_code = '{0}'
//                                            --and a.AccountNumber = '{1}'
//                                            --and Invoice <> '{5}'
//                                            --and b.work_item is null
//
//                                            select distinct a.work_item, CAST(a.ecp_date as datetime) ecp_date
//                                            from tbl_EM_PayAppWorkItems a
//                                            where line_item not like '%deposit%'
//                                            and ((work_item_status in ('Payment Approved', 'Closed') and ecp_date is not null)
//                                            or (work_item_status in ('Payment Approved', 'Closed') and (ecp_remarks is null or ecp_remarks like '%approve%')) and work_item_action = 1)
//                                            and a.property_code = '{0}'
//                                            and a.utility = case when '{6}' in ('T', 'D', 'S') then 'W' else '{6}' end
//                                            union all
//                                            select [Work Order #], [Month] from tbl_EM_InvoiceReconFile a
//                                            left join tbl_EM_PayAppWorkItems b
//                                            on a.[Work Order #] = b.work_item
//                                            where [Loan #] = '{0}'
//                                            and [Line Item Code] = case when '{6}' in ('T', 'D', 'S') then 'Water' when '{6}' = 'G' then 'Gas' else 'Electricity' end
//                                            and b.work_item is null
//
//                                            ) as tbl_dup
//                                            where DATEDIFF(day,ecp_date, GETDATE()) <= 60",
//                                  propertycode, AccountNumber, amount.Replace(",", ""), DateTime.Now, DateTime.Now, invoice, utility);

            string query = String.Format(@"select distinct * from 
                                            (                                            
                                            select work_item [Work Order #], ecp_date from tbl_EM_PayAppWorkItems a
                                            where property_code = '{0}' and work_item_status not in ('Cancelled', 'Cr-Cancel', 'Payment Rejected', 'Rejected') 
                                            and DATEDIFF(day,ecp_date,GETDATE()) <= 45
                                            union all
                                            select 'Pending', GETDATE() from vw_EMS_BulkUploadQuery
                                            where [Property ID] = '{0}'
                                            union all
                                            select 'Pending', GETDATE() from vw_EM_ManualWIQueue
                                            where property_code = '{0}'
                                            union all
                                            select 'Pending', GETDATE()
                                            from dbo.vw_EM_VendorAssignmentQueue a
                                            where a.property_code = '{0}'
                                            union all
                                            select 'Created', a.date_bulkuploaded
                                            from tbl_ADHOC_EM_Tasks a
                                            where a.property_code = '{0}'
                                            and a.PendingReasons like 'Approve for%'
                                            and a.date_bulkuploaded is not null
                                            and DATEDIFF(day, a.date_bulkuploaded,GETDATE()) <= 45
                                            union all
                                            select 'For review', GETDATE()
                                            from tbl_ADHOC_EM_Tasks a
                                            where a.property_code = '{0}'
                                            and a.PendingReasons = 'Pre-work item Review'
                                            and a.isLatestData = 1
                                            ) as tbl_dup",
                                  propertycode, AccountNumber, amount.Replace(",", ""), DateTime.Now, DateTime.Now, invoice, utility);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows && rd[0] != null && rd[0].ToString() != string.Empty && rd[0].ToString() != "")
            {
                try
                {
                    isDuplicate = true;
                }
                catch
                {
                    isDuplicate = false;
                }
            }
            else
            {
                isDuplicate = false;
            }

            conn.Close();

            return isDuplicate;
        }
    }
}
