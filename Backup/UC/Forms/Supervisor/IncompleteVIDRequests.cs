﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace UC.Forms.Supervisor
{
    public partial class IncompleteVIDRequests : Form
    {
        public int id = 0;

        public IncompleteVIDRequests()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void EnableSaveButton()
        {
            bool hasblank = false;

            foreach (Control c in groupBox1.Controls)
            {
                if (c.GetType() == typeof(TextBox))
                {
                    if (((TextBox)c).Text == string.Empty)
                    {
                        hasblank = true;
                    }
                }
            }

            button1.Enabled = !hasblank;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            SaveUpdate();

            LoadSummary();

            LoadRequestForAudit();
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void textBox7_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            EnableSaveButton();
        }

        private void SaveUpdate()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest]
                                           SET [vendor_account_requested] = '{0}'
                                              ,[name] = '{1}'
                                              ,[telephone] = '{2}'
                                              ,[postal_code] = '{3}'
                                              ,[state] = '{4}'
                                              ,[email] = '{5}'
                                              ,[city] = '{6}'
                                              ,[street_name] = '{7}'
                                              ,[status] = 'NEW'
                                              ,[username] = '{8}'
                                              ,[last_date_modified] = CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,GETUTCDATE()),'+08:00'))
                                         WHERE request_id = {9}",
                                               textBox1.Text,
                                               textBox2.Text,
                                               textBox3.Text,
                                               textBox7.Text,
                                               textBox5.Text,
                                               textBox4.Text,
                                               textBox8.Text,
                                               textBox6.Text,
                                               Environment.UserName.ToLower(),
                                               id);

            SqlCommand cmd = new SqlCommand(query, conn);

            try
            {
                int result = cmd.ExecuteNonQuery();

                if (result == 0)
                {
                    MessageBox.Show("Request was not updated.");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
            cmd = null;
            conn.Close();
        }

        private void LoadSummary()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select a.status, COUNT(*) from tbl_EM_VendorIDRequest a
                                            where a.status = 'AUDIT'
                                            group by a.status");

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            dataGridView1.Rows.Clear();

            while (rd.Read())
            {
                dataGridView1.Rows.Add(rd[0], rd[1]);
            }

            rd.Close();
            cmd = null;
            conn.Close();
        }

        private void LoadRequestForAudit()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select top 1 * from tbl_EM_VendorIDRequest a
                                            where a.status = 'AUDIT'
                                            order by last_date_modified");

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                id = (int)rd["request_id"];
                textBox9.Text = rd["username"].ToString();
                textBox10.Text = DateTime.Parse(rd["date_requested"].ToString()).ToShortDateString();
                textBox1.Text = rd["vendor_account_requested"].ToString();
                textBox2.Text = rd["name"].ToString();
                textBox6.Text = rd["street_name"].ToString();
                textBox8.Text = rd["city"].ToString();
                textBox5.Text = rd["state"].ToString();
                textBox7.Text = rd["postal_code"].ToString();
                textBox3.Text = rd["telephone"].ToString();
                textBox4.Text = rd["email"].ToString();
            }
            else
            {
                MessageBox.Show("No more Vendor ID Requests for audit.");
            }
            
            rd.Close();
            cmd = null;
            conn.Close();
        }

        private void IncompleteVIDRequests_Load(object sender, EventArgs e)
        {
            LoadSummary();
            LoadRequestForAudit();
        }

        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) || char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
