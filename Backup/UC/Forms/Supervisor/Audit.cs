﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Reflection;
using System.Net.Mail;
using System.DirectoryServices.AccountManagement;
using System.Net.Mime;
using System.Net;

namespace UC.Forms.Supervisor
{
    public partial class Audit : Form
    {
        BackgroundWorker bwLoad = new BackgroundWorker(), bwLoadInvoiceInformation = new BackgroundWorker(), bwRscApproval = new BackgroundWorker();

        Control[] ctrlsIsBillReadable, ctrlsVendorGroup, ctrlsAccountNumber, ctrlsPropertyDataEntered, ctrlsUspOnInvoice,
                    ctrlsBillingInformation, ctrlsPreviousAmount, ctrlsPaymentReceived, ctrlsCurrentCharge, ctrlsLateFee,
                    ctrlsDisconnectionFee, ctrlsAmountAfterDue, ctrlsNoAmount, ctrlsFinalBill, ctrlsTransAmt, ctrlsInvoiceDate, ctrlsDueDate, ctrlsBillingCycle,
                    ctrlsDisconnectionDate, ctrlsDoNotMail, ctrlsProperty, ctrlsVendor, ctrlsASFI, ctrlsnASFI;

        bool hasDoneAcctSearch = false, hasDoneHouseNoZipSearch = false, hasDoneHouseNoCitySearch = false, hasDoneHouseNoStreetSearch = false,
                hasDoneHouseNoStateSearch = false, HouseNoStateSearchHasResults = false, HouseNoStreetSearchHasResults = false,
                HouseNoCitySearcHasResults = false, HouseNoZipSearchHasResults = false, acctSearchHasResults = false;
        
        DateTime Property_Status_Change_Date = new DateTime(), Inactive_Date = new DateTime();

        bool isOutOfREO = false, isTrailingAsset = false;

        public List<Database.DB.VDR> listOfVDR;

        Timer myTimer = new Timer();

        int duration = 0;


        Boolean initialize = true;
        int iniQueue = 0;
        string iniPropeCode = string.Empty;
        DateTime REODate = new DateTime();

        Database.DBConnection.DatabaseEM_Tasks EM_conn = new Database.DBConnection.DatabaseEM_Tasks();
        public Database.DB.EM_Tasks data = new Database.DB.EM_Tasks();

        public string CurrentLoadedInvoice;
        
        public Audit()
        {
            InitializeComponent();

            ctrlsIsBillReadable = new Control[] { checkBox20, checkBox19 };
            ctrlsVendorGroup = new Control[] { checkBox18, checkBox17, checkBox16, checkBox15 , comboBox1 };
            ctrlsAccountNumber = new Control[] { textBox10, checkBox6 };
            ctrlsASFI = new Control[] { cmbBillName, btnAdd };
            ctrlsnASFI = new Control[] { chkNYes, chknNo };
            ctrlsPropertyDataEntered = new Control[] { textBox4, textBox14, textBox16, textBox17, textBox1, chkbx_noZipCode, chkBx_NoPropertyAddress };
            ctrlsUspOnInvoice = new Control[] { cmboBx_UspOnInvoice };
            ctrlsBillingInformation = new Control[] { dateTimePicker7, dateTimePicker8, dateTimePicker9, dateTimePicker10, dateTimePicker6,
                                                    checkBox30, checkBox31, checkBox32, checkBox27, checkBox28, checkBox29 };
            ctrlsPreviousAmount = new Control[] { textBox20 };
            ctrlsPaymentReceived = new Control[] { textBox21 };
            ctrlsCurrentCharge = new Control[] { textBox23 };
            ctrlsLateFee = new Control[] { textBox6 };
            ctrlsDisconnectionFee = new Control[] { textBox8 };
            ctrlsAmountAfterDue = new Control[] { textBox22 };
            ctrlsNoAmount = new Control[] { checkBox35 };
            ctrlsFinalBill = new Control[] { checkBox33, checkBox34, textBox18 };
            ctrlsTransAmt = new Control[] { chkTransAmtYes, chkTransAmtNo };
            ctrlsInvoiceDate = new Control[] { dateTimePicker9, checkBox30 };
            ctrlsDueDate = new Control[] { dateTimePicker7, checkBox32 };
            ctrlsBillingCycle = new Control[] { dateTimePicker8, dateTimePicker10, checkBox31 };
            ctrlsDisconnectionDate = new Control[] { dateTimePicker6, checkBox27 };
            ctrlsDoNotMail = new Control[] { checkBox28, checkBox29 };
            ctrlsProperty = new Control[] { dataGridView1 };
            ctrlsVendor = new Control[] { comboBox5, checkBox8, dgv_usp, txtUSPCity, txtUSPState, txtUSPZip};

            myTimer.Tick += new EventHandler(myTimer_Tick);
            myTimer.Interval = 1000;
        }

        private void bwRscApproval_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(data.Invoice) &&
                    !string.IsNullOrEmpty(data.property_code) &&
                    !string.IsNullOrEmpty(data.VendorId) &&
                    !string.IsNullOrEmpty(data.Amount) &&
                    !string.IsNullOrEmpty(data.vendor_group) &&
                    !string.IsNullOrEmpty(data.InvoiceFolderName))
                {
                    createRSCapprovalRequest(
                                       data.Invoice,
                                       data.property_code,
                                       data.VendorId,
                                       data.Amount,
                                       data.vendor_group,
                                       data.InvoiceFolderName);
                }
            }
            catch
            {
            }
        }

        private void createRSCapprovalRequest(string invoice, string property_code, string vendor_code, string amount,
            string vendor_type, string invoice_link)
        {
            Database.DBConnection.DatabaseRSCApproval conn = new Database.DBConnection.DatabaseRSCApproval();
            Database.DBConnection.DatabaseREO_Properties conn1 = new Database.DBConnection.DatabaseREO_Properties();
            Database.DBConnection.DatabaseUSP_Contact conn2 = new Database.DBConnection.DatabaseUSP_Contact();
            Database.DBConnection.DatabaseAssetManager conn3 = new Database.DBConnection.DatabaseAssetManager();
            Database.DB.REO_Properties prop_info = new Database.DB.REO_Properties();
            Database.DB.USP_Contact usp_info = new Database.DB.USP_Contact();
            //Database.DB.AssetManager assetMgr = new Database.DB.AssetManager();

            string assetMgr = string.Empty;
            
            prop_info = conn1.GetData(property_code);
            usp_info = conn2.GetUspData(vendor_code);
            //assetMgr = conn3.GetDataProcessor(property_code);

            string full_address, Vendor_Name, property_status, asset_manager;

            try
            {
                //asset_manager = assetMgr.asset_manager;
                assetMgr = prop_info.asset_manager;
            }
            catch
            {
                assetMgr = string.Empty;
            }

            try
            {
                full_address = prop_info.full_address;
            }
            catch
            {
                full_address = string.Empty;
            }

            try
            {
                Vendor_Name = usp_info.Name;
            }
            catch
            {
                Vendor_Name = string.Empty;
            }
            try
            {
                property_status = prop_info.property_status;
            }
            catch
            {
                property_status = string.Empty;
            }

            try
            {

                Double finamount = 0;

                try
                {
                    finamount = Convert.ToDouble(amount);
                }
                catch
                {
                    finamount = 0;
                }

                conn.WriteData(new Database.DB.RSCApproval()
                {
                    amount = finamount,
                    created_by = Environment.UserName,
                    date_created = DateTime.Now,
                    date_modified = DateTime.Now,
                    full_address = full_address,
                    invoice = invoice,
                    modified_by = Environment.UserName,
                    notes = "New Request " + DateTime.Now.ToShortDateString(),
                    property_code = property_code,
                    status = "Pending",
                    vendor_code = vendor_code,
                    vendor_name = Vendor_Name,
                    vendor_type = vendor_type,
                    property_status = property_status,
                    invoice_link = invoice_link,
                    asset_manager = assetMgr
                });
            }
            catch
            {
            }
        }

        private void bwGetVdrList_DoWork(object sender, DoWorkEventArgs e)
        {
            Database.DBConnection.DatabaseVDR_Data conn = new Database.DBConnection.DatabaseVDR_Data();
            listOfVDR = conn.GetAllData();

            comboBox5.Invoke((Action)delegate
            {
                comboBox5.Items.Clear();
                comboBox5.Items.Add("--------------------------------------------");
                comboBox5.Items.Add("No Matching Biller Name");
                comboBox5.Items.Add("--------------------------------------------");
            });

            if (listOfVDR != null)
            {
                foreach (Database.DB.VDR data in listOfVDR)
                {
                    comboBox5.Invoke((Action)delegate
                    {
                        if (!comboBox5.Items.Contains(data.Vendor_Name))
                        {
                            comboBox5.Items.Add(data.Vendor_Name.Replace("'", ""));
                        }
                    });
                }

                comboBox5.Invoke((Action)delegate
                {
                    comboBox5.Items.Add("--------------------------------------------");
                    comboBox5.Items.Add("No Matching Biller Name");
                    comboBox5.Items.Add("--------------------------------------------");
                });
            }
        }

        private void bwLoadUSP_DoWork(object sender, DoWorkEventArgs e)
        {
            preloadUSP();
        }

        public void preloadUSP()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);
            conn.Open();

            string query = @"SELECT RTRIM(USPGroupName) [USP Name] FROM [MIS_ALTI].[dbo].[tbl_UC_USPGroup] ORDER BY [USP Name]";

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            if (cmboBx_UspOnInvoice.InvokeRequired)
            {
                cmboBx_UspOnInvoice.Invoke((Action)delegate
                {
                    cmboBx_UspOnInvoice.Items.Clear();
                    cmboBx_UspOnInvoice.Items.Add("----------------------------");
                    cmboBx_UspOnInvoice.Items.Add("USP Not Listed");
                    cmboBx_UspOnInvoice.Items.Add("----------------------------");

                    try
                    {
                        while (rd.Read())
                        {
                            cmboBx_UspOnInvoice.Items.Add(rd[0].ToString());
                        }

                        cmboBx_UspOnInvoice.Items.Add("----------------------------");
                        cmboBx_UspOnInvoice.Items.Add("USP Not Listed");
                        cmboBx_UspOnInvoice.Items.Add("----------------------------");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Unable to pull up list of USP. Error: " + ex.Message);
                    }
                });
            }
            else
            {
                cmboBx_UspOnInvoice.Items.Clear();
                cmboBx_UspOnInvoice.Items.Add("----------------------------");
                cmboBx_UspOnInvoice.Items.Add("USP Not Listed");
                cmboBx_UspOnInvoice.Items.Add("----------------------------");

                try
                {
                    while (rd.Read())
                    {
                        cmboBx_UspOnInvoice.Items.Add(rd[0].ToString());
                    }

                    cmboBx_UspOnInvoice.Items.Add("----------------------------");
                    cmboBx_UspOnInvoice.Items.Add("USP Not Listed");
                    cmboBx_UspOnInvoice.Items.Add("----------------------------");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to pull up list of USP. Error: " + ex.Message);
                }
            }

            conn.Close();
        }

        public void myTimer_Tick(object sender, EventArgs e)
        {
            duration++;
            label3.Text = duration.ToString();
        }

        public void LoadForAudit()
        {
            int id = 0, lockeddata = 0, count = 0;

            id = GetDataForAudit();

            if (id != 0)
            {
                while (lockeddata == 0 && count < 5)
                {
                    id = GetDataForAudit();

                    lockeddata = LockDataForAudit(id);

                    if (lockeddata != 0)
                    {
                        LoadLockedData(id);        
                    }

                    count++;
                }
            }
            else
            {
                MessageBox.Show("There are no data for audit");

                //this.Close();
            }
        }

        public int GetDataForAudit()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query1 = string.Format(@"select top 50 a.* from tbl_ADHOC_EM_Tasks a
                                            left join tbl_EM_MPDUSP b
                                            on a.VendorId = b.VendorID
                                            where a.isLatestData = 1
                                            and a.Complete = (select top 1 case when current_queue = 'QA 1' then 21 when current_queue = 'QA 2' then 22 else 99 end from tbl_EM_Queue where userid = '{0}')
                                            and a.lockedto = '{0}'
                                            and a.LastDateModified > '2015-04-01' and a.userid <> '{0}'
                                            and a.Invoice <> '' 
                                            and a.InvoiceFolderName <> '' 
                                            order by ISNULL(b.isMPD, 0) desc, a.LastDateModified desc"
                //, "cruremie");
                                            , Environment.UserName.ToLower());


            string query = string.Format(@"select top 50 a.* from tbl_ADHOC_EM_Tasks a
                                            left join tbl_EM_MPDUSP b
                                            on a.VendorId = b.VendorID
                                            where a.isLatestData = 1
                                            and a.Complete = (select top 1 case when current_queue = 'QA 1' then 21 when current_queue = 'QA 2' then 22 else 99 end from tbl_EM_Queue where userid = '{0}')
                                            and (a.lockedto in ('0','') OR a.lockedto is null OR a.lockedto = '{0}')
                                            and a.LastDateModified > '2015-04-01' and a.userid <> '{0}' and isnull(a.auditedby, '') <> '{0}'
                                            and a.complete not in (31,32) 
                                            --and (a.PendingReasons like '%Manual WI%' or a.PendingReasons like '%Bulk Upload%')
                                            and a.Invoice <> '' 
                                            and a.InvoiceFolderName <> '' 
                                            order by ISNULL(b.isMPD, 0) desc, a.LastDateModified desc"
                //, "cruremie");
                                            , Environment.UserName.ToLower());


//            string query1 = string.Format(@"select top 50 * from tbl_ADHOC_EM_Tasks a
//                                            where a.id in (1990239)"
//                                            , Environment.UserName.ToLower());


            
            SqlCommand cmd1 = new SqlCommand(query1, conn);

            SqlDataReader rd = cmd1.ExecuteReader();

            int id = 0;

            if (rd.HasRows == true)
            {

                while (rd.Read() && id == 0)
                {
                    string invoicelink = rd["InvoiceFolderName"].ToString();

                    if (System.IO.File.Exists(invoicelink))
                    {
                        id = (int)rd[0];
                    }
                }
            }
            else
            {

                conn.Close();

                conn.Open();

                SqlCommand cmd = new SqlCommand(query, conn);

                rd = cmd.ExecuteReader();

                while (rd.Read() && id == 0)
                {
                    string invoicelink = rd["InvoiceFolderName"].ToString();

                    if (System.IO.File.Exists(invoicelink))
                    {
                        id = (int)rd[0];
                    }
                }

                conn.Close();
            }          

            
            return id;
        }

        public bool isTempUser(string user)
        {

            bool userisnew = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select * from tbl_EM_tempUsers where isActive = 1 and userid = '{0}'", user);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            if (rd.HasRows == true)
            {
                userisnew = true;
            }
            else
            {
                userisnew = false;
            }
            
            conn.Close();

            return userisnew;
        }


        public int LockDataForAudit(int id)
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"update tbl_ADHOC_EM_Tasks
                                            set lockedto = '{0}'
                                            where id = {1}
                                            and isLatestData = 1
                                            and Complete = (select top 1 case when current_queue = 'QA 1' then 21 when current_queue = 'QA 2' then 22 else 99 end from tbl_EM_Queue where userid = '{0}')
                                            and (lockedto in ('0','') OR lockedto is null OR lockedto = '{2}')",
                                            Environment.UserName.ToLower(),
                                            id,
                                            Environment.UserName.ToLower());

//            string query = string.Format(@"update tbl_ADHOC_EM_Tasks
//                                            set lockedto = '{0}'
//                                            where id = {1}",

//                                            Environment.UserName.ToLower(),
//                                            id,
//                                            Environment.UserName.ToLower());

            SqlCommand cmd = new SqlCommand(query, conn);

            int rowsAffected = cmd.ExecuteNonQuery();

            conn.Close();

            return rowsAffected;
        }

        public void LoadLockedData(int id)
        {
            hasDoneAcctSearch = false; hasDoneHouseNoZipSearch = false; hasDoneHouseNoCitySearch = false; hasDoneHouseNoStreetSearch = false;
                hasDoneHouseNoStateSearch = false; HouseNoStateSearchHasResults = false; HouseNoStreetSearchHasResults = false;
                HouseNoCitySearcHasResults = false; HouseNoZipSearchHasResults = false; acctSearchHasResults = false;


                Control[] ControlsToRefresh = new Control[] { pct_isbillreadablecheck, pictureBox24, pictureBox26, pictureBox35, pictureBox63, pictureBox15,
                    pictureBox49, pictureBox52, pictureBox55, pictureBox58, pictureBox9, pictureBox26, pictureBox8, pictureBox18, pictureBox21, pictureBox25,
                    pictureBox39, pictureBox33, pictureBox40, pictureBox43, pictureBox12, pictureBox70, pictureBox64, button2 };

                foreach (Control c in ControlsToRefresh)
                {
                    c.Visible = true;
                    c.Enabled = true;
                }


                UpdateReport();

                dataGridView1.Rows.Clear();
                dgv_usp.Rows.Clear();

            Property_Status_Change_Date = new DateTime(); Inactive_Date = new DateTime();

            isOutOfREO = false; isTrailingAsset = false;

            duration = 0;
            myTimer.Start();

            data = new Database.DB.EM_Tasks();
            data = EM_conn.GetDataFromId((uint)id);

            if (data == null)
            {

                ResetControl(ctrlsIsBillReadable);
                ResetControl(ctrlsVendorGroup);
                ResetControl(ctrlsPropertyDataEntered);
            }
            else
            {

                iniQueue = data.Complete;

                //Reset Timer
                myTimer.Start();

               // Load PDF
                if (System.IO.File.Exists(data.InvoiceFolderName))
                {
                    CurrentLoadedInvoice = data.InvoiceFolderName;

                    webBrowser1.Navigate(CurrentLoadedInvoice);
                }
                else
                {
                    string tempFileName = string.Format("{0}{1}.pdf", Constants.TempFolder, data.Invoice);
                    CurrentLoadedInvoice = tempFileName;
                    if (System.IO.File.Exists(CurrentLoadedInvoice))
                    {
                        webBrowser1.Navigate(CurrentLoadedInvoice);
                    }
                }

                // Is this bill readable?
                ResetControl(ctrlsIsBillReadable);

                if (data.PendingReasons.ToLower().Contains(@"blank/unreadable/cut invoice"))
                {
                    checkBox19.Checked = true;
                    checkBox20.Checked = false;
                }
                else
                {
                    checkBox19.Checked = false;
                    checkBox20.Checked = true;
                }

                // Is this an electricity, gas or water bill?
                ResetControl(ctrlsVendorGroup);
                switch (data.vendor_group)
                {
                    case "E": checkBox18.Checked = true; break;
                    case "W": checkBox16.Checked = true; break;
                    case "G": checkBox17.Checked = true; break;
                    case "T": checkBox15.Checked = true; comboBox1.Text = "Garbage/Trash/Disposal"; break;
                    case "D": checkBox15.Checked = true; comboBox1.Text = "Stormwater/Drainage/WasteWater/Sewer"; break;
                    case "S": checkBox15.Checked = true; comboBox1.Text = "Septic"; break;
                    case "None": checkBox15.Checked = true; comboBox1.Text = data.PendingReasons; break; 
                }

                
                //ASFI
                if (data.isAddressedToASFI == true)
                {
                    cmbBillName.Text = "ASFI or c/o ASFI";
                    chkNYes.Checked = true;
                    chknNo.Checked = false;

                    if (data.Invoice.ToString().Contains("Escalation") == true)
                    {
                        pictureBox30_Click(null, null);
                        pictureBox30.Enabled = true;
                    }
                }
                else
                {

                    cmbBillName.Text = data.BillTo;
                    
                    chkNYes.Checked = false;
                    chknNo.Checked = true;
                }               
                
                
                // Account Number
                ResetControl(ctrlsAccountNumber);
                textBox10.Text = data.AccountNumber;

                //if (textBox10.Text == "None")
                //{
                //    checkBox6.Checked = true;
                //}

                txtTransferAccount.Text = data.InvoiceCount.ToString();

                // Property Data Entered
                ResetControl(ctrlsPropertyDataEntered);
                textBox4.Text = data.HouseNoEntered;
                textBox14.Text = data.StreetEntered;
                textBox16.Text = data.CityEntered;
                textBox17.Text = data.StateEntered;
                textBox1.Text = data.ZipcodeEntered;

                //Agent name
                //label20.Text = data.userid;

                //Special Instruction
                textBox9.Text = string.Empty;
                grpBox_SpecialInstructions.Visible = false;
                if (data.SpecialInstruction != string.Empty)
                {
                    grpBox_SpecialInstructions.Visible = true;
                    textBox9.Text = data.SpecialInstruction;
                }
                
                // Billing Information
                ResetControl(ctrlsBillingInformation);
                if (data.invoice_date == DateTime.MinValue)
                {
                    checkBox30.Checked = true;
                    checkBox30.Enabled = false;
                    dateTimePicker9.Value = DateTime.Now.Date;
                    dateTimePicker9.Enabled = false;
                }
                else
                {
                    checkBox30.Checked = false;
                    checkBox30.Enabled = false;
                    dateTimePicker9.Value = data.invoice_date;
                    dateTimePicker9.Enabled = false;
                }

                if (data.DueDate == DateTime.MinValue)
                {
                    checkBox32.Checked = true;
                    checkBox32.Enabled = false;
                    dateTimePicker7.Value = DateTime.Today.Date;
                    dateTimePicker7.Enabled = false;
                }
                else
                {
                    checkBox32.Checked = false;
                    checkBox32.Enabled = false;
                    dateTimePicker7.Value = data.DueDate;
                    dateTimePicker7.Enabled = false;
                }

                if (data.ServiceFrom == DateTime.MinValue && data.ServiceTo == DateTime.MinValue)
                {
                    checkBox31.Checked = true;
                    checkBox31.Enabled = false;
                    dateTimePicker8.Value = DateTime.Today.Date;
                    dateTimePicker8.Enabled = false;
                    dateTimePicker10.Value = DateTime.Today.Date;
                    dateTimePicker10.Enabled = false;
                }
                else
                {
                    checkBox31.Checked = false;
                    checkBox31.Enabled = false;
                    dateTimePicker8.Value = data.ServiceFrom;
                    dateTimePicker8.Enabled = false;
                    dateTimePicker10.MinDate = data.ServiceFrom;

                    try
                    {
                        dateTimePicker10.Value = data.ServiceTo;
                    }
                    catch
                    {
                        dateTimePicker10.Value = DateTime.Now.Date;
                    }
                    
                    dateTimePicker10.Enabled = false;
                }
                
                if (data.DisconnectionDate == DateTime.MinValue)
                {
                    dateTimePicker6.Value = DateTime.Now.Date;
                    dateTimePicker6.Enabled = false;
                    checkBox27.Checked = false;
                    checkBox27.Enabled = false;
                }
                else
                {
                    dateTimePicker6.Value = data.DisconnectionDate;
                    dateTimePicker6.Enabled = false;
                    checkBox27.Checked = true;
                    checkBox27.Enabled = false;
                }

                if (data.PendingReasons == "Do Not Mail")
                {
                    checkBox29.Checked = true;
                    checkBox28.Checked = false;
                }
                else
                {
                    checkBox29.Checked = false;
                    checkBox28.Checked = true;
                }

                // Payment Breakdown
                ResetControl(ctrlsPreviousAmount);
                ResetControl(ctrlsPaymentReceived);
                ResetControl(ctrlsCurrentCharge);
                ResetControl(ctrlsLateFee);
                ResetControl(ctrlsDisconnectionFee);
                ResetControl(ctrlsAmountAfterDue);
                ResetControl(ctrlsNoAmount);
                ResetControl(ctrlsFinalBill);

                if (data.PendingReasons == "No Amount")
                {
                    checkBox35.Checked = true;
                }
                else
                {
                    checkBox35.Checked = false;
                }

                textBox20.Text = string.Format("{0:N2}", data.PreviousBalance);
                textBox21.Text = string.Format("{0:N2}", data.PaymentReceived);
                textBox23.Text = string.Format("{0:N2}", data.CurrentCharge);
                textBox6.Text = string.Format("{0:N2}", data.LateFee);
                textBox8.Text = string.Format("{0:N2}", data.DisconnectionFee);

                //----------------------------------
                // Update Start: 6/16/2015 5:50 AM
                // Desc: Removed Amount Before Due
                //----------------------------------
                //if (data.AmountAfterDue == 0)
                //{
                //    textBox24.Text = data.Amount;
                //}
                //else
                //{
                //    textBox24.Text = string.Format("{0:N2}", data.AmountAfterDue);
                //    textBox22.Text = data.Amount;
                //}

                if (data.AmountBeforeDue != 0)
                {
                    textBox24.Text = string.Format("{0:N2}", data.AmountBeforeDue);
                    textBox22.Text = data.Amount;

                    if (data.Amount == "0.00" ||  data.Amount == string.Empty)
                    {
                        ComputeAmount(textBox23, null);
                        textBox24.Text = data.Amount;
                    }
                }
                else
                {
                    textBox24.Text = data.Amount;

                    if (data.Amount == "0.00" || data.Amount == string.Empty)
                    {
                        ComputeAmount(textBox23, null);
                        textBox24.Text = data.Amount;
                    }
                }

                if (data.AmountAfterDue != 0)
                {
                    textBox22.Text = data.AmountAfterDue.ToString();
                }
                

                textBox24.Text = data.Amount;
                // Updte End: 6/16/2015 5:51 AM

                if (data.final_bill == "Yes")
                {
                    checkBox34.Checked = true;
                    checkBox33.Checked = false;
                }
                else
                {
                    checkBox34.Checked = false;
                    checkBox33.Checked = true;
                }


                if (data.Invoice.Contains("TRANSFER"))
                {
                    chkTransAmtYes.Checked = true;
                    chkTransAmtNo.Checked = false;
                }
                else
                {
                    chkTransAmtYes.Checked = false;
                    chkTransAmtNo.Checked = true;
                }

                
                // Property 
                if (data.property_code != string.Empty)
                {
                    Database.DBConnection.DatabaseREO_Properties prop_conn = new Database.DBConnection.DatabaseREO_Properties();
                    Database.DB.REO_Properties reo_prop = prop_conn.GetData(data.property_code);
                    
                    if (reo_prop != null)
                    {
                        DisplayPropertyInDGV(reo_prop, data.vendor_group);

                        chkBx_NoPropertyAddress.Checked = false;
                    }
                }
                else
                {
                    if (data.PendingReasons == "No Property/Service Address")
                    {
                        chkBx_NoPropertyAddress.Checked = true;
                        cmboBx_UspOnInvoice.Text = data.usp_name;
                    }
                    else
                    {
                        chkBx_NoPropertyAddress.Checked = false;
                        cmboBx_UspOnInvoice.Text = string.Empty;
                    }
                }


                // Vendor
                if (data.VendorId != string.Empty)
                {
                    Database.DBConnection.DatabaseVDR_Data usp_conn = new Database.DBConnection.DatabaseVDR_Data();
                    Database.DB.VDR usp = usp_conn.GetDataProcessor(data.VendorId);

                    Database.DBConnection.DatabaseVendorIdRequest usp_conn2 = new Database.DBConnection.DatabaseVendorIdRequest();
                    Database.DB.VendorIdRequest usp2 = usp_conn2.GetDataProcessor(data.VendorId);

                    if (usp != null)
                    {
                        LoadVendorInDGV(usp);

                        checkBox8.Checked = false;
                    }
                    else
                    {
                        if (usp2 != null)
                        {
                            LoadVendorInDGV(usp);

                            checkBox8.Checked = false;

                            data.Complete = 9;
                        }
                    }
                }
                else
                {
                    if (data.PendingReasons == "No USP Address on Invoice")
                    {
                        checkBox8.Checked = true;
                    }
                    else
                    {
                        checkBox8.Checked = false;

                        if (data.usp_name != string.Empty)
                        {
                            dgv_usp.Rows.Add(null, "NO DATA FOUND", null, null);
                        }
                        else
                        {
                            dgv_usp.Rows.Add(null, "NO DATA FOUND", null, null);
                        }
                    }
                }

                if (data.PendingReasons == "Do Not Mail")
                {
                    checkBox29.Checked = true;
                }
                else
                {
                    checkBox29.Checked = false;
                }

                initialize = false;
            }
        }

        //-----------------
        // OLD FUNCTIONS
        //-----------------
        private void ComputeAmount(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;

            string value = tb.Text;
            char[] delimiters = new char[] { '+', '-' };
            string[] parts = value.Split(delimiters);
            List<string> listOfsigns = new List<string>();

            foreach (char ch in value)
            {
                if (ch == '+' || ch == '-')
                {
                    listOfsigns.Add(ch.ToString());
                }
            }

            string[] signs = listOfsigns.ToArray();

            try
            {
                decimal amount = Convert.ToDecimal(parts[0]);

                if (parts.Count() > 1)
                {
                    for (int i = 1; i < parts.Count(); i++)
                    {
                        switch (signs[i - 1])
                        {
                            case "+":
                                amount += Convert.ToDecimal(parts[i]);
                                break;

                            case "-":
                                amount -= Convert.ToDecimal(parts[i]);
                                break;
                        }
                    }
                    tb.Text = string.Format("{0:N2}", amount);
                }
            }
            catch
            {
                MessageBox.Show(string.Format("{0} is in incorrect format", tb.Text));
                tb.Text = string.Empty;
            }
        }

        private void DisplayPropertyInDGV(Database.DB.REO_Properties p, string vendor_group)
        {
            Database.DBConnection.DatabaseAccountNumber conn = new Database.DBConnection.DatabaseAccountNumber();
            List<Database.DB.AccountNumber> acctNos = new List<Database.DB.AccountNumber>();
            acctNos = conn.GetAccountNumbers(p.property_code, vendor_group);

            string strAcctNos = string.Empty;

            try
            {
                if (acctNos != null)
                {
                    foreach (Database.DB.AccountNumber acct in acctNos)
                    {
                        if (strAcctNos == string.Empty)
                        {
                            strAcctNos = acct.account_number;
                        }
                        else
                        {
                            strAcctNos = strAcctNos + ", " + acct.account_number;
                        }
                    }
                }

                dataGridView1.Invoke((Action)delegate
                {
                    bool display = true;

                    foreach (DataGridViewRow dr in dataGridView1.Rows)
                    {
                        if (dr.Cells[0].Value.ToString().Trim() == p.property_code &&
                            dr.Cells[1].Value.ToString().Trim() == p.full_address &&
                            dr.Cells[3].Value.ToString().Trim() == p.active)
                        {
                            display = false;
                        }
                    }

                    if (display)
                    {
                        dataGridView1.Rows.Add(
                            p.property_code,
                            p.full_address,
                            p.customer_name,
                            p.active,
                            p.investor_name,
                            p.reosrc_date,
                            p.reoslc_date,
                            p.reosfc_date,
                            strAcctNos,
                            p.pod,
                            p.property_status_change_date,
                            p.inactive_date
                            );

                        iniPropeCode = p.property_code;
                    }
                });
            }
            catch
            {
                // do nothing
            }
        }

        private void LoadVendorInDGV(Database.DB.VDR v)
        {
            Database.DBConnection.DatabaseVDR_Data conn = new Database.DBConnection.DatabaseVDR_Data();
            Database.DB.VDR vdrData = conn.GetDataProcessor(data.VendorId);

            Database.DBConnection.DatabaseVendorIdRequest conn2 = new Database.DBConnection.DatabaseVendorIdRequest();
            Database.DB.VendorIdRequest vidrData = conn2.GetDataProcessor(data.VendorId);

            dgv_usp.Visible = true;
            dgv_usp.Rows.Clear();

            if (vdrData != null)
            {
                dgv_usp.Rows.Add(vdrData.Vendor_ID, vdrData.Vendor_Name, vdrData.Address, vdrData.vdr_status == 1 ? "Active" : "Inactive", GetStatus(vdrData.vms_status.ToString()));
            }
            else
            {
                if (vidrData != null)
                {
                    dgv_usp.Rows.Add(vidrData.request_id, vidrData.name, vidrData.street_name + ", " + vidrData.city + ", " + vidrData.state + ", " + vidrData.postal_code, "Pending", "Pending");
                }
                else
                {
                    dgv_usp.Rows.Add(null, "NO DATA FOUND", null, null);
                }
            }

           
        }

        private void SaveButtonsEnabled(bool isEnabled)
        {
            button2.Enabled = isEnabled;
        }

        private void DisplayPropertyInDGV(Database.DB.RRProperties p)
        {
            try
            {
                dataGridView1.Rows.Add(p.property_id, p.street_name, "RR", "Active", string.Empty, null, null, null, null, null, null);
            }
            catch
            {
                // do nothing
            }
        }

        private bool SearchPropertyUsingAddress(string houseNo, string streetName, string cityName, string stateName, string zipcodeNumber)
        {
            Database.DBConnection.DatabaseREO_Properties conn = new Database.DBConnection.DatabaseREO_Properties();
            Database.DBConnection.DatabaseRRProperties conn1 = new Database.DBConnection.DatabaseRRProperties();
            List<Database.DB.REO_Properties> listOfProperties = new List<Database.DB.REO_Properties>();
            List<Database.DB.RRProperties> listOfRRProperties = new List<Database.DB.RRProperties>();
            List<string> listOfDisplayedProperties = new List<string>();

            //----------------------------------------------
            //          Remove 0 from zipcode
            //----------------------------------------------
            if (zipcodeNumber != string.Empty)
            {
                if (zipcodeNumber.StartsWith("0"))
                {
                    zipcodeNumber = textBox1.Text.Substring(1);
                }
                else
                {
                    zipcodeNumber = textBox1.Text;
                }
            }

            try
            {
                listOfProperties = conn.SearchActiveProperty(houseNo, streetName, cityName, stateName, zipcodeNumber);
                listOfRRProperties = conn1.SearchProperty(houseNo, streetName, cityName, zipcodeNumber);
            }
            catch
            {
                listOfProperties = null;
                listOfRRProperties = null;
            }

            bool result = false;

            //----------------------------------------------------
            //          Enable property list controls
            //----------------------------------------------------
            groupBox1.Visible = true;
            groupBox1.Enabled = true;
            dataGridView1.Visible = true;
            dataGridView1.Enabled = true;

            // Clear the properties datagridview
            dataGridView1.Rows.Clear();

            if (listOfProperties != null)
            {
                foreach (Database.DB.REO_Properties p in listOfProperties)
                {
                    if (listOfDisplayedProperties.Count(s => s == p.full_address) == 0)
                    {
                        DisplayPropertyInDGV(p, data.vendor_group);

                        listOfDisplayedProperties.Add(p.full_address);

                        result = true;
                    }
                }

                if (listOfRRProperties != null)
                {
                    foreach (Database.DB.RRProperties r in listOfRRProperties)
                    {
                        DisplayPropertyInDGV(r);
                        result = true;
                    }
                }
            }
            else if (listOfRRProperties != null)
            {
                if (listOfProperties == null)
                {
                    foreach (Database.DB.RRProperties r in listOfRRProperties)
                    {
                        DisplayPropertyInDGV(r);
                        result = true;
                    }
                }
                else
                {
                    try
                    {
                        dataGridView1.Rows.Add(
                            null,
                            "NO PROPERTY FOUND",
                            null,
                            null,
                            null,
                            null,
                            null,
                            null
                            );

                        result = false;
                    }
                    catch
                    { }
                }
            }
            else
            {
                try
                {
                    dataGridView1.Rows.Add(
                        null,
                        "NO PROPERTY FOUND",
                        null,
                        null,
                        null,
                        null,
                        null,
                        null
                        );

                    result = false;
                }
                catch
                { }
            }

            if (result == true)
            {
                button6.Visible = true;
            }
            else
            {
                button6.Visible = false;
            }

            return result;
        }

        private void ResetPropertyInformation()
        {
            foreach (Control c in groupBox8.Controls)
            {
                if (c.GetType() == typeof(TextBox) || c.GetType() == typeof(ComboBox))
                {
                    c.Text = string.Empty;
                }
            }
        }

        private void DisplayPropertyInDGV(Database.DB.REO_Properties p, Database.DB.AccountNumber a)
        {
            try
            {
                dataGridView1.Rows.Add(
                    p.property_code,
                    p.full_address,
                    p.customer_name,
                    p.active,
                    p.investor_name,
                    p.reosrc_date,
                    p.reoslc_date,
                    p.reosfc_date,
                    a.account_number,
                    p.pod,
                    p.property_status_change_date,
                    p.inactive_date
                    );
            }
            catch
            {
                // do nothing
            }
        }

        private void SearchPropertyUsingAccountNumber()
        {
            Database.DBConnection.DatabaseAccountNumber conn = new Database.DBConnection.DatabaseAccountNumber();
            Database.DBConnection.DatabaseREO_Properties conn1 = new Database.DBConnection.DatabaseREO_Properties();
            List<string> listOfPropertyCodes = new List<string>();
            List<Database.DB.REO_Properties> listOfPropDisplay = new List<Database.DB.REO_Properties>();
            List<Database.DB.AccountNumber> listOfAcctProp = new List<Database.DB.AccountNumber>();

            bool ShowData = true;

            // Clear the properties datagridview
            dataGridView1.Rows.Clear();

            if (textBox10.Text != string.Empty)
            {
                try { listOfAcctProp = conn.GetDataProcessor(textBox10.Text.Trim(), data.vendor_group); }
                catch (Exception ex)
                {
                    listOfAcctProp = null;
                    MessageBox.Show("An error occurred when retrieving data for account numbers. " +
                        Environment.NewLine + "Error: " + ex.Message);
                }
            }

            if (listOfAcctProp != null)
            {
                dataGridView1.Rows.Clear();

                foreach (Database.DB.AccountNumber acctData in listOfAcctProp)
                {
                    Database.DB.REO_Properties propInfo = new Database.DB.REO_Properties();
                    propInfo = conn1.GetData(acctData.property_code);

                    if (propInfo != null)
                    {
                        listOfPropDisplay.Add(propInfo);
                    }
                }

                foreach (Database.DB.REO_Properties propInfo in listOfPropDisplay)
                {
                    if (listOfPropDisplay.Count(p => p.full_address == propInfo.full_address) > 1)
                    {
                        if (propInfo.active.Trim() == "Active")
                        {
                            ShowData = true;
                        }
                        else
                        {
                            ShowData = false;
                        }
                    }
                    else
                    {
                        ShowData = true;
                    }

                    if (ShowData == true)
                    {
                        DisplayPropertyInDGV(propInfo, listOfAcctProp.First(p => p.property_code == propInfo.property_code));

                        acctSearchHasResults = true;
                    }
                }
            }
            else
            {
                acctSearchHasResults = false;

                groupBox1.Visible = true;
                dataGridView1.Rows.Clear();
                dataGridView1.Rows.Add(null, "NO PROPERTY FOUND", null, null, null, null, null, null);
                dataGridView1.Enabled = false;
            }
            hasDoneAcctSearch = true;
        }

        private bool CheckIfTrailingAsset(string investor_code, double numberOfDaysOREO)
        {
            Database.DBConnection.DatabaseProperty_Investors conn = new Database.DBConnection.DatabaseProperty_Investors();
            Database.DB.Property_Investors data = new Database.DB.Property_Investors();

            try
            {
                data = conn.GetDataProcessor(investor_code);
                if (data != null)
                {
                    float RequiredNumberOfDays = data.number_of_days;

                    if (numberOfDaysOREO > RequiredNumberOfDays)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else { return false; }
            }
            catch
            {
                return false;
            }
        }

        public void CheckIfOutOfREO(int RowIndex)
        {
            DateTime rcDate = new DateTime();

            DateTime reosrcDate = (DateTime)dataGridView1.Rows[RowIndex].Cells["cReosrcDate"].Value;
            DateTime reoslcDate = (DateTime)dataGridView1.Rows[RowIndex].Cells["cReoslcDate"].Value;
            DateTime reosfcDate = (DateTime)dataGridView1.Rows[RowIndex].Cells["cReosfcDate"].Value;
            string investorName = dataGridView1.Rows[RowIndex].Cells["cInvestorName"].Value.ToString().ToString();

            if (reosrcDate != DateTime.MinValue)
            {
                rcDate = reosrcDate;
            }

            if (reoslcDate != DateTime.MinValue)
            {
                rcDate = reoslcDate;
            }

            if (reosfcDate != DateTime.MinValue)
            {
                rcDate = reosfcDate;
            }

            if (rcDate == null || rcDate == DateTime.MinValue)
            {
                isOutOfREO = false;
            }
            else
            {
                TimeSpan OutOfREO = DateTime.Today.Date - rcDate;

                // Check if property is a trailing asset
                isTrailingAsset = false;
                isTrailingAsset = CheckIfTrailingAsset(investorName, OutOfREO.Days);

                if (isTrailingAsset == true)
                {
                    isOutOfREO = true;
                }
                else if (OutOfREO.Days > 45)
                {
                    //--------------------------------------
                    // Need process flow for OREO > 45 days
                    //--------------------------------------
                    isOutOfREO = true;
                }
                else
                {
                    isOutOfREO = false;
                }
            }
        }

        private void dgv_usp_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) { return; }

            dgv_usp.Enabled = false;

            var result = MessageBox.Show("Is this the correct match? Please check the invoice.",
                "Utility Service Provider", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.No)
            {
                MessageBox.Show("Please choose the appropriate USP or just click on no match found",
                    "Utility Service Provider", MessageBoxButtons.OK, MessageBoxIcon.Information);

                dgv_usp.Rows.Clear();
                panel1.Visible = true;
                comboBox5.DroppedDown = true;
                label4.Visible = true;
                button_noMatchUSP.Visible = true;
                btnSearchUSP.Visible = true;
            }
            else
            {
                data.usp_name = dgv_usp.Rows[e.RowIndex].Cells[1].Value.ToString();
                data.VendorId = dgv_usp.Rows[e.RowIndex].Cells[0].Value.ToString();
                data.vendor_address = dgv_usp.Rows[e.RowIndex].Cells[2].Value.ToString();

                Database.DB.VDR vendor_data = listOfVDR.First(p => p.Vendor_ID == data.VendorId);

                if (vendor_data != null)
                {
                    //if (vendor_data.isActive == true)
                    //{
                    //    if (data.Complete != 9)
                    //    {
                    //        data.VendorAssignedStatus = "New";
                    //    }
                    //}
                    //else
                    //{
                    //    Form vendorIdRequestForm = new Forms.VendorIdRequest(this, "ACTIVATION");
                    //    vendorIdRequestForm.Show();
                    //}

                    if (vendor_data.vdr_status == 0)
                    {
                        //Form vendorIdRequestForm = new Forms.VendorIdRequest(this, "ACTIVATION");
                        //vendorIdRequestForm.Show();

                        dgv_usp.Rows.Clear();

                        Form vendorIdRequestForm = new VendorIDRequestForm.Form1("ACTIVATION", data.VendorId, "EMS", data.Invoice, data.vendor_group);

                        vendorIdRequestForm.Show();

                        data.Complete = 9;

                        data.PendingReasons = "Vendor ID Request";
                    }
                    else if (vendor_data.vdr_status == 1 && vendor_data.vms_status == 0)
                    {

                        data.Complete = 9;
                        data.PendingReasons = "Vendor ID Request";
                        data.VendorId = dgv_usp.CurrentRow.Cells[0].Value.ToString().Trim();

                        SqlConnection cnn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;"); ;
                        SqlCommand cmd;
                        string sql = string.Format(@"if (select COUNT(*) from tbl_EM_VendorIDRequest where [type] = 'VMS ACTIVATION' and [status] = 'NEW' and vendor_account_requested = '{0}') = 0
                                                    begin
                                                        insert into tbl_EM_VendorIDRequest(vendor_account_requested, [type], date_requested, invoice_name, userid_requestor, name, postal_code, [state], city, street_name, [status])
                                                        select top 1 VendorId, 'VMS ACTIVATION', GETDATE(), '{1}', '{2}', [Name], Zipcode, [State], City, [Address], 'NEW' from tbl_UtilityServiceProvider where VendorId = '{0}'
                                                    end",
                                                 data.VendorId,
                                                 data.Invoice, 
                                                 Environment.UserName);

                        try
                        {
                            cnn.Open();
                            cmd = new SqlCommand(sql, cnn);
                            cmd.ExecuteNonQuery();
                            cmd.Dispose();
                            cnn.Close();

                            this.Close();
                        }
                        catch (Exception ex)
                        {
                        }
                    }

                }
            }

            if (data.client_code == "RR")
            {
                data.VendorAssignedStatus = "Dont Assign - RR Property";
            }

            dgv_usp.Enabled = true;
        }

        private void LoadPreviousUsp_DoWork(object sender, EventArgs e)
        {
            Database.DBConnection.DatabaseEM_Tasks emConn = new Database.DBConnection.DatabaseEM_Tasks();
            Database.DBConnection.DatabaseVDR_Data vdrConn = new Database.DBConnection.DatabaseVDR_Data();
            Database.DB.EM_Tasks emInfo = emConn.GetPreviousInfo(data.property_code, data.client_code, data.AccountNumber, data.vendor_group);
            Database.DB.VDR vdrInfo = new Database.DB.VDR();

            if (emInfo != null)
            {
                vdrInfo = vdrConn.GetDataProcessor(emInfo.VendorId);

                if (vdrInfo != null)
                {
                    dgv_usp.Invoke((Action)delegate
                    {
                        dgv_usp.Visible = true;
                        dgv_usp.Rows.Clear();
                        dgv_usp.Rows.Add(vdrInfo.Vendor_ID, vdrInfo.Vendor_Name, vdrInfo.Address, vdrInfo.vdr_status == 1 ? "Active" : "Inactive", GetStatus(vdrInfo.vms_status.ToString()));
                    });
                }
                else
                {
                    comboBox5.Invoke((Action)delegate
                    {
                        comboBox5.Visible = true;
                        comboBox5.DroppedDown = true;
                    });

                    label4.Invoke((Action)delegate
                    {
                        label4.Visible = true;
                    });

                    button_noMatchUSP.Invoke((Action)delegate
                    {
                        button_noMatchUSP.Visible = true;
                    });

                    btnSearchUSP.Invoke((Action)delegate
                    {
                        btnSearchUSP.Visible = true;
                    });
                }
            }
            else
            {
                comboBox5.Invoke((Action)delegate
                {
                    panel1.Visible = true;
                    comboBox5.DroppedDown = true;
                });

                label4.Invoke((Action)delegate
                {
                    label4.Visible = true;
                });

                button_noMatchUSP.Invoke((Action)delegate
                {
                    button_noMatchUSP.Visible = true;
                });

                btnSearchUSP.Invoke((Action)delegate
                {
                    btnSearchUSP.Visible = true;
                });
            }
        }

        private void CheckIfPartialDuplicate(string property_code, string vendor_id, decimal previousbalance, string amount, string vendor_group)
        {
            if (property_code != string.Empty &&
                vendor_id != string.Empty &&
                previousbalance != 0)
            {
                Database.DBConnection.DatabaseWorkItemsLast30Days previousWIConn = new Database.DBConnection.DatabaseWorkItemsLast30Days();
                Database.DB.WorkItemsLast30Days previousWIData = new Database.DB.WorkItemsLast30Days();

                previousWIData = previousWIConn.GetDataProcessor(property_code, vendor_id, previousbalance, vendor_group);

                if (previousWIData != null)
                {
                    data.Amount = (Convert.ToDecimal(amount) - previousbalance).ToString();

                    data.SpecialInstruction = string.Format("Previous balance {0:0,0.##} already paid in {1}",
                        previousWIData.Amount, previousWIData.work_order_number);
                    
                    //data.Complete = 1;
                    //data.PendingReasons = "Manual WI";
                    //data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                }
            }
        }

        //----------------------------------------------------------------------------------------------
        // This function checks if there is an exact duplicate on list of Manual WI for the last 90 days
        // and tag it as pending
        //----------------------------------------------------------------------------------------------
        public void CheckIfExactDuplicate(string property_code, string vendor_group, string servFrom, string servTo, string client_code, string amount, string account_number, string invoice)
        {
            if (property_code != null && 
                client_code != null && 
                amount != null && 
                account_number != null)
            {
                if (property_code != string.Empty &&
                    client_code != string.Empty &&
                    amount != string.Empty &&
                    Convert.ToDecimal(amount) != 0 &&
                    account_number != string.Empty)
                {
                    Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
                    Database.DBConnection.DatabaseWorkItemsLast30Days wiConn = new Database.DBConnection.DatabaseWorkItemsLast30Days();
                    Database.DB.WorkItemsLast30Days wiData = wiConn.GetDataProcessor(property_code, client_code, Convert.ToDecimal(amount), vendor_group);
                    Database.DB.EM_Tasks EM_duplicate = conn.SearchForDuplicates(property_code, vendor_group, servFrom, servTo, account_number, amount, DateTime.Now.AddMonths(-2), invoice, 0);

                    if (EM_duplicate != null)
                    {
                        if (System.IO.File.Exists(EM_duplicate.InvoiceFolderName))
                        {
                            data.PendingReasons = "Pending Duplicate";
                            data.remarks = EM_duplicate.InvoiceFolderName;
                        }
                    }

                    if (wiData != null)
                    {
                        data.remarks = wiData.work_order_number;
                        
                    }
                }
            }
        }

        //---------------------------------------------------------------------------------------------------
        // This function checks if the invoice came from Call-Out queue and tags it as "Manual WI - Callout")
        //---------------------------------------------------------------------------------------------------
        private void CheckIfFromCallout(int complete, string invoice)
        {
            if (complete == 1)
            {
                Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();
                Database.DB.EM_Tasks latestData = new Database.DB.EM_Tasks();
                latestData = conn.GetLatestInvoiceData(invoice);

                if (latestData != null)
                {
                    if (latestData.Complete == 0)
                    {
                        data.PendingReasons = "Manual WI - Callout";
                    }
                    if (latestData.PendingReasons == "Manual WI - Callout")
                    {
                        data.PendingReasons = "Manual WI - Callout";
                    }
                }
            }
        }

        private void CheckIfAccountNumberHasLetters()
        {
            if (data.AccountNumber != null)
            {
                if (!string.IsNullOrEmpty(data.AccountNumber) &&
                    data.Complete == 1 &&
                    data.PendingReasons == "Bulk Upload")
                {
                    foreach (char c in data.AccountNumber)
                    {
                        if (!char.IsLetter(c))
                        {
                            data.Complete = 1;
                            //data.PendingReasons = "Manual WI";
                            data.PendingReasons = "Pre-work item Review";
                        }
                    }
                }
            }
        }

        private void CheckIfNotUtilityVendor()
        {
            if (data.PendingReasons != null && data.vendor_group != null)
            {
                if (data.Complete == 1 &&
                    data.PendingReasons == "Bulk Upload" &&
                    (data.vendor_group == "T" || data.vendor_group == "D" || data.vendor_group == "S"))
                {
                    //data.PendingReasons = "Manual WI";
                    data.PendingReasons = "Pre-work item Review";
                }
            }
        }

        private bool CheckIfMoreThan1000()
        {
            bool result = false;

            if (data.AmountAfterDue != 0)
            {
                if ((data.client_code == "RESI") && (data.AmountAfterDue >= (decimal)500 && data.AmountAfterDue < (decimal)1000))
                {
                    data.Complete = 1;
                    data.PendingReasons = "For Review RESI Amount > $500";
                    data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                    data.VendorAssignedStatus = string.Empty;
                    result = true;
                }

                if (data.AmountAfterDue >= 1000)
                {
                    data.Complete = 1;
                    data.PendingReasons = "For Review Amount > $1,000";
                    data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                    data.VendorAssignedStatus = string.Empty;
                    result = true;
                }

                if (data.AmountAfterDue >= 10000 && (data.client_code == "OLSR" || data.client_code == "PFC"))
                {
                    data.Complete = 1;
                    data.PendingReasons = string.Format("For Review {0} Amount > $10,000", data.client_code);
                    data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                    data.VendorAssignedStatus = string.Empty;
                }

                // Check if PROPERTY_CODE is OLSR and AMOUNT > $2,500

                Double dtAmt = 0;

                try
                {
                    dtAmt = Convert.ToDouble(data.Amount);
                }
                catch
                {
                    dtAmt = 0;
                }

                if (data.client_code == "OLSR" && dtAmt >= 2500 && dtAmt < 10000)
                {
                    data.Complete = 14;
                    data.PendingReasons = "RSC Approval - High Amount";
                    data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.RSCFolder);
                    data.VendorAssignedStatus = string.Empty;

                    bwRscApproval.RunWorkerAsync();
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(data.Amount))
                {
                    if ((data.client_code == "RESI") && (Convert.ToDecimal(data.Amount) >= (decimal)500 && Convert.ToDecimal(data.Amount) < (decimal)1000))
                    {
                        data.Complete = 1;
                        data.PendingReasons = "For Review RESI Amount > $500";
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                        data.VendorAssignedStatus = string.Empty;
                        result = true;
                    }

                    if (Convert.ToDecimal(data.Amount) >= 1000)
                    {
                        data.Complete = 1;
                        data.PendingReasons = "For Review Amount > $1,000";
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                        data.VendorAssignedStatus = string.Empty;
                        result = true;
                    }

                    if (Convert.ToDecimal(data.Amount) >= 10000 && (data.client_code == "OLSR" || data.client_code == "PFC"))
                    {
                        data.Complete = 1;
                        data.PendingReasons = string.Format("For Review {0} Amount > $10,000", data.client_code);
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                        data.VendorAssignedStatus = string.Empty;
                        result = true;
                    }

                    // Check if PROPERTY_CODE is OLSR and AMOUNT > $2,500

                    Double dtAmt2 = 0;

                    try
                    {
                        dtAmt2 = Convert.ToDouble(data.Amount);
                    }
                    catch
                    {
                        dtAmt2 = 0;
                    }

                    if (data.client_code == "OLSR" && dtAmt2 >= 2500 && dtAmt2 < 10000)
                    {
                        data.Complete = 14;
                        data.PendingReasons = "RSC Approval - High Amount";
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.RSCFolder);
                        data.VendorAssignedStatus = string.Empty;

                        bwRscApproval.RunWorkerAsync();
                    }
                }
            }

            return result;
        }

        private bool isUnderASFI()
        {

            bool isASFI = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select top 1 a.PropertyCode from tbl_EM_RESI_ASFI_Properties a
                                            where (a.PropertyCode = '{0}' or a.PropertyCode = '{0}' + '1')", data.property_code);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    isASFI = true;
                }
                catch
                {
                    isASFI = false;
                }
            }
            else
            {
                isASFI = false;
            }

            conn.Close();

            return isASFI;
        }

        private void SendToDeactivation(string propertycode, string utility, int mode, string outofREODate)
        {
            SqlConnection cnn = new SqlConnection(Constants.connectionString); ;
            SqlCommand cmd;
            string sql = string.Empty;

            if (mode == 1)
            {
                sql += string.Format(@"insert into tbl_EM_DeactivationfromEM (EM_ID, PropertyCode, Utility, DeactivationDate, DeactivatedBy, DateEMValidated) 
                                    select TOP 1 {0}, '{1}', '{2}', PostedDate, PostedBy, GETDATE() from tbl_UC_PropertyDeact where ([Property ID] = '{1}' or [Property ID] = '{1}' + '1') and SUBSTRING(LTRIM(Utility),1,1) = '{2}' and [Status] = 'Deactivated' and DATEDIFF(day,PostedDate,GETDATE()) > 45 order by Posteddate desc  " + Environment.NewLine, data.id, data.property_code, data.vendor_group);
                sql += string.Format(@"update tbl_UC_PropertyDeact set [Status] = 'Rework', UpdateFlag = 0, UpdatedVMS = 0, VMSPostedDate = NULL where ([Property ID] = '{0}' or [Property ID] = '{0}' + '1') and SUBSTRING(LTRIM(Utility),1,1) = '{1}' and [Status] = 'Deactivated' and DATEDIFF(day,PostedDate,GETDATE()) > 45 ", data.property_code, data.vendor_group);
            }
            else
            {



                if (data.client_code != "PFC" && (data.vendor_group == "E" || data.vendor_group == "G" || data.vendor_group == "W"))
                {

                    sql += string.Format(@"insert into tbl_EM_DeactivationfromEM (EM_ID, PropertyCode, Utility, DeactivationDate, DateEMValidated) 
                                       values ({0},'{1}','{2}','{3}',GETDATE())", data.id, data.property_code, data.vendor_group, outofREODate.ToString());

                    string vg = string.Empty;
                    string cl = string.Empty;

                    if (data.client_code == "OLSR")
                    {
                        cl = "REO";
                    }
                    else
                    {
                        cl = data.client_code;
                    }

                    if (utility == "E")
                    {
                        vg = "Elec";
                    }
                    else if (utility == "G")
                    {
                        vg = "Gas";
                    }
                    else if (utility == "W")
                    {
                        vg = "Water";
                    }

                    sql += string.Format(@"if (select top 1 [Status] from tbl_UC_PropertyDeact where [Property ID] = '{0}' and Utility = '{1}') = 'Deactivated'
                                        begin
	                                        update tbl_UC_PropertyDeact set [Status] = 'Rework', UpdateFlag = 0, UpdatedVMS = 0, VMSPostedDate = NULL where [Property ID] = '{0}' and Utility = '{1}'
                                        end
                                        else
                                        begin
                                        	
	                                        if (select COUNT(*) from tbl_UC_PropertyDeact where [Property ID] = '{0}' and Utility = '{1}') = 0
	                                        begin
		                                        print 'Insert'
                                        		
		                                        if (select COUNT(*) from tbl_UC_PropertyInFlow where Origin = 'EMS' and PropertyCode = '{0}' and Utility = '{1}' and Uploaded = 0) = 0
                                                    begin
                                                    
                                                    insert into tbl_UC_PropertyInFlow (USPName, PropertyCode, AccountNo, [Address], Category, Task, Utility, Origin, DateUploaded)
                                                    values('{2}', '{0}', '{3}', '{4}', '{5}', 'Deactivation', '{1}', 'EMS', CAST(GETDATE() AS DATE))
                                                    end

                                                update tbl_UC_ParameterReference set REFVALUE = '3' where REFKEY = 'Update_OpenRpt'
	                                        end
                                        end", propertycode, vg, data.usp_name, data.AccountNumber, dataGridView1.Rows[0].Cells["cFullAddress"].Value, cl);
                }              

                REODate = new DateTime();
            }
           

            try
            {
                cnn.Open();
                cmd = new SqlCommand(sql, cnn);
                cmd.ExecuteNonQuery();
                cmd.Dispose();
                cnn.Close();
                //MessageBox.Show("ExecuteNonQuery in SqlCommand executed !!");
            }
            catch (Exception ex)
            {
            }
        }

        private bool GetAuditResult()
        {
            bool isFail = false;

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;Trusted_Connection=Yes;");

            conn.Open();

            string sql = string.Empty;

            sql += string.Format(@"EXEC dbo.sp_EM_GetAuditResult '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}'", 
                                data.id, data.vendor_group, data.AccountNumber, data.property_code, data.VendorId, data.invoice_date, data.DueDate, data.ServiceFrom, data.ServiceTo,
                                data.DisconnectionDate, data.final_bill, data.PreviousBalance, data.PaymentReceived, data.CurrentCharge, data.LateFee, data.DisconnectionFee, data.AmountAfterDue, Environment.UserName);
            

            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    isFail = true;
                }
                catch
                {
                    isFail = false;
                }
            }
            else
            {
                isFail = false;
            }

            conn.Close();

            return isFail;
        }


        private void GetValidationResult()
        {

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;Trusted_Connection=Yes;");
            conn.Open();

            string sql = string.Empty;

            sql += string.Format(@"EXEC dbo.sp_EM_ValidateInvoice '{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}','{15}','{16}','{17}','{18}'",
                                data.id, data.property_code, data.vendor_group, data.AccountNumber, data.ServiceFrom, data.ServiceTo, data.invoice_date, data.PendingReasons, data.VendorId,
                                data.usp_name, data.PreviousBalance, data.PaymentReceived, data.AmountAfterDue, data.Amount.Replace(",",""), data.final_bill, textBox18.Text == string.Empty ? "0" : textBox18.Text,
                                data.Invoice, data.InvoiceFolderName, Environment.UserName);


            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {

                try
                {
                    data.Complete = Convert.ToInt32(rd[0]);
                    data.PendingReasons = rd[1].ToString();
                    data.client_code = rd[2].ToString();
                    data.Amount = rd[3].ToString();
                }
                catch
                {
                    //isFail = false;
                }
            }
            else
            {
                //isFail = false;
            }

            conn.Close();
        }

        private bool CheckIfPropertyIsDeactivated(string propcode, string utility)
        {

            bool isDeactivated = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select top 1 [Property ID], SUBSTRING(LTRIM(Utility),1,1) from tbl_UC_PropertyDeact where ([Property ID] = '{0}' or [Property ID] = '{0}' + '1') and SUBSTRING(LTRIM(Utility),1,1) = '{1}' and [Status] = 'Deactivated' and DATEDIFF(day,DateUploaded,GETDATE()) > 45 order by DateUploaded desc ", data.property_code, data.vendor_group);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    isDeactivated = true;
                }
                catch
                {
                    isDeactivated = false;
                }
            }
            else
            {
                isDeactivated = false;
            }

            conn.Close();

            return isDeactivated;
        }

        private static bool IsDate(string val)
        {
            string strDate = val;
            try
            {
                DateTime dt = DateTime.Parse(strDate);
                if (dt != DateTime.MinValue && dt != DateTime.MaxValue)
                    return true;
                return false;
            }
            catch
            {
                return false;
            }
        }

        private bool CheckIfOutOfREO()
        {

            bool isOutofREO = false;

            Database.DBConnection.DatabaseREO_Properties propconn = new Database.DBConnection.DatabaseREO_Properties();
            Database.DB.REO_Properties propdata = new Database.DB.REO_Properties();
            propdata = propconn.GetData2(data.property_code);

            if (IsDate(propdata.reosrc_date.ToString()))
            {
                REODate = propdata.reosrc_date;
            }

            if (IsDate(propdata.reoslc_date.ToString()))
            {
                REODate = propdata.reoslc_date;
            }

            if (IsDate(propdata.reosfc_date.ToString()))
            {
                REODate = propdata.reosfc_date;
            }

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select DATEDIFF(DAY, '{0}', GETDATE())", REODate);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    if (Convert.ToInt32(rd[0]) > 160)
                    {
                        isOutofREO = true;
                    }
                    else
                    {
                        isOutofREO = false;
                    }
                }
                catch
                {
                    isOutofREO = false;
                }
            }
            else
            {
                isOutofREO = false;
            }

            conn.Close();

            return isOutofREO;
        }

        private void SendToRSC(string propaddress, string propstatus, string amount)
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"insert into tbl_EM_RSCApproval(created_by,date_created,date_modified,invoice,modified_by,notes,[status],
                                            property_code,full_address,vendor_code,Vendor_Name,amount,vendor_type,property_status,invoice_link,asset_manager)
                                            values('{0}', '{10}', '{10}', '{1}', '{0}', 'New Request ' + CAST(CAST(GETDATE() as date) as varchar(15)), 'Pending',
                                            '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}', 
                                            (select top 1 asset_manager from tbl_REO_Properties where (property_code = '{2}' or property_code like '{2}%'))) ",
                                            Environment.UserName.ToLower(), data.Invoice, data.property_code, propaddress, data.VendorId, data.usp_name, amount, data.vendor_group, propstatus, data.InvoiceFolderName, DateTime.Now.ToString());

            SqlCommand cmd = new SqlCommand(query, conn);

            int rowsAffected = cmd.ExecuteNonQuery();

            conn.Close();
        }

        private string CheckifRPM()
        {
            string ASFIreq = string.Empty;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = String.Format(@"select EndDate from tbl_RPM_Utility where (PropCode = '{0}' or PropCode = '{0}' + '1') and substring(Utility,1,1) = '{1}'",
                                  data.property_code, data.vendor_group);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                ASFIreq = rd[0].ToString();
            }

            return ASFIreq;
        }

        private bool Check21Days()
        {
            bool is21day = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = String.Format(@"if (((select count(*) from tbl_EM_WorkItemsLast30Days 
										where (property_code = '{0}' or property_code = '{0}' + '1') 
										and vendor_code = '{1}' and SUBSTRING(wo_task, 1,1) = '{2}' 
										and vendor_price = '{3}' and DATEDIFF(DAY, issued_date, GETDATE()) <= 21) > 0) 
										or
										(select COUNT(*) from tbl_ADHOC_EM_Tasks 
										where (property_code = '{0}' or property_code = '{0}' + '1') 
										and vendor_group = '{2}' and work_item <> '' 
										and DATEDIFF(DAY, date_bulkuploaded, GETDATE()) <= 21) > 0)
										begin
											
											select 'True'
										end",
                                  data.property_code, data.VendorId, data.vendor_group, data.Amount);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    is21day = true;
                }
                catch
                {
                    is21day = false;
                }
            }
            else
            {
                is21day = false;
            }

            conn.Close();

            return is21day;
        }

        private bool CheckDuplicates()
        {
            bool isDuplicate = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

//            string query = String.Format(@"select distinct * from 
//                                            (                                            
//                                            select [Work Order #], y.ecp_date from tbl_EM_InvoiceReconFile a
//                                            left join tbl_EM_PayAppWorkItems y
//                                            on a.[Work Order #] = y.work_item
//                                            where [Loan #] = '{0}'
//                                            and SUBSTRING([Line Item Code], 1, 1) = case when '{6}' in ('T', 'D', 'S') then 'W' when '{6}' = 'G' then 'G' else 'E' end
//                                            --and b.work_item is null
//                                            ) as tbl_dup
//                                            where DATEDIFF(day,ecp_date, GETDATE()) <= 60",
//                                  data.property_code, data.AccountNumber, data.Amount.Replace(",", ""), DateTime.Now, DateTime.Now, data.Invoice, data.vendor_group, data.ServiceFrom.ToString().Replace("0001", "1900"), data.ServiceTo.ToString().Replace("0001", "1900"));

            string query = String.Format(@"select distinct * from 
                                            (                                            
                                            select work_item [Work Order #], ecp_date from tbl_EM_PayAppWorkItems a
                                            where property_code = '{0}' and work_item_status not in ('Cancelled', 'Cr-Cancel', 'Payment Rejected', 'Rejected') 
                                            and DATEDIFF(day,ecp_date,GETDATE()) <= 45
                                            union all
                                            select 'Pending', GETDATE() from vw_EMS_BulkUploadQuery
                                            where [Property ID] = '{0}'
                                            union all
                                            select 'Pending', GETDATE() from vw_EM_ManualWIQueue
                                            where property_code = '{0}'
                                            union all
                                            select 'Pending', GETDATE()
                                            from dbo.vw_EM_VendorAssignmentQueue a
                                            where a.property_code = '{0}'
                                            union all
                                            select 'Created', a.date_bulkuploaded
                                            from tbl_ADHOC_EM_Tasks a
                                            where a.property_code = '{0}'
                                            and a.PendingReasons like 'Approve for%'
                                            and a.date_bulkuploaded is not null
                                            and DATEDIFF(day, a.date_bulkuploaded,GETDATE()) <= 45
                                            union all
                                            select 'For review', GETDATE()
                                            from tbl_ADHOC_EM_Tasks a
                                            where a.property_code = '{0}'
                                            and a.PendingReasons = 'Pre-work item Review'
                                            and a.isLatestData = 1
                                            ) as tbl_dup",
                                  data.property_code, data.AccountNumber, data.Amount.Replace(",", ""), DateTime.Now, DateTime.Now, data.Invoice, data.vendor_group, data.ServiceFrom.ToString().Replace("0001", "1900"), data.ServiceTo.ToString().Replace("0001", "1900"));

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows && rd[0] != null && rd[0].ToString() != string.Empty && rd[0].ToString() != "")
            {
                try
                {
                    isDuplicate = true;
                }
                catch
                {
                    isDuplicate = false;
                }
            }
            else
            {
                isDuplicate = false;
            }

            conn.Close();

            return isDuplicate;
        }

        private bool CheckIfRRProperty()
        {

            bool isRR = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select top 1 * from 
                                            (
                                            select property_id, status_date, 'RR' [Source] from tbl_EM_RRProperties where (property_id = '{0}' or property_id = '{0}' + '1')
                                            union all
                                            select property_code, property_status_change_date, 'EMS' [Source] from tbl_REO_Properties where (property_code = '{0}' or property_code = '{0}' + '1')
                                            ) tblsrc order by status_date desc", data.property_code);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    if (rd[2].ToString() == "RR")
                    {
                        isRR = true;
                    }
                    else
                    {
                        isRR = false;
                    }
                    
                }
                catch
                {
                    isRR = false;
                }
            }
            else
            {
                isRR = false;
            }

            conn.Close();

            return isRR;
        }


        private bool CheckIfBOAProperty()
        {

            bool isBOA = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select a.PropertyCode, case when b.active = 'Active' then b.customer_name else a.PortfolioName end from tbl_BOAProperties a 
                                            left join tbl_REO_Properties b
                                            on a.PropertyCode = b.property_code
                                            where case when b.active = 'Active' then b.customer_name else a.PortfolioName end in ('PFC-BOA', 'BofA_12')
                                            and a.PropertyCode = '%{0}%'", data.property_code);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                isBOA = true;                
            }
            else
            {
                isBOA = false;
            }

            conn.Close();

            return isBOA;
        }


        private bool CheckIfRPMConserviceProperty()
        {

            bool isRPMC = false;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select * from tbl_REO_Properties WHERE (customer_name IN ('RPM', 'IH', 'OBO')) and property_code = '{0}'", data.property_code);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                isRPMC = true;
            }
            else
            {
                isRPMC = false;
            }

            conn.Close();

            return isRPMC;
        }


        private int CheckifTrailingAsset(string investor, string outofREOdate)
        {
            int isTrailingAsset = 0;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

//            string query = string.Format(@"if (DATEDIFF(day, cast('{1}' as date), cast(GETDATE() as date)) > (select top 1 number_of_days from tbl_EM_property_investors where (investor_code1 = '{0}' or investor_code2 = '{0}'))) 
//                                            or (DATEDIFF(day, cast('{1}' as date), cast(GETDATE() as date)) > 160)
//                                            begin select 'True' end", investor, outofREOdate);

            string query = string.Format(@"select DATEDIFF(day, cast('{1}' as date), cast(GETDATE() as date))", investor, outofREOdate);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    isTrailingAsset = Convert.ToInt32(rd[0].ToString());
                }
                catch
                {
                    isTrailingAsset = 0;
                }
            }
            else
            {
                isTrailingAsset = 0;
            }

            conn.Close();

            return isTrailingAsset;
        }

        private string GetParentAccountData(string Invoice)
        {
            string data = string.Empty;

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select 'Property code: ' + isnull(property_code,'') + ' Account no.: ' + isnull(AccountNumber,'') from tbl_ADHOC_EM_Tasks 
                                        where Invoice = '{0}' and Complete <> 20", Invoice);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                try
                {
                    data = rd[0].ToString();
                }
                catch
                {
                    data = string.Empty;
                }
            }
            else
            {
                data = string.Empty;
            }

            conn.Close();

            return data;
        }

        private void CheckIf21Days()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select * from tbl_EM_WorkItemsLast30Days a
                                            where a.property_code = '{0}' 
                                            and a.vendor_code = '{1}'
                                            and SUBSTRING(a.wo_task,0,1) = '{2}'
                                            and DATEDIFF(DAY, a.issued_date, GETDATE()) <= 21",
                                            data.property_code,
                                            data.VendorId,
                                            data.vendor_group);

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                data.VendorAssignedStatus = "NA";
                data.Complete = 1;
                data.PendingReasons = "Filtered by 21 Days";
                data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
            }

            rd.Close();
            conn.Close();
        }

        private void CheckIfPayableInactive()
        {
            if (Inactive_Date != DateTime.MinValue)
            {
                if (data.invoice_date != DateTime.MinValue)
                {
                    if (Inactive_Date < data.invoice_date)
                    {
                        if (data.vendor_group == "W" && isUnderASFI() == true)
                        {
                            data.Complete = 1;
                            data.PendingReasons = "Manual WI - ASFI";
                            data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.InactiveFolder);
                        }
                        else
                        {
                            data.VendorAssignedStatus = "Dont Assign - Inactive Property";
                            data.Complete = 12;
                            data.PendingReasons = "Inactive Property";
                            data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.InactiveFolder);
                        }                        
                    }
                    else
                    {
                        data.VendorAssignedStatus = "New";
                        data.Complete = 1;
                        //data.PendingReasons = "Manual WI";
                        data.PendingReasons = "Pre-work item Review";
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                    }
                }

                if (data.ServiceFrom != data.ServiceTo)
                {
                    if (Inactive_Date < data.ServiceTo)
                    {
                        data.VendorAssignedStatus = "New";
                        data.Complete = 1;
                        //data.PendingReasons = "Manual WI";
                        data.PendingReasons = "Pre-work item Review";
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                    }
                    else
                    {
                        if (data.vendor_group == "W" && isUnderASFI() == true)
                        {
                            data.Complete = 1;
                            data.PendingReasons = "Manual WI - ASFI";
                            data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.InactiveFolder);
                        }
                        else
                        {
                            data.VendorAssignedStatus = "Dont Assign - Inactive Property";
                            data.Complete = 12;
                            data.PendingReasons = "Inactive Property";
                            data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.InactiveFolder);
                        }                       
                    }
                }
            }
        }

        private void ValidateAudit()
        {

            Database.DBConnection.DatabaseEM_Tasks xconn = new Database.DBConnection.DatabaseEM_Tasks();
            Database.DB.EM_Tasks currdata = new Database.DB.EM_Tasks();
            currdata = xconn.GetDataFromId((uint)data.id);


            // Get Audit Result
            if (data.vendor_group != currdata.vendor_group)
            {
                data.AuditResult = "Fail";
                InputAuditData("Wrong vendor type");
            }

            if (data.AccountNumber != currdata.AccountNumber)
            {
                data.AuditResult = "Fail";
                InputAuditData("Wrong account number");
            }


            if (data.property_code != currdata.property_code)
            {
                data.AuditResult = "Fail";
                InputAuditData("Wrong service property");
            }

            if (data.VendorId != currdata.VendorId)
            {
                data.AuditResult = "Fail";
                InputAuditData("Wrong USP");
            }


            if (data.invoice_date != null && currdata.invoice_date != null)
            {
                if (data.invoice_date.Date.ToString() != currdata.invoice_date.Date.ToString())
                {
                    data.AuditResult = "Fail";
                    InputAuditData("Wrong invoice date");
                }
            }
            else
            {
                if (data.invoice_date.Date.ToString() != currdata.invoice_date.Date.ToString())
                {
                    data.AuditResult = "Fail";
                    InputAuditData("Wrong invoice date");
                }
            }


            if (data.DueDate != null && currdata.DueDate != null)
            {
                if (data.DueDate.Date.ToString() != currdata.DueDate.Date.ToString())
                {
                    data.AuditResult = "Fail";
                    InputAuditData("Wrong Due date");
                }
            }
            else
            {
                if (data.DueDate.Date.ToString() != currdata.DueDate.Date.ToString())
                {
                    data.AuditResult = "Fail";
                    InputAuditData("Wrong Due date");
                }
            }


            if (data.ServiceFrom != null && currdata.ServiceFrom != null)
            {
                if (data.ServiceFrom.Date.ToString() != currdata.ServiceFrom.Date.ToString())
                {
                    data.AuditResult = "Fail";
                    InputAuditData("Wrong Billing cycle");
                }
            }
            else
            {
                if (data.ServiceFrom.Date.ToString() != currdata.ServiceFrom.Date.ToString())
                {
                    data.AuditResult = "Fail";
                    InputAuditData("Wrong Billing cycle");
                }
            }


            if (data.ServiceTo != null && currdata.ServiceTo != null)
            {
                if (data.ServiceTo.Date.ToString() != currdata.ServiceTo.Date.ToString())
                {
                    data.AuditResult = "Fail";
                    InputAuditData("Wrong Billing cycle");
                }
            }
            else
            {
                if (data.ServiceTo.Date.ToString() != currdata.ServiceTo.Date.ToString())
                {
                    data.AuditResult = "Fail";
                    InputAuditData("Wrong Billing cycle");
                }
            }


            if (data.DisconnectionDate != null && currdata.DisconnectionDate != null)
            {
                if (data.DisconnectionDate.Date.ToString() != currdata.DisconnectionDate.Date.ToString())
                {
                    data.AuditResult = "Fail";
                    InputAuditData("Wrong Disconnection date");
                }
            }
            else
            {
                if (data.ServiceTo.Date.ToString() != currdata.ServiceTo.Date.ToString())
                {
                    data.AuditResult = "Fail";
                    InputAuditData("Wrong Disconnection date");
                }
            }


            //if (data.final_bill != currdata.final_bill && currdata.final_bill != string.Empty)
            //{
            //    data.AuditResult = "Fail";
            //    InputAuditData("Wrong tagging of Final Bill");
            //}


            if (data.PreviousBalance != currdata.PreviousBalance)
            {
                data.AuditResult = "Fail";
                InputAuditData("Wrong Previous Balance");
            }
            
            if (data.PaymentReceived != currdata.PaymentReceived)
            {
                data.AuditResult = "Fail";
                InputAuditData("Wrong Payment received");
            }

            if (data.CurrentCharge != currdata.CurrentCharge)
            {
                data.AuditResult = "Fail";
                InputAuditData("Wrong Current charge");
            }

            if (data.LateFee != currdata.LateFee)
            {
                data.AuditResult = "Fail";
                InputAuditData("Wrong Late fee");
            }

            if (data.DisconnectionFee != currdata.DisconnectionFee)
            {
                data.AuditResult = "Fail";
                InputAuditData("Wrong Disconnection fee");
            }

            if (data.AmountAfterDue != currdata.AmountAfterDue)
            {
                data.AuditResult = "Fail";
                InputAuditData("Wrong Amount after due");
            }

            if ((data.PreviousBalance != currdata.PreviousBalance) || (data.PaymentReceived != currdata.PaymentReceived) || (data.CurrentCharge != currdata.CurrentCharge) || (data.LateFee != currdata.LateFee) || (data.DisconnectionFee != currdata.DisconnectionFee))
            {
                //-----------------------------------------
                // Remove previous balance if already paid
                //-----------------------------------------
                decimal balanceForwarded = data.PreviousBalance - data.PaymentReceived;
                if (balanceForwarded > 0)
                {
                    CheckIfPartialDuplicate(data.property_code, data.VendorId, balanceForwarded, data.Amount, data.vendor_group);
                }
            }

            // Remove Deposit, if any
            if (!string.IsNullOrEmpty(textBox18.Text))
            {
                if (data.final_bill == "Yes")
                {
                    data.Amount = (Convert.ToDecimal(data.Amount) - Convert.ToDecimal(textBox18.Text)).ToString();
                }
            }                
        }

        private void ValidateInvoice()
        {

            if (data.Complete == 9)
            {
                data.PendingReasons = "Vendor ID Request";

                return;
            }

            if (data.vendor_group == null)
            {
                data.Complete = 6;
                data.vendor_group = "None";
            }

            if (data.vendor_group != "None") 
            {

                if (data.property_code == null || data.property_code == string.Empty)
                {
                    data.Complete = 3;
                    data.PendingReasons = "PNF";
                    
                    return;
                }
                else
                {
                                      
                        if (CheckIfBOAProperty() == true)
                        {
                            data.Complete = 1;
                            data.PendingReasons = "BOA Property";

                            return;
                        }
                        else
                        {
                            if (CheckIfRPMConserviceProperty() == true)
                            {
                                data.Complete = 1;
                                data.PendingReasons = "RPM Conservice";

                                return;
                            }
                            else
                            {

                                Database.DBConnection.DatabaseREO_Properties propconn = new Database.DBConnection.DatabaseREO_Properties();
                                Database.DB.REO_Properties propdata = new Database.DB.REO_Properties();
                                propdata = propconn.GetData2(data.property_code);

                                if (propdata != null)
                                {
                                    string outofREOdate = string.Empty;

                                    if (IsDate(propdata.reosrc_date.ToString()))
                                    {
                                        outofREOdate = propdata.reosrc_date.ToString();
                                    }

                                    if (IsDate(propdata.reoslc_date.ToString()))
                                    {
                                        outofREOdate = propdata.reoslc_date.ToString();
                                    }

                                    if (IsDate(propdata.reosfc_date.ToString()))
                                    {
                                        outofREOdate = propdata.reosfc_date.ToString();
                                    }
                                    
                                    if (outofREOdate == "1/1/0001 12:00:00 AM")
                                    {
                                        outofREOdate = string.Empty;
                                    }

                                    data.client_code = propdata.customer_name;
                                    data.property_code = propdata.property_code;

                                    if (propdata.customer_name == "HSBC")
                                    {
                                        data.Complete = 1;
                                        data.PendingReasons = "HSBC Property";
                                        data.client_code = "HSBC";

                                        return;
                                    }
                                    else if (propdata.customer_name == "CAPONE")
                                    {
                                        data.Complete = 1;
                                        data.PendingReasons = "CAPONE Property";
                                        data.client_code = "CAPONE";

                                        return;
                                    }
                                    else
                                    {
                                        if (data.Amount == null || data.Amount == string.Empty)
                                        {
                                            data.Amount = "0.00";
                                        }

                                        if (Convert.ToDouble(data.Amount) <= 0)
                                        {
                                            data.Complete = 1;
                                            data.PendingReasons = "Zero or Negative Value";

                                            return;
                                        }

                                        if (Convert.ToDouble(data.Amount) > 0 && Convert.ToDouble(data.Amount) < 1.10)
                                        {
                                            data.Complete = 1;
                                            data.PendingReasons = "Below Minimum Price";

                                            return;
                                        }

                                        if (propdata.active == "Inactive")
                                        {
                                            data.Complete = 1;
                                            data.PendingReasons = "Inactive Property";

                                            return;
                                        }
                                        else if (propdata.property_status == "Bankruptcy")
                                        {
                                            data.Complete = 1;
                                            data.PendingReasons = "Property status is Bankruptcy";

                                            return;
                                        }
                                        else if ((propdata.occupancy_status.ToLower() == "occupied" || propdata.occupancy_status.ToLower() == "owner occupied" || propdata.occupancy_status.ToLower() == "tenant occupied") && propdata.customer_name == "PFC")
                                        {
                                            data.Complete = 1;
                                            data.PendingReasons = "Occupied";

                                            return;
                                        }
                                        else if (propdata.property_status == "REOS/FC" && propdata.pod == "POD OMS" && outofREOdate != string.Empty && CheckifTrailingAsset(propdata.investor_name, outofREOdate) >= 45)
                                        {
                                            int outofREOAge = CheckifTrailingAsset(propdata.investor_name, outofREOdate);

                                            if (outofREOAge >= 20 && outofREOAge <= 45)
                                            {
                                                data.Complete = 1;
                                                data.PendingReasons = "Potential Trailing OMS > 20 days";
                                            }
                                            else if (outofREOAge > 45)
                                            {
                                                data.Complete = 1;
                                                data.PendingReasons = "Trailing OMS > 45 days";
                                            }                                            

                                            if (data.client_code != "PFC")
                                            {
                                                SendToDeactivation(data.property_code, data.vendor_group, 0, outofREOdate);
                                            }

                                            return;
                                        }
                                        else if (propdata.property_status == "REOS/FC" && propdata.pod != "POD OMS" && outofREOdate != string.Empty && CheckifTrailingAsset(propdata.investor_name, outofREOdate) >= 45)
                                        {
                                            int outofREOAge = CheckifTrailingAsset(propdata.investor_name, outofREOdate);

                                            if (outofREOAge >= 45 && outofREOAge <= 90)
                                            {
                                                data.Complete = 1;
                                                data.PendingReasons = "Potential Trailing > 45 days";
                                            }
                                            else if (outofREOAge > 90 && outofREOAge <= 150)
                                            {
                                                data.Complete = 1;
                                                data.PendingReasons = "Potential Trailing > 90 days";
                                            }
                                            else if (outofREOAge > 150)
                                            {
                                                data.Complete = 1;
                                                data.PendingReasons = "Trailing Asset";
                                            }

                                            if (data.client_code != "PFC")
                                            {
                                                SendToDeactivation(data.property_code, data.vendor_group, 0, outofREOdate);
                                            }

                                            return;
                                        }
                                        else
                                        {
                                            string RPM = CheckifRPM();

                                            if (RPM != string.Empty)
                                            {
                                                data.isRPM = true;
                                            }
                                            else
                                            {
                                                data.isRPM = false;
                                            }


                                            if (RPM == "ASFI Required")
                                            {
                                                data.isASFI = true;
                                            }
                                            else
                                            {
                                                data.isASFI = false;
                                            }


                                            // Check if high amount

                                            double amt = 0;

                                            if (Convert.ToDouble(data.AmountAfterDue) > 0)
                                            {
                                                amt = Convert.ToDouble(data.AmountAfterDue);
                                            }
                                            else
                                            {
                                                amt = Convert.ToDouble(data.Amount);
                                            }

                                            if (amt > 500 && amt <= 1000)
                                            {
                                                if (cmbBillName.Text != "ASFI or c/o ASFI" && cmbBillName.Text != string.Empty && propdata.customer_name == "RESI")
                                                {
                                                    data.Complete = 6;
                                                    data.PendingReasons = "Non-ASFI Invoice";
                                                    return;
                                                }
                                                else
                                                {
                                                    data.Complete = 1;
                                                    data.PendingReasons = "For Review > $500";
                                                    return;
                                                }
                                            }

                                            if (amt > 1000 && amt <= 2500)
                                            {
                                                if (cmbBillName.Text != "ASFI or c/o ASFI" && cmbBillName.Text != string.Empty && propdata.customer_name == "RESI")
                                                {
                                                    data.Complete = 6;
                                                    data.PendingReasons = "Non-ASFI Invoice";
                                                    return;
                                                }
                                                else
                                                {
                                                    data.Complete = 1;
                                                    data.PendingReasons = "For Review > $1000";
                                                    return;
                                                }
                                            }

                                            if (amt > 2500 && amt < 10000)
                                            {
                                                data.Complete = 1;
                                                data.PendingReasons = "For Review > $2500";

                                                if (cmbBillName.Text != "ASFI or c/o ASFI" && cmbBillName.Text != string.Empty && propdata.customer_name == "RESI")
                                                {
                                                    data.Complete = 6;
                                                    data.PendingReasons = "Non-ASFI Invoice";
                                                    return;
                                                }
                                                else
                                                {
                                                    if (propdata.customer_name == "OLSR")
                                                    {
                                                        SendToRSC(propdata.full_address, propdata.property_status, amt.ToString());
                                                    }

                                                    return;
                                                }
                                            }

                                            if (amt >= 10000)
                                            {
                                                if (cmbBillName.Text != "ASFI or c/o ASFI" && cmbBillName.Text != string.Empty && propdata.customer_name == "RESI")
                                                {
                                                    data.Complete = 6;
                                                    data.PendingReasons = "Non-ASFI Invoice";
                                                    return;
                                                }
                                                else
                                                {
                                                    data.Complete = 1;
                                                    data.PendingReasons = "For Review > $10000";
                                                    return;
                                                }
                                            }

                                            //Check duplicates

                                            if (CheckDuplicates() == true)
                                            {
                                                data.Complete = 1;
                                                data.PendingReasons = "Pending Duplicate";

                                                return;
                                            }

                                            if (cmbBillName.Text != "ASFI or c/o ASFI" && cmbBillName.Text != string.Empty)
                                            {

                                                if (propdata.customer_name == "OLSR" || propdata.customer_name == "PFC")
                                                {
                                                    if (propdata.customer_name == "PFC")
                                                    {
                                                        data.Complete = 1;
                                                        //data.PendingReasons = "Manual WI";
                                                        data.PendingReasons = "Pre-work item Review";
                                                        return;
                                                    }
                                                    //else if (propdata.pod == "POD OMS")
                                                    //{
                                                    //    data.Complete = 1;
                                                    //    data.PendingReasons = "POD OMS";
                                                    //    return;
                                                    //}
                                                    else
                                                    {

                                                        if (propdata.customer_name != "RESI" && Check21Days() == true)
                                                        {
                                                            data.Complete = 1;
                                                            data.PendingReasons = "Filtered by 21 Day";
                                                            return;
                                                        }

                                                        if (data.SpecialInstruction != string.Empty)
                                                        {
                                                            data.Complete = 1;
                                                            //data.PendingReasons = "Manual WI";
                                                            data.PendingReasons = "Pre-work item Review";
                                                            return;
                                                        }
                                                        else
                                                        {
                                                            data.Complete = 1;
                                                            //data.PendingReasons = "Bulk Upload";
                                                            data.PendingReasons = "Pre-work item Review";
                                                            return;
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    data.Complete = 6;
                                                    data.PendingReasons = "Non-ASFI Invoice";
                                                    return;
                                                }
                                            }
                                            else
                                            {
                                                if (propdata.customer_name == "PFC")
                                                {
                                                    data.Complete = 1;
                                                    //data.PendingReasons = "Manual WI";
                                                    data.PendingReasons = "Pre-work item Review";
                                                    return;
                                                }
                                                //else if (propdata.pod == "POD OMS")
                                                //{
                                                //    data.Complete = 1;
                                                //    data.PendingReasons = "POD OMS";
                                                //    return;
                                                //}
                                                else
                                                {

                                                    if (propdata.customer_name != "RESI" && Check21Days() == true)
                                                    {
                                                        data.Complete = 1;
                                                        data.PendingReasons = "Filtered by 21 Days";
                                                        return;
                                                    }
                                                    
                                                    if (data.SpecialInstruction != string.Empty)
                                                    {
                                                        data.Complete = 1;
                                                        //data.PendingReasons = "Manual WI";
                                                        data.PendingReasons = "Pre-work item Review";
                                                        return;
                                                    }
                                                    else
                                                    {
                                                        data.Complete = 1;
                                                        //data.PendingReasons = "Bulk Upload";
                                                        data.PendingReasons = "Pre-work item Review";
                                                        return;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    data.Complete = 3;
                                    data.PendingReasons = "PNF";
                                    return;
                                }
                            }
                        }  
                }
            }
        }


        private void SaveItem()
        {

            string invoicepath = string.Empty;
            double totalCharge = 0;

            if (cmbBillName.Text == "ASFI or c/o ASFI" && ((textBox10.Text == "None" || checkBox8.Checked == true || checkBox35.Checked == true || checkBox19.Checked == true) && data.Amount != "") || chkBx_NoPropertyAddress.Checked == true)
            {
               
                if (textBox10.Text == "None" && data.Amount != "0" && data.Amount != "0.00")
                {
                    data.Complete = 0;
                    data.PendingReasons = "No Account Number";
                }

                if (chkBx_NoPropertyAddress.Checked == true && data.Amount != "0" && data.Amount != "0.00")
                {
                    data.Complete = 0;
                    data.PendingReasons = "No Property/Service Address";
                }

                if (checkBox8.Checked == true && data.VendorId == string.Empty && (data.Amount == "0" || data.Amount == "0.00" || data.Amount == string.Empty))
                {
                    data.Complete = 0;
                    data.PendingReasons = "No USP Address on Invoice";
                }

                if (checkBox35.Checked == true)
                {
                    data.Complete = 0;
                    data.PendingReasons = "No Amount";
                }

                if (checkBox19.Checked == true)
                {
                    data.Complete = 6;
                    data.PendingReasons = "Blank/Unreadable/Cut Invoice";
                }
            }

            if (chkNYes.Checked == true && data.Complete != 0 && data.Complete != 6 && data.property_code != string.Empty)
            {
                if (data.vendor_group == null || data.vendor_group == string.Empty)
                {
                    MessageBox.Show("Please select if bill is Electricity, Gas or Water");
                    return;
                }

                if ((data.AccountNumber == null || data.AccountNumber == string.Empty) && data.vendor_group != "None")
                {
                    MessageBox.Show("Please enter account number. Enter 'none' if there is no any.");
                    return;
                }

                if (string.IsNullOrEmpty(textBox20.Text) && data.vendor_group != "None")
                {
                    MessageBox.Show("Please enter previous balance. Enter '0.00' if there is no any");
                    return;
                }
            }

            
            //-------------------------------------------------
            // Bug Fix for missing Service From and Service To
            //-------------------------------------------------
            if (data.invoice_date == null)
            {
                data.invoice_date = DateTime.MinValue;
            }
            if (data.DueDate == null)
            {
                data.DueDate = DateTime.Today.Date;
            }
            if (data.ServiceFrom == null)
            {
                data.ServiceFrom = data.DueDate;
            }
            if (data.ServiceTo == null)
            {
                data.ServiceTo = data.DueDate;
            }
            //-------------------------------------------------

            if (!string.IsNullOrEmpty(textBox24.Text))
            {
                // If there is Total AMT After Due Date, Ignore Total Computed Amount

                Double amtAfterDue = 0;

                if (textBox22.Text != string.Empty)
                {
                    amtAfterDue = Convert.ToDouble(textBox22.Text);
                }
                else
                {
                    amtAfterDue = 0;
                }

                totalCharge = amtAfterDue > 0 ? amtAfterDue : Convert.ToDouble(textBox24.Text);

                data.CurrentCharge = Convert.ToDecimal(textBox23.Text);
                data.DisconnectionFee = Convert.ToDecimal(textBox8.Text);
                data.LateFee = Convert.ToDecimal(textBox6.Text);
                data.PaymentReceived = Convert.ToDecimal(textBox21.Text);
                data.PreviousBalance = Convert.ToDecimal(textBox20.Text);                
            }

            Database.DBConnection.DatabaseEM_Tasks conn = new Database.DBConnection.DatabaseEM_Tasks();

            myTimer.Stop();

            data.AuditedBy = Environment.UserName;
            invoicepath = data.InvoiceFolderName;

            string[] message = new string[] {"Greetings,", Environment.NewLine, "This is an automated email. Please do not reply back.",
                    "Please contact PhpUMAL@altisource.com for queries.", Environment.NewLine, "Warm Regards,", "Expense Management Team"};

            //if (iniQueue == 21) // QA 2
            //{
            //    if (data.AuditResult == string.Empty || data.AuditResult == null)
            //    {
            //        conn.UpdateAuditPass(data.id, data.AuditDate, data.AuditedBy, Convert.ToInt32(label3.Text));
            //    }
            //    else
            //    {
            //        conn.UpdateAuditFail(data.id, data.AuditDate, data.AuditedBy, Convert.ToInt32(label3.Text));
            //    }

            //    data.Complete = 22;
            //    data.PendingReasons = "Pending for Audit";

            //    conn.UpdateLatestData(data.id);

            //    conn.WriteData(data);

            //    MessageBox.Show("Task was saved");
            //}
            //else
            //{
                if (data.AuditResult == string.Empty || data.AuditResult == null)
                {
                    conn.UpdateAuditPass(data.id, data.AuditDate, data.AuditedBy, Convert.ToInt32(label3.Text));

                    if (checkBox18.Checked == false && checkBox17.Checked == false && checkBox16.Checked == false)
                    {
                        switch (comboBox1.Text)
                        {
                            case "TaxBill/TaxSale/property bill":

                                SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "tax@ocwen.com");

                                break;

                            case "Cut Grass/weeds removal/Lawn maintenance":

                                SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "cv-mailbox@altisource.com");

                                break;

                            case "Removal of debris":


                                SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "cv-mailbox@altisource.com");

                                break;

                            default:

                                data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);
                                break;
                        }
                    }

                    if (data.PendingReasons == "Blank/Unreadable/Cut Invoice")
                    {
                        data.Complete = 6;
                    }
                    else
                    {
                        if (data.Complete != 0 && data.Complete != 6)
                        {
                            ValidateInvoice();

                            if (data.Invoice.Contains("TRANSFER") == true)
                            {
                                if (txtTransferAccount.Text == "0")
                                {
                                    data.SpecialInstruction += string.Format(@"Paying only ${0}. Balance transfers will be processed individually.", data.Amount);
                                }

                                if (Convert.ToInt32(txtTransferAccount.Text) > 0)
                                {
                                    string pdata = GetParentAccountData(data.Invoice);

                                    data.SpecialInstruction += string.Format(@"Paying balance transfer amount of ${0} which was transferred unto {1}.", data.Amount, pdata);
                                }
                            }

                            if (data.Complete == 1 && (data.vendor_group == "E" || data.vendor_group == "G" || data.vendor_group == "W") && (data.PendingReasons.Contains("Manual WI") || data.PendingReasons.Contains("Bulk Upload")) && data.final_bill == "No")
                            {
                                if (CheckIfPropertyIsDeactivated(data.property_code, data.vendor_group) == true)
                                {
                                    SendToDeactivation(data.property_code, data.vendor_group, 1, REODate.ToString());
                                }
                            }
                        }
                    }

                    //conn.UpdateInvoice(data);

                    //MessageBox.Show("Task was saved");


                    if (data.PendingReasons == string.Empty)
                    {
                        MessageBox.Show(string.Format("An error occured while saving the data. Please contact your system administrator."));
                        return;
                    }

                    conn.UpdateLatestData(data.id);

                    conn.WriteData(data);

                    MessageBox.Show("Task was saved");
                }
                else
                {

                    conn.UpdateAuditFail(data.id, data.AuditDate, data.AuditedBy, Convert.ToInt32(label3.Text));

                    data.remarks = string.Empty;
                    data.userid = Environment.UserName.ToLower();

                    if (checkBox18.Checked == false && checkBox17.Checked == false && checkBox16.Checked == false)
                    {
                        switch (comboBox1.Text)
                        {
                            case "TaxBill/TaxSale/property bill":

                                SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "tax@ocwen.com");

                                break;

                            case "Cut Grass/weeds removal/Lawn maintenance":

                                SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "cv-mailbox@altisource.com");

                                break;

                            case "Removal of debris":


                                SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "cv-mailbox@altisource.com");

                                break;

                            default:

                                data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);
                                break;
                        }
                    }

                    if ((cmbBillName.Text == "ASFI or c/o ASFI" && (textBox10.Text == "None" || checkBox8.Checked == true || checkBox35.Checked == true || checkBox19.Checked == true) && data.Amount != "") || chkBx_NoPropertyAddress.Checked == true)
                    {

                        if (textBox10.Text == "None" && data.Amount != "0" && data.Amount != "0.00")
                        {
                            data.Complete = 0;
                            data.PendingReasons = "No Account Number";
                        }

                        if (chkBx_NoPropertyAddress.Checked == true && data.Amount != "0" && data.Amount != "0.00")
                        {
                            data.Complete = 0;
                            data.PendingReasons = "No Property/Service Address";
                        }

                        if (checkBox8.Checked == true && data.VendorId == string.Empty && (data.Amount == "0" || data.Amount == "0.00"))
                        {
                            data.Complete = 0;
                            data.PendingReasons = "No USP Address on Invoice";
                        }

                        if (checkBox35.Checked == true)
                        {
                            data.Complete = 0;
                            data.PendingReasons = "No Amount";
                        }

                        if (checkBox19.Checked == true)
                        {
                            data.Complete = 6;
                            data.PendingReasons = "Blank/Unreadable/Cut Invoice";
                        }
                    }

                    if (data.PendingReasons == "Blank/Unreadable/Cut Invoice")
                    {
                        data.Complete = 6;
                    }
                    else
                    {
                        if (data.Complete != 0 && data.Complete != 6)
                        {
                            ValidateInvoice();

                            if (data.Invoice.Contains("TRANSFER") == true)
                            {
                                if (txtTransferAccount.Text == "0")
                                {
                                    data.SpecialInstruction += string.Format(@"Paying only ${0}. Balance transfers will be processed individually.", data.Amount);
                                }

                                if (Convert.ToInt32(txtTransferAccount.Text) > 0)
                                {
                                    string pdata = GetParentAccountData(data.Invoice);

                                    data.SpecialInstruction += string.Format(@"Paying balance transfer amount of ${0} which was transferred unto {1}.", data.Amount, pdata);
                                }
                            }

                            if (data.Complete == 1 && (data.vendor_group == "E" || data.vendor_group == "G" || data.vendor_group == "W") && (data.PendingReasons.Contains("Manual WI") || data.PendingReasons.Contains("Bulk Upload")) && data.final_bill == "No")
                            {
                                if (CheckIfPropertyIsDeactivated(data.property_code, data.vendor_group) == true)
                                {
                                    SendToDeactivation(data.property_code, data.vendor_group, 1, REODate.ToString());
                                }
                            }
                        }
                    }

                    conn.UpdateLatestData(data.id);

                    conn.WriteData(data);

                    MessageBox.Show("Task was saved");
                }
            //}
        }

        private void InputAuditData(string result)
        {
            SqlConnection sqlcon = new SqlConnection(Constants.connectionString);

            try
            {
                sqlcon.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save data. Error: {0}", ex.Message);

                sqlcon.Close();
                return;
            }


            string query = string.Format(@"if (select COUNT(*) from tbl_EM_Audit where em_task_id = '{0}' and result = '{1}') = 0
	                                        begin
                                                INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_Audit]
                                                   ([em_task_id]
                                                   ,[result]
                                                   ,[auditor]
                                                   ,[timestamp])
                                                VALUES
                                                   ({0}
                                                   ,'{1}'
                                                   ,'{2}'
                                                   ,CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,GETUTCDATE()),'+08:00')))
                                            end",
                                                   data.id,
                                                   result,
                                                   Environment.UserName.ToLower());

            SqlCommand sqlcom = new SqlCommand(query, sqlcon);

            try
            {
                sqlcom.ExecuteNonQuery();

                sqlcon.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save audit remark. Error: " + ex.Message);

                sqlcon.Close();

                return;
            }
        }


        //-----------------
        // OLD FUNCTIONS
        //-----------------

        // Is this bill readable?
        private void pct_isbillreadable_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsIsBillReadable, pct_isbillreadableapprove, pct_isbillreadablecheck, true);

            data.AuditResult = "Fail";
            InputAuditData("Unable to detect if bill is readable");
        }

        private void pct_isbillreadableapprove_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsIsBillReadable, pct_isbillreadableapprove, false);
        }
        
        // Is this an electricity, gas or water bill?
        private void pictureBox23_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsVendorGroup, pictureBox22, pictureBox24, true);

            
            //data.AuditResult = "Fail";
            //InputAuditData("Wrong vendor type");
        }

        private void pictureBox22_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsVendorGroup, pictureBox22, false);
        }

        // Account Number
        private void pictureBox37_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsAccountNumber, pictureBox36, pictureBox39, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong account number");
        }

        private void pictureBox36_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsAccountNumber, pictureBox36, false);
        }

        // Property Data Entered
        private void pictureBox4_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsPropertyDataEntered, pictureBox3, pictureBox26, true);

            hasDoneAcctSearch = true;

            InputAuditData("Wrong data entered on property search");
            data.AuditResult = "Fail";
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsPropertyDataEntered, pictureBox3, false);
        }

        // USP On Invoice
        private void pictureBox28_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsUspOnInvoice, pictureBox27, pictureBox35, true);
        }

        private void pictureBox27_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsUspOnInvoice, pictureBox27, false);
        }

        // Do Not Mail
        private void pictureBox2_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsDoNotMail, pictureBox1, true);
                        
            InputAuditData("Wrong tagging of Do Not Mail");
            data.AuditResult = "Fail";
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsDoNotMail, pictureBox1, false);
        }

        // Previous Amount
        private void pictureBox7_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsPreviousAmount, pictureBox6, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong Previous Balance");
        }

        private void pictureBox6_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsPreviousAmount, pictureBox6, false);
        }

        // Payment Received
        private void pictureBox17_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsPaymentReceived, pictureBox16, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong Payment Received");
        }

        private void pictureBox16_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsPaymentReceived, pictureBox16, false);
        }

        // Current Charge
        private void pictureBox20_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsCurrentCharge, pictureBox19, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong Current Charge");
        }

        private void pictureBox19_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsCurrentCharge, pictureBox19, false);
        }

        // Late Fee
        private void pictureBox32_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsLateFee, pictureBox31, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong Late Fee");
        }

        private void pictureBox31_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsLateFee, pictureBox31, false);
        }

        private void pictureBox34_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsDisconnectionFee, pictureBox34, false);
        }

        // Amount
        private void pictureBox38_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsDisconnectionFee, pictureBox34, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong Disconnection Fee");
        }

        // Amount After Due
        private void pictureBox42_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsAmountAfterDue, pictureBox41, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong Amount After Due");
        }

        private void pictureBox41_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsAmountAfterDue, pictureBox41, false);
        }

        // No Amount
        private void pictureBox11_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsNoAmount, pictureBox10, true);

            data.AuditResult = "Fail";
            InputAuditData("Wrong tagging of No Amount");
        }

        private void pictureBox10_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsNoAmount, pictureBox10, false);
        }

        // Final Bill
        private void pictureBox45_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsFinalBill, pictureBox44, true);

            data.AuditResult = "Fail";
            InputAuditData("Wrong tagging of Final Bill");
        }

        private void pictureBox44_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsFinalBill, pictureBox44, false);
        }

        // Invoice Date
        private void pictureBox48_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsInvoiceDate, pictureBox47, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong invoice date");
        }

        private void pictureBox47_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsInvoiceDate, pictureBox47, false);
        }

        // Due Date
        private void pictureBox51_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsDueDate, pictureBox50, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong due date");
        }

        private void pictureBox50_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsDueDate, pictureBox50, false);
        }

        // Billing Cycle
        private void pictureBox54_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsBillingCycle, pictureBox53, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong billing cycle");
        }

        private void pictureBox53_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsBillingCycle, pictureBox53, false);
        }

        // Disconnection Date
        private void pictureBox57_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsDisconnectionDate, pictureBox56, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong disconnection date");
        }

        private void pictureBox56_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsDisconnectionDate, pictureBox56, false);
        }

        // Property
        private void pictureBox60_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsProperty, pictureBox59, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong service property");
        }

        private void pictureBox59_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsProperty, pictureBox59, false);
        }

        // Vendor
        private void pictureBox14_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsVendor, pictureBox13, true);

            //data.AuditResult = "Fail";
            //InputAuditData("Wrong USP");
        }

        private void pictureBox13_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsVendor, pictureBox13, false);
        }

        private void ChangeControlState(Control[] controls, PictureBox approveButton, bool isEnabled)
        {
            foreach (Control c in controls)
            {
                c.Enabled = isEnabled;
            }

            approveButton.Visible = isEnabled;
        }

        private void ChangeControlState(Control[] controls, PictureBox approveButton, PictureBox checkbutton, bool isEnabled)
        {
            foreach (Control c in controls)
            {
                c.Enabled = isEnabled;
            }

            approveButton.Visible = isEnabled;
            checkbutton.Visible = isEnabled;
        }

        private void ResetControl(Control[] controls)
        {
            foreach (Control c in controls)
            {
                if (c.GetType() == typeof(CheckBox))
                {
                    ((CheckBox)c).Checked = false;
                    ((CheckBox)c).Enabled = false;
                }

                if (c.GetType() == typeof(ComboBox))
                {
                    ((ComboBox)c).Text = string.Empty;
                    ((ComboBox)c).Enabled = false;
                }

                if (c.GetType() == typeof(TextBox))
                {
                    ((TextBox)c).Text = string.Empty;
                    ((TextBox)c).Enabled = false;
                }

                if (c.GetType() == typeof(DateTimePicker))
                {
                    ((DateTimePicker)c).Value = DateTime.Today;
                    ((DateTimePicker)c).Enabled = false;
                }
            }
        }

        private void ResetControl(Control[] controls, Control[] exception)
        {
            foreach (Control c in controls)
            {
                if (!exception.Contains(c))
                {
                    if (c.GetType() == typeof(CheckBox))
                    {
                        ((CheckBox)c).Checked = false;
                    }

                    if (c.GetType() == typeof(ComboBox))
                    {
                        ((ComboBox)c).Text = string.Empty;
                    }

                    if (c.GetType() == typeof(TextBox))
                    {
                        ((TextBox)c).Text = string.Empty;
                    }
                }
            }
        }

        private void checkBox20_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox20.Checked)
            {
                checkBox19.Checked = false;
            }
        }

        private void checkBox19_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox19.Checked)
            {
                checkBox20.Checked = false;

                var result = MessageBox.Show("Would you like to change your response?",
                    "Is this bill readable",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question);

                if (result == DialogResult.Yes)
                {
                    checkBox19.Checked = false;
                    checkBox20.Checked = true;
                    return;
                }

                button2_Click(null, null);

                button2.Enabled = true;
            }
        }

        private void checkBox15_CheckedChanged(object sender, EventArgs e)
        {
            //if (checkBox15.Checked)
            //{
            //    ResetControl(ctrlsVendorGroup, new Control[] { checkBox15 });

            //    data.vendor_group = "None";

            //    MessageBox.Show("Please select type of bill");
            //}
        }

        private void checkBox18_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox18.Checked)
            {
                ResetControl(ctrlsVendorGroup, new Control[] {checkBox18});

                comboBox1.Text = string.Empty;
                //data.vendor_group = "E";
            }
        }

        private void checkBox17_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox17.Checked)
            {
                ResetControl(ctrlsVendorGroup, new Control[] { checkBox17 });
                comboBox1.Text = string.Empty;
                //data.vendor_group = "G";
            }
        }

        private void checkBox16_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox16.Checked)
            {
                ResetControl(ctrlsVendorGroup, new Control[] { checkBox16 });
                comboBox1.Text = string.Empty;
                //data.vendor_group = "W";
            }
        }

        private void chkbx_noZipCode_CheckedChanged(object sender, EventArgs e)
        {
            if (chkbx_noZipCode.Checked)
            {
                textBox1.Text = string.Empty;
                textBox1.Enabled = false;
            }
            else
            {
                textBox1.Enabled = true;
            }
        }

        private void checkBox6_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox6.Checked)
            {
                textBox10.Text = "None";
                textBox10.Enabled = false;

                data.Complete = 0;
                data.PendingReasons = "No Account Number";
                data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CallOutFolder);

                SaveButtonsEnabled(true);
                MessageBox.Show("Please click SAVE.");
            }
            else
            {
                textBox10.Enabled = true;
                textBox10.Text = data.AccountNumber;
                SaveButtonsEnabled(false);
            }
        }

        private void chkBx_NoPropertyAddress_CheckedChanged(object sender, EventArgs e)
        {
            if (initialize == false && chkBx_NoPropertyAddress.Checked == true)
            {
                MessageBox.Show("Please select USP on Invoice then click Save", "Instructions");

                dataGridView1.Rows.Clear();
                data.property_code = string.Empty;
                data.Complete = 0;
                data.PendingReasons = "No Property/Service Address";
                data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CallOutFolder);
            }
        }

        public string strCreateInvoiceFolderName(string folder)
        {
            string strDate = DateTime.Now.ToString("MM-dd-yyyy");

            return string.Format(@"{0}{1}\{2}.pdf", folder, strDate, data.Invoice);
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

            //string[] message = new string[] {"Greetings,", Environment.NewLine, "This is an automated email. Please do not reply back.",
            //"Please contact PhpUMAL@altisource.com for queries.", Environment.NewLine, "Warm Regards,", "Expense Management Team"};


            switch (comboBox1.Text)
            {
                case "Garbage/Trash/Disposal":

                    data.vendor_group = "T";
                    data.Complete = 1;
                    //data.PendingReasons = "Bulk Upload";
                    data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                    break;

                case "Stormwater/Drainage/WasteWater/Sewer":

                    data.vendor_group = "D";
                    data.Complete = 1;
                    //data.PendingReasons = "Bulk Upload";
                    data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                    break;

                case "Septic":

                    data.vendor_group = "S";
                    data.Complete = 1;
                    //data.PendingReasons = "Bulk Upload";
                    data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                    break;

                case "TaxBill/TaxSale/property bill":

                    var result = MessageBox.Show("Is the invoice a Tax Bill / Tax Sale / Property Bill?", "Type of Bill", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (result == DialogResult.Yes)
                    {
                        //SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "tax@ocwen.com");

                        data.vendor_group = "None";
                        data.Complete = 6;
                        data.PendingReasons = comboBox1.Text;
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);

                        button2_Click(null, null);
                    }
                    else
                    {
                        comboBox1.Text = string.Empty;
                    }
                    break;

                case "Cut Grass/weeds removal/Lawn maintenance":

                    var result1 = MessageBox.Show("Is the invoice a Cut Grass / Weeds Removal / Lawn Maintenance Bill?", "Type of Bill", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result1 == DialogResult.Yes)
                    {
                        //SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "cv-mailbox@altisource.com");

                        data.vendor_group = "None";
                        data.Complete = 6;
                        data.PendingReasons = comboBox1.Text;
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);

                        button2_Click(null, null);
                    }
                    else
                    {
                        comboBox1.Text = string.Empty;
                    }
                    break;

                case "Removal of debris":

                    var result2 = MessageBox.Show("Is the invoice a Removal of Debris Bill?", "Type of Bill", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result2 == DialogResult.Yes)
                    {
                        //SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "cv-mailbox@altisource.com");

                        data.vendor_group = "None";
                        data.Complete = 6;
                        data.PendingReasons = comboBox1.Text;
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);

                        button2_Click(null, null);
                    }
                    else
                    {
                        comboBox1.Text = string.Empty;
                    }
                    break;

                case "Association/assessment fee":

                    var result3 = MessageBox.Show("Is the invoice an Association/assessment fee?", "Type of Bill", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (result3 == DialogResult.Yes)
                    {
                        //SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "cv-mailbox@altisource.com");

                        data.vendor_group = "None";
                        data.Complete = 6;
                        data.PendingReasons = comboBox1.Text;
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);

                        button2_Click(null, null);
                    }
                    else
                    {
                        comboBox1.Text = string.Empty;
                    }
                    break;

                default:

                    data.vendor_group = "None";
                    data.Complete = 6;
                    data.PendingReasons = comboBox1.Text;
                    data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);
                    //button2_Click(null, null);
                    break;
            }
                
        }

        private void button2_Click(object sender, EventArgs e)
        {

            Control[] checkUpdateBeforeSaving = new Control[] { pct_isbillreadablecheck,pictureBox24,pictureBox39,pictureBox35,pictureBox26,pictureBox63,
                pictureBox15,pictureBox49,pictureBox52,pictureBox55,pictureBox58,pictureBox9,pictureBox46,pictureBox64,pictureBox8,pictureBox18,
                pictureBox21,pictureBox33,pictureBox40,pictureBox43,pictureBox12, pictureBox62 };

            bool hasSkipUpdate = false;

            foreach (Control x in checkUpdateBeforeSaving)
            {
                if (x.Visible == true)
                {
                    hasSkipUpdate = true;
                }
            }

            if (hasSkipUpdate)
            {
                MessageBox.Show("Please check all the fields.");

                return;
            }
            

            Control[] checkBeforeSaving = new Control[] { pct_isbillreadableapprove, pictureBox22, pictureBox36, pictureBox3,
            pictureBox27, pictureBox13, pictureBox47, pictureBox50, pictureBox53, pictureBox56, pictureBox1, pictureBox44,
            pictureBox6, pictureBox16, pictureBox19, pictureBox31, pictureBox41, pictureBox10, pictureBox66, pictureBox25 };

            bool hasControlOpened = false;

            foreach (Control c in checkBeforeSaving)
            {
                if (c.Visible == true)
                {
                    hasControlOpened = true;
                }
            }

            if (hasControlOpened)
            {
                MessageBox.Show("Please save all changes before saving.");

                return;
            }


            if ((data.PendingReasons.ToLower().Contains(@"blank/unreadable/cut invoice") == true && checkBox20.Checked == true) ||
                (data.PendingReasons.ToLower().Contains(@"blank/unreadable/cut invoice") == false && checkBox19.Checked == true))
            {
                InputAuditData("Unable to detect if bill is readable");
                data.AuditResult = "Fail";

                data.AccountNumber = "None";
                data.Amount = "0.00";
                data.vendor_group = "None";
                data.Complete = 6;
                data.PendingReasons = "Blank/Unreadable/Cut Invoice";
                data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);
            }

           
            if (checkBox18.Checked == false && checkBox17.Checked == false && checkBox16.Checked == false)
            {
                switch (comboBox1.Text)
                {
                    case "TaxBill/TaxSale/property bill":

                        //SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "tax@ocwen.com");

                        data.vendor_group = "None";
                        data.Complete = 6;
                        data.PendingReasons = comboBox1.Text;
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);

                        break;

                    case "Cut Grass/weeds removal/Lawn maintenance":

                        //SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "cv-mailbox@altisource.com");

                        data.vendor_group = "None";
                        data.Complete = 6;
                        data.PendingReasons = comboBox1.Text;
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);
                        break;

                    case "Removal of debris":


                        //SendEmail(CurrentLoadedInvoice, comboBox1.Text, message, "cv-mailbox@altisource.com");

                        data.vendor_group = "None";
                        data.Complete = 6;
                        data.PendingReasons = comboBox1.Text;
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);
                        break;

                    case "Others (Non-Billing Information)":
                        
                        data.vendor_group = "None";
                        data.Complete = 6;
                        data.PendingReasons = comboBox1.Text;
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);
                        break;

                    default:

                    //    data.vendor_group = "None";
                    //    data.Complete = 6;
                    //    data.PendingReasons = comboBox1.Text;
                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.NoActionNeededFolder);
                        break;
                }
            }
            else
            {
                if (checkBox18.Checked) {data.vendor_group = "E";}
                if (checkBox17.Checked) {data.vendor_group = "G";}
                if (checkBox16.Checked) {data.vendor_group = "W";}
            }

            button2.Enabled = false;

            ValidateAudit();

            SaveItem();

            initialize = true;

            LoadForAudit();

            UpdateReport();
        }

        private void UpdateReport()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select COUNT(*) [Pending For Audit] from tbl_ADHOC_EM_Tasks a
                                            where a.LastDateModified > '{0}' and a.lockedto <> 'SKIP'
                                            and a.Complete = 21 and a.isLatestdata = 1", DateTime.Now.Date.AddDays(-5));

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                label16.Text = rd[0].ToString();
            }

            rd.Close();
            cmd = null;

            query = string.Format(@"select COUNT(*) [Count of Audited Invoice] from tbl_ADHOC_EM_Tasks a
                                    where a.LastDateModified > '{0}'
                                    and a.AuditDate is not null
                                    and a.AuditedBy = '{1}'
                                    and a.AuditDate > '{2}'",
                                                            DateTime.Now.Date.AddDays(-5),
                                                            Environment.UserName.ToLower(),
                                                            DateTime.Now.AddHours(-12));

            cmd = new SqlCommand(query, conn);

            rd = cmd.ExecuteReader();

            rd.Read();

            if (rd.HasRows)
            {
                label18.Text = rd[0].ToString();
            }

            rd.Close();
            cmd = null;
            conn.Close();
        } 

        private void button7_Click(object sender, EventArgs e)
        {
            if (hasDoneAcctSearch)
            {

                SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, textBox16.Text, textBox17.Text, textBox1.Text);

                button5.Visible = true;

                var res = MessageBox.Show("Would you like to change the information you put in the address field?",
                    "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.Yes)
                {
                    button9_Click(null, null);
                }
                else
                {
                    data.Complete = 3;
                    data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.PropertyNotFoundFolder);
                }


                //if (textBox1.Text == string.Empty || textBox4.Text == string.Empty) { hasDoneHouseNoZipSearch = true; }

                //if (hasDoneHouseNoZipSearch)
                //{
                //    if (textBox4.Text == string.Empty || textBox16.Text == string.Empty) { hasDoneHouseNoCitySearch = true; }

                //    if (hasDoneHouseNoCitySearch)
                //    {
                //        if (textBox4.Text == string.Empty || textBox14.Text == string.Empty) { hasDoneHouseNoStreetSearch = true; }

                //        if (hasDoneHouseNoStreetSearch)
                //        {
                //            if (textBox4.Text == string.Empty || textBox17.Text == string.Empty) { hasDoneHouseNoStateSearch = true; }

                //            if (hasDoneHouseNoStateSearch)
                //            {
                //                if (string.IsNullOrEmpty(textBox4.Text) &&
                //                    string.IsNullOrEmpty(textBox14.Text) &&
                //                    string.IsNullOrEmpty(textBox16.Text) &&
                //                    string.IsNullOrEmpty(textBox17.Text) &&
                //                    string.IsNullOrEmpty(textBox1.Text))
                //                {
                //                    MessageBox.Show("Please enter address to search", "Search Property Address");
                //                    textBox4.Focus();
                //                }
                //                else
                //                {
                //                    SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, textBox16.Text, textBox17.Text, textBox1.Text);

                //                    button5.Visible = true;

                //                    var res = MessageBox.Show("Would you like to change the information you put in the address field?",
                //                        "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //                    if (res == DialogResult.Yes)
                //                    {
                //                        button2_Click(null, null);
                //                    }
                //                    else
                //                    {
                //                        data.Complete = 3;
                //                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.PropertyNotFoundFolder);
                //                    }
                //                }
                //            }
                //            else
                //            {
                //                HouseNoStateSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, "", "", textBox17.Text, "");
                //                hasDoneHouseNoStateSearch = true;

                //                if (!HouseNoStreetSearchHasResults)
                //                {
                //                    button5.Visible = true;

                //                    var res = MessageBox.Show("Would you like to change the information you put in the address field?",
                //                        "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //                    if (res == DialogResult.Yes)
                //                    {
                //                        ResetPropertyInformation();
                //                    }
                //                    else
                //                    {
                //                        data.Complete = 3;
                //                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.PropertyNotFoundFolder);

                //                        MessageBox.Show("Please click on SAVE!");
                //                    }
                //                }
                //            }
                //        }
                //        else
                //        {
                //            HouseNoStreetSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, "", "", "");
                //            hasDoneHouseNoStreetSearch = true;

                //            if (!HouseNoStreetSearchHasResults)
                //            {
                //                HouseNoStateSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, "", "", textBox17.Text, "");
                //                hasDoneHouseNoStateSearch = true;

                //                if (!HouseNoStreetSearchHasResults)
                //                {
                //                    button5.Visible = true;

                //                    var res = MessageBox.Show("Would you like to change the information you put in the address field?",
                //                        "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //                    if (res == DialogResult.Yes)
                //                    {
                //                        ResetPropertyInformation();
                //                    }
                //                    else
                //                    {
                //                        data.Complete = 3;
                //                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.PropertyNotFoundFolder);

                //                        MessageBox.Show("Please click on SAVE!");
                //                    }
                //                }
                //            }
                //        }
                //    }
                //    else
                //    {
                //        HouseNoCitySearcHasResults = SearchPropertyUsingAddress(textBox4.Text, "", textBox16.Text, "", "");
                //        hasDoneHouseNoCitySearch = true;

                //        if (!HouseNoCitySearcHasResults)
                //        {
                //            HouseNoStreetSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, "", "", "");
                //            hasDoneHouseNoStreetSearch = true;

                //            if (!HouseNoStreetSearchHasResults)
                //            {
                //                HouseNoStateSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, "", "", textBox17.Text, "");
                //                hasDoneHouseNoStateSearch = true;

                //                if (!HouseNoStreetSearchHasResults)
                //                {
                //                    button5.Visible = true;

                //                    var res = MessageBox.Show("Would you like to change the information you put in the address field?",
                //                        "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //                    if (res == DialogResult.Yes)
                //                    {
                //                        ResetPropertyInformation();
                //                    }
                //                    else
                //                    {
                //                        data.Complete = 3;
                //                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.PropertyNotFoundFolder);

                //                        MessageBox.Show("Please click on SAVE!");
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
                //else
                //{
                //    HouseNoZipSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, "","","", textBox1.Text);
                //    hasDoneHouseNoZipSearch = true;

                //    if (!HouseNoZipSearchHasResults)
                //    {
                //        HouseNoCitySearcHasResults = SearchPropertyUsingAddress(textBox4.Text, "", textBox16.Text, "", "");
                //        hasDoneHouseNoCitySearch = true;

                //        if (!HouseNoCitySearcHasResults)
                //        {
                //            HouseNoStreetSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, textBox14.Text, "", "", "");
                //            hasDoneHouseNoStreetSearch = true;

                //            if (!HouseNoStreetSearchHasResults)
                //            {
                //                HouseNoStateSearchHasResults = SearchPropertyUsingAddress(textBox4.Text, "", "", textBox17.Text, "");
                //                hasDoneHouseNoStateSearch = true;

                //                if (!HouseNoStreetSearchHasResults)
                //                {
                //                    button5.Visible = true;

                //                    var res = MessageBox.Show("Would you like to change the information you put in the address field?",
                //                        "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                //                    if (res == DialogResult.Yes)
                //                    {
                //                        ResetPropertyInformation();
                //                    }
                //                    else
                //                    {
                //                        data.Complete = 3;
                //                        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.PropertyNotFoundFolder);

                //                        MessageBox.Show("Please click on SAVE!");
                //                    }
                //                }
                //            }
                //        }
                //    }
                //}
            }
            else
            {
                if (!string.IsNullOrEmpty(textBox10.Text))
                {
                    SearchPropertyUsingAccountNumber();
                    hasDoneAcctSearch = true;

                    ResetPropertyInformation();

                    if (acctSearchHasResults)
                    {
                        button6.Visible = true;
                    }
                    else
                    {
                        button7.Visible = true;
                    }
                }
                else
                {
                    MessageBox.Show("Please type the Account Number", "Account Number Search");
                    textBox10.Focus();
                }
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            foreach (Control c in groupBox8.Controls)
            {
                if (c.GetType() == typeof(TextBox))
                {
                    ((TextBox)c).Text = string.Empty;
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex == -1) { return; }

            dataGridView1.Enabled = false;

            string full_address = (string)dataGridView1.Rows[e.RowIndex].Cells[1].Value;

            if (!full_address.ToUpper().Contains("NO PROPERTY"))
            {
                data.property_code = (string)dataGridView1.Rows[e.RowIndex].Cells[0].Value;
                string strClientCode = (string)dataGridView1.Rows[e.RowIndex].Cells[2].Value;
                data.isActive = (string)dataGridView1.Rows[e.RowIndex].Cells[3].Value == "Active" ? true : false;
                string podId = (string)dataGridView1.Rows[e.RowIndex].Cells[9].Value;

                //------------------------------
                //  Solves Client Code is empty
                //------------------------------
                if (strClientCode == "RESI" || strClientCode.Contains("Residential"))
                {
                    data.client_code = "RESI";
                }
                else
                {
                    data.client_code = strClientCode;
                }

                BackgroundWorker LoadPreviousUsp = new BackgroundWorker();
                LoadPreviousUsp.DoWork += new DoWorkEventHandler(LoadPreviousUsp_DoWork);
                LoadPreviousUsp.RunWorkerAsync();

                ////--------------------------
                //// Check if Real Resolution
                ////--------------------------
                //if (data.client_code == "RR")
                //{
                //    data.Complete = 1;
                //    data.PendingReasons = "Real Resolution";
                //    data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.RR_NEW);
                //    data.VendorAssignedStatus = "Dont Assign - RR Property";
                //}
                //else
                //{

                //    // ----------------------------
                //    // Check if Property is HSBC
                //    // ----------------------------
                //    if (data.client_code == "HSBC")
                //    {
                //        data.Complete = 1;
                //        data.PendingReasons = "HSBC Property";
                //        data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.HSBC_NEW);
                //        data.VendorAssignedStatus = "Dont Assign - HSBC Property";
                //    }
                //    else
                //    {
                //        CheckIfOutOfREO(e.RowIndex);
                //        data.isOutOfREO = isOutOfREO;
                //        // Check if Trailing Asset
                //        if (isTrailingAsset)
                //        {
                //            data.Complete = 1;
                //            data.PendingReasons = "Trailing Asset";
                //            data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.TrailingAssetFolder);
                //        }
                //        else
                //        {
                //            data.Complete = 1;
                //            data.PendingReasons = "Bulk Upload";
                //            data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                //        }

                //        //------------------------------------------------------
                //        // Solves Bulk Upload Error: Property ID does not exist
                //        //------------------------------------------------------
                //        if (data.isActive == false)
                //        {
                //            Property_Status_Change_Date = new DateTime();
                //            Property_Status_Change_Date = (DateTime)dataGridView1.Rows[e.RowIndex].Cells["cproperty_status_change_date"].Value;
                //            Inactive_Date = new DateTime();
                //            Inactive_Date = (DateTime)dataGridView1.Rows[e.RowIndex].Cells["cinactivedate"].Value;

                //            data.Complete = 1;
                //            data.PendingReasons = "Manual WI";
                //            data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                //        }

                //        //--------------------------------------------------------------------------------------------
                //        // Solves Bulk Upload Error: The specified Line Item is not mapped at Property Investor level
                //        //--------------------------------------------------------------------------------------------
                //        if (podId == "POD GSE")
                //        {
                //            data.Complete = 1;
                //            data.PendingReasons = "Manual WI";
                //            data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CompleteFolder);
                //        }
                //    }
                //}
            }
            dataGridView1.Enabled = true;
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            BackgroundWorker bwLoadVendorInDGV = new BackgroundWorker();
            bwLoadVendorInDGV.DoWork += new DoWorkEventHandler(bwLoadVendorInDGV_DoWork);
            bwLoadVendorInDGV.RunWorkerAsync(comboBox5.Text);

            while (bwLoadVendorInDGV.IsBusy)
            {
                System.Windows.Forms.Application.DoEvents();
            }

            if (comboBox5.Text.Contains("----") || comboBox5.Text.Contains("No Matching"))
            {
                //Form vendorIdRequestForm = new Forms.VendorIdRequest(this, "CREATE NEW");
                //vendorIdRequestForm.Show();

                Form vendorIdRequestForm = new VendorIDRequestForm.Form1("CREATE NEW", "", "EMS", data.Invoice, data.vendor_group);

                vendorIdRequestForm.Show();

                data.Complete = 9;
                data.PendingReasons = "Vendor ID Request";
            }
        }

        private void bwLoadVendorInDGV_DoWork(object sender, DoWorkEventArgs e)
        {
            Database.DBConnection.DatabaseVDR_Data conn = new Database.DBConnection.DatabaseVDR_Data();
            List<Database.DB.VDR> vdrData = conn.GetData(e.Argument.ToString());

            dgv_usp.Invoke((Action)delegate
            {
                dgv_usp.Visible = true;
                dgv_usp.Rows.Clear();
            });

            if (vdrData != null)
            {
                foreach (Database.DB.VDR data in vdrData)
                {
                    dgv_usp.Invoke((Action)delegate
                    {
                        dgv_usp.Rows.Add(data.Vendor_ID, data.Vendor_Name, data.Address, data.vdr_status == 1 ? "Active" : "Inactive", GetStatus(data.vms_status.ToString()));
                    });
                }
            }
            else
            {
                dgv_usp.Invoke((Action)delegate
                {
                    dgv_usp.Rows.Add(null, "NO DATA FOUND", null, null);
                });
            }
        }

        private void checkBox34_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox34.Checked)
            {
                data.final_bill = "Yes";
                textBox18.Enabled = true;
                checkBox33.Checked = false;
            }
            
        }

        private void checkBox33_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox33.Checked)
            {
                data.final_bill = "No";
                textBox18.Enabled = false;
                textBox18.Text = string.Empty;
                checkBox34.Enabled = false;
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            data.client_code = string.Empty;
            data.Complete = 3;
            data.property_code = string.Empty;
            data.userid = Environment.UserName;
            data.vendor_group = "None";
            data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.PropertyNotFoundFolder);
            data.PendingReasons = "PNF";

            dataGridView1.Enabled = false;

            button2_Click(null, null);
        }

        private void cmboBx_UspOnInvoice_SelectedIndexChanged(object sender, EventArgs e)
        {
            //button2.Enabled = false;

            if (cmboBx_UspOnInvoice.Text == "USP Not Listed" || cmboBx_UspOnInvoice.Text == "----------------------------")
            {
                Form addusp = new Forms.AddUSP();
                addusp.ShowDialog(this);
                preloadUSP();

                cmboBx_UspOnInvoice.Text = string.Empty;
            }

            if (cmboBx_UspOnInvoice.Text != string.Empty)
            {
                button2.Enabled = true;

                data.Complete = 0;
                data.PendingReasons = "No Property/Service Address";
                data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CallOutFolder);

                SaveButtonsEnabled(true);
                MessageBox.Show("Please click SAVE.");
            }
        }

        private void button_noMatchUSP_Click(object sender, EventArgs e)
        {
            //Form vendorIdRequestForm = new Forms.VendorIdRequest(this, "CREATE NEW");
            //vendorIdRequestForm.Show();

            Form vendorIdRequestForm = new VendorIDRequestForm.Form1("CREATE NEW", "", "EMS", data.Invoice, data.vendor_group);

            vendorIdRequestForm.Show();

            data.Complete = 9;
            data.PendingReasons = "Vendor ID Request";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string text = string.Empty;
            
            foreach (var reportField in data.GetType().GetProperties())
            {
                text = string.Format("{0}{1}{2}: {3}", text, Environment.NewLine, reportField.Name, reportField.GetValue(data, null));
            }

            MessageBox.Show(text);
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
            if (textBox4.Enabled == true)
            {
                data.HouseNoEntered = textBox4.Text;   
            }
        }

        private void textBox14_TextChanged(object sender, EventArgs e)
        {
            if (textBox14.Enabled == true)
            {
                data.StreetEntered = textBox14.Text;
            }
        }

        private void textBox16_TextChanged(object sender, EventArgs e)
        {
            if (textBox16.Enabled == true)
            {
                data.CityEntered = textBox16.Text;
            }
        }

        private void textBox17_TextChanged(object sender, EventArgs e)
        {
            if (textBox17.Enabled == true)
            {
                data.StateEntered = textBox17.Text;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Enabled == true)
            {
                data.ZipcodeEntered = textBox1.Text;
            }
        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {
            if (textBox10.Enabled == true)
            {
                data.AccountNumber = textBox10.Text;
            }
        }

        private void dateTimePicker9_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker9.Enabled == true)
            {
                data.invoice_date = dateTimePicker9.Value.Date;
            }
        }

        private void dateTimePicker7_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker7.Enabled == true)
            {
                data.DueDate = dateTimePicker7.Value.Date;
            }
        }

        private void dateTimePicker8_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker8.Enabled == true)
            {
                data.ServiceFrom = dateTimePicker8.Value.Date;
                dateTimePicker10.MinDate = dateTimePicker8.Value.Date;
            }
        }

        private void dateTimePicker10_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker10.Enabled == true)
            {
                data.ServiceTo = dateTimePicker10.Value.Date;
            }
        }

        private void dateTimePicker6_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker6.Enabled == true)
            {
                data.DisconnectionDate = dateTimePicker6.Value.Date;
            }
        }

        private void textBox20_TextChanged(object sender, EventArgs e)
        {
            //if (textBox20.Enabled == true)
            //{
                try
                {
                    data.PreviousBalance = Convert.ToDecimal(textBox20.Text);

                    ComputeTotalAmount();
                }
                catch
                {
                    
                }
            //}
        }

        private void ComputeTotalAmount()
        {
            decimal totalAmount = 0;

            totalAmount += Convert.ToDecimal(textBox20.Text);
            totalAmount -= Convert.ToDecimal(textBox21.Text);
            totalAmount += Convert.ToDecimal(textBox6.Text);
            totalAmount += Convert.ToDecimal(textBox23.Text);
            totalAmount += Convert.ToDecimal(textBox8.Text);

            textBox24.Text = string.Format("{0:N2}", totalAmount);
        }

        private void textBox21_TextChanged(object sender, EventArgs e)
        {
            //if (textBox21.Enabled == true)
            //{
                try
                {
                    data.PaymentReceived = Convert.ToDecimal(textBox21.Text);

                    ComputeTotalAmount();
                }
                catch
                {
                    
                }
            //}
        }

        private void textBox23_TextChanged(object sender, EventArgs e)
        {
            //if (textBox23.Enabled == true)
            //{
                try
                {
                    data.CurrentCharge = Convert.ToDecimal(textBox23.Text);

                    ComputeTotalAmount();
                }
                catch
                {
                    
                }
            //}
        }

        private void textBox6_TextChanged(object sender, EventArgs e)
        {
            //if (textBox6.Enabled == true)
            //{
                try
                {
                    data.LateFee = Convert.ToDecimal(textBox6.Text);

                    ComputeTotalAmount();
                }
                catch
                {
                    
                }
            //}
        }

        private void textBox24_TextChanged(object sender, EventArgs e)
        {
            //if (textBox24.Enabled == true)
            //{
                try
                {
                    data.Amount = textBox24.Text;
                }
                catch
                {

                }
            //}
        }

        private void textBox22_TextChanged(object sender, EventArgs e)
        {
            if (textBox22.Enabled == true)
            {
                try
                {
                    data.AmountAfterDue = Convert.ToDecimal(textBox22.Text);
                }
                catch
                {
                    MessageBox.Show("Incorrect value for LateFee");
                }
            }
        }

        private void checkBox35_CheckedChanged(object sender, EventArgs e)
        {
            if (initialize == false)
            {
                if (checkBox35.Checked == true)
                {
                    data.Complete = 0;
                    data.PendingReasons = "No Amount";
                    data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CallOutFolder);

                    textBox24.Text = "0.00";

                    SaveButtonsEnabled(true);

                    MessageBox.Show("Please click SAVE.", "Instructions", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    if (data.PendingReasons == "No Amount")
                    {
                        data.PendingReasons = "";
                    }
                }
            }
        }

        private void checkBox30_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox30.Enabled == true)
            {
                data.invoice_date = DateTime.MinValue;
                dateTimePicker9.Enabled = false;
            }
            else
            {
                dateTimePicker9.Enabled = true;
            }
        }

        private void checkBox32_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox32.Enabled == true)
            {
                data.DueDate = DateTime.MinValue;
                dateTimePicker7.Enabled = false;
            }
            else
            {
                dateTimePicker7.Enabled = true;
            }
        }

        private void checkBox31_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox31.Enabled == true)
            {
                data.ServiceFrom = DateTime.MinValue;
                data.ServiceTo = DateTime.MinValue;
                dateTimePicker8.Enabled = false;
                dateTimePicker10.Enabled = false;
            }
            else
            {
                dateTimePicker8.Enabled = true;
                dateTimePicker10.Enabled = true;
            }
        }

        private void checkBox28_CheckedChanged(object sender, EventArgs e)
        {
            if (data.PendingReasons == "Do Not Mail")
            {
                data.PendingReasons = string.Empty;
            }
        }

        private void checkBox29_CheckedChanged(object sender, EventArgs e)
        {
            if (initialize == false && checkBox29.Checked == true)
            {
                data.Complete = 0;
                data.PendingReasons = "Do Not Mail";
                data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CallOutFolder);

                MessageBox.Show("Please click SAVE.", "Instructions", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void checkBox8_CheckedChanged(object sender, EventArgs e)
        {
            if (initialize == false && checkBox8.Checked == true)
            {
                data.Complete = 0;
                data.usp_name = string.Empty;
                data.VendorId = string.Empty;
                data.PendingReasons = "No USP Address on Invoice";
                data.InvoiceFolderName = strCreateInvoiceFolderName(Constants.CallOutFolder);
                dgv_usp.Rows.Clear();

                MessageBox.Show("Please click SAVE.", "Instructions", MessageBoxButtons.OK, MessageBoxIcon.Information);

                SaveButtonsEnabled(true);
            }
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {
            if (textBox8.Enabled == true)
            {
                try
                {
                    data.DisconnectionFee = Convert.ToDecimal(textBox6.Text);

                    ComputeTotalAmount();
                }
                catch
                {

                }
            }
        }

        private void Audit_Load(object sender, EventArgs e)
        {
            BackgroundWorker bwGetVdrList = new BackgroundWorker();
            bwGetVdrList.DoWork += new DoWorkEventHandler(bwGetVdrList_DoWork);
            bwGetVdrList.RunWorkerAsync();

            BackgroundWorker bwLoadUSP = new BackgroundWorker();
            bwLoadUSP.DoWork += new DoWorkEventHandler(bwLoadUSP_DoWork);
            bwLoadUSP.RunWorkerAsync();

            bwRscApproval = new BackgroundWorker();
            bwRscApproval.DoWork += new DoWorkEventHandler(bwRscApproval_DoWork);

            
          LoadForAudit();

            dateTimePicker10.MinDate = DateTime.Today.Date.AddYears(-2);
            dateTimePicker9.MinDate = DateTime.Today.Date.AddYears(-2);
            dateTimePicker8.MinDate = DateTime.Today.Date.AddYears(-2);
            dateTimePicker7.MinDate = DateTime.Today.Date.AddYears(-2);

            dateTimePicker10.MaxDate = DateTime.Today.Date.AddYears(2);
            dateTimePicker9.MaxDate = DateTime.Today.Date.AddYears(2);
            dateTimePicker8.MaxDate = DateTime.Today.Date.AddYears(2);
            dateTimePicker7.MaxDate = DateTime.Today.Date.AddYears(2);
            
        }

        private void HideCheck(object sender, EventArgs e)
        {
            if (sender.GetType() == typeof(PictureBox))
            {
                ((PictureBox)sender).Visible = false;
            }
        }

        private void webBrowser1_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {

        }

        private void SendEmail(string strAttachment, string strSubject, string[] strMessage, string strSendTo)
        {
            try
            {
                MailMessage message = new MailMessage();
                message.From = new MailAddress(UserPrincipal.Current.EmailAddress);
                message.To.Add(strSendTo);
                message.CC.Add("DEV_MNL@altisource.com");
                message.Subject = strSubject;
                message.Body = string.Empty;

                foreach (string msg in strMessage)
                {
                    message.Body += msg + Environment.NewLine;
                }

                Attachment data = new Attachment(strAttachment, MediaTypeNames.Application.Octet);

                message.Attachments.Add(data);
                message.Attachments[0].Name = System.IO.Path.GetFileNameWithoutExtension(strAttachment).Split('_')[0] + ".pdf";

                SmtpClient client = new SmtpClient("Internal-Mail.ascorp.com");
                client.Credentials = CredentialCache.DefaultNetworkCredentials;
                client.Send(message);

                data.Dispose();
            }
            catch
            {
            }
        }

        private void pictureBox65_Click(object sender, EventArgs e)
        {
            
            ChangeControlState(ctrlsTransAmt, pictureBox65, true);

            data.AuditResult = "Fail";
            InputAuditData("Wrong tagging of Transfer Amount");
        }

        private void pictureBox66_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsTransAmt, pictureBox65, false);        
        }

        private void pictureBox30_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsASFI, pictureBox25, pictureBox62, true);

            LoadBillName();
        }

        private void pictureBox62_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsASFI, pictureBox62, false);
        }

        public void LoadBillName()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);
            conn.Open();

            string query = @"SELECT distinct RTRIM(BillName) BillName FROM [MIS_ALTI].[dbo].[tbl_EM_BillName] ORDER BY [BillName]";

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            if (cmbBillName.InvokeRequired)
            {
                cmbBillName.Invoke((Action)delegate
                {
                    cmbBillName.Items.Clear();
                    cmbBillName.Items.Add("---- Not Listed ----");

                    try
                    {

                        while (rd.Read())
                        {
                            cmbBillName.Items.Add(rd[0].ToString());
                        }

                        cmbBillName.Items.Add("---- Not Listed ----");
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Unable to pull up list.. Error: " + ex.Message);
                    }
                });
            }
            else
            {
                cmbBillName.Items.Clear();
                cmbBillName.Items.Add("---- Not Listed ----");

                try
                {
                    while (rd.Read())
                    {
                        cmbBillName.Items.Add(rd[0].ToString());
                    }

                    cmbBillName.Items.Add("---- Not Listed ----");
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to pull up list. Error: " + ex.Message);
                }
            }

            conn.Close();

        }

        private void cmbBillName_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cmbBillName.Text == "---- Not Listed ----")
            {
                cmbBillName.Items.Clear();
                btnAdd.Enabled = false;
                this.cmbBillName.DropDownStyle = ComboBoxStyle.Simple;
                btnAdd.Visible = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {

            SqlConnection sqlcon = new SqlConnection(Constants.connectionString);

            try
            {
                sqlcon.Open();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save data. Error: {0}", ex.Message);

                sqlcon.Close();
                return;
            }


            string query = string.Format(@"insert into tbl_EM_BillName (BillName, CreatedBy, CreatedDatetime)
                                            values ('{0}','{1}',getdate()) ",
                                                   cmbBillName.Text,
                                                   Environment.UserName.ToLower());

            SqlCommand sqlcom = new SqlCommand(query, sqlcon);

            try
            {
                sqlcom.ExecuteNonQuery();

                LoadBillName();

                this.cmbBillName.DropDownStyle = ComboBoxStyle.DropDown;
                
                btnAdd.Visible = false;

                if (cmbBillName.Text != "ASFI or c/o ASFI")
                {
                    data.isAddressedToASFI = false;
                    data.BillTo = cmbBillName.Text;

                    data.Complete = 6;
                    data.PendingReasons = "Non-ASFI Invoice";
                }
                else
                {
                    data.isAddressedToASFI = true;
                    data.BillTo = "ASFI or c/o ASFI";
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save the data. Error: " + ex.Message);

                sqlcon.Close();

                return;
            }        
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.cmbBillName.DropDownStyle = ComboBoxStyle.DropDown;
            cmbBillName.Text = "ASFI or c/o ASFI";
            LoadBillName();
            btnAdd.Visible = false;
        }

        private void cmbBillName_TextChanged(object sender, EventArgs e)
        {
            if (cmbBillName.Text.Trim() != string.Empty)
            {
                btnAdd.Enabled = true;
            }
            else
            {
                btnAdd.Enabled = false;
            }
        }
        

        private void pictureBox67_Click(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsnASFI, pictureBox67, false);
        }

        private void pictureBox68_Click_1(object sender, EventArgs e)
        {
            ChangeControlState(ctrlsnASFI, pictureBox70, pictureBox67, true);
        }

        private void chkNYes_CheckedChanged(object sender, EventArgs e)
        {
            if (chkNYes.Checked == true)
            {
                data.isAddressedToASFI = true;
                chknNo.Checked = false;
                chkNYes.Checked = true;
                data.BillTo = cmbBillName.Text.ToString();
            }
            else
            {
                data.isAddressedToASFI = false;
                chkNYes.Checked = false;
                chknNo.Checked = true;
                data.BillTo = cmbBillName.Text.ToString();
            }

        }

        private void chknNo_CheckedChanged(object sender, EventArgs e)
        {
            if (chknNo.Checked == true)
            {
                data.isAddressedToASFI = false;
                chkNYes.Checked = false;
                chknNo.Checked = true;
                data.BillTo = cmbBillName.Text.ToString();
            }
            else
            {
                data.isAddressedToASFI = true;
                chknNo.Checked = false;
                chkNYes.Checked = true;
                data.BillTo = cmbBillName.Text.ToString();
            }
        }

        private void btnSearchUSP_Click(object sender, EventArgs e)
        {
            Database.DBConnection.DatabaseVDR_Data conn = new Database.DBConnection.DatabaseVDR_Data();
            List<Database.DB.VDR> vdrData = conn.GetDataWithAddress(comboBox5.Text.Trim(), txtUSPStrt.Text.Trim(), txtUSPCity.Text.Trim(), txtUSPState.Text.Trim(), txtUSPZip.Text.Trim());

            dgv_usp.Invoke((Action)delegate
            {
                dgv_usp.Visible = true;
                dgv_usp.Rows.Clear();
            });

            if (vdrData != null)
            {
                foreach (Database.DB.VDR data in vdrData)
                {
                    dgv_usp.Invoke((Action)delegate
                    {
                        dgv_usp.Rows.Add(data.Vendor_ID, data.Vendor_Name, data.Address + ", " + data.City + ", " + data.State + ", " + data.Zipcode, data.vdr_status == 1 ? "Active" : "Inactive", GetStatus(data.vms_status.ToString()));
                    });
                }

            }
            else
            {
                dgv_usp.Invoke((Action)delegate
                {
                    dgv_usp.Rows.Add(null, "NO DATA FOUND", null, null);
                });
            }
        }


        string GetStatus(string val)
        {

            string x = "-";

            switch (val)
            {
                case "0":
                    x = "Inactive";
                    break;
                case "1":
                    x = "Active";
                    break;
                case "2":
                    x = "-";
                    break;
            }

            return x;
        }
                
    }

    
}
