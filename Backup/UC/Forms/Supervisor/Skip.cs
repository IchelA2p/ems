﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms.Supervisor
{
    public partial class Skip : Form
    {
        Timer myTimer;
        BackgroundWorker bwLoad;

        public Skip()
        {
            InitializeComponent();

            myTimer = new Timer();
            myTimer.Interval = 3600000;
            myTimer.Tick += new EventHandler(myTimer_Tick);
            myTimer.Start();

            bwLoad = new BackgroundWorker();
            bwLoad.DoWork += new DoWorkEventHandler(bwLoad_DoWork);
        }

        protected void bwLoad_DoWork(object sender, DoWorkEventArgs e)
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select a.lockedto, a.Invoice, a.InvoiceFolderName, b.current_queue
                                            from tbl_ADHOC_EM_Tasks a left join tbl_EM_Queue b on a.lockedto = b.userid
                                            where a.lockedto is not null 
                                            order by a.lockedto");

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            dataGridView1.Invoke((Action)delegate {
                dataGridView1.Rows.Clear();
            });
            
            while (rd.Read())
            {
                dataGridView1.Invoke((Action)delegate
                {
                    dataGridView1.Rows.Add(false, rd[0], rd[1], rd[2], rd[3]);
                });
            }

            conn.Close();
        }

        protected void myTimer_Tick(object sender, EventArgs e)
        {
            if (!bwLoad.IsBusy)
            {
                bwLoad.RunWorkerAsync();
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex > -1 && e.ColumnIndex == 2)
            {
                if (System.IO.File.Exists(dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString()))
                {
                    webBrowser1.Navigate(dataGridView1.Rows[e.RowIndex].Cells[3].Value.ToString());
                }
            }
        }

        private void Skip_Load(object sender, EventArgs e)
        {
            bwLoad.RunWorkerAsync();
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                dr.Cells[0].Value = true;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (DataGridViewRow dr in dataGridView1.Rows)
            {
                if ((bool)dr.Cells[0].Value == true)
                {
                    SqlConnection conn = new SqlConnection(Constants.connectionString);

                    conn.Open();

                    string query = string.Format(@"update tbl_ADHOC_EM_Tasks 
                                                    set lockedto = 'SKIP'
                                                    where lockedto = '{0}'
                                                    and Invoice = '{1}'

                                                    insert into tbl_EM_InvoiceSkipLog (invoice, skippedby, skiptimestamp, lockedto)
                                                    values ('{1}', '{2}', GETDATE(), '{0}')",
                                                    dr.Cells[1].Value.ToString(),
                                                    dr.Cells[2].Value.ToString(),
                                                    Environment.UserName);

                    SqlCommand cmd = new SqlCommand(query, conn);

                    cmd.ExecuteNonQuery();

                    cmd = null;
                    conn.Close();
                }
            }

            MessageBox.Show("Done!");

            if (!bwLoad.IsBusy)
            {
                bwLoad.RunWorkerAsync();
            }
        }
    }
}
