﻿namespace UC.Forms.Supervisor
{
    partial class frmHighAmountApproval
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ClientCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DateProcessed = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.LateFee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Aging = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PreviousBalance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.CurrentCharge = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PropertyCode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Location = new System.Drawing.Point(12, 4);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(644, 647);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Invoice";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dataGridView1);
            this.groupBox2.Location = new System.Drawing.Point(662, 44);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(655, 607);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Pending";
            // 
            // dataGridView1
            // 
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ClientCode,
            this.DateProcessed,
            this.LateFee,
            this.Aging,
            this.PreviousBalance,
            this.Amount,
            this.CurrentCharge,
            this.PropertyCode});
            this.dataGridView1.Location = new System.Drawing.Point(6, 21);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(643, 580);
            this.dataGridView1.TabIndex = 0;
            // 
            // ClientCode
            // 
            this.ClientCode.HeaderText = "Client Code";
            this.ClientCode.Name = "ClientCode";
            // 
            // DateProcessed
            // 
            this.DateProcessed.HeaderText = "Date Processed";
            this.DateProcessed.Name = "DateProcessed";
            // 
            // LateFee
            // 
            this.LateFee.HeaderText = "Late Fee";
            this.LateFee.Name = "LateFee";
            // 
            // Aging
            // 
            this.Aging.HeaderText = "Aging (Days)";
            this.Aging.Name = "Aging";
            // 
            // PreviousBalance
            // 
            this.PreviousBalance.HeaderText = "Previous Balance";
            this.PreviousBalance.Name = "PreviousBalance";
            // 
            // Amount
            // 
            this.Amount.HeaderText = "Amount";
            this.Amount.Name = "Amount";
            // 
            // CurrentCharge
            // 
            this.CurrentCharge.HeaderText = "Current Charge";
            this.CurrentCharge.Name = "CurrentCharge";
            // 
            // PropertyCode
            // 
            this.PropertyCode.HeaderText = "Property Code";
            this.PropertyCode.Name = "PropertyCode";
            // 
            // frmHighAmountApproval
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 14F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1500, 663);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("Calibri", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Name = "frmHighAmountApproval";
            this.Text = "High Amount Invoice Approval Form";
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn ClientCode;
        private System.Windows.Forms.DataGridViewTextBoxColumn DateProcessed;
        private System.Windows.Forms.DataGridViewTextBoxColumn LateFee;
        private System.Windows.Forms.DataGridViewTextBoxColumn Aging;
        private System.Windows.Forms.DataGridViewTextBoxColumn PreviousBalance;
        private System.Windows.Forms.DataGridViewTextBoxColumn Amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn CurrentCharge;
        private System.Windows.Forms.DataGridViewTextBoxColumn PropertyCode;
    }
}