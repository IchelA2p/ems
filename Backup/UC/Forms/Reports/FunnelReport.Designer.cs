﻿namespace UC.Forms.Reports
{
    partial class FunnelReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgv_inflow = new System.Windows.Forms.DataGridView();
            this.inflow_desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.inflow_count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_workitemscreated = new System.Windows.Forms.DataGridView();
            this.wicreated_category = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wicreated_subcategory = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.wicreated_amount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_workitemswillnotbecreated = new System.Windows.Forms.DataGridView();
            this.winotcreated_cat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.winotcreated_cat2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.winotcreated_amt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.dgv_posssibleworkitemcreation = new System.Windows.Forms.DataGridView();
            this.possiblewicreation_cat1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.possiblewicreation_cat2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.possiblewicreation_amt = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dgv_summary_inflow = new System.Windows.Forms.DataGridView();
            this.description = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.percentage = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dgv_bulkuploaderrors = new System.Windows.Forms.DataGridView();
            this.bulkuploaderror_desc = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.bulkuploaderror_count = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.dgv_assignvendorerrors = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_inflow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_workitemscreated)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_workitemswillnotbecreated)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_posssibleworkitemcreation)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_summary_inflow)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_bulkuploaderrors)).BeginInit();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_assignvendorerrors)).BeginInit();
            this.SuspendLayout();
            // 
            // dgv_inflow
            // 
            this.dgv_inflow.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_inflow.BackgroundColor = System.Drawing.Color.White;
            this.dgv_inflow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_inflow.ColumnHeadersVisible = false;
            this.dgv_inflow.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.inflow_desc,
            this.inflow_count});
            this.dgv_inflow.Location = new System.Drawing.Point(6, 19);
            this.dgv_inflow.Name = "dgv_inflow";
            this.dgv_inflow.RowHeadersVisible = false;
            this.dgv_inflow.Size = new System.Drawing.Size(571, 131);
            this.dgv_inflow.TabIndex = 0;
            // 
            // inflow_desc
            // 
            this.inflow_desc.HeaderText = "desc";
            this.inflow_desc.Name = "inflow_desc";
            // 
            // inflow_count
            // 
            this.inflow_count.HeaderText = "count";
            this.inflow_count.Name = "inflow_count";
            // 
            // dgv_workitemscreated
            // 
            this.dgv_workitemscreated.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_workitemscreated.BackgroundColor = System.Drawing.Color.White;
            this.dgv_workitemscreated.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_workitemscreated.ColumnHeadersVisible = false;
            this.dgv_workitemscreated.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.wicreated_category,
            this.wicreated_subcategory,
            this.wicreated_amount});
            this.dgv_workitemscreated.Location = new System.Drawing.Point(6, 19);
            this.dgv_workitemscreated.Name = "dgv_workitemscreated";
            this.dgv_workitemscreated.RowHeadersVisible = false;
            this.dgv_workitemscreated.Size = new System.Drawing.Size(571, 173);
            this.dgv_workitemscreated.TabIndex = 6;
            // 
            // wicreated_category
            // 
            this.wicreated_category.HeaderText = "category";
            this.wicreated_category.Name = "wicreated_category";
            // 
            // wicreated_subcategory
            // 
            this.wicreated_subcategory.HeaderText = "subcategory";
            this.wicreated_subcategory.Name = "wicreated_subcategory";
            // 
            // wicreated_amount
            // 
            this.wicreated_amount.HeaderText = "amount";
            this.wicreated_amount.Name = "wicreated_amount";
            // 
            // dgv_workitemswillnotbecreated
            // 
            this.dgv_workitemswillnotbecreated.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_workitemswillnotbecreated.BackgroundColor = System.Drawing.Color.White;
            this.dgv_workitemswillnotbecreated.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_workitemswillnotbecreated.ColumnHeadersVisible = false;
            this.dgv_workitemswillnotbecreated.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.winotcreated_cat,
            this.winotcreated_cat2,
            this.winotcreated_amt});
            this.dgv_workitemswillnotbecreated.Location = new System.Drawing.Point(6, 19);
            this.dgv_workitemswillnotbecreated.Name = "dgv_workitemswillnotbecreated";
            this.dgv_workitemswillnotbecreated.RowHeadersVisible = false;
            this.dgv_workitemswillnotbecreated.Size = new System.Drawing.Size(571, 220);
            this.dgv_workitemswillnotbecreated.TabIndex = 8;
            // 
            // winotcreated_cat
            // 
            this.winotcreated_cat.HeaderText = "cat1";
            this.winotcreated_cat.Name = "winotcreated_cat";
            // 
            // winotcreated_cat2
            // 
            this.winotcreated_cat2.HeaderText = "cat2";
            this.winotcreated_cat2.Name = "winotcreated_cat2";
            // 
            // winotcreated_amt
            // 
            this.winotcreated_amt.HeaderText = "amt";
            this.winotcreated_amt.Name = "winotcreated_amt";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.dgv_workitemscreated);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox1.Location = new System.Drawing.Point(361, 174);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(583, 199);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Work Items Created";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.dgv_posssibleworkitemcreation);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox2.Location = new System.Drawing.Point(950, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(611, 612);
            this.groupBox2.TabIndex = 10;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "For Possible Work Item Creation";
            // 
            // dgv_posssibleworkitemcreation
            // 
            this.dgv_posssibleworkitemcreation.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_posssibleworkitemcreation.BackgroundColor = System.Drawing.Color.White;
            this.dgv_posssibleworkitemcreation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_posssibleworkitemcreation.ColumnHeadersVisible = false;
            this.dgv_posssibleworkitemcreation.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.possiblewicreation_cat1,
            this.possiblewicreation_cat2,
            this.possiblewicreation_amt});
            this.dgv_posssibleworkitemcreation.Location = new System.Drawing.Point(6, 19);
            this.dgv_posssibleworkitemcreation.Name = "dgv_posssibleworkitemcreation";
            this.dgv_posssibleworkitemcreation.RowHeadersVisible = false;
            this.dgv_posssibleworkitemcreation.Size = new System.Drawing.Size(599, 587);
            this.dgv_posssibleworkitemcreation.TabIndex = 11;
            // 
            // possiblewicreation_cat1
            // 
            this.possiblewicreation_cat1.HeaderText = "cat1";
            this.possiblewicreation_cat1.Name = "possiblewicreation_cat1";
            // 
            // possiblewicreation_cat2
            // 
            this.possiblewicreation_cat2.HeaderText = "cat2";
            this.possiblewicreation_cat2.Name = "possiblewicreation_cat2";
            // 
            // possiblewicreation_amt
            // 
            this.possiblewicreation_amt.HeaderText = "amt";
            this.possiblewicreation_amt.Name = "possiblewicreation_amt";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.dgv_workitemswillnotbecreated);
            this.groupBox3.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox3.Location = new System.Drawing.Point(361, 379);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(583, 245);
            this.groupBox3.TabIndex = 11;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Work Items Will Not Be Created";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.dgv_inflow);
            this.groupBox4.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox4.Location = new System.Drawing.Point(361, 12);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(583, 156);
            this.groupBox4.TabIndex = 12;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Inflow";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.dgv_summary_inflow);
            this.groupBox5.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox5.Location = new System.Drawing.Point(12, 174);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(343, 199);
            this.groupBox5.TabIndex = 13;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Summary";
            // 
            // dgv_summary_inflow
            // 
            this.dgv_summary_inflow.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_summary_inflow.BackgroundColor = System.Drawing.Color.White;
            this.dgv_summary_inflow.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_summary_inflow.ColumnHeadersVisible = false;
            this.dgv_summary_inflow.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.description,
            this.count,
            this.percentage});
            this.dgv_summary_inflow.Location = new System.Drawing.Point(6, 19);
            this.dgv_summary_inflow.Name = "dgv_summary_inflow";
            this.dgv_summary_inflow.RowHeadersVisible = false;
            this.dgv_summary_inflow.Size = new System.Drawing.Size(331, 173);
            this.dgv_summary_inflow.TabIndex = 1;
            // 
            // description
            // 
            this.description.FillWeight = 200F;
            this.description.HeaderText = "description";
            this.description.Name = "description";
            // 
            // count
            // 
            this.count.HeaderText = "count";
            this.count.Name = "count";
            // 
            // percentage
            // 
            this.percentage.HeaderText = "percentage";
            this.percentage.Name = "percentage";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.dateTimePicker1);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.button1);
            this.groupBox6.Controls.Add(this.comboBox2);
            this.groupBox6.Controls.Add(this.comboBox1);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox6.Location = new System.Drawing.Point(12, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(337, 156);
            this.groupBox6.TabIndex = 14;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Report Filter";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(129, 116);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "Show Report";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "2015",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020",
            "2021",
            "2022",
            "2023",
            "2024",
            "2025"});
            this.comboBox2.Location = new System.Drawing.Point(200, 58);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(80, 24);
            this.comboBox2.TabIndex = 2;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
            "August",
            "September",
            "October",
            "November",
            "December"});
            this.comboBox1.Location = new System.Drawing.Point(58, 58);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(136, 24);
            this.comboBox1.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(56, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Select month to show:";
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.dgv_bulkuploaderrors);
            this.groupBox7.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox7.Location = new System.Drawing.Point(361, 630);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(583, 187);
            this.groupBox7.TabIndex = 15;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Bulk Upload Errors";
            // 
            // dgv_bulkuploaderrors
            // 
            this.dgv_bulkuploaderrors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_bulkuploaderrors.BackgroundColor = System.Drawing.Color.White;
            this.dgv_bulkuploaderrors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_bulkuploaderrors.ColumnHeadersVisible = false;
            this.dgv_bulkuploaderrors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bulkuploaderror_desc,
            this.bulkuploaderror_count});
            this.dgv_bulkuploaderrors.Location = new System.Drawing.Point(6, 19);
            this.dgv_bulkuploaderrors.Name = "dgv_bulkuploaderrors";
            this.dgv_bulkuploaderrors.RowHeadersVisible = false;
            this.dgv_bulkuploaderrors.Size = new System.Drawing.Size(571, 160);
            this.dgv_bulkuploaderrors.TabIndex = 0;
            // 
            // bulkuploaderror_desc
            // 
            this.bulkuploaderror_desc.HeaderText = "desc";
            this.bulkuploaderror_desc.Name = "bulkuploaderror_desc";
            // 
            // bulkuploaderror_count
            // 
            this.bulkuploaderror_count.HeaderText = "count";
            this.bulkuploaderror_count.Name = "bulkuploaderror_count";
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.dgv_assignvendorerrors);
            this.groupBox8.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.groupBox8.Location = new System.Drawing.Point(950, 630);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(611, 187);
            this.groupBox8.TabIndex = 16;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Assign Vendor Errors";
            // 
            // dgv_assignvendorerrors
            // 
            this.dgv_assignvendorerrors.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv_assignvendorerrors.BackgroundColor = System.Drawing.Color.White;
            this.dgv_assignvendorerrors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_assignvendorerrors.ColumnHeadersVisible = false;
            this.dgv_assignvendorerrors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1,
            this.dataGridViewTextBoxColumn2});
            this.dgv_assignvendorerrors.Location = new System.Drawing.Point(6, 19);
            this.dgv_assignvendorerrors.Name = "dgv_assignvendorerrors";
            this.dgv_assignvendorerrors.RowHeadersVisible = false;
            this.dgv_assignvendorerrors.Size = new System.Drawing.Size(599, 160);
            this.dgv_assignvendorerrors.TabIndex = 0;
            // 
            // dataGridViewTextBoxColumn1
            // 
            this.dataGridViewTextBoxColumn1.HeaderText = "desc";
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dataGridViewTextBoxColumn2
            // 
            this.dataGridViewTextBoxColumn2.HeaderText = "count";
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(56, 90);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 16);
            this.label2.TabIndex = 4;
            this.label2.Text = "Cut-Off Date:";
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.CustomFormat = "MM/dd/yyyy HH:mm";
            this.dateTimePicker1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimePicker1.Location = new System.Drawing.Point(152, 88);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(128, 22);
            this.dateTimePicker1.TabIndex = 5;
            // 
            // FunnelReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1567, 823);
            this.Controls.Add(this.groupBox8);
            this.Controls.Add(this.groupBox7);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FunnelReport";
            this.ShowIcon = false;
            this.Text = "Funnel Report MTD";
            this.Load += new System.EventHandler(this.FunnelReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_inflow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_workitemscreated)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_workitemswillnotbecreated)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_posssibleworkitemcreation)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_summary_inflow)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_bulkuploaderrors)).EndInit();
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_assignvendorerrors)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgv_inflow;
        private System.Windows.Forms.DataGridView dgv_workitemscreated;
        private System.Windows.Forms.DataGridView dgv_workitemswillnotbecreated;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.DataGridView dgv_posssibleworkitemcreation;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.DataGridView dgv_summary_inflow;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn inflow_desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn inflow_count;
        private System.Windows.Forms.DataGridViewTextBoxColumn wicreated_category;
        private System.Windows.Forms.DataGridViewTextBoxColumn wicreated_subcategory;
        private System.Windows.Forms.DataGridViewTextBoxColumn wicreated_amount;
        private System.Windows.Forms.DataGridViewTextBoxColumn winotcreated_cat;
        private System.Windows.Forms.DataGridViewTextBoxColumn winotcreated_cat2;
        private System.Windows.Forms.DataGridViewTextBoxColumn winotcreated_amt;
        private System.Windows.Forms.DataGridViewTextBoxColumn possiblewicreation_cat1;
        private System.Windows.Forms.DataGridViewTextBoxColumn possiblewicreation_cat2;
        private System.Windows.Forms.DataGridViewTextBoxColumn possiblewicreation_amt;
        private System.Windows.Forms.DataGridViewTextBoxColumn description;
        private System.Windows.Forms.DataGridViewTextBoxColumn count;
        private System.Windows.Forms.DataGridViewTextBoxColumn percentage;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.DataGridView dgv_bulkuploaderrors;
        private System.Windows.Forms.DataGridViewTextBoxColumn bulkuploaderror_desc;
        private System.Windows.Forms.DataGridViewTextBoxColumn bulkuploaderror_count;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DataGridView dgv_assignvendorerrors;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.Label label2;

    }
}