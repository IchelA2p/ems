﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms.Reports
{
    public partial class FunnelReport : Form
    {
        Timer t;

        BackgroundWorker bwComputeInflow;
        BackgroundWorker bwComputeWorkItemsCreated;
        BackgroundWorker bwComputeWorkItemsWillNotBeCreated;
        BackgroundWorker bwPossibleWorkItemCreation;
        BackgroundWorker bwComputeSummary;
        BackgroundWorker bwComputeBulkUploadErrors;
        BackgroundWorker bwComputeAssignVendorErrors;

        DateTime reportDt;

        public class report
        {
            public string category;
            public string subcategory;
            public int number;
        }

        public FunnelReport()
        {
            InitializeComponent();

            bwComputeInflow = new BackgroundWorker();
            bwComputeInflow.DoWork += new DoWorkEventHandler(bwComputeInflowManual_DoWork);

            bwComputeWorkItemsCreated = new BackgroundWorker();
            bwComputeWorkItemsCreated.DoWork += new DoWorkEventHandler(bwComputeWorkItemsCreated_DoWork);

            bwComputeWorkItemsWillNotBeCreated = new BackgroundWorker();
            bwComputeWorkItemsWillNotBeCreated.DoWork += new DoWorkEventHandler(bwComputeWorkItemsWillNotBeCreated_DoWork);

            bwPossibleWorkItemCreation = new BackgroundWorker();
            bwPossibleWorkItemCreation.DoWork += new DoWorkEventHandler(bwPossibleWorkItemCreation_DoWork);

            bwComputeSummary = new BackgroundWorker();
            bwComputeSummary.DoWork += new DoWorkEventHandler(bwComputeSummary_DoWork);

            bwComputeBulkUploadErrors = new BackgroundWorker();
            bwComputeBulkUploadErrors.DoWork += new DoWorkEventHandler(bwComputeBulkUploadErrors_DoWork);

            bwComputeAssignVendorErrors = new BackgroundWorker();
            bwComputeAssignVendorErrors.DoWork += new DoWorkEventHandler(bwComputeAssignVendorErrors_DoWork);
        }

        void t_Tick(object sender, EventArgs e)
        {
            try
            {
                bwComputeInflow.RunWorkerAsync();
                bwComputeWorkItemsCreated.RunWorkerAsync();
                bwComputeWorkItemsWillNotBeCreated.RunWorkerAsync();
                bwPossibleWorkItemCreation.RunWorkerAsync();
                bwComputeSummary.RunWorkerAsync();
                bwComputeBulkUploadErrors.RunWorkerAsync();
                bwComputeAssignVendorErrors.RunWorkerAsync();
            }
            catch (Exception ex)
            {
               
            }           
        }

        void bwComputeSummary_DoWork(object sender, DoWorkEventArgs e)
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);
            conn.Open();

            decimal totalInflow = 0;
            decimal createdWorkItem = 0;
            decimal possibleWorkItemCreation = 0;
            decimal workItemWillNotBeCreated = 0;
            decimal bulkUploadError = 0;
            decimal assignVendorError = 0;
            decimal totalOutflow = 0;
            decimal wrongInformation = 0;

            string query = string.Format(@"SELECT COUNT(a.Invoice) [count]
                                              FROM tbl_ADHOC_EM_Tasks a
                                              JOIN tbl_UC_Status b ON 
                                              a.Complete = b.StatusCodes
                                              WHERE a.Invoice LIKE '{0}%'
                                              AND a.Complete = 20
                                              AND a.LastDateModified < '{1}'",
                                              reportDt.ToString("yyyyMM"),
                                              dateTimePicker1.Value);

            
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader rd = cmd.ExecuteReader();
            rd.Read();
            
            if (rd.HasRows)
            {
                totalInflow = Convert.ToDecimal(rd[0].ToString());
            }

            if (reportDt == new DateTime(2015,2,1).Date)
            {
                totalInflow = 19671;
            }

            query = string.Format(@"SELECT COUNT(a.Invoice) [count]
                                              FROM tbl_ADHOC_EM_Tasks a
                                              JOIN tbl_UC_Status b ON 
                                              a.Complete = b.StatusCodes
                                              WHERE a.Invoice Like '{0}%'
                                              AND a.isLatestData = 1
                                              AND a.work_item <> ''
                                              AND a.Complete not in (3,6)
                                              AND a.PendingReasons NOT IN ('Zero or Negative Value','Inactive Property','Out of REO','Duplicate','Filtered by 21 Day','HSBC Property','Property Status: Bankruptcy', 'Property status is Bankruptcy','Real Resolution','Trailing Asset','Pending for Audit')
                                              AND a.LastDateModified < '{1}'",
                                      reportDt.ToString("yyyyMM"),
                                      dateTimePicker1.Value);

            rd.Close();
            cmd = new SqlCommand(query, conn);
            rd = cmd.ExecuteReader();
            rd.Read();

            createdWorkItem = Convert.ToDecimal(rd[0].ToString());

            query = string.Format(@"SELECT COUNT(a.Invoice) [count]
                                            FROM tbl_ADHOC_EM_Tasks a
                                            JOIN tbl_UC_Status b ON 
                                            a.Complete = b.StatusCodes
                                            WHERE a.Invoice Like '{0}%'
                                            AND a.isLatestData = 1
                                            AND (a.work_item = '' OR a.work_item IS NULL)
                                            AND a.Complete not in (3,6)
                                            AND a.PendingReasons NOT IN ('Zero or Negative Value','Inactive Property','Out of REO','Duplicate','Filtered by 21 Day','HSBC Property','Property Status: Bankruptcy', 'Property status is Bankruptcy','Real Resolution','Trailing Asset')
                                            AND a.id NOT IN (
	                                            SELECT c.id
	                                            FROM tbl_ADHOC_EM_Tasks c
	                                            WHERE c.Invoice Like '{1}%'
	                                            AND c.isLatestData = 1
	                                            AND c.work_item = ''
	                                            AND c.Complete not in (3,6)
	                                            AND c.PendingReasons NOT IN ('Zero or Negative Value','Inactive Property','Out of REO','Duplicate','Filtered by 21 Day','HSBC Property','Property Status: Bankruptcy', 'Property status is Bankruptcy','Real Resolution','Trailing Asset')
	                                            AND (c.PendingReasons LIKE '%Bulk Upload%' OR c.PendingReasons LIKE '%Manual WI%')
	                                            --AND ((c.PendingReasons LIKE '%Bulk Upload%' OR c.PendingReasons LIKE '%Manual WI%') AND (c.property_code = '' OR c.VendorId = '' OR c.Amount = '' OR c.vendor_group in ('','None'))) 
	                                            AND (((c.VendorAssignedStatus IN ('USP is correct.','New') OR c.VendorAssignedStatus LIKE '%successfully%') AND c.date_bulkuploaded IS NOT NULL AND c.remarks <> '') OR
	                                            (c.VendorAssignedStatus NOT IN ('USP is correct.','New') AND c.VendorAssignedStatus NOT LIKE '%successfully%' OR c.VendorAssignedStatus IS NULL))
                                            )
                                            AND a.id NOT IN (
	                                            SELECT d.id
	                                            FROM tbl_ADHOC_EM_Tasks d
	                                            WHERE  d.Invoice LIKE '{2}%'
	                                            AND d.isLatestData = 1
	                                            AND (d.PendingReasons LIKE '%Bulk Upload%' OR d.PendingReasons LIKE '%Manual WI%' OR d.PendingReasons LIKE '%Individual Work Item%')
	                                            AND (d.property_code = '' 
		                                            OR d.VendorId = '' 
		                                            OR d.Amount = '' 
		                                            OR d.AccountNumber = '' 
		                                            OR d.vendor_group IN ('','None')
		                                            OR DATEDIFF(Year, GETDATE(), d.ServiceFrom) > 5
		                                            OR DATEDIFF(Year, GETDATE(), d.ServiceFrom) < -5 
		                                            OR DATEDIFF(Year, GETDATE(), d.ServiceTo) > 5
		                                            OR DATEDIFF(Year, GETDATE(), d.ServiceTo) < -5
		                                            OR DATEDIFF(Year, GETDATE(), d.DueDate) > 5
		                                            OR DATEDIFF(Year, GETDATE(), d.DueDate) < -5)
                                            )
                                            AND a.LastDateModified < '{3}'",
                                      reportDt.ToString("yyyyMM"),
                                      reportDt.ToString("yyyyMM"),
                                      reportDt.ToString("yyyyMM"),
                                      dateTimePicker1.Value);

            rd.Close();
            cmd = new SqlCommand(query, conn);
            cmd.CommandTimeout = 180;
            rd = cmd.ExecuteReader();
            rd.Read();

            possibleWorkItemCreation = Convert.ToDecimal(rd[0].ToString());

            query = string.Format(@"SELECT COUNT(a.Invoice) [count]
                                        FROM tbl_ADHOC_EM_Tasks a
                                        JOIN tbl_UC_Status b ON 
                                        a.Complete = b.StatusCodes
                                        WHERE a.Invoice Like '{0}%'
                                        AND a.isLatestData = 1
                                        AND (a.Complete in (3,6) OR
                                        (a.Complete = 1 AND (PendingReasons IN ('Zero or Negative Value','Out of REO','Filtered by 21 Day','Duplicate','Property Status: Bankruptcy') OR PendingReasons like '%Disapproved%')) OR 
                                        (a.Complete = 12 AND PendingReasons = 'Inactive Property') OR
                                        (a.remarks in ('The work item frequency has exceeded the maximum limit set for the selected line item',
                                        'LI Level: The work item frequency has exceeded the maximum limit set for the selected line item',
                                        'Work item already exists for this respective Bill Cycle',
                                        'WI already created from EM Upload',
                                        'WI already created from Urjanet Upload',
                                        'Work Item cannot be created ,due to Stop Preservation flag is enabled in Property level',
                                        'Work item already exists for this respective Bill Cycle and Account Number',
                                        'Invalid amount')))
                                        AND a.LastDateModified < '{1}'",
                                      reportDt.ToString("yyyyMM"),
                                      dateTimePicker1.Value);

            rd.Close();
            cmd = new SqlCommand(query, conn);
            rd = cmd.ExecuteReader();
            rd.Read();

            workItemWillNotBeCreated = Convert.ToDecimal(rd[0].ToString());

            query = string.Format(@"SELECT COUNT(a.Invoice) [count]
                                        FROM tbl_ADHOC_EM_Tasks a
                                        JOIN tbl_UC_Status b ON 
                                        a.Complete = b.StatusCodes
                                        WHERE a.Invoice Like '{0}%'
                                        AND a.isLatestData = 1
                                        AND a.work_item = ''
                                        AND a.Complete not in (3,6)
                                        AND a.PendingReasons NOT IN ('Zero or Negative Value','Inactive Property','Out of REO','Duplicate','Filtered by 21 Day','HSBC Property','Property Status: Bankruptcy', 'Property status is Bankruptcy','Real Resolution','Trailing Asset')
                                        AND (a.PendingReasons LIKE '%Bulk Upload%' OR a.PendingReasons LIKE '%Manual WI%')
                                        AND (a.remarks not in ('The work item frequency has exceeded the maximum limit set for the selected line item',
                                        'LI Level: The work item frequency has exceeded the maximum limit set for the selected line item',
                                        'Work item already exists for this respective Bill Cycle',
                                        'WI already created from EM Upload',
                                        'WI already created from Urjanet Upload',
                                        'Work Item cannot be created ,due to Stop Preservation flag is enabled in Property level',
                                        'Work item already exists for this respective Bill Cycle and Account Number',
                                        'Invalid amount'))
                                        AND ((a.VendorAssignedStatus IN ('USP is correct.','New') OR a.VendorAssignedStatus LIKE '%successfully%') AND a.date_bulkuploaded IS NOT NULL AND a.remarks <> '')
                                        AND a.LastDateModified < '{1}'",
                                      reportDt.ToString("yyyyMM"),
                                      dateTimePicker1.Value);

            rd.Close();
            cmd = new SqlCommand(query, conn);
            rd = cmd.ExecuteReader();
            rd.Read();

            bulkUploadError = Convert.ToDecimal(rd[0].ToString());

            query = string.Format(@"SELECT COUNT(a.Invoice) [count]
                                            FROM tbl_ADHOC_EM_Tasks a
                                            JOIN tbl_UC_Status b ON 
                                            a.Complete = b.StatusCodes
                                            WHERE a.Invoice Like '{0}%'
                                            AND a.isLatestData = 1
                                            AND (a.Complete not in (6,12,3,9)
                                            AND a.VendorAssignedStatus NOT IN ('USP is correct.','New','Property status is Exception.','NA','Manual WI',
                                            'USP successfully assigned','USP successfully assigned; Utility is Exception','ForAudit','ForAudit2','Dont Assign - RR Property',
                                            'For Vendor ID Request','Dont Assign',''))
                                            or (a.VendorAssignedStatus in ('ForAudit','ForAudit2') and isnull(lockedto,'') <> 'Skip')
                                            AND a.LastDateModified < '{1}'",
                                      reportDt.ToString("yyyyMM"),
                                      dateTimePicker1.Value);

            rd.Close();
            cmd = new SqlCommand(query, conn);
            rd = cmd.ExecuteReader();
            rd.Read();

            assignVendorError = Convert.ToDecimal(rd[0].ToString());

            query = string.Format(@"SELECT COUNT(*)
                                    FROM tbl_ADHOC_EM_Tasks d
                                    WHERE  d.Invoice LIKE '{0}%'
                                    AND d.isLatestData = 1
                                    AND (d.PendingReasons LIKE '%Bulk Upload%' OR d.PendingReasons LIKE '%Manual WI%' OR d.PendingReasons LIKE '%Individual Work Item%')
                                    AND (d.property_code = '' 
	                                    OR d.VendorId = '' 
	                                    OR d.Amount = '' 
	                                    OR d.AccountNumber = '' 
	                                    OR d.vendor_group IN ('','None')
	                                    OR DATEDIFF(Year, GETDATE(), d.ServiceFrom) > 5
	                                    OR DATEDIFF(Year, GETDATE(), d.ServiceFrom) < -5 
	                                    OR DATEDIFF(Year, GETDATE(), d.ServiceTo) > 5
	                                    OR DATEDIFF(Year, GETDATE(), d.ServiceTo) < -5
	                                    OR DATEDIFF(Year, GETDATE(), d.DueDate) > 5
	                                    OR DATEDIFF(Year, GETDATE(), d.DueDate) < -5)
                                    AND d.LastDateModified < '{1}'",
                                      reportDt.ToString("yyyyMM"),
                                      dateTimePicker1.Value);



            rd.Close();
            cmd = new SqlCommand(query, conn);
            rd = cmd.ExecuteReader();
            rd.Read();

            wrongInformation = Convert.ToDecimal(rd[0].ToString());

            totalOutflow = createdWorkItem + possibleWorkItemCreation + workItemWillNotBeCreated + bulkUploadError + assignVendorError + wrongInformation;

            dgv_summary_inflow.Invoke((Action)delegate {
                dgv_summary_inflow.Rows.Clear();

                dgv_summary_inflow.Rows.Add("Total Inflow", string.Format("{0:N0}",totalInflow), "");
                dgv_summary_inflow.Rows.Add("Total Outflow", string.Format("{0:N0}",(decimal)totalOutflow), "");
                dgv_summary_inflow.Rows.Add("Created WI MTD", string.Format("{0:N0}",createdWorkItem),
                    string.Format("{0:P0}", (decimal)createdWorkItem / (decimal)totalInflow));
                dgv_summary_inflow.Rows.Add("For possible WI creation", string.Format("{0:N0}",possibleWorkItemCreation),
                    string.Format("{0:P0}", (decimal)possibleWorkItemCreation / (decimal)totalInflow));
                dgv_summary_inflow.Rows.Add("Work Item will not be created", string.Format("{0:N0}",workItemWillNotBeCreated),
                    string.Format("{0:P0}", (decimal)workItemWillNotBeCreated / (decimal)totalInflow));
                dgv_summary_inflow.Rows.Add("Bulk Upload Errors", string.Format("{0:N0}", bulkUploadError),
                    string.Format("{0:P0}", (decimal)bulkUploadError / (decimal)totalInflow));
                dgv_summary_inflow.Rows.Add("Assign Vendor Errors", string.Format("{0:N0}", assignVendorError),
                    string.Format("{0:P0}", (decimal)assignVendorError / (decimal)totalInflow));
                dgv_summary_inflow.Rows.Add("Wrong Information", string.Format("{0:N0}", wrongInformation),
                    string.Format("{0:P0}", (decimal)wrongInformation / (decimal)totalInflow));
            });

        }

        void bwPossibleWorkItemCreation_DoWork(object sender, DoWorkEventArgs e)
        {
            string query = string.Format(@"SELECT b.Status, case when b.Status = 'Encoding Audit' then 'Pending for Audit' else a.PendingReasons end PendingReasons, COUNT(a.Invoice) [count]
                                            FROM tbl_ADHOC_EM_Tasks a
                                            JOIN tbl_UC_Status b ON 
                                            a.Complete = b.StatusCodes
                                            WHERE a.Invoice Like '{0}%'
                                            AND a.isLatestData = 1
                                            AND (a.work_item = '' OR a.work_item IS NULL)
                                            AND a.Complete not in (3,6)
                                            AND a.PendingReasons NOT IN ('Zero or Negative Value','Inactive Property','Out of REO','Duplicate','Filtered by 21 Day','HSBC Property','Property Status: Bankruptcy', 'Property status is Bankruptcy','Real Resolution','Trailing Asset','RPM Conservice','BOA Property','Declined High Amount')
                                            AND a.id NOT IN (
                                                SELECT c.id
                                                FROM tbl_ADHOC_EM_Tasks c
                                                WHERE c.Invoice Like '{0}%'
                                                AND c.isLatestData = 1
                                                AND c.work_item = ''
                                                AND c.Complete not in (3,6)
                                                AND c.PendingReasons NOT IN ('Zero or Negative Value','Inactive Property','Out of REO','Duplicate','Filtered by 21 Day','HSBC Property','Property Status: Bankruptcy', 'Property status is Bankruptcy','Real Resolution','Trailing Asset','RPM Conservice','BOA Property','Declined High Amount')
                                                AND (c.PendingReasons LIKE '%Bulk Upload%' OR c.PendingReasons LIKE '%Manual WI%')
                                                --AND ((c.PendingReasons LIKE '%Bulk Upload%' OR c.PendingReasons LIKE '%Manual WI%') AND (c.property_code = '' OR c.VendorId = '' OR c.Amount = '' OR c.vendor_group in ('','None'))) 
                                                AND (((c.VendorAssignedStatus IN ('USP is correct.','New') OR c.VendorAssignedStatus LIKE '%successfully%') AND c.date_bulkuploaded IS NOT NULL AND c.remarks <> '') OR
                                                (c.VendorAssignedStatus NOT IN ('USP is correct.','New') AND c.VendorAssignedStatus NOT LIKE '%successfully%' OR c.VendorAssignedStatus IS NULL))
                                            )
                                            AND a.id NOT IN 
                                            (
                                                SELECT d.id
                                                FROM tbl_ADHOC_EM_Tasks d
                                                WHERE  d.Invoice LIKE '{0}%'
                                                AND d.isLatestData = 1
                                                AND (d.PendingReasons LIKE '%Bulk Upload%' OR d.PendingReasons LIKE '%Manual WI%' OR d.PendingReasons LIKE '%Individual Work Item%')
                                                AND (d.property_code = '' 
                                                    OR d.VendorId = '' 
                                                    OR d.Amount = '' 
                                                    OR d.AccountNumber = '' 
                                                    OR d.vendor_group IN ('','None')
                                                    OR DATEDIFF(Year, GETDATE(), d.ServiceFrom) > 5
                                                    OR DATEDIFF(Year, GETDATE(), d.ServiceFrom) < -5 
                                                    OR DATEDIFF(Year, GETDATE(), d.ServiceTo) > 5
                                                    OR DATEDIFF(Year, GETDATE(), d.ServiceTo) < -5
                                                    OR DATEDIFF(Year, GETDATE(), d.DueDate) > 5
                                                    OR DATEDIFF(Year, GETDATE(), d.DueDate) < -5)
                                            )
                                            AND a.id NOT IN 
                                            (
                                                SELECT e.id
                                                    FROM tbl_ADHOC_EM_Tasks e
                                                    WHERE e.Invoice Like '{0}%'
                                                    AND e.isLatestData = 1
                                                    AND (e.Complete in (3,6) OR
                                                    (e.Complete = 1 AND (e.PendingReasons IN ('Zero or Negative Value','Out of REO','Filtered by 21 Day','Duplicate','Property Status: Bankruptcy','Property status is Bankruptcy','Real Resolution','Trailing Asset','RPM Conservice','BOA Property','Declined High Amount') OR e.PendingReasons like '%Disapproved%')) OR 
                                                    (e.Complete = 12 AND e.PendingReasons = 'Inactive Property') OR
                                                    (e.remarks in ('The work item frequency has exceeded the maximum limit set for the selected line item',
                                                    'LI Level: The work item frequency has exceeded the maximum limit set for the selected line item',
                                                    'Work item already exists for this respective Bill Cycle',
                                                    'WI already created from EM Upload',
                                                    'WI already created from Urjanet Upload',
                                                    'Work Item cannot be created ,due to Stop Preservation flag is enabled in Property level',
                                                    'Work item already exists for this respective Bill Cycle and Account Number',
                                                    'Invalid amount')))
                                            )
                                            AND a.LastDateModified < '{3}' AND ISNULL(a.lockedto, '') <> 'Skip'
                                            GROUP BY ROLLUP(b.Status, case when b.Status = 'Encoding Audit' then 'Pending for Audit' else a.PendingReasons end)
                                            ORDER BY b.Status, case when b.Status = 'Encoding Audit' then 'Pending for Audit' else a.PendingReasons end",
                                              reportDt.ToString("yyyyMM"),
                                              reportDt.ToString("yyyyMM"),
                                              reportDt.ToString("yyyyMM"),
                                              dateTimePicker1.Value);

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();

            
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.SelectCommand.CommandTimeout = 180;
            da.Fill(dt);

            ChangeWord("Bulk Upload", "For Bulk Upload", ref dt);
            ChangeWord("Manual WI", "For Individual Work Item Creation", ref dt);
            ChangeWord("Manual WI - Inactive", "For Individual Work Item Creation", ref dt);
            ChangeWord("Manual WI - RFC", "For Individual Work Item Creation", ref dt);
            ChangeWord("Manual WI - RPA", "For Individual Work Item Creation", ref dt);
            ChangeWord("Manual WI - Urjanet", "For Individual Work Item Creation", ref dt);
            ChangeWord("Manual WI - Callout", "For Individual Work Item Creation from Callout", ref dt);
            ChangeWord("Pending", "For Review For Duplicates", ref dt);
            ChangeWord("Pending Duplicate", "For Review For Duplicates", ref dt);

            dgv_posssibleworkitemcreation.Invoke((Action)delegate
            {
                dgv_posssibleworkitemcreation.Rows.Clear();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][0].GetType() == typeof(DBNull) && dt.Rows[i][1].GetType() == typeof(DBNull))
                    {
                        dgv_posssibleworkitemcreation.Rows.Add("TOTAL", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() != string.Empty && dt.Rows[i][1].ToString() != string.Empty)
                    {
                        dgv_posssibleworkitemcreation.Rows.Add("", dt.Rows[i][1], string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() != string.Empty && dt.Rows[i][1].GetType() == typeof(DBNull))
                    {
                        dgv_posssibleworkitemcreation.Rows.Add(dt.Rows[i][0], "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                }
            });

            conn.Close();
        }

        void bwComputeWorkItemsWillNotBeCreated_DoWork(object sender, DoWorkEventArgs e)
        {
            string query = string.Format(@"SELECT b.Status, case when (a.PendingReasons like '%Bulk Upload%' or a.PendingReasons like '%Manual WI%') then 
	                                            case when a.remarks like '%exceeded the maximum limit%' then 'Rejected by the 21 day restriction' 
	                                            when a.remarks like '%Work item already exists for this respective Bill Cycle%' then 'Work item already exists for this respective Bill Cycle'
	                                            else a.remarks end
                                            else a.PendingReasons end PendingReasons, COUNT(a.Invoice) [count]
                                            FROM tbl_ADHOC_EM_Tasks a
                                            JOIN tbl_UC_Status b ON 
                                            a.Complete = b.StatusCodes
                                            WHERE a.Invoice Like '{0}%'
                                            AND a.isLatestData = 1
                                            AND (a.Complete in (3,6) OR
                                            (a.Complete = 1 AND (PendingReasons IN ('Zero or Negative Value','Out of REO','Filtered by 21 Day','Duplicate','Property Status: Bankruptcy','Property status is Bankruptcy','Real Resolution','Trailing Asset','RPM Conservice','BOA Property','Declined High Amount') OR PendingReasons like '%Disapproved%')) OR 
                                            (a.Complete = 12 AND PendingReasons = 'Inactive Property') OR
                                            (a.remarks in ('The work item frequency has exceeded the maximum limit set for the selected line item',
                                            'LI Level: The work item frequency has exceeded the maximum limit set for the selected line item',
                                            'Work item already exists for this respective Bill Cycle',
                                            'WI already created from EM Upload',
                                            'WI already created from Urjanet Upload',
                                            'Work Item cannot be created ,due to Stop Preservation flag is enabled in Property level',
                                            'Work item already exists for this respective Bill Cycle and Account Number',
                                            'Invalid amount')))
                                            AND a.LastDateModified < '{1}'
                                            GROUP BY ROLLUP(b.Status, case when (a.PendingReasons like '%Bulk Upload%' or a.PendingReasons like '%Manual WI%') then 
	                                            case when a.remarks like '%exceeded the maximum limit%' then 'Rejected by the 21 day restriction' 
	                                            when a.remarks like '%Work item already exists for this respective Bill Cycle%' then 'Work item already exists for this respective Bill Cycle'
	                                            else a.remarks end
                                            else a.PendingReasons end)
                                            ORDER BY b.Status, case when (a.PendingReasons like '%Bulk Upload%' or a.PendingReasons like '%Manual WI%') then 
	                                            case when a.remarks like '%exceeded the maximum limit%' then 'Rejected by the 21 day restriction' 
	                                            when a.remarks like '%Work item already exists for this respective Bill Cycle%' then 'Work item already exists for this respective Bill Cycle'
	                                            else a.remarks end
                                            else a.PendingReasons end",
                                              reportDt.ToString("yyyyMM"),
                                              dateTimePicker1.Value);

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);

            dgv_workitemswillnotbecreated.Invoke((Action)delegate
            {
                dgv_workitemswillnotbecreated.Rows.Clear();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][0].GetType() == typeof(DBNull) && dt.Rows[i][1].GetType() == typeof(DBNull))
                    {
                        dgv_workitemswillnotbecreated.Rows.Add("TOTAL", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() != string.Empty && dt.Rows[i][1].ToString() != string.Empty)
                    {
                        dgv_workitemswillnotbecreated.Rows.Add("", dt.Rows[i][1], string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() != string.Empty && dt.Rows[i][1].GetType() == typeof(DBNull))
                    {
                        dgv_workitemswillnotbecreated.Rows.Add(dt.Rows[i][0], "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                }
            });

            conn.Close();
        }

        void bwComputeWorkItemsCreated_DoWork(object sender, DoWorkEventArgs e)
        {
            string query = string.Format(@"SELECT b.Status, case when a.PendingReasons like '%Manual WI%' then 'Individual Work Item' else a.PendingReasons end, COUNT(a.Invoice) [count]
                                          FROM tbl_ADHOC_EM_Tasks a
                                          JOIN tbl_UC_Status b ON 
                                          a.Complete = b.StatusCodes
                                          WHERE a.Invoice Like '{0}%'
                                          AND a.isLatestData = 1
                                          AND a.work_item <> '' AND (a.PendingReasons like '%Manual WI%' or a.PendingReasons like '%Bulk Upload%')
                                          AND a.LastDateModified < '{1}'
                                          GROUP BY ROLLUP(b.Status, case when a.PendingReasons like '%Manual WI%' then 'Individual Work Item' else a.PendingReasons end)
                                          ORDER BY b.Status, case when a.PendingReasons like '%Manual WI%' then 'Individual Work Item' else a.PendingReasons end",
                                          reportDt.ToString("yyyyMM"),
                                          dateTimePicker1.Value);

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();
            
            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);

            dgv_workitemscreated.Invoke((Action)delegate
            {
                dgv_workitemscreated.Rows.Clear();

                ChangeWord("Manual WI", "Individual Work Items", ref dt);

                for (int i = 0; i < dt.Rows.Count; i++)
                { 
                    if (dt.Rows[i][0].GetType() == typeof(DBNull) && dt.Rows[i][1].GetType() == typeof(DBNull))
                    {
                        dgv_workitemscreated.Rows.Add("TOTAL", "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() != string.Empty && dt.Rows[i][1].ToString() != string.Empty)
                    {
                        dgv_workitemscreated.Rows.Add("", dt.Rows[i][1], string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                    else if (dt.Rows[i][0].ToString() != string.Empty && dt.Rows[i][1].GetType() == typeof(DBNull))
                    {
                        dgv_workitemscreated.Rows.Add(dt.Rows[i][0], "", string.Format("{0:N0}", (int)dt.Rows[i][2]));
                    }
                }
            });

            conn.Close();
        }

        void bwComputeInflowManual_DoWork(object sender, DoWorkEventArgs e)
        {
            DateTime datefrom = reportDt;

            if (reportDt == new DateTime(2015, 2, 1).Date)
            {
                datefrom = new DateTime(2015, 2, 11).Date;
            }

//            string query = string.Format(@"SELECT convert(varchar, a.LastDateModified, 101) [Date arrived], COUNT(a.Invoice) [Number of invoice]
//                                              FROM tbl_ADHOC_EM_Tasks a
//                                              JOIN tbl_UC_Status b ON 
//                                              a.Complete = b.StatusCodes
//                                              WHERE a.Invoice LIKE '{0}%'
//                                              AND a.LastDateModified > '{1}'
//                                              AND a.Complete = 20
//                                              AND a.LastDateModified < '{2}'
//                                              GROUP BY ROLLUP(convert(varchar, a.LastDateModified, 101))
//                                              ORDER BY [Date arrived]",
//                                              datefrom.ToString("yyyyMM"),
//                                              datefrom.ToString("yyyy-MM-dd HH:mm:ss"),
//                                              dateTimePicker1.Value);


            string query = string.Format(@"select convert(varchar, CAST(DATEADD(HH, -12, LastDateModified) AS DATE), 101) [Date], COUNT(*) [Count]
                                            from tbl_ADHOC_EM_Tasks
                                            where Invoice like '{0}%'
                                            and Complete = 20
                                            and CAST(DATEADD(HH, -12, LastDateModified) AS DATE) > '{1}' and CAST(DATEADD(HH, -12, LastDateModified) AS DATE) < '{2}' 
                                            group by rollup (CAST(DATEADD(HH, -12, LastDateModified) AS DATE))
                                            order by CAST(DATEADD(HH, -12, LastDateModified) AS DATE)",
                                              datefrom.ToString("yyyyMM"),
                                              datefrom.ToString("yyyy-MM-dd HH:mm:ss"),
                                              dateTimePicker1.Value);


            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);

            dgv_inflow.Invoke((Action)delegate
            {
                dgv_inflow.Rows.Clear();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][0].ToString() == string.Empty)
                    {
                        if (reportDt == new DateTime(2015, 2, 1).Date)
                        {
                            int diff = 19671 - (int)dt.Rows[i][1];

                            dgv_inflow.Rows.Add("TOTAL", string.Format("{0:N0}", (int)dt.Rows[i][1] + diff));

                            dgv_inflow.Rows.Add("02/01/2015 - 02/10/2015", diff);
                        }
                        else
                        {
                            dgv_inflow.Rows.Add("TOTAL", string.Format("{0:N0}", (int)dt.Rows[i][1]));
                        }
                    }
                    else
                    {
                        dgv_inflow.Rows.Add(dt.Rows[i][0], string.Format("{0:N0}", (int)dt.Rows[i][1]));
                    }
                }
            });

            conn.Close();
        }

        void bwComputeBulkUploadErrors_DoWork(object sender, DoWorkEventArgs e)
        {
            string query = string.Format(@"SELECT a.remarks [Bulk Upload Errors], COUNT(a.Invoice) [count]
                                            FROM tbl_ADHOC_EM_Tasks a
                                            JOIN tbl_UC_Status b ON 
                                            a.Complete = b.StatusCodes
                                            WHERE a.Invoice Like '{0}%'
                                            AND a.isLatestData = 1
                                            AND a.work_item = ''
                                            AND a.Complete not in (3,6)
                                            AND a.PendingReasons NOT IN ('Zero or Negative Value','Inactive Property','Out of REO')
                                            AND (a.PendingReasons LIKE '%Bulk Upload%' OR a.PendingReasons LIKE '%Manual WI%')
                                            AND (a.remarks not in ('The work item frequency has exceeded the maximum limit set for the selected line item',
                                            'LI Level: The work item frequency has exceeded the maximum limit set for the selected line item',
                                            'Work item already exists for this respective Bill Cycle',
                                            'WI already created from EM Upload',
                                            'WI already created from Urjanet Upload',
                                            'Work Item cannot be created ,due to Stop Preservation flag is enabled in Property level',
                                            'Work item already exists for this respective Bill Cycle and Account Number',
                                            'Invalid amount'))
                                            AND ((a.VendorAssignedStatus IN ('USP is correct.','New') OR a.VendorAssignedStatus LIKE '%successfully%') AND a.date_bulkuploaded IS NOT NULL AND a.remarks <> '')
                                            AND a.LastDateModified < '{1}'
                                            GROUP BY ROLLUP(a.remarks)
                                            ORDER BY count DESC",
                                              reportDt.ToString("yyyyMM"),
                                              dateTimePicker1.Value);

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);

            dgv_bulkuploaderrors.Invoke((Action)delegate
            {
                dgv_bulkuploaderrors.Rows.Clear();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][0].GetType() == typeof(DBNull))
                    {
                        dgv_bulkuploaderrors.Rows.Add("TOTAL", string.Format("{0:N0}", (int)dt.Rows[i][1]));
                    }
                    else
                    {
                        dgv_bulkuploaderrors.Rows.Add(dt.Rows[i][0], string.Format("{0:N0}", (int)dt.Rows[i][1]));
                    }
                }
            });

            conn.Close();
        }

        void bwComputeAssignVendorErrors_DoWork(object sender, DoWorkEventArgs e)
        {
            string query = string.Format(@"SELECT 
                                            a.VendorAssignedStatus [Assign Vendor Errors], COUNT(a.Invoice) [count]
                                            FROM tbl_ADHOC_EM_Tasks a
                                            JOIN tbl_UC_Status b ON 
                                            a.Complete = b.StatusCodes
                                            WHERE a.Invoice Like '{0}%'
                                            AND a.isLatestData = 1
                                            AND (a.Complete not in (6,12,3,9)
                                            AND (a.VendorAssignedStatus NOT IN ('USP is correct.','New','Property status is Exception.','NA','Manual WI',
                                            'USP successfully assigned','USP successfully assigned; Utility is Exception','ForAudit','ForAudit2','Dont Assign - RR Property',
                                            'For Vendor ID Request','Dont Assign',''))
                                            or (a.VendorAssignedStatus in ('ForAudit','ForAudit2') and isnull(lockedto,'') <> 'Skip')) and a.Lastdatemodified < '{1}'
                                            GROUP BY ROLLUP(a.VendorAssignedStatus)
                                            ORDER BY count DESC",
                                              reportDt.ToString("yyyyMM"),
                                              dateTimePicker1.Value);

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            DataTable dt = new DataTable();
            conn.Open();

            SqlDataAdapter da = new SqlDataAdapter(query, conn);
            da.Fill(dt);

            dgv_assignvendorerrors.Invoke((Action)delegate
            {
                dgv_assignvendorerrors.Rows.Clear();

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i][0].GetType() == typeof(DBNull))
                    {
                        dgv_assignvendorerrors.Rows.Add("TOTAL", string.Format("{0:N0}", (int)dt.Rows[i][1]));
                    }
                    else
                    {
                        dgv_assignvendorerrors.Rows.Add(dt.Rows[i][0], string.Format("{0:N0}", (int)dt.Rows[i][1]));
                    }
                }
            });

            conn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            reportDt = new DateTime(Convert.ToInt16(comboBox2.Text),
                DateTime.ParseExact(comboBox1.Text, "MMMM", System.Globalization.CultureInfo.CurrentCulture).Month, 1).Date;

            t = new Timer();
            t.Interval = 1000 * 60 * 10;
            t.Tick += new EventHandler(t_Tick);
            t.Start();

            t_Tick(null, null);
        }

        private void ChangeWord(string oldString, string newString, ref DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    if (dr[dc].GetType() != typeof(DBNull))
                    {
                        if (dr[dc].ToString() == oldString)
                        {
                            dr[dc] = newString;
                        }
                    }
                }
            }
        }

        private void FunnelReport_Load(object sender, EventArgs e)
        {
            dateTimePicker1.Value = DateTime.Now.Date.AddHours(3);

            comboBox1.SelectedIndex = DateTime.Now.Month - 1;
            comboBox2.Text = DateTime.Now.Year.ToString();
        }
    }
}
