﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms.Reports
{
    public partial class InvoiceTimeline : Form
    {
        DateTime start_date;
        DateTime end_date;

        public InvoiceTimeline()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Custom date range")
            {
                label1.Visible = true;
                label2.Visible = true;
                dateTimePicker1.Visible = true;
                dateTimePicker2.Visible = true;
            }
            else
            {
                label1.Visible = false;
                label2.Visible = false;
                dateTimePicker1.Visible = false;
                dateTimePicker2.Visible = false;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.Text == "Month to date")
            {
                this.WindowState = FormWindowState.Maximized;

                BackgroundWorker loadreport = new BackgroundWorker();
                loadreport.DoWork += new DoWorkEventHandler(loadreport_DoWork);
                loadreport.RunWorkerAsync(DateTime.Now.Date.AddDays(-DateTime.Now.Day+1));
            }
            else if (comboBox1.Text == "Year to date")
            {
                this.WindowState = FormWindowState.Maximized;

                BackgroundWorker loadreport = new BackgroundWorker();
                loadreport.DoWork += new DoWorkEventHandler(loadreport_DoWork);
                loadreport.RunWorkerAsync(DateTime.Now.Date.AddDays(-DateTime.Now.DayOfYear+1));
            }
            else if (comboBox1.Text == "Custom date range")
            {
                this.WindowState = FormWindowState.Maximized;

                BackgroundWorker loadreport1 = new BackgroundWorker();
                loadreport1.DoWork += new DoWorkEventHandler(loadreport1_DoWork);
                loadreport1.RunWorkerAsync();
            }
        }

        private void loadreport1_DoWork(object sender, DoWorkEventArgs e)
        {
            string query = string.Format(@"declare @listOfReceivedDates table (invoice varchar(50), date_received datetime)
                                            declare @process_start_date datetime, @process_end_date datetime

                                            set @process_start_date = '{0}'
                                            set @process_end_date = '{1}'

                                            insert into @listOfReceivedDates (invoice, date_received)
                                            select a.Invoice, a.LastDateModified from tbl_ADHOC_EM_Tasks a
                                            where a.LastDateModified > @process_start_date
                                            and a.LastDateModified < @process_end_date
                                            and a.Complete = 20

                                            select 
                                            CONVERT(varchar(50), (DATEADD(dd, @@DATEFIRST - DATEPART(dw, a.LastDateModified) - 6, a.LastDateModified)), 101) [start date], 
                                            CONVERT(varchar(50), (DATEADD(dd, @@DATEFIRST - DATEPART(dw, a.LastDateModified), a.LastDateModified)), 101) [end date],
                                            AVG(DATEDIFF(HOUR, a.invoice_date, b.date_received)) [invoice date to date recieved (hours)],
                                            AVG(DATEDIFF(HOUR, b.date_received, a.LastDateModified)) [date recieved to date processed (hours)],
                                            AVG(DATEDIFF(HOUR, a.LastDateModified, a.date_bulkuploaded)) [date processed to date wi issued (hours)],
                                            AVG(DATEDIFF(HOUR, b.date_received, a.date_bulkuploaded)) [date recieved to date wi issued (hours)]
                                            from tbl_ADHOC_EM_Tasks a join
                                            @listOfReceivedDates b on a.Invoice = b.invoice
                                            where a.LastDateModified > @process_start_date
                                            and a.LastDateModified < @process_end_date
                                            and a.invoice_date is not null
                                            and a.date_bulkuploaded is not null
                                            and a.work_item <> ''
                                            group by CONVERT(varchar(50), (DATEADD(dd, @@DATEFIRST - DATEPART(dw, a.LastDateModified) - 6, a.LastDateModified)), 101),
                                            CONVERT(varchar(50), (DATEADD(dd, @@DATEFIRST - DATEPART(dw, a.LastDateModified), a.LastDateModified)), 101)
                                            order by (varchar(50), (DATEADD(dd, @@DATEFIRST - DATEPART(dw, a.LastDateModified), a.LastDateModified)), 101)
                                            ", start_date, end_date);

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            conn.Open();
            SqlDataAdapter adp = new SqlDataAdapter(query, conn);
            SqlCommandBuilder cb = new SqlCommandBuilder(adp);

            DataSet ds = new DataSet();
            adp.Fill(ds);

            adp = null;
            cb = null;
            conn.Close();

            dataGridView1.Invoke((Action)delegate
            {
                dataGridView1.DataSource = ds.Tables[0];
            });
        }

        private void loadreport_DoWork(object sender, DoWorkEventArgs e)
        {
            string query = string.Format(@"declare @listOfReceivedDates table (invoice varchar(50), date_received datetime)
                                            declare @process_start_date datetime

                                            set @process_start_date = '{0}'

                                            insert into @listOfReceivedDates (invoice, date_received)
                                            select a.Invoice, a.LastDateModified from tbl_ADHOC_EM_Tasks a
                                            where a.LastDateModified > @process_start_date
                                            and a.Complete = 20

                                            select
                                            CONVERT(varchar(50), (DATEADD(dd, @@DATEFIRST - DATEPART(dw, a.LastDateModified) - 6, a.LastDateModified)), 101) [start date], 
                                            CONVERT(varchar(50), (DATEADD(dd, @@DATEFIRST - DATEPART(dw, a.LastDateModified), a.LastDateModified)), 101) [end date], 
                                            AVG(DATEDIFF(HOUR, a.invoice_date, b.date_received)) [invoice date to date recieved (hours)],
                                            AVG(DATEDIFF(HOUR, b.date_received, a.LastDateModified)) [date recieved to date processed (hours)],
                                            AVG(DATEDIFF(HOUR, a.LastDateModified, a.date_bulkuploaded)) [date processed to date wi issued (hours)],
                                            AVG(DATEDIFF(HOUR, b.date_received, a.date_bulkuploaded)) [date recieved to date wi issued (hours)]
                                            from tbl_ADHOC_EM_Tasks a join
                                            @listOfReceivedDates b on a.Invoice = b.invoice
                                            where a.LastDateModified > @process_start_date
                                            and a.invoice_date is not null
                                            and a.date_bulkuploaded is not null
                                            and a.work_item <> ''
                                            group by CONVERT(varchar(50), (DATEADD(dd, @@DATEFIRST - DATEPART(dw, a.LastDateModified) - 6, a.LastDateModified)), 101), 
                                            CONVERT(varchar(50), (DATEADD(dd, @@DATEFIRST - DATEPART(dw, a.LastDateModified), a.LastDateModified)), 101)
                                            order by
                                            CONVERT(varchar(50), (DATEADD(dd, @@DATEFIRST - DATEPART(dw, a.LastDateModified), a.LastDateModified)), 101)"
                                            ,(DateTime)e.Argument);

            SqlConnection conn = new SqlConnection(Constants.connectionString);
            conn.Open();
            SqlDataAdapter adp = new SqlDataAdapter(query, conn);
            SqlCommandBuilder cb = new SqlCommandBuilder(adp);

            DataSet ds = new DataSet();
            adp.Fill(ds);

            adp = null;
            cb = null;
            conn.Close();

            dataGridView1.Invoke((Action)delegate {
                dataGridView1.DataSource = ds.Tables[0];
            });
        }

        private void InvoiceTimeline_Load(object sender, EventArgs e)
        {
            dateTimePicker2.Value = dateTimePicker1.Value.AddMonths(1).Date;
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker1.Visible == true)
            {
                start_date = dateTimePicker1.Value.Date;
            }
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            if (dateTimePicker2.Visible == true)
            {
                end_date = dateTimePicker2.Value.Date;
            }
        }

    }
}
