﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms.Reports
{
    public partial class Dashboard_QA : Form
    {
        DateTime start, end;

        public Dashboard_QA()
        {
            InitializeComponent();
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            start = dateTimePicker1.Value;
            dateTimePicker2.Value = dateTimePicker1.Value.AddHours(9);
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            end = dateTimePicker2.Value;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BackgroundWorker bwLoadDashboard = new BackgroundWorker();
            bwLoadDashboard.DoWork += new DoWorkEventHandler(bwLoadDashboard_DoWork);
            bwLoadDashboard.RunWorkerAsync();
        }

        void bwLoadDashboard_DoWork(object sender, DoWorkEventArgs e)
        {
            List<string> errors = new List<string>();

            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"select LOWER(a.userid) [Userid],
                                            SUM(case when b.result = 'Wrong account number' then 1 else 0 end) [Wrong account number],
                                            SUM(case when b.result = 'Unable to detect if bill is readable' then 1 else 0 end) [Unable to detect if bill is readable],
                                            SUM(case when b.result = 'Wrong disconnection date' then 1 else 0 end) [Wrong disconnection date],
                                            SUM(case when b.result = 'Wrong Previous Balance' then 1 else 0 end) [Wrong Previous Balance],
                                            SUM(case when b.result = 'Wrong data entered on property search' then 1 else 0 end) [Wrong data entered on property search],
                                            SUM(case when b.result = 'Wrong tagging of Do Not Mail' then 1 else 0 end) [Wrong tagging of Do Not Mail],
                                            SUM(case when b.result = 'Wrong vendor type' then 1 else 0 end) [Wrong vendor type],
                                            SUM(case when b.result = 'Wrong USP' then 1 else 0 end) [Wrong USP],
                                            SUM(case when b.result = 'Wrong tagging of Final Bill' then 1 else 0 end) [Wrong tagging of Final Bill],
                                            SUM(case when b.result = 'Wrong tagging of No Amount' then 1 else 0 end) [Wrong tagging of No Amount],
                                            SUM(case when b.result = 'Wrong service property' then 1 else 0 end) [Wrong service property],
                                            SUM(case when b.result = 'Wrong Payment Received' then 1 else 0 end) [Wrong Payment Received],
                                            SUM(case when b.result = 'Wrong Disconnection Fee' then 1 else 0 end) [Wrong Disconnection Fee],
                                            SUM(case when b.result = 'Wrong invoice date' then 1 else 0 end) [Wrong invoice date],
                                            SUM(case when b.result = 'Wrong due date' then 1 else 0 end) [Wrong due date],
                                            SUM(case when b.result = 'Wrong Current Charge' then 1 else 0 end) [Wrong Current Charge],
                                            SUM(case when b.result = 'Wrong Late Fee' then 1 else 0 end) [Wrong Late Fee],
                                            SUM(case when b.result = 'Wrong billing cycle' then 1 else 0 end) [Wrong billing cycle],
                                            SUM(case when b.result = 'Incorrectly tagged as for CallOut' then 1 else 0 end) [Incorrectly tagged as for CallOut],
                                            SUM(case when b.result = 'Wrong Amount After Due' then 1 else 0 end) [Wrong Amount After Due]
                                            from tbl_ADHOC_EM_Tasks a
                                            left join tbl_EM_Audit b on a.id = b.em_task_id
                                            where a.Auditdate > '{0}'
                                            and a.Auditdate < '{1}'
                                            and a.userid <> 'auto'
                                            group by a.userid
                                            order by a.userid",
                                            start, end);

            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            SqlCommandBuilder cb = new SqlCommandBuilder(da);

            DataSet ds = new DataSet();

            da.Fill(ds);

            cb = null;
            da = null;
            conn.Close();

            dataGridView1.Invoke((Action)delegate {
                dataGridView1.DataSource = ds.Tables[0];

                
                foreach (DataGridViewColumn dc in dataGridView1.Columns)
                {
                    foreach (DataGridViewRow dr in dataGridView1.Rows)
                    {
                        try
                        {
                            if (dataGridView1[dc.Index, dr.Index].Value.ToString() != "0" && dc.Index != 0)
                            {
                                if (Convert.ToInt32(dataGridView1[dc.Index, dr.Index].Value.ToString()) < 5)
                                {
                                    dataGridView1[dc.Index, dr.Index].Style.BackColor = Color.Yellow;
                                }
                                else
                                {
                                    dataGridView1[dc.Index, dr.Index].Style.BackColor = Color.Red;
                                }
                            }
                        }
                        catch
                        { }
                    }
                }
            });
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
