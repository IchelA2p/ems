﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms.Reports
{
    public partial class Dashboard_Audit : Form
    {
        DateTime startdate, enddate;

        public Dashboard_Audit()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            BackgroundWorker bwLoadSummary = new BackgroundWorker();
            bwLoadSummary.DoWork += new DoWorkEventHandler(bwLoadSummary_DoWork);
            bwLoadSummary.RunWorkerAsync();
        }

        void bwLoadSummary_DoWork(object sender, DoWorkEventArgs e)
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);

            conn.Open();

            string query = string.Format(@"declare @datestart datetime, @dateend datetime

                                            set @datestart = '{0}'
                                            set @dateend = '{1}'

                                            select a.AuditedBy,
                                            Sum(case when a.AuditDate between @datestart and DATEADD(hour, 1, @datestart) then 1 else 0 end) [8PM-9PM],
                                            Sum(case when a.AuditDate between DATEADD(hour, 1, @datestart) and DATEADD(hour, 2, @datestart) then 1 else 0 end) [9PM-10PM],
                                            Sum(case when a.AuditDate between DATEADD(hour, 2, @datestart) and DATEADD(hour, 3, @datestart) then 1 else 0 end) [10PM-11PM],
                                            Sum(case when a.AuditDate between DATEADD(hour, 3, @datestart) and DATEADD(hour, 4, @datestart) then 1 else 0 end) [11PM-12MN],
                                            Sum(case when a.AuditDate between DATEADD(hour, 4, @datestart) and DATEADD(hour, 5, @datestart) then 1 else 0 end) [12MN-1AM],
                                            Sum(case when a.AuditDate between DATEADD(hour, 5, @datestart) and DATEADD(hour, 6, @datestart) then 1 else 0 end) [1AM-2AM],
                                            Sum(case when a.AuditDate between DATEADD(hour, 6, @datestart) and DATEADD(hour, 7, @datestart) then 1 else 0 end) [2AM-3AM],
                                            Sum(case when a.AuditDate between DATEADD(hour, 7, @datestart) and DATEADD(hour, 8, @datestart) then 1 else 0 end) [3AM-4AM],
                                            Sum(case when a.AuditDate between DATEADD(hour, 8, @datestart) and DATEADD(hour, 9, @datestart) then 1 else 0 end) [4AM-5AM],
                                            Sum(case when a.AuditDate between DATEADD(hour, 9, @datestart) and DATEADD(hour, 10, @datestart) then 1 else 0 end) [5AM-6AM],
                                            Sum(case when a.AuditDate between DATEADD(hour, 10, @datestart) and DATEADD(hour, 11, @datestart) then 1 else 0 end) [6AM-7AM]
                                            from tbl_ADHOC_EM_Tasks a
                                            where a.AuditedBy is not null
                                            and a.AuditResult <> ''
                                            and a.AuditDate between @datestart and @dateend
                                            group by a.AuditedBy", startdate, enddate);

            SqlDataAdapter adp = new SqlDataAdapter(query, conn);
            SqlCommandBuilder cb = new SqlCommandBuilder(adp);

            DataSet ds = new DataSet();
            adp.Fill(ds);

            cb = null;
            adp = null;
            conn.Close();

            dgv_summary.Invoke((Action)delegate {
                dgv_summary.DataSource = ds.Tables[0];
            });
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            startdate = dateTimePicker1.Value.Date.AddHours(20);
            enddate = startdate.AddHours(11);
        }

        private void Dashboard_Audit_Load(object sender, EventArgs e)
        {
            if (DateTime.Now > DateTime.Now.Date.AddHours(20))
            {
                startdate = DateTime.Today.Date.AddHours(20);
                enddate = startdate.AddHours(11);
            }
            else
            {
                startdate = DateTime.Today.Date.AddDays(-1).AddHours(20);
                enddate = startdate.AddHours(11);
            }
        }
    }
}
