﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace UC.Forms
{
    public partial class AddUSP : Form
    {
        public AddUSP()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(textBox1.Text))
            {
                CreateNewUSP();
            }
        }

        private void CreateNewUSP()
        {
            SqlConnection conn = new SqlConnection(Constants.connectionString);
            conn.Open();

            string query = string.Format(@"SELECT Count([USPGroupName]) [USP Name] FROM [MIS_ALTI].[dbo].[tbl_UC_USPGroup]
                                            WHERE USPGroupName = '{0}'", textBox1.Text.Replace("  ", ""));

            SqlCommand cmd = new SqlCommand(query, conn);

            SqlDataReader rd = cmd.ExecuteReader();

            rd.Read();

            if ((int)rd[0] == 0)
            {
                query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_UC_USPGroup]
                                                   ([USPGroupName]
                                                   ,[PhoneNo]
                                                   ,[PostedBy]
                                                   ,[PostedDate])
                                             VALUES
                                                   ('{0}'
                                                   ,'{1}'
                                                   ,'{2}'
                                                   ,'{3}')",
                                           textBox1.Text.Replace("  ", "").ToUpper(),
                                           textBox2.Text,
                                           Environment.UserName.ToLower(),
                                           DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                rd.Close();

                cmd = new SqlCommand(query, conn);

                int result = cmd.ExecuteNonQuery();

                if (result == 0)
                {
                    MessageBox.Show("USP was not saved! Please check typed information");
                }
                else
                {
                    MessageBox.Show("Saved! Please select the USP from the list then SAVE");
                }
            }
            else
            {
                MessageBox.Show(string.Format("{0} is already listed on USP list", textBox1.Text));
            }

            conn.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
