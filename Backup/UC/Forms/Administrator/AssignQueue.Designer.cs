﻿namespace UC.Forms.Administrator
{
    partial class AssignQueue
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.queue_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queue_userid = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queue_status = new System.Windows.Forms.DataGridViewComboBoxColumn();
            this.queue_assignedby = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.queue_fixed = new System.Windows.Forms.DataGridViewCheckBoxColumn();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.queue_id,
            this.queue_userid,
            this.queue_status,
            this.queue_assignedby,
            this.queue_fixed});
            this.dataGridView1.Location = new System.Drawing.Point(12, 12);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(577, 321);
            this.dataGridView1.TabIndex = 0;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // queue_id
            // 
            this.queue_id.HeaderText = "Id";
            this.queue_id.Name = "queue_id";
            this.queue_id.Visible = false;
            // 
            // queue_userid
            // 
            this.queue_userid.HeaderText = "User ID";
            this.queue_userid.Name = "queue_userid";
            // 
            // queue_status
            // 
            this.queue_status.HeaderText = "Current Queue";
            this.queue_status.Items.AddRange(new object[] {
            "New Invoice",
            "Call Out",
            "QA 1",
            "QA 2",
            "Escalations"});
            this.queue_status.Name = "queue_status";
            this.queue_status.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.queue_status.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // queue_assignedby
            // 
            this.queue_assignedby.HeaderText = "Assignee";
            this.queue_assignedby.Name = "queue_assignedby";
            // 
            // queue_fixed
            // 
            this.queue_fixed.HeaderText = "Fixed";
            this.queue_fixed.Name = "queue_fixed";
            this.queue_fixed.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.queue_fixed.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(263, 339);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // AssignQueue
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(601, 368);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.dataGridView1);
            this.Name = "AssignQueue";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Assign Task";
            this.Load += new System.EventHandler(this.AssignQueue_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridViewTextBoxColumn queue_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn queue_userid;
        private System.Windows.Forms.DataGridViewComboBoxColumn queue_status;
        private System.Windows.Forms.DataGridViewTextBoxColumn queue_assignedby;
        private System.Windows.Forms.DataGridViewCheckBoxColumn queue_fixed;
    }
}