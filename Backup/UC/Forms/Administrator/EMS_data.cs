﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Database;

namespace UC.Forms.Administrator
{
    public partial class EMS_data : Form
    {
        private Database.DB.EM_Tasks _info;
        private List<PaymentClass> _payment;

        public EMS_data(Database.DB.EM_Tasks info, List<PaymentClass> payment)
        {
            InitializeComponent();

            _info = new Database.DB.EM_Tasks();
            _info = info;

            _payment = new List<PaymentClass>();
            _payment = payment;
        }

        private void EMS_data_Load(object sender, EventArgs e)
        {
            Type typ = typeof(Database.DB.EM_Tasks);
            System.Reflection.FieldInfo[] fields = typ.GetFields();

            label4.Text = fields.Count().ToString();

            foreach (System.Reflection.FieldInfo fd in fields)
            {
                try
                {
                    dataGridView1.Rows.Add(fd.Name, fd.GetValue(_info).ToString());
                }
                catch
                {

                }
            }

            try
            {
                foreach (PaymentClass pc in _payment)
                {
                    dataGridView2.Rows.Add(pc.PaymentBreakdown, pc.Amount);
                }
            }
            catch
            { 
                
            }
        }

    }
}
