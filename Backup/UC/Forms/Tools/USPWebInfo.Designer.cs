﻿namespace UC.Forms.Tools
{
    partial class USPWebInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.usp_id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usp_uspname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usp_website = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usp_username = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usp_password = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usp_isworking = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usp_updatedby = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usp_lastdatemodified = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usp_oldusername = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.usp_oldpassword = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(63, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "USP Name:";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(81, 6);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(587, 20);
            this.textBox1.TabIndex = 1;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(674, 4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(55, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "ADD";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.BackgroundColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.LightSteelBlue;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.usp_id,
            this.usp_uspname,
            this.usp_website,
            this.usp_username,
            this.usp_password,
            this.usp_isworking,
            this.usp_updatedby,
            this.usp_lastdatemodified,
            this.usp_oldusername,
            this.usp_oldpassword});
            this.dataGridView1.Location = new System.Drawing.Point(15, 32);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.Size = new System.Drawing.Size(714, 296);
            this.dataGridView1.TabIndex = 4;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // usp_id
            // 
            this.usp_id.HeaderText = "ID";
            this.usp_id.Name = "usp_id";
            this.usp_id.ReadOnly = true;
            this.usp_id.Visible = false;
            // 
            // usp_uspname
            // 
            this.usp_uspname.HeaderText = "USP Name";
            this.usp_uspname.Name = "usp_uspname";
            this.usp_uspname.ReadOnly = true;
            // 
            // usp_website
            // 
            this.usp_website.HeaderText = "Website";
            this.usp_website.Name = "usp_website";
            this.usp_website.ReadOnly = true;
            // 
            // usp_username
            // 
            this.usp_username.HeaderText = "Username";
            this.usp_username.Name = "usp_username";
            this.usp_username.ReadOnly = true;
            // 
            // usp_password
            // 
            this.usp_password.HeaderText = "Password";
            this.usp_password.Name = "usp_password";
            this.usp_password.ReadOnly = true;
            // 
            // usp_isworking
            // 
            this.usp_isworking.HeaderText = "Working";
            this.usp_isworking.Name = "usp_isworking";
            this.usp_isworking.ReadOnly = true;
            // 
            // usp_updatedby
            // 
            this.usp_updatedby.HeaderText = "Updated By";
            this.usp_updatedby.Name = "usp_updatedby";
            this.usp_updatedby.ReadOnly = true;
            this.usp_updatedby.Visible = false;
            // 
            // usp_lastdatemodified
            // 
            this.usp_lastdatemodified.HeaderText = "Last Date Modified";
            this.usp_lastdatemodified.Name = "usp_lastdatemodified";
            this.usp_lastdatemodified.ReadOnly = true;
            this.usp_lastdatemodified.Visible = false;
            // 
            // usp_oldusername
            // 
            this.usp_oldusername.HeaderText = "Old Username";
            this.usp_oldusername.Name = "usp_oldusername";
            this.usp_oldusername.ReadOnly = true;
            this.usp_oldusername.Visible = false;
            // 
            // usp_oldpassword
            // 
            this.usp_oldpassword.HeaderText = "Old Password";
            this.usp_oldpassword.Name = "usp_oldpassword";
            this.usp_oldpassword.ReadOnly = true;
            this.usp_oldpassword.Visible = false;
            // 
            // USPWebInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(741, 340);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.Name = "USPWebInfo";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "USP Web Information";
            this.Load += new System.EventHandler(this.USPWebInfo_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DataGridViewTextBoxColumn usp_id;
        private System.Windows.Forms.DataGridViewTextBoxColumn usp_uspname;
        private System.Windows.Forms.DataGridViewTextBoxColumn usp_website;
        private System.Windows.Forms.DataGridViewTextBoxColumn usp_username;
        private System.Windows.Forms.DataGridViewTextBoxColumn usp_password;
        private System.Windows.Forms.DataGridViewTextBoxColumn usp_isworking;
        private System.Windows.Forms.DataGridViewTextBoxColumn usp_updatedby;
        private System.Windows.Forms.DataGridViewTextBoxColumn usp_lastdatemodified;
        private System.Windows.Forms.DataGridViewTextBoxColumn usp_oldusername;
        private System.Windows.Forms.DataGridViewTextBoxColumn usp_oldpassword;
    }
}