﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace UC
{
    public partial class LoadingScreen : Form
    {
        string message_;
        public LoadingScreen(string message)
        {
            InitializeComponent();
            message_ = message;
            label1.Text = message_;
        }

    }
}
