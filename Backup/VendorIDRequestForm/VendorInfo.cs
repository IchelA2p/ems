﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VendorIDRequestForm
{
    public class VendorInfo
    {
        public string requested_vendorid { get; set; }
        public string created_vendorid { get; set; }
        public string name { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string c_o { get; set; }
        public string Bldg { get; set; }
        public string Office { get; set; }
        public string usp_dept { get; set; }
        public string POBox { get; set; }
        public string state { get; set; }
        public string zipcode { get; set; }
        public string dept { get; set; }
        public DateTime request_date { get; set; }
        public string compliance_status { get; set; }
        public string compliance_decline_reason { get; set; }
        public string avs_status { get; set; }
        public string avs_decline_reason { get; set; }
        public string notes { get; set; }
    }
}
