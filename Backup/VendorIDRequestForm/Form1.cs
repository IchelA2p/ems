﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Data.Linq.SqlClient;
using System.Globalization;
using System.Diagnostics;
using ExtensionMethods;
using System.IO;

namespace VendorIDRequestForm
{
    public partial class Form1 : Form
    {

        DateTime TimeTypingBegan { get; set; }
        DateTime TypingInterval { get; set; }

        BackgroundWorker bwLoad;
        BackgroundWorker bwZip;
        BackgroundWorker bwLoad_VIDSearch;

        DataTable dtvid;
        int cd = 0;
       string req_type;
       string ini_vid;
       string req_from;
       string ini_invoice;
       string user_role;
       string ini_utility;
       int mode;
       string app_action;

       public Form1(string type, string vid, string request_from, string invoice, string utility)
       {
           InitializeComponent();

           req_type = type;
           ini_vid = vid;
           ini_invoice = invoice;
           ini_utility = utility;
           req_from = request_from;          

           
           Form1_Load(null, null);

           FindUSP();

           dgv_VIDRequest.DataSource = null;

           if (user_role != "")
           {
               GetPendingVIDRequestsbyRole();
               ShowPriority();
           }

           //bwLoad = new BackgroundWorker();
           //bwLoad.DoWork += new DoWorkEventHandler(bwLoad_DoWork);


           //bwLoad_VIDSearch = new BackgroundWorker();
           //bwLoad_VIDSearch.DoWork += new DoWorkEventHandler(bwLoad_VIDSearch_DoWork);
       }

        public void bwLoad_DoWork(object sender, DoWorkEventArgs e)
        {
            string name = string.Empty;
            string state = string.Empty;
            DataTable dtvdr = new DataTable();

            char[] delimiterChars = { ' ', '\t' };

            tb_USPName.Invoke((Action)delegate{
                name = tb_USPName.Text.ToLower();
            });


            cmb_State.Invoke((Action)delegate
            {
                state =  cmb_State.Text.ToLower();
            });

            string[] words = name.Split(delimiterChars);
            string x = "%";

            foreach (string w in words)
            {
                x += w + "%";
            }

            DBDataContext db = new DBDataContext();
           
            var vdr_list = from vdr_data in db.vw_VIDs
                           where SqlMethods.Like(vdr_data.Vendor_Name.ToLower(), x.ToLower())
                           || (SqlMethods.Like(vdr_data.Address.ToLower(), tb_Street.Text.ToLower()) || SqlMethods.Equals(vdr_data.Address, string.Empty))
                           || (SqlMethods.Like(vdr_data.City.ToLower(), tb_City.Text.ToLower()) || SqlMethods.Equals(vdr_data.City, string.Empty))
                           || (SqlMethods.Like(vdr_data.State.ToLower(), cmb_State.Text.ToLower()) || SqlMethods.Equals(vdr_data.State, string.Empty))
                           || (SqlMethods.Like(vdr_data.Zipcode.ToLower(), tb_ZipCode.Text.ToLower()) || SqlMethods.Equals(vdr_data.Zipcode, string.Empty))
                           orderby vdr_data.Vendor_Name, vdr_data.Address 
                           select new VDRInfo()
                           {
                               Vendor_ID = vdr_data.Vendor_ID.Trim(),
                               vdr_status = GetStatus(vdr_data.vdr_status.ToString()),
                               vms_status = GetStatus(vdr_data.vms_status.ToString()),
                               Vendor_Name = vdr_data.Vendor_Name.Trim(),
                               Address = vdr_data.Address.Trim(),
                               City = vdr_data.City.Trim(),
                               State = vdr_data.State.Trim(),
                               Zipcode = vdr_data.Zipcode.Trim(),
                               Phonenumber = vdr_data.Phonenumber.Trim(),
                           };

            dgv_VDR.Invoke((Action)delegate
            {
               dgv_VDR.DataSource = vdr_list;
            });
        }


        string GetStatus(string val)
        {

            string x = "-";

            switch (val)
            {
                case "0":
                    x = "Inactive";
                    break;
                case "1":
                    x = "Active";
                    break;
                case "2":
                    x = "-";
                    break;
            }

            return x;
        }

        public void bwLoad_State()
        {
            string zip = tb_ZipCode.Text;

            DBDataContext db = new DBDataContext();

            var list = from data in db.vw_ZipState
                       where data.zip_code.ToLower().Equals(zip.ToLower())
                       select new ZipState()
                       {
                           zip_code = data.zip_code,
                           state = data.abbrev_state
                       };

                cmb_State.DataSource = list;
                cmb_State.DisplayMember = "state";
                cmb_State.ValueMember = "zip_code";
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (comboBox1.Text == "CREATE NEW" && dgv_VDR.Rows.Count == 0)
            {
                textBox5.Text = string.Empty;

                tb_USPName.ReadOnly = false;
                tb_Street.ReadOnly = false;
                tb_City.ReadOnly = false;
                cmb_State.Enabled = true;
                tb_ZipCode.ReadOnly = false;
                tb_CoOrAtt.ReadOnly = false;
                tb_BldgName.ReadOnly = false;
                tb_OfficeName.ReadOnly = false;
                tb_Dept.ReadOnly = false;
                tb_POBox.ReadOnly = false;
                textBox5.ReadOnly = false;
                textBox5.ReadOnly = false;
                tb_Email.ReadOnly = false;
            }
            else
            {
                if (textBox5.Text != "" && textBox5.Text != "none" && comboBox1.Text == "ACTIVATION")
                {
                    tb_USPName.ReadOnly = true;
                    tb_Street.ReadOnly = true;
                    tb_City.ReadOnly = true;
                    cmb_State.Enabled = false;
                    tb_ZipCode.ReadOnly = true;
                    tb_CoOrAtt.ReadOnly = true;
                    tb_BldgName.ReadOnly = true;
                    tb_OfficeName.ReadOnly = true;
                    tb_Dept.ReadOnly = true;
                    tb_POBox.ReadOnly = true;
                    textBox5.ReadOnly = true;
                    textBox5.ReadOnly = true;
                }
                else
                {
                    tb_USPName.ReadOnly = false;
                    tb_Street.ReadOnly = false;
                    tb_City.ReadOnly = false;
                    cmb_State.Enabled = true;
                    tb_ZipCode.ReadOnly = false;
                    tb_CoOrAtt.ReadOnly = false;
                    tb_BldgName.ReadOnly = false;
                    tb_OfficeName.ReadOnly = false;
                    tb_Dept.ReadOnly = false;
                    tb_POBox.ReadOnly = false;
                    textBox5.ReadOnly = false;
                    textBox5.ReadOnly = false;
                    tb_Email.ReadOnly = false;
                }
            }


            //tb_USPName.Text = string.Empty;
            //tb_Street.Text = string.Empty;
            //tb_City.Text = string.Empty;
            //cmb_State.Text = string.Empty;
            //tb_ZipCode.Text = string.Empty;
            //tb_CoOrAtt.Text = string.Empty;
            //tb_BldgName.Text = string.Empty;
            //tb_OfficeName.Text = string.Empty;
            //tb_Dept.Text = string.Empty;
            //tb_POBox.Text = string.Empty;
            //textBox5.Tag = null;
            //tb_Telephone.Text = string.Empty;
            //tb_Email.Text = string.Empty;

        }

        private void EnableSubmitButton(object sender, EventArgs e)
        {
            bool hasBlank = false;

            if (comboBox1.Text == "ACTIVATION")
            {

                if (textBox5.Text.ToString().Trim() == "")
                {
                    hasBlank = true;
                }
            }

            if (tb_USPName.Text.ToString().Trim() == "")
            {
                hasBlank = true;
            }


            if (tb_Street.Text.ToString().Trim() == "" && tb_POBox.Text.ToString().Trim() == "")
            {
                hasBlank = true;
            }


            if (cmb_State.Text.ToString().Trim() == "")
            {
                hasBlank = true;
            }

            if (tb_City.Text.ToString().Trim() == "")
            {
                hasBlank = true;
            }


            if (tb_ZipCode.Text.ToString().Trim() == "")
            {
                hasBlank = true;
            }


            if (tb_Telephone.Text.ToString().Trim() == "")
            {
                hasBlank = true;
            }


            if (cmbSource.SelectedIndex == -1)
            {
                hasBlank = true;
            }


            if (chkBox_Payments.Checked == false && chkBox_Water.Checked == false && chkBox_Others.Checked == false && chkBox_Gas.Checked == false && chkBox_Electricity.Checked == false)
            {
                hasBlank = true;
            }

            btn_Submit.Enabled = !hasBlank;



            //if (textBox5.Text != "none" && textBox5.Text != "")
            //{
            //    if (!bwLoad_VIDSearch.IsBusy)
            //    {
            //        bwLoad_VIDSearch.RunWorkerAsync();
            //    }
            //}
            //else
            //{
            //    if (!bwLoad.IsBusy)
            //    {
            //        bwLoad.RunWorkerAsync();
            //    }
            //}
        }

        
        private void numbersOnly(object sender, KeyPressEventArgs e)
        {
                            if (char.IsNumber(e.KeyChar) ||
                    char.IsControl(e.KeyChar))
                {
                    e.Handled = false;


                }
                else
                {
                    e.Handled = true;
                }

        }

        
        private void button1_Click(object sender, EventArgs e)
        {

            bool hasBlank = false;


            if (tb_USPName.Text.ToString().Trim() == "")
            {
                hasBlank = true;
            }


            if (tb_Street.Text.ToString().Trim() == "")
            {
                hasBlank = true;
            }


            if (tb_City.Text.ToString().Trim() == "")
            {
                hasBlank = true;
            }


            if (tb_ZipCode.Text.ToString().Trim() == "")
            {
                hasBlank = true;
            }


            if (tb_Telephone.Text.ToString().Trim() == "")
            {
                hasBlank = true;
            }


            if (hasBlank == true)
            {
                MessageBox.Show("Please fill in the mandatory fields.");

                return;
            }

            DBDataContext db = new DBDataContext();

            tbl_EM_VendorIDRequest data = new tbl_EM_VendorIDRequest();

            //data.type = comboBox1.Text;
            //data.dept = comboBox2.Text;
            data.vendor_account_requested = textBox5.Text;
            data.name = tb_USPName.Text;
            data.street_name = tb_Street.Text;
            data.city = tb_City.Text;
            data.state = cmb_State.Text;
            data.c_o = tb_CoOrAtt.Text;
            data.Bldg = tb_BldgName.Text;
            data.Office = tb_OfficeName.Text;
            data.POBox = tb_POBox.Text;
            data.usp_dept = tb_Dept.Text;
            data.postal_code = tb_ZipCode.Text;
            data.telephone = tb_Telephone.Text;
            data.source_of_information = tb_SourceOfInfo.Text;

           data.high_priority = cmbPrio.Text;

            //data.notes = 
            


            if (Convert.ToInt32(tb_ZipCode.Text.Length.ToString().Trim()) < 5)
            {
                MessageBox.Show("Zipcode must be 5 digits.");

                return;
            }


            if (Convert.ToInt32(tb_Telephone.Text.Length.ToString().Trim()) < 10)           
            {   
                MessageBox.Show("Telephone must be 10 digits.");

                return;
            }


            if (tb_Telephone.Text.ToString().Contains("99999") || tb_Telephone.Text.ToString().Contains("88888") || tb_Telephone.Text.ToString().Contains("77777") || tb_Telephone.Text.ToString().Contains("66666") || tb_Telephone.Text.ToString().Contains("55555") || tb_Telephone.Text.ToString().Contains("44444") || tb_Telephone.Text.ToString().Contains("33333") || tb_Telephone.Text.ToString().Contains("22222") || tb_Telephone.Text.ToString().Contains("11111") || tb_Telephone.Text.ToString().Contains("00000") || tb_Telephone.Text.ToString().Contains("12345") || tb_Telephone.Text.ToString().Contains("98765"))
            {
                MessageBox.Show("Invalid telephone number.");

                return;
            }

            if (cmbSource.Text == "Research/Internet" && tb_Email.Text.Trim() == "")
            {
                MessageBox.Show("Please provide the link for the source of information.");

                tb_Email.Focus();

                return;
            }

            if (req_from != "EMS" && cmbSource.Text == "Invoice" && tb_SourceOfInfo.Text.Trim() == "")
            {
                MessageBox.Show("Please provide the link of the invoice.");

                tb_SourceOfInfo.Focus();

                return;
            }

            string vendorGroupUtility = "";
            if (chkBox_Electricity.Checked)
            {
                vendorGroupUtility += "E";
            }
            if (chkBox_Gas.Checked)
            {
                vendorGroupUtility += "G";
            }
            if (chkBox_Others.Checked)
            {
                vendorGroupUtility += "O";
            }
            if (chkBox_Water.Checked)
            {
                vendorGroupUtility += "W";
            }
            if (chkBox_Payments.Checked)
            {
                vendorGroupUtility += "P";
            }

            data.vendor_group = vendorGroupUtility;

            if (textBox5.Tag == "" && comboBox1.Text == "ACTIVATION")
            {
                MessageBox.Show("Vendor ID is mandatory.");

                return;
            }

            string qry = string.Format(@"select distinct vdr_status, vms_status, Vendor_ID from vw_VID where Vendor_ID = '{0}'", textBox5.Text);

            SqlConnection con = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dtx = new DataTable();
            SqlDataAdapter dax = new SqlDataAdapter(qry, con);

            con.Open();
            dax.SelectCommand.CommandTimeout = 1800;
            dax.Fill(dtx);

            if (comboBox1.Text == "ACTIVATION")
            {
                
                if (dtx.Rows[0][0].ToString() == "1" && dtx.Rows[0][1].ToString() == "1")
                {
                    MessageBox.Show("Vendor ID is active.");

                    return;
                }
                else if (dtx.Rows[0][0].ToString() == "0")
                {
                    data.type = "VDR ACTIVATION";
                }
                else if (dtx.Rows[0][0].ToString() == "1" || dtx.Rows[0][1].ToString() == "0")
                {
                    data.type = "VMS ACTIVATION";
                }
                else if (dtx.Rows[0][0].ToString() == "2")
                {
                    data.type = "CREATE NEW";
                }
            }
            else
            {
                if (dtx.Rows.Count > 0)
                {
                    if (dtx.Rows[0][0].ToString() == "1" || dtx.Rows[0][1].ToString() == "2")
                    {
                        data.type = "CREATE NEW VMS";
                        data.vendor_account_requested = dtx.Rows[0][2].ToString();
                    }
                    else
                    {
                        data.type = "CREATE NEW";
                    }
                }
                else
                {
                    data.type = "CREATE NEW";
                }
            }

                string name = string.Empty;
                string strt = string.Empty;

                char[] delimiterChars = { ' ', '\t' };

                name = tb_USPName.Text.ToLower();
                strt = tb_Street.Text.ToLower();

                string[] words = name.Split(delimiterChars);
                string x = "%";

                foreach (string w in words)
                {
                    x += w + "%";
                }

                string[] street = strt.Split(delimiterChars);
                string y = "%";

                foreach (string z in street)
                {
                    y += z + "%";
                }


                string query = "";

                if (comboBox1.Text != "CREATE NEW")
                {

                    if (mode == 1)
                    {

                        query = string.Format(@"select COUNT(*) from tbl_EM_VendorIDRequest 
                                                where date_requested > '2016-11-16' and ([status] in ('NEW', 'RECON_APP', 'OPEN_HOLD') or [status] = 'COMP_APP' and ISNULL(vmo_status, '') IN ('', 'REQ_PEND'))
                                                and [type] = '{7}' 
                                                and (vendor_account_requested = '{0}' or
                                                (name like '{1}' and
                                                (street_name like '%''' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('{2}', 'PO', ''), 'P O', ''), 'P.O', ''), 'box', ''), ' ', '') + '''%'
                                                or street_name like '%{2}%'
                                                or street_name like '{4}'
                                                or pobox = '{3}')
                                                and [state] = '{5}'
                                                and postal_code = '{6}'))
                                                and request_id <> '{8}'",
                                                    textBox5.Text,
                                                    x.Replace("'", "''"),
                                                    strt,
                                                    tb_POBox.Text,
                                                    y.Replace("'", "''"),
                                                    cmb_State.Text,
                                                    tb_ZipCode.Text,
                                                    data.type,
                                                    comboBox1.Tag);
                    }
                    else
                    {
                        query = string.Format(@"select COUNT(*) from tbl_EM_VendorIDRequest 
                                                where date_requested > '2016-11-16' and ([status] in ('NEW', 'RECON_APP', 'OPEN_HOLD') or [status] = 'COMP_APP' and ISNULL(vmo_status, '') IN ('', 'REQ_PEND'))
                                                and [type] = '{7}' 
                                                and (vendor_account_requested = '{0}' or
                                                (name like '{1}' and
                                                (street_name like '%''' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('{2}', 'PO', ''), 'P O', ''), 'P.O', ''), 'box', ''), ' ', '') + '''%'
                                                or street_name like '%{2}%'
                                                or street_name like '{4}'
                                                or pobox = '{3}')
                                                and [state] = '{5}'
                                                and postal_code = '{6}'))",
                                                    textBox5.Text,
                                                    x.Replace("'", "''"),
                                                    strt,
                                                    tb_POBox.Text,
                                                    y.Replace("'", "''"),
                                                    cmb_State.Text,
                                                    tb_ZipCode.Text,
                                                    data.type);
                    }
                }
                else
                {

                    if (tb_POBox.Text != string.Empty)
                    {

                        if (mode == 1)
                        {
                            query = string.Format(@"select COUNT(*) from tbl_EM_VendorIDRequest 
                                                where date_requested > '2016-11-16' and [type] = 'CREATE NEW' and ([status] in ('NEW', 'RECON_APP', 'OPEN_HOLD') or [status] = 'COMP_APP' and ISNULL(vmo_status, '') IN ('', 'REQ_PEND'))  
                                                and
                                                (name like '{1}' and
                                                (street_name like '%''' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('{2}', 'PO', ''), 'P O', ''), 'P.O', ''), 'box', ''), ' ', '') + '''%'
                                                or street_name like '%{2}%'
                                                or street_name like '{4}'
                                                or pobox = '{3}')
                                                and [state] = '{5}'
                                                and postal_code = '{6}')
                                                and request_id <> '{8}'",
                                                        textBox5.Text,
                                                        x.Replace("'", "''"),
                                                        strt,
                                                        tb_POBox.Text,
                                                        y.Replace("'", "''"),
                                                        cmb_State.Text,
                                                        tb_ZipCode.Text,
                                                        comboBox1.Text,
                                                        comboBox1.Tag);
                        }
                        else
                        {
                            query = string.Format(@"select COUNT(*) from tbl_EM_VendorIDRequest 
                                                where date_requested > '2016-11-16' and [type] = 'CREATE NEW' and ([status] in ('NEW', 'RECON_APP', 'OPEN_HOLD') or [status] = 'COMP_APP' and ISNULL(vmo_status, '') IN ('', 'REQ_PEND'))  
                                                and
                                                (name like '{1}' and
                                                (street_name like '%''' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('{2}', 'PO', ''), 'P O', ''), 'P.O', ''), 'box', ''), ' ', '') + '''%'
                                                or street_name like '%{2}%'
                                                or street_name like '{4}'
                                                or pobox = '{3}')
                                                and [state] = '{5}'
                                                and postal_code = '{6}')",
                                                       textBox5.Text,
                                                       x.Replace("'", "''"),
                                                       strt,
                                                       tb_POBox.Text,
                                                       y.Replace("'", "''"),
                                                       cmb_State.Text,
                                                       tb_ZipCode.Text,
                                                       comboBox1.Text);
                        }
                    }
                    else
                    {

                        if (mode == 1)
                        {
                            query = string.Format(@"select COUNT(*) from tbl_EM_VendorIDRequest 
                                                where date_requested > '2016-11-16' and [type] = 'CREATE NEW' and ([status] in ('NEW', 'RECON_APP', 'OPEN_HOLD') or [status] = 'COMP_APP' and ISNULL(vmo_status, '') IN ('', 'REQ_PEND'))  
                                                and
                                                (name like '{1}' and
                                                (street_name like '%''' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('{2}', 'PO', ''), 'P O', ''), 'P.O', ''), 'box', ''), ' ', '') + '''%'
                                                or street_name like '%{2}%'
                                                or street_name like '{4}')
                                                and [state] = '{5}'
                                                and postal_code = '{6}')
                                                and request_id <> '{8}'",
                                                        textBox5.Text,
                                                        x.Replace("'", "''"),
                                                        strt,
                                                        tb_POBox.Text,
                                                        y.Replace("'", "''"),
                                                        cmb_State.Text,
                                                        tb_ZipCode.Text,
                                                        comboBox1.Text,
                                                        comboBox1.Tag);
                        }
                        else
                        {
                            query = string.Format(@"select COUNT(*) from tbl_EM_VendorIDRequest 
                                                where date_requested > '2016-11-16' and [type] = 'CREATE NEW' and ([status] in ('NEW', 'RECON_APP', 'OPEN_HOLD') or [status] = 'COMP_APP' and ISNULL(vmo_status, '') IN ('', 'REQ_PEND'))  
                                                and
                                                (name like '{1}' and
                                                (street_name like '%''' + REPLACE(REPLACE(REPLACE(REPLACE(REPLACE('{2}', 'PO', ''), 'P O', ''), 'P.O', ''), 'box', ''), ' ', '') + '''%'
                                                or street_name like '%{2}%'
                                                or street_name like '{4}')
                                                and [state] = '{5}'
                                                and postal_code = '{6}')",
                                                        textBox5.Text,
                                                        x.Replace("'", "''"),
                                                        strt,
                                                        tb_POBox.Text,
                                                        y.Replace("'", "''"),
                                                        cmb_State.Text,
                                                        tb_ZipCode.Text,
                                                        comboBox1.Text);
                        }
                    }

                }

                SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(query, conn);

                conn.Open();
                da.SelectCommand.CommandTimeout = 1800;
                da.Fill(dt);

                if (dt.Rows[0][0].ToString() != "0")
                {
                    MessageBox.Show("Vendor ID request already exists.");

                    return;
                }
            
            data.email = tb_Email.Text;
            data.status = "NEW";
            data.userid_requestor = Environment.UserName;
            data.date_requested = DateTime.Now;

            if (ini_invoice != "")
            {

                data.invoice_name = cmbSource.Text;
                data.source_of_information = ini_invoice;
            }
            else
            {

                data.invoice_name = cmbSource.Text;
            }

            data.version = "4.4";

            db.tbl_EM_VendorIDRequests.InsertOnSubmit(data);

            try
            {

                if ((user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS") && app_action == "update_req")
                {
                    updateRequest("UPDATE INFORMATION", comboBox1.Tag.ToString());
                }

                db.SubmitChanges();
                
                MessageBox.Show("Saved!");

                if (req_from == "EMS")
                {
                    this.Close();
                }

                //btnClear_Click(null, null);

                comboBox1.SelectedIndex = -1;

                dgv_VDR.DataSource = null;
                dgv_VIDRequest.DataSource = null;

                if (user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS")
                {
                    GetPendingVIDRequestsbyRole();
                    ShowPriority();
                }
                else
                {
                    FindVIDRequests();
                    //FindUSP();
                }

                app_action = "";
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ClearControls()
        {
            foreach (Control c in this.Controls)
            {
                if (c.GetType() == typeof(TextBox))
                {
                    ((TextBox)c).Text = string.Empty;
                }

                if (c.GetType() == typeof(ComboBox))
                {
                    ((ComboBox)c).Text = string.Empty;
                }

                if (c.GetType() == typeof(CheckBox))
                {
                    ((CheckBox)c).Checked = false;
                }
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (cb_NoEmail.Checked)
            {
                tb_Email.Text = "none";
                tb_Email.ReadOnly = true;
            }
            else
            {
                tb_Email.Text = string.Empty;
                tb_Email.ReadOnly = false;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            btnMain_Click(null, null);

            textBox5.Text = ini_vid;

            if (req_from == "EMS")
            {
                label27.Visible = true;
                grpSummary.Visible = false;
                grpSummary2.Visible = false;
                user_role = string.Empty;
                
                cmbSource.SelectedIndex = 1;
                cmbSource.Enabled = false;

                tb_SourceOfInfo.Text = ini_invoice;
                tb_SourceOfInfo.Enabled = false;

                label39.Visible = false;
                cmbPrio.Visible = false;

                if (ini_utility == "E")
                {
                    chkBox_Electricity.Checked = true;
                }
                else if (ini_utility == "G")
                {
                    chkBox_Gas.Checked = true;
                }
                else if (ini_utility == "W")
                {
                    chkBox_Water.Checked = true;
                }
                else if (ini_utility == "D" || ini_utility == "T" || ini_utility == "S")
                {
                    chkBox_Others.Checked = true;
                }
            }
            else
            {
                label27.Visible = false;

                user_role = GetUserRole();

                if (user_role != "")
                {

                    mode = 1;
                    grpSummary.Visible = true;
                    grpSummary2.Visible = true;
                    btnShowAll.Visible = true;
                    label17.Visible = false;

                    if (user_role != "L1" || user_role == "Admin")
                    {
                        tbRejects.HidePage();
                    }

                }
                else
                {                                        
                    grpSummary.Visible = false;
                    grpSummary2.Visible = false;
                    user_role = string.Empty;
                    btnShowAll.Visible = false;

                    tbRejects.HidePage();
                }
            }

        }


        private string GetUserRole()
        {
            string query = string.Format(@"select top 1 REFDESC from tbl_EM_ParameterReference where REFKEY = 'VIDMngtRole' and REFVALUE = '{0}'", Environment.UserName);

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            if (dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            else
            {
                return string.Empty;
            }
        }


        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            EnableSubmitButton(null, null);
        }

        private void cmbState_Leave(object sender, EventArgs e)
        {

            cmb_State.Text = cmb_State.Text.ToUpper();
            populateState();
        }

        private void textBox4_KeyUp(object sender, KeyEventArgs e)
        {

            cmb_State.DataSource = null;
            bwLoad_State();

            //if (comboBox1.Text == "CREATE NEW")
            //{
            //    FindUSP();
            //}
        }

        private void GetVIDRDtls(string req_id)
        {
            string query = string.Format(@"select distinct case when vendor_account_requested = 'none' then '' else vendor_account_requested end vendor_account_requested, name, c_o, Bldg, Office, dept, POBox, street_name, a.City, a.[State], postal_code, telephone, b.vdr_status, b.vms_status, [type], request_id, vendor_group, high_priority, source_of_information, case when invoice_name in ('Callout', 'Invoice', 'Research/Internet') then invoice_name else '' end [Source] from tbl_EM_VendorIDRequest a left join vw_VID b on a.vendor_account_requested = b.Vendor_ID where a.date_requested > '2016-11-16' and (vendor_account_requested = '{0}' or vendor_account_created = '{0}' or request_id = '{0}') ", req_id);
            string utility = "";

            chkBox_Electricity.Checked = false;
            chkBox_Gas.Checked = false;
            chkBox_Water.Checked = false;
            chkBox_Others.Checked = false;
            chkBox_Payments.Checked = false;
            cmbPrio.SelectedIndex = -1;

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            if (dt.Rows.Count > 0)
            {
                utility = dt.Rows[0]["vendor_group"].ToString();

                foreach (char c in utility.ToLower())
                {
                    if (c == 'e')
                    {
                        chkBox_Electricity.Checked = true;
                    }
                    if (c == 'g')
                    {
                        chkBox_Gas.Checked = true;
                    }
                    if (c == 'w')
                    {
                        chkBox_Water.Checked = true;
                    }
                    if (c == 'o')
                    {
                        chkBox_Others.Checked = true;
                    }
                    if (c == 'p')
                    {
                        chkBox_Payments.Checked = true;
                    }
                }

                cmbPrio.Text = dt.Rows[0]["high_priority"].ToString();

                textBox5.Text = dt.Rows[0][0].ToString().Trim();
                tb_USPName.Text = dt.Rows[0][1].ToString().Trim();
                tb_CoOrAtt.Text = dt.Rows[0][2].ToString().Trim();
                tb_BldgName.Text = dt.Rows[0][3].ToString().Trim();
                tb_OfficeName.Text = dt.Rows[0][4].ToString().Trim();
                tb_Dept.Text = dt.Rows[0][5].ToString().Trim();
                tb_POBox.Text = dt.Rows[0][6].ToString().Trim();

                tb_Street.Text = dt.Rows[0][7].ToString().Trim();
                tb_City.Text = dt.Rows[0][8].ToString().Trim();
                cmb_State.Text = dt.Rows[0][9].ToString().Trim();
                tb_ZipCode.Text = dt.Rows[0][10].ToString().Trim();
                tb_Telephone.Text = dt.Rows[0][11].ToString().Trim();
                

                if (dt.Rows[0][14].ToString() == "CREATE NEW")
                {
                    comboBox1.SelectedIndex = 1;
                }
                else
                {
                    comboBox1.SelectedIndex = 0;
                }

                if (dt.Rows[0]["vdr_status"].ToString() == "1" && dt.Rows[0]["vms_status"].ToString() == "1" && comboBox1.Text == "ACTIVATION")
                {
                    //MessageBox.Show("Vendor ID is active.");

                    textBox5.Tag = "True";
                    
                    return;
                }

                comboBox1.Tag = dt.Rows[0][15].ToString();
                tb_SourceOfInfo.Text = dt.Rows[0]["source_of_information"].ToString().Trim();
                cmbSource.Text = dt.Rows[0]["Source"].ToString().Trim();
            }
            else
            {



                tb_USPName.Text = string.Empty;
                tb_Street.Text = string.Empty;
                tb_City.Text = string.Empty;
                cmb_State.Text = string.Empty;
                tb_ZipCode.Text = string.Empty;
                tb_Telephone.Text = string.Empty;

                tb_CoOrAtt.Text = string.Empty;
                tb_BldgName.Text = string.Empty;
                tb_OfficeName.Text = string.Empty;
                tb_Dept.Text = string.Empty;
                tb_POBox.Text = string.Empty;

                comboBox1.SelectedIndex = 1;
            }
        }

        private void GetVIDDtls(string vendorid)
        {
            string query = string.Format(@"select distinct Vendor_ID, Vendor_Name, [Address], City, [State], case when len(Zipcode) = 4 then '0' + Zipcode else Zipcode end Zipcode, Phonenumber, vdr_status, vms_status from vw_VID where Vendor_ID = '{0}'", vendorid);

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            if (dt.Rows.Count > 0)
            {         


                textBox5.Text = dt.Rows[0][0].ToString().Trim();
                tb_USPName.Text = dt.Rows[0][1].ToString().Trim();
                tb_Street.Text = dt.Rows[0][2].ToString().Trim();
                tb_City.Text = dt.Rows[0][3].ToString().Trim();
                cmb_State.Text = dt.Rows[0][4].ToString().Trim();
                tb_ZipCode.Text = dt.Rows[0][5].ToString().Trim();
                tb_Telephone.Text = dt.Rows[0][6].ToString().Trim();
                //string utilities = dt.Rows[0]["vendorid
                if (dt.Rows[0]["vdr_status"].ToString() == "1" && dt.Rows[0]["vms_status"].ToString() == "1" && comboBox1.Text == "ACTIVATION")
                {
                    //MessageBox.Show("Vendor ID is active.");

                    textBox5.Tag = "True";

                    return;
                }
                else
                {

                    textBox5.Tag = "False";

                    tb_USPName.ReadOnly = true;
                    tb_Street.ReadOnly = true;
                    tb_City.ReadOnly = true;
                    cmb_State.Enabled = false;
                    tb_ZipCode.ReadOnly = true;
                    tb_CoOrAtt.ReadOnly = true;
                    tb_BldgName.ReadOnly = true;
                    tb_OfficeName.ReadOnly = true;
                    tb_Dept.ReadOnly = true;
                    tb_POBox.ReadOnly = true;
                    textBox5.ReadOnly = true;

                }

                comboBox1.SelectedIndex = 0;
            }
            else
            {

               

                tb_USPName.Text = string.Empty;
                tb_Street.Text = string.Empty;
                tb_City.Text = string.Empty;
                cmb_State.Text = string.Empty;
                tb_ZipCode.Text = string.Empty;
                tb_Telephone.Text = string.Empty;

                comboBox1.SelectedIndex = 1;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                populateUSP();
            }
            else
            {
                if (e.Handled = !(char.IsLetter(e.KeyChar)))
                {
                    e.Handled = false;
                }
                else
                {

                    e.Handled = true;
                }
            }
        }

        private void txtNote_TextChanged(object sender, EventArgs e)
        {

        }


        // VDR Datagrid view
        private void FindUSP()
        {

            string name = string.Empty;

            char[] delimiterChars = { ' ', '\t' };

            try
            {
            tb_USPName.Invoke((Action)delegate
            {
                name = tb_USPName.Text.ToLower().Replace("'", "''");
            });
            }
            catch { }
            
            string[] words = name.Split(delimiterChars);
            string x = "%";

            foreach (string w in words)
            {
                x += w + "%";
            }

            string query = string.Format(@"select distinct rtrim(Vendor_ID) [VENDOR ID], case when vdr_status = 0 then 'Inactive' when vdr_status = 1 then 'Active' else '-' end [VDR STATUS], case when vms_status = 0 then 'Inactive' when vms_status = 1 then 'Active' else '-' end [VMS STATUS], rtrim(Vendor_Name) [VENDOR NAME], rtrim([Address]) [ADDRESS], rtrim(City) CITY, rtrim([State]) [STATE], rtrim(Zipcode) ZIPCODE, Phonenumber [PHONE NUMBER] from  vw_VID  with(index(vendor_id, vendor_name)) where vdr_status < 2 and ");

            if (textBox5.Text != string.Empty)
            {
                query += string.Format("Vendor_ID like '{0}%' and ", textBox5.Text == "none" ? "" : textBox5.Text);
            }

            if (tb_USPName.Text != string.Empty)
            {
                query += string.Format("Vendor_Name like '{0}' and ", x);
            }


            if (tb_POBox.Text != string.Empty)
            {
                query += string.Format("[Address] like '%{0}%' and ", tb_POBox.Text);
            }


            if (tb_Street.Text != string.Empty)
            {
                query += string.Format("[Address] like '%{0}%' and ", tb_Street.Text);
            }


            if (tb_City.Text != string.Empty)
            {
                query += string.Format("City like '%{0}%' and ", tb_City.Text);
            }


            if (cmb_State.Text != string.Empty)
            {
                query += string.Format("[State] = '{0}' and ", cmb_State.Text);
            }


            if (tb_ZipCode.Text != string.Empty)
            {
                query += string.Format("Zipcode like '%{0}%' and ", tb_ZipCode.Text);
            }

            if (query.ToString().Substring(query.Length - 5, 5) == " and ")
            {
                query = query.ToString().Substring(0, query.Length - 4) + "order by [VENDOR NAME], [ADDRESS]";
            }
            else
            {
                query = query.Replace("where", "") + "order by [VENDOR NAME], [ADDRESS]";
            }

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            dgv_VDR.DataSource = dt;

            dgv_VDR.ColumnHeadersDefaultCellStyle.Font = new Font("Calibri", 9.75F, FontStyle.Bold);
        }


        private void FindVIDRequests()
        {
           

            string name = string.Empty;

            char[] delimiterChars = { ' ', '\t' };


            try
            {
                tb_USPName.Invoke((Action)delegate
                {
                    name = tb_USPName.Text.ToLower().Replace("'", "''");
                });
            }
            catch { }

            string[] words = name.Split(delimiterChars);
            string x = "%";

            foreach (string w in words)
            {
                x += w + "%";
            }


            string query = string.Format(@"select request_id [Request ID], case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end [Vendor ID], [type] [Request Type], name [USP Name],
                                        ltrim(rtrim(isnull(c_o, '') + ' ' + isnull(Bldg, '') + ' ' + isnull(Office, '') + ' ' + isnull(usp_dept, '') + ' ' + isnull(POBox, '') + ' ' + isnull(street_name, ''))) [Street],
                                        city [City], [state] [State], postal_code [Zipcode], telephone [Business Phone], userid_requestor [Requested By], date_requested [Date Requested],
                                        CASE WHEN [Status] = 'NEW' THEN 'Pending' WHEN [status] = 'RECON_APP' THEN 'Approved' WHEN [Status] IN ('RECON_REJ', 'DECLINED') THEN 'Rejected' END [1st Level Review Status], tl_decline_reason [1st Level Review Decline Reason], 
                                        CASE WHEN isnull(vmo_status, '') in ('REQ_PEND', '') then 'Pending'  when vmo_status = 'REQ_COM' then 'Approved' when vmo_status = 'REQ_INC' then 'Rejected' end [AVS Status], decline_reason [AVS Decline Reason], 
                                        CASE WHEN [status] in ('NEW', 'OPEN', 'RECON_APP') then 'Pending' when [status] = 'COMP_APP' then 'Approved' when [status] = 'COMP_REJ' then 'Rejected' end [Compliance Status], com_decline_reason [Compliance Decline Reason], [Notes], source_of_information [Source of Information], high_priority [Priority]
                                        from tbl_EM_VendorIDRequest where (([status] in ('NEW', 'OPEN', 'OPEN_HOLD') and [type] in ('VMS ACTIVATION', 'CREATE NEW VMS')) or ([status] IN ('NEW', 'OPEN', 'OPEN_HOLD', 'RECON_APP') and [type] in ('VDR ACTIVATION')) or ([status] IN ('COMP_APP', 'RECON_APP', 'NEW') and vmo_status is null and [type] = 'CREATE NEW')) and date_requested > '2016-11-16' AND ");
          

            //if (rbtnPending.Checked)
            //{
            //    query += "([status] in ('NEW', 'OPEN') or [status] = 'COMP_APP' and ISNULL(vmo_status, '') IN ('', 'REQ_PEND')) and ";
            //}

            if (textBox5.Text != string.Empty)
            {
                query += string.Format("case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end like '{0}%' and ", textBox5.Text == "none" ? "" : textBox5.Text);
            }

            if (tb_USPName.Text != string.Empty)
            {
                query += string.Format("name like '{0}' and ", x);
            }


            if (tb_POBox.Text != string.Empty)
            {
                query += string.Format("c_o like '%{0}%' and ", tb_POBox.Text);
            }


            if (tb_Street.Text != string.Empty)
            {
                query += string.Format("[street_name] like '%{0}%' and ", tb_Street.Text);
            }


            if (tb_City.Text != string.Empty)
            {
                query += string.Format("City like '%{0}%' and ", tb_City.Text);
            }


            if (cmb_State.Text != string.Empty)
            {
                query += string.Format("[State] = '{0}' and ", cmb_State.Text);
            }


            if (tb_ZipCode.Text != string.Empty)
            {
                query += string.Format("postal_code like '%{0}%' and ", tb_ZipCode.Text);
            }

            query = query.ToString().Substring(0, query.Length - 4) + "order by date_requested asc ";

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            dgv_VIDRequest.DataSource = null;
            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            dgv_VIDRequest.DataSource = dt;


            GetAllVIDRequests(x);

            try
            {
                dgv_VIDRequest.Columns["request_id"].Visible = false;
                dgv_VIDRequest.Columns["vendor_group"].Visible = false;
                dgv_VIDRequest.Columns["imgPrio"].Visible = false;
            }
            catch 
            {
            }
        }

        private void GetPendingVIDRequestsbyRole()
        {

            string name = string.Empty;

            char[] delimiterChars = { ' ', '\t' };


            try
            {
                tb_USPName.Invoke((Action)delegate
                {
                    name = tb_USPName.Text.ToLower().Replace("'", "''");
                });
            }
            catch { }

            string[] words = name.Split(delimiterChars);
            string x = "%";

            foreach (string w in words)
            {
                x += w + "%";
            }


            string query = string.Empty;

            if (user_role == "L1"  || user_role == "Admin")
            {

                GetRejectedVIDRequests(x);

                query = string.Format(@"select notes [Notes], case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end [Vendor ID], [type] [Request Type], name [USP Name], 
                                            ltrim(rtrim(isnull(c_o, '') + ' ' + isnull(Bldg, '') + ' ' + isnull(Office, '') + ' ' + isnull(usp_dept, '') + ' ' + isnull(POBox, '') + ' ' + isnull(street_name, ''))) [Street],
                                            city [City], [state] [State], postal_code [Zipcode], telephone [Business Phone], userid_requestor [Requested by], date_requested [Date Requested], request_id, high_priority [Priority reason], DATEDIFF(DAY, date_requested, GETDATE()) [Aging],
                                            case when isnull(b.InvoiceFolderName, '') <> '' then b.InvoiceFolderName when isnull(b.InvoiceFolderName, '') = '' and isnull(a.email, '') <> '' then a.email else a.source_of_information end [Link]
                                            from tbl_EM_VendorIDRequest a
                                            outer apply
                                            (select top 1 InvoiceFolderName from tbl_ADHOC_EM_Tasks where Invoice = a.source_of_information and isLatestData = 1) b 
                                            where date_requested > '2016-11-16' and [status] in ('NEW', 'OPEN_HOLD') and ");
            }
            else if (user_role == "COMP")
            {
                query = string.Format(@"select  notes [Notes], case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end [Vendor ID], [type] [Request Type], name [USP Name], 
                                            ltrim(rtrim(isnull(c_o, '') + ' ' + isnull(Bldg, '') + ' ' + isnull(Office, '') + ' ' + isnull(usp_dept, '') + ' ' + isnull(POBox, '') + ' ' + isnull(street_name, ''))) [Street], 
                                            city [City], [state] [State], postal_code [Zipcode], telephone [Business Phone], tl_reviewedby [Reviewed By], tl_reviewtimestamp [Date Reviewed], request_id, high_priority [Priority reason], DATEDIFF(DAY, tl_reviewtimestamp, GETDATE()) [Aging],
                                            case when isnull(b.InvoiceFolderName, '') <> '' then b.InvoiceFolderName when isnull(b.InvoiceFolderName, '') = '' and isnull(a.email, '') <> '' then a.email else a.source_of_information end [Link]
                                            from tbl_EM_VendorIDRequest a
                                            outer apply
                                            (select top 1 InvoiceFolderName from tbl_ADHOC_EM_Tasks where Invoice = a.source_of_information and isLatestData = 1) b 
                                            where [type] in ('VDR ACTIVATION', 'CREATE NEW') and [status] = 'RECON_APP' and date_requested > '2016-11-16' and ");
            }
            else if (user_role == "AVS")
            {
                query = string.Format(@"select  notes [Notes], case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end [Vendor ID], [type] [Request Type], name [USP Name], 
                                            ltrim(rtrim(isnull(c_o, '') + ' ' + isnull(Bldg, '') + ' ' + isnull(Office, '') + ' ' + isnull(usp_dept, '') + ' ' + isnull(POBox, '') + ' ' + isnull(street_name, ''))) [Street], 
                                            city [City], [state] [State], postal_code [Zipcode], telephone [Business Phone], tl_reviewedby [1st Level Review], tl_reviewtimestamp [1st Level Review Timestamp], 
                                            userid_compapp [Compliance Review], date_compapp [Compliance Review Timestamp], request_id, high_priority [Priority reason], DATEDIFF(DAY, date_compapp, GETDATE()) [Aging],
                                            case when isnull(b.InvoiceFolderName, '') <> '' then b.InvoiceFolderName when isnull(b.InvoiceFolderName, '') = '' and isnull(a.email, '') <> '' then a.email else a.source_of_information end [Link]
                                            from tbl_EM_VendorIDRequest a
                                            outer apply
                                            (select top 1 InvoiceFolderName from tbl_ADHOC_EM_Tasks where Invoice = a.source_of_information and isLatestData = 1) b 
                                            where [type] in ('VDR ACTIVATION', 'CREATE NEW') and [status] = 'COMP_APP' and vmo_status = 'REQ_PEND' and date_requested > '2016-11-16' and ");
            }

            if (textBox5.Text != string.Empty)
            {
                query += string.Format("case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end like '{0}%' and ", textBox5.Text == "none" ? "" : textBox5.Text);
            }

            if (tb_USPName.Text != string.Empty)
            {
                query += string.Format("name like '{0}' and ", x);
            }


            if (tb_POBox.Text != string.Empty)
            {
                query += string.Format("c_o like '%{0}%' and ", tb_POBox.Text);
            }


            if (tb_Street.Text != string.Empty)
            {
                query += string.Format("[street_name] like '%{0}%' and ", tb_Street.Text);
            }


            if (tb_City.Text != string.Empty)
            {
                query += string.Format("City like '%{0}%' and ", tb_City.Text);
            }


            if (cmb_State.Text != string.Empty)
            {
                query += string.Format("[State] = '{0}' and ", cmb_State.Text);
            }


            if (tb_ZipCode.Text != string.Empty)
            {
                query += string.Format("postal_code like '%{0}%' and ", tb_ZipCode.Text);
            }


            if (user_role == "L1" || user_role == "Admin")
            {
                query = query.ToString().Substring(0, query.Length - 4) + "order by case when isnull(high_priority, '') <> '' and isnull(high_priority, '') <> 'Others' then 1 else 0 end desc, date_requested asc ";
            }
            else if (user_role == "COMP")
            {
                query = query.ToString().Substring(0, query.Length - 4) + "order by case when isnull(high_priority, '') <> '' and isnull(high_priority, '') <> 'Others' then 1 else 0 end desc, tl_reviewtimestamp asc ";
            }
            else if (user_role == "AVS")
            {
                query = query.ToString().Substring(0, query.Length - 4) + "order by case when isnull(high_priority, '') <> '' and isnull(high_priority, '') <> 'Others' then 1 else 0 end desc, date_compapp asc ";
            }
           

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            dgv_VIDRequest.DataSource = dt;            

            try
            {
                dgv_VIDRequest.Columns["btnApprove"].Visible = true;
                dgv_VIDRequest.Columns["btnReject"].Visible = true;
                dgv_VIDRequest.Columns["imgPrio"].Visible = true;
                dgv_VIDRequest.Columns["cmbDeclineReason"].Visible = true;

                dgv_VIDRequest.Columns["request_id"].Visible = false;
                //dgv_VIDRequest.Columns["vendor_group"].Visible = false;

                dgv_VIDRequest.Columns["btnApprove"].Frozen = true;
                dgv_VIDRequest.Columns["btnReject"].Frozen = true;
                dgv_VIDRequest.Columns["imgPrio"].Frozen = true;


                //for (int row = 0; row <= dgv_VIDRequest.Rows.Count - 1; row++)
                //{
                //    ////DataGridViewImageCell cell = dgv_VIDRequest.Rows[row].Cells["imgPrio"] as DataGridViewImageCell;

                //    //if (dgv_VIDRequest.Rows[row].Cells["Priority"].Value != null && dgv_VIDRequest.Rows[row].Cells["Priority"].Value.ToString() != string.Empty)
                //    //{
                //    //    dgv_VIDRequest.Rows[row].Cells["imgPrio"].Value = (System.Drawing.Image)Properties.Resources.flag;
                //    //}
                //    //else
                //    //{
                //    //    dgv_VIDRequest.Rows[row].Cells["imgPrio"].Value = (System.Drawing.Image)Properties.Resources.dflag;
                //    //}
                //}
            }
            catch
            {

            }

            currentCount();


            GetAllVIDRequests(x);
        }

        private void GetRejectedVIDRequests(string vname)
        {
            string query = string.Format(@"select isnull(tl_decline_reason, '') [1st Level Rejection Reason], isnull(com_decline_reason, '') [2nd Level Rejection Reason], isnull(decline_reason, '') [3rd Level Rejection Reason], 
                                            notes [Notes], case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end [Vendor ID], [type] [Request Type], name [USP Name], 
                                            ltrim(rtrim(isnull(c_o, '') + ' ' + isnull(Bldg, '') + ' ' + isnull(Office, '') + ' ' + isnull(usp_dept, '') + ' ' + isnull(POBox, '') + ' ' + isnull(street_name, ''))) [Street], 
                                            city [City], [state] [State], postal_code [Zipcode], telephone [Business Phone], tl_reviewedby [1st Level Review], tl_reviewtimestamp [1st Level Review Timestamp], 
                                            userid_compapp [2nd Level Review], date_compapp [2nd Level Review Timestamp], username [3rd Level Review], last_date_modified [3rd Level Review Timestamp], request_id, high_priority [Priority reason], DATEDIFF(DAY, date_compapp, GETDATE()) [Aging],
                                            case when isnull(b.InvoiceFolderName, '') <> '' then b.InvoiceFolderName else a.source_of_information end [Link]
                                            from tbl_EM_VendorIDRequest a
                                            outer apply
                                            (select top 1 InvoiceFolderName from tbl_ADHOC_EM_Tasks where Invoice = a.source_of_information and isLatestData = 1) b  
                                            where date_requested > '2016-11-16' and ([status] = 'COMP_REJ' or vmo_status = 'REQ_INC') and ");

            if (textBox5.Text != string.Empty)
            {
                query += string.Format("case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end like '{0}%' and ", textBox5.Text == "none" ? "" : textBox5.Text);
            }

            if (tb_USPName.Text != string.Empty)
            {
                query += string.Format("name like '{0}' and ", vname);
            }


            if (tb_POBox.Text != string.Empty)
            {
                query += string.Format("c_o like '%{0}%' and ", tb_POBox.Text);
            }


            if (tb_Street.Text != string.Empty)
            {
                query += string.Format("[street_name] like '%{0}%' and ", tb_Street.Text);
            }


            if (tb_City.Text != string.Empty)
            {
                query += string.Format("City like '%{0}%' and ", tb_City.Text);
            }


            if (cmb_State.Text != string.Empty)
            {
                query += string.Format("[State] = '{0}' and ", cmb_State.Text);
            }


            if (tb_ZipCode.Text != string.Empty)
            {
                query += string.Format("postal_code like '%{0}%' and ", tb_ZipCode.Text);
            }


            if (user_role == "L1" || user_role == "Admin")
            {
                query = query.ToString().Substring(0, query.Length - 4) + "order by case when isnull(high_priority, '') <> '' and isnull(high_priority, '') <> 'Others' then 1 else 0 end desc, date_requested asc ";
            }

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            dgRejects.DataSource = dt;

            try
            {
                dgRejects.Columns["btnApprove_rej"].Visible = true;
                dgRejects.Columns["btnReject_rej"].Visible = true;
                dgRejects.Columns["imgPrio_rej"].Visible = true;
                dgRejects.Columns["cmbDeclineReason_rej"].Visible = true;

                dgRejects.Columns["request_id"].Visible = false;
            }
            catch (Exception ex)
            {

            }
        }

        private void GetAllVIDRequests(string vname)
        {
            string query = string.Format(@"select 
                                            request_id [Request ID], case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end [Vendor ID], [type] [Request Type], name [USP Name],
                                            ltrim(rtrim(isnull(c_o, '') + ' ' + isnull(Bldg, '') + ' ' + isnull(Office, '') + ' ' + isnull(usp_dept, '') + ' ' + isnull(POBox, '') + ' ' + isnull(street_name, ''))) [Street],
                                            city [City], [state] [State], postal_code [Zipcode], telephone [Business Phone], userid_requestor [Requested By], date_requested [Date Requested],
                                            CASE WHEN [Status] = 'NEW' THEN 'Pending' WHEN [status] in ('RECON_APP', 'COMP_APP', 'COMP_REJ') THEN 'Approved' WHEN [Status] IN ('RECON_REJ', 'DECLINED') THEN 'Rejected' END [1st Level Review Status], tl_decline_reason [1st Level Review Decline Reason], tl_reviewedby [1st Level Reviewed by], tl_reviewtimestamp [1st Level Review Timestamp],  
                                            CASE WHEN [status] in ('NEW', 'OPEN', 'RECON_APP') then 'Pending' when [status] = 'COMP_APP' then 'Approved' when [status] = 'COMP_REJ' then 'Rejected' end [Compliance Status], com_decline_reason [Compliance Decline Reason], userid_compapp [Compliance Reviewed by], date_compapp [Compliance Review Timestamp], 
                                            CASE WHEN [status] in ('RECON_REJ', 'COMP_REJ') then NULL WHEN isnull(vmo_status, '') in ('REQ_PEND', '') then 'Pending' when vmo_status = 'REQ_COM' then 'Approved' when vmo_status = 'REQ_INC' then 'Rejected' end [AVS Status], decline_reason [AVS Decline Reason], username [AVS Reviewed by], last_date_modified [AVS Review Timestamp], 
                                            [Notes], source_of_information [Source of Information]                                   
                                            from tbl_EM_VendorIDRequest
                                            where date_requested > '2016-11-16'
                                            and [type] in ('CREATE NEW', 'VDR ACTIVATION', 'VMS ACTIVATION') and [status] <> 'UPDATE INFORMATION' AND ");

            if (textBox5.Text != string.Empty)
            {
                query += string.Format("case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end like '{0}%' and ", textBox5.Text == "none" ? "" : textBox5.Text);
            }

            if (tb_USPName.Text != string.Empty)
            {
                query += string.Format("name like '{0}' and ", vname);
            }


            if (tb_POBox.Text != string.Empty)
            {
                query += string.Format("c_o like '%{0}%' and ", tb_POBox.Text);
            }


            if (tb_Street.Text != string.Empty)
            {
                query += string.Format("[street_name] like '%{0}%' and ", tb_Street.Text);
            }


            if (tb_City.Text != string.Empty)
            {
                query += string.Format("City like '%{0}%' and ", tb_City.Text);
            }


            if (cmb_State.Text != string.Empty)
            {
                query += string.Format("[State] = '{0}' and ", cmb_State.Text);
            }


            if (tb_ZipCode.Text != string.Empty)
            {
                query += string.Format("postal_code like '%{0}%' and ", tb_ZipCode.Text);
            }


            
            query = query.ToString().Substring(0, query.Length - 4) + "order by date_requested asc ";
            

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            dgAll.DataSource = dt;
        }


        private void tb_POBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.Handled = !(char.IsLetter(e.KeyChar)))
            {
                e.Handled = false;


            }
            else
            {
                e.Handled = true;
                if (e.KeyChar == (char)Keys.Enter)
                {
                    populatePOBox();
                }

            }
        }

        private void tb_POBox_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {
            
                if (textBox5.Text != "none" && textBox5.Text != "")
                {
                    int test;

                    if (int.TryParse(textBox5.Text, out test) == true)
                    {
                        //FindUSP();

                        if (mode == 1)
                        {
                            GetPendingVIDRequestsbyRole();
                            ShowPriority();
                        }
                        else
                        {
                            FindVIDRequests();
                        }

                        if (textBox5.Text.Length > 8 && mode != 1)
                        {
                            GetVIDDtls(textBox5.Text.ToString().Trim());
                        }
                        else
                        {
                            GetVIDRDtls(textBox5.Text.ToString().Trim());
                        }
                    }
                    else
                    {
                        MessageBox.Show("Invalid vendor ID");
                        textBox5.Text = string.Empty;
                    }

                    EnableSubmitButton(null, null);
                }
                else if (tb_USPName.Text != string.Empty)
                {
                    //FindUSP();

                    populateUSP();

                    if (mode == 1)
                    {
                        GetPendingVIDRequestsbyRole();
                        ShowPriority();
                    }
                    else
                    {
                        FindVIDRequests();
                    }
                }
        }

        private void tb_USPName_TextChanged(object sender, EventArgs e)
        {

            EnableSubmitButton(null, null);

            tb_USPName.CharacterCasing = CharacterCasing.Upper;

            tb_USPName.Text = tb_USPName.Text.ToString().Replace("  ", " ");

            if (textBox5.Text != string.Empty && textBox5.Text != "none")
            {
                comboBox1.Text = "ACTIVATION";
                //textBox5.Enabled = true;
                label18.Visible = true;
            }
            else
            {

                comboBox1.Text = "CREATE NEW";
                //textBox5.Text = "";
                //textBox5.Enabled = false;
                label18.Visible = false;
            }

            ////FindUSP();

            //if (!bwLoad.IsBusy)
            //{
            //    bwLoad.RunWorkerAsync();
            //}

            //FindVIDRequests();
        }

        private void cmb_State_TextChanged(object sender, EventArgs e)
        {

            EnableSubmitButton(null, null);

            //FindUSP();

            if (user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS")
            {
                GetPendingVIDRequestsbyRole();
                ShowPriority();
            }
            else
            {
                FindVIDRequests();
            }

            //if (cmb_State.Text.ToString().Trim() != "")
            //{
            //    FindUSP();

            //    FindVIDRequests();
            //}


        }

        private void tb_ZipCode_TextChanged(object sender, EventArgs e)
        {

            
        }

        private void tb_City_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void tb_Street_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void dgv_VDR_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (user_role == "" || req_from == "EMS")
            {
                if (dgv_VDR.RowCount > 0 && dgv_VDR .CurrentCell.RowIndex > -1)
                {
                    if (dgv_VDR.CurrentRow.Cells["VDR STATUS"].Value.ToString() == "Active" && (dgv_VDR.CurrentRow.Cells["VMS STATUS"].Value.ToString() == "Active"))
                    {
                        MessageBox.Show("Vendor ID is already active."); 
                    }
                    else if (dgv_VDR.CurrentRow.Cells["VDR STATUS"].Value.ToString() == "-")
                    {
                        GetVIDDtls(dgv_VDR.CurrentRow.Cells[0].Value.ToString().Trim());
                        comboBox1.SelectedIndex = 1;
                    }
                    else if (dgv_VDR.CurrentRow.Cells["VMS STATUS"].Value.ToString() == "-")
                    {
                        GetVIDDtls(dgv_VDR.CurrentRow.Cells[0].Value.ToString().Trim());
                        comboBox1.SelectedIndex = 1;
                    }
                    else
                    {
                        GetVIDDtls(dgv_VDR.CurrentRow.Cells[0].Value.ToString().Trim());
                    }
                }
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tb_USPName.Text = string.Empty;
            tb_Street.Text = string.Empty;
            tb_City.Text = string.Empty;
            cmb_State.Text = string.Empty;
            tb_ZipCode.Text = string.Empty;
            tb_CoOrAtt.Text = string.Empty;
            tb_BldgName.Text = string.Empty;
            tb_OfficeName.Text = string.Empty;
            tb_Dept.Text = string.Empty;
            tb_POBox.Text = string.Empty;
            textBox5.Text = string.Empty;
            textBox5.Tag = null;
            tb_Telephone.Text = string.Empty;
            tb_Email.Text = string.Empty;
            cb_NoEmail.Checked = false;
            cmbSource.SelectedIndex = -1;

            tb_USPName.ReadOnly = false;
            tb_Street.ReadOnly = false;
            tb_City.ReadOnly = false;
            cmb_State.Enabled = true;
            tb_ZipCode.ReadOnly = false;
            tb_CoOrAtt.ReadOnly = false;
            tb_BldgName.ReadOnly = false;
            tb_OfficeName.ReadOnly = false;
            tb_Dept.ReadOnly = false;
            tb_POBox.ReadOnly = false;
            textBox5.ReadOnly = false;
            textBox5.ReadOnly = false;
            tb_Email.ReadOnly = false;
            tb_SourceOfInfo.Text = "";

            comboBox1.Text = "";

            chkBox_Electricity.Checked = false;
            chkBox_Gas.Checked = false;
            chkBox_Others.Checked = false;
            chkBox_Water.Checked = false;
            chkBox_Payments.Checked = false;
            cmbPrio.SelectedIndex = -1;

            populateUSP();
            //FindUSP();

            if (user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS")
            {
                GetPendingVIDRequestsbyRole();
                ShowPriority();
            }
            else
            {
                    FindVIDRequests();
            }
            
        }

        private void rbtnPending_CheckedChanged(object sender, EventArgs e)
        {

            if (mode == 1)
            {
                GetPendingVIDRequestsbyRole();
                ShowPriority();
            }
            {
                FindVIDRequests();
            }

            //if (rbtnPending.Checked)
            //{
            //    dgv_VIDRequest.Columns["btnApprove"].Visible = true;
            //    dgv_VIDRequest.Columns["btnReject"].Visible = true;

            //    dgv_VIDRequest.Columns["btnApprove"].Frozen = true;
            //    dgv_VIDRequest.Columns["btnReject"].Frozen = true;
            //}
            //else
            //{
            //    dgv_VIDRequest.Columns["btnApprove"].Visible = false;
            //    dgv_VIDRequest.Columns["btnReject"].Visible = false;
            //}
        }
        private void dgv_VIDRequest_CellMouseDown(object sender, DataGridViewCellMouseEventArgs e)
        {
            // Ignore if a column or row header is clicked
            if (req_from == "EMS" && e.RowIndex != -1 && e.ColumnIndex != -1)
            {
                if (e.Button == MouseButtons.Right)
                {
                    DataGridViewCell clickedCell = (sender as DataGridView).Rows[e.RowIndex].Cells[e.ColumnIndex];

                    // Here you can do whatever you want with the cell
                    this.dgv_VIDRequest.CurrentCell = clickedCell;  // Select the clicked cell, for instance

                    // Get mouse position relative to the vehicles grid
                    var relativeMousePosition = dgv_VIDRequest.PointToClient(Cursor.Position);

                    // Show the context menu
                    this.contextMenuStrip1.Show(dgv_VIDRequest, relativeMousePosition);
                }
            }
        }

        private bool hasVIDReqInv()
        {
            string query = string.Format(@"select top 1 vendor_account_requested, name, street_name + ', ' + city + ', ' + [state] + ', ' + postal_code [Address] from tbl_EM_VendorIDRequest where invoice_name = '{0}' order by date_requested desc", ini_invoice);

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            if (dt.Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void tagToInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (ini_invoice != string.Empty)
            {

                if (hasVIDReqInv() == true)
                {
                    var res = MessageBox.Show("There is already a VID request tagged to the invoice. Do you want to change it?",
                            "Property Search", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (res == DialogResult.No)
                    {
                        this.Close();

                        return;
                    }
                }

                if ((dgv_VIDRequest.CurrentRow.Cells["AVS Status"].Value.ToString() == "Pending" || dgv_VIDRequest.CurrentRow.Cells["AVS Status"].Value.ToString() == "Approved") && dgv_VIDRequest.CurrentRow.Cells["Compliance Status"].Value.ToString() == "Pending")
                {
                    SqlConnection cnn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;"); ;
                    SqlCommand cmd;
                    string sql = string.Format(@"insert into tbl_EM_VendorIDRequest(vendor_account_requested, [type], date_requested, invoice_name, userid_requestor, name, postal_code, [state], city, street_name, [status])
                                             values('{0}', 'TAG TO INVOICE', GETDATE(), '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}')",
                                             dgv_VIDRequest.CurrentRow.Cells["Request ID"].Value, 
                                             ini_invoice, 
                                             Environment.UserName,
                                             dgv_VIDRequest.CurrentRow.Cells["USP Name"].Value,
                                             dgv_VIDRequest.CurrentRow.Cells["Zipcode"].Value,
                                             dgv_VIDRequest.CurrentRow.Cells["State"].Value,
                                             dgv_VIDRequest.CurrentRow.Cells["City"].Value,
                                             dgv_VIDRequest.CurrentRow.Cells["Street"].Value,
                                             "TAGGED");

                    try
                    {
                        cnn.Open();
                        cmd = new SqlCommand(sql, cnn);
                        cmd.ExecuteNonQuery();
                        cmd.Dispose();
                        cnn.Close();
                        
                        MessageBox.Show("Saved!");

                        this.Close();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                        
                        return;
                    }
                }
                else
                {
                    MessageBox.Show("Vendor ID request is already approved/rejected.");
                }
            }
        }

        private void tb_Telephone_TextChanged(object sender, EventArgs e)
        {
            if (tb_Telephone.Text != "")
            {

                int test;

                if (int.TryParse(tb_Telephone.Text, out test) == true)
                {

                    EnableSubmitButton(null, null);
                }
            }
        }

        private void comboBox1_TextChanged(object sender, EventArgs e)
        {
            EnableSubmitButton(null, null);
        }

        public void city_Changed()
        {
            string query = string.Empty;
            query = string.Format(@"select distinct(abbrev_state) from tbl_EM_ZipcodeStateCity where city_name='{0}'", tb_City.Text.ToString());
            //cmb_State.Items.Clear();
            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            conn.Open();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader rdr;
            rdr = cmd.ExecuteReader();
            List<string> lst = new List<string>();
            try
            {

                if (tb_ZipCode.Text == "" || tb_ZipCode.Text == null)
                {
                    cmb_State.Text = "";

                    while (rdr.Read())
                    {
                        //cmb_State.Items.Add(rdr[0].ToString());
                        lst.Add(rdr[0].ToString());
                    }


                    if (lst.Count > 0)
                    {
                        for (int x = 0; x < lst.Count; x++)
                        {
                            cmb_State.Items.Add(lst[x]);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
            rdr.Close();
            conn.Close();
        }

        public void zipCode_Changed()
        {

            string query = string.Empty;
            query = string.Format(@"select distinct(city_name),abbrev_state from tbl_EM_ZipcodeStateCity where zip_code='{0}'", tb_ZipCode.Text);

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            conn.Open();
            SqlCommand cmd = new SqlCommand(query, conn);
            SqlDataReader dr;
            dr = cmd.ExecuteReader();
            if (dr.Read())
            {
                tb_City.Text = dr[0].ToString();
                cmb_State.Text = dr[1].ToString();
            }


            dr.Close();
            conn.Close();
        }

        private void tb_Telephone_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }

        private void tb_ZipCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);

            if (e.KeyChar == (char)Keys.Enter)
            {
                populateZipCode();
            }

        }

        private void timer_CountDown_tick(object sender, EventArgs e)
        {
                       

        }

        private void dgv_VDR_DataSourceChanged(object sender, EventArgs e)
        {
            tb_VDR.Text = dgv_VDR.RowCount.ToString();
        }

        private void dgv_VIDRequest_DataSourceChanged(object sender, EventArgs e)
        {
            tb_VIDR.Text = dgv_VIDRequest.RowCount.ToString();

            if (user_role == "")
            {
                foreach (DataGridViewColumn column in dgv_VIDRequest.Columns)
                {
                    column.ReadOnly = true;
                }
            }
        }

        private void tb_USPName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                populateUSP();
            }
        }

        public void populatePOBox()
        {
            if (tb_POBox.Text.ToString().Trim() != "" || tb_USPName.Text.Length > 2)
            {
                //FindUSP();

                if (user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS")
                {
                    GetPendingVIDRequestsbyRole();
                    ShowPriority();
                }
                else
                {
                    FindVIDRequests();
                }
            }
        }

        public void populateStreet()
        {
            tb_Street.CharacterCasing = CharacterCasing.Upper;

            EnableSubmitButton(null, null);

            if (tb_Street.Text.ToString().Trim() != "")
            {
                city_Changed();

                //FindUSP();
                if (user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS")
                {
                    GetPendingVIDRequestsbyRole();
                    ShowPriority();
                }
                else
                {
                    FindVIDRequests();
                }
            }
        }

        public void populateCity()
        {
            tb_City.CharacterCasing = CharacterCasing.Upper;

            EnableSubmitButton(null, null);

            if (tb_City.Text.ToString().Trim() != "")
            {

                city_Changed();

                //FindUSP();

                if (user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS")
                {
                    GetPendingVIDRequestsbyRole();
                    ShowPriority();
                }
                else
                {
                    FindVIDRequests();
                }
            }
        }

        public void ShowPriority()
        {
            
            for (int row = 0; row <= dgv_VIDRequest.Rows.Count - 1; row++)
            {
                if (dgv_VIDRequest.Rows[row].Cells["Priority reason"].Value != null && dgv_VIDRequest.Rows[row].Cells["Priority reason"].Value.ToString() != String.Empty && dgv_VIDRequest.Rows[row].Cells["Priority reason"].Value.ToString() != "Others")
                {
                    dgv_VIDRequest.Rows[row].Cells["imgPrio"].Value = (System.Drawing.Image)Properties.Resources.flag;
                }
                else
                {
                    dgv_VIDRequest.Rows[row].Cells["imgPrio"].Value = (System.Drawing.Image)Properties.Resources.dflag;
                }

                dgv_VIDRequest.Columns["imgPrio"].ReadOnly = true;

                ((DataGridViewTextBoxColumn)dgv_VIDRequest.Columns["Vendor ID"]).MaxInputLength = 10;

                if (dgv_VIDRequest.Rows[row].Cells["Request Type"].Value.ToString().ToUpper() != "CREATE NEW")
                {
                    dgv_VIDRequest.Rows[row].Cells["Vendor ID"].ReadOnly = true;
                }

                if (user_role == "L1" || user_role == "Admin")
                {
                    if ( Convert.ToInt32(dgv_VIDRequest.Rows[row].Cells["Aging"].Value) > 1)
                    {
                        dgv_VIDRequest.Rows[row].Cells["USP Name"].Style.ForeColor = Color.Red;
                    }
                    else
                    {
                        dgv_VIDRequest.Rows[row].Cells["USP Name"].Style.ForeColor = Color.Black;
                    }
                }

                if (user_role == "COMP")
                {
                    if (Convert.ToInt32(dgv_VIDRequest.Rows[row].Cells["Aging"].Value) > 2)
                    {
                        dgv_VIDRequest.Rows[row].Cells["USP Name"].Style.ForeColor = Color.Red;
                    }
                    else
                    {
                        dgv_VIDRequest.Rows[row].Cells["USP Name"].Style.ForeColor = Color.Black;
                    }
                }

                if (user_role == "AVS")
                {
                    if (Convert.ToInt32(dgv_VIDRequest.Rows[row].Cells["Aging"].Value) > 1)
                    {
                        dgv_VIDRequest.Rows[row].Cells["USP Name"].Style.ForeColor = Color.Red;
                    }
                    else
                    {
                        dgv_VIDRequest.Rows[row].Cells["USP Name"].Style.ForeColor = Color.Black;
                    }
                }
            }


            for (int row = 0; row <= dgRejects.Rows.Count - 1; row++)
            {
                if (dgRejects.Rows[row].Cells["Priority reason"].Value != null && dgRejects.Rows[row].Cells["Priority reason"].Value.ToString() != String.Empty && dgRejects.Rows[row].Cells["Priority reason"].Value.ToString() != "Others")
                {
                    dgRejects.Rows[row].Cells["imgPrio_rej"].Value = (System.Drawing.Image)Properties.Resources.flag;
                }
                else
                {
                    dgRejects.Rows[row].Cells["imgPrio_rej"].Value = (System.Drawing.Image)Properties.Resources.dflag;
                }

                dgRejects.Columns["imgPrio_rej"].ReadOnly = true;

                ((DataGridViewTextBoxColumn)dgRejects.Columns["Vendor ID"]).MaxInputLength = 10;

                if (dgRejects.Rows[row].Cells["Request Type"].Value.ToString().ToUpper() != "CREATE NEW")
                {
                    dgRejects.Rows[row].Cells["Vendor ID"].ReadOnly = true;
                }

                if (user_role == "L1" || user_role == "Admin")
                {

                    if (Convert.ToInt32(dgRejects.Rows[row].Cells["Aging"].Value) > 1)
                    {
                        dgRejects.Rows[row].Cells["USP Name"].Style.ForeColor = Color.Red;
                    }
                    else
                    {
                        dgRejects.Rows[row].Cells["USP Name"].Style.ForeColor = Color.Black;
                    }
                }
            }
        }


        public void populateUSP()
        {
            EnableSubmitButton(null, null);

            //tb_USPName.CharacterCasing = CharacterCasing.Upper;

            //tb_USPName.Text = tb_USPName.Text.ToString().Replace("  ", " ");

            //if (textBox5.Text != string.Empty && textBox5.Text != "none")
            //{
            //    comboBox1.Text = "ACTIVATION";
            //    //textBox5.Enabled = true;
            //    label18.Visible = true;
            //}
            //else
            //{

            //     comboBox1.Text = "CREATE NEW";
            //    //textBox5.Text = "";
            //    //textBox5.Enabled = false;
            //    label18.Visible = false;
            //}

            ////FindUSP();

            //if (!bwLoad.IsBusy)
            //{
            //    bwLoad.RunWorkerAsync();
            //}

            FindUSP();

            if (user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS")
            {
                GetPendingVIDRequestsbyRole();
                ShowPriority();
            }
            else
            {
                FindVIDRequests();
            }
        }

        private void tb_USPName_Leave(object sender, EventArgs e)
        {
            if (tb_USPName.Text.Length > 1)
            {
                populateUSP();
            }
        }

        private void tb_POBox_Leave(object sender, EventArgs e)
        {
            populatePOBox();
        }

        private void tb_Street_Leave(object sender, EventArgs e)
        {
            populateStreet();
        }

        private void tb_Street_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                populateStreet();
            }
        }

        private void tb_City_Leave(object sender, EventArgs e)
        {
            populateCity();
        }

        private void tb_City_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                populateCity();
            }
        }

        public void populateState()
        {

            EnableSubmitButton(null, null);

            //FindUSP();

            if (user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS")
            {
                GetPendingVIDRequestsbyRole();
                ShowPriority();
            }
            else
            {
                FindVIDRequests();
            }
        }

        public void populateZipCode()
        {

            int test;

            if (int.TryParse(tb_ZipCode.Text, out test) == true)
            {

                EnableSubmitButton(null, null);

                if (tb_ZipCode.Text.ToString().Trim() != "")
                {
                    zipCode_Changed();

                    //FindUSP();

                    if (user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS")
                    {
                        GetPendingVIDRequestsbyRole();
                        ShowPriority();
                    }
                    else
                    {
                        FindVIDRequests();
                    }
                }
            }
            else
            {
                if (tb_ZipCode.Text != "")
                {

                    MessageBox.Show("Invalid zipcode.");
                    tb_ZipCode.Text = "";
                }
            }

        }

        private void cmb_State_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                populateState();
            }
          
        }

        private void tb_ZipCode_Leave(object sender, EventArgs e)
        {
            populateZipCode();
        }


        public void updateRequest(string _updateStatus, string currentId)
        {

            string query = string.Format("Update tbl_EM_VendorIDRequest set status='{0}', last_date_modified = GETDATE(), username = '{1}' where request_id='{2}'  ", _updateStatus, Environment.UserName, currentId);
            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            SqlCommand cmd_updateRequest = new SqlCommand(query, conn);
            conn.Open();
            cmd_updateRequest.ExecuteNonQuery();
        }

        public void updateStatus_L1(string _updateStatus, int mode)
        {
            string userName = Environment.UserName;
            string currentDate = DateTime.Now.ToString();
            string currentId = dgv_VIDRequest.CurrentRow.Cells["request_id"].Value.ToString();
            string rejreason = string.Empty;
            string currentNote = dgv_VIDRequest.CurrentRow.Cells["notes"].Value.ToString();
            string currentPriority = dgv_VIDRequest.CurrentRow.Cells["Priority reason"].Value.ToString();

            if (mode == 0)
            {
                CreateLogs();

                currentId = dgRejects.CurrentRow.Cells["request_id"].Value.ToString();
                currentNote = dgRejects.CurrentRow.Cells["notes"].Value.ToString();
                currentPriority = dgRejects.CurrentRow.Cells["Priority reason"].Value.ToString();

                if (_updateStatus == "RECON_REJ")
                {
                    rejreason = dgRejects.CurrentRow.Cells["cmbDeclineReason_rej"].Value.ToString();
                }
            }
            else
            {
                if (_updateStatus == "RECON_REJ")
                {
                    rejreason = dgv_VIDRequest.CurrentRow.Cells["cmbDeclineReason"].Value.ToString();
                }
            }

            string query = string.Empty;

            if (_updateStatus == "RECON_REJ")
            {
                query = string.Format("Update tbl_EM_VendorIDRequest set status='{0}',tl_reviewtimestamp=GETDATE(),tl_reviewedby='{2}',tl_decline_reason='{4}', notes=isnull(notes,'') + '{5}', high_priority = '{6}' where request_id='{3}'  ", _updateStatus, currentDate, userName, currentId, rejreason, currentNote, currentPriority);

                if ((dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("www") == false || dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("@") == false) && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString() != string.Empty)
                {
                    if ((dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("www") == false && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("@") == false) && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString() != string.Empty)
                    {
                        if (dgv_VIDRequest.CurrentRow.Cells["Request Type"].Value.ToString() == "VMS ACTIVATION" || dgv_VIDRequest.CurrentRow.Cells["Request Type"].Value.ToString() == "CREATE NEW VMS")
                        {
                            query += string.Format(@"update c set c.Complete = 33, c.PendingReasons = 'Rejected VID request'
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_ADHOC_EM_Tasks c
                                            on a.source_of_information = c.Invoice
                                            and c.PendingReasons = 'Vendor ID request'
                                            and c.isLatestData = 1
                                            where a.request_id = {0}


                                            update c set c.Complete = 33, c.PendingReasons = 'Rejected VID request'
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_EM_VendorIDRequest b
                                            on cast(a.request_id as varchar(11)) = b.vendor_account_requested
                                            left join tbl_ADHOC_EM_Tasks c
                                            on b.invoice_name = c.Invoice
                                            and c.PendingReasons = 'Vendor ID request'
                                            and c.isLatestData = 1
                                            where a.request_id = {0}", currentId);
                        }
                    }
                }
            }
            else
            {
                query = string.Format("Update tbl_EM_VendorIDRequest set status='{0}',tl_reviewtimestamp=GETDATE(),tl_reviewedby='{2}',tl_decline_reason='{4}', high_priority = '{6}' where request_id='{3}'  ", _updateStatus, currentDate, userName, currentId, rejreason, currentNote, currentPriority);

                if ((dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("www") == false && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("@") == false) && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString() != string.Empty)
                {
                    if (dgv_VIDRequest.CurrentRow.Cells["Request Type"].Value.ToString() == "VMS ACTIVATION" || dgv_VIDRequest.CurrentRow.Cells["Request Type"].Value.ToString() == "CREATE NEW VMS")
                    {
                        query += string.Format(@"update c set c.Complete = 1, c.PendingReasons = 'Approved VID request', c.VendorId = a.vendor_account_requested
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_ADHOC_EM_Tasks c
                                            on a.source_of_information = c.Invoice
                                            and c.PendingReasons = 'Vendor ID request'
                                            and c.isLatestData = 1
                                            where a.request_id = {0}


                                            update c set c.Complete = 1, c.PendingReasons = 'Approved VID request', c.VendorId = a.vendor_account_requested
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_EM_VendorIDRequest b
                                            on cast(a.request_id as varchar(11)) = b.vendor_account_requested
                                            left join tbl_ADHOC_EM_Tasks c
                                            on b.invoice_name = c.Invoice
                                            and c.PendingReasons = 'Vendor ID request'
                                            and c.isLatestData = 1
                                            where a.request_id = {0}


                                            update c set c.VendorId = a.vendor_account_requested
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_EM_VendorIDRequest b
                                            on cast(a.request_id as varchar(11)) = b.vendor_account_requested
                                            left join tbl_ADHOC_EM_Tasks c
                                            on cast(a.request_id as varchar(25)) = c.VendorId
                                            and c.PendingReasons <> 'Vendor ID request'
                                            and c.isLatestData = 1
                                            where a.request_id = {0}", currentId);
                    }
                }
            }
            
            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            SqlCommand cmd_updateStatus = new SqlCommand(query, conn);
            conn.Open();

            try
            {
                cmd_updateStatus.ExecuteNonQuery();

                if (_updateStatus == "RECON_APP")
                {
                    MessageBox.Show("Request has been approved!");
                }
                else
                {
                    MessageBox.Show("Request has been rejected!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save the changes. Error: " + ex.ToString());
                conn.Close();
                return;
            }
        }

        public void updateStatus_Comp(string _updateStatus, string _updateVMOStatus)
        {
            string userName = Environment.UserName;
            string currentDate = DateTime.Now.ToString();
            string currentId = dgv_VIDRequest.CurrentRow.Cells["request_id"].Value.ToString();
            string currentNote = dgv_VIDRequest.CurrentRow.Cells["notes"].Value.ToString();
            string currentPriority = dgv_VIDRequest.CurrentRow.Cells["Priority reason"].Value.ToString();
            string rejreason = string.Empty;

            if (_updateVMOStatus == "null")
            {
                _updateVMOStatus = "=null";
            }
            else
            {
                _updateVMOStatus = "='" + _updateVMOStatus+"'";
            }

            if (_updateStatus == "COMP_REJ")
            {
                rejreason = dgv_VIDRequest.CurrentRow.Cells["cmbDeclineReason"].Value.ToString();
            }

            string query = string.Empty;
            
            
            query = string.Format("Update tbl_EM_VendorIDRequest set status='{0}',date_compapp=GETDATE(),userid_compapp='{2}', vmo_status {4}, notes= isnull(notes,'') + '{5}', com_decline_reason='{6}' where request_id='{3}'  ", _updateStatus, currentDate, userName, currentId, _updateVMOStatus, currentNote, rejreason);

            if (_updateStatus == "COMP_APP")
            {
                if ((dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("www") == false && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("@") == false) && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString() != string.Empty)
                {
                    if (dgv_VIDRequest.CurrentRow.Cells["Request Type"].Value.ToString() == "VDR ACTIVATION")
                    {
                        query += string.Format(@"update c set c.Complete = 1, c.PendingReasons = 'Approved VID request', c.VendorId = a.vendor_account_requested
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_ADHOC_EM_Tasks c
                                            on a.source_of_information = c.Invoice
                                            and c.PendingReasons = 'Vendor ID request'
                                            and c.isLatestData = 1
                                            where a.request_id = {0}


                                            update c set c.Complete = 1, c.PendingReasons = 'Approved VID request', c.VendorId = a.vendor_account_requested
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_EM_VendorIDRequest b
                                            on cast(a.request_id as varchar(11)) = b.vendor_account_requested
                                            left join tbl_ADHOC_EM_Tasks c
                                            on b.invoice_name = c.Invoice
                                            and c.PendingReasons = 'Vendor ID request'
                                            and c.isLatestData = 1
                                            where a.request_id = {0}


                                            update c set c.VendorId = a.vendor_account_requested
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_EM_VendorIDRequest b
                                            on cast(a.request_id as varchar(11)) = b.vendor_account_requested
                                            left join tbl_ADHOC_EM_Tasks c
                                            on cast(a.request_id as varchar(25)) = c.VendorId
                                            and c.PendingReasons <> 'Vendor ID request'
                                            and c.isLatestData = 1
                                            where a.request_id = {0}", currentId);
                    }
                }
            }
            else
            {
                if ((dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("www") == false && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("@") == false) && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString() != string.Empty)
                {
                    query += string.Format(@"update c set c.Complete = 33, c.PendingReasons = 'Rejected VID request'
                                        from tbl_EM_VendorIDRequest a
                                        left join tbl_ADHOC_EM_Tasks c
                                        on a.source_of_information = c.Invoice
                                        and c.PendingReasons = 'Vendor ID request'
                                        and c.isLatestData = 1
                                        where a.request_id = {0}


                                        update c set c.Complete = 33, c.PendingReasons = 'Rejected VID request'
                                        from tbl_EM_VendorIDRequest a
                                        left join tbl_EM_VendorIDRequest b
                                        on cast(a.request_id as varchar(11)) = b.vendor_account_requested
                                        left join tbl_ADHOC_EM_Tasks c
                                        on b.invoice_name = c.Invoice
                                        and c.PendingReasons = 'Vendor ID request'
                                        and c.isLatestData = 1
                                        where a.request_id = {0}", currentId);
                }
            }


            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            SqlCommand cmd_updateStatus = new SqlCommand(query, conn);
            conn.Open();

            try
            {                
                cmd_updateStatus.ExecuteNonQuery();

                if (_updateStatus == "COMP_APP")
                {
                    MessageBox.Show("Request has been approved!");
                }
                else
                {
                    MessageBox.Show("Request has been rejected!");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Unable to save the changes. Error: " + ex.ToString());
                conn.Close();
                return;
            }
        }

        public void updateStatus_AVS(string _updateVMOStatus)
        {

            string userName = Environment.UserName;
            string currentDate = DateTime.Now.ToString();
            string currentId = dgv_VIDRequest.CurrentRow.Cells["request_id"].Value.ToString();
            string currentNote = dgv_VIDRequest.CurrentRow.Cells["notes"].Value.ToString();
            string currentPriority = dgv_VIDRequest.CurrentRow.Cells["Priority reason"].Value.ToString();
            string vendorID = dgv_VIDRequest.CurrentRow.Cells["Vendor ID"].Value.ToString();
            string rejreason = string.Empty;

            if (_updateVMOStatus == "REQ_INC")
            {
                rejreason = dgv_VIDRequest.CurrentRow.Cells["cmbDeclineReason"].Value.ToString();
            }
            
            string u_query = string.Format(@"Update tbl_EM_VendorIDRequest set vmo_status='{0}',last_date_modified=GETDATE(),username='{2}', notes=isnull(notes,'') + '{4}', vendor_account_created='{5}', decline_reason='{6}' where request_id='{3}'", _updateVMOStatus, currentDate, userName, currentId, currentNote, vendorID, rejreason);

            if (_updateVMOStatus == "REQ_COM")
            {
                if ((dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("www") == false && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("@") == false) && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString() != string.Empty)
                {
                    u_query += string.Format(@"update c set c.Complete = 1, c.PendingReasons = 'Approved VID request', c.VendorId = a.vendor_account_created
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_ADHOC_EM_Tasks c
                                            on a.source_of_information = c.Invoice
                                            and c.PendingReasons = 'Vendor ID request'
                                            and c.isLatestData = 1
                                            where a.request_id = {0}


                                            update c set c.Complete = 1, c.PendingReasons = 'Approved VID request', c.VendorId = a.vendor_account_created
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_EM_VendorIDRequest b
                                            on cast(a.request_id as varchar(11)) = b.vendor_account_created
                                            left join tbl_ADHOC_EM_Tasks c
                                            on b.invoice_name = c.Invoice
                                            and c.PendingReasons = 'Vendor ID request'
                                            and c.isLatestData = 1
                                            where a.request_id = {0}


                                            update c set c.VendorId = a.vendor_account_created
                                            from tbl_EM_VendorIDRequest a
                                            left join tbl_EM_VendorIDRequest b
                                            on cast(a.request_id as varchar(11)) = b.vendor_account_created
                                            left join tbl_ADHOC_EM_Tasks c
                                            on cast(a.request_id as varchar(25)) = c.VendorId
                                            and c.PendingReasons <> 'Vendor ID request'
                                            and c.isLatestData = 1
                                            where a.request_id = {0}", currentId);
                }
            }
            else
            {
                if ((dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("www") == false && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString().Contains("@") == false) && dgv_VIDRequest.CurrentRow.Cells["Link"].Value.ToString() != string.Empty)
                {
                    u_query += string.Format(@"update c set c.Complete = 33, c.PendingReasons = 'Rejected VID request'
                                        from tbl_EM_VendorIDRequest a
                                        left join tbl_ADHOC_EM_Tasks c
                                        on a.source_of_information = c.Invoice
                                        and c.PendingReasons = 'Vendor ID request'
                                        and c.isLatestData = 1
                                        where a.request_id = {0}


                                        update c set c.Complete = 33, c.PendingReasons = 'Rejected VID request'
                                        from tbl_EM_VendorIDRequest a
                                        left join tbl_EM_VendorIDRequest b
                                        on cast(a.request_id as varchar(11)) = b.vendor_account_requested
                                        left join tbl_ADHOC_EM_Tasks c
                                        on b.invoice_name = c.Invoice
                                        and c.PendingReasons = 'Vendor ID request'
                                        and c.isLatestData = 1
                                        where a.request_id = {0}", currentId);
                }
            }

//            string i_query = string.Format(@"insert into tbl_EM_VDR(Vendor_ID, Vendor_Name, [Address], isActive, City, [State], Zipcode, [Type])
//                                        select '{5}', name, ltrim(rtrim(isnull(c_o, '') + ' ' + isnull(Bldg, '') + ' ' + isnull(Office, '') + ' ' + isnull(usp_dept, '') + ' ' + isnull(POBox, '') + ' ' + isnull(street_name, ''))),
//                                        1, city, [state], postal_code, 'UTILITY' from tbl_EM_VendorIDRequest where request_id = '{3}'", _updateVMOStatus, currentDate, userName, currentId, currentNote, vendorID, rejreason);

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            SqlCommand cmd_updateStatus = new SqlCommand(u_query, conn);
            
            //SqlCommand cmd_newVID = new SqlCommand(i_query, conn);

            conn.Open();
            cmd_updateStatus.ExecuteNonQuery();
            conn.Close();

            try
            {
                conn.Open();
                //cmd_newVID.ExecuteNonQuery();
                conn.Close();

                if (_updateVMOStatus == "REQ_COM")
                {
                    MessageBox.Show("Request has been approved!");
                }
                else
                {
                    MessageBox.Show("Request has been rejected!");
                }
            }
            catch (Exception ex)
            {
                if (ex.ToString().Contains("duplicate key"))
                {
                    MessageBox.Show("Vendor ID already exists!");
                }
                else
                {
                    MessageBox.Show("Unable to save the changes. Error: " + ex.ToString());
                }

                conn.Close();
                return;
            }
        }

        private void CreateLogs()
        {
            string i_query = string.Format(@"insert into tbl_EM_VendorIDRequestLogs
                                            select request_id, case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end, name, 
                                            ltrim(rtrim(isnull(c_o, '') + ' ' + isnull(Bldg, '') + ' ' + isnull(Office, '') + ' ' + isnull(usp_dept, '') + ' ' + isnull(POBox, '') + ' ' + isnull(street_name, ''))),
                                            city, [state], postal_code, telephone,tl_reviewedby, tl_reviewtimestamp, tl_decline_reason, userid_compapp, date_compapp, com_decline_reason, username, last_date_modified, decline_reason, notes 
                                            from tbl_EM_VendorIDRequest where request_id = '{0}'", dgRejects.CurrentRow.Cells["request_id"].Value.ToString());

            string u_query = string.Format(@"update tbl_EM_VendorIDRequest set vmo_status = NULL, userid_compapp = NULL, date_compapp = NULL, com_decline_reason = NULL, tl_decline_reason = NULL, tl_reviewedby = NULL, tl_reviewtimestamp = NULL, username = NULL, last_date_modified = NULL, decline_reason = NULL, notes = NULL where request_id = '{0}'",
                                            dgRejects.CurrentRow.Cells["request_id"].Value.ToString());

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            SqlCommand cmd_update = new SqlCommand(u_query, conn);
            SqlCommand cmd_insert = new SqlCommand(i_query, conn);


            try
            {
                conn.Open();
                cmd_insert.ExecuteNonQuery();
                conn.Close();


                try
                {
                    conn.Open();
                    cmd_update.ExecuteNonQuery();
                    conn.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Unable to save the changes. Error: " + ex.ToString());
                    conn.Close();
                    return;
                }

            }
            catch (Exception ex)
            {
               MessageBox.Show("Unable to save the changes. Error: " + ex.ToString());
               
                conn.Close();
                return;
            }
        }


        public void currentCount()
        {
            string userName = Environment.UserName;
            lblApproved.Text = "-";
            lblPending.Text = "-";
            lblRejected.Text = "-";
            string qry_Pending = "";
            string qry_Pending_rej = "";
            string qry_Approved = "";
            string qry_Rejected = "";
            string qry_Approved_o = "";
            string qry_Rejected_o = "";
            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            SqlCommand cmd_countApproved = new SqlCommand();
            SqlCommand cmd_countRejected = new SqlCommand();
            SqlCommand cmd_countApproved_o = new SqlCommand();
            SqlCommand cmd_countRejected_o = new SqlCommand();
            SqlCommand cmd_countPending = new SqlCommand();
            SqlCommand cmd_countPending_rej = new SqlCommand();

            if (user_role == "L1" || user_role == "Admin")
            {
                qry_Approved = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where status IN ('RECON_APP') AND tl_reviewedby='{0}' and date_requested > '2016-11-16' ", userName);
                qry_Rejected = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where status='RECON_REJ' AND tl_reviewedby='{0}' and date_requested > '2016-11-16' ", userName);
                qry_Approved_o = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where status IN ('RECON_APP') and date_requested > '2016-11-16' ", userName);
                qry_Rejected_o = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where status='RECON_REJ' and date_requested > '2016-11-16' ", userName);
                qry_Pending = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where status IN ('NEW', 'OPEN_HOLD') and date_requested > '2016-11-16' ");
                qry_Pending_rej = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where ([status] = 'COMP_REJ' or vmo_status = 'REQ_INC') and date_requested > '2016-11-16' ");
            }
            else if (user_role == "COMP")
            {
                qry_Approved = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where status='COMP_APP' AND vmo_status='REQ_PEND'  AND userid_compapp='{0}' and date_requested > '2016-11-16' ", userName);
                qry_Rejected = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where status='COMP_REJ' AND vmo_status is null AND userid_compapp='{0}' and date_requested > '2016-11-16' ", userName);
                qry_Approved_o = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where status='COMP_APP' AND vmo_status='REQ_PEND' and date_requested > '2016-11-16' ", userName);
                qry_Rejected_o = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where status='COMP_REJ' AND vmo_status is null and date_requested > '2016-11-16' ", userName);
                qry_Pending = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where status='RECON_APP' AND [type] in ('CREATE NEW', 'VDR ACTIVATION') and date_requested > '2016-11-16' ");

            }
            else if (user_role == "AVS")
            {
                qry_Approved = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where vmo_status='REQ_COM' AND userid_compapp='{0}' and date_requested > '2016-11-16' ", userName);
                qry_Rejected = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where vmo_status='REQ_INC' AND userid_compapp='{0}' and date_requested > '2016-11-16' ", userName);
                qry_Approved_o = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where vmo_status='REQ_COM' and date_requested > '2016-11-16' ", userName);
                qry_Rejected_o = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where vmo_status='REQ_INC' and date_requested > '2016-11-16' ", userName);
                qry_Pending = string.Format("select COUNT(*) from tbl_EM_VendorIDRequest where vmo_status='REQ_PEND' AND [type] in ('CREATE NEW', 'VDR ACTIVATION') and date_requested > '2016-11-16' ");
            }

            conn.Open();
            cmd_countApproved = new SqlCommand(qry_Approved, conn);
            cmd_countRejected = new SqlCommand(qry_Rejected, conn);
            cmd_countApproved_o = new SqlCommand(qry_Approved_o, conn);
            cmd_countRejected_o = new SqlCommand(qry_Rejected_o, conn);
            cmd_countPending = new SqlCommand(qry_Pending, conn);
            cmd_countPending_rej = new SqlCommand(qry_Pending_rej, conn);
                        
            int app = Convert.ToInt32(cmd_countApproved.ExecuteScalar());
            int rej = Convert.ToInt32(cmd_countRejected.ExecuteScalar());

            int app_o = Convert.ToInt32(cmd_countApproved_o.ExecuteScalar());
            int rej_o = Convert.ToInt32(cmd_countRejected_o.ExecuteScalar());

            lblApproved.Text = app.ToString();
            lblRejected.Text = rej.ToString();

            if (user_role == "L1" || user_role == "Admin")
            {
                lblPending.Text = cmd_countPending.ExecuteScalar().ToString() + " : " + cmd_countPending_rej.ExecuteScalar().ToString();
            }
            else
            {
                lblPending.Text = cmd_countPending.ExecuteScalar().ToString();
            }

            lbluTotal.Text = (app + rej).ToString();

            lbloApp.Text = app_o.ToString();
            lbloRej.Text = rej_o.ToString();
            lbloTotal.Text = (app_o + rej_o).ToString();

            conn.Close();
        }

        public void declineReason()
        {
          
            //DataGridViewComboBoxCell cmb_Reason_Column = (DataGridViewComboBoxCell)dgv_VIDRequest.Columns[dgv_VIDRequest.Columns.Count].CellTemplate;
            DataGridViewComboBoxColumn cbc = new DataGridViewComboBoxColumn();
           cbc.Items.Add("Incomplete information – Email missing/Invalid");
           cbc.Items.Add("Incomplete information – Phone missing/Invalid");
           cbc.Items.Add("Incomplete information – Address missing/invalid");
           cbc.Items.Add("Vendor already created – Duplicate Address");
           cbc.Items.Add("Incompatible request – Vendor cannot be used due to Transaction issues");
           cbc.HeaderText = "Decline Reason";
            //cbc.Columns.MaxDropDownItems = 6;
           cbc.ReadOnly = false;
            
           dgv_VIDRequest.Columns.Add(cbc);            
            
        }

        private void dgv_VIDRequest_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            //var senderGrid = (DataGridView)sender;
            
            if (e.ColumnIndex == dgv_VIDRequest.Columns["btnApprove"].Index && e.RowIndex >= 0)
            {

                app_action = "app_go";

                var res = MessageBox.Show("Approve this request?",
                            "Vendor ID Request Approval", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.No)
                {

                    return;
                }

                if (user_role == "L1" || user_role == "Admin")
                {
                    updateStatus_L1("RECON_APP", 1);
                }
                else if (user_role == "COMP")
                {
                    updateStatus_Comp("COMP_APP", "REQ_PEND");
                }
                else if (user_role == "AVS")
                {

                    try
                    {
                        if (dgv_VIDRequest.CurrentRow.Cells["Vendor ID"].Value.ToString().ToLower() == "none" || dgv_VIDRequest.CurrentRow.Cells["Vendor ID"].Value.ToString().ToLower() == string.Empty || dgv_VIDRequest.CurrentRow.Cells["Vendor ID"].Value == null)
                        {
                            MessageBox.Show("Vendor ID is mandatory");

                            dgv_VIDRequest.CurrentRow.Cells["Vendor ID"].Style.BackColor = Color.Salmon;

                            return;
                        }
                    }
                    catch
                    {
                        MessageBox.Show("Vendor ID is mandatory");

                        dgv_VIDRequest.CurrentRow.Cells["Vendor ID"].Style.BackColor = Color.Salmon;

                        return;
                    }

                    updateStatus_AVS("REQ_COM");
                }
               
                GetPendingVIDRequestsbyRole();
                ShowPriority();
            }

            if (e.ColumnIndex == dgv_VIDRequest.Columns["btnReject"].Index && e.RowIndex >= 0)
            {

                app_action = "app_rej";

                if (dgv_VIDRequest.CurrentRow.Cells["cmbDeclineReason"].Value == null)
                {
                    MessageBox.Show("Reject reason is mandatory");

                    return;
                }

                if ((dgv_VIDRequest.CurrentRow.Cells["cmbDeclineReason"].Value.ToString() == "Vendor ID already exists" || dgv_VIDRequest.CurrentRow.Cells["cmbDeclineReason"].Value.ToString() == "Incorrect Vendor Details" || dgv_VIDRequest.CurrentRow.Cells["cmbDeclineReason"].Value.ToString() == "Require Documentation") && (dgv_VIDRequest.CurrentRow.Cells["Notes"].Value == null || dgv_VIDRequest.CurrentRow.Cells["Notes"].Value == string.Empty))
                {
                    MessageBox.Show("Notes is mandatory for the selected reject reason");

                    return;
                }

                var res = MessageBox.Show("Reject this request?",
                           "Vendor ID Request Approval", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.No)
                {
                    dgv_VIDRequest.CurrentRow.Cells["cmbDeclineReason"].Value = null;
                    return;
                }

                if (user_role == "L1" || user_role == "Admin")
                {
                    updateStatus_L1("RECON_REJ", 1);
                }
                else if (user_role == "COMP")
                {
                    updateStatus_Comp("COMP_REJ", "null");
                }
                else if (user_role == "AVS")
                {
                    updateStatus_AVS("REQ_INC");
                }


                tb_USPName.Text = string.Empty;
                tb_Street.Text = string.Empty;
                tb_City.Text = string.Empty;
                cmb_State.Text = string.Empty;
                tb_ZipCode.Text = string.Empty;
                tb_CoOrAtt.Text = string.Empty;
                tb_BldgName.Text = string.Empty;
                tb_OfficeName.Text = string.Empty;
                tb_Dept.Text = string.Empty;
                tb_POBox.Text = string.Empty;
                textBox5.Text = string.Empty;
                textBox5.Tag = null;
                tb_Telephone.Text = string.Empty;
                tb_Email.Text = string.Empty;
                cb_NoEmail.Checked = false;

                tb_USPName.ReadOnly = false;
                tb_Street.ReadOnly = false;
                tb_City.ReadOnly = false;
                cmb_State.Enabled = true;
                tb_ZipCode.ReadOnly = false;
                tb_CoOrAtt.ReadOnly = false;
                tb_BldgName.ReadOnly = false;
                tb_OfficeName.ReadOnly = false;
                tb_Dept.ReadOnly = false;
                tb_POBox.ReadOnly = false;
                textBox5.ReadOnly = false;
                textBox5.ReadOnly = false;
                tb_Email.ReadOnly = false;

                comboBox1.Text = "";
                MessageBox.Show("Request has been rejected!");

                GetPendingVIDRequestsbyRole();
                ShowPriority();
            }


            if (user_role != "")
            {
                if (e.ColumnIndex == dgv_VIDRequest.Columns["Link"].Index && e.RowIndex >= 0)
                {
                    if (dgv_VIDRequest.CurrentCell.Value.ToString() != null && dgv_VIDRequest.CurrentCell.Value.ToString() != string.Empty)
                    {
                        Process.Start(dgv_VIDRequest.CurrentCell.Value.ToString());
                    }
                }
            }

            //if (e.ColumnIndex == dgv_VIDRequest.Columns["cmbDeclineReason"].Index &&
            //    e.RowIndex >= 0)
            //{
            //    DataGridViewComboBoxColumn cmb = new DataGridViewComboBoxColumn();

            //    cmb = (DataGridViewComboBoxColumn)this.dgv_VIDRequest.Columns["cmbDeclineReason"];

            //    cmb.Items.Add("Vendor already created – Duplicate Address");
            //    cmb.Items.Add("Incompatible request – Vendor cannot be used due to Transaction issues");

            //}
            //dgv_VIDRequest.Columns["priority"].ReadOnly = false;
            //if (e.ColumnIndex == dgv_VIDRequest.Columns["priority"].Index &&
            //    e.RowIndex >= 0)
            //{
            //    string prioValue = dgv_VIDRequest.CurrentRow.Cells["priority"].Value.ToString();

            //    if (prioValue.ToLower() == "false")
            //    {
            //        dgv_VIDRequest.CurrentRow.Cells["priority"].Value = "true";
            //    }
            //    else
            //    {
            //        dgv_VIDRequest.CurrentRow.Cells["priority"].Value = "false";
            //    }

            //}
            

        }


        private void dgv_VIDRequest_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (mode == 1)
            {
                if (dgv_VIDRequest.RowCount > 0 && dgv_VIDRequest.CurrentCell.RowIndex > -1)
                {

                    app_action = "update_req";

                    GetVIDRDtls(dgv_VIDRequest.CurrentRow.Cells["request_id"].Value.ToString());

                    EnableSubmitButton(null, null);
                }
            }
            
        }


        private void cmbSource_SelectedIndexChanged(object sender, EventArgs e)
        {

            EnableSubmitButton(null, null);

            if (cmbSource.Text == "Research/Internet")
            {
                cb_NoEmail.Enabled = false;
            }

            if (cmbSource.Text == "Invoice" && req_from != "EMS")
            {
                tb_SourceOfInfo.Visible = true;
                tb_SourceOfInfo.ReadOnly = false;
            }
        }

        private void chkBox_Electricity_CheckedChanged(object sender, EventArgs e)
        {
            EnableSubmitButton(null, null);
        }

        private void chkBox_Gas_CheckedChanged(object sender, EventArgs e)
        {
            EnableSubmitButton(null, null);
        }

        private void chkBox_Water_CheckedChanged(object sender, EventArgs e)
        {
            EnableSubmitButton(null, null);
        }

        private void chkBox_Payments_CheckedChanged(object sender, EventArgs e)
        {
            EnableSubmitButton(null, null);
        }

        private void chkBox_Others_CheckedChanged(object sender, EventArgs e)
        {
            EnableSubmitButton(null, null);
        }

        private void dgv_VIDRequest_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            e.Control.KeyPress -= new KeyPressEventHandler(VID_KeyPress);

            if (dgv_VIDRequest.IsCurrentCellDirty)
            {
                dgv_VIDRequest.CommitEdit(DataGridViewDataErrorContexts.Commit);
            }

            if (dgv_VIDRequest.CurrentCell.ColumnIndex == 5) //Desired Column
            {
                TextBox tb = e.Control as TextBox;
                if (tb != null)
                {
                    tb.KeyPress += new KeyPressEventHandler(VID_KeyPress);
                }
            }
        }

        private void VID_KeyPress(object sender, KeyPressEventArgs e)
        {
            // allowed only numeric value  ex.10
            //if (!char.IsControl(e.KeyChar)
            //    && !char.IsDigit(e.KeyChar))
            //{
            //    e.Handled = true;
            //}

            // allowed numeric and one dot  ex. 10.23
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            else
            {
                dgv_VIDRequest.CurrentRow.Cells["Vendor ID"].Style.BackColor = Color.White;
            }
        }

        private void dgv_VIDRequest_Sorted(object sender, EventArgs e)
        {
            if (dgv_VIDRequest.Rows.Count > 0)
            {
                ShowPriority();
            }
        }

        private void btnShowAll_Click(object sender, EventArgs e)
        {
            populateUSP();

            if (user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS")
            {
                GetPendingVIDRequestsbyRole();
                ShowPriority();
            }
            else
            {
                FindVIDRequests();
            }
        }

        private void dgv_VIDRequest_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            if (user_role == "L1" || user_role == "Admin" || user_role == "COMP" || user_role == "AVS")
            {
                foreach (DataGridViewRow r in dgv_VIDRequest.Rows)
                {
                    r.Cells["Link"] = new DataGridViewLinkCell();
                    r.Cells["Link"].ReadOnly = false;
                }

                ShowPriority();
            }
        }


        private void dgRejects_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {

            foreach (DataGridViewRow r in dgRejects.Rows)
            {
                r.Cells["Link"] = new DataGridViewLinkCell();
                r.Cells["Link"].ReadOnly = false;
            }

            ShowPriority();
        }

        private void dgRejects_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgRejects.Columns["btnApprove_rej"].Index && e.RowIndex >= 0)
            {
                app_action = "app_go";

                var res = MessageBox.Show("Approve this request?", "Vendor ID Request Approval", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.No)
                {
                    return;
                }

                updateStatus_L1("RECON_APP", 0);                
                GetPendingVIDRequestsbyRole();
                ShowPriority();
            }

            if (e.ColumnIndex == dgRejects.Columns["btnReject_rej"].Index && e.RowIndex >= 0)
            {

                app_action = "app_rej";

                if (dgRejects.CurrentRow.Cells["cmbDeclineReason_rej"].Value == null)
                {
                    MessageBox.Show("Reject reason is mandatory");

                    return;
                }

                if ((dgRejects.CurrentRow.Cells["cmbDeclineReason_rej"].Value.ToString() == "Vendor ID already exists" || dgRejects.CurrentRow.Cells["cmbDeclineReason_rej"].Value.ToString() == "Incorrect Vendor Details" || dgRejects.CurrentRow.Cells["cmbDeclineReason_rej"].Value.ToString() == "Require Documentation") && (dgRejects.CurrentRow.Cells["Notes"].Value == null || dgRejects.CurrentRow.Cells["Notes"].Value == string.Empty))
                {
                    MessageBox.Show("Notes is mandatory for the selected reject reason");

                    return;
                }

                var res = MessageBox.Show("Reject this request?", "Vendor ID Request Approval", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (res == DialogResult.No)
                {
                    dgRejects.CurrentRow.Cells["cmbDeclineReason_rej"].Value = null;
                    return;
                }

                updateStatus_L1("RECON_REJ", 0);
                
                tb_USPName.Text = string.Empty;
                tb_Street.Text = string.Empty;
                tb_City.Text = string.Empty;
                cmb_State.Text = string.Empty;
                tb_ZipCode.Text = string.Empty;
                tb_CoOrAtt.Text = string.Empty;
                tb_BldgName.Text = string.Empty;
                tb_OfficeName.Text = string.Empty;
                tb_Dept.Text = string.Empty;
                tb_POBox.Text = string.Empty;
                textBox5.Text = string.Empty;
                textBox5.Tag = null;
                tb_Telephone.Text = string.Empty;
                tb_Email.Text = string.Empty;
                cb_NoEmail.Checked = false;

                tb_USPName.ReadOnly = false;
                tb_Street.ReadOnly = false;
                tb_City.ReadOnly = false;
                cmb_State.Enabled = true;
                tb_ZipCode.ReadOnly = false;
                tb_CoOrAtt.ReadOnly = false;
                tb_BldgName.ReadOnly = false;
                tb_OfficeName.ReadOnly = false;
                tb_Dept.ReadOnly = false;
                tb_POBox.ReadOnly = false;
                textBox5.ReadOnly = false;
                textBox5.ReadOnly = false;
                tb_Email.ReadOnly = false;

                comboBox1.Text = "";
                //MessageBox.Show("Request has been rejected!");

                GetPendingVIDRequestsbyRole();
                ShowPriority();
            }
            

            if (e.ColumnIndex == dgRejects.Columns["Link"].Index && e.RowIndex >= 0)
            {
                if (dgRejects.CurrentCell.Value.ToString() != null && dgRejects.CurrentCell.Value.ToString() != string.Empty)
                {
                    Process.Start(dgRejects.CurrentCell.Value.ToString());
                }
            }
        }

        private void dgRejects_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (dgRejects.RowCount > 0 && dgRejects.CurrentCell.RowIndex > -1)
            {

                app_action = "update_req";

                GetVIDRDtls(dgRejects.CurrentRow.Cells["request_id"].Value.ToString());

                EnableSubmitButton(null, null);
            }
        }

        private void btnShow_Click(object sender, EventArgs e)
        {
            string query = string.Format(@"select 
                                            request_id [Request ID], case when isnull(vendor_account_created, '') = '' then vendor_account_requested else vendor_account_created end [Vendor ID], [type] [Request Type], name [USP Name],
                                            ltrim(rtrim(isnull(c_o, '') + ' ' + isnull(Bldg, '') + ' ' + isnull(Office, '') + ' ' + isnull(usp_dept, '') + ' ' + isnull(POBox, '') + ' ' + isnull(street_name, ''))) [Street],
                                            city [City], [state] [State], postal_code [Zipcode], telephone [Business Phone], userid_requestor [Requested By], date_requested [Date Requested],
                                            CASE WHEN [Status] = 'NEW' THEN 'Pending' WHEN [status] in ('RECON_APP', 'COMP_APP', 'COMP_REJ') THEN 'Approved' WHEN [Status] IN ('RECON_REJ', 'DECLINED') THEN 'Rejected' END [1st Level Review Status], tl_decline_reason [1st Level Review Decline Reason], tl_reviewedby [1st Level Reviewed by], tl_reviewtimestamp [1st Level Review Timestamp],  
                                            CASE WHEN [status] in ('RECON_APP') then 'Pending' when [status] = 'COMP_APP' then 'Approved' when [status] = 'COMP_REJ' then 'Rejected' else NULL end [Compliance Status], com_decline_reason [Compliance Decline Reason], userid_compapp [Compliance Reviewed by], date_compapp [Compliance Review Timestamp], 
                                            CASE WHEN ([status] in ('RECON_REJ', 'COMP_REJ') or isnull(vmo_status, '') = '') then NULL WHEN isnull(vmo_status, '') in ('REQ_PEND') then 'Pending' when vmo_status = 'REQ_COM' then 'Approved' when vmo_status = 'REQ_INC' then 'Rejected' end [AVS Status], decline_reason [AVS Decline Reason], username [AVS Reviewed by], last_date_modified [AVS Review Timestamp], 
                                            [Notes], source_of_information [Source of Information]                                   
                                            from tbl_EM_VendorIDRequest
                                            where date_requested > '2016-11-16'
                                            and [type] in ('CREATE NEW', 'VDR ACTIVATION', 'VMS ACTIVATION') and [status] <> 'UPDATE INFORMATION' and ");

            if (cmbApprover.Text == "All")
            {
                query += string.Format("(date_requested between '{0}' and '{1}' or tl_reviewtimestamp between '{0}' and '{1}' or date_compapp between '{0}' and '{1}' or last_date_modified between '{0}' and '{1}') and ", dtpFrom.Value.Date, dtpTo.Value.Date);
            }
            else if (cmbApprover.Text == "TL" && cmbStatus.Text != "Pending")
            {
                query += string.Format("(tl_reviewtimestamp between '{0}' and '{1}') and ", dtpFrom.Value.Date, dtpTo.Value.Date);
            }
            else if (cmbApprover.Text == "Compliance" && cmbStatus.Text != "Pending")
            {
                query += string.Format("(date_compapp between '{0}' and '{1}') and ", dtpFrom.Value.Date, dtpTo.Value.Date);
            }
            else if (cmbApprover.Text == "AVS" && cmbStatus.Text != "Pending")
            {
                query += string.Format("(last_date_modified between '{0}' and '{1}') and ", dtpFrom.Value.Date, dtpTo.Value.Date);
            }


            if (cmbStatus.Text == "Pending" && cmbApprover.Text == "All")
            {
                query += string.Format("([status] in ('NEW', 'OPEN', 'RECON_APP') or vmo_status = 'REQ_PEND') and ");
            }
            else if (cmbStatus.Text == "Pending" && cmbApprover.Text == "TL")
            {
                query += string.Format("[status] in ('NEW', 'OPEN') and ");
            }
            else if (cmbStatus.Text == "Pending" && cmbApprover.Text == "Compliance")
            {
                query += string.Format("[status] = 'RECON_APP' and [type] <> 'VMS ACTIVATION' and ");
            }
            else if (cmbStatus.Text == "Pending" && cmbApprover.Text == "AVS")
            {
                query += string.Format("([status] = 'COMP_APP') and vmo_status = 'REQ_PEND' and [type] <> 'VMS ACTIVATION' and ");
            }


            if (cmbStatus.Text == "Approved" && cmbApprover.Text == "All")
            {
                query += string.Format("([status] in ('RECON_APP', 'COMP_APP') or vmo_status = 'REQ_COM') and ");
            }
            else if (cmbStatus.Text == "Approved" && cmbApprover.Text == "TL")
            {
                query += string.Format("[status] in ('RECON_APP') and ");
            }
            else if (cmbStatus.Text == "Approved" && cmbApprover.Text == "Compliance")
            {
                query += string.Format("[status] = 'COMP_APP' and [type] <> 'VMS ACTIVATION' and ");
            }
            else if (cmbStatus.Text == "Approved" && cmbApprover.Text == "AVS")
            {
                query += string.Format("(vmo_status = 'REQ_COM' and [type] <> 'VMS ACTIVATION' and ");
            }


            if (cmbStatus.Text == "Rejected" && cmbApprover.Text == "All")
            {
                query += string.Format("([status] in ('RECON_REJ', 'COMP_REJ') or vmo_status = 'REQ_INC') and ");
            }
            else if (cmbStatus.Text == "Rejected" && cmbApprover.Text == "TL")
            {
                query += string.Format("[status] in ('RECON_REJ') and ");
            }
            else if (cmbStatus.Text == "Rejected" && cmbApprover.Text == "Compliance")
            {
                query += string.Format("[status] = 'COMP_REJ' and [type] <> 'VMS ACTIVATION' and ");
            }
            else if (cmbStatus.Text == "Rejected" && cmbApprover.Text == "AVS")
            {
                query += string.Format("(vmo_status = 'REQ_INQ' and [type] <> 'VMS ACTIVATION' and ");
            }

            if (cmbType.Text != "All" && cmbType.Text != string.Empty)
            {
                query += string.Format(@"[type] = '{0}' and ", cmbType.Text);
            }

            query = query.ToString().Substring(0, query.Length - 4) + "order by date_requested asc ";

            SqlConnection conn = new SqlConnection(@"Data Source=PIV8DBMISNP01;Database=MIS_ALTI;uid=mis414;pwd=juchUt4a;Connection Timeout=300;");
            DataTable dt = new DataTable();
            SqlDataAdapter da = new SqlDataAdapter(query, conn);

            dgv_VIDRequest.DataSource = null;
            conn.Open();
            da.SelectCommand.CommandTimeout = 1800;
            da.Fill(dt);

            conn.Close();

            dgDashboard.DataSource = dt;
        }

        private void chkAll_CheckStateChanged(object sender, EventArgs e)
        {
            if (chkAll.Checked)
            {
                dtpFrom.Value = DateTime.Parse("2016-11-16");
                dtpTo.Value = TimeZoneInfo.ConvertTime(DateTime.Now, TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time"));
            }
        }

        private void btnMain_Click(object sender, EventArgs e)
        {
            grpReports.Visible = false;
            dgDashboard.Visible = false;
            groupBox1.Visible = true;

            grpSummary.Visible = true;
            grpSummary2.Visible = true;


            label16.Visible = true;
            label17.Visible = true;
            dgv_VDR.Visible = true;
            label12.Visible = true;
            label27.Visible = true;
            tbMain.Visible = true;
        }

        private void btnReports_Click(object sender, EventArgs e)
        {
            grpReports.Visible = true;
            dgDashboard.Visible = true;
            groupBox1.Visible = false;

            grpSummary.Visible = false;
            grpSummary2.Visible = false;


            label16.Visible = false;
            label17.Visible = false;
            dgv_VDR.Visible = false;
            label12.Visible = false;
            label27.Visible = false;
            tbMain.Visible = false;

            cmbApprover.SelectedIndex = 0;
            cmbStatus.SelectedIndex = 0;
            cmbType.SelectedIndex = 0;
        }

        private void btnExport_Click(object sender, EventArgs e)
        {
            string filename = System.IO.Path.Combine(System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location), string.Format(@"Vendor ID Request Report {0}.csv", System.DateTime.Now.ToString("yyyyMMddhhmmss")));
            FileStream fs = new FileStream(filename, FileMode.OpenOrCreate, FileAccess.ReadWrite);
            StreamWriter wr = new StreamWriter(fs);
            string hdr = string.Empty;
            string data = string.Empty;


            for (int z = 0; z <= dgDashboard.Columns.Count - 1; z++)
            {
                hdr += dgDashboard.Columns[z].Name + ",";
            }

            wr.WriteLine(hdr);

            for (int i = 0; i <= dgDashboard.Rows.Count - 1; i++)
            {
                for (int x = 0; x <= dgDashboard.Columns.Count - 1; x++)
                {                                        
                    data += dgDashboard.Rows[i].Cells[x].Value.ToString().Replace(",", "") + ",";
                }

                wr.WriteLine(data);
                data = string.Empty;
            }
            
            wr.Flush();

            fs.Close();

            if (File.Exists(filename))
            {
                label47.Visible = true;
                lnkFile.Visible = true;
                lnkFile.Text = filename;
            }
            else
            {
                label47.Visible = true;
                lnkFile.Visible = true;
                lnkFile.Text = "-";
            }
        }

        private void lnkFile_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start(lnkFile.Text);
                this.lnkFile.LinkVisited = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Could not open link.");
            }

        }
    }
}
