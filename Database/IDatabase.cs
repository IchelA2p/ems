﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database
{
    public interface IDatabase<D>
    {
        List<D> GetAllData();
        uint WriteData(D data);
        void UpdateData(D data);
    }
}
