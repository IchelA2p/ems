﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseEmployee_Relation : DatabaseConnector, IDatabase<DB.Employee_Relation>
    {
        public List<DB.Employee_Relation> GetAllData()
        {
            DatabaseSelector selector = new SelectorEmployeeRelation();
            selector.SetQuery(@"SELECT * FROM SELECT * FROM MIS_Alti.dbo.tbl_PAY_Employeerelation");

            base.ExecuteSelect(selector);

            return ((SelectorEmployeeRelation)selector).GetResults();
        }

        public uint WriteData(DB.Employee_Relation data)
        {
            string query = string.Format(@"INSERT INTO SELECT * FROM MIS_Alti.dbo.tbl_PAY_Employeerelation(
                                                UpperRank_EmployeePrimaryId,
                                                LowerRank_EmployeePrimaryId)
                                            VALUES({0},{1})",
                                                  data.UpperRank_EmployeePrimaryId,
                                                  data.LowerRank_EmployeePrimaryId);

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.Employee_Relation data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_PAY_Employeerelation
                                           SET
                                               LowerRank_EmployeePrimaryId = {0}
                                           WHERE
                                               UpperRank_EmployeePrimaryId = {1}",

                                            data.LowerRank_EmployeePrimaryId,
                                            data.UpperRank_EmployeePrimaryId);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.Employee_Relation GetData(uint LowerRank_EmployeePrimaryId)
        {
            DatabaseSelector selector = new SelectorEmployeeRelation();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Employeerelation WHERE LowerRank_EmployeePrimaryId={0}", LowerRank_EmployeePrimaryId));
            
            base.ExecuteSelect(selector);

            return ((SelectorEmployeeRelation)selector).GetResult();
        }

        public List<DB.Employee_Relation> GetDataProcessors(int UpperRank_EmployeePrimaryId)
        {
            DatabaseSelector selector = new SelectorEmployeeRelation();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Employeerelation WHERE UpperRank_EmployeePrimaryId = {0}", UpperRank_EmployeePrimaryId));
            base.ExecuteSelect(selector);

            return ((SelectorEmployeeRelation)selector).GetResults();
        }


    }
}
