﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseSchedule_Data : DatabaseConnector, IDatabase<DB.Schedule_Data>
    {
        public List<DB.Schedule_Data> GetAllData()
        {
            DatabaseSelector selector = new SelectorSchedule();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Scheduleofemployee");

            base.ExecuteSelect(selector);

            return ((SelectorSchedule)selector).GetResults();
        }

        public uint WriteData(DB.Schedule_Data data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_PAY_Scheduleofemployee(
                                                UserId,
                                                SchedIn,
                                                SchedOut,
                                                Date,
                                                Break1start,
                                                Break1end,
                                                lunchstart,
                                                lunchend,
                                                Break2start,
                                                Break2end,
                                                PTO,
                                                RestDay,
                                                Absent)
                                            VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},{11},{12})",
                                                  data.userId, //0
                                                  data.SchedIn.ToString("yyyy-MM-dd HH:mm:ss"), //1
                                                  data.SchedOut.ToString("yyyy-MM-dd HH:mm:ss"), //2
                                                  data.Date.ToString("yyyy-MM-dd HH:mm:ss"), //3
                                                  data.Break1Start.ToString("yyyy-MM-dd HH:mm:ss"), //4
                                                  data.Break1End.ToString("yyyy-MM-dd HH:mm:ss"), //5
                                                  data.LunchStart.ToString("yyyy-MM-dd HH:mm:ss"), //6
                                                  data.LunchEnd.ToString("yyyy-MM-dd HH:mm:ss"), //7
                                                  data.Break2Start.ToString("yyyy-MM-dd HH:mm:ss"), //8
                                                  data.Break2End.ToString("yyyy-MM-dd HH:mm:ss"), //9
                                                  data.PTO == true ? 1 : 0, //10
                                                  data.RestDay == true ? 1 : 0, //11
                                                  data.Absent == true ? 1 : 0); //12

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.Schedule_Data data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_PAY_Scheduleofemployee
                                           SET
                                                UserId = '{0}',
                                                SchedIn '{1}',
                                                SchedOut '{2}',
                                                Date '{3}',
                                                Break1start = '{4}',
                                                Break1end = '{5}',
                                                lunchstart = '{6}',
                                                lunchend = '{7}',
                                                Break2start = '{8}',
                                                Break2end = '{9}',
                                                PTO = {10},
                                                RestDay = {11},
                                                Absent = {12},
                                           WHERE
                                               Scheduleofemployeeid = {13}",

                                            data.userId, //0
                                            data.SchedIn.ToString("yyyy-MM-dd HH:mm:ss"), //1
                                            data.SchedOut.ToString("yyyy-MM-dd HH:mm:ss"), //2
                                            data.Date.ToString("yyyy-MM-dd HH:mm:ss"), //3
                                            data.Break1Start.ToString("yyyy-MM-dd HH:mm:ss"), //4
                                            data.Break1End.ToString("yyyy-MM-dd HH:mm:ss"), //5
                                            data.LunchStart.ToString("yyyy-MM-dd HH:mm:ss"), //6
                                            data.LunchEnd.ToString("yyyy-MM-dd HH:mm:ss"), //7
                                            data.Break2Start.ToString("yyyy-MM-dd HH:mm:ss"), //8
                                            data.Break2End.ToString("yyyy-MM-dd HH:mm:ss"), //9
                                            data.PTO == true ? 1 : 0, //10
                                            data.RestDay == true ? 1 : 0, //11
                                            data.Absent == true ? 1 : 0, //12      

                                            data.ScheduleOfEmployeeId); //13

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.Schedule_Data GetData(uint ScheduleOfEmployeeId)
        {
            DatabaseSelector selector = new SelectorSchedule();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Scheduleofemployee WHERE Scheduleofemployeeid={0}", ScheduleOfEmployeeId));
            
            base.ExecuteSelect(selector);

            return ((SelectorSchedule)selector).GetResult();
        }

        public List<DB.Schedule_Data> GetDataProcessor(string userId)
        {
            DatabaseSelector selector = new SelectorSchedule();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Scheduleofemployee WHERE userId='{0}'", userId));

            base.ExecuteSelect(selector);

            return ((SelectorSchedule)selector).GetResults();
        }
    }
}
