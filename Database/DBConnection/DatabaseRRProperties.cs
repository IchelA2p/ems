﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseRRProperties : DatabaseConnector, IDatabase<DB.RRProperties>
    {
        public List<DB.RRProperties> GetAllData()
        {
            DatabaseSelector selector = new SelectorRRProperties();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_RRProperties]");

            base.ExecuteSelect(selector);

            return ((SelectorRRProperties)selector).GetResults();
        }

        public uint WriteData(DB.RRProperties data)
        {
            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_RRProperties](
                                                city,
                                                property_id,
                                                status,
                                                status_date,
                                                street_name,
                                                street_number,
                                                zip)
                                            VALUES('{0}','{1}','{2}',{3},'{4}','{5}','{6}')",
                                                  data.city, //0
                                                  data.property_id, //1
                                                  data.status,  //2
                                                  GetString(data.status_date),  //3
                                                  data.street_name, //4
                                                  data.street_number,   //5
                                                  data.zip);    //6

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.RRProperties data)
        {
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_EM_RRProperties]
                                           SET
                                                city = '{1}',
                                                status = '{2}',
                                                status_date = {3},
                                                street_name = '{4}',
                                                street_number = '{5}',
                                                zip = '{6}'
                                           WHERE
                                                property_id = '{0}'",

                                            data.property_id,   //0
                                            data.city,  //1
                                            data.status,    //2
                                            GetString(data.status_date),    //3
                                            data.street_name,   //4
                                            data.street_number, //5
                                            data.zip);  //6

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.RRProperties GetData(uint property_id)
        {
            DatabaseSelector selector = new SelectorRRProperties();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_RRProperties] WHERE property_id={0}", property_id));
            
            base.ExecuteSelect(selector);

            return ((SelectorRRProperties)selector).GetResult();
        }

        public List<DB.RRProperties> GetDataProcessor(string property_id)
        {
            DatabaseSelector selector = new SelectorRRProperties();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_RRProperties] WHERE property_id = '{0}'", property_id));

            base.ExecuteSelect(selector);

            return ((SelectorRRProperties)selector).GetResults();
        }

        public List<DB.RRProperties> SearchProperty(string street_number, string street_name, string city, string zip)
        {
            DatabaseSelector selector = new SelectorRRProperties();
            string query = String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_RRProperties] 
                                              WHERE 
                                              street_number Like '{0}%' AND
                                              street_name Like '%{1}%' AND
                                              city Like '%{2}%' AND
                                              zip = '{3}'",
                                              street_number,
                                              street_name,
                                              city,
                                              zip);
            selector.SetQuery(DBHelper.toSingleSpace(query));

            base.ExecuteSelect(selector);

            return ((SelectorRRProperties)selector).GetResults();
        }
    }
}
