﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseZipcodeStateCity : DatabaseConnector, IDatabase<DB.ZipcodeStateCity>
    {
        public List<DB.ZipcodeStateCity> GetAllData()
        {
            DatabaseSelector selector = new SelectorZipcodeStateCity();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_ZipcodeStateCity]");

            base.ExecuteSelect(selector);

            return ((SelectorZipcodeStateCity)selector).GetResults();
        }

        public uint WriteData(DB.ZipcodeStateCity data)
        {
            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_ZipcodeStateCity](
                                                city_name,
                                                state,
                                                zip_code,
                                                abbrev_state)
                                            VALUES('{0}','{1}','{2}','{3}')",
                                                  data.city_name,   //0
                                                  data.state,       //1
                                                  data.zip_code,    //2
                                                  data.abbrev_state);   //3

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.ZipcodeStateCity data)
        {
            /*
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_EM_ZipcodeStateCity]
                                           SET
                                                city_name = '{1}',
                                                state = '{2}',
                                                zip_code = {3}
                                           WHERE
                                                id = {0}",

                                            data.id,
                                            data.city_name,
                                            data.state,
                                            data.zip_code); //1

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);*/
        }

        public DB.ZipcodeStateCity GetData(uint id)
        {
            DatabaseSelector selector = new SelectorZipcodeStateCity();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_ZipcodeStateCity] WHERE id={0}", id));
            
            base.ExecuteSelect(selector);

            return ((SelectorZipcodeStateCity)selector).GetResult();
        }

        public List<DB.ZipcodeStateCity> GetCityStates(string zip_code)
        {
            DatabaseSelector selector = new SelectorZipcodeStateCity();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_ZipcodeStateCity] WHERE zip_code='{0}'", zip_code));

            base.ExecuteSelect(selector);

            return ((SelectorZipcodeStateCity)selector).GetResults();
        }
    }
}
