﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseVDR_Data : DatabaseConnector, IDatabase<DB.VDR>
    {
        public List<DB.VDR> GetAllData()
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.vw_VID where vdr_status < 2 and vms_status < 2 ORDER BY Vendor_Name");

            base.ExecuteSelect(selector);

            return ((SelectorVDR)selector).GetResults();
        }

        public uint WriteData(DB.VDR data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_EM_VDR(
                                                Address,
                                                Vendor_ID,
                                                Vendor_Name,
                                                isActive,
                                                City,
                                                State,
                                                Zipcode,
                                                Type)
                                            VALUES('{0}','{1}','{2}',{3},'{4}','{5}','{6}','{7}')",
                                                  data.Address,
                                                  data.Vendor_ID,
                                                  data.Vendor_Name,
                                                  data.vdr_status == 1 ? 1 : 0,
                                                  data.City,
                                                  data.State,
                                                  data.Zipcode,
                                                  data.Type);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
            return 0;
        }

        public void UpdateData(DB.VDR data)
        {
            // update function not available
        }

        public DB.VDR GetData(uint Vendor_ID)
        {
            DatabaseSelector selector = new SelectorVDR();
            //selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_VDR WHERE Vendor_ID = '{0}'", Vendor_ID));
            selector.SetQuery(String.Format(@"select * from vw_VID with(index(vendor_id, vendor_name)) where vendorid = '{0}'", Vendor_ID));
             
            base.ExecuteSelect(selector);

            return ((SelectorVDR)selector).GetResult();
        }

        public List<DB.VDR> GetData(string Vendor_Name)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"select * from vw_VID with(index(vendor_id, vendor_name)) WHERE Vendor_Name= '{0}'", Vendor_Name));

            base.ExecuteSelect(selector);

            return ((SelectorVDR)selector).GetResults();
        }

        public List<DB.VDR> GetDataWithAddress(string Vendor_Name, string Street, string City, string State, string ZipCode)
        {
            DatabaseSelector selector = new SelectorVDR();
            string qry = "select * from vw_VID with(index(vendor_name)) WHERE vdr_status < 2 and ";

            if (Vendor_Name != string.Empty)
            {
                qry += string.Format(@"(replace(Vendor_Name, ' ', '') like replace('{0}', ' ', '') or replace(VMS_Vendor_Name, ' ', '') like replace('{0}', ' ', '')) and ", Vendor_Name);
            }


            if (Street != string.Empty)
            {
                qry += string.Format(@"replace([Address], ' ', '') like replace('%{0}%', ' ', '') and ", Street);
            }


            if (City != string.Empty)
            {
                qry += string.Format(@"replace(city, ' ', '') like replace('%{0}%', ' ', '') and ", City);
            }

            if (State != string.Empty)
            {
                qry += string.Format(@"[state] = '{0}' and ", State);
            }

            if (ZipCode != string.Empty)
            {
                qry += string.Format(@"zipcode = '{0}' and ", ZipCode);
            }

            selector.SetQuery(String.Format(qry.ToString().Substring(0, qry.Length - 4) + "order by Vendor_Name asc, [Address] asc, vdr_status desc", Vendor_Name));

            base.ExecuteSelect(selector);

            return ((SelectorVDR)selector).GetResults();
        }

        public void TruncateTable()
        {
            string query = @"delete from [MIS_ALTI].[dbo].[tbl_EM_VDR]";

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.VDR GetDataProcessor(string Vendor_ID)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"SELECT * FROM dbo.vw_VID WHERE Vendor_ID = '{0}'", Vendor_ID));

            base.ExecuteSelect(selector);

            return ((SelectorVDR)selector).GetResult();
        }

        public DB.VDR CheckIfForActivation(string vendor_id, string vendor_name, string address, string city, string state, string zipcode)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"SELECT * from vw_VID with(index(vendor_id, vendor_name))
                                                WHERE Vendor_ID = '{0}'
                                                AND Vendor_Name = '{1}'
                                                AND Address LIKE '%{2}%'
                                                AND City = '{3}'
                                                AND State = '{4}'
                                                AND Zipcode = '{5}'
                                                AND vdr_status = 0",
                                                vendor_id,
                                                vendor_name,
                                                address,
                                                city,
                                                state,
                                                zipcode));

            base.ExecuteSelect(selector);

            return ((SelectorVDR)selector).GetResult();
        }

        public DB.VDR CheckIfUpdateInformation(string vendor_id, string vendor_name)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VDR]
                                                WHERE Vendor_ID = '{0}'
                                                AND Vendor_Name = '{1}'
                                                AND (Address = '' OR Address IS NULL)
                                                AND Type LIKE '%Inactive%'",
                                                vendor_id,
                                                vendor_name));

            base.ExecuteSelect(selector);

            return ((SelectorVDR)selector).GetResult();
        }

        public DB.VDR CheckIfForCreation(string vendor_name, string address)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"SELECT * from vw_VID with(index(vendor_id, vendor_name))
                                                WHERE Vendor_Name = '{0}' 
                                                AND Address LIKE '%{1}%'",
                                                vendor_name,
                                                address));

            base.ExecuteSelect(selector);

            return ((SelectorVDR)selector).GetResult();
        }

        public DB.VDR CheckIfForCreation1(string VendorId)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"SELECT * from vw_VID with(index(vendor_id, vendor_name)) a
                                                WHERE a.Vendor_ID = '{0}'",
                                                VendorId));
            base.ExecuteSelect(selector);
            return ((SelectorVDR)selector).GetResult();
        }

        public DB.VDR CheckIfForUpdateInfo1(string Vendor_ID, string Vendor_Name)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"SELECT * from vw_VID with(index(vendor_id, vendor_name)) a
                                                WHERE a.Vendor_ID = '{0}'
                                                AND a.Address NOT LIKE '%{1}%'",
                                                Vendor_ID,
                                                Vendor_Name));
            base.ExecuteSelect(selector);
            return ((SelectorVDR)selector).GetResult();
        }

        public DB.VDR CheckIfForUpdateInfo2(string Vendor_ID, string Vendor_Name)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"SELECT * from vw_VID with(index(vendor_id, vendor_name)) a
                                                WHERE a.Vendor_ID = '{0}'
                                                AND a.Vendor_Name = '{1}'
                                                AND a.Address = ''",
                                                Vendor_ID,
                                                Vendor_Name));
            base.ExecuteSelect(selector);
            return ((SelectorVDR)selector).GetResult();
        }

        public DB.VDR CheckIfForCreation2(string Vendor_ID, string Vendor_Name, string Vendor_Address)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"SELECT * from vw_VID with(index(vendor_id, vendor_name)) a
                                                WHERE a.vdr_status < 2 and a.vms_status < 2 AND a.Vendor_ID = '{0}'
                                                AND a.Vendor_Name = '{1}'
                                                AND a.Address <> ''
                                                AND a.Address NOT LIKE '%{2}%'",
                                                Vendor_ID,
                                                Vendor_Name,
                                                Vendor_Address));
            base.ExecuteSelect(selector);
            return ((SelectorVDR)selector).GetResult();
        }

        public DB.VDR CheckIfForActivation1(string Vendor_ID, string Vendor_Name, string Vendor_Address)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"SELECT * from vw_VID with(index(vendor_id, vendor_name)) a
                                                WHERE a.vdr_status < 2 and a.vms_status < 2 AND a.Vendor_ID = '{0}'
                                                AND a.Vendor_Name = '{1}'
                                                AND a.Address <> ''
                                                AND a.Address LIKE '%{2}%'
                                                AND a.vdr_status = 0",
                                                Vendor_ID,
                                                Vendor_Name,
                                                Vendor_Address));
            base.ExecuteSelect(selector);
            return ((SelectorVDR)selector).GetResult();
        }

        public List<DB.VDR> SearchOnVDR(string name, string houseNo, string streetName, string state)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"SELECT * FROM  MIS_Alti.dbo.vw_VID 
                                                WHERE Vendor_Name Like '%{0}%' AND
                                                Address Like '{1}%' AND
                                                Address Like '%{2}%' AND
                                                Address Like '%{3}%'",
                                                name,
                                                houseNo,
                                                streetName,
                                                state));

            base.ExecuteSelect(selector);

            return ((SelectorVDR)selector).GetResults();
        }

        public DB.VDR GetDuplicateRequest(string name, string street_name, string city, string state, string postal_code)
        {
            DatabaseSelector selector = new SelectorVDR();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest]
                                                WHERE 
                                                [name] = '{0}' AND 
                                                [street_name] = '{1}' AND 
                                                [city] = '{2}' AND 
                                                [state] = '{3}' AND 
                                                [postal_code] = '{4}'",
                                                name,
                                                street_name,
                                                city,
                                                state,
                                                postal_code));
            base.ExecuteSelect(selector);

            return ((SelectorVDR)selector).GetResult();
        }
    }
}
