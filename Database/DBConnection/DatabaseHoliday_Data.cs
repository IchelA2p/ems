﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseHoliday_Data : DatabaseConnector, IDatabase<DB.Holiday>
    {
        public List<DB.Holiday> GetAllData()
        {
            DatabaseSelector selector = new SelectorHoliday();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Holiday");

            base.ExecuteSelect(selector);

            return ((SelectorHoliday)selector).GetResults();
        }

        public uint WriteData(DB.Holiday data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_PAY_Holiday(
                                                date,
                                                regularholiday,
                                                specialholiday,
                                                rate,
                                                name)
                                            VALUES('{0}',{1},{2},{3},'{4}')",
                                                  data.date.ToString("yyyy-MM-dd HH:mm:ss"),
                                                  data.regularholiday,
                                                  data.specialholiday,
                                                  data.rate,
                                                  data.name);

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.Holiday data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_PAY_Holiday
                                           SET
                                               date = '{0}',
                                               regularholiday = {1},
                                               specialholiday = {2},
                                               rate = {3},
                                               name = {4}
                                           WHERE
                                               holiday_id = {5};",

                                            data.date.ToString("yyyy-MM-dd HH:mm:ss"),
                                            data.regularholiday == true ? 1 : 0,
                                            data.specialholiday == true ? 1 : 0,
                                            data.rate,
                                            data.name,
                                            data.holiday_id);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.Holiday GetData(uint holiday_id)
        {
            DatabaseSelector selector = new SelectorHoliday();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Holiday WHERE holiday_id={0}", holiday_id));
            
            base.ExecuteSelect(selector);

            return ((SelectorHoliday)selector).GetResult();
        }

        public DB.Holiday GetDataProcessor(DateTime holiday)
        {
            DatabaseSelector selector = new SelectorHoliday();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Holiday WHERE date = '{0}'", holiday.Date));

            base.ExecuteSelect(selector);

            return ((SelectorHoliday)selector).GetResult();
        }

    }
}
