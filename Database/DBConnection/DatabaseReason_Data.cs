﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseReason_Data : DatabaseConnector, IDatabase<DB.Reason_Data>
    {
        public List<DB.Reason_Data> GetAllData()
        {
            DatabaseSelector selector = new SelectorReasonData();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Reasondata");

            base.ExecuteSelect(selector);

            return ((SelectorReasonData)selector).GetResults();
        }

        public uint WriteData(DB.Reason_Data data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_PAY_Reasondata(
                                                Reasonid,
                                                Description,
                                                Excludeonpayment)
                                            VALUES('{0}','{1}',{2})",
                                                  data.ReasonId,
                                                  data.Description,
                                                  data.ExcludeOnPayment);

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.Reason_Data data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_PAY_Reasondata
                                           SET
                                                Description = '{0}',
                                                Excludeonpayment = {1},
                                                Reasonid = '{2}',
                                           WHERE
                                               ReasonId_Reference = {3}",

                                            data.Description,
                                            data.ExcludeOnPayment == true ? 1 : 0,
                                            data.ReasonId,
                                            data.ReasonId_Reference);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.Reason_Data GetData(uint ReasonId_Reference)
        {
            DatabaseSelector selector = new SelectorReasonData();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Reasondata WHERE ReasonId_Reference={0}", ReasonId_Reference));
            
            base.ExecuteSelect(selector);

            return ((SelectorReasonData)selector).GetResult();
        }

        public DB.Reason_Data GetDataUsingReasonId(string ReasonId)
        {
            DatabaseSelector selector = new SelectorReasonData();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Reasondata WHERE Reasonid = '{0}'", ReasonId));

            base.ExecuteSelect(selector);

            return ((SelectorReasonData)selector).GetResult();
        }

        public DB.Reason_Data GetDataUsingDescription(string Description)
        {
            DatabaseSelector selector = new SelectorReasonData();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Reasondata WHERE Description = '{0}'", Description));

            base.ExecuteSelect(selector);

            return ((SelectorReasonData)selector).GetResult();
        }

        public List<DB.Reason_Data> GetDataProcessor(string ReasonId)
        {
            DatabaseSelector selector = new SelectorReasonData();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Reasondata WHERE Reasonid='{0}'", ReasonId));

            base.ExecuteSelect(selector);

            return ((SelectorReasonData)selector).GetResults();
        }
    }
}
