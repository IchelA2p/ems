﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseRSCApproval : DatabaseConnector, IDatabase<DB.RSCApproval>
    {
        public List<DB.RSCApproval> GetAllData()
        {
            DatabaseSelector selector = new SelectorRSCApproval();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_RSCApproval] ORDER BY date_created DESC");

            base.ExecuteSelect(selector);

            return ((SelectorRSCApproval)selector).GetResults();
        }

        public uint WriteData(DB.RSCApproval data)
        {
            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_RSCApproval](
                                                created_by,
                                                date_created,
                                                date_modified,
                                                invoice,
                                                modified_by,
                                                notes,
                                                status,
                                                property_code,
                                                full_address,
                                                vendor_code,
                                                vendor_name,
                                                amount,
                                                vendor_type,
                                                property_status,
                                                invoice_link,
                                                asset_manager)
                                            VALUES('{0}',{1},{2},'{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}',{11},'{12}','{13}','{14}','{15}')",
                                                  data.created_by,  //0
                                                  GetDateString(data.date_created),    //1
                                                  GetDateString(data.date_modified),   //2
                                                  data.invoice, //3
                                                  data.modified_by, //4
                                                  data.notes,   //5
                                                  data.status,  //6
                                                  data.property_code,   //7
                                                  data.full_address,    //8
                                                  data.vendor_code, //9
                                                  data.vendor_name, //10
                                                  data.amount,  //11
                                                  data.vendor_type, //12
                                                  data.property_status, //13
                                                  data.invoice_link,    //14
                                                  data.asset_manager); //15

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.RSCApproval data)
        {
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_EM_RSCApproval]
                                           SET
                                                created_by = '{1}',
                                                date_created = {2},
                                                date_modified = {3},
                                                invoice = '{4}',
                                                modified_by = '{5}',
                                                notes = '{6}',
                                                status = '{7}',
                                                property_code = '{8}',
                                                full_address = '{9}',
                                                vendor_code = '{10}',
                                                vendor_name = '{11}',
                                                amount = {12},
                                                vendor_type = '{13}',
                                                property_status = '{14}',
                                                invoice_link = '{15}',
                                                asset_manager = '{16}'
                                           WHERE
                                                rscapproval_id = {0}",

                                            data.rscapproval_id, //0
                                            data.created_by,    //1
                                            GetDateString(data.date_created),  //2
                                            GetDateString(data.date_modified), //3
                                            data.invoice,   //4
                                            data.modified_by,   //5
                                            data.notes, //6
                                            data.status,    //7
                                            data.property_code, //8
                                            data.full_address,  //9
                                            data.vendor_code,   //10
                                            data.vendor_name,   //11
                                            data.amount,    //12
                                            data.vendor_type,   //13
                                            data.property_status,   //14
                                            data.invoice_link,  //15
                                            data.asset_manager);   //16

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.RSCApproval GetData(uint rscapproval_id)
        {
            DatabaseSelector selector = new SelectorRSCApproval();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_RSCApproval] WHERE rscapproval_id={0}", rscapproval_id));
            
            base.ExecuteSelect(selector);

            return ((SelectorRSCApproval)selector).GetResult();
        }

        public List<DB.RSCApproval> GetDataProcessor(string invoice)
        {
            DatabaseSelector selector = new SelectorRSCApproval();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_RSCApproval] WHERE invoice='{0}'", invoice));

            base.ExecuteSelect(selector);

            return ((SelectorRSCApproval)selector).GetResults();
        }

        public List<DB.RSCApproval> GetRSCbyDates(DateTime from, DateTime to, string status)
        {
            DatabaseSelector selector = new SelectorRSCApproval();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_RSCApproval] 
                                              WHERE date_created > '{0}' AND date_created < '{1}' AND status = '{2}'",
                                              from,to,status));
            base.ExecuteSelect(selector);

            return ((SelectorRSCApproval)selector).GetResults();
        }

        public List<DB.RSCApproval> GetRSCbyStatus(string status, string asset_manager)
        {
            DatabaseSelector selector = new SelectorRSCApproval();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_RSCApproval] 
                                              WHERE status = '{0}' AND asset_manager = '{1}' ORDER BY date_created",
                                              status, asset_manager));
            base.ExecuteSelect(selector);

            return ((SelectorRSCApproval)selector).GetResults();
        }

        private string GetDateString(DateTime dt)
        {
            if (dt == DateTime.MinValue)
            {
                return "NULL";
            }
            else
            {
                return string.Format("'{0}'", dt.ToString("yyyy-MM-dd HH:mm:ss"));
            }
        }
    }
}
