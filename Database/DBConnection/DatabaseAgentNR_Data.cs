﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseAgentNR_Data : DatabaseConnector, IDatabase<DB.AgentNRData>
    {
        public List<DB.AgentNRData> GetAllData()
        {
            DatabaseSelector selector = new SelectorAgentNR();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Notready");

            base.ExecuteSelect(selector);

            return ((SelectorAgentNR)selector).GetResults();
        }

        public uint WriteData(DB.AgentNRData data)
        {
            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_PAY_Notready(
                                                userid,
                                                NotReadyEnddt,
                                                NotReadyStartdt,
                                                reasonid,
                                                validationid)
                                            VALUES('{0}','{1}','{2}','{3}',{4})",
                                                  data.userId,
                                                  data.NotReadyEnddt.ToString("yyyy-MM-dd HH:mm:ss"),
                                                  data.NotReadyStartdt.ToString("yyyy-MM-dd HH:mm:ss"),
                                                  data.ReasonId,
                                                  data.ValidationId);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
            return 0;
        }

        public void UpdateData(DB.AgentNRData data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_PAY_Notready
                                           SET
                                               userid = '{0}',
                                               NotReadyStartdt = '{1}',
                                               NotReadyEnddt = '{2}',
                                               reasonid = '{3}',
                                               validationid = {4}
                                           WHERE
                                               agentnrid = {5};",

                                            data.userId,
                                            data.NotReadyStartdt.ToString("yyyy-MM-dd HH:mm:ss"),
                                            data.NotReadyEnddt.ToString("yyyy-MM-dd HH:mm:ss"),
                                            data.ReasonId,
                                            //data.workSched.ToString("yyyy-MM-dd HH:mm:ss"),
                                            data.ValidationId,
                                            data.AgentNRId);

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.AgentNRData GetData(uint AgentNRId)
        {
            DatabaseSelector selector = new SelectorAgentNR();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Notready WHERE agentnrid={0}", AgentNRId));
            
            base.ExecuteSelect(selector);

            return ((SelectorAgentNR)selector).GetResult();
        }

        public List<DB.AgentNRData> GetDataProcessor(string userId)
        {
            DatabaseSelector selector = new SelectorAgentNR();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_PAY_Notready WHERE userid = '{0}'", userId));

            base.ExecuteSelect(selector);

            return ((SelectorAgentNR)selector).GetResults();
        }

        public List<DB.AgentNRData> GetDataBetweenDates(DateTime startdate, DateTime enddate, string userid)
        {
            DatabaseSelector selector = new SelectorAgentNR();
            selector.SetQuery(String.Format(@"declare @start_date datetime, @end_date datetime

                                                set @start_date = '{0}'
                                                set @end_date = '{1}'

                                                select * from tbl_PAY_Notready a
                                                where a.NotReadyStartdt between @start_date and @end_date
                                                and a.NotReadyEnddt between @start_date and @end_date
                                                and a.userid = '{2}'",
                                                startdate, enddate, userid));

            base.ExecuteSelect(selector);

            return ((SelectorAgentNR)selector).GetResults();
        }

        public void DeleteNotReadyData(uint AgentNRId)
        {
            DatabaseSelector selector = new SelectorAgentNR();
            selector.SetQuery(String.Format(@"DELETE FROM MIS_Alti.dbo.tbl_PAY_Notready WHERE agentnrid={0}", AgentNRId));

            base.ExecuteSelect(selector);
        }
    }
}
