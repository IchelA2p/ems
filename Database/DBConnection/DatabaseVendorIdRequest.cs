﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseVendorIdRequest : DatabaseConnector, IDatabase<DB.VendorIdRequest>
    {
        public List<DB.VendorIdRequest> GetAllData()
        {
            DatabaseSelector selector = new SelectorVendorIdRequest();
            selector.SetQuery(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest] (nolock)");

            base.ExecuteSelect(selector);

            return ((SelectorVendorIdRequest)selector).GetResults();
        }

        public uint WriteData(DB.VendorIdRequest data)
        {
            string query = string.Format(@"INSERT INTO [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest](
                                                city,
                                                date_completed,
                                                date_requested,
                                                email,
                                                invoice_name,
                                                name,
                                                postal_code,
                                                status,
                                                state,
                                                street_name,
                                                telephone,
                                                vendor_account_created,
                                                vendor_account_requested,
                                                notes,
                                                type,
                                                username,
                                                last_date_modified,
                                                vmo_status,
                                                userid_requestor,
                                                userid_compapp,
                                                userid_reqcom,
                                                date_compapp,
                                                decline_reason,
                                                dept,
                                                com_decline_reason,
                                                c_o,
                                                Bldg,
                                                Office,
                                                usp_dept,
                                                POBox)
                                            VALUES('{0}',{1},CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,GETUTCDATE()),'+08:00')),
                                                    '{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}','{10}','{11}','{12}','{13}','{14}',
                                                    CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,GETUTCDATE()),'+08:00')),
                                                    '{15}','{16}','{17}','{18}',{19},'{20}','{21}','{22}','{23}','{24}','{25}','{26}','{27}')",
                                                  data.city, //0
                                                  GetDate(data.date_completed), //1
                                                  data.email,   //2
                                                  data.invoice_name, //3
                                                  data.name, //4
                                                  data.postal_code, //5
                                                  data.status, //6
                                                  data.state, //7
                                                  data.street_name, //8
                                                  data.telephone, //9
                                                  data.vendor_account_created, //10
                                                  data.vendor_account_requested, //11
                                                  data.notes,   //12
                                                  data.type,    //13
                                                  data.username,    //14
                                                  data.vmo_status,  //15
                                                  data.userid_requestor,    //16
                                                  data.userid_compapp,  //17
                                                  data.userid_reqcom,   //18
                                                  GetDate(data.date_compapp),   //19
                                                  data.decline_reason, //20
                                                  data.dept,
                                                  data.com_decline_reason,
                                                  data.c_o,
                                                  data.Bldg,
                                                  data.Office,
                                                  data.usp_dept,
                                                  data.POBox); 

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.VendorIdRequest data)
        {
            string query = string.Format(@"UPDATE [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest]
                                           SET
                                                date_completed = {1},
                                                date_requested = {2},
                                                email = '{3}',
                                                invoice_name = '{4}',
                                                name = '{5}',
                                                postal_code = '{6}',
                                                status = '{7}',
                                                state = '{8}',
                                                street_name = '{9}',
                                                telephone = '{10}',
                                                vendor_account_created = '{11}',
                                                vendor_account_requested = '{12}',
                                                notes = '{13}',
                                                type = '{14}',
                                                username = '{15}',
                                                last_date_modified = CONVERT(datetime,SWITCHOFFSET(CONVERT(datetimeoffset,GETUTCDATE()),'+08:00')),
                                                vmo_status = '{16}',
                                                city = '{17}',
                                                userid_requestor = '{18}',
                                                userid_compapp = '{19}',
                                                userid_reqcom = '{20}',
                                                date_compapp = {21},
                                                decline_reason = '{22}',
                                                dept = '{23}',
                                                tl_reviewedby = '{24}',
                                                tl_reviewtimestamp = '{25}',
                                                com_decline_reason = '{26}'
                                           WHERE
                                                request_id = {0}",

                                                  data.request_id,  //0
                                                  GetDate(data.date_completed), //1
                                                  GetDate(data.date_requested), //2
                                                  data.email,   //3
                                                  data.invoice_name, //4
                                                  data.name, //5
                                                  data.postal_code, //6
                                                  data.status, //7
                                                  data.state, //8
                                                  data.street_name, //9
                                                  data.telephone, //10
                                                  data.vendor_account_created, //11
                                                  data.vendor_account_requested, //12
                                                  data.notes,   //13
                                                  data.type,    //14
                                                  data.username,    //15
                                                  data.vmo_status,  //16
                                                  data.city,    //17
                                                  data.userid_requestor,    //18
                                                  data.userid_compapp,  //19
                                                  data.userid_reqcom,   //20
                                                  GetDate(data.date_compapp),   //21
                                                  data.decline_reason,
                                                  data.dept, //22
                                                  data.tl_reviewedby, //23
                                                  data.tl_reviewtimestamp,
                                                  data.com_decline_reason); //24


            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.VendorIdRequest GetData(uint request_id)
        {
            DatabaseSelector selector = new SelectorVendorIdRequest();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest] (nolock) WHERE request_id={0}", request_id));
            
            base.ExecuteSelect(selector);

            return ((SelectorVendorIdRequest)selector).GetResult();
        }

        public DB.VendorIdRequest GetDataProcessor(string request_id)
        {
            DatabaseSelector selector = new SelectorVendorIdRequest();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest] (nolock) WHERE request_id = '{0}'", request_id));

            base.ExecuteSelect(selector);

            return ((SelectorVendorIdRequest)selector).GetResult();
        }

        public List<DB.VendorIdRequest> GetDataProcessor(DateTime from, DateTime to)
        {
            DatabaseSelector selector = new SelectorVendorIdRequest();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest] (nolock) WHERE last_date_modified > '{0}' and last_date_modified < '{1}'", from, to));

            base.ExecuteSelect(selector);

            return ((SelectorVendorIdRequest)selector).GetResults();
        }

        public List<DB.VendorIdRequest> GetDataCOMStatus(DateTime from, DateTime to, string status)
        {
            DatabaseSelector selector = new SelectorVendorIdRequest();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest] (nolock) 
                                            WHERE last_date_modified > '{0}' and last_date_modified < '{1}' and status ='{2}' ORDER BY date_compapp", from, to, status));

            base.ExecuteSelect(selector);

            return ((SelectorVendorIdRequest)selector).GetResults();
        }

        public List<DB.VendorIdRequest> GetDataVMOStatus(DateTime from, DateTime to, string vmo_status)
        {
            DatabaseSelector selector = new SelectorVendorIdRequest();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest] (nolock) 
                                            WHERE last_date_modified > '{0}' and last_date_modified < '{1}' and vmo_status = '{2}'", from, to, vmo_status));

            base.ExecuteSelect(selector);

            return ((SelectorVendorIdRequest)selector).GetResults();
        }

        public List<DB.VendorIdRequest> GetDataByStatus(string cmo_status)
        {
            DatabaseSelector selector = new SelectorVendorIdRequest();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest] (nolock) 
                                            WHERE status = '{0}' ORDER BY date_requested", cmo_status));

            base.ExecuteSelect(selector);

            return ((SelectorVendorIdRequest)selector).GetResults();
        }

        public List<DB.VendorIdRequest> GetDataByVMOStatus(string vmo_status)
        {
            DatabaseSelector selector = new SelectorVendorIdRequest();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest] (nolock) WHERE vmo_status = '{0}' ORDER BY date_compapp", vmo_status));

            base.ExecuteSelect(selector);

            return ((SelectorVendorIdRequest)selector).GetResults();
        }

        private string GetDate(DateTime dt)
        {
            if (dt != DateTime.MinValue)
            {
                return string.Format("'{0}'", dt.ToString("yyyy-MM-dd HH:mm:ss"));
            }
            else
            {
                return "NULL";
            }
        }

        public DB.VendorIdRequest GetDuplicateRequest(string vendor_account_requested, string name, string telephone,
            string postal_code, string state, string email, string city, string street_name)
        {
            DatabaseSelector selector = new SelectorVendorIdRequest();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest]
                                            WHERE 
                                                [vendor_account_requested] = '{0}' AND
                                                [name] = '{1}' AND
                                                [telephone] = '{2}' AND
                                                [postal_code] = '{3}' AND
                                                [state] = '{4}' AND
                                                [email] = '{5}' AND
                                                [city] = '{6}' AND
                                                [street_name] = '{7}'",
                                                  vendor_account_requested,
                                                  name,
                                                  telephone,
                                                  postal_code,
                                                  state,
                                                  email,
                                                  city,
                                                  street_name));

            base.ExecuteSelect(selector);

            return ((SelectorVendorIdRequest)selector).GetResult();
        }

        public DB.VendorIdRequest GetDuplicateRequest2(string name, string telephone,
            string postal_code, string state, string email, string city, string street_name)
        {
            DatabaseSelector selector = new SelectorVendorIdRequest();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest]
                                            WHERE 
                                                [name] = '{0}' AND
                                                [telephone] = '{1}' AND
                                                [postal_code] = '{2}' AND
                                                [state] = '{3}' AND
                                                [email] = '{4}' AND
                                                [city] = '{5}' AND
                                                [street_name] = '{6}'",
                                                  name,
                                                  telephone,
                                                  postal_code,
                                                  state,
                                                  email,
                                                  city,
                                                  street_name));

            base.ExecuteSelect(selector);

            return ((SelectorVendorIdRequest)selector).GetResult();
        }

        public List<DB.VendorIdRequest> GetDuplicateRequests2(string name, string telephone,
            string postal_code, string state, string email, string city, string street_name)
        {
            DatabaseSelector selector = new SelectorVendorIdRequest();
            selector.SetQuery(String.Format(@"SELECT * FROM [MIS_ALTI].[dbo].[tbl_EM_VendorIDRequest]
                                            WHERE 
                                                [name] = '{0}' AND
                                                [telephone] = '{1}' AND
                                                [postal_code] = '{2}' AND
                                                [state] = '{3}' AND
                                                [email] = '{4}' AND
                                                [city] = '{5}' AND
                                                [street_name] = '{6}' AND
                                                ([vmo_status] NOT IN ('REQ_COM') OR [vmo_status] IS NULL)",
                                                  name,
                                                  telephone,
                                                  postal_code,
                                                  state,
                                                  email,
                                                  city,
                                                  street_name));

            base.ExecuteSelect(selector);

            return ((SelectorVendorIdRequest)selector).GetResults();
        }
    }
}
