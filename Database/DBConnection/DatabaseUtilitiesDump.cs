﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabaseUtilitiesDump : DatabaseConnector, IDatabase<DB.Utilities_Dump>
    {


        public List<DB.Utilities_Dump> GetAllData()
        {
            DatabaseSelector selector = new SelectorUtilitiesDump();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_Utilities_Dump");

            base.ExecuteSelect(selector);

            return ((SelectorUtilitiesDump)selector).GetResults();
        }

        public uint WriteData(DB.Utilities_Dump data)
        {
            string query = string.Format(@"");

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.Utilities_Dump data)
        {

        }

        public DB.Utilities_Dump GetData(string property_code, string utility)
        {
            DatabaseSelector selector = new SelectorUtilitiesDump();
            selector.SetQuery(String.Format(@"SELECT * FROM
                                (
                                SELECT     PROPERTY_CODE, ELEC_VENDOR_CODE, ELEC_VENDOR_NAME, ELEC_ACCNTNO, 'E' [Utility]
                                FROM         dbo.tbl_EM_Utilities_Dump
                                WHERE     (ELEC_VENDOR_CODE IS NOT NULL)
                                UNION ALL
                                SELECT     PROPERTY_CODE, GAS_VENDOR_CODE, GAS_VENDOR_NAME, GAS_ACCNTNO, 'G' [Utility]
                                FROM         dbo.tbl_EM_Utilities_Dump
                                WHERE     (GAS_VENDOR_CODE IS NOT NULL)
                                UNION ALL
                                SELECT     PROPERTY_CODE, WATER_VENDOR_CODE, WATER_VENDOR_NAME, WATER_ACCNTNO, 'W' [Utility]
                                FROM         dbo.tbl_EM_Utilities_Dump
                                WHERE     (WATER_VENDOR_CODE IS NOT NULL)
                                ) tbl_src
                                WHERE PROPERTY_CODE = '{0}' AND Utility = '{1}'",
                                property_code,
                                utility));

            base.ExecuteSelect(selector);

            return ((SelectorUtilitiesDump)selector).GetResult();
        }

    }
}
