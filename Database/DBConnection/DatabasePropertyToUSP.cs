﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Database.DBSelector;
using Database.DB;

namespace Database.DBConnection
{
    public class DatabasePropertyToUSP : DatabaseConnector, IDatabase<DB.PropertyToUSP>
    {
        public List<DB.PropertyToUSP> GetAllData()
        {
            DatabaseSelector selector = new SelectorPropertyToUSP();
            selector.SetQuery(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_PropertyToUSP (nolock)");

            base.ExecuteSelect(selector);

            return ((SelectorPropertyToUSP)selector).GetResults();
        }

        public uint WriteData(DB.PropertyToUSP data)
        {
            string eturnoffdate = "NULL";
            string gturnoffdate = "NULL";
            string wturnoffdate = "NULL";
            
            if (data.electricity_turnoff_date != DateTime.MinValue)
            {
                eturnoffdate = string.Format("'{0}'", data.electricity_turnoff_date.ToString("yyyy-MM-dd HH:mm:ss"));   
            }

            if (data.gas_turnoff_date != DateTime.MinValue)
            {
                gturnoffdate = string.Format("'{0}'", data.gas_turnoff_date.ToString("yyyy-MM-dd HH:mm:ss"));
            }

            if (data.water_turnoff_date != DateTime.MinValue)
            {
                wturnoffdate = string.Format("'{0}'", data.water_turnoff_date.ToString("yyyy-MM-dd HH:mm:ss"));
            }

            string query = string.Format(@"INSERT INTO MIS_Alti.dbo.tbl_EM_PropertyToUSP(
                                                property_code,
                                                electricity_vendor_code,
                                                gas_vendor_code,
                                                water_vendor_code,
                                                electricity_turnoff_date,
                                                gas_turnoff_date,
                                                water_turnoff_date,
                                                active)
                                            VALUES('{0}','{1}','{2}','{3}',{4},{5},{6},'Active')",
                                                  data.property_code, //0
                                                  data.electricity_vendor_code, //1
                                                  data.gas_vendor_code, //2
                                                  data.water_vendor_code, //3
                                                  eturnoffdate, //4
                                                  gturnoffdate, //5
                                                  wturnoffdate); //6

            query = DBHelper.toSingleSpace(query);
            return (uint)base.ExecuteNonQuery(query);
        }

        public void UpdateData(DB.PropertyToUSP data)
        {
            string query = string.Format(@"UPDATE MIS_Alti.dbo.tbl_EM_PropertyToUSP
                                           SET
                                                electricity_vendor_code = '{0}',
                                                gas_vendor_code = '{1}',
                                                water_vendor_code = '{2}',
                                                electricity_turnoff_date = '{3}',
                                                gas_turnoff_date = '{4}',
                                                water_turnoff_date = '{5}',
                                           WHERE
                                                property_code = '{6}'",

                                            data.electricity_vendor_code, //0
                                            data.gas_vendor_code, //1
                                            data.water_vendor_code, //2
                                            data.electricity_turnoff_date.ToString("yyyy-MM-dd HH:mm:ss"), //3
                                            data.gas_turnoff_date.ToString("yyyy-MM-dd HH:mm:ss"), //4
                                            data.water_turnoff_date.ToString("yyyy-MM-dd HH:mm:ss"), //5
                                            
                                            data.property_code); //6

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

        public DB.PropertyToUSP GetData(uint property_code)
        {
            DatabaseSelector selector = new SelectorPropertyToUSP();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_PropertyToUSP (nolock) WHERE property_code={0}", property_code));
            
            base.ExecuteSelect(selector);

            return ((SelectorPropertyToUSP)selector).GetResult();
        }

        public DB.PropertyToUSP GetDataProcessor(string property_code)
        {
            DatabaseSelector selector = new SelectorPropertyToUSP();
            selector.SetQuery(String.Format(@"SELECT * FROM MIS_Alti.dbo.tbl_EM_PropertyToUSP (nolock) WHERE property_code = '{0}'", property_code));

            base.ExecuteSelect(selector);

            return ((SelectorPropertyToUSP)selector).GetResult();
        }

        public void DeleteData()
        {
            string query = @"Truncate Table MIS_Alti.dbo.tbl_EM_PropertyToUSP"; //6

            query = DBHelper.toSingleSpace(query);
            base.ExecuteNonQuery(query);
        }

    }
}
