﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class Payroll_Data
    {
        public int Payroll_Data_Id;
        public string userId;
        public DateTime workSched;
        public double HoursND;
        public double HoursReg;
        public double HoursOT;
        public double HoursNDAdjustment;
        public double HoursRegAdjustment;
        public double HoursOTAdjustment;
        public double HoursNDHoliday;
        public double HoursRegHoliday;
        public double HoursOTHoliday;
    }
}
