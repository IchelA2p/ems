﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class EbillWebsites
    {
        public int id;
        public string website_name;
        public string usp_name;
        public string username;
        public string password;
        public string email;
        public string createdby;
        public string updatedby;
        public DateTime date_created;
        public DateTime last_date_modified;
    }
}
