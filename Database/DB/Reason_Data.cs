﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class Reason_Data
    {
        public int ReasonId_Reference;
        public string ReasonId;
        public string Description;
        public bool ExcludeOnPayment;
    }
}
