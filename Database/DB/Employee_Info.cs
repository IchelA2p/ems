﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class Employee_Info
    {
        public int EmployeePrimaryId;
        public string EmployeeId;
        public string userId;
        public string FirstName;
        public string MiddleName;
        public string LastName;
        public string Nickname;
        public string NTID;
        public string Position;
        public bool isActive;
        public string Username;
        public string Password;
    }
}
