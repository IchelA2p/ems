﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class PreviousWI
    {
        public string property_code;
        public string vendor_code;
        public string work_order_number;
        public string wo_task;
        public string work_order_status;
        public decimal vendor_price;
        public DateTime issued_date;
        public DateTime completed_date;
        public string customer_name;
    }
}
