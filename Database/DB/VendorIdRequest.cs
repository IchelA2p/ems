﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Database.DB
{
    public class VendorIdRequest
    {
        public string vendor_account_requested;
        public string vendor_account_created;
        public string name;
        public string telephone;
        public string postal_code;
        public string state;
        public string email;
        public string city;
        public string street_name;
        public string status;
        public string type;
        public string dept;
        public DateTime date_requested;
        public DateTime date_completed;
        public string invoice_name;
        public string notes;
        public string username;
        public int request_id;
        public DateTime last_date_modified;
        public string vmo_status;
        public string userid_compapp;
        public string userid_reqcom;
        public string userid_requestor;
        public DateTime date_compapp;
        public string decline_reason;
        public DateTime tl_reviewtimestamp;
        public string tl_reviewedby;
        public string com_decline_reason;
        public string c_o;
        public string Bldg;
        public string Office;
        public string usp_dept;
        public string POBox;        
    }
}
