﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorEmployee : DatabaseSelector
    {
        private List<DB.Employee_Info> employeelist;

        public SelectorEmployee()
         {
             employeelist = new List<DB.Employee_Info>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            employeelist.Clear();

            int isActive = 0;
            while (dataReader.Read())
            {
                DB.Employee_Info info = new DB.Employee_Info();
                info.EmployeePrimaryId = (int)dataReader["Employeeprimaryid"];
                try
                { info.EmployeeId = ((string)dataReader["Employeeid"]).Trim(); }
                catch
                { info.EmployeeId = string.Empty; }
                try
                { info.userId = ((string)dataReader["Userid"]).Trim(); }
                catch
                { info.userId = string.Empty; }
                //info.userId = (string)dataReader["userId"];
                info.FirstName = ((string)dataReader["Firstname"]).Trim();
                try
                { info.MiddleName = ((string)dataReader["Middlename"]).Trim(); }
                catch
                { info.MiddleName = string.Empty; }
                info.LastName = ((string)dataReader["Lastname"]).Trim();
                try
                { info.Nickname = ((string)dataReader["Nickname"]).Trim(); }
                catch
                { info.Nickname = string.Empty; }
                try
                { info.NTID = ((string)dataReader["ntid"]).Trim(); }
                catch
                { info.NTID = string.Empty; }
                try
                { info.Position = ((string)dataReader["Position"]).Trim(); }
                catch
                { info.Position = string.Empty; }

                isActive = Convert.ToInt32(dataReader["isActive"]);

                if (isActive == 1)
                {
                    info.isActive = true;
                }
                else
                {
                    info.isActive = false;
                }
                try
                {
                    info.Username = ((string)dataReader["Username"]).Trim();
                }
                catch
                {
                    info.Username = string.Empty;
                }
                try
                {
                    info.Password = ((string)dataReader["Password"]).Trim();
                }
                catch
                {
                    info.Password = string.Empty;
                }
                

                employeelist.Add(info);
            }
        }

        public List<DB.Employee_Info> GetResults()
        {
            return employeelist.Count == 0 ? null : employeelist;
        }

        public DB.Employee_Info GetResult()
        {
            return employeelist.Count == 0 ? null : employeelist[0];
        }
    }
}
