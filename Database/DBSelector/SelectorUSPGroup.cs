﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorUSPGroup : DatabaseSelector
    {
        private List<Database.DB.USPGroup> list;

        public SelectorUSPGroup()
         {
             list = new List<DB.USPGroup>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.USPGroup info = new DB.USPGroup();

                info.USPGroupName = GetString("USPGroupName", dataReader);
                info.PhoneNo = GetString("PhoneNo", dataReader);

                list.Add(info);
            }
        }

        public List<DB.USPGroup> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.USPGroup GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }

        public string GetString(string columnName, SqlDataReader rd)
        {
            try
            {
                return ((string)rd[columnName]).Trim();
            }
            catch
            {
                return string.Empty;
            }
        }
    }
}
