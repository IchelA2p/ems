﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorEmployeeRelation : DatabaseSelector
    {
        private List<Database.DB.Employee_Relation> list;

        public SelectorEmployeeRelation()
         {
             list = new List<DB.Employee_Relation>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.Employee_Relation info = new DB.Employee_Relation();
                info.UpperRank_EmployeePrimaryId = (int)dataReader["UpperRank_EmployeePrimaryId"];
                info.LowerRank_EmployeePrimaryId = (int)dataReader["LowerRank_EmployeePrimaryId"];

                list.Add(info);
            }
        }

        public List<DB.Employee_Relation> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.Employee_Relation GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
