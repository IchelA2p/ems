﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;


namespace Database.DBSelector
{
    public abstract class DatabaseSelector
    {
        private string Query;
        public abstract void GetData(SqlDataReader dataReader);

        public void SetQuery(string query)
        {
            this.Query = query;
        }

        public string GetQuery()
        {
            return Query;
        }

        public string GetStrData(SqlDataReader rd, string columnName)
        {
            try
            {
                return rd[columnName].ToString().Trim();
            }
            catch
            {
                return string.Empty;
            }
        }

        public DateTime GetDateTimeData(SqlDataReader rd, string columnName)
        {
            try
            {
                return DateTime.Parse(rd[columnName].ToString());
            }
            catch
            {
                return DateTime.MinValue;
            }
        }

        public bool GetBool(SqlDataReader rd, string columnName)
        {
            try
            {
                return Convert.ToInt32(rd[columnName].ToString()) == 1 ? true : false;
            }
            catch
            {

                try
                {
                    return rd[columnName].ToString() == "True" ? true : false;
                }
                catch
                {
                    return false;
                }              
            }
        }

        public decimal GetDecimal(SqlDataReader sqlReader, string columnName)
        {
            try
            {
                return Convert.ToDecimal(sqlReader[columnName]);
            }
            catch
            {
                return 0;
            }
        }
    }
}
