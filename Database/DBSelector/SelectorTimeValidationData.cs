﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorTimeValidationData : DatabaseSelector
    {
        private List<Database.DB.TimeValidation_Data> list;

        public SelectorTimeValidationData()
         {
             list = new List<DB.TimeValidation_Data>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.TimeValidation_Data info = new DB.TimeValidation_Data();
                info.TimeValidationId = (int)dataReader["Time_validationid"];
                info.StartDate = GetDateTimeData(dataReader, "StartDate");
                info.EndDate = GetDateTimeData(dataReader, "EndDate");
                info.Validator_EmpId = (int)dataReader["Validator_EmpId"];
                info.TimeValidationDate = GetDateTimeData(dataReader, "TimeValidationDate");
                info.Userid = ((string)dataReader["Userid"]).Trim();
                info.isCreatedColumn = GetBool(dataReader, "isCreatedColumn");
                info.NewNotReadyReasonId = GetStrData(dataReader, "NewNotReadyReasonId"); 
                
                list.Add(info);
            }
        }

        public List<DB.TimeValidation_Data> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.TimeValidation_Data GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
