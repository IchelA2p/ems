﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorEbillAccountNumber : DatabaseSelector
    {
        private List<Database.DB.EbillAccountNumber> list;

        public SelectorEbillAccountNumber()
         {
             list = new List<DB.EbillAccountNumber>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.EbillAccountNumber info = new DB.EbillAccountNumber();
                info.acct_id = int.Parse(dataReader["acct_id"].ToString());
                info.ebill_website_id = int.Parse(dataReader["ebill_website_id"].ToString());
                info.account_number = GetString(dataReader, "account_number");

                list.Add(info);
            }
        }

        public List<DB.EbillAccountNumber> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.EbillAccountNumber GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }

        public string GetString(SqlDataReader rd, string columnName)
        {
            try
            {
                return rd[columnName].ToString().Trim();
            }
            catch
            {
                return string.Empty;
            }
        }

        public DateTime GetDateTime(SqlDataReader rd, string columnName)
        {
            try
            {
                return DateTime.Parse(rd[columnName].ToString());        
            }
            catch
            {
                return DateTime.MinValue;
            }
        }
    }
}
