﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorREO_Properties : DatabaseSelector
    {
        private List<Database.DB.REO_Properties> list;

        public SelectorREO_Properties()
         {
             list = new List<DB.REO_Properties>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.REO_Properties info = new DB.REO_Properties();
                try
                {
                    info.city_name = ((string)dataReader["city_name"]).Trim();
                }
                catch
                {
                    info.city_name = string.Empty;
                }
                try
                {
                    info.full_address = ((string)dataReader["full_address"]).Trim();
                }
                catch
                {
                    info.city_name = string.Empty;
                }
                try
                {
                    info.property_code = ((string)dataReader["property_code"]).Trim();
                }
                catch
                {
                    info.city_name = string.Empty;
                }
                try
                {
                    info.property_number = ((string)dataReader["property_number"]).Trim();
                }
                catch
                {
                    info.city_name = string.Empty;
                }
                try
                {
                    info.state = ((string)dataReader["state"]).Trim();
                }
                catch
                {
                    info.city_name = string.Empty;
                }
                try
                {
                    info.zip_code = ((string)dataReader["zip_code"]).Trim();
                }
                catch
                {
                    info.city_name = string.Empty;
                }

                try
                {
                    info.customer_id = ((string)dataReader["customer_id"]).Trim();
                }
                catch
                {
                    info.customer_id = string.Empty;
                }
                try
                {
                    info.customer_name = ((string)dataReader["customer_name"]).Trim();
                }
                catch
                {
                    info.customer_name = string.Empty;
                }
                try
                {
                    info.investor_name = ((string)dataReader["investor_name"]).Trim();
                }
                catch
                {
                    info.investor_name = string.Empty;
                }
                try
                {
                    info.property_status = ((string)dataReader["property_status"]).Trim();
                }
                catch
                {
                    info.property_status = string.Empty;
                }
                try
                {
                    info.reosrc_date = (DateTime)dataReader["reosrc_date"];
                }
                catch
                {
                    info.reosrc_date = DateTime.MinValue;
                }
                try
                {
                    info.reoslc_date = (DateTime)dataReader["reoslc_date"];
                }
                catch
                {
                    info.reoslc_date = DateTime.MinValue;
                }
                try
                {
                    info.reosfc_date = (DateTime)dataReader["reosfc_date"];
                }
                catch
                {
                    info.reosfc_date = DateTime.MinValue;
                }
                try
                {
                    info.active = (string)dataReader["active"];
                }
                catch
                {
                    info.active = "active";
                }
                try
                {
                    info.occupancy_status = (string)dataReader["occupancy_status"];
                }
                catch
                {
                    info.occupancy_status = "occupancy_status";
                }
                try
                {
                    info.property_status_change_date = (DateTime)dataReader["property_status_change_date"];
                }
                catch
                {
                    info.property_status_change_date = DateTime.MinValue;
                }
                try
                {
                    info.inactive_date = (DateTime)dataReader["inactive_date"];
                }
                catch
                {
                    info.inactive_date = DateTime.MinValue;
                }

                info.pod = GetStrData(dataReader, "pod");

                list.Add(info);
            }
        }

        public List<DB.REO_Properties> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.REO_Properties GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
