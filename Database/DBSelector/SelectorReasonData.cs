﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorReasonData : DatabaseSelector
    {
        private List<Database.DB.Reason_Data> list;

        public SelectorReasonData()
         {
             list = new List<DB.Reason_Data>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.Reason_Data info = new DB.Reason_Data();
                info.ReasonId = ((string)dataReader["ReasonId"]).Trim();
                info.Description = ((string)dataReader["Description"]).Trim();
                info.ExcludeOnPayment = GetBool(dataReader, "ExcludeOnPayment");

                list.Add(info);
            }
        }

        public List<DB.Reason_Data> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.Reason_Data GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
