﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorHoliday : DatabaseSelector
    {
        private List<Database.DB.Holiday> list;

        public SelectorHoliday()
         {
             list = new List<DB.Holiday>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            int regularholiday = 0;
            int specialholiday = 0;
            while (dataReader.Read())
            {
                DB.Holiday info = new DB.Holiday();
                info.holiday_id = (int)dataReader["holiday_id"];
                info.date = (DateTime)dataReader["date"];
                regularholiday = (int)dataReader["regularholiday"];
                specialholiday = (int)dataReader["specialholiday"];
                info.regularholiday = false;
                info.specialholiday = false;
                if (regularholiday == 1)
                {
                    info.regularholiday = true;
                }
                if (specialholiday == 1)
                {
                    info.specialholiday = true;
                }
                info.rate = (double)dataReader["rate"];
                info.name = ((string)dataReader["name"]).Trim();

                list.Add(info);
            }
        }

        public List<DB.Holiday> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.Holiday GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
