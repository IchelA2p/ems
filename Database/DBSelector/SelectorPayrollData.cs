﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorPayrollData : DatabaseSelector
    {
        private List<Database.DB.Payroll_Data> list;

        public SelectorPayrollData()
         {
             list = new List<DB.Payroll_Data>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.Payroll_Data info = new DB.Payroll_Data();
                info.Payroll_Data_Id = (int)dataReader["Payroll_Data_Id"];
                info.userId = (string)dataReader["userId"];
                info.workSched = (DateTime)dataReader["workSched"];
                info.HoursND = (int)dataReader["HoursND"];
                info.HoursReg = (int)dataReader["HoursReg"];
                info.HoursOT = (int)dataReader["HoursOT"];
                info.HoursNDAdjustment = (int)dataReader["HoursNDAdjustment"];
                info.HoursRegAdjustment = (int)dataReader["HoursRegAdjustment"];
                info.HoursOTAdjustment = (int)dataReader["HoursOTAdjustment"];
                info.HoursNDHoliday = (int)dataReader["HoursNDHoliday"];
                info.HoursRegHoliday = (int)dataReader["HoursRegHoliday"];
                info.HoursOTHoliday = (int)dataReader["HoursOTHoliday"];

                list.Add(info);
            }
        }

        public List<DB.Payroll_Data> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.Payroll_Data GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
