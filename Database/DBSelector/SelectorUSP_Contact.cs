﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorUSP_Contact : DatabaseSelector
    {
        private List<Database.DB.USP_Contact> list;

        public SelectorUSP_Contact()
         {
             list = new List<DB.USP_Contact>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.USP_Contact info = new DB.USP_Contact();

                info.Address = GetStrData(dataReader, "Address");
                info.Name = GetStrData(dataReader, "Name");
                info.PhoneNumber = GetStrData(dataReader, "PhoneNumber");
                info.Type = GetStrData(dataReader, "Type");
                info.VendorId = GetStrData(dataReader, "VendorId");
                info.City = GetStrData(dataReader, "City");
                info.State = GetStrData(dataReader, "State");
                info.Zipcode = GetStrData(dataReader, "Zipcode");
                info.Status = GetStrData(dataReader, "Status");

                list.Add(info);
            }
        }

        public List<DB.USP_Contact> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.USP_Contact GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
