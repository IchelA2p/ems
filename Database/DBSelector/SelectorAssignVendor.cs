﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorAssignVendor : DatabaseSelector
    {
        private List<Database.DB.AssignVendor> list;

        public SelectorAssignVendor()
         {
             list = new List<DB.AssignVendor>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.AssignVendor info = new DB.AssignVendor();
                info.id = (int)dataReader["id"];
                info.dateAssigned = GetDateTimeData(dataReader, "dateAssigned");
                info.dateCreated = GetDateTimeData(dataReader, "dateCreated");
                info.isAssigned = (bool)dataReader["isAssigned"];
                info.property_code = GetStrData(dataReader, "property_code");
                info.remarks = GetStrData(dataReader, "remarks");
                info.vendor_code = GetStrData(dataReader, "vendor_code");
                info.vendor_group = GetStrData(dataReader, "vendor_group");
                info.isActive = (bool)dataReader["isActive"];
                info.createdBy = GetStrData(dataReader, "createdBy");

                list.Add(info);
            }
        }

        public List<DB.AssignVendor> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.AssignVendor GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
