﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorUtilitiesDump : DatabaseSelector
    {
        private List<Database.DB.Utilities_Dump> list;

        public SelectorUtilitiesDump()
        {
            list = new List<DB.Utilities_Dump>();
        }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.Utilities_Dump info = new DB.Utilities_Dump();
                try
                {
                    info.property_code = ((string)dataReader["PROPERTY_CODE"]).Trim();
                }
                catch
                {
                    info.property_code = string.Empty;
                }


                try
                {
                    info.elec_vendor_code = ((string)dataReader["ELEC_VENDOR_CODE"]).Trim();
                }
                catch
                {
                    info.elec_vendor_code = string.Empty;
                }

                
                try
                {
                    info.elec_vendor_name = ((string)dataReader["ELEC_VENDOR_NAME"]).Trim();
                }
                catch
                {
                    info.elec_vendor_name = string.Empty;
                }


                try
                {
                    info.elec_accntno = ((string)dataReader["ELEC_ACCNTNO"]).Trim();
                }
                catch
                {
                    info.elec_accntno = string.Empty;
                }


                try
                {
                    info.gas_vendor_code = ((string)dataReader["GAS_VENDOR_CODE"]).Trim();
                }
                catch
                {
                    info.gas_vendor_code = string.Empty;
                }


                try
                {
                    info.gas_vendor_name = ((string)dataReader["GAS_VENDOR_NAME"]).Trim();
                }
                catch
                {
                    info.gas_vendor_name = string.Empty;
                }


                try
                {
                    info.gas_accntno = ((string)dataReader["GAS_ACCNTNO"]).Trim();
                }
                catch
                {
                    info.gas_accntno = string.Empty;
                }

                try
                {
                    info.water_vendor_code = ((string)dataReader["WATER_VENDOR_CODE"]).Trim();
                }
                catch
                {
                    info.water_vendor_code = string.Empty;
                }


                try
                {
                    info.water_vendor_name = ((string)dataReader["WATER_VENDOR_NAME"]).Trim();
                }
                catch
                {
                    info.water_vendor_name = string.Empty;
                }


                try
                {
                    info.water_accntno = ((string)dataReader["WATER_ACCNTNO"]).Trim();
                }
                catch
                {
                    info.water_accntno = string.Empty;
                }


                list.Add(info);
            }
        }

        public List<DB.Utilities_Dump> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.Utilities_Dump GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
