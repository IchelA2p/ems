﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorAssetManager : DatabaseSelector
    {
        private List<DB.AssetManager> list;

        public SelectorAssetManager()
         {
             list = new List<DB.AssetManager>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.AssetManager info = new DB.AssetManager();
                info.property_code = GetStrData(dataReader, "property_code");
                info.asset_manager = GetStrData(dataReader, "asset_manager");

                list.Add(info);
            }
        }

        public List<DB.AssetManager> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.AssetManager GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
