﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorVendorIdRequest : DatabaseSelector
    {
        private List<Database.DB.VendorIdRequest> list;

        public SelectorVendorIdRequest()
         {
             list = new List<DB.VendorIdRequest>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.VendorIdRequest info = new DB.VendorIdRequest();

                info.username = GetString(dataReader, "username");
                info.city = GetString(dataReader, "city");
                info.email = GetString(dataReader, "email");
                info.invoice_name = GetString(dataReader, "invoice_name");
                info.name = GetString(dataReader, "name");
                info.postal_code = GetString(dataReader, "postal_code");
                info.vmo_status = GetString(dataReader, "vmo_status");
                info.status = GetString(dataReader, "status");
                info.type = GetString(dataReader, "type");
                info.dept = GetString(dataReader, "dept");
                info.state = GetString(dataReader, "state");
                info.street_name = GetString(dataReader, "street_name");
                info.telephone = GetString(dataReader, "telephone");
                info.vendor_account_created = GetString(dataReader, "vendor_account_created");
                info.vendor_account_requested = GetString(dataReader, "vendor_account_requested");
                info.date_completed = GetDateTime(dataReader, "date_completed");
                info.date_requested = GetDateTime(dataReader, "date_requested");
                info.request_id = (int)dataReader["request_id"];
                info.notes = GetString(dataReader, "notes");
                info.last_date_modified = GetDateTime(dataReader, "last_date_modified");
                info.userid_requestor = GetString(dataReader, "userid_requestor");
                info.userid_compapp = GetString(dataReader, "userid_compapp");
                info.userid_reqcom = GetString(dataReader, "userid_reqcom");
                info.date_compapp = GetDateTime(dataReader, "date_compapp");
                info.decline_reason = GetString(dataReader, "decline_reason");
                info.tl_reviewtimestamp = GetDateTime(dataReader, "tl_reviewtimestamp");
                info.tl_reviewedby = GetString(dataReader, "tl_reviewedby");
                info.com_decline_reason = GetString(dataReader, "com_decline_reason");
                info.c_o = GetString(dataReader, "c_o");
                info.Bldg = GetString(dataReader, "Bldg");
                info.Office = GetString(dataReader, "Office");
                info.usp_dept = GetString(dataReader, "usp_dept");
                info.POBox = GetString(dataReader, "POBox");                
                list.Add(info);
            }
        }

        public List<DB.VendorIdRequest> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.VendorIdRequest GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }

        public string GetString(SqlDataReader reader, string columnName)
        {
            string result = string.Empty;
            try
            {
                result = ((string)reader[columnName]).Trim();
            }
            catch
            {
                result = string.Empty;
            }
            return result;
        }

        public DateTime GetDateTime(SqlDataReader reader, string columnName)
        {
            DateTime result = new DateTime();
            try
            {
                result = (DateTime)reader[columnName];
            }
            catch
            {
                result = DateTime.MinValue;
            }
            return result;
        }
    }
}
