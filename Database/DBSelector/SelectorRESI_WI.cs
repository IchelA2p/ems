﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorRESI_WI : DatabaseSelector
    {
        private List<Database.DB.RESI_WI> list;

        public SelectorRESI_WI()
         {
             list = new List<DB.RESI_WI>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.RESI_WI info = new DB.RESI_WI();
                info.property_code = (string)dataReader["property_code"];
                try
                {
                    info.Gas_WI = (string)dataReader["Gas_WI"];
                    info.Gas_Price = (string)dataReader["Gas_Price"];
                    info.Gas_WI_Date = (DateTime)dataReader["Gas_WI_Date"];
                }
                catch
                {
                    info.Gas_WI = string.Empty;
                    info.Gas_Price = string.Empty;
                    info.Gas_WI_Date = DateTime.MinValue;
                }
                try
                {
                    info.Water_WI = (string)dataReader["Water_WI"];
                    info.Water_Price = (string)dataReader["Water_Price"];
                    info.Water_WI_Date = (DateTime)dataReader["Water_WI_Date"];
                }
                catch
                {
                    info.Water_WI = string.Empty;
                    info.Water_Price = string.Empty;
                    info.Water_WI_Date = DateTime.MinValue;
                }
                try
                {
                    info.Elec_WI = (string)dataReader["Elec_WI"];
                    info.Elec_Price = (string)dataReader["Elec_Price"];
                    info.Elec_WI_Date = (DateTime)dataReader["Elec_WI_Date"];
                }
                catch
                {
                    info.Elec_WI = string.Empty;
                    info.Elec_Price = string.Empty;
                    info.Elec_WI_Date = DateTime.MinValue;
                }

                list.Add(info);
            }
        }

        public List<DB.RESI_WI> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.RESI_WI GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
