﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorUSPWebsite : DatabaseSelector
    {
        private List<Database.DB.USPWebsite> list;

        public SelectorUSPWebsite()
         {
             list = new List<DB.USPWebsite>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.USPWebsite info = new DB.USPWebsite();
                try { info.dateUpdated = DateTime.Parse(dataReader["dateUpdated"].ToString()); }
                catch { info.dateUpdated = DateTime.MinValue; }
                info.isWorking = (bool)dataReader["isWorking"];
                info.oldPassword = GetStrData(dataReader, "oldPassword");
                info.oldUsername = GetStrData(dataReader, "oldUsername");
                info.password = GetStrData(dataReader, "password");
                info.updatedBy = GetStrData(dataReader, "updatedBy");
                info.username = GetStrData(dataReader, "username");
                info.uspName = GetStrData(dataReader, "uspName");
                info.uspWebsiteID = (int)dataReader["uspWebsiteID"];
                info.website = GetStrData(dataReader, "website");
                
                list.Add(info);
            }
        }

        public List<DB.USPWebsite> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.USPWebsite GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
