﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    class SelectorAspect : DatabaseSelector
    {
        private List<Database.DB.Aspect_Data> aspectlist;

        public SelectorAspect()
         {
             aspectlist = new List<DB.Aspect_Data>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            aspectlist.Clear();
            while (dataReader.Read())
            {
                DB.Aspect_Data info = new DB.Aspect_Data();
                info.AspectDataId = (int)dataReader["aspectdataid"];
                info.logindt = GetDateTimeData(dataReader, "logindt");
                info.logoutdt = GetDateTimeData(dataReader, "logoutdt");
                info.workSched = GetDateTimeData(dataReader, "workSched");
                info.userId = ((string)dataReader["userId"]).Trim();
                try
                {
                    info.validationid = (int)dataReader["validationid"];
                }
                catch
                {
                    info.validationid = 0;
                }
                
                aspectlist.Add(info);
            }
        }

        public List<DB.Aspect_Data> GetResults()
        {
            return aspectlist.Count == 0 ? null : aspectlist;
        }

        public DB.Aspect_Data GetResult()
        {
            return aspectlist.Count == 0 ? null : aspectlist[0];
        }
    }
}
