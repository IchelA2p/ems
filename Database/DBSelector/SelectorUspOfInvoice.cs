﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorUspOfInvoice : DatabaseSelector
    {
        private List<Database.DB.UspOfInvoice> list;

        public SelectorUspOfInvoice()
         {
             list = new List<DB.UspOfInvoice>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.UspOfInvoice info = new DB.UspOfInvoice();
                try
                {
                    info.invoice_name = ((string)dataReader["invoice_name"]).Trim();
                }
                catch
                {
                    info.invoice_name = string.Empty;
                }
                try
                {
                    info.usp_name = ((string)dataReader["usp_name"]).Trim();
                }
                catch
                {
                    info.usp_name = string.Empty;
                }
                //processed
                try
                {
                    info.processed = ((string)dataReader["processed"]).Trim();
                }
                catch
                {
                    info.processed = string.Empty;
                }
                list.Add(info);
            }
        }

        public List<DB.UspOfInvoice> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.UspOfInvoice GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
