﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace Database.DBSelector
{
    public class SelectorAccountNumber : DatabaseSelector
    {
        private List<Database.DB.AccountNumber> list;

        public SelectorAccountNumber()
         {
             list = new List<DB.AccountNumber>();
         }

        public override void GetData(SqlDataReader dataReader)
        {
            list.Clear();
            while (dataReader.Read())
            {
                DB.AccountNumber info = new DB.AccountNumber();
                try
                {
                    info.property_code = ((string)dataReader["property_code"]).Trim();
                }
                catch
                {
                    info.property_code = string.Empty;
                }
                try
                {
                    info.account_number = ((string)dataReader["account_number"]).Trim();
                }
                catch
                {
                    info.account_number = string.Empty;
                }

                list.Add(info);
            }
        }

        public List<DB.AccountNumber> GetResults()
        {
            return list.Count == 0 ? null : list;
        }

        public DB.AccountNumber GetResult()
        {
            return list.Count == 0 ? null : list[0];
        }
    }
}
